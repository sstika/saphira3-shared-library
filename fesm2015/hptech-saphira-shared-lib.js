import { Injectable, ɵɵdefineInjectable, ɵɵinject, Component, Inject, EventEmitter, Input, Output, Pipe, NgModule, ViewChild } from '@angular/core';
import { Router, RouterModule, ActivatedRoute } from '@angular/router';
import { Validators, FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog, MatAutocompleteModule, MatButtonModule, MatButtonToggleModule, MatCardModule, MatCheckboxModule, MatChipsModule, MatDatepickerModule, MatDialogModule, MatExpansionModule, MatFormFieldModule, MatGridListModule, MatIconModule, MatInputModule, MatListModule, MatMenuModule, MatNativeDateModule, MatPaginatorModule, MatProgressBarModule, MatProgressSpinnerModule, MatRadioModule, MatSelectModule, MatSortModule, MatSidenavModule, MatSliderModule, MatSlideToggleModule, MatSnackBarModule, MatTableModule, MatTabsModule, MatToolbarModule, MatTooltipModule, MatDatepicker } from '@angular/material';
import { HttpErrorResponse, HttpClient } from '@angular/common/http';
import { throwError, BehaviorSubject, of, EMPTY, interval, concat, timer, Subject } from 'rxjs';
import { map, catchError, tap, skipWhile, mergeMap, retry, takeWhile, distinctUntilChanged } from 'rxjs/operators';
import { isNullOrUndefined } from 'util';
import { isEmpty, isNull, isUndefined, cloneDeep } from 'lodash';
import * as moment_ from 'moment';
import { Title } from '@angular/platform-browser';
import 'moment-timezone';
import { MatDialogRef as MatDialogRef$1, MAT_DIALOG_DATA as MAT_DIALOG_DATA$1 } from '@angular/material/dialog';
import { DatePipe, CommonModule } from '@angular/common';
import { AgGridModule } from 'ag-grid-angular/main';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
const translateDistance = (/**
 * @param {?} unit
 * @param {?=} full
 * @return {?}
 */
(unit, full = false) => {
    /** @type {?} */
    let string = '';
    switch (unit) {
        case 'METER':
            string = 'Meters';
            break;
        case 'KILOMETER':
            string = 'Kilometers';
            break;
        case 'FOOT':
            string = 'Feet';
            break;
        case 'FOOT_US':
            string = 'ft US';
            break;
        case 'YARD':
            string = 'Yards';
            break;
        case 'MILE':
            string = 'Miles';
            break;
        default:
            string = unit;
            break;
    }
    return string;
});
/** @type {?} */
const distanceArray = ['FOOT', 'FOOT_US', 'KILOMETER', 'METER', 'MILE', 'YARD'];
/** @type {?} */
const distanceObject = {
    'FOOT': 'Feet',
    'FOOT_US': 'ft US',
    'KILOMETER': 'Kilometers',
    'METER': 'Meters',
    'MILE': 'Miles',
    'YARD': 'Yards'
};
/** @type {?} */
const formatObject = {
    'DEGREE': '°',
    'GRADIAN': 'gon',
    'RADIAN': 'rad',
    'NANOTESLA': 'nT',
    'MICROTESLA': 'uT',
    'MILLITESLA': 'mT',
    'TESLA': 'T',
    'GAUSS': 'Gauss',
    'GAMMA': 'Gamma',
    'M_PER_SEC_SQ': 'M Per Sec Sq',
    'FT_PER_SEC_SQ': 'Ft Per Sec Sq',
    'G': 'G',
    'MILLI_G': 'Milli G',
    'G_98': 'G 98',
    'GAL': 'Gal',
    'MILLI_GAL': 'Milli Gal',
    'FOOT': 'Feet',
    'FOOT_US': 'ft US',
    'KILOMETER': 'Kilometers',
    'METER': 'Meters',
    'MILE': 'Miles',
    'YARD': 'Yards',
    'STANDARD': 'D-S',
    'POOR': 'D-P',
    'DEFINITIVE_INTERPOLATED': 'D-I',
    'BAD': 'X-B',
    'ACC_CHECK': 'X-A',
    'CHECKSHOT': 'X-C',
    'INTERPOLATED': 'X-I'
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
/** @type {?} */
const environment = {
    production: false,
    googleAPIKey: 'AIzaSyDIhkFBA0wAfqQnblc2OxCOn--FG_mDWFU',
    apiRoot: 'http://localhost'
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*
 * NOTES
 *   Using withCredentials is a huge security no-no!
 *
 *   It leaks the user's username and password with every call, and while
 *   using HTTPS help mitigate the problem, it is generally regarded as a
 *   bad practice.
 *
 *   We would need to fix the back end to patch this vulernability.
 */
class DataService {
    /**
     * @param {?} http
     */
    constructor(http) {
        this.http = http;
        this.urlRoot = environment.apiRoot + '/saphira/api/';
    }
    /**
     * This function is for extracting the usable value from GET,POST,PUT,DELETE Response.
     * @protected
     * @param {?} res - {any} the response object (possibly) from a call;
     * @return {?} Observable<any> -  Returns an Observable<any> .
     */
    extractData(res) {
        if (!res) {
            return null;
        }
        if (res.body === undefined) {
            return res;
        }
        return res.body;
    }
    /**
     * Display a notification that informs the user that an HTTP error occurred.
     * Must be consumed by the child service, does NOT get called by default.
     * @protected
     * @param {?} msg User friendly message to display to the user
     * @return {?}
     */
    displayErrorMsg(msg) {
        // TODO: this is a placeholder, a snackbar is what should be used
    }
    /**
     * Sends a verbose error message to the console for further debugging
     * @protected
     * @param {?} error - HttpErrorResponse error object that is supplied via HttpClient
     * @param {?} method - the HTTP method used
     * @return {?} `HttpError` that was passed in or a placeholder if none was provided
     */
    logError(error, method) {
        if (isNullOrUndefined(error) || isEmpty(error)) {
            /** @type {?} */
            const err = new Error('An unknown error occurred and no error was supplied to the handler');
            error = new HttpErrorResponse({ error: err });
        }
        /** @type {?} */
        const message = error.message || 'An error occurred and no message was supplied to the handler';
        /** @type {?} */
        const url = error.url || '???';
        // TODO: if we get a lot of ???'s we can pass in the URL
        /** @type {?} */
        const status = error.status || '???';
        /** @type {?} */
        const statusText = error.statusText || '???';
        /** @type {?} */
        const headers = this.headers2str(error.headers) || '???';
        /** @type {?} */
        const stack = error.error.stack || 'No Call Stack Provided';
        /** @type {?} */
        const moment = moment_;
        // TODO: remote logging would go here!
        // Error Message here!
        // Context: http://localhost:4200/#/
        // Action:  GET /saphira/api/ping
        // Status:  400 statusText
        // Time:    2019-04-23T15:28:00+8
        // Headers: {foo: bar}
        // call stack
        // <this line intentionally left blank>
        console.error(message + '\n' +
            `Context: ${window.location.href}\n` +
            `Action:  ${method} ${url}\n` +
            `Status:  ${status} ${statusText}\n` +
            `Time:    ${moment().toISOString(true)}\n` +
            `Headers: ${headers}\n` +
            stack + '\n\n');
        return throwError(error);
    }
    /**
     * Maps `HttpHeaders` to simple JSON string for debugging
     * @private
     * @param {?} headers
     * @return {?}
     */
    headers2str(headers) {
        if (isNullOrUndefined(headers)) {
            return null;
        }
        /** @type {?} */
        const json = {};
        for (const key of headers.keys()) {
            /** @type {?} */
            const values = headers.getAll(key);
            if (values.length === 1) {
                json[key] = values[0];
                continue;
            }
            json[key] = [...values];
        }
        return JSON.stringify(json, null, 2);
    }
    /**
     * Master GET function.
     * @protected
     * @param {?} path - string path of the endpoint;
     * @param {?=} headers - custom headers supplied to the call;
     * @return {?} Observable<any> -  Returns an Observable<any> .
     */
    get(path, headers) {
        /** @type {?} */
        const options = { withCredentials: true };
        if (headers) {
            options.headers = headers;
        }
        return this.http
            .get(this.urlRoot + path, options)
            .pipe(map(this.extractData), catchError((/**
         * @param {?} err
         * @return {?}
         */
        err => this.logError(err, 'GET'))));
    }
    /**
     * Master PUT function.
     * @protected
     * @param {?} path - string path of the endpoint;
     * @param {?} obj - object for which you want to put;
     * @param {?=} headers - custom headers supplied to the call;
     * @return {?} Observable<any> -  Returns an Observable<any> .
     */
    put(path, obj, headers) {
        /** @type {?} */
        const options = { withCredentials: true };
        if (headers) {
            options.headers = headers;
        }
        return this.http
            .put(this.urlRoot + path, obj, options)
            .pipe(map(this.extractData), catchError((/**
         * @param {?} err
         * @return {?}
         */
        err => this.logError(err, 'PUT'))));
    }
    /**
     * Master POST function.
     * @protected
     * @param {?} path - string path of the endpoint;
     * @param {?} obj - object for which you want to put;
     * @param {?=} headers - custom headers supplied to the call;
     * @return {?} Observable<any> -  Returns an Observable<any> .
     */
    post(path, obj, headers) {
        /** @type {?} */
        const options = { withCredentials: true };
        if (headers) {
            options.headers = headers;
        }
        return this.http
            .post(this.urlRoot + path, obj, options)
            .pipe(map(this.extractData), map((/**
         * @param {?} res
         * @return {?}
         */
        res => res || isNullOrUndefined(res))), // TODO: Need to investigate further
        catchError((/**
         * @param {?} err
         * @return {?}
         */
        err => this.logError(err, 'POST'))));
    }
    /**
     * Master DELETE function.
     * @protected
     * @param {?} path - string path of the endpoint;
     * @param {?=} headers - custom headers supplied to the call;
     * @return {?} Observable<any> -  Returns an Observable<any> .
     */
    delete(path, headers) {
        /** @type {?} */
        const options = { withCredentials: true };
        if (headers) {
            options.headers = headers;
        }
        return this.http
            .delete(this.urlRoot + path, options)
            .pipe(map(this.extractData), map((/**
         * @param {?} res
         * @return {?}
         */
        res => res || isNullOrUndefined(res))), // TODO: Need to investigate further
        catchError((/**
         * @param {?} err
         * @return {?}
         */
        err => this.logError(err, 'DELETE'))));
    }
}
DataService.decorators = [
    { type: Injectable }
];
/** @nocollapse */
DataService.ctorParameters = () => [
    { type: HttpClient }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class CompanyService extends DataService {
    /**
     * @param {?} http
     */
    constructor(http) {
        super(http);
        this.allCompanies = 'repositories/company/getAll';
        this.getAllUnderCompany = 'repositories/company/getAllUnderCompany';
        this.addComp = 'repositories/company/add';
        this.getOneComp = 'repositories/company/getOne';
        this.deleteComp = 'repositories/company/delete';
        this.companySnapshot = 'utils/getCompanySnapshot';
        this.restoreFromCompanySnapshot = 'utils/restoreFromCompanySnapshot';
        this.downloadAtt = 'attachments/search/findById';
        this.getOneMeta = 'repositories/company/getOneMeta';
        this.update = 'repositories/company/update';
        this.lastModifiedDate = 'repositories/company/lastModifiedDate';
        this.currentCompany = new BehaviorSubject(null);
    }
    /**
     * @return {?}
     */
    getAllCompanies() {
        return super
            .get(this.allCompanies).pipe(catchError((/**
         * @param {?} err
         * @return {?}
         */
        err => of(false))));
    }
    /**
     * @param {?} compId
     * @return {?}
     */
    getAllForCompany(compId) {
        return super
            .get(`${this.getAllUnderCompany}?id=${compId}`).pipe(catchError((/**
         * @param {?} err
         * @return {?}
         */
        err => of(false))));
    }
    /**
     * @param {?} compId
     * @return {?}
     */
    getOne(compId) {
        return super
            .get(`${this.getOneComp}?id=${compId}`).pipe(tap((/**
         * @param {?} company
         * @return {?}
         */
        company => this.currentCompany.next(company))), catchError((/**
         * @param {?} err
         * @return {?}
         */
        err => of(false))));
    }
    /**
     * @return {?}
     */
    getCurrent() {
        return this.currentCompany
            .asObservable()
            .pipe(skipWhile((/**
         * @param {?} company
         * @return {?}
         */
        company => isNullOrUndefined(company))));
    }
    /**
     * @param {?} compId
     * @return {?}
     */
    delete(compId) {
        return super
            .delete(`${this.deleteComp}?id=${compId}`).pipe(catchError((/**
         * @param {?} err
         * @return {?}
         */
        err => of(false))));
    }
    /**
     * @param {?} newCompObj
     * @param {?=} headerObj
     * @return {?}
     */
    add(newCompObj, headerObj = {}) {
        return this
            .post(this.addComp, newCompObj, headerObj).pipe(catchError((/**
         * @param {?} err
         * @return {?}
         */
        err => of(false))));
    }
    /**
     * @param {?} compId
     * @return {?}
     */
    getCompanySnapshot(compId) {
        return super
            .get(`${this.companySnapshot}?companyId=${compId}`)
            .pipe(catchError((/**
         * @param {?} err
         * @return {?}
         */
        err => of(false))));
    }
    /**
     * @param {?} bodyObj
     * @param {?=} headerObj
     * @return {?}
     */
    postCompanyUpdate(bodyObj, headerObj = {}) {
        return super.post(this.update, bodyObj, headerObj).pipe(catchError((/**
         * @param {?} err
         * @return {?}
         */
        err => of(false))));
    }
    /**
     * @return {?}
     */
    returnFileTreeCompanies() {
        return super
            .get(this.allCompanies).pipe(tap((/**
         * @param {?} comps
         * @return {?}
         */
        comps => {
            return this.companiesFiltered = this.cleanCompanies(comps);
        })), catchError((/**
         * @param {?} err
         * @return {?}
         */
        err => of(false))));
    }
    /**
     * @param {?} companies
     * @return {?}
     */
    cleanCompanies(companies) {
        if (!companies) {
            return;
        }
        /** @type {?} */
        const uniqueCompanyNames = [];
        return companies.reduce((/**
         * @param {?} newArr
         * @param {?} comp
         * @return {?}
         */
        (newArr, comp) => {
            /** @type {?} */
            let incrementer;
            if (!uniqueCompanyNames.includes(comp.name)) {
                uniqueCompanyNames.push(comp.name);
                comp['hierarchy'] = [`${comp['commonName']} : ${comp['name']}`];
            }
            else {
                comp['hierarchy'] = [`${comp['commonName']} : ${comp['name']}${incrementer += 1}`];
            }
            newArr.push(comp);
            return newArr;
        }), []);
    }
    /**
     * @return {?}
     */
    returnFilteredComps() {
        return this.companiesFiltered;
    }
}
CompanyService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
CompanyService.ctorParameters = () => [
    { type: HttpClient }
];
/** @nocollapse */ CompanyService.ngInjectableDef = ɵɵdefineInjectable({ factory: function CompanyService_Factory() { return new CompanyService(ɵɵinject(HttpClient)); }, token: CompanyService, providedIn: "root" });

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class EditCoordinationService {
    constructor() {
        this.editStatus = {
            infoCompany: false,
            infoField: false,
            infoPad: false,
            infoWell: false,
            wellboreInfo: false,
            wellboreMagnetic: false,
            trajectoryInfo: false,
            trajectoryTieIn: false,
            trajectorySurveys: false,
            surveySetInfo: false,
            surveySetRigContact: false,
            surveySetGrid: false,
        };
        this.editFieldStatus = new BehaviorSubject(false);
        this.getEditFieldStatus = this.editFieldStatus.asObservable();
    }
    /**
     * @param {?} field
     * @param {?} status
     * @return {?}
     */
    toggleField(field, status) {
        this.editStatus[field] = status;
        this.setEditFieldStatus(this.editStatus);
    }
    /**
     * @return {?}
     */
    areEditFieldsClosed() {
        /** @type {?} */
        let currentlyEdited = 0;
        Object.keys(this.editStatus).forEach((/**
         * @param {?} es
         * @return {?}
         */
        es => {
            if (this.editStatus[es] === true) {
                currentlyEdited++;
            }
        }));
        return currentlyEdited === 0;
    }
    /**
     * @private
     * @param {?} set
     * @return {?}
     */
    setEditFieldStatus(set) {
        this.editFieldStatus.next(set);
    }
}
EditCoordinationService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
EditCoordinationService.ctorParameters = () => [];
/** @nocollapse */ EditCoordinationService.ngInjectableDef = ɵɵdefineInjectable({ factory: function EditCoordinationService_Factory() { return new EditCoordinationService(); }, token: EditCoordinationService, providedIn: "root" });

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class ErrorModalComponent {
    /**
     * @param {?} dialogRef
     * @param {?} data
     */
    constructor(dialogRef, data) {
        this.dialogRef = dialogRef;
        this.data = data;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
}
ErrorModalComponent.decorators = [
    { type: Component, args: [{
                selector: 'app-error-modal',
                template: "<div mat-dialog-content class=\"modal-content\">\n    <div class=\"modal-detail\">\n      <i *ngIf=\"data['action'] === 'error'\" class=\"material-icons warning\">\n        error_outline\n      </i>\n      <i *ngIf=\"data['action'] === 'field-validity'\" class=\"material-icons warning\">\n        warning\n      </i>\n      <h2 class=\"alert-fields main-alert\">{{data.errMessage}}</h2>\n      <div class=\"alert-fields\" *ngIf=\"data['action'] === 'error'\">\n        If error continues reload the page, check network connectivity or contact MagVAR support directly\n      </div>\n    </div>\n</div>\n<div mat-dialog-actions class=\"button-container\">\n  <button class=\"btn\"\n          id=\"alert-modal-ok-btn\"\n          [mat-dialog-close]\n          mat-flat-button\n          color=\"primary\">Ok</button>\n</div>\n",
                styles: ["@media print{.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-appearance-legacy.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-1.28122em) scale(.75);transform:translateY(-1.28122em) scale(.75)}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-form-field-autofill-control:-webkit-autofill+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.28121em) scale(.75);transform:translateY(-1.28121em) scale(.75)}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.2812em) scale(.75);transform:translateY(-1.2812em) scale(.75)}}.mat-badge-content{font-weight:600;font-size:12px;font-family:NunitoSans,sans-serif}.mat-badge-small .mat-badge-content{font-size:9px}.mat-badge-large .mat-badge-content{font-size:24px}.mat-h1,.mat-headline,.mat-typography h1{font:400 24px/32px NunitoSans,sans-serif;margin:0 0 16px}.mat-h2,.mat-title,.mat-typography h2{font:500 20px/32px NunitoSans,sans-serif;margin:0 0 16px}.mat-h3,.mat-subheading-2,.mat-typography h3{font:400 16px/28px NunitoSans,sans-serif;margin:0 0 16px}.mat-h4,.mat-subheading-1,.mat-typography h4{font:400 15px/24px NunitoSans,sans-serif;margin:0 0 16px}.mat-h5,.mat-h6,.mat-typography h5,.mat-typography h6{margin:0 0 12px}.mat-body-2,.mat-body-strong{font:500 14px/24px NunitoSans,sans-serif}.mat-body,.mat-body-1,.mat-typography{font:400 14px/20px NunitoSans,sans-serif}.mat-body p,.mat-body-1 p,.mat-typography p{margin:0 0 12px}.mat-caption,.mat-small{font:400 12px/20px NunitoSans,sans-serif}.mat-display-4,.mat-typography .mat-display-4{font:300 112px/112px NunitoSans,sans-serif;letter-spacing:-.05em;margin:0 0 56px}.mat-display-3,.mat-typography .mat-display-3{font:400 56px/56px NunitoSans,sans-serif;letter-spacing:-.02em;margin:0 0 64px}.mat-display-2,.mat-typography .mat-display-2{font:400 45px/48px NunitoSans,sans-serif;letter-spacing:-.005em;margin:0 0 64px}.mat-display-1,.mat-typography .mat-display-1{font:400 34px/40px NunitoSans,sans-serif;margin:0 0 64px}.mat-bottom-sheet-container{font:400 14px/20px NunitoSans,sans-serif}.mat-button,.mat-fab,.mat-flat-button,.mat-icon-button,.mat-mini-fab,.mat-raised-button,.mat-stroked-button{font-family:NunitoSans,sans-serif;font-size:14px;font-weight:500}.mat-button-toggle,.mat-card{font-family:NunitoSans,sans-serif}.mat-card-title{font-size:24px;font-weight:500}.mat-card-header .mat-card-title{font-size:20px}.mat-card-content,.mat-card-subtitle{font-size:14px}.mat-checkbox{font-family:NunitoSans,sans-serif}.mat-checkbox-layout .mat-checkbox-label{line-height:24px}.mat-chip{font-size:14px;font-weight:500}.mat-chip .mat-chip-remove.mat-icon,.mat-chip .mat-chip-trailing-icon.mat-icon{font-size:18px}.mat-table{font-family:NunitoSans,sans-serif}.mat-header-cell{font-size:12px;font-weight:500}.mat-cell,.mat-footer-cell{font-size:14px}.mat-calendar{font-family:NunitoSans,sans-serif}.mat-calendar-body{font-size:13px}.mat-calendar-body-label,.mat-calendar-period-button{font-size:14px;font-weight:500}.mat-calendar-table-header th{font-size:11px;font-weight:400}.mat-dialog-title{font:500 20px/32px NunitoSans,sans-serif}.mat-expansion-panel-header{font-family:NunitoSans,sans-serif;font-size:15px;font-weight:400}.mat-expansion-panel-content{font:400 14px/20px NunitoSans,sans-serif}.mat-form-field{font-size:inherit;font-weight:400;line-height:1.125;font-family:NunitoSans,sans-serif}.mat-form-field-wrapper{padding-bottom:1.34375em}.mat-form-field-prefix .mat-icon,.mat-form-field-suffix .mat-icon{font-size:150%;line-height:1.125}.mat-form-field-prefix .mat-icon-button,.mat-form-field-suffix .mat-icon-button{height:1.5em;width:1.5em}.mat-form-field-prefix .mat-icon-button .mat-icon,.mat-form-field-suffix .mat-icon-button .mat-icon{height:1.125em;line-height:1.125}.mat-form-field-infix{padding:.5em 0;border-top:.84375em solid transparent}.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-1.34373em) scale(.75);transform:translateY(-1.34373em) scale(.75);width:133.3333533333%}.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.34372em) scale(.75);transform:translateY(-1.34372em) scale(.75);width:133.3333633333%}.mat-form-field-label-wrapper{top:-.84375em;padding-top:.84375em}.mat-form-field-label{top:1.34375em}.mat-form-field-underline{bottom:1.34375em}.mat-form-field-subscript-wrapper{font-size:75%;margin-top:.6666666667em;top:calc(100% - 1.7916666667em)}.mat-form-field-appearance-legacy .mat-form-field-wrapper{padding-bottom:1.25em}.mat-form-field-appearance-legacy .mat-form-field-infix{padding:.4375em 0}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-appearance-legacy.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00106px);transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00106px);-ms-transform:translateY(-1.28119em) scale(.75);width:133.3333933333%}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-form-field-autofill-control:-webkit-autofill+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00107px);transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00107px);-ms-transform:translateY(-1.28118em) scale(.75);width:133.3334033333%}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00108px);transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00108px);-ms-transform:translateY(-1.28117em) scale(.75);width:133.3334133333%}.mat-form-field-appearance-legacy .mat-form-field-label{top:1.28125em}.mat-form-field-appearance-legacy .mat-form-field-underline{bottom:1.25em}.mat-form-field-appearance-legacy .mat-form-field-subscript-wrapper{margin-top:.5416666667em;top:calc(100% - 1.6666666667em)}@media print{.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-appearance-legacy.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-1.28116em) scale(.75);transform:translateY(-1.28116em) scale(.75)}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-form-field-autofill-control:-webkit-autofill+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.28115em) scale(.75);transform:translateY(-1.28115em) scale(.75)}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.28114em) scale(.75);transform:translateY(-1.28114em) scale(.75)}}.mat-form-field-appearance-fill .mat-form-field-infix{padding:.25em 0 .75em}.mat-form-field-appearance-fill .mat-form-field-label{top:1.09375em;margin-top:-.5em}.mat-form-field-appearance-fill.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-appearance-fill.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-.59373em) scale(.75);transform:translateY(-.59373em) scale(.75);width:133.3333533333%}.mat-form-field-appearance-fill.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-.59372em) scale(.75);transform:translateY(-.59372em) scale(.75);width:133.3333633333%}.mat-form-field-appearance-outline .mat-form-field-infix{padding:1em 0}.mat-form-field-appearance-outline .mat-form-field-label{top:1.84375em;margin-top:-.25em}.mat-form-field-appearance-outline.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-appearance-outline.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-1.59373em) scale(.75);transform:translateY(-1.59373em) scale(.75);width:133.3333533333%}.mat-form-field-appearance-outline.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.59372em) scale(.75);transform:translateY(-1.59372em) scale(.75);width:133.3333633333%}.mat-grid-tile-footer,.mat-grid-tile-header{font-size:14px}.mat-grid-tile-footer .mat-line,.mat-grid-tile-header .mat-line{white-space:nowrap;overflow:hidden;text-overflow:ellipsis;display:block;box-sizing:border-box}.mat-grid-tile-footer .mat-line:nth-child(n+2),.mat-grid-tile-header .mat-line:nth-child(n+2){font-size:12px}input.mat-input-element{margin-top:-.0625em}.mat-menu-item{font-family:NunitoSans,sans-serif;font-size:14px;font-weight:400}.mat-paginator,.mat-paginator-page-size .mat-select-trigger{font-family:NunitoSans,sans-serif;font-size:12px}.mat-radio-button,.mat-select{font-family:NunitoSans,sans-serif}.mat-select-trigger{height:1.125em}.mat-slide-toggle-content{font-family:NunitoSans,sans-serif}.mat-slider-thumb-label-text{font-family:NunitoSans,sans-serif;font-size:12px;font-weight:500}.mat-stepper-horizontal,.mat-stepper-vertical{font-family:NunitoSans,sans-serif}.mat-step-label{font-size:14px;font-weight:400}.mat-step-sub-label-error{font-weight:400}.mat-step-label-error{font-size:14px}.mat-step-label-selected{font-size:14px;font-weight:500}.mat-tab-group{font-family:NunitoSans,sans-serif}.mat-tab-label,.mat-tab-link{font-family:NunitoSans,sans-serif;font-size:14px;font-weight:500}.mat-toolbar,.mat-toolbar h1,.mat-toolbar h2,.mat-toolbar h3,.mat-toolbar h4,.mat-toolbar h5,.mat-toolbar h6{font:500 20px/32px NunitoSans,sans-serif;margin:0}.mat-tooltip{font-family:NunitoSans,sans-serif;font-size:10px;padding-top:6px;padding-bottom:6px}.mat-tooltip-handset{font-size:14px;padding-top:8px;padding-bottom:8px}.mat-list-item,.mat-list-option{font-family:NunitoSans,sans-serif}.mat-list-base .mat-list-item{font-size:16px}.mat-list-base .mat-list-item .mat-line{white-space:nowrap;overflow:hidden;text-overflow:ellipsis;display:block;box-sizing:border-box}.mat-list-base .mat-list-item .mat-line:nth-child(n+2){font-size:14px}.mat-list-base .mat-list-option{font-size:16px}.mat-list-base .mat-list-option .mat-line{white-space:nowrap;overflow:hidden;text-overflow:ellipsis;display:block;box-sizing:border-box}.mat-list-base .mat-list-option .mat-line:nth-child(n+2){font-size:14px}.mat-list-base .mat-subheader{font-family:NunitoSans,sans-serif;font-size:14px;font-weight:500}.mat-list-base[dense] .mat-list-item{font-size:12px}.mat-list-base[dense] .mat-list-item .mat-line{white-space:nowrap;overflow:hidden;text-overflow:ellipsis;display:block;box-sizing:border-box}.mat-list-base[dense] .mat-list-item .mat-line:nth-child(n+2),.mat-list-base[dense] .mat-list-option{font-size:12px}.mat-list-base[dense] .mat-list-option .mat-line{white-space:nowrap;overflow:hidden;text-overflow:ellipsis;display:block;box-sizing:border-box}.mat-list-base[dense] .mat-list-option .mat-line:nth-child(n+2){font-size:12px}.mat-list-base[dense] .mat-subheader{font-family:NunitoSans,sans-serif;font-size:12px;font-weight:500}.mat-option{font-family:NunitoSans,sans-serif;font-size:16px}.mat-optgroup-label{font:500 14px/24px NunitoSans,sans-serif}.mat-simple-snackbar{font-family:NunitoSans,sans-serif;font-size:14px}.mat-simple-snackbar-action{line-height:1;font-family:inherit;font-size:inherit;font-weight:500}.mat-tree{font-family:NunitoSans,sans-serif}.mat-nested-tree-node,.mat-tree-node{font-weight:400;font-size:14px}.mat-ripple{overflow:hidden;position:relative}.mat-ripple.mat-ripple-unbounded{overflow:visible}.mat-ripple-element{position:absolute;border-radius:50%;pointer-events:none;-webkit-transition:opacity,-webkit-transform cubic-bezier(0,0,.2,1);transition:opacity,transform cubic-bezier(0,0,.2,1),-webkit-transform cubic-bezier(0,0,.2,1);-webkit-transform:scale(0);transform:scale(0)}@media (-ms-high-contrast:active){.mat-ripple-element{display:none}}.cdk-visually-hidden{border:0;clip:rect(0 0 0 0);height:1px;margin:-1px;overflow:hidden;padding:0;position:absolute;width:1px;outline:0;-webkit-appearance:none;-moz-appearance:none}.cdk-global-overlay-wrapper,.cdk-overlay-container{pointer-events:none;top:0;left:0;height:100%;width:100%}.cdk-overlay-container{position:fixed;z-index:1000}.cdk-overlay-container:empty{display:none}.cdk-global-overlay-wrapper{display:-webkit-box;display:flex;position:absolute;z-index:1000}.cdk-overlay-pane{position:absolute;pointer-events:auto;box-sizing:border-box;z-index:1000;display:-webkit-box;display:flex;max-width:100%;max-height:100%}.cdk-overlay-backdrop{position:absolute;top:0;bottom:0;left:0;right:0;z-index:1000;pointer-events:auto;-webkit-tap-highlight-color:transparent;-webkit-transition:opacity .4s cubic-bezier(.25,.8,.25,1);transition:opacity .4s cubic-bezier(.25,.8,.25,1);opacity:0}.cdk-overlay-backdrop.cdk-overlay-backdrop-showing{opacity:1}@media screen and (-ms-high-contrast:active){.cdk-overlay-backdrop.cdk-overlay-backdrop-showing{opacity:.6}}.cdk-overlay-dark-backdrop{background:rgba(0,0,0,.32)}.cdk-overlay-transparent-backdrop,.cdk-overlay-transparent-backdrop.cdk-overlay-backdrop-showing{opacity:0}.cdk-overlay-connected-position-bounding-box{position:absolute;z-index:1000;display:-webkit-box;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;flex-direction:column;min-width:1px;min-height:1px}.cdk-global-scrollblock{position:fixed;width:100%;overflow-y:scroll}@-webkit-keyframes cdk-text-field-autofill-start{/*!*/}@keyframes cdk-text-field-autofill-start{/*!*/}@-webkit-keyframes cdk-text-field-autofill-end{/*!*/}@keyframes cdk-text-field-autofill-end{/*!*/}.cdk-text-field-autofill-monitored:-webkit-autofill{-webkit-animation-name:cdk-text-field-autofill-start;animation-name:cdk-text-field-autofill-start}.cdk-text-field-autofill-monitored:not(:-webkit-autofill){-webkit-animation-name:cdk-text-field-autofill-end;animation-name:cdk-text-field-autofill-end}textarea.cdk-textarea-autosize{resize:none}textarea.cdk-textarea-autosize-measuring{height:auto!important;overflow:hidden!important;padding:2px 0!important;box-sizing:content-box!important}.modal-content{display:-webkit-box;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;flex-direction:column;align-content:center;font-family:NunitoSans,sans-serif;margin:20px 10px 10px}.modal-detail{display:-webkit-box;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;flex-direction:column;-webkit-box-align:center;align-items:center;text-align:center}.button-container{display:-webkit-box;display:flex;justify-content:space-around;margin:10px 10px 20px}.btn{width:120px}.alert-fields{margin:10px 0}.warning{color:#f99;font-size:120px}.info{color:#64b5f6;font-size:120px}"]
            }] }
];
/** @nocollapse */
ErrorModalComponent.ctorParameters = () => [
    { type: MatDialogRef },
    { type: undefined, decorators: [{ type: Inject, args: [MAT_DIALOG_DATA,] }] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class SuccessModalComponent {
    /**
     * @param {?} dialogRef
     * @param {?} data
     */
    constructor(dialogRef, data) {
        this.dialogRef = dialogRef;
        this.data = data;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
}
SuccessModalComponent.decorators = [
    { type: Component, args: [{
                selector: 'app-success-modal',
                template: "<div mat-dialog-content class=\"modal-content\">\n    <div class=\"modal-detail\">\n      <i class=\"material-icons success\">\n        {{data['icon'] ? data.icon : 'check_circle'}}\n      </i>\n      <h2 class=\"alert-fields main-alert\">{{data.successMessage}}</h2>\n    </div>\n</div>\n<div mat-dialog-actions class=\"button-container\">\n  <button class=\"btn\"\n          id=\"alert-modal-ok-btn\"\n          [mat-dialog-close]\n          mat-flat-button\n          color=\"primary\">Ok</button>\n</div>\n",
                styles: ["@media print{.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-appearance-legacy.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-1.28122em) scale(.75);transform:translateY(-1.28122em) scale(.75)}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-form-field-autofill-control:-webkit-autofill+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.28121em) scale(.75);transform:translateY(-1.28121em) scale(.75)}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.2812em) scale(.75);transform:translateY(-1.2812em) scale(.75)}}.mat-badge-content{font-weight:600;font-size:12px;font-family:NunitoSans,sans-serif}.mat-badge-small .mat-badge-content{font-size:9px}.mat-badge-large .mat-badge-content{font-size:24px}.mat-h1,.mat-headline,.mat-typography h1{font:400 24px/32px NunitoSans,sans-serif;margin:0 0 16px}.mat-h2,.mat-title,.mat-typography h2{font:500 20px/32px NunitoSans,sans-serif;margin:0 0 16px}.mat-h3,.mat-subheading-2,.mat-typography h3{font:400 16px/28px NunitoSans,sans-serif;margin:0 0 16px}.mat-h4,.mat-subheading-1,.mat-typography h4{font:400 15px/24px NunitoSans,sans-serif;margin:0 0 16px}.mat-h5,.mat-h6,.mat-typography h5,.mat-typography h6{margin:0 0 12px}.mat-body-2,.mat-body-strong{font:500 14px/24px NunitoSans,sans-serif}.mat-body,.mat-body-1,.mat-typography{font:400 14px/20px NunitoSans,sans-serif}.mat-body p,.mat-body-1 p,.mat-typography p{margin:0 0 12px}.mat-caption,.mat-small{font:400 12px/20px NunitoSans,sans-serif}.mat-display-4,.mat-typography .mat-display-4{font:300 112px/112px NunitoSans,sans-serif;letter-spacing:-.05em;margin:0 0 56px}.mat-display-3,.mat-typography .mat-display-3{font:400 56px/56px NunitoSans,sans-serif;letter-spacing:-.02em;margin:0 0 64px}.mat-display-2,.mat-typography .mat-display-2{font:400 45px/48px NunitoSans,sans-serif;letter-spacing:-.005em;margin:0 0 64px}.mat-display-1,.mat-typography .mat-display-1{font:400 34px/40px NunitoSans,sans-serif;margin:0 0 64px}.mat-bottom-sheet-container{font:400 14px/20px NunitoSans,sans-serif}.mat-button,.mat-fab,.mat-flat-button,.mat-icon-button,.mat-mini-fab,.mat-raised-button,.mat-stroked-button{font-family:NunitoSans,sans-serif;font-size:14px;font-weight:500}.mat-button-toggle,.mat-card{font-family:NunitoSans,sans-serif}.mat-card-title{font-size:24px;font-weight:500}.mat-card-header .mat-card-title{font-size:20px}.mat-card-content,.mat-card-subtitle{font-size:14px}.mat-checkbox{font-family:NunitoSans,sans-serif}.mat-checkbox-layout .mat-checkbox-label{line-height:24px}.mat-chip{font-size:14px;font-weight:500}.mat-chip .mat-chip-remove.mat-icon,.mat-chip .mat-chip-trailing-icon.mat-icon{font-size:18px}.mat-table{font-family:NunitoSans,sans-serif}.mat-header-cell{font-size:12px;font-weight:500}.mat-cell,.mat-footer-cell{font-size:14px}.mat-calendar{font-family:NunitoSans,sans-serif}.mat-calendar-body{font-size:13px}.mat-calendar-body-label,.mat-calendar-period-button{font-size:14px;font-weight:500}.mat-calendar-table-header th{font-size:11px;font-weight:400}.mat-dialog-title{font:500 20px/32px NunitoSans,sans-serif}.mat-expansion-panel-header{font-family:NunitoSans,sans-serif;font-size:15px;font-weight:400}.mat-expansion-panel-content{font:400 14px/20px NunitoSans,sans-serif}.mat-form-field{font-size:inherit;font-weight:400;line-height:1.125;font-family:NunitoSans,sans-serif}.mat-form-field-wrapper{padding-bottom:1.34375em}.mat-form-field-prefix .mat-icon,.mat-form-field-suffix .mat-icon{font-size:150%;line-height:1.125}.mat-form-field-prefix .mat-icon-button,.mat-form-field-suffix .mat-icon-button{height:1.5em;width:1.5em}.mat-form-field-prefix .mat-icon-button .mat-icon,.mat-form-field-suffix .mat-icon-button .mat-icon{height:1.125em;line-height:1.125}.mat-form-field-infix{padding:.5em 0;border-top:.84375em solid transparent}.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-1.34373em) scale(.75);transform:translateY(-1.34373em) scale(.75);width:133.3333533333%}.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.34372em) scale(.75);transform:translateY(-1.34372em) scale(.75);width:133.3333633333%}.mat-form-field-label-wrapper{top:-.84375em;padding-top:.84375em}.mat-form-field-label{top:1.34375em}.mat-form-field-underline{bottom:1.34375em}.mat-form-field-subscript-wrapper{font-size:75%;margin-top:.6666666667em;top:calc(100% - 1.7916666667em)}.mat-form-field-appearance-legacy .mat-form-field-wrapper{padding-bottom:1.25em}.mat-form-field-appearance-legacy .mat-form-field-infix{padding:.4375em 0}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-appearance-legacy.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00106px);transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00106px);-ms-transform:translateY(-1.28119em) scale(.75);width:133.3333933333%}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-form-field-autofill-control:-webkit-autofill+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00107px);transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00107px);-ms-transform:translateY(-1.28118em) scale(.75);width:133.3334033333%}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00108px);transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00108px);-ms-transform:translateY(-1.28117em) scale(.75);width:133.3334133333%}.mat-form-field-appearance-legacy .mat-form-field-label{top:1.28125em}.mat-form-field-appearance-legacy .mat-form-field-underline{bottom:1.25em}.mat-form-field-appearance-legacy .mat-form-field-subscript-wrapper{margin-top:.5416666667em;top:calc(100% - 1.6666666667em)}@media print{.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-appearance-legacy.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-1.28116em) scale(.75);transform:translateY(-1.28116em) scale(.75)}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-form-field-autofill-control:-webkit-autofill+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.28115em) scale(.75);transform:translateY(-1.28115em) scale(.75)}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.28114em) scale(.75);transform:translateY(-1.28114em) scale(.75)}}.mat-form-field-appearance-fill .mat-form-field-infix{padding:.25em 0 .75em}.mat-form-field-appearance-fill .mat-form-field-label{top:1.09375em;margin-top:-.5em}.mat-form-field-appearance-fill.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-appearance-fill.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-.59373em) scale(.75);transform:translateY(-.59373em) scale(.75);width:133.3333533333%}.mat-form-field-appearance-fill.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-.59372em) scale(.75);transform:translateY(-.59372em) scale(.75);width:133.3333633333%}.mat-form-field-appearance-outline .mat-form-field-infix{padding:1em 0}.mat-form-field-appearance-outline .mat-form-field-label{top:1.84375em;margin-top:-.25em}.mat-form-field-appearance-outline.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-appearance-outline.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-1.59373em) scale(.75);transform:translateY(-1.59373em) scale(.75);width:133.3333533333%}.mat-form-field-appearance-outline.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.59372em) scale(.75);transform:translateY(-1.59372em) scale(.75);width:133.3333633333%}.mat-grid-tile-footer,.mat-grid-tile-header{font-size:14px}.mat-grid-tile-footer .mat-line,.mat-grid-tile-header .mat-line{white-space:nowrap;overflow:hidden;text-overflow:ellipsis;display:block;box-sizing:border-box}.mat-grid-tile-footer .mat-line:nth-child(n+2),.mat-grid-tile-header .mat-line:nth-child(n+2){font-size:12px}input.mat-input-element{margin-top:-.0625em}.mat-menu-item{font-family:NunitoSans,sans-serif;font-size:14px;font-weight:400}.mat-paginator,.mat-paginator-page-size .mat-select-trigger{font-family:NunitoSans,sans-serif;font-size:12px}.mat-radio-button,.mat-select{font-family:NunitoSans,sans-serif}.mat-select-trigger{height:1.125em}.mat-slide-toggle-content{font-family:NunitoSans,sans-serif}.mat-slider-thumb-label-text{font-family:NunitoSans,sans-serif;font-size:12px;font-weight:500}.mat-stepper-horizontal,.mat-stepper-vertical{font-family:NunitoSans,sans-serif}.mat-step-label{font-size:14px;font-weight:400}.mat-step-sub-label-error{font-weight:400}.mat-step-label-error{font-size:14px}.mat-step-label-selected{font-size:14px;font-weight:500}.mat-tab-group{font-family:NunitoSans,sans-serif}.mat-tab-label,.mat-tab-link{font-family:NunitoSans,sans-serif;font-size:14px;font-weight:500}.mat-toolbar,.mat-toolbar h1,.mat-toolbar h2,.mat-toolbar h3,.mat-toolbar h4,.mat-toolbar h5,.mat-toolbar h6{font:500 20px/32px NunitoSans,sans-serif;margin:0}.mat-tooltip{font-family:NunitoSans,sans-serif;font-size:10px;padding-top:6px;padding-bottom:6px}.mat-tooltip-handset{font-size:14px;padding-top:8px;padding-bottom:8px}.mat-list-item,.mat-list-option{font-family:NunitoSans,sans-serif}.mat-list-base .mat-list-item{font-size:16px}.mat-list-base .mat-list-item .mat-line{white-space:nowrap;overflow:hidden;text-overflow:ellipsis;display:block;box-sizing:border-box}.mat-list-base .mat-list-item .mat-line:nth-child(n+2){font-size:14px}.mat-list-base .mat-list-option{font-size:16px}.mat-list-base .mat-list-option .mat-line{white-space:nowrap;overflow:hidden;text-overflow:ellipsis;display:block;box-sizing:border-box}.mat-list-base .mat-list-option .mat-line:nth-child(n+2){font-size:14px}.mat-list-base .mat-subheader{font-family:NunitoSans,sans-serif;font-size:14px;font-weight:500}.mat-list-base[dense] .mat-list-item{font-size:12px}.mat-list-base[dense] .mat-list-item .mat-line{white-space:nowrap;overflow:hidden;text-overflow:ellipsis;display:block;box-sizing:border-box}.mat-list-base[dense] .mat-list-item .mat-line:nth-child(n+2),.mat-list-base[dense] .mat-list-option{font-size:12px}.mat-list-base[dense] .mat-list-option .mat-line{white-space:nowrap;overflow:hidden;text-overflow:ellipsis;display:block;box-sizing:border-box}.mat-list-base[dense] .mat-list-option .mat-line:nth-child(n+2){font-size:12px}.mat-list-base[dense] .mat-subheader{font-family:NunitoSans,sans-serif;font-size:12px;font-weight:500}.mat-option{font-family:NunitoSans,sans-serif;font-size:16px}.mat-optgroup-label{font:500 14px/24px NunitoSans,sans-serif}.mat-simple-snackbar{font-family:NunitoSans,sans-serif;font-size:14px}.mat-simple-snackbar-action{line-height:1;font-family:inherit;font-size:inherit;font-weight:500}.mat-tree{font-family:NunitoSans,sans-serif}.mat-nested-tree-node,.mat-tree-node{font-weight:400;font-size:14px}.mat-ripple{overflow:hidden;position:relative}.mat-ripple.mat-ripple-unbounded{overflow:visible}.mat-ripple-element{position:absolute;border-radius:50%;pointer-events:none;-webkit-transition:opacity,-webkit-transform cubic-bezier(0,0,.2,1);transition:opacity,transform cubic-bezier(0,0,.2,1),-webkit-transform cubic-bezier(0,0,.2,1);-webkit-transform:scale(0);transform:scale(0)}@media (-ms-high-contrast:active){.mat-ripple-element{display:none}}.cdk-visually-hidden{border:0;clip:rect(0 0 0 0);height:1px;margin:-1px;overflow:hidden;padding:0;position:absolute;width:1px;outline:0;-webkit-appearance:none;-moz-appearance:none}.cdk-global-overlay-wrapper,.cdk-overlay-container{pointer-events:none;top:0;left:0;height:100%;width:100%}.cdk-overlay-container{position:fixed;z-index:1000}.cdk-overlay-container:empty{display:none}.cdk-global-overlay-wrapper{display:-webkit-box;display:flex;position:absolute;z-index:1000}.cdk-overlay-pane{position:absolute;pointer-events:auto;box-sizing:border-box;z-index:1000;display:-webkit-box;display:flex;max-width:100%;max-height:100%}.cdk-overlay-backdrop{position:absolute;top:0;bottom:0;left:0;right:0;z-index:1000;pointer-events:auto;-webkit-tap-highlight-color:transparent;-webkit-transition:opacity .4s cubic-bezier(.25,.8,.25,1);transition:opacity .4s cubic-bezier(.25,.8,.25,1);opacity:0}.cdk-overlay-backdrop.cdk-overlay-backdrop-showing{opacity:1}@media screen and (-ms-high-contrast:active){.cdk-overlay-backdrop.cdk-overlay-backdrop-showing{opacity:.6}}.cdk-overlay-dark-backdrop{background:rgba(0,0,0,.32)}.cdk-overlay-transparent-backdrop,.cdk-overlay-transparent-backdrop.cdk-overlay-backdrop-showing{opacity:0}.cdk-overlay-connected-position-bounding-box{position:absolute;z-index:1000;display:-webkit-box;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;flex-direction:column;min-width:1px;min-height:1px}.cdk-global-scrollblock{position:fixed;width:100%;overflow-y:scroll}@-webkit-keyframes cdk-text-field-autofill-start{/*!*/}@keyframes cdk-text-field-autofill-start{/*!*/}@-webkit-keyframes cdk-text-field-autofill-end{/*!*/}@keyframes cdk-text-field-autofill-end{/*!*/}.cdk-text-field-autofill-monitored:-webkit-autofill{-webkit-animation-name:cdk-text-field-autofill-start;animation-name:cdk-text-field-autofill-start}.cdk-text-field-autofill-monitored:not(:-webkit-autofill){-webkit-animation-name:cdk-text-field-autofill-end;animation-name:cdk-text-field-autofill-end}textarea.cdk-textarea-autosize{resize:none}textarea.cdk-textarea-autosize-measuring{height:auto!important;overflow:hidden!important;padding:2px 0!important;box-sizing:content-box!important}.modal-content{display:-webkit-box;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;flex-direction:column;align-content:center;font-family:NunitoSans,sans-serif;margin:20px 10px 10px}.modal-detail{display:-webkit-box;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;flex-direction:column;-webkit-box-align:center;align-items:center;text-align:center}.button-container{display:-webkit-box;display:flex;justify-content:space-around;margin:10px 10px 20px}.btn{width:120px}.alert-fields{margin:10px 0}.info{color:#64b5f6;font-size:120px}.success{color:#48abd6;font-size:120px}"]
            }] }
];
/** @nocollapse */
SuccessModalComponent.ctorParameters = () => [
    { type: MatDialogRef },
    { type: undefined, decorators: [{ type: Inject, args: [MAT_DIALOG_DATA,] }] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class ConfirmationModalComponent {
    /**
     * @param {?} dialogRef
     * @param {?} data
     */
    constructor(dialogRef, data) {
        this.dialogRef = dialogRef;
        this.data = data;
        this.submitEmitter = new EventEmitter();
        this.response = {};
    }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
    /**
     * @return {?}
     */
    onSubmit() {
        this.response['response'] = true;
        this.submitEmitter.emit(this.response);
    }
    /**
     * @return {?}
     */
    onCancel() {
        this.response['response'] = false;
        this.submitEmitter.emit(this.response);
    }
}
ConfirmationModalComponent.decorators = [
    { type: Component, args: [{
                selector: 'app-confirmation-modal',
                template: "<div mat-dialog-content class=\"modal-content\">\n  <div *ngIf=\"data['action'] === 'confirm'\"\n       class=\"modal-detail\">\n    <i class=\"material-icons warning\">\n      warning\n    </i>\n    <h2 class=\"alert-fields main-alert\">{{data.message}}</h2>\n  </div>\n\n  <div *ngIf=\"data['action'] === 'verify-save-permissions'\"\n       class=\"modal-detail\">\n    <i class=\"material-icons warning\">\n      warning\n    </i>\n    <h2 class=\"alert-fields main-alert\">Are you sure?</h2>\n    <div class=\"alert-fields\"\n         *ngIf=\"data['action'] === 'verify-save-permissions'\">Permission modifications will take place immediately!</div>\n  </div>\n</div>\n<div mat-dialog-actions class=\"button-container\">\n  <button class=\"btn\"\n          [mat-dialog-close]\n          mat-flat-button\n          color=\"primary\"\n          (click)=\"onSubmit()\">OK</button>\n  <button class=\"btn\"\n          [mat-dialog-close]\n          mat-flat-button\n          color=\"primary\"\n          (click)=\"onCancel()\">Cancel</button>\n</div>\n\n\n",
                styles: ["@media print{.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-appearance-legacy.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-1.28122em) scale(.75);transform:translateY(-1.28122em) scale(.75)}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-form-field-autofill-control:-webkit-autofill+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.28121em) scale(.75);transform:translateY(-1.28121em) scale(.75)}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.2812em) scale(.75);transform:translateY(-1.2812em) scale(.75)}}.mat-badge-content{font-weight:600;font-size:12px;font-family:NunitoSans,sans-serif}.mat-badge-small .mat-badge-content{font-size:9px}.mat-badge-large .mat-badge-content{font-size:24px}.mat-h1,.mat-headline,.mat-typography h1{font:400 24px/32px NunitoSans,sans-serif;margin:0 0 16px}.mat-h2,.mat-title,.mat-typography h2{font:500 20px/32px NunitoSans,sans-serif;margin:0 0 16px}.mat-h3,.mat-subheading-2,.mat-typography h3{font:400 16px/28px NunitoSans,sans-serif;margin:0 0 16px}.mat-h4,.mat-subheading-1,.mat-typography h4{font:400 15px/24px NunitoSans,sans-serif;margin:0 0 16px}.mat-h5,.mat-h6,.mat-typography h5,.mat-typography h6{margin:0 0 12px}.mat-body-2,.mat-body-strong{font:500 14px/24px NunitoSans,sans-serif}.mat-body,.mat-body-1,.mat-typography{font:400 14px/20px NunitoSans,sans-serif}.mat-body p,.mat-body-1 p,.mat-typography p{margin:0 0 12px}.mat-caption,.mat-small{font:400 12px/20px NunitoSans,sans-serif}.mat-display-4,.mat-typography .mat-display-4{font:300 112px/112px NunitoSans,sans-serif;letter-spacing:-.05em;margin:0 0 56px}.mat-display-3,.mat-typography .mat-display-3{font:400 56px/56px NunitoSans,sans-serif;letter-spacing:-.02em;margin:0 0 64px}.mat-display-2,.mat-typography .mat-display-2{font:400 45px/48px NunitoSans,sans-serif;letter-spacing:-.005em;margin:0 0 64px}.mat-display-1,.mat-typography .mat-display-1{font:400 34px/40px NunitoSans,sans-serif;margin:0 0 64px}.mat-bottom-sheet-container{font:400 14px/20px NunitoSans,sans-serif}.mat-button,.mat-fab,.mat-flat-button,.mat-icon-button,.mat-mini-fab,.mat-raised-button,.mat-stroked-button{font-family:NunitoSans,sans-serif;font-size:14px;font-weight:500}.mat-button-toggle,.mat-card{font-family:NunitoSans,sans-serif}.mat-card-title{font-size:24px;font-weight:500}.mat-card-header .mat-card-title{font-size:20px}.mat-card-content,.mat-card-subtitle{font-size:14px}.mat-checkbox{font-family:NunitoSans,sans-serif}.mat-checkbox-layout .mat-checkbox-label{line-height:24px}.mat-chip{font-size:14px;font-weight:500}.mat-chip .mat-chip-remove.mat-icon,.mat-chip .mat-chip-trailing-icon.mat-icon{font-size:18px}.mat-table{font-family:NunitoSans,sans-serif}.mat-header-cell{font-size:12px;font-weight:500}.mat-cell,.mat-footer-cell{font-size:14px}.mat-calendar{font-family:NunitoSans,sans-serif}.mat-calendar-body{font-size:13px}.mat-calendar-body-label,.mat-calendar-period-button{font-size:14px;font-weight:500}.mat-calendar-table-header th{font-size:11px;font-weight:400}.mat-dialog-title{font:500 20px/32px NunitoSans,sans-serif}.mat-expansion-panel-header{font-family:NunitoSans,sans-serif;font-size:15px;font-weight:400}.mat-expansion-panel-content{font:400 14px/20px NunitoSans,sans-serif}.mat-form-field{font-size:inherit;font-weight:400;line-height:1.125;font-family:NunitoSans,sans-serif}.mat-form-field-wrapper{padding-bottom:1.34375em}.mat-form-field-prefix .mat-icon,.mat-form-field-suffix .mat-icon{font-size:150%;line-height:1.125}.mat-form-field-prefix .mat-icon-button,.mat-form-field-suffix .mat-icon-button{height:1.5em;width:1.5em}.mat-form-field-prefix .mat-icon-button .mat-icon,.mat-form-field-suffix .mat-icon-button .mat-icon{height:1.125em;line-height:1.125}.mat-form-field-infix{padding:.5em 0;border-top:.84375em solid transparent}.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-1.34373em) scale(.75);transform:translateY(-1.34373em) scale(.75);width:133.3333533333%}.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.34372em) scale(.75);transform:translateY(-1.34372em) scale(.75);width:133.3333633333%}.mat-form-field-label-wrapper{top:-.84375em;padding-top:.84375em}.mat-form-field-label{top:1.34375em}.mat-form-field-underline{bottom:1.34375em}.mat-form-field-subscript-wrapper{font-size:75%;margin-top:.6666666667em;top:calc(100% - 1.7916666667em)}.mat-form-field-appearance-legacy .mat-form-field-wrapper{padding-bottom:1.25em}.mat-form-field-appearance-legacy .mat-form-field-infix{padding:.4375em 0}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-appearance-legacy.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00106px);transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00106px);-ms-transform:translateY(-1.28119em) scale(.75);width:133.3333933333%}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-form-field-autofill-control:-webkit-autofill+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00107px);transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00107px);-ms-transform:translateY(-1.28118em) scale(.75);width:133.3334033333%}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00108px);transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00108px);-ms-transform:translateY(-1.28117em) scale(.75);width:133.3334133333%}.mat-form-field-appearance-legacy .mat-form-field-label{top:1.28125em}.mat-form-field-appearance-legacy .mat-form-field-underline{bottom:1.25em}.mat-form-field-appearance-legacy .mat-form-field-subscript-wrapper{margin-top:.5416666667em;top:calc(100% - 1.6666666667em)}@media print{.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-appearance-legacy.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-1.28116em) scale(.75);transform:translateY(-1.28116em) scale(.75)}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-form-field-autofill-control:-webkit-autofill+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.28115em) scale(.75);transform:translateY(-1.28115em) scale(.75)}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.28114em) scale(.75);transform:translateY(-1.28114em) scale(.75)}}.mat-form-field-appearance-fill .mat-form-field-infix{padding:.25em 0 .75em}.mat-form-field-appearance-fill .mat-form-field-label{top:1.09375em;margin-top:-.5em}.mat-form-field-appearance-fill.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-appearance-fill.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-.59373em) scale(.75);transform:translateY(-.59373em) scale(.75);width:133.3333533333%}.mat-form-field-appearance-fill.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-.59372em) scale(.75);transform:translateY(-.59372em) scale(.75);width:133.3333633333%}.mat-form-field-appearance-outline .mat-form-field-infix{padding:1em 0}.mat-form-field-appearance-outline .mat-form-field-label{top:1.84375em;margin-top:-.25em}.mat-form-field-appearance-outline.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-appearance-outline.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-1.59373em) scale(.75);transform:translateY(-1.59373em) scale(.75);width:133.3333533333%}.mat-form-field-appearance-outline.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.59372em) scale(.75);transform:translateY(-1.59372em) scale(.75);width:133.3333633333%}.mat-grid-tile-footer,.mat-grid-tile-header{font-size:14px}.mat-grid-tile-footer .mat-line,.mat-grid-tile-header .mat-line{white-space:nowrap;overflow:hidden;text-overflow:ellipsis;display:block;box-sizing:border-box}.mat-grid-tile-footer .mat-line:nth-child(n+2),.mat-grid-tile-header .mat-line:nth-child(n+2){font-size:12px}input.mat-input-element{margin-top:-.0625em}.mat-menu-item{font-family:NunitoSans,sans-serif;font-size:14px;font-weight:400}.mat-paginator,.mat-paginator-page-size .mat-select-trigger{font-family:NunitoSans,sans-serif;font-size:12px}.mat-radio-button,.mat-select{font-family:NunitoSans,sans-serif}.mat-select-trigger{height:1.125em}.mat-slide-toggle-content{font-family:NunitoSans,sans-serif}.mat-slider-thumb-label-text{font-family:NunitoSans,sans-serif;font-size:12px;font-weight:500}.mat-stepper-horizontal,.mat-stepper-vertical{font-family:NunitoSans,sans-serif}.mat-step-label{font-size:14px;font-weight:400}.mat-step-sub-label-error{font-weight:400}.mat-step-label-error{font-size:14px}.mat-step-label-selected{font-size:14px;font-weight:500}.mat-tab-group{font-family:NunitoSans,sans-serif}.mat-tab-label,.mat-tab-link{font-family:NunitoSans,sans-serif;font-size:14px;font-weight:500}.mat-toolbar,.mat-toolbar h1,.mat-toolbar h2,.mat-toolbar h3,.mat-toolbar h4,.mat-toolbar h5,.mat-toolbar h6{font:500 20px/32px NunitoSans,sans-serif;margin:0}.mat-tooltip{font-family:NunitoSans,sans-serif;font-size:10px;padding-top:6px;padding-bottom:6px}.mat-tooltip-handset{font-size:14px;padding-top:8px;padding-bottom:8px}.mat-list-item,.mat-list-option{font-family:NunitoSans,sans-serif}.mat-list-base .mat-list-item{font-size:16px}.mat-list-base .mat-list-item .mat-line{white-space:nowrap;overflow:hidden;text-overflow:ellipsis;display:block;box-sizing:border-box}.mat-list-base .mat-list-item .mat-line:nth-child(n+2){font-size:14px}.mat-list-base .mat-list-option{font-size:16px}.mat-list-base .mat-list-option .mat-line{white-space:nowrap;overflow:hidden;text-overflow:ellipsis;display:block;box-sizing:border-box}.mat-list-base .mat-list-option .mat-line:nth-child(n+2){font-size:14px}.mat-list-base .mat-subheader{font-family:NunitoSans,sans-serif;font-size:14px;font-weight:500}.mat-list-base[dense] .mat-list-item{font-size:12px}.mat-list-base[dense] .mat-list-item .mat-line{white-space:nowrap;overflow:hidden;text-overflow:ellipsis;display:block;box-sizing:border-box}.mat-list-base[dense] .mat-list-item .mat-line:nth-child(n+2),.mat-list-base[dense] .mat-list-option{font-size:12px}.mat-list-base[dense] .mat-list-option .mat-line{white-space:nowrap;overflow:hidden;text-overflow:ellipsis;display:block;box-sizing:border-box}.mat-list-base[dense] .mat-list-option .mat-line:nth-child(n+2){font-size:12px}.mat-list-base[dense] .mat-subheader{font-family:NunitoSans,sans-serif;font-size:12px;font-weight:500}.mat-option{font-family:NunitoSans,sans-serif;font-size:16px}.mat-optgroup-label{font:500 14px/24px NunitoSans,sans-serif}.mat-simple-snackbar{font-family:NunitoSans,sans-serif;font-size:14px}.mat-simple-snackbar-action{line-height:1;font-family:inherit;font-size:inherit;font-weight:500}.mat-tree{font-family:NunitoSans,sans-serif}.mat-nested-tree-node,.mat-tree-node{font-weight:400;font-size:14px}.mat-ripple{overflow:hidden;position:relative}.mat-ripple.mat-ripple-unbounded{overflow:visible}.mat-ripple-element{position:absolute;border-radius:50%;pointer-events:none;-webkit-transition:opacity,-webkit-transform cubic-bezier(0,0,.2,1);transition:opacity,transform cubic-bezier(0,0,.2,1),-webkit-transform cubic-bezier(0,0,.2,1);-webkit-transform:scale(0);transform:scale(0)}@media (-ms-high-contrast:active){.mat-ripple-element{display:none}}.cdk-visually-hidden{border:0;clip:rect(0 0 0 0);height:1px;margin:-1px;overflow:hidden;padding:0;position:absolute;width:1px;outline:0;-webkit-appearance:none;-moz-appearance:none}.cdk-global-overlay-wrapper,.cdk-overlay-container{pointer-events:none;top:0;left:0;height:100%;width:100%}.cdk-overlay-container{position:fixed;z-index:1000}.cdk-overlay-container:empty{display:none}.cdk-global-overlay-wrapper{display:-webkit-box;display:flex;position:absolute;z-index:1000}.cdk-overlay-pane{position:absolute;pointer-events:auto;box-sizing:border-box;z-index:1000;display:-webkit-box;display:flex;max-width:100%;max-height:100%}.cdk-overlay-backdrop{position:absolute;top:0;bottom:0;left:0;right:0;z-index:1000;pointer-events:auto;-webkit-tap-highlight-color:transparent;-webkit-transition:opacity .4s cubic-bezier(.25,.8,.25,1);transition:opacity .4s cubic-bezier(.25,.8,.25,1);opacity:0}.cdk-overlay-backdrop.cdk-overlay-backdrop-showing{opacity:1}@media screen and (-ms-high-contrast:active){.cdk-overlay-backdrop.cdk-overlay-backdrop-showing{opacity:.6}}.cdk-overlay-dark-backdrop{background:rgba(0,0,0,.32)}.cdk-overlay-transparent-backdrop,.cdk-overlay-transparent-backdrop.cdk-overlay-backdrop-showing{opacity:0}.cdk-overlay-connected-position-bounding-box{position:absolute;z-index:1000;display:-webkit-box;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;flex-direction:column;min-width:1px;min-height:1px}.cdk-global-scrollblock{position:fixed;width:100%;overflow-y:scroll}@-webkit-keyframes cdk-text-field-autofill-start{/*!*/}@keyframes cdk-text-field-autofill-start{/*!*/}@-webkit-keyframes cdk-text-field-autofill-end{/*!*/}@keyframes cdk-text-field-autofill-end{/*!*/}.cdk-text-field-autofill-monitored:-webkit-autofill{-webkit-animation-name:cdk-text-field-autofill-start;animation-name:cdk-text-field-autofill-start}.cdk-text-field-autofill-monitored:not(:-webkit-autofill){-webkit-animation-name:cdk-text-field-autofill-end;animation-name:cdk-text-field-autofill-end}textarea.cdk-textarea-autosize{resize:none}textarea.cdk-textarea-autosize-measuring{height:auto!important;overflow:hidden!important;padding:2px 0!important;box-sizing:content-box!important}.modal-content{display:-webkit-box;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;flex-direction:column;align-content:center;font-family:NunitoSans,sans-serif;margin:10px}.modal-detail{display:-webkit-box;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;flex-direction:column;-webkit-box-align:center;align-items:center;margin:20px;text-align:center}.button-container{display:-webkit-box;display:flex;justify-content:space-around;margin:10px 10px 20px}.btn{width:100px}.warning{color:#f99;font-size:120px}.info{color:#64b5f6;font-size:120px}"]
            }] }
];
/** @nocollapse */
ConfirmationModalComponent.ctorParameters = () => [
    { type: MatDialogRef },
    { type: undefined, decorators: [{ type: Inject, args: [MAT_DIALOG_DATA,] }] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class CompanyInfoComponent {
    /**
     * @param {?} companyService
     * @param {?} router
     * @param {?} _fb
     * @param {?} dialog
     * @param {?} editCoordinationService
     */
    constructor(companyService, router, _fb, dialog, editCoordinationService) {
        this.companyService = companyService;
        this.router = router;
        this._fb = _fb;
        this.dialog = dialog;
        this.editCoordinationService = editCoordinationService;
        this.loading = true;
        this.distanceTypes = [];
        this.distanceObject = {};
        this.formValidatorFloat = '[^A-z ]*';
        this.formValidatorInteger = '[^A-z \.]*';
        // triggers
        this.companyIsEditable = false;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.distanceTypes = distanceArray;
        this.distanceObject = distanceObject;
        this.makeCompanyForm();
    }
    /**
     * @return {?}
     */
    ngOnChanges() {
        this.makeCompanyForm();
    }
    /**
     * @param {?} unit
     * @return {?}
     */
    translateDistance(unit) {
        return translateDistance(unit, true);
    }
    /**
     * @param {?} sectionName
     * @return {?}
     */
    toggleEdit(sectionName) {
        this.enableCompanyForm();
        this.companyIsEditable = true;
        this.sendEditStatus('infoCompany', true);
    }
    /**
     * @param {?} sectionName
     * @return {?}
     */
    cancelEdit(sectionName) {
        this.makeCompanyForm();
        this.companyIsEditable = false;
        this.sendEditStatus('infoCompany', false);
    }
    /**
     * @param {?} formStatus
     * @return {?}
     */
    checkTooltip(formStatus) {
        if (formStatus) {
            return 'Save';
        }
        else {
            return 'Cannot save - certain fields are invalid.';
        }
    }
    /**
     * @param {?} sectionName
     * @return {?}
     */
    saveEdit(sectionName) {
        this.onSubmitCompany();
        this.companyIsEditable = false;
        this.sendEditStatus('infoCompany', false);
    }
    /**
     * @return {?}
     */
    makeCompanyForm() {
        this.companyForm = this._fb.group({
            name: [{ value: this.company['name'], disabled: true }, [Validators.required]],
            sigmaLevel: [{ value: this.company['sigmaLevel'], disabled: true },
                [Validators.required, Validators.min(0.0001), Validators.max(3), Validators.pattern(this.formValidatorFloat)]]
        });
        this.loading = false;
    }
    /**
     * @return {?}
     */
    enableCompanyForm() {
        this.companyForm.controls.name.enable();
        this.companyForm.controls.sigmaLevel.enable();
    }
    /**
     * @return {?}
     */
    onSubmitCompany() {
        /** @type {?} */
        const newObj = {};
        newObj['message'] = `You are about to update Company information. Do you wish to continue?`;
        newObj['action'] = 'confirm';
        /** @type {?} */
        const dialogRef = this.dialog.open(ConfirmationModalComponent, {
            width: '600px',
            data: newObj,
            disableClose: true,
        });
        dialogRef.componentInstance.submitEmitter.subscribe((/**
         * @param {?} data
         * @return {?}
         */
        (data) => {
            if (!data.response) {
                this.makeCompanyForm();
                return;
            }
            else {
                this.sendInformation();
            }
        }));
    }
    /**
     * @return {?}
     */
    sendInformation() {
        this.loading = true;
        this.companyForm.controls.name.disable();
        this.companyForm.controls.sigmaLevel.disable();
        /** @type {?} */
        const companyClone = Object.assign({}, this.company);
        companyClone['name'] = this.companyForm.value['name'];
        companyClone['sigmaLevel'] = this.companyForm.value['sigmaLevel'];
        this.companyService.postCompanyUpdate(companyClone).subscribe((/**
         * @param {?} res
         * @return {?}
         */
        res => {
            if (res === true) {
                this.companyService.getOne(this.company['id']).subscribe((/**
                 * @return {?}
                 */
                () => {
                    /** @type {?} */
                    const newObj = {};
                    newObj['successMessage'] = 'Company information updated.';
                    newObj['icon'] = 'check_circle';
                    newObj['action'] = 'success';
                    this.dialog.open(SuccessModalComponent, {
                        width: '600px',
                        data: newObj
                    });
                }));
            }
            else {
                /** @type {?} */
                const newObj = {};
                newObj['errMessage'] = 'Unable to update company.';
                newObj['action'] = 'error';
                this.dialog.open(ErrorModalComponent, {
                    width: '600px',
                    data: newObj
                });
                this.makeCompanyForm();
            }
        }), (/**
         * @param {?} error
         * @return {?}
         */
        error => {
            /** @type {?} */
            const newObj = {};
            newObj['errMessage'] = 'Unable to update company.';
            newObj['action'] = 'error';
            this.dialog.open(ErrorModalComponent, {
                width: '600px',
                data: newObj
            });
        }));
    }
    /**
     * @return {?}
     */
    checkEditButtonRenderStatus() {
        return !this.companyIsEditable && this.companyForm && !this.loading && this.editCoordinationService.areEditFieldsClosed();
    }
    /**
     * @param {?} field
     * @param {?} status
     * @return {?}
     */
    sendEditStatus(field, status) {
        this.editCoordinationService.toggleField(field, status);
    }
}
CompanyInfoComponent.decorators = [
    { type: Component, args: [{
                selector: 'app-company-info',
                template: "<mat-card class=\"form-card\">\n  <mat-card-header class=\"form-card-header\">\n    <mat-card-title class=\"title-type\">Company</mat-card-title>\n    <span class=\"edit-container\">\n      <mat-icon *ngIf=\"checkEditButtonRenderStatus()\" name=\"companyEditIcon\" matTooltip=\"Edit\" (click)=\"toggleEdit('company')\" class=\"edit-button-icon\">edit</mat-icon>\n      <mat-icon *ngIf=\"companyIsEditable\" matTooltip=\"cancel\" name=\"companyCancelIcon\" (click)=\"cancelEdit('company')\" class=\"edit-button-icon\">cancel</mat-icon>\n      <mat-icon *ngIf=\"companyIsEditable\" name=\"companySaveIcon\" [matTooltip]=\"this.checkTooltip(this.companyForm.valid)\"\n        (click)=\"this.companyForm.valid ? saveEdit('company') : ''\"\n        [ngClass]=\"this.companyForm.valid ? 'edit-button-icon' : 'invalid-save-icon'\">check_circle</mat-icon>\n    </span>\n  </mat-card-header>\n  <mat-spinner *ngIf=\"loading\" [diameter]=\"35\"></mat-spinner>\n  <mat-card-content class=\"form-card-content\" *ngIf=\"this.companyForm && !this.loading\">\n    <form class=\"horizontal-form-container\" (ngSubmit)=\"onSubmitCompany()\" name=\"companyForm\" [formGroup]=\"companyForm\">\n      <mat-form-field>\n        <input matInput class=\"general-form-field\" id=\"companyInfoName\" placeholder=\"Name\" formControlName=\"name\">\n        <mat-error *ngIf=\"this.companyForm.get('name').hasError('required')\">Company name cannot be blank.</mat-error>\n      </mat-form-field>\n      <mat-form-field>\n        <input matInput type=\"number\" class=\"general-form-field\" id=\"companyInfoSigmaLevel\" placeholder=\"Sigma Level\" formControlName=\"sigmaLevel\">\n        <mat-error *ngIf=\"this.companyForm.get('sigmaLevel').hasError('required')\">Sigma Level must be a number.</mat-error>\n        <mat-error *ngIf=\"this.companyForm.get('sigmaLevel').hasError('pattern')\">Sigma Level must be a number.</mat-error>\n        <mat-error *ngIf=\"this.companyForm.get('sigmaLevel').hasError('max')\">Sigma Level must be 3 or smaller.</mat-error>\n        <mat-error *ngIf=\"this.companyForm.get('sigmaLevel').hasError('min')\">Sigma Level must be greater than 0.</mat-error>\n      </mat-form-field>\n    </form>\n    <mat-card-actions align=\"end\">\n      <button id=\"alert-modal-ok-btn\"\n              color=\"primary\" mat-flat-button *ngIf=\"parentApp === 'Admin'\">\n        <mat-icon matTooltip=\"Select\" class=\"add-icon\" name=\"addField\">add_circle</mat-icon>\n        Add Field\n      </button>\n    </mat-card-actions>\n  </mat-card-content>\n</mat-card>\n",
                styles: ["@media print{.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-appearance-legacy.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-1.28122em) scale(.75);transform:translateY(-1.28122em) scale(.75)}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-form-field-autofill-control:-webkit-autofill+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.28121em) scale(.75);transform:translateY(-1.28121em) scale(.75)}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.2812em) scale(.75);transform:translateY(-1.2812em) scale(.75)}}.mat-badge-content{font-weight:600;font-size:12px;font-family:NunitoSans,sans-serif}.mat-badge-small .mat-badge-content{font-size:9px}.mat-badge-large .mat-badge-content{font-size:24px}.mat-h1,.mat-headline,.mat-typography h1{font:400 24px/32px NunitoSans,sans-serif;margin:0 0 16px}.mat-h2,.mat-title,.mat-typography h2{font:500 20px/32px NunitoSans,sans-serif;margin:0 0 16px}.mat-h3,.mat-subheading-2,.mat-typography h3{font:400 16px/28px NunitoSans,sans-serif;margin:0 0 16px}.mat-h4,.mat-subheading-1,.mat-typography h4{font:400 15px/24px NunitoSans,sans-serif;margin:0 0 16px}.mat-h5,.mat-h6,.mat-typography h5,.mat-typography h6{margin:0 0 12px}.mat-body-2,.mat-body-strong{font:500 14px/24px NunitoSans,sans-serif}.mat-body,.mat-body-1,.mat-typography{font:400 14px/20px NunitoSans,sans-serif}.mat-body p,.mat-body-1 p,.mat-typography p{margin:0 0 12px}.mat-caption,.mat-small{font:400 12px/20px NunitoSans,sans-serif}.mat-display-4,.mat-typography .mat-display-4{font:300 112px/112px NunitoSans,sans-serif;letter-spacing:-.05em;margin:0 0 56px}.mat-display-3,.mat-typography .mat-display-3{font:400 56px/56px NunitoSans,sans-serif;letter-spacing:-.02em;margin:0 0 64px}.mat-display-2,.mat-typography .mat-display-2{font:400 45px/48px NunitoSans,sans-serif;letter-spacing:-.005em;margin:0 0 64px}.mat-display-1,.mat-typography .mat-display-1{font:400 34px/40px NunitoSans,sans-serif;margin:0 0 64px}.mat-bottom-sheet-container{font:400 14px/20px NunitoSans,sans-serif}.mat-button,.mat-fab,.mat-flat-button,.mat-icon-button,.mat-mini-fab,.mat-raised-button,.mat-stroked-button{font-family:NunitoSans,sans-serif;font-size:14px;font-weight:500}.mat-button-toggle,.mat-card{font-family:NunitoSans,sans-serif}.mat-card-title{font-size:24px;font-weight:500}.mat-card-header .mat-card-title{font-size:20px}.mat-card-content,.mat-card-subtitle{font-size:14px}.mat-checkbox{font-family:NunitoSans,sans-serif}.mat-checkbox-layout .mat-checkbox-label{line-height:24px}.mat-chip{font-size:14px;font-weight:500}.mat-chip .mat-chip-remove.mat-icon,.mat-chip .mat-chip-trailing-icon.mat-icon{font-size:18px}.mat-table{font-family:NunitoSans,sans-serif}.mat-header-cell{font-size:12px;font-weight:500}.mat-cell,.mat-footer-cell{font-size:14px}.mat-calendar{font-family:NunitoSans,sans-serif}.mat-calendar-body{font-size:13px}.mat-calendar-body-label,.mat-calendar-period-button{font-size:14px;font-weight:500}.mat-calendar-table-header th{font-size:11px;font-weight:400}.mat-dialog-title{font:500 20px/32px NunitoSans,sans-serif}.mat-expansion-panel-header{font-family:NunitoSans,sans-serif;font-size:15px;font-weight:400}.mat-expansion-panel-content{font:400 14px/20px NunitoSans,sans-serif}.mat-form-field{font-size:inherit;font-weight:400;line-height:1.125;font-family:NunitoSans,sans-serif}.mat-form-field-wrapper{padding-bottom:1.34375em}.mat-form-field-prefix .mat-icon,.mat-form-field-suffix .mat-icon{font-size:150%;line-height:1.125}.mat-form-field-prefix .mat-icon-button,.mat-form-field-suffix .mat-icon-button{height:1.5em;width:1.5em}.mat-form-field-prefix .mat-icon-button .mat-icon,.mat-form-field-suffix .mat-icon-button .mat-icon{height:1.125em;line-height:1.125}.mat-form-field-infix{padding:.5em 0;border-top:.84375em solid transparent}.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-1.34373em) scale(.75);transform:translateY(-1.34373em) scale(.75);width:133.3333533333%}.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.34372em) scale(.75);transform:translateY(-1.34372em) scale(.75);width:133.3333633333%}.mat-form-field-label-wrapper{top:-.84375em;padding-top:.84375em}.mat-form-field-label{top:1.34375em}.mat-form-field-underline{bottom:1.34375em}.mat-form-field-subscript-wrapper{font-size:75%;margin-top:.6666666667em;top:calc(100% - 1.7916666667em)}.mat-form-field-appearance-legacy .mat-form-field-wrapper{padding-bottom:1.25em}.mat-form-field-appearance-legacy .mat-form-field-infix{padding:.4375em 0}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-appearance-legacy.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00106px);transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00106px);-ms-transform:translateY(-1.28119em) scale(.75);width:133.3333933333%}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-form-field-autofill-control:-webkit-autofill+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00107px);transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00107px);-ms-transform:translateY(-1.28118em) scale(.75);width:133.3334033333%}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00108px);transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00108px);-ms-transform:translateY(-1.28117em) scale(.75);width:133.3334133333%}.mat-form-field-appearance-legacy .mat-form-field-label{top:1.28125em}.mat-form-field-appearance-legacy .mat-form-field-underline{bottom:1.25em}.mat-form-field-appearance-legacy .mat-form-field-subscript-wrapper{margin-top:.5416666667em;top:calc(100% - 1.6666666667em)}@media print{.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-appearance-legacy.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-1.28116em) scale(.75);transform:translateY(-1.28116em) scale(.75)}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-form-field-autofill-control:-webkit-autofill+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.28115em) scale(.75);transform:translateY(-1.28115em) scale(.75)}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.28114em) scale(.75);transform:translateY(-1.28114em) scale(.75)}}.mat-form-field-appearance-fill .mat-form-field-infix{padding:.25em 0 .75em}.mat-form-field-appearance-fill .mat-form-field-label{top:1.09375em;margin-top:-.5em}.mat-form-field-appearance-fill.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-appearance-fill.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-.59373em) scale(.75);transform:translateY(-.59373em) scale(.75);width:133.3333533333%}.mat-form-field-appearance-fill.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-.59372em) scale(.75);transform:translateY(-.59372em) scale(.75);width:133.3333633333%}.mat-form-field-appearance-outline .mat-form-field-infix{padding:1em 0}.mat-form-field-appearance-outline .mat-form-field-label{top:1.84375em;margin-top:-.25em}.mat-form-field-appearance-outline.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-appearance-outline.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-1.59373em) scale(.75);transform:translateY(-1.59373em) scale(.75);width:133.3333533333%}.mat-form-field-appearance-outline.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.59372em) scale(.75);transform:translateY(-1.59372em) scale(.75);width:133.3333633333%}.mat-grid-tile-footer,.mat-grid-tile-header{font-size:14px}.mat-grid-tile-footer .mat-line,.mat-grid-tile-header .mat-line{white-space:nowrap;overflow:hidden;text-overflow:ellipsis;display:block;box-sizing:border-box}.mat-grid-tile-footer .mat-line:nth-child(n+2),.mat-grid-tile-header .mat-line:nth-child(n+2){font-size:12px}input.mat-input-element{margin-top:-.0625em}.mat-menu-item{font-family:NunitoSans,sans-serif;font-size:14px;font-weight:400}.mat-paginator,.mat-paginator-page-size .mat-select-trigger{font-family:NunitoSans,sans-serif;font-size:12px}.mat-radio-button,.mat-select{font-family:NunitoSans,sans-serif}.mat-select-trigger{height:1.125em}.mat-slide-toggle-content{font-family:NunitoSans,sans-serif}.mat-slider-thumb-label-text{font-family:NunitoSans,sans-serif;font-size:12px;font-weight:500}.mat-stepper-horizontal,.mat-stepper-vertical{font-family:NunitoSans,sans-serif}.mat-step-label{font-size:14px;font-weight:400}.mat-step-sub-label-error{font-weight:400}.mat-step-label-error{font-size:14px}.mat-step-label-selected{font-size:14px;font-weight:500}.mat-tab-group{font-family:NunitoSans,sans-serif}.mat-tab-label,.mat-tab-link{font-family:NunitoSans,sans-serif;font-size:14px;font-weight:500}.mat-toolbar,.mat-toolbar h1,.mat-toolbar h2,.mat-toolbar h3,.mat-toolbar h4,.mat-toolbar h5,.mat-toolbar h6{font:500 20px/32px NunitoSans,sans-serif;margin:0}.mat-tooltip{font-family:NunitoSans,sans-serif;font-size:10px;padding-top:6px;padding-bottom:6px}.mat-tooltip-handset{font-size:14px;padding-top:8px;padding-bottom:8px}.mat-list-item,.mat-list-option{font-family:NunitoSans,sans-serif}.mat-list-base .mat-list-item{font-size:16px}.mat-list-base .mat-list-item .mat-line{white-space:nowrap;overflow:hidden;text-overflow:ellipsis;display:block;box-sizing:border-box}.mat-list-base .mat-list-item .mat-line:nth-child(n+2){font-size:14px}.mat-list-base .mat-list-option{font-size:16px}.mat-list-base .mat-list-option .mat-line{white-space:nowrap;overflow:hidden;text-overflow:ellipsis;display:block;box-sizing:border-box}.mat-list-base .mat-list-option .mat-line:nth-child(n+2){font-size:14px}.mat-list-base .mat-subheader{font-family:NunitoSans,sans-serif;font-size:14px;font-weight:500}.mat-list-base[dense] .mat-list-item{font-size:12px}.mat-list-base[dense] .mat-list-item .mat-line{white-space:nowrap;overflow:hidden;text-overflow:ellipsis;display:block;box-sizing:border-box}.mat-list-base[dense] .mat-list-item .mat-line:nth-child(n+2),.mat-list-base[dense] .mat-list-option{font-size:12px}.mat-list-base[dense] .mat-list-option .mat-line{white-space:nowrap;overflow:hidden;text-overflow:ellipsis;display:block;box-sizing:border-box}.mat-list-base[dense] .mat-list-option .mat-line:nth-child(n+2){font-size:12px}.mat-list-base[dense] .mat-subheader{font-family:NunitoSans,sans-serif;font-size:12px;font-weight:500}.mat-option{font-family:NunitoSans,sans-serif;font-size:16px}.mat-optgroup-label{font:500 14px/24px NunitoSans,sans-serif}.mat-simple-snackbar{font-family:NunitoSans,sans-serif;font-size:14px}.mat-simple-snackbar-action{line-height:1;font-family:inherit;font-size:inherit;font-weight:500}.mat-tree{font-family:NunitoSans,sans-serif}.mat-nested-tree-node,.mat-tree-node{font-weight:400;font-size:14px}.mat-ripple{overflow:hidden;position:relative}.mat-ripple.mat-ripple-unbounded{overflow:visible}.mat-ripple-element{position:absolute;border-radius:50%;pointer-events:none;-webkit-transition:opacity,-webkit-transform cubic-bezier(0,0,.2,1);transition:opacity,transform cubic-bezier(0,0,.2,1),-webkit-transform cubic-bezier(0,0,.2,1);-webkit-transform:scale(0);transform:scale(0)}@media (-ms-high-contrast:active){.mat-ripple-element{display:none}}.cdk-visually-hidden{border:0;clip:rect(0 0 0 0);height:1px;margin:-1px;overflow:hidden;padding:0;position:absolute;width:1px;outline:0;-webkit-appearance:none;-moz-appearance:none}.cdk-global-overlay-wrapper,.cdk-overlay-container{pointer-events:none;top:0;left:0;height:100%;width:100%}.cdk-overlay-container{position:fixed;z-index:1000}.cdk-overlay-container:empty{display:none}.cdk-global-overlay-wrapper{display:-webkit-box;display:flex;position:absolute;z-index:1000}.cdk-overlay-pane{position:absolute;pointer-events:auto;box-sizing:border-box;z-index:1000;display:-webkit-box;display:flex;max-width:100%;max-height:100%}.cdk-overlay-backdrop{position:absolute;top:0;bottom:0;left:0;right:0;z-index:1000;pointer-events:auto;-webkit-tap-highlight-color:transparent;-webkit-transition:opacity .4s cubic-bezier(.25,.8,.25,1);transition:opacity .4s cubic-bezier(.25,.8,.25,1);opacity:0}.cdk-overlay-backdrop.cdk-overlay-backdrop-showing{opacity:1}@media screen and (-ms-high-contrast:active){.cdk-overlay-backdrop.cdk-overlay-backdrop-showing{opacity:.6}}.cdk-overlay-dark-backdrop{background:rgba(0,0,0,.32)}.cdk-overlay-transparent-backdrop,.cdk-overlay-transparent-backdrop.cdk-overlay-backdrop-showing{opacity:0}.cdk-overlay-connected-position-bounding-box{position:absolute;z-index:1000;display:-webkit-box;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;flex-direction:column;min-width:1px;min-height:1px}.cdk-global-scrollblock{position:fixed;width:100%;overflow-y:scroll}@-webkit-keyframes cdk-text-field-autofill-start{/*!*/}@keyframes cdk-text-field-autofill-start{/*!*/}@-webkit-keyframes cdk-text-field-autofill-end{/*!*/}@keyframes cdk-text-field-autofill-end{/*!*/}.cdk-text-field-autofill-monitored:-webkit-autofill{-webkit-animation-name:cdk-text-field-autofill-start;animation-name:cdk-text-field-autofill-start}.cdk-text-field-autofill-monitored:not(:-webkit-autofill){-webkit-animation-name:cdk-text-field-autofill-end;animation-name:cdk-text-field-autofill-end}textarea.cdk-textarea-autosize{resize:none}textarea.cdk-textarea-autosize-measuring{height:auto!important;overflow:hidden!important;padding:2px 0!important;box-sizing:content-box!important}mat-spinner{margin:0 auto}mat-card-actions{padding:0}.edit-container{width:60px;display:-webkit-box;display:flex;-webkit-box-orient:horizontal;-webkit-box-direction:reverse;flex-direction:row-reverse}.invalid-save-icon{color:#f99}.show-map-icon{cursor:pointer}.add-icon{color:#fff}.form-card-header{display:-webkit-box;display:flex;-webkit-box-pack:justify;justify-content:space-between}.horizontal-flex-spread{flex-wrap:wrap}.vertical-form-container{display:-webkit-box;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;flex-direction:column}.form-field-spacer{margin:0 10px 0 0}.small-form-field{width:148px}.general-form-field{margin:0 10px 0 0}.wide-form-field{min-width:250px}.wider-form-field{min-width:350px}.multiple-input-form-field{display:-webkit-box;display:flex;min-width:350px}.full-width-form-field{width:100%}.pad-table{margin:0 auto}"]
            }] }
];
/** @nocollapse */
CompanyInfoComponent.ctorParameters = () => [
    { type: CompanyService },
    { type: Router },
    { type: FormBuilder },
    { type: MatDialog },
    { type: EditCoordinationService }
];
CompanyInfoComponent.propDecorators = {
    company: [{ type: Input }],
    parentApp: [{ type: Input }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class AuthService extends DataService {
    /**
     * @param {?} http
     * @param {?} router
     */
    constructor(http, router) {
        super(http);
        this.router = router;
        this.loginPath = 'login';
        this.logoutPath = 'logout';
        this.userInfo = null;
    }
    /**
     * @param {?} username
     * @param {?} password
     * @return {?}
     */
    login(username, password) {
        // token specific header, will need to be altered for usage
        /** @type {?} */
        const hdr = {
            'Authorization': 'Basic ' + window.btoa(username + ':' + password)
        };
        return super.get(this.loginPath, hdr)
            .pipe(tap((/**
         * @param {?} res
         * @return {?}
         */
        res => {
            if (res) {
                this.userInfo = res;
            }
        })), catchError((/**
         * @return {?}
         */
        () => of(null))));
    }
    /**
     * @return {?}
     */
    logout() {
        this.userInfo = null;
        this.router.navigate(['/login']);
        return this.post(this.logoutPath, null);
    }
    /**
     * Called when a 401 status is encountered which means user is logged out
     * @return {?}
     */
    on401Error() {
        this.userInfo = null;
        this.router.navigate(['/login']);
    }
    /**
     * @return {?}
     */
    getUser() {
        // Check our cache
        if (this.userInfo) {
            return of(this.userInfo);
        }
        // Try to get it from the server
        return super.get(this.loginPath);
    }
    // TODO: Make these permissions an Enum
    // TODO: Is this list complete?
    /* Permissions from legacy saphira
         CREATE,
         READ,
         UPDATE,
         DELETE,
         MSA,
         IMPORT_CSV, //Old
         SNAPSHOT, //Import
         EXPORT_FULL, //Exporting survey set only
         EXPORT_TRAJECTORY, //Exporting trajectory only
         EXPORT_MINIMAL, //Not used yet, kept for future use (surveyset export without msa sixaxis data?)
         METADATA,
         MWD_READER,
         ATTACHMENT,
         QC_PLOTS,
         QC_RAW,
         QC_CORR,
         QC_TOL,
         QC_INCAZI_CALC,
         QC_INCAZI_CORR,
         QC_VALIDATION_ERRORS,
         AUTOMATION,
         SHIFT_NOTES,
         CHAT,
         MODIFY_PERMISSIONS,
         ACTIVITY_LOG,
         DATA_VIEW,
         COORD_SYSTEM,
         NOTIFICATIONS,
         API_SURVEY_READ,
         API_SURVEY_SUBMIT;
      */
    // TODO: move into user model
    // TODO: this isn't quite correct
    /**
     * @param {?} perm2Match
     * @return {?}
     */
    userhasPermission(perm2Match) {
        /** @type {?} */
        const hasAuth = this.userInfo['authorities'].filter((/**
         * @param {?} auth
         * @return {?}
         */
        (auth) => {
            return auth['role'] === 'ROLE_MAGVAR_SUPERADMIN' ||
                auth['role'] === 'ROLE_crowd-administrators' ||
                auth['role'] === 'ROLE_MAGVAR_DEVELOPER' ||
                auth['role'].indexOf(perm2Match) > -1;
        }));
        return hasAuth.length > 0;
    }
    // TODO: move into model
    /**
     * @return {?}
     */
    userIsCustomer() {
        /** @type {?} */
        const hasAuth = this.userInfo['authorities'].filter((/**
         * @param {?} auth
         * @return {?}
         */
        (auth) => {
            return auth['role'] === 'ROLE_Rig' || auth['role'] === 'ROLE_Operators';
        }));
        return hasAuth.length > 0;
    }
}
AuthService.decorators = [
    { type: Injectable }
];
/** @nocollapse */
AuthService.ctorParameters = () => [
    { type: HttpClient },
    { type: Router }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class PingService extends DataService {
    /**
     * @param {?} http
     */
    constructor(http) {
        super(http);
        this.pingSuffix = 'ping';
        this.pingStatus = new BehaviorSubject(true);
        /**
         * Round trip time in ms
         */
        this.tripTime = 0;
    }
    /**
     * @return {?}
     */
    start() {
        // only one ping
        if (this.ping) {
            return;
        }
        /** @type {?} */
        let startTime;
        /** @type {?} */
        const firstPing = super.get(this.pingSuffix).pipe(catchError((/**
         * @return {?}
         */
        () => {
            this.pingStatus.next(false);
            return EMPTY;
        })));
        /** @type {?} */
        const recurringPing = interval(10000).pipe(tap((/**
         * @return {?}
         */
        () => startTime = performance.now())), mergeMap((/**
         * @return {?}
         */
        () => super.get(this.pingSuffix))), catchError((/**
         * @param {?} error
         * @return {?}
         */
        error => {
            this.pingStatus.next(false);
            return throwError(error);
        })), retry() // DON'T STOP ME NOW!
        );
        this.ping = concat(firstPing, recurringPing)
            .subscribe((/**
         * @param {?} apiVersion
         * @return {?}
         */
        apiVersion => {
            this.tripTime = performance.now() - startTime;
            this.pingStatus.next(true);
            this.apiVersion = apiVersion;
        }));
    }
    /**
     * @return {?}
     */
    status() {
        return this.pingStatus.asObservable();
    }
}
PingService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
PingService.ctorParameters = () => [
    { type: HttpClient }
];
/** @nocollapse */ PingService.ngInjectableDef = ɵɵdefineInjectable({ factory: function PingService_Factory() { return new PingService(ɵɵinject(HttpClient)); }, token: PingService, providedIn: "root" });

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class ClientRequisitesService extends DataService {
    /*
      * Constructor
      */
    /**
     * @param {?} http
     */
    constructor(http) {
        super(http);
        this.clientRequisitesPath = 'required';
        this.clientRequisites = new BehaviorSubject({});
        this.getClientReqs = this.clientRequisites.asObservable();
    }
    /*
      * Get the client requisites. Only loaded once for now.
      * TODO implement some timeout on the cache so requisites are reloaded once every 10 minutes or something idk
      */
    /**
     * @param {?=} reload
     * @return {?}
     */
    getClientRequisites(reload) {
        if (!this.cachedData || reload) {
            return super
                .get(`${this.clientRequisitesPath}`).pipe(tap((/**
             * @param {?} requisites
             * @return {?}
             */
            requisites => this.cachedData = requisites)), catchError((/**
             * @param {?} err
             * @return {?}
             */
            err => of(false))));
        }
        else {
            return of(this.cachedData);
        }
    }
    /*
      * Set the client requisites
      */
    /**
     * @param {?} data
     * @return {?}
     */
    setClientRequisitesService(data) {
        this.clientRequisites.next(data);
        this.cachedData = data;
    }
    /*
      * Return the cached field
      */
    /**
     * @param {?} requestedField
     * @return {?}
     */
    returnCachedField(requestedField) {
        return this.cachedData[requestedField];
    }
    /*
      * Return all the cached data
      */
    /**
     * @return {?}
     */
    returnAllCached() {
        return of(this.cachedData);
    }
}
ClientRequisitesService.decorators = [
    { type: Injectable }
];
/** @nocollapse */
ClientRequisitesService.ctorParameters = () => [
    { type: HttpClient }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
// Service to flash titleof currect browser tab.
class TitleFlasherService {
    /**
     * @param {?} titleService
     */
    constructor(titleService) {
        this.titleService = titleService;
    }
    // Method to start flashing document title
    // Interval: interval of flash
    // String to flash in browser title
    /**
     * @param {?} interval
     * @param {?} titleToFlash
     * @return {?}
     */
    startFlashing(interval, titleToFlash) {
        if (!this.isAlreadyFlashingTitle) {
            this.prevTitle = this.titleService.getTitle();
            this.documentTitleFlash = timer(0, interval);
            this.documentTitleFlashSubscription = this.documentTitleFlash.subscribe((/**
             * @param {?} data
             * @return {?}
             */
            (data) => {
                if (data % 2 === 0) {
                    this.setTitle(titleToFlash);
                }
                else {
                    this.setTitle(this.prevTitle);
                }
            }));
            this.isAlreadyFlashingTitle = true;
        }
    }
    /**
     * @return {?}
     */
    stopFlashing() {
        if (this.isAlreadyFlashingTitle) {
            this.documentTitleFlashSubscription.unsubscribe();
            this.setTitle(this.prevTitle);
            this.isAlreadyFlashingTitle = false;
        }
    }
    /**
     * @param {?} title
     * @return {?}
     */
    setTitle(title) {
        this.titleService.setTitle(title);
    }
    /**
     * @return {?}
     */
    getTitle() {
        return this.titleService.getTitle();
    }
}
TitleFlasherService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
TitleFlasherService.ctorParameters = () => [
    { type: Title }
];
/** @nocollapse */ TitleFlasherService.ngInjectableDef = ɵɵdefineInjectable({ factory: function TitleFlasherService_Factory() { return new TitleFlasherService(ɵɵinject(Title)); }, token: TitleFlasherService, providedIn: "root" });

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class SplashComponent {
    /**
     * @param {?} auth
     * @param {?} matDialog
     * @param {?} router
     * @param {?} ping
     * @param {?} requisiteServ
     * @param {?} title
     */
    constructor(auth, matDialog, router, ping, requisiteServ, title) {
        this.auth = auth;
        this.matDialog = matDialog;
        this.router = router;
        this.ping = ping;
        this.requisiteServ = requisiteServ;
        this.title = title;
        this.loginFailed = false;
        this.connectedStatus = 'Not connected';
        this.statusClass = 'not-connected login-support-text login-connection-status';
        this.shouldPing = true;
        this.afterLoginRoute = ['/'];
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.matDialog.closeAll();
        this.title.setTitle('Saphira 3.0');
        this.ping
            .status()
            .pipe(takeWhile((/**
         * @return {?}
         */
        () => this.shouldPing)))
            .subscribe((/**
         * @param {?} connected
         * @return {?}
         */
        connected => {
            if (connected) {
                this.statusClass = 'connected login-support-text login-connection-status';
                this.connectedStatus = 'Connected';
            }
            else {
                this.statusClass = 'not-connected login-support-text login-connection-status';
                this.connectedStatus = 'Not connected';
            }
        }));
    }
    /**
     * @param {?} passwordLength
     * @return {?}
     */
    inputChanged(passwordLength) {
        if (passwordLength.length) {
            this.loginFailed = false;
        }
    }
    /**
     * @return {?}
     */
    sendEmail() {
        location.href = 'mailto:surveys@magvar.com';
    }
    /**
     * @return {?}
     */
    login() {
        this.auth
            .login(this.username, this.password)
            .subscribe((/**
         * @param {?} res
         * @return {?}
         */
        res => {
            if (res) {
                this.router.navigate(this.afterLoginRoute);
            }
            else {
                this.password = null;
                this.loginFailed = true;
            }
        }), (/**
         * @param {?} err
         * @return {?}
         */
        err => {
            this.password = null;
            this.loginFailed = true;
        }));
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        this.shouldPing = false;
    }
}
SplashComponent.decorators = [
    { type: Component, args: [{
                selector: 'app-splash',
                template: "<div class=\"background\">\n\n    <!--top-->\n    <div class=\"material-masthead-login\">\n      <div class=\"material-inner-masthead\">\n        <div class=\"logo-max\">\n          <img class=\"login-logo\" src=\"assets/saphira-logo-secondary.png\">\n          <h4 class=\"login-support-text\">Survey Management</h4>\n          <h4 [class]=\"this.statusClass\">{{connectedStatus}}</h4>\n        </div>\n      </div>\n    </div>\n\n    <div class=\"login-flex\" style=\"float: right;\">\n      <div class=\"login-box\">\n        <form (ngSubmit)=\"login()\" #loginForm=\"ngForm\" class=\"login-form\" id=\"loginme\">\n          <div class=\"input-container\">\n            <mat-form-field color=\"primary\">\n              <input matInput type=\"text\" id=\"username\" required [(ngModel)]=\"username\" name=\"username\" placeholder=\"Username\">\n            </mat-form-field>\n            <mat-form-field color=\"primary\">\n              <input matInput type=\"password\" id=\"password\" required [(ngModel)]=\"password\" (ngModelChange)=\"inputChanged($event)\"\n                placeholder=\"Password\" name=\"password\">\n            </mat-form-field>\n          </div>\n          <div class=\"login-btn-container\">\n            <button mat-raised-button class=\"login-btn\" [ngClass]=\"{'valid': loginForm.form.valid}\" type=\"submit\"\n              [disabled]=\"!loginForm.form.valid\" id=\"login-button\">\n              <span class=\"mat-button-wrapper\">Login</span>\n              <div class=\"mat-button-ripple mat-ripple\">\n              </div>\n              <div class=\"mat-button-focus-overlay\">\n              </div>\n            </button>\n          </div>\n        </form>\n        <span class=\"login-credentials-error\" *ngIf=\"loginFailed\">Invalid credentials, please try again</span>\n      </div>\n    </div>\n    <div class=\"login-support\">\n      <h4 class=\"login-support-text\">MagVAR Real-Time Center</h4>\n      <button class=\"menu-button phone-icon\" mat-icon-button matTooltip=\"Direct: 303-264-6380, Cell: 720-384-7649\">\n        <mat-icon>phone</mat-icon>\n      </button>\n      <button class=\"menu-button email-icon\" mat-icon-button matTooltip=\"Email directly at: surveys@magvar.com\" (click)=\"sendEmail()\">\n        <mat-icon>email</mat-icon>\n      </button>\n    </div>\n    <div class=\"copyright-container\">\n      <p class=\"copyright-text\">Copyright \u00A9 Magnetic Variations Services LLC 2019</p>\n    </div>\n  </div>\n",
                styles: ["@media print{.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-appearance-legacy.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-1.28122em) scale(.75);transform:translateY(-1.28122em) scale(.75)}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-form-field-autofill-control:-webkit-autofill+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.28121em) scale(.75);transform:translateY(-1.28121em) scale(.75)}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.2812em) scale(.75);transform:translateY(-1.2812em) scale(.75)}}.mat-badge-content{font-weight:600;font-size:12px;font-family:NunitoSans,sans-serif}.mat-badge-small .mat-badge-content{font-size:9px}.mat-badge-large .mat-badge-content{font-size:24px}.mat-h1,.mat-headline,.mat-typography h1{font:400 24px/32px NunitoSans,sans-serif;margin:0 0 16px}.mat-h2,.mat-title,.mat-typography h2{font:500 20px/32px NunitoSans,sans-serif;margin:0 0 16px}.mat-h3,.mat-subheading-2,.mat-typography h3{font:400 16px/28px NunitoSans,sans-serif;margin:0 0 16px}.mat-h4,.mat-subheading-1,.mat-typography h4{font:400 15px/24px NunitoSans,sans-serif;margin:0 0 16px}.mat-h5,.mat-h6,.mat-typography h5,.mat-typography h6{margin:0 0 12px}.mat-body-2,.mat-body-strong{font:500 14px/24px NunitoSans,sans-serif}.mat-body,.mat-body-1,.mat-typography{font:400 14px/20px NunitoSans,sans-serif}.mat-body p,.mat-body-1 p,.mat-typography p{margin:0 0 12px}.mat-caption,.mat-small{font:400 12px/20px NunitoSans,sans-serif}.mat-display-4,.mat-typography .mat-display-4{font:300 112px/112px NunitoSans,sans-serif;letter-spacing:-.05em;margin:0 0 56px}.mat-display-3,.mat-typography .mat-display-3{font:400 56px/56px NunitoSans,sans-serif;letter-spacing:-.02em;margin:0 0 64px}.mat-display-2,.mat-typography .mat-display-2{font:400 45px/48px NunitoSans,sans-serif;letter-spacing:-.005em;margin:0 0 64px}.mat-display-1,.mat-typography .mat-display-1{font:400 34px/40px NunitoSans,sans-serif;margin:0 0 64px}.mat-bottom-sheet-container{font:400 14px/20px NunitoSans,sans-serif}.mat-button,.mat-fab,.mat-flat-button,.mat-icon-button,.mat-mini-fab,.mat-raised-button,.mat-stroked-button{font-family:NunitoSans,sans-serif;font-size:14px;font-weight:500}.mat-button-toggle,.mat-card{font-family:NunitoSans,sans-serif}.mat-card-title{font-size:24px;font-weight:500}.mat-card-header .mat-card-title{font-size:20px}.mat-card-content,.mat-card-subtitle{font-size:14px}.mat-checkbox{font-family:NunitoSans,sans-serif}.mat-checkbox-layout .mat-checkbox-label{line-height:24px}.mat-chip{font-size:14px;font-weight:500}.mat-chip .mat-chip-remove.mat-icon,.mat-chip .mat-chip-trailing-icon.mat-icon{font-size:18px}.mat-table{font-family:NunitoSans,sans-serif}.mat-header-cell{font-size:12px;font-weight:500}.mat-cell,.mat-footer-cell{font-size:14px}.mat-calendar{font-family:NunitoSans,sans-serif}.mat-calendar-body{font-size:13px}.mat-calendar-body-label,.mat-calendar-period-button{font-size:14px;font-weight:500}.mat-calendar-table-header th{font-size:11px;font-weight:400}.mat-dialog-title{font:500 20px/32px NunitoSans,sans-serif}.mat-expansion-panel-header{font-family:NunitoSans,sans-serif;font-size:15px;font-weight:400}.mat-expansion-panel-content{font:400 14px/20px NunitoSans,sans-serif}.mat-form-field{font-size:inherit;font-weight:400;line-height:1.125;font-family:NunitoSans,sans-serif}.mat-form-field-wrapper{padding-bottom:1.34375em}.mat-form-field-prefix .mat-icon,.mat-form-field-suffix .mat-icon{font-size:150%;line-height:1.125}.mat-form-field-prefix .mat-icon-button,.mat-form-field-suffix .mat-icon-button{height:1.5em;width:1.5em}.mat-form-field-prefix .mat-icon-button .mat-icon,.mat-form-field-suffix .mat-icon-button .mat-icon{height:1.125em;line-height:1.125}.mat-form-field-infix{padding:.5em 0;border-top:.84375em solid transparent}.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-1.34373em) scale(.75);transform:translateY(-1.34373em) scale(.75);width:133.3333533333%}.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.34372em) scale(.75);transform:translateY(-1.34372em) scale(.75);width:133.3333633333%}.mat-form-field-label-wrapper{top:-.84375em;padding-top:.84375em}.mat-form-field-label{top:1.34375em}.mat-form-field-underline{bottom:1.34375em}.mat-form-field-subscript-wrapper{font-size:75%;margin-top:.6666666667em;top:calc(100% - 1.7916666667em)}.mat-form-field-appearance-legacy .mat-form-field-wrapper{padding-bottom:1.25em}.mat-form-field-appearance-legacy .mat-form-field-infix{padding:.4375em 0}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-appearance-legacy.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00106px);transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00106px);-ms-transform:translateY(-1.28119em) scale(.75);width:133.3333933333%}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-form-field-autofill-control:-webkit-autofill+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00107px);transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00107px);-ms-transform:translateY(-1.28118em) scale(.75);width:133.3334033333%}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00108px);transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00108px);-ms-transform:translateY(-1.28117em) scale(.75);width:133.3334133333%}.mat-form-field-appearance-legacy .mat-form-field-label{top:1.28125em}.mat-form-field-appearance-legacy .mat-form-field-underline{bottom:1.25em}.mat-form-field-appearance-legacy .mat-form-field-subscript-wrapper{margin-top:.5416666667em;top:calc(100% - 1.6666666667em)}@media print{.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-appearance-legacy.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-1.28116em) scale(.75);transform:translateY(-1.28116em) scale(.75)}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-form-field-autofill-control:-webkit-autofill+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.28115em) scale(.75);transform:translateY(-1.28115em) scale(.75)}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.28114em) scale(.75);transform:translateY(-1.28114em) scale(.75)}}.mat-form-field-appearance-fill .mat-form-field-infix{padding:.25em 0 .75em}.mat-form-field-appearance-fill .mat-form-field-label{top:1.09375em;margin-top:-.5em}.mat-form-field-appearance-fill.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-appearance-fill.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-.59373em) scale(.75);transform:translateY(-.59373em) scale(.75);width:133.3333533333%}.mat-form-field-appearance-fill.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-.59372em) scale(.75);transform:translateY(-.59372em) scale(.75);width:133.3333633333%}.mat-form-field-appearance-outline .mat-form-field-infix{padding:1em 0}.mat-form-field-appearance-outline .mat-form-field-label{top:1.84375em;margin-top:-.25em}.mat-form-field-appearance-outline.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-appearance-outline.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-1.59373em) scale(.75);transform:translateY(-1.59373em) scale(.75);width:133.3333533333%}.mat-form-field-appearance-outline.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.59372em) scale(.75);transform:translateY(-1.59372em) scale(.75);width:133.3333633333%}.mat-grid-tile-footer,.mat-grid-tile-header{font-size:14px}.mat-grid-tile-footer .mat-line,.mat-grid-tile-header .mat-line{white-space:nowrap;overflow:hidden;text-overflow:ellipsis;display:block;box-sizing:border-box}.mat-grid-tile-footer .mat-line:nth-child(n+2),.mat-grid-tile-header .mat-line:nth-child(n+2){font-size:12px}input.mat-input-element{margin-top:-.0625em}.mat-menu-item{font-family:NunitoSans,sans-serif;font-size:14px;font-weight:400}.mat-paginator,.mat-paginator-page-size .mat-select-trigger{font-family:NunitoSans,sans-serif;font-size:12px}.mat-radio-button,.mat-select{font-family:NunitoSans,sans-serif}.mat-select-trigger{height:1.125em}.mat-slide-toggle-content{font-family:NunitoSans,sans-serif}.mat-slider-thumb-label-text{font-family:NunitoSans,sans-serif;font-size:12px;font-weight:500}.mat-stepper-horizontal,.mat-stepper-vertical{font-family:NunitoSans,sans-serif}.mat-step-label{font-size:14px;font-weight:400}.mat-step-sub-label-error{font-weight:400}.mat-step-label-error{font-size:14px}.mat-step-label-selected{font-size:14px;font-weight:500}.mat-tab-group{font-family:NunitoSans,sans-serif}.mat-tab-label,.mat-tab-link{font-family:NunitoSans,sans-serif;font-size:14px;font-weight:500}.mat-toolbar,.mat-toolbar h1,.mat-toolbar h2,.mat-toolbar h3,.mat-toolbar h4,.mat-toolbar h5,.mat-toolbar h6{font:500 20px/32px NunitoSans,sans-serif;margin:0}.mat-tooltip{font-family:NunitoSans,sans-serif;font-size:10px;padding-top:6px;padding-bottom:6px}.mat-tooltip-handset{font-size:14px;padding-top:8px;padding-bottom:8px}.mat-list-item,.mat-list-option{font-family:NunitoSans,sans-serif}.mat-list-base .mat-list-item{font-size:16px}.mat-list-base .mat-list-item .mat-line{white-space:nowrap;overflow:hidden;text-overflow:ellipsis;display:block;box-sizing:border-box}.mat-list-base .mat-list-item .mat-line:nth-child(n+2){font-size:14px}.mat-list-base .mat-list-option{font-size:16px}.mat-list-base .mat-list-option .mat-line{white-space:nowrap;overflow:hidden;text-overflow:ellipsis;display:block;box-sizing:border-box}.mat-list-base .mat-list-option .mat-line:nth-child(n+2){font-size:14px}.mat-list-base .mat-subheader{font-family:NunitoSans,sans-serif;font-size:14px;font-weight:500}.mat-list-base[dense] .mat-list-item{font-size:12px}.mat-list-base[dense] .mat-list-item .mat-line{white-space:nowrap;overflow:hidden;text-overflow:ellipsis;display:block;box-sizing:border-box}.mat-list-base[dense] .mat-list-item .mat-line:nth-child(n+2),.mat-list-base[dense] .mat-list-option{font-size:12px}.mat-list-base[dense] .mat-list-option .mat-line{white-space:nowrap;overflow:hidden;text-overflow:ellipsis;display:block;box-sizing:border-box}.mat-list-base[dense] .mat-list-option .mat-line:nth-child(n+2){font-size:12px}.mat-list-base[dense] .mat-subheader{font-family:NunitoSans,sans-serif;font-size:12px;font-weight:500}.mat-option{font-family:NunitoSans,sans-serif;font-size:16px}.mat-optgroup-label{font:500 14px/24px NunitoSans,sans-serif}.mat-simple-snackbar{font-family:NunitoSans,sans-serif;font-size:14px}.mat-simple-snackbar-action{line-height:1;font-family:inherit;font-size:inherit;font-weight:500}.mat-tree{font-family:NunitoSans,sans-serif}.mat-nested-tree-node,.mat-tree-node{font-weight:400;font-size:14px}.mat-ripple{overflow:hidden;position:relative}.mat-ripple.mat-ripple-unbounded{overflow:visible}.mat-ripple-element{position:absolute;border-radius:50%;pointer-events:none;-webkit-transition:opacity,-webkit-transform cubic-bezier(0,0,.2,1);transition:opacity,transform cubic-bezier(0,0,.2,1),-webkit-transform cubic-bezier(0,0,.2,1);-webkit-transform:scale(0);transform:scale(0)}@media (-ms-high-contrast:active){.mat-ripple-element{display:none}}.cdk-visually-hidden{border:0;clip:rect(0 0 0 0);height:1px;margin:-1px;overflow:hidden;padding:0;position:absolute;width:1px;outline:0;-webkit-appearance:none;-moz-appearance:none}.cdk-global-overlay-wrapper,.cdk-overlay-container{pointer-events:none;top:0;left:0;height:100%;width:100%}.cdk-overlay-container{position:fixed;z-index:1000}.cdk-overlay-container:empty{display:none}.cdk-global-overlay-wrapper{display:-webkit-box;display:flex;position:absolute;z-index:1000}.cdk-overlay-pane{position:absolute;pointer-events:auto;box-sizing:border-box;z-index:1000;display:-webkit-box;display:flex;max-width:100%;max-height:100%}.cdk-overlay-backdrop{position:absolute;top:0;bottom:0;left:0;right:0;z-index:1000;pointer-events:auto;-webkit-tap-highlight-color:transparent;-webkit-transition:opacity .4s cubic-bezier(.25,.8,.25,1);transition:opacity .4s cubic-bezier(.25,.8,.25,1);opacity:0}.cdk-overlay-backdrop.cdk-overlay-backdrop-showing{opacity:1}@media screen and (-ms-high-contrast:active){.cdk-overlay-backdrop.cdk-overlay-backdrop-showing{opacity:.6}}.cdk-overlay-dark-backdrop{background:rgba(0,0,0,.32)}.cdk-overlay-transparent-backdrop,.cdk-overlay-transparent-backdrop.cdk-overlay-backdrop-showing{opacity:0}.cdk-overlay-connected-position-bounding-box{position:absolute;z-index:1000;display:-webkit-box;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;flex-direction:column;min-width:1px;min-height:1px}.cdk-global-scrollblock{position:fixed;width:100%;overflow-y:scroll}@-webkit-keyframes cdk-text-field-autofill-start{/*!*/}@keyframes cdk-text-field-autofill-start{/*!*/}@-webkit-keyframes cdk-text-field-autofill-end{/*!*/}@keyframes cdk-text-field-autofill-end{/*!*/}.cdk-text-field-autofill-monitored:-webkit-autofill{-webkit-animation-name:cdk-text-field-autofill-start;animation-name:cdk-text-field-autofill-start}.cdk-text-field-autofill-monitored:not(:-webkit-autofill){-webkit-animation-name:cdk-text-field-autofill-end;animation-name:cdk-text-field-autofill-end}textarea.cdk-textarea-autosize{resize:none}textarea.cdk-textarea-autosize-measuring{height:auto!important;overflow:hidden!important;padding:2px 0!important;box-sizing:content-box!important}input:-webkit-autofill,input:-webkit-autofill:active,input:-webkit-autofill:focus,input:-webkit-autofill:hover{-webkit-transition:background-color 5000s ease-in-out 500s;transition:background-color 5000s ease-in-out 500s;background-color:#fff;font-size:.8em;padding:5px}.material-inner-masthead{padding:15px}.logo-max{position:absolute;margin:auto;padding:135px 0 0;text-align:center}.logo-custom-login{position:absolute;top:43px;left:229px}.symphony-version-login{color:#fff;z-index:9887;position:fixed;font-size:1.2em;margin:28px 0 0 163px}.input-container{display:-webkit-box;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;flex-direction:column}.login-h1{font-size:2.2em;color:#fff;line-height:2em;font-weight:300;font-family:Poppins,sans-serif}.login-h2{font-size:3em;color:#bdbdbd;font-weight:300;list-style-type:none;line-height:1.2em}.login-h3{color:#bdbdbd;font-weight:300;font-size:1.4em;list-style-type:none;line-height:1.5em}.login-flex{display:-webkit-box;display:flex;-webkit-box-align:center;align-items:center;-webkit-box-pack:center;justify-content:center;margin-top:135px}.login-box{z-index:2;border-radius:6px;line-height:2em;font-size:1.45em;width:285px;height:270px;display:-webkit-box;display:flex;padding:0 40px 30px 0;-webkit-box-orient:vertical;-webkit-box-direction:normal;flex-direction:column;-webkit-box-pack:right;justify-content:right}.login-form{display:-webkit-box;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;flex-direction:column;-webkit-box-align:center;align-items:center}.login-btn-container{float:right;padding:20px 0 0}.login-btn{background-color:#1e4e79;color:#fff;float:right;font-weight:800;letter-spacing:1px;text-transform:uppercase}.login-btn .mat-button-wrapper{font-size:14px!important}.login-btn.valid:hover{background:rgba(189,189,189,.75);color:#233538;text-decoration:none}.login-label{color:#000;font-size:.75em;font-weight:300;text-decoration:none;float:right;margin:10px 50px 0 0;display:-webkit-box;display:flex}.login-credentials-error{font-size:.65em;color:#48abd6;border-radius:5px;font-weight:600;padding:0;width:100%;font-style:italic;float:right;display:block;text-align:center}.background{width:700px;height:600px;background-color:#fff;background-size:cover;padding:0;overflow:hidden;margin:0 auto;box-shadow:-6px 6px 15px 2px rgba(0,0,0,.4)}.background-overlay{display:-webkit-box;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;flex-direction:column;border-radius:8px;bottom:0;width:100%;height:100%}.mat-button-wrapper{font-size:12px!important}.mat-accent.mat-input-element ::ng-deep .mat-input-placeholder{color:#143450}.mat-accent.mat-input-element ::ng-deep .mat-form-field-required-marker{color:#143450}.mat-accent.mat-input-element ::ng-deep .mat-form-field-ripple{background-color:#143450}.mat-input-container.ng-touched.ng-invalid ::ng-deep .mat-input-placeholder{color:#143450}.mat-input-container.ng-touched.ng-invalid ::ng-deep .mat-form-field-required-marker{color:#143450}.mat-input-container.ng-touched.ng-invalid ::ng-deep .mat-form-field-ripple{background-color:#143450}:host ::ng-deep .mat-input-placeholder{font-size:large;font-family:Poppins,sans-serif}.login-support{display:-webkit-inline-box;display:inline-flex;-webkit-box-align:center;align-items:center;right:0;width:100%;-webkit-box-pack:center;justify-content:center}.login-support-text{font-family:NunitoSans,sans-serif;font-weight:200}.login-logo{width:370px}.connected{background-color:#00ff40;color:#000;width:94px;text-align:center;margin:10px auto;border-radius:12px;padding:4px}.not-connected{background-color:#ff5454;color:#000;width:120px;text-align:center;margin:10px auto;border-radius:12px;padding:4px}.copyright-container{display:-webkit-box;display:flex;-webkit-box-pack:center;justify-content:center;height:81px;-webkit-box-align:end;align-items:flex-end}.copyright-text{font-family:NunitoSans;font-size:12px;letter-spacing:.2px;color:rgba(0,0,0,.7)}"]
            }] }
];
/** @nocollapse */
SplashComponent.ctorParameters = () => [
    { type: AuthService },
    { type: MatDialog },
    { type: Router },
    { type: PingService },
    { type: ClientRequisitesService },
    { type: TitleFlasherService }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * Opens new browser tabs.
 * Workaround for Chrome.
 * @see https://blog.chromium.org/2009/12/links-that-open-in-new-processes.html
 * @see https://stackoverflow.com/a/40909198
 */
class BrowserTabService {
    constructor() { }
    /**
     * @param {?} tag
     * @return {?}
     */
    set anchorTag(tag) {
        if (!tag) {
            return;
        }
        this.tag = tag;
    }
    /**
     * Opens a new browser tab
     * @param {?} url
     * @return {?}
     */
    open(url) {
        if (!url) {
            return;
        }
        if (!this.tag) {
            return;
        }
        this.tag.href = url;
        this.tag.click();
    }
    /**
     * Opens Wellbore Detail in a new browser tab
     * @param {?} wellboreId
     * @return {?}
     */
    openWellboreDetail(wellboreId) {
        if (!wellboreId) {
            return;
        }
        /** @type {?} */
        const url = window.location.origin
            + window.location.pathname
            + '#/wellbore-detail/'
            + wellboreId;
        this.open(url);
    }
}
BrowserTabService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
BrowserTabService.ctorParameters = () => [];
/** @nocollapse */ BrowserTabService.ngInjectableDef = ɵɵdefineInjectable({ factory: function BrowserTabService_Factory() { return new BrowserTabService(); }, token: BrowserTabService, providedIn: "root" });

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class HeaderComponent {
    /**
     * @param {?} auth
     * @param {?} ping
     * @param {?} router
     * @param {?} browserTabService
     */
    constructor(auth, ping, router, browserTabService) {
        this.auth = auth;
        this.ping = ping;
        this.router = router;
        this.browserTabService = browserTabService;
        this.isAdmin = false;
        this.username = '';
        this.isConnected = 'status connected';
        this.connectedStatus = 'Connected';
        this.shouldPing = true;
        this.oldSaphiraAdmin = {
            dev: `http://localhost:8245/#/admin/permissions`,
            prod: `${window.location.origin}/#/admin/permissions`
        };
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.updateUsername();
        this.ping
            .status()
            .pipe(takeWhile((/**
         * @return {?}
         */
        () => this.shouldPing))).subscribe((/**
         * @param {?} connected
         * @return {?}
         */
        connected => {
            if (connected) {
                this.isConnected = 'status connected';
                this.connectedStatus = 'Connected';
                if (this.ping.tripTime > 1) {
                    this.connectedStatus +=
                        ` at ${this.ping.tripTime.toFixed(0)}ms to server`;
                }
            }
            else {
                this.isConnected = 'status not-connected';
                this.connectedStatus = 'Not connected';
            }
        }));
    }
    /**
     * @return {?}
     */
    updateUsername() {
        this.auth.getUser().subscribe((/**
         * @param {?} res
         * @return {?}
         */
        res => {
            if (res) {
                Object.values(res['principal']['attributes']).forEach((/**
                 * @param {?} o
                 * @return {?}
                 */
                o => {
                    if (o['name'] === 'displayName') {
                        this.username = o['values'][0];
                    }
                }));
                if (!this.username.length) {
                    this.username = res['principal']['name'];
                }
            }
            else {
                this.username = '';
            }
        }));
    }
    /**
     * @return {?}
     */
    sendEmail() {
        location.href = 'mailto:surveys@magvar.com';
    }
    /**
     * @return {?}
     */
    logout() {
        this.username = '';
        this.auth.logout().subscribe();
    }
    /**
     * @return {?}
     */
    goToOldSaphiraAdmin() {
        {
            this.browserTabService.open(this.oldSaphiraAdmin.dev);
        }
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        this.shouldPing = false;
    }
}
HeaderComponent.decorators = [
    { type: Component, args: [{
                selector: 'app-header',
                template: "<section class=\"main-header\">\n  <div class=\"header-left\">\n    <div class=\"text-logo-container\">\n      <img class=\"login-logo\" src=\"assets/saphira-font-logo-white.png\" [routerLink]=\"['/wellbore-selection']\" routerLinkActive=\"router-link-active\">\n    </div>\n\n      <button class=\"menu-button\"\n              matTooltip=\"Direct: 303-264-6380, Cell: 720-384-7649\"\n              matTooltipClass=\"larger-tooltip\"\n              mat-icon-button>\n        <mat-icon class=\"contact-icons\">phone</mat-icon>\n      </button>\n      <button class=\"menu-button\"\n              matTooltip=\"surveys@magvar.com\"\n              matTooltipClass=\"larger-tooltip\"\n              mat-icon-button\n              (click)=\"sendEmail()\">\n        <mat-icon class=\"contact-icons\">email</mat-icon>\n      </button>\n    </div>\n\n    <div class=\"magvar-logo-container\">\n      <img class=\"login-logo\" src=\"assets/magvar_logo_white.png\" [routerLink]=\"['/wellbore-selection']\" routerLinkActive=\"router-link-active\">\n      <p class=\"copyright-text\">Copyright \u00A9 Magnetic Variations Services LLC 2019</p>\n    </div>\n\n    <span class=\"right-menu\">\n      <div class=\"versions\">\n        <span class=\"lighter\">VERSIONS </span>\n        <span>UI </span>\n        <strong>{{version}} </strong>\n        <span>BE </span>\n        <strong>{{ping.apiVersion}}</strong>\n      </div>\n      <div class=\"connected-container\">\n        <i class=\"material-icons account-icon\">\n          account_circle\n        </i>\n        <div [matTooltip]=\"connectedStatus\" [class]=\"isConnected\"></div>\n      </div>\n      <h4 class=\"user-name\">{{this.username}}</h4>\n\n      <button class=\"menu-button\" mat-icon-button [matMenuTriggerFor]=\"primaryMenu\" id=\"user-menu\">\n        <mat-icon class=\"material-icons menu-arrow-down\">\n          keyboard_arrow_down\n        </mat-icon>\n      </button>\n\n      <mat-menu #primaryMenu=\"matMenu\">\n        <a [routerLink]=\"['/wellbore-selection']\" routerLinkActive=\"router-link-active\">\n          <button mat-menu-item class=\"button-link\" class=\"user-menu-item\" id=\"wellbore-selection-btn\">\n            Wellbore Selection\n          </button>\n        </a>\n        <a [routerLink]=\"['/wellbore-status']\" routerLinkActive=\"router-link-active\">\n          <button mat-menu-item class=\"button-link\" class=\"user-menu-item\">\n            Wellbore Status\n          </button>\n        </a>\n        <br>\n        <button mat-menu-item class=\"user-menu-item\" id=\"admin-view-btn\" (click)=\"goToOldSaphiraAdmin()\">Admin View</button>\n        <a routerLinkActive=\"router-link-active\">\n          <button mat-menu-item (click)=\"logout()\" class=\"user-menu-item\" id=\"logout-btn\">Logout</button>\n        </a>\n      </mat-menu>\n    </span>\n</section>\n",
                styles: ["@media print{.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-appearance-legacy.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-1.28122em) scale(.75);transform:translateY(-1.28122em) scale(.75)}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-form-field-autofill-control:-webkit-autofill+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.28121em) scale(.75);transform:translateY(-1.28121em) scale(.75)}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.2812em) scale(.75);transform:translateY(-1.2812em) scale(.75)}}.mat-badge-content{font-weight:600;font-size:12px;font-family:NunitoSans,sans-serif}.mat-badge-small .mat-badge-content{font-size:9px}.mat-badge-large .mat-badge-content{font-size:24px}.mat-h1,.mat-headline,.mat-typography h1{font:400 24px/32px NunitoSans,sans-serif;margin:0 0 16px}.mat-h2,.mat-title,.mat-typography h2{font:500 20px/32px NunitoSans,sans-serif;margin:0 0 16px}.mat-h3,.mat-subheading-2,.mat-typography h3{font:400 16px/28px NunitoSans,sans-serif;margin:0 0 16px}.mat-h4,.mat-subheading-1,.mat-typography h4{font:400 15px/24px NunitoSans,sans-serif;margin:0 0 16px}.mat-h5,.mat-h6,.mat-typography h5,.mat-typography h6{margin:0 0 12px}.mat-body-2,.mat-body-strong{font:500 14px/24px NunitoSans,sans-serif}.mat-body,.mat-body-1,.mat-typography{font:400 14px/20px NunitoSans,sans-serif}.mat-body p,.mat-body-1 p,.mat-typography p{margin:0 0 12px}.mat-caption,.mat-small{font:400 12px/20px NunitoSans,sans-serif}.mat-display-4,.mat-typography .mat-display-4{font:300 112px/112px NunitoSans,sans-serif;letter-spacing:-.05em;margin:0 0 56px}.mat-display-3,.mat-typography .mat-display-3{font:400 56px/56px NunitoSans,sans-serif;letter-spacing:-.02em;margin:0 0 64px}.mat-display-2,.mat-typography .mat-display-2{font:400 45px/48px NunitoSans,sans-serif;letter-spacing:-.005em;margin:0 0 64px}.mat-display-1,.mat-typography .mat-display-1{font:400 34px/40px NunitoSans,sans-serif;margin:0 0 64px}.mat-bottom-sheet-container{font:400 14px/20px NunitoSans,sans-serif}.mat-button,.mat-fab,.mat-flat-button,.mat-icon-button,.mat-mini-fab,.mat-raised-button,.mat-stroked-button{font-family:NunitoSans,sans-serif;font-size:14px;font-weight:500}.mat-button-toggle,.mat-card{font-family:NunitoSans,sans-serif}.mat-card-title{font-size:24px;font-weight:500}.mat-card-header .mat-card-title{font-size:20px}.mat-card-content,.mat-card-subtitle{font-size:14px}.mat-checkbox{font-family:NunitoSans,sans-serif}.mat-checkbox-layout .mat-checkbox-label{line-height:24px}.mat-chip{font-size:14px;font-weight:500}.mat-chip .mat-chip-remove.mat-icon,.mat-chip .mat-chip-trailing-icon.mat-icon{font-size:18px}.mat-table{font-family:NunitoSans,sans-serif}.mat-header-cell{font-size:12px;font-weight:500}.mat-cell,.mat-footer-cell{font-size:14px}.mat-calendar{font-family:NunitoSans,sans-serif}.mat-calendar-body{font-size:13px}.mat-calendar-body-label,.mat-calendar-period-button{font-size:14px;font-weight:500}.mat-calendar-table-header th{font-size:11px;font-weight:400}.mat-dialog-title{font:500 20px/32px NunitoSans,sans-serif}.mat-expansion-panel-header{font-family:NunitoSans,sans-serif;font-size:15px;font-weight:400}.mat-expansion-panel-content{font:400 14px/20px NunitoSans,sans-serif}.mat-form-field{font-size:inherit;font-weight:400;line-height:1.125;font-family:NunitoSans,sans-serif}.mat-form-field-wrapper{padding-bottom:1.34375em}.mat-form-field-prefix .mat-icon,.mat-form-field-suffix .mat-icon{font-size:150%;line-height:1.125}.mat-form-field-prefix .mat-icon-button,.mat-form-field-suffix .mat-icon-button{height:1.5em;width:1.5em}.mat-form-field-prefix .mat-icon-button .mat-icon,.mat-form-field-suffix .mat-icon-button .mat-icon{height:1.125em;line-height:1.125}.mat-form-field-infix{padding:.5em 0;border-top:.84375em solid transparent}.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-1.34373em) scale(.75);transform:translateY(-1.34373em) scale(.75);width:133.3333533333%}.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.34372em) scale(.75);transform:translateY(-1.34372em) scale(.75);width:133.3333633333%}.mat-form-field-label-wrapper{top:-.84375em;padding-top:.84375em}.mat-form-field-label{top:1.34375em}.mat-form-field-underline{bottom:1.34375em}.mat-form-field-subscript-wrapper{font-size:75%;margin-top:.6666666667em;top:calc(100% - 1.7916666667em)}.mat-form-field-appearance-legacy .mat-form-field-wrapper{padding-bottom:1.25em}.mat-form-field-appearance-legacy .mat-form-field-infix{padding:.4375em 0}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-appearance-legacy.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00106px);transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00106px);-ms-transform:translateY(-1.28119em) scale(.75);width:133.3333933333%}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-form-field-autofill-control:-webkit-autofill+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00107px);transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00107px);-ms-transform:translateY(-1.28118em) scale(.75);width:133.3334033333%}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00108px);transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00108px);-ms-transform:translateY(-1.28117em) scale(.75);width:133.3334133333%}.mat-form-field-appearance-legacy .mat-form-field-label{top:1.28125em}.mat-form-field-appearance-legacy .mat-form-field-underline{bottom:1.25em}.mat-form-field-appearance-legacy .mat-form-field-subscript-wrapper{margin-top:.5416666667em;top:calc(100% - 1.6666666667em)}@media print{.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-appearance-legacy.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-1.28116em) scale(.75);transform:translateY(-1.28116em) scale(.75)}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-form-field-autofill-control:-webkit-autofill+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.28115em) scale(.75);transform:translateY(-1.28115em) scale(.75)}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.28114em) scale(.75);transform:translateY(-1.28114em) scale(.75)}}.mat-form-field-appearance-fill .mat-form-field-infix{padding:.25em 0 .75em}.mat-form-field-appearance-fill .mat-form-field-label{top:1.09375em;margin-top:-.5em}.mat-form-field-appearance-fill.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-appearance-fill.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-.59373em) scale(.75);transform:translateY(-.59373em) scale(.75);width:133.3333533333%}.mat-form-field-appearance-fill.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-.59372em) scale(.75);transform:translateY(-.59372em) scale(.75);width:133.3333633333%}.mat-form-field-appearance-outline .mat-form-field-infix{padding:1em 0}.mat-form-field-appearance-outline .mat-form-field-label{top:1.84375em;margin-top:-.25em}.mat-form-field-appearance-outline.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-appearance-outline.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-1.59373em) scale(.75);transform:translateY(-1.59373em) scale(.75);width:133.3333533333%}.mat-form-field-appearance-outline.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.59372em) scale(.75);transform:translateY(-1.59372em) scale(.75);width:133.3333633333%}.mat-grid-tile-footer,.mat-grid-tile-header{font-size:14px}.mat-grid-tile-footer .mat-line,.mat-grid-tile-header .mat-line{white-space:nowrap;overflow:hidden;text-overflow:ellipsis;display:block;box-sizing:border-box}.mat-grid-tile-footer .mat-line:nth-child(n+2),.mat-grid-tile-header .mat-line:nth-child(n+2){font-size:12px}input.mat-input-element{margin-top:-.0625em}.mat-menu-item{font-family:NunitoSans,sans-serif;font-size:14px;font-weight:400}.mat-paginator,.mat-paginator-page-size .mat-select-trigger{font-family:NunitoSans,sans-serif;font-size:12px}.mat-radio-button,.mat-select{font-family:NunitoSans,sans-serif}.mat-select-trigger{height:1.125em}.mat-slide-toggle-content{font-family:NunitoSans,sans-serif}.mat-slider-thumb-label-text{font-family:NunitoSans,sans-serif;font-size:12px;font-weight:500}.mat-stepper-horizontal,.mat-stepper-vertical{font-family:NunitoSans,sans-serif}.mat-step-label{font-size:14px;font-weight:400}.mat-step-sub-label-error{font-weight:400}.mat-step-label-error{font-size:14px}.mat-step-label-selected{font-size:14px;font-weight:500}.mat-tab-group{font-family:NunitoSans,sans-serif}.mat-tab-label,.mat-tab-link{font-family:NunitoSans,sans-serif;font-size:14px;font-weight:500}.mat-toolbar,.mat-toolbar h1,.mat-toolbar h2,.mat-toolbar h3,.mat-toolbar h4,.mat-toolbar h5,.mat-toolbar h6{font:500 20px/32px NunitoSans,sans-serif;margin:0}.mat-tooltip{font-family:NunitoSans,sans-serif;font-size:10px;padding-top:6px;padding-bottom:6px}.mat-tooltip-handset{font-size:14px;padding-top:8px;padding-bottom:8px}.mat-list-item,.mat-list-option{font-family:NunitoSans,sans-serif}.mat-list-base .mat-list-item{font-size:16px}.mat-list-base .mat-list-item .mat-line{white-space:nowrap;overflow:hidden;text-overflow:ellipsis;display:block;box-sizing:border-box}.mat-list-base .mat-list-item .mat-line:nth-child(n+2){font-size:14px}.mat-list-base .mat-list-option{font-size:16px}.mat-list-base .mat-list-option .mat-line{white-space:nowrap;overflow:hidden;text-overflow:ellipsis;display:block;box-sizing:border-box}.mat-list-base .mat-list-option .mat-line:nth-child(n+2){font-size:14px}.mat-list-base .mat-subheader{font-family:NunitoSans,sans-serif;font-size:14px;font-weight:500}.mat-list-base[dense] .mat-list-item{font-size:12px}.mat-list-base[dense] .mat-list-item .mat-line{white-space:nowrap;overflow:hidden;text-overflow:ellipsis;display:block;box-sizing:border-box}.mat-list-base[dense] .mat-list-item .mat-line:nth-child(n+2),.mat-list-base[dense] .mat-list-option{font-size:12px}.mat-list-base[dense] .mat-list-option .mat-line{white-space:nowrap;overflow:hidden;text-overflow:ellipsis;display:block;box-sizing:border-box}.mat-list-base[dense] .mat-list-option .mat-line:nth-child(n+2){font-size:12px}.mat-list-base[dense] .mat-subheader{font-family:NunitoSans,sans-serif;font-size:12px;font-weight:500}.mat-option{font-family:NunitoSans,sans-serif;font-size:16px}.mat-optgroup-label{font:500 14px/24px NunitoSans,sans-serif}.mat-simple-snackbar{font-family:NunitoSans,sans-serif;font-size:14px}.mat-simple-snackbar-action{line-height:1;font-family:inherit;font-size:inherit;font-weight:500}.mat-tree{font-family:NunitoSans,sans-serif}.mat-nested-tree-node,.mat-tree-node{font-weight:400;font-size:14px}.mat-ripple{overflow:hidden;position:relative}.mat-ripple.mat-ripple-unbounded{overflow:visible}.mat-ripple-element{position:absolute;border-radius:50%;pointer-events:none;-webkit-transition:opacity,-webkit-transform cubic-bezier(0,0,.2,1);transition:opacity,transform cubic-bezier(0,0,.2,1),-webkit-transform cubic-bezier(0,0,.2,1);-webkit-transform:scale(0);transform:scale(0)}@media (-ms-high-contrast:active){.mat-ripple-element{display:none}}.cdk-visually-hidden{border:0;clip:rect(0 0 0 0);height:1px;margin:-1px;overflow:hidden;padding:0;position:absolute;width:1px;outline:0;-webkit-appearance:none;-moz-appearance:none}.cdk-global-overlay-wrapper,.cdk-overlay-container{pointer-events:none;top:0;left:0;height:100%;width:100%}.cdk-overlay-container{position:fixed;z-index:1000}.cdk-overlay-container:empty{display:none}.cdk-global-overlay-wrapper{display:-webkit-box;display:flex;position:absolute;z-index:1000}.cdk-overlay-pane{position:absolute;pointer-events:auto;box-sizing:border-box;z-index:1000;display:-webkit-box;display:flex;max-width:100%;max-height:100%}.cdk-overlay-backdrop{position:absolute;top:0;bottom:0;left:0;right:0;z-index:1000;pointer-events:auto;-webkit-tap-highlight-color:transparent;-webkit-transition:opacity .4s cubic-bezier(.25,.8,.25,1);transition:opacity .4s cubic-bezier(.25,.8,.25,1);opacity:0}.cdk-overlay-backdrop.cdk-overlay-backdrop-showing{opacity:1}@media screen and (-ms-high-contrast:active){.cdk-overlay-backdrop.cdk-overlay-backdrop-showing{opacity:.6}}.cdk-overlay-dark-backdrop{background:rgba(0,0,0,.32)}.cdk-overlay-transparent-backdrop,.cdk-overlay-transparent-backdrop.cdk-overlay-backdrop-showing{opacity:0}.cdk-overlay-connected-position-bounding-box{position:absolute;z-index:1000;display:-webkit-box;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;flex-direction:column;min-width:1px;min-height:1px}.cdk-global-scrollblock{position:fixed;width:100%;overflow-y:scroll}@-webkit-keyframes cdk-text-field-autofill-start{/*!*/}@keyframes cdk-text-field-autofill-start{/*!*/}@-webkit-keyframes cdk-text-field-autofill-end{/*!*/}@keyframes cdk-text-field-autofill-end{/*!*/}.cdk-text-field-autofill-monitored:-webkit-autofill{-webkit-animation-name:cdk-text-field-autofill-start;animation-name:cdk-text-field-autofill-start}.cdk-text-field-autofill-monitored:not(:-webkit-autofill){-webkit-animation-name:cdk-text-field-autofill-end;animation-name:cdk-text-field-autofill-end}textarea.cdk-textarea-autosize{resize:none}textarea.cdk-textarea-autosize-measuring{height:auto!important;overflow:hidden!important;padding:2px 0!important;box-sizing:content-box!important}.main-header{padding:0 20px;height:56px;display:-webkit-box;display:flex;-webkit-box-pack:justify;justify-content:space-between;border-bottom:1px solid #333;background-color:#1e4e79;-webkit-box-align:center;align-items:center}.main-header .header-left{display:-webkit-box;display:flex;-webkit-box-orient:horizontal;-webkit-box-direction:normal;flex-direction:row;width:244px;-webkit-box-pack:justify;justify-content:space-between}.main-header .header-left .text-logo-container{display:-webkit-box;display:flex;-webkit-box-align:center;align-items:center;margin-right:4px}.main-header .header-left .text-logo-container img{height:24px}.main-header .magvar-logo-container{left:50%;-webkit-transform:translate(-50%);transform:translate(-50%);position:absolute;display:-webkit-box;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;flex-direction:column;-webkit-box-align:center;align-items:center}.copyright-text{width:275px;height:14px;font-family:NunitoSans;font-size:10px;font-weight:400;font-style:normal;font-stretch:normal;line-height:normal;letter-spacing:.2px;color:#fff;text-align:center}.button-link{text-decoration:none}.right-menu{-webkit-box-align:center;align-items:center;display:-webkit-box;display:flex;margin-right:-12px}.right-menu .connected-container{position:relative;margin-right:12px}.right-menu .connected-container i{color:#fff;font-size:29px}.menu-arrow-down{color:#fff}.contact-icons{color:#b4e1f1}.versions{color:#fff;font-family:NunitoSans;font-size:16px;font-weight:200;letter-spacing:.2px;margin-right:8px;padding-top:4px}.lighter{color:#fff8}.user-name{color:#fff;font-family:NunitoSans;font-size:20px;font-weight:200;letter-spacing:.2px}.status{position:absolute;top:0;right:-2px;width:10px;height:10px;border-radius:6px;border:.5px solid #1e4e79}.connected{background:radial-gradient(#c4f100,#00ff40,#00ff40);box-shadow:-3px 4px 5px rgba(196,241,0,.5)}.not-connected{background:radial-gradient(#fd7302,#f90000,#9e0602);box-shadow:-3px 4px 5px rgba(251,104,104,.5)}.main-text{font-family:NunitoSans,sans-serif;font-size:36px;font-weight:200;color:#333;cursor:pointer;outline:0}.main-subtext{color:#777}.menu-button{-webkit-transition-duration:.2s;transition-duration:.2s;color:#fff}.menu-button:hover{-webkit-transition-duration:.15s;transition-duration:.15s}a{text-decoration:none}.larger-tooltip{font-size:14px}"]
            }] }
];
/** @nocollapse */
HeaderComponent.ctorParameters = () => [
    { type: AuthService },
    { type: PingService },
    { type: Router },
    { type: BrowserTabService }
];
HeaderComponent.propDecorators = {
    version: [{ type: Input }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
const moment = moment_;
class TimeService {
    constructor() {
        this.getMomentFromInstant = (/**
         * @param {?} instant
         * @return {?}
         */
        (instant) => {
            // convert the nanos to seconds, add to seconds, and multiply by 1000 to get milliseconds,
            // required by the moment constructor.
            return moment(1000.0 * (instant.seconds + (instant.nanos / 1000000000.0)));
        });
        this.getTwelveHourMomentFromInstant = (/**
         * @param {?} instant
         * @return {?}
         */
        (instant) => {
            // convert the nanos to seconds, add to seconds, and multiply by 1000 to get milliseconds,
            // required by the moment constructor.
            return moment(1000.0 * (instant.seconds + (instant.nanos / 1000000000.0))).tz('UTC').format('MMM Do YYYY, h:mm:ss a');
        });
    }
    /**
     * @param {?} timestamp
     * @return {?}
     */
    momentFromNow(timestamp) {
        /** @type {?} */
        const now = moment();
        if (timestamp && moment.isMoment(timestamp)) {
            if (timestamp > now) {
                return 'Now';
            }
            else {
                return timestamp.fromNow();
            }
        }
        else {
            return 'Never';
        }
    }
    /**
     * @param {?} timestamp
     * @return {?}
     */
    momentFromNowFormatter(timestamp) {
        /** @type {?} */
        const now = moment();
        if (timestamp.value && moment.isMoment(timestamp.value)) {
            if (timestamp.value > now) {
                return 'Now';
            }
            else {
                return timestamp.value.fromNow();
            }
        }
        else {
            return 'Never';
        }
    }
    // format date
    /**
     * @param {?} timestamp
     * @param {?} format
     * @return {?}
     */
    standardDateFormatter(timestamp, format) {
        return moment(timestamp).format(format);
    }
    // parse date based on format provided
    /**
     * @param {?} timestamp
     * @param {?} format
     * @return {?}
     */
    convertDatetoMoment(timestamp, format) {
        return moment(timestamp, format);
    }
    // changed from date1/date2  to momentA/momentB resp, since its confusing
    /**
     * @param {?} momentA
     * @param {?} momentB
     * @return {?}
     */
    dateComparator(momentA, momentB) {
        if (momentA && momentB) {
            if (momentA.isBefore(momentB)) {
                return 1;
            }
            else if (momentB.isBefore(momentA)) {
                return -1;
            }
            else {
                return 0;
            }
        }
        else if (momentA) {
            return -1;
        }
        else if (momentB) {
            return 1;
        }
        else {
            return 0;
        }
    }
    /**
     * @param {?} instant
     * @return {?}
     */
    getUTCTimestampFromInstant(instant) {
        // convert the nanos to seconds, add to seconds, and multiply by 1000 to get milliseconds,
        // required by the moment constructor.
        if (!instant) {
            return instant;
        }
        return moment.utc(1000.0 * (instant.seconds + (instant.nanos / 1000000000.0))).format('YYYY[-]MM[-]DD[T]HH[:]mm[:]ss.SSS[Z]');
    }
    /**
     * @return {?}
     */
    getTimeZoneAbbr() {
        /** @type {?} */
        const tz = {};
        tz['server'] = 'UTC';
        /** @type {?} */
        const tzClient = moment.tz(moment.tz.guess()).zoneAbbr();
        tz['client'] = (!isNull(tzClient) && !isUndefined(tzClient)) ? tzClient : 'Local TimeZone';
        return tz;
    }
    // convert to UTC
    /**
     * @param {?} timestamp
     * @return {?}
     */
    convertToUtc(timestamp) {
        return moment.utc(timestamp);
    }
    // get current date in specified format
    /**
     * @param {?} format
     * @return {?}
     */
    getCurrentDate(format) {
        return moment().format(format);
    }
}
TimeService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
TimeService.ctorParameters = () => [];
/** @nocollapse */ TimeService.ngInjectableDef = ɵɵdefineInjectable({ factory: function TimeService_Factory() { return new TimeService(); }, token: TimeService, providedIn: "root" });

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class DateTimePickerComponent {
    /**
     * @param {?} timeService
     * @param {?} dialogRef
     * @param {?} data
     */
    constructor(timeService, dialogRef, data) {
        this.timeService = timeService;
        this.dialogRef = dialogRef;
        this.data = data;
        this.dateEmitter = new EventEmitter();
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        if (this.data && this.data['date']) {
            this.value = new Date(this.data['date']);
        }
        else {
            this.value = new Date();
        }
    }
    /**
     * @return {?}
     */
    clearField() {
        this.dateEmitter.emit(null);
        this.closeField();
    }
    /**
     * @return {?}
     */
    setField() {
        this.dateEmitter.emit(this.timeService.standardDateFormatter(this.value, 'LLL'));
        this.closeField();
    }
    /**
     * @return {?}
     */
    closeField() {
        this.dialogRef.close();
    }
}
DateTimePickerComponent.decorators = [
    { type: Component, args: [{
                selector: 'app-date-time-picker',
                template: "<div mat-dialog-content id=\"date-time-container\">\n  <button mat-stroked-button class=\"date-time-button\" (click)=\"clearField()\">Clear Date/Time</button>\n  <owl-date-time-inline [(ngModel)]=\"value\"></owl-date-time-inline>\n  <span id=\"close-buttons\">\n    <button class=\"date-time-button bottom-buttons\" mat-stroked-button (click)=\"setField()\">Set Time</button>\n    <button class=\"date-time-button bottom-buttons\" mat-stroked-button (click)=\"closeField()\">Close</button>\n  </span>\n</div>\n",
                styles: ["@media print{.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-appearance-legacy.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-1.28122em) scale(.75);transform:translateY(-1.28122em) scale(.75)}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-form-field-autofill-control:-webkit-autofill+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.28121em) scale(.75);transform:translateY(-1.28121em) scale(.75)}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.2812em) scale(.75);transform:translateY(-1.2812em) scale(.75)}}.mat-badge-content{font-weight:600;font-size:12px;font-family:NunitoSans,sans-serif}.mat-badge-small .mat-badge-content{font-size:9px}.mat-badge-large .mat-badge-content{font-size:24px}.mat-h1,.mat-headline,.mat-typography h1{font:400 24px/32px NunitoSans,sans-serif;margin:0 0 16px}.mat-h2,.mat-title,.mat-typography h2{font:500 20px/32px NunitoSans,sans-serif;margin:0 0 16px}.mat-h3,.mat-subheading-2,.mat-typography h3{font:400 16px/28px NunitoSans,sans-serif;margin:0 0 16px}.mat-h4,.mat-subheading-1,.mat-typography h4{font:400 15px/24px NunitoSans,sans-serif;margin:0 0 16px}.mat-h5,.mat-h6,.mat-typography h5,.mat-typography h6{margin:0 0 12px}.mat-body-2,.mat-body-strong{font:500 14px/24px NunitoSans,sans-serif}.mat-body,.mat-body-1,.mat-typography{font:400 14px/20px NunitoSans,sans-serif}.mat-body p,.mat-body-1 p,.mat-typography p{margin:0 0 12px}.mat-caption,.mat-small{font:400 12px/20px NunitoSans,sans-serif}.mat-display-4,.mat-typography .mat-display-4{font:300 112px/112px NunitoSans,sans-serif;letter-spacing:-.05em;margin:0 0 56px}.mat-display-3,.mat-typography .mat-display-3{font:400 56px/56px NunitoSans,sans-serif;letter-spacing:-.02em;margin:0 0 64px}.mat-display-2,.mat-typography .mat-display-2{font:400 45px/48px NunitoSans,sans-serif;letter-spacing:-.005em;margin:0 0 64px}.mat-display-1,.mat-typography .mat-display-1{font:400 34px/40px NunitoSans,sans-serif;margin:0 0 64px}.mat-bottom-sheet-container{font:400 14px/20px NunitoSans,sans-serif}.mat-button,.mat-fab,.mat-flat-button,.mat-icon-button,.mat-mini-fab,.mat-raised-button,.mat-stroked-button{font-family:NunitoSans,sans-serif;font-size:14px;font-weight:500}.mat-button-toggle,.mat-card{font-family:NunitoSans,sans-serif}.mat-card-title{font-size:24px;font-weight:500}.mat-card-header .mat-card-title{font-size:20px}.mat-card-content,.mat-card-subtitle{font-size:14px}.mat-checkbox{font-family:NunitoSans,sans-serif}.mat-checkbox-layout .mat-checkbox-label{line-height:24px}.mat-chip{font-size:14px;font-weight:500}.mat-chip .mat-chip-remove.mat-icon,.mat-chip .mat-chip-trailing-icon.mat-icon{font-size:18px}.mat-table{font-family:NunitoSans,sans-serif}.mat-header-cell{font-size:12px;font-weight:500}.mat-cell,.mat-footer-cell{font-size:14px}.mat-calendar{font-family:NunitoSans,sans-serif}.mat-calendar-body{font-size:13px}.mat-calendar-body-label,.mat-calendar-period-button{font-size:14px;font-weight:500}.mat-calendar-table-header th{font-size:11px;font-weight:400}.mat-dialog-title{font:500 20px/32px NunitoSans,sans-serif}.mat-expansion-panel-header{font-family:NunitoSans,sans-serif;font-size:15px;font-weight:400}.mat-expansion-panel-content{font:400 14px/20px NunitoSans,sans-serif}.mat-form-field{font-size:inherit;font-weight:400;line-height:1.125;font-family:NunitoSans,sans-serif}.mat-form-field-wrapper{padding-bottom:1.34375em}.mat-form-field-prefix .mat-icon,.mat-form-field-suffix .mat-icon{font-size:150%;line-height:1.125}.mat-form-field-prefix .mat-icon-button,.mat-form-field-suffix .mat-icon-button{height:1.5em;width:1.5em}.mat-form-field-prefix .mat-icon-button .mat-icon,.mat-form-field-suffix .mat-icon-button .mat-icon{height:1.125em;line-height:1.125}.mat-form-field-infix{padding:.5em 0;border-top:.84375em solid transparent}.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-1.34373em) scale(.75);transform:translateY(-1.34373em) scale(.75);width:133.3333533333%}.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.34372em) scale(.75);transform:translateY(-1.34372em) scale(.75);width:133.3333633333%}.mat-form-field-label-wrapper{top:-.84375em;padding-top:.84375em}.mat-form-field-label{top:1.34375em}.mat-form-field-underline{bottom:1.34375em}.mat-form-field-subscript-wrapper{font-size:75%;margin-top:.6666666667em;top:calc(100% - 1.7916666667em)}.mat-form-field-appearance-legacy .mat-form-field-wrapper{padding-bottom:1.25em}.mat-form-field-appearance-legacy .mat-form-field-infix{padding:.4375em 0}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-appearance-legacy.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00106px);transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00106px);-ms-transform:translateY(-1.28119em) scale(.75);width:133.3333933333%}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-form-field-autofill-control:-webkit-autofill+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00107px);transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00107px);-ms-transform:translateY(-1.28118em) scale(.75);width:133.3334033333%}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00108px);transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00108px);-ms-transform:translateY(-1.28117em) scale(.75);width:133.3334133333%}.mat-form-field-appearance-legacy .mat-form-field-label{top:1.28125em}.mat-form-field-appearance-legacy .mat-form-field-underline{bottom:1.25em}.mat-form-field-appearance-legacy .mat-form-field-subscript-wrapper{margin-top:.5416666667em;top:calc(100% - 1.6666666667em)}@media print{.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-appearance-legacy.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-1.28116em) scale(.75);transform:translateY(-1.28116em) scale(.75)}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-form-field-autofill-control:-webkit-autofill+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.28115em) scale(.75);transform:translateY(-1.28115em) scale(.75)}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.28114em) scale(.75);transform:translateY(-1.28114em) scale(.75)}}.mat-form-field-appearance-fill .mat-form-field-infix{padding:.25em 0 .75em}.mat-form-field-appearance-fill .mat-form-field-label{top:1.09375em;margin-top:-.5em}.mat-form-field-appearance-fill.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-appearance-fill.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-.59373em) scale(.75);transform:translateY(-.59373em) scale(.75);width:133.3333533333%}.mat-form-field-appearance-fill.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-.59372em) scale(.75);transform:translateY(-.59372em) scale(.75);width:133.3333633333%}.mat-form-field-appearance-outline .mat-form-field-infix{padding:1em 0}.mat-form-field-appearance-outline .mat-form-field-label{top:1.84375em;margin-top:-.25em}.mat-form-field-appearance-outline.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-appearance-outline.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-1.59373em) scale(.75);transform:translateY(-1.59373em) scale(.75);width:133.3333533333%}.mat-form-field-appearance-outline.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.59372em) scale(.75);transform:translateY(-1.59372em) scale(.75);width:133.3333633333%}.mat-grid-tile-footer,.mat-grid-tile-header{font-size:14px}.mat-grid-tile-footer .mat-line,.mat-grid-tile-header .mat-line{white-space:nowrap;overflow:hidden;text-overflow:ellipsis;display:block;box-sizing:border-box}.mat-grid-tile-footer .mat-line:nth-child(n+2),.mat-grid-tile-header .mat-line:nth-child(n+2){font-size:12px}input.mat-input-element{margin-top:-.0625em}.mat-menu-item{font-family:NunitoSans,sans-serif;font-size:14px;font-weight:400}.mat-paginator,.mat-paginator-page-size .mat-select-trigger{font-family:NunitoSans,sans-serif;font-size:12px}.mat-radio-button,.mat-select{font-family:NunitoSans,sans-serif}.mat-select-trigger{height:1.125em}.mat-slide-toggle-content{font-family:NunitoSans,sans-serif}.mat-slider-thumb-label-text{font-family:NunitoSans,sans-serif;font-size:12px;font-weight:500}.mat-stepper-horizontal,.mat-stepper-vertical{font-family:NunitoSans,sans-serif}.mat-step-label{font-size:14px;font-weight:400}.mat-step-sub-label-error{font-weight:400}.mat-step-label-error{font-size:14px}.mat-step-label-selected{font-size:14px;font-weight:500}.mat-tab-group{font-family:NunitoSans,sans-serif}.mat-tab-label,.mat-tab-link{font-family:NunitoSans,sans-serif;font-size:14px;font-weight:500}.mat-toolbar,.mat-toolbar h1,.mat-toolbar h2,.mat-toolbar h3,.mat-toolbar h4,.mat-toolbar h5,.mat-toolbar h6{font:500 20px/32px NunitoSans,sans-serif;margin:0}.mat-tooltip{font-family:NunitoSans,sans-serif;font-size:10px;padding-top:6px;padding-bottom:6px}.mat-tooltip-handset{font-size:14px;padding-top:8px;padding-bottom:8px}.mat-list-item,.mat-list-option{font-family:NunitoSans,sans-serif}.mat-list-base .mat-list-item{font-size:16px}.mat-list-base .mat-list-item .mat-line{white-space:nowrap;overflow:hidden;text-overflow:ellipsis;display:block;box-sizing:border-box}.mat-list-base .mat-list-item .mat-line:nth-child(n+2){font-size:14px}.mat-list-base .mat-list-option{font-size:16px}.mat-list-base .mat-list-option .mat-line{white-space:nowrap;overflow:hidden;text-overflow:ellipsis;display:block;box-sizing:border-box}.mat-list-base .mat-list-option .mat-line:nth-child(n+2){font-size:14px}.mat-list-base .mat-subheader{font-family:NunitoSans,sans-serif;font-size:14px;font-weight:500}.mat-list-base[dense] .mat-list-item{font-size:12px}.mat-list-base[dense] .mat-list-item .mat-line{white-space:nowrap;overflow:hidden;text-overflow:ellipsis;display:block;box-sizing:border-box}.mat-list-base[dense] .mat-list-item .mat-line:nth-child(n+2),.mat-list-base[dense] .mat-list-option{font-size:12px}.mat-list-base[dense] .mat-list-option .mat-line{white-space:nowrap;overflow:hidden;text-overflow:ellipsis;display:block;box-sizing:border-box}.mat-list-base[dense] .mat-list-option .mat-line:nth-child(n+2){font-size:12px}.mat-list-base[dense] .mat-subheader{font-family:NunitoSans,sans-serif;font-size:12px;font-weight:500}.mat-option{font-family:NunitoSans,sans-serif;font-size:16px}.mat-optgroup-label{font:500 14px/24px NunitoSans,sans-serif}.mat-simple-snackbar{font-family:NunitoSans,sans-serif;font-size:14px}.mat-simple-snackbar-action{line-height:1;font-family:inherit;font-size:inherit;font-weight:500}.mat-tree{font-family:NunitoSans,sans-serif}.mat-nested-tree-node,.mat-tree-node{font-weight:400;font-size:14px}.mat-ripple{overflow:hidden;position:relative}.mat-ripple.mat-ripple-unbounded{overflow:visible}.mat-ripple-element{position:absolute;border-radius:50%;pointer-events:none;-webkit-transition:opacity,-webkit-transform cubic-bezier(0,0,.2,1);transition:opacity,transform cubic-bezier(0,0,.2,1),-webkit-transform cubic-bezier(0,0,.2,1);-webkit-transform:scale(0);transform:scale(0)}@media (-ms-high-contrast:active){.mat-ripple-element{display:none}}.cdk-visually-hidden{border:0;clip:rect(0 0 0 0);height:1px;margin:-1px;overflow:hidden;padding:0;position:absolute;width:1px;outline:0;-webkit-appearance:none;-moz-appearance:none}.cdk-global-overlay-wrapper,.cdk-overlay-container{pointer-events:none;top:0;left:0;height:100%;width:100%}.cdk-overlay-container{position:fixed;z-index:1000}.cdk-overlay-container:empty{display:none}.cdk-global-overlay-wrapper{display:-webkit-box;display:flex;position:absolute;z-index:1000}.cdk-overlay-pane{position:absolute;pointer-events:auto;box-sizing:border-box;z-index:1000;display:-webkit-box;display:flex;max-width:100%;max-height:100%}.cdk-overlay-backdrop{position:absolute;top:0;bottom:0;left:0;right:0;z-index:1000;pointer-events:auto;-webkit-tap-highlight-color:transparent;-webkit-transition:opacity .4s cubic-bezier(.25,.8,.25,1);transition:opacity .4s cubic-bezier(.25,.8,.25,1);opacity:0}.cdk-overlay-backdrop.cdk-overlay-backdrop-showing{opacity:1}@media screen and (-ms-high-contrast:active){.cdk-overlay-backdrop.cdk-overlay-backdrop-showing{opacity:.6}}.cdk-overlay-dark-backdrop{background:rgba(0,0,0,.32)}.cdk-overlay-transparent-backdrop,.cdk-overlay-transparent-backdrop.cdk-overlay-backdrop-showing{opacity:0}.cdk-overlay-connected-position-bounding-box{position:absolute;z-index:1000;display:-webkit-box;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;flex-direction:column;min-width:1px;min-height:1px}.cdk-global-scrollblock{position:fixed;width:100%;overflow-y:scroll}@-webkit-keyframes cdk-text-field-autofill-start{/*!*/}@keyframes cdk-text-field-autofill-start{/*!*/}@-webkit-keyframes cdk-text-field-autofill-end{/*!*/}@keyframes cdk-text-field-autofill-end{/*!*/}.cdk-text-field-autofill-monitored:-webkit-autofill{-webkit-animation-name:cdk-text-field-autofill-start;animation-name:cdk-text-field-autofill-start}.cdk-text-field-autofill-monitored:not(:-webkit-autofill){-webkit-animation-name:cdk-text-field-autofill-end;animation-name:cdk-text-field-autofill-end}textarea.cdk-textarea-autosize{resize:none}textarea.cdk-textarea-autosize-measuring{height:auto!important;overflow:hidden!important;padding:2px 0!important;box-sizing:content-box!important}#date-time-container{display:-webkit-box;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;flex-direction:column;margin:0 auto}.mat-dialog-content{padding:0;margin:0;overflow:visible;max-height:80vh}#close-buttons{display:-webkit-box;display:flex;-webkit-box-orient:horizontal;-webkit-box-direction:normal;flex-direction:row;-webkit-box-pack:justify;justify-content:space-between}.bottom-buttons{width:148px}.date-time-button{border-radius:0;background-color:#fff}"]
            }] }
];
/** @nocollapse */
DateTimePickerComponent.ctorParameters = () => [
    { type: TimeService },
    { type: MatDialogRef$1 },
    { type: undefined, decorators: [{ type: Inject, args: [MAT_DIALOG_DATA$1,] }] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class DefinitiveTableErrorsComponent {
    constructor() {
        this.conflictLines = new EventEmitter();
        this._surveys = [];
        this.errors = [];
        this.trackingIdErrors = {};
        this.trackingIdKeys = [];
    }
    /**
     * @param {?} surveys
     * @return {?}
     */
    set definitiveSurveys(surveys) {
        if (!surveys || !surveys.length) {
            this.trackingIdKeys = [];
            this.trackingIdErrors = {};
            this.errors = [];
            return;
        }
        this._surveys = surveys;
        this.evaluateErrors();
    }
    /**
     * @return {?}
     */
    get definitiveSurveys() {
        return this._surveys;
    }
    /**
     * @private
     * @return {?}
     */
    evaluateErrors() {
        if (this.definitiveSurveys === null || !this.definitiveSurveys[0]) {
            return;
        }
        this.errors = [];
        // Todo: uncomment once the pipes are implemented
        // this.mdUnits = shorthandUnitsObject[this.definitiveSurveys[0].md.unit];
        /** @type {?} */
        const standardSurveys = this.definitiveSurveys.filter((/**
         * @param {?} survey
         * @return {?}
         */
        survey => (survey.type === 'STANDARD' || survey.type === 'POOR' || survey.type === 'DEFINITIVE_INTERPOLATED')));
        for (let i = 1; i < standardSurveys.length; i++) {
            if (standardSurveys[i].md.value === standardSurveys[i - 1].md.value) {
                /** @type {?} */
                const duplicatedSurvey = this.errors.find((/**
                 * @param {?} error
                 * @return {?}
                 */
                error => standardSurveys[i].md.value === error.md.value));
                if (duplicatedSurvey) {
                    duplicatedSurvey.counter++;
                }
                else {
                    this.errors.push({ md: standardSurveys[i].md, counter: 2 });
                }
            }
        }
        this.trackingIdErrors = {};
        this.trackingIdKeys = [];
        this.trackingIdErrors = standardSurveys.reduce((/**
         * @param {?} acc
         * @param {?} survey
         * @return {?}
         */
        (acc, survey) => {
            if (acc[survey['trackingId']]) {
                acc[survey['trackingId']]++;
            }
            else if (survey['trackingId']) {
                acc[survey['trackingId']] = 1;
            }
            return acc;
        }), {});
        this.trackingIdKeys = Object.keys(this.trackingIdErrors).filter((/**
         * @param {?} key
         * @return {?}
         */
        key => {
            if (this.trackingIdErrors[key] > 1) {
                return key;
            }
        }));
        /** @type {?} */
        let spaceUsed = 0;
        if (this.errors.length) {
            spaceUsed++;
        }
        if (this.trackingIdKeys.length && this.showTrackingIds) {
            spaceUsed++;
        }
        this.conflictLines.emit(spaceUsed);
    }
}
DefinitiveTableErrorsComponent.decorators = [
    { type: Component, args: [{
                selector: 'app-definitive-table-errors',
                template: "<div *ngIf=\"errors.length\" class=\"definitive-error-header md-errors\">\n  <span class=\"definitive-header-spacing definitive-error-msg\">Multiple D Type surveys at the following MD(s):</span>\n  <span class=\"definitive-error-msg definitive-header-spacing\" *ngFor=\"let error of errors\"> ({{ error.counter }}X) {{ error.md.value }} {{ mdUnits }}</span>\n</div>\n<div *ngIf=\"trackingIdKeys.length && showTrackingIds\" class=\"definitive-error-header\">\n  <span class=\"definitive-header-spacing definitive-error-msg\">Multiple D Type surveys with the following tracking ID's:</span>\n  <span class=\"definitive-error-msg definitive-header-spacing\" *ngFor=\"let trackingId of trackingIdKeys\">({{ trackingIdErrors[trackingId] }}X) {{ trackingId }}</span>\n</div>\n",
                styles: ["@media print{.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-appearance-legacy.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-1.28122em) scale(.75);transform:translateY(-1.28122em) scale(.75)}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-form-field-autofill-control:-webkit-autofill+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.28121em) scale(.75);transform:translateY(-1.28121em) scale(.75)}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.2812em) scale(.75);transform:translateY(-1.2812em) scale(.75)}}.mat-badge-content{font-weight:600;font-size:12px;font-family:NunitoSans,sans-serif}.mat-badge-small .mat-badge-content{font-size:9px}.mat-badge-large .mat-badge-content{font-size:24px}.mat-h1,.mat-headline,.mat-typography h1{font:400 24px/32px NunitoSans,sans-serif;margin:0 0 16px}.mat-h2,.mat-title,.mat-typography h2{font:500 20px/32px NunitoSans,sans-serif;margin:0 0 16px}.mat-h3,.mat-subheading-2,.mat-typography h3{font:400 16px/28px NunitoSans,sans-serif;margin:0 0 16px}.mat-h4,.mat-subheading-1,.mat-typography h4{font:400 15px/24px NunitoSans,sans-serif;margin:0 0 16px}.mat-h5,.mat-h6,.mat-typography h5,.mat-typography h6{margin:0 0 12px}.mat-body-2,.mat-body-strong{font:500 14px/24px NunitoSans,sans-serif}.mat-body,.mat-body-1,.mat-typography{font:400 14px/20px NunitoSans,sans-serif}.mat-body p,.mat-body-1 p,.mat-typography p{margin:0 0 12px}.mat-caption,.mat-small{font:400 12px/20px NunitoSans,sans-serif}.mat-display-4,.mat-typography .mat-display-4{font:300 112px/112px NunitoSans,sans-serif;letter-spacing:-.05em;margin:0 0 56px}.mat-display-3,.mat-typography .mat-display-3{font:400 56px/56px NunitoSans,sans-serif;letter-spacing:-.02em;margin:0 0 64px}.mat-display-2,.mat-typography .mat-display-2{font:400 45px/48px NunitoSans,sans-serif;letter-spacing:-.005em;margin:0 0 64px}.mat-display-1,.mat-typography .mat-display-1{font:400 34px/40px NunitoSans,sans-serif;margin:0 0 64px}.mat-bottom-sheet-container{font:400 14px/20px NunitoSans,sans-serif}.mat-button,.mat-fab,.mat-flat-button,.mat-icon-button,.mat-mini-fab,.mat-raised-button,.mat-stroked-button{font-family:NunitoSans,sans-serif;font-size:14px;font-weight:500}.mat-button-toggle,.mat-card{font-family:NunitoSans,sans-serif}.mat-card-title{font-size:24px;font-weight:500}.mat-card-header .mat-card-title{font-size:20px}.mat-card-content,.mat-card-subtitle{font-size:14px}.mat-checkbox{font-family:NunitoSans,sans-serif}.mat-checkbox-layout .mat-checkbox-label{line-height:24px}.mat-chip{font-size:14px;font-weight:500}.mat-chip .mat-chip-remove.mat-icon,.mat-chip .mat-chip-trailing-icon.mat-icon{font-size:18px}.mat-table{font-family:NunitoSans,sans-serif}.mat-header-cell{font-size:12px;font-weight:500}.mat-cell,.mat-footer-cell{font-size:14px}.mat-calendar{font-family:NunitoSans,sans-serif}.mat-calendar-body{font-size:13px}.mat-calendar-body-label,.mat-calendar-period-button{font-size:14px;font-weight:500}.mat-calendar-table-header th{font-size:11px;font-weight:400}.mat-dialog-title{font:500 20px/32px NunitoSans,sans-serif}.mat-expansion-panel-header{font-family:NunitoSans,sans-serif;font-size:15px;font-weight:400}.mat-expansion-panel-content{font:400 14px/20px NunitoSans,sans-serif}.mat-form-field{font-size:inherit;font-weight:400;line-height:1.125;font-family:NunitoSans,sans-serif}.mat-form-field-wrapper{padding-bottom:1.34375em}.mat-form-field-prefix .mat-icon,.mat-form-field-suffix .mat-icon{font-size:150%;line-height:1.125}.mat-form-field-prefix .mat-icon-button,.mat-form-field-suffix .mat-icon-button{height:1.5em;width:1.5em}.mat-form-field-prefix .mat-icon-button .mat-icon,.mat-form-field-suffix .mat-icon-button .mat-icon{height:1.125em;line-height:1.125}.mat-form-field-infix{padding:.5em 0;border-top:.84375em solid transparent}.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-1.34373em) scale(.75);transform:translateY(-1.34373em) scale(.75);width:133.3333533333%}.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.34372em) scale(.75);transform:translateY(-1.34372em) scale(.75);width:133.3333633333%}.mat-form-field-label-wrapper{top:-.84375em;padding-top:.84375em}.mat-form-field-label{top:1.34375em}.mat-form-field-underline{bottom:1.34375em}.mat-form-field-subscript-wrapper{font-size:75%;margin-top:.6666666667em;top:calc(100% - 1.7916666667em)}.mat-form-field-appearance-legacy .mat-form-field-wrapper{padding-bottom:1.25em}.mat-form-field-appearance-legacy .mat-form-field-infix{padding:.4375em 0}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-appearance-legacy.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00106px);transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00106px);-ms-transform:translateY(-1.28119em) scale(.75);width:133.3333933333%}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-form-field-autofill-control:-webkit-autofill+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00107px);transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00107px);-ms-transform:translateY(-1.28118em) scale(.75);width:133.3334033333%}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00108px);transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00108px);-ms-transform:translateY(-1.28117em) scale(.75);width:133.3334133333%}.mat-form-field-appearance-legacy .mat-form-field-label{top:1.28125em}.mat-form-field-appearance-legacy .mat-form-field-underline{bottom:1.25em}.mat-form-field-appearance-legacy .mat-form-field-subscript-wrapper{margin-top:.5416666667em;top:calc(100% - 1.6666666667em)}@media print{.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-appearance-legacy.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-1.28116em) scale(.75);transform:translateY(-1.28116em) scale(.75)}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-form-field-autofill-control:-webkit-autofill+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.28115em) scale(.75);transform:translateY(-1.28115em) scale(.75)}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.28114em) scale(.75);transform:translateY(-1.28114em) scale(.75)}}.mat-form-field-appearance-fill .mat-form-field-infix{padding:.25em 0 .75em}.mat-form-field-appearance-fill .mat-form-field-label{top:1.09375em;margin-top:-.5em}.mat-form-field-appearance-fill.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-appearance-fill.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-.59373em) scale(.75);transform:translateY(-.59373em) scale(.75);width:133.3333533333%}.mat-form-field-appearance-fill.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-.59372em) scale(.75);transform:translateY(-.59372em) scale(.75);width:133.3333633333%}.mat-form-field-appearance-outline .mat-form-field-infix{padding:1em 0}.mat-form-field-appearance-outline .mat-form-field-label{top:1.84375em;margin-top:-.25em}.mat-form-field-appearance-outline.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-appearance-outline.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-1.59373em) scale(.75);transform:translateY(-1.59373em) scale(.75);width:133.3333533333%}.mat-form-field-appearance-outline.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.59372em) scale(.75);transform:translateY(-1.59372em) scale(.75);width:133.3333633333%}.mat-grid-tile-footer,.mat-grid-tile-header{font-size:14px}.mat-grid-tile-footer .mat-line,.mat-grid-tile-header .mat-line{white-space:nowrap;overflow:hidden;text-overflow:ellipsis;display:block;box-sizing:border-box}.mat-grid-tile-footer .mat-line:nth-child(n+2),.mat-grid-tile-header .mat-line:nth-child(n+2){font-size:12px}input.mat-input-element{margin-top:-.0625em}.mat-menu-item{font-family:NunitoSans,sans-serif;font-size:14px;font-weight:400}.mat-paginator,.mat-paginator-page-size .mat-select-trigger{font-family:NunitoSans,sans-serif;font-size:12px}.mat-radio-button,.mat-select{font-family:NunitoSans,sans-serif}.mat-select-trigger{height:1.125em}.mat-slide-toggle-content{font-family:NunitoSans,sans-serif}.mat-slider-thumb-label-text{font-family:NunitoSans,sans-serif;font-size:12px;font-weight:500}.mat-stepper-horizontal,.mat-stepper-vertical{font-family:NunitoSans,sans-serif}.mat-step-label{font-size:14px;font-weight:400}.mat-step-sub-label-error{font-weight:400}.mat-step-label-error{font-size:14px}.mat-step-label-selected{font-size:14px;font-weight:500}.mat-tab-group{font-family:NunitoSans,sans-serif}.mat-tab-label,.mat-tab-link{font-family:NunitoSans,sans-serif;font-size:14px;font-weight:500}.mat-toolbar,.mat-toolbar h1,.mat-toolbar h2,.mat-toolbar h3,.mat-toolbar h4,.mat-toolbar h5,.mat-toolbar h6{font:500 20px/32px NunitoSans,sans-serif;margin:0}.mat-tooltip{font-family:NunitoSans,sans-serif;font-size:10px;padding-top:6px;padding-bottom:6px}.mat-tooltip-handset{font-size:14px;padding-top:8px;padding-bottom:8px}.mat-list-item,.mat-list-option{font-family:NunitoSans,sans-serif}.mat-list-base .mat-list-item{font-size:16px}.mat-list-base .mat-list-item .mat-line{white-space:nowrap;overflow:hidden;text-overflow:ellipsis;display:block;box-sizing:border-box}.mat-list-base .mat-list-item .mat-line:nth-child(n+2){font-size:14px}.mat-list-base .mat-list-option{font-size:16px}.mat-list-base .mat-list-option .mat-line{white-space:nowrap;overflow:hidden;text-overflow:ellipsis;display:block;box-sizing:border-box}.mat-list-base .mat-list-option .mat-line:nth-child(n+2){font-size:14px}.mat-list-base .mat-subheader{font-family:NunitoSans,sans-serif;font-size:14px;font-weight:500}.mat-list-base[dense] .mat-list-item{font-size:12px}.mat-list-base[dense] .mat-list-item .mat-line{white-space:nowrap;overflow:hidden;text-overflow:ellipsis;display:block;box-sizing:border-box}.mat-list-base[dense] .mat-list-item .mat-line:nth-child(n+2),.mat-list-base[dense] .mat-list-option{font-size:12px}.mat-list-base[dense] .mat-list-option .mat-line{white-space:nowrap;overflow:hidden;text-overflow:ellipsis;display:block;box-sizing:border-box}.mat-list-base[dense] .mat-list-option .mat-line:nth-child(n+2){font-size:12px}.mat-list-base[dense] .mat-subheader{font-family:NunitoSans,sans-serif;font-size:12px;font-weight:500}.mat-option{font-family:NunitoSans,sans-serif;font-size:16px}.mat-optgroup-label{font:500 14px/24px NunitoSans,sans-serif}.mat-simple-snackbar{font-family:NunitoSans,sans-serif;font-size:14px}.mat-simple-snackbar-action{line-height:1;font-family:inherit;font-size:inherit;font-weight:500}.mat-tree{font-family:NunitoSans,sans-serif}.mat-nested-tree-node,.mat-tree-node{font-weight:400;font-size:14px}.mat-ripple{overflow:hidden;position:relative}.mat-ripple.mat-ripple-unbounded{overflow:visible}.mat-ripple-element{position:absolute;border-radius:50%;pointer-events:none;-webkit-transition:opacity,-webkit-transform cubic-bezier(0,0,.2,1);transition:opacity,transform cubic-bezier(0,0,.2,1),-webkit-transform cubic-bezier(0,0,.2,1);-webkit-transform:scale(0);transform:scale(0)}@media (-ms-high-contrast:active){.mat-ripple-element{display:none}}.cdk-visually-hidden{border:0;clip:rect(0 0 0 0);height:1px;margin:-1px;overflow:hidden;padding:0;position:absolute;width:1px;outline:0;-webkit-appearance:none;-moz-appearance:none}.cdk-global-overlay-wrapper,.cdk-overlay-container{pointer-events:none;top:0;left:0;height:100%;width:100%}.cdk-overlay-container{position:fixed;z-index:1000}.cdk-overlay-container:empty{display:none}.cdk-global-overlay-wrapper{display:-webkit-box;display:flex;position:absolute;z-index:1000}.cdk-overlay-pane{position:absolute;pointer-events:auto;box-sizing:border-box;z-index:1000;display:-webkit-box;display:flex;max-width:100%;max-height:100%}.cdk-overlay-backdrop{position:absolute;top:0;bottom:0;left:0;right:0;z-index:1000;pointer-events:auto;-webkit-tap-highlight-color:transparent;-webkit-transition:opacity .4s cubic-bezier(.25,.8,.25,1);transition:opacity .4s cubic-bezier(.25,.8,.25,1);opacity:0}.cdk-overlay-backdrop.cdk-overlay-backdrop-showing{opacity:1}@media screen and (-ms-high-contrast:active){.cdk-overlay-backdrop.cdk-overlay-backdrop-showing{opacity:.6}}.cdk-overlay-dark-backdrop{background:rgba(0,0,0,.32)}.cdk-overlay-transparent-backdrop,.cdk-overlay-transparent-backdrop.cdk-overlay-backdrop-showing{opacity:0}.cdk-overlay-connected-position-bounding-box{position:absolute;z-index:1000;display:-webkit-box;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;flex-direction:column;min-width:1px;min-height:1px}.cdk-global-scrollblock{position:fixed;width:100%;overflow-y:scroll}@-webkit-keyframes cdk-text-field-autofill-start{/*!*/}@keyframes cdk-text-field-autofill-start{/*!*/}@-webkit-keyframes cdk-text-field-autofill-end{/*!*/}@keyframes cdk-text-field-autofill-end{/*!*/}.cdk-text-field-autofill-monitored:-webkit-autofill{-webkit-animation-name:cdk-text-field-autofill-start;animation-name:cdk-text-field-autofill-start}.cdk-text-field-autofill-monitored:not(:-webkit-autofill){-webkit-animation-name:cdk-text-field-autofill-end;animation-name:cdk-text-field-autofill-end}textarea.cdk-textarea-autosize{resize:none}textarea.cdk-textarea-autosize-measuring{height:auto!important;overflow:hidden!important;padding:2px 0!important;box-sizing:content-box!important}.definitive-header-spacing{margin-left:5px}.definitive-error-msg{color:#f5ff8b}.definitive-error-header{margin-left:5px}"]
            }] }
];
/** @nocollapse */
DefinitiveTableErrorsComponent.ctorParameters = () => [];
DefinitiveTableErrorsComponent.propDecorators = {
    showTrackingIds: [{ type: Input }],
    definitiveSurveys: [{ type: Input, args: ['definitiveSurveys',] }],
    conflictLines: [{ type: Output }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class CapitalizePipe {
    /**
     * @param {?} value
     * @return {?}
     */
    transform(value) {
        if (value) {
            return value.charAt(0).toUpperCase() + value.slice(1);
        }
        return value;
    }
}
CapitalizePipe.decorators = [
    { type: Pipe, args: [{ name: 'capitalize' },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class EllipsisizePipe {
    /**
     * @param {?} value
     * @param {?} desiredLengthIncludingElip
     * @return {?}
     */
    transform(value, desiredLengthIncludingElip) {
        if (value && value.length > desiredLengthIncludingElip) {
            // TODO: need to make this clickable to show the whole string or something
            return value.substring(0, desiredLengthIncludingElip - 3) + '...';
        }
        return value;
    }
}
EllipsisizePipe.decorators = [
    { type: Pipe, args: [{ name: 'ellipsisize' },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class GravityUnitPipe {
    /**
     * @param {?} unit
     * @param {?=} full
     * @return {?}
     */
    transform(unit, full) {
        /** @type {?} */
        let string = '';
        switch (unit) {
            case 'M_PER_SEC_SQ':
                string = (full === true ? 'Meters per Second Sq.' : 'm/s' + decodeURI('%C2%B2'));
                break;
            case 'FT_PER_SEC_SQ':
                string = (full === true ? 'Feet per Second Sq.' : 'f/s' + decodeURI('%C2%B2'));
                break;
            case 'G':
                string = 'G';
                break;
            case 'MILLI_G':
                string = 'mG';
                break;
            case 'G_98':
                string = 'G (9.8)';
                break;
            case 'GAL':
                string = 'Gal';
                break;
            case 'MILLI_GAL':
                string = 'mGal';
                break;
            default:
                string = unit;
                break;
        }
        return string;
    }
}
GravityUnitPipe.decorators = [
    { type: Pipe, args: [{
                name: 'gravityUnit'
            },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class WellboreStatusPipe {
    /**
     * @param {?} value
     * @param {?=} args
     * @return {?}
     */
    transform(value, args) {
        switch (value) {
            case 'ACTIVE_BATCH_PROCESS':
                return 'Active - Batch Process';
            case 'ACTIVE_REALTIME':
                return 'Active - Realtime';
            case 'PRE_OPERATIONS':
                return 'Pre-Operations';
            case 'COMPLETED':
                return 'Completed';
            default:
                return 'Unknown status';
        }
    }
}
WellboreStatusPipe.decorators = [
    { type: Pipe, args: [{
                name: 'wellboreStatus'
            },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class AngleUnitPipe {
    /**
     * @param {?} unit
     * @param {?=} full
     * @return {?}
     */
    transform(unit, full) {
        /** @type {?} */
        let string = '';
        switch (unit) {
            case 'DEGREE':
                string = (full === true ? 'Degrees' : decodeURI('%C2%B0'));
                break;
            case 'RADIAN':
                string = (full === true ? 'Radians' : 'rad');
                break;
            case 'GRADIAN':
                string = (full === true ? 'Gradians' : 'grad');
                break;
            default:
                string = unit;
                break;
        }
        return string;
    }
}
AngleUnitPipe.decorators = [
    { type: Pipe, args: [{
                name: 'angleUnit'
            },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class MagneticUnitPipe {
    /**
     * @param {?} unit
     * @param {?=} full
     * @return {?}
     */
    transform(unit, full) {
        /** @type {?} */
        let string = '';
        switch (unit) {
            case 'NANOTESLA':
                string = (full === true ? 'Nanotesla' : 'nT');
                break;
            case 'MICROTESLA':
                string = (full === true ? 'Microtesla' : decodeURI('%C2%B5') + 'T');
                break;
            case 'MILLITESLA':
                string = (full === true ? 'Millitesla' : 'mT');
                break;
            case 'TESLA':
                string = (full === true ? 'Tesla' : 'T');
                break;
            case 'GAUSS':
                string = (full === true ? 'Gauss' : 'Gs');
                break;
            case 'GAMMA':
                string = (full === true ? 'Gamma' : decodeURI('%CE%B3'));
                break;
            default:
                string = unit;
                break;
        }
        return string;
    }
}
MagneticUnitPipe.decorators = [
    { type: Pipe, args: [{
                name: 'magneticUnit'
            },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class DistanceUnitPipe {
    /**
     * @param {?} unit
     * @param {?=} full
     * @return {?}
     */
    transform(unit, full) {
        /** @type {?} */
        let string = '';
        switch (unit) {
            case 'METER':
                string = 'Meters';
                break;
            case 'KILOMETER':
                string = 'Kilometers';
                break;
            case 'FOOT':
                string = 'Feet';
                break;
            case 'FOOT_US':
                string = 'ft US';
                break;
            case 'YARD':
                string = 'Yards';
                break;
            case 'MILE':
                string = 'Miles';
                break;
            default:
                string = unit;
                break;
        }
        return string;
    }
}
DistanceUnitPipe.decorators = [
    { type: Pipe, args: [{
                name: 'distanceUnit'
            },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
// A simple filter that converts 'true' to 'On' and 'false' to 'Off'.
class BooleanOnOffPipe {
    /**
     * @param {?} value
     * @return {?}
     */
    transform(value) {
        if (value) {
            return 'On';
        }
        else {
            return 'Off';
        }
    }
}
BooleanOnOffPipe.decorators = [
    { type: Pipe, args: [{
                name: 'booleanOnOff'
            },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
// A filter to convert a moment to a human readable string
// of the format "X seconds ago", "Y minutes ago", "4 months ago"
class TimeAgoPipe {
    /**
     * @param {?} time
     * @return {?}
     */
    transform(time) {
        /** @type {?} */
        const moment = moment_;
        /** @type {?} */
        const now = moment();
        if (time && moment.isMoment(time)) {
            if (time > now) {
                return 'Now';
            }
            else {
                return time.fromNow();
            }
        }
        else {
            return 'Never';
        }
    }
}
TimeAgoPipe.decorators = [
    { type: Pipe, args: [{
                name: 'timeAgo'
            },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class SurveySetStatePipe {
    /**
     * @param {?} value
     * @return {?}
     */
    transform(value) {
        switch (value) {
            case 'WAITING_SURVEY_SUBMIT':
                return 'Waiting for a new survey';
            case 'WAITING_CORRECTION':
                return 'Waiting for a correction';
            case 'WAITING_VALIDATION':
                return 'Waiting for validation';
            default:
                return 'Unknown';
        }
    }
}
SurveySetStatePipe.decorators = [
    { type: Pipe, args: [{
                name: 'surveySetState'
            },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
// Returns the distance as a string with an amount of decimal places based on its unit.
// Also formats the string with commas for readability.
class DistanceDecimalPipe {
    constructor() {
        // number of decimal places to display for various distance units.
        this.decimalMap = {
            // Old Format
            METER: 2,
            KILOMETER: 1,
            FOOT: 2,
            FOOT_US: 2,
            YARD: 2,
            MILE: 1,
            // New Format
            Meters: 2,
            Kilometers: 1,
            Feet: 2,
            'ft US': 2,
            'U.S. Survey Feet': 2,
            Yards: 2,
            Miles: 1,
        };
    }
    /**
     * @param {?} value
     * @param {?=} unit
     * @return {?}
     */
    transform(value, unit) {
        if (!this.isValidFloat(value)) {
            return 'N/A';
        }
        else {
            value = parseFloat(value);
            if (typeof (this.decimalMap[unit]) !== 'undefined') {
                return this.formatNumber(value.toFixed(this.decimalMap[unit]));
            }
            else {
                return 'Unsupported Unit: ' + unit;
            }
        }
    }
    /**
     * isValidFloat:
     *
     * A stricter float filter than parseFloat().
     *
     * @param {?} value Input value to be parsed as float
     * @return {?} float
     */
    isValidFloat(value) {
        return !isNaN(parseFloat(value));
    }
    /**
     * formatNumber:
     *
     * Adds commas and removes the negative sign for zero values.
     *
     * @param {?} number Input value to be formatted
     * @return {?}
     */
    formatNumber(number) {
        return this.removeSignOnZero(this.addCommas(number));
    }
    /**
     * removeSignOnZero:
     *
     * If the value is zero, remove any negative signs. (converts -0.00 to 0.00)
     *
     * @param {?} number Input value to be sanitized
     * @return {?}
     */
    removeSignOnZero(number) {
        /** @type {?} */
        const split = number.toString().split('-');
        if (split.length === 2) {
            if (split[1] === 0) {
                return split[1];
            }
        }
        return number;
    }
    /**
     * addCommas:
     *
     * Helper function that adds in commas to a number for readability.
     *
     * @param {?} number Input value to add commas
     * @return {?} string
     */
    addCommas(number) {
        // check if we have a decimal
        /** @type {?} */
        const split = number.toString().split('.');
        // if we have a decimal
        if (split.length === 2) {
            // only add commas to the first half and return the second half
            return split[0].replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,') + '.' + split[1];
        }
        else {
            // add commas throughout
            return split[0].replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
        }
    }
}
DistanceDecimalPipe.decorators = [
    { type: Pipe, args: [{
                name: 'distanceDecimal'
            },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
// Returns the angle as a string with an amount of decimal places based on its unit.
// Also formats the string with commas for readability.
class AngleDecimalPipe {
    constructor() {
        // number of decimal places to display for various angle units
        this.decimalMap = {
            DEGREE: 2,
            RADIAN: 2,
            GRADIAN: 2,
            Degrees: 2,
            '°': 2,
            Radians: 2,
            rad: 2,
            Gradians: 2,
            grad: 2
        };
    }
    /**
     * @param {?} value
     * @param {?=} unit
     * @return {?}
     */
    transform(value, unit) {
        if (!this.isValidFloat(value)) {
            return 'N/A';
        }
        else {
            value = parseFloat(value);
            if (typeof (this.decimalMap[unit]) !== 'undefined') {
                return this.formatNumber(value.toFixed(this.decimalMap[unit]));
            }
            else {
                return 'Unsupported Unit: ' + unit;
            }
        }
    }
    /**
     * isValidFloat:
     *
     * A stricter float filter than parseFloat().
     *
     * @param {?} value Input value to be parsed as float
     * @return {?} float
     */
    isValidFloat(value) {
        return !isNaN(parseFloat(value));
    }
    /**
     * formatNumber:
     *
     * Adds commas and removes the negative sign for zero values.
     *
     * @param {?} number Input value to be formatted
     * @return {?}
     */
    formatNumber(number) {
        return this.removeSignOnZero(this.addCommas(number));
    }
    /**
     * removeSignOnZero:
     *
     * If the value is zero, remove any negative signs. (converts -0.00 to 0.00)
     *
     * @param {?} number Input value to be sanitized
     * @return {?}
     */
    removeSignOnZero(number) {
        /** @type {?} */
        const split = number.toString().split('-');
        if (split.length === 2) {
            if (split[1] === 0) {
                return split[1];
            }
        }
        return number;
    }
    /**
     * addCommas:
     *
     * Helper function that adds in commas to a number for readability.
     *
     * @param {?} number Input value to add commas
     * @return {?} string
     */
    addCommas(number) {
        // check if we have a decimal
        /** @type {?} */
        const split = number.toString().split('.');
        // if we have a decimal
        if (split.length === 2) {
            // only add commas to the first half and return the second half
            return split[0].replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,') + '.' + split[1];
        }
        else {
            // add commas throughout
            return split[0].replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
        }
    }
}
AngleDecimalPipe.decorators = [
    { type: Pipe, args: [{
                name: 'angleDecimal'
            },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
// Returns the gravity value as a string with an amount of decimal places based on its unit.
// Also formats the string with commas for readability.
class GravityDecimalPipe {
    constructor() {
        // number of decimal places to display for various gravity units
        this.decimalMap = {
            M_PER_SEC_SQ: 3,
            FT_PER_SEC_SQ: 3,
            G: 4,
            MILLI_G: 2,
            G_98: 1,
            GAL: 4,
            MILLI_GAL: 1,
            'Meters per Second Sq.': 3,
            'm/s²': 3,
            'f/s²': 3,
            mG: 2,
            'G (9.8)': 1,
            Gal: 4,
            mGal: 1,
        };
    }
    /**
     * @param {?} value
     * @param {?=} unit
     * @return {?}
     */
    transform(value, unit) {
        if (!this.isValidFloat(value)) {
            return 'N/A';
        }
        else {
            value = parseFloat(value);
            if (typeof (this.decimalMap[unit]) !== 'undefined') {
                return this.formatNumber(value.toFixed(this.decimalMap[unit]));
            }
            else {
                return 'Unsupported Unit: ' + unit;
            }
        }
    }
    /**
     * isValidFloat:
     *
     * A stricter float filter than parseFloat().
     *
     * @param {?} value Input value to be parsed as float
     * @return {?} float
     */
    isValidFloat(value) {
        return !isNaN(parseFloat(value));
    }
    /**
     * formatNumber:
     *
     * Adds commas and removes the negative sign for zero values.
     *
     * @param {?} number Input value to be formatted
     * @return {?}
     */
    formatNumber(number) {
        return this.removeSignOnZero(this.addCommas(number));
    }
    /**
     * removeSignOnZero:
     *
     * If the value is zero, remove any negative signs. (converts -0.00 to 0.00)
     *
     * @param {?} number Input value to be sanitized
     * @return {?}
     */
    removeSignOnZero(number) {
        /** @type {?} */
        const split = number.toString().split('-');
        if (split.length === 2) {
            if (split[1] === 0) {
                return split[1];
            }
        }
        return number;
    }
    /**
     * addCommas:
     *
     * Helper function that adds in commas to a number for readability.
     *
     * @param {?} number Input value to add commas
     * @return {?} string
     */
    addCommas(number) {
        // check if we have a decimal
        /** @type {?} */
        const split = number.toString().split('.');
        // if we have a decimal
        if (split.length === 2) {
            // only add commas to the first half and return the second half
            return split[0].replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,') + '.' + split[1];
        }
        else {
            // add commas throughout
            return split[0].replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
        }
    }
}
GravityDecimalPipe.decorators = [
    { type: Pipe, args: [{
                name: 'gravityDecimal'
            },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
// Returns the angle as a string with an amount of decimal places based on its unit.
// Also formats the string with commas for readability.
class MagneticDecimalPipe {
    constructor() {
        // number of decimal places to display for various angle units
        this.decimalMap = {
            // Old Format
            NANOTESLA: 0,
            MICROTESLA: 4,
            MILLITESLA: 0,
            TESLA: 0,
            GAUSS: 5,
            GAMMA: 0,
            // New Format
            Nanotesla: 0,
            nT: 0,
            Microtesla: 4,
            'µT': 4,
            Millitesla: 0,
            mT: 0,
            Tesla: 0,
            T: 0,
            Gauss: 5,
            Gs: 5,
            Gamma: 0,
            'γ': 0
        };
    }
    /**
     * @param {?} value
     * @param {?=} unit
     * @return {?}
     */
    transform(value, unit) {
        if (!this.isValidFloat(value)) {
            return 'N/A';
        }
        else {
            value = parseFloat(value);
            if (typeof (this.decimalMap[unit]) !== 'undefined') {
                return this.formatNumber(value.toFixed(this.decimalMap[unit]));
            }
            else {
                return 'Unsupported Unit: ' + unit;
            }
        }
    }
    /**
     * isValidFloat:
     *
     * A stricter float filter than parseFloat().
     *
     * @param {?} value Input value to be parsed as float
     * @return {?} float
     */
    isValidFloat(value) {
        return !isNaN(parseFloat(value));
    }
    /**
     * formatNumber:
     *
     * Adds commas and removes the negative sign for zero values.
     *
     * @param {?} number Input value to be formatted
     * @return {?}
     */
    formatNumber(number) {
        return this.removeSignOnZero(this.addCommas(number));
    }
    /**
     * removeSignOnZero:
     *
     * If the value is zero, remove any negative signs. (converts -0.00 to 0.00)
     *
     * @param {?} number Input value to be sanitized
     * @return {?}
     */
    removeSignOnZero(number) {
        /** @type {?} */
        const split = number.toString().split('-');
        if (split.length === 2) {
            if (split[1] === 0) {
                return split[1];
            }
        }
        return number;
    }
    /**
     * addCommas:
     *
     * Helper function that adds in commas to a number for readability.
     *
     * @param {?} number Input value to add commas
     * @return {?} string
     */
    addCommas(number) {
        // check if we have a decimal
        /** @type {?} */
        const split = number.toString().split('.');
        // if we have a decimal
        if (split.length === 2) {
            // only add commas to the first half and return the second half
            return split[0].replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,') + '.' + split[1];
        }
        else {
            // add commas throughout
            return split[0].replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
        }
    }
}
MagneticDecimalPipe.decorators = [
    { type: Pipe, args: [{
                name: 'magneticDecimal'
            },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class ResolvedStatePipe {
    // A simple filter that converts resolved state enums to friendlier string values.
    /**
     * @param {?} resolution
     * @param {?=} value
     * @return {?}
     */
    transform(resolution, value) {
        if (resolution === 'RESOLVED') {
            return 'Resolved';
        }
        else if (resolution === 'UNRESOLVED') {
            return 'Unresolved';
        }
        else {
            return 'Manually Set : ' + value;
        }
    }
}
ResolvedStatePipe.decorators = [
    { type: Pipe, args: [{
                name: 'resolvedState'
            },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
// Converts between survey types and abbreviations.
class SurveyTypePipe {
    constructor() {
        this.surveyTypes = {
            'STANDARD': {
                definitive: true,
                interpolated: false,
                acronym: 'D-S'
            },
            'POOR': {
                definitive: true,
                interpolated: false,
                acronym: 'D-P'
            },
            'DEFINITIVE_INTERPOLATED': {
                definitive: true,
                interpolated: true,
                acronym: 'D-I'
            },
            'BAD': {
                definitive: false,
                interpolated: false,
                acronym: 'X-B'
            },
            'ACC_CHECK': {
                definitive: false,
                interpolated: false,
                acronym: 'X-A'
            },
            'CHECKSHOT': {
                definitive: false,
                interpolated: false,
                acronym: 'X-C'
            },
            'INTERPOLATED': {
                definitive: false,
                interpolated: false,
                acronym: 'X-I'
            }
        };
    }
    /**
     * @param {?} value
     * @return {?}
     */
    transform(value) {
        if (this.surveyTypes[value]) {
            return this.surveyTypes[value].acronym;
        }
        else {
            return 'Unsupported Type';
        }
    }
}
SurveyTypePipe.decorators = [
    { type: Pipe, args: [{
                name: 'surveyType'
            },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class DefinitiveSurveysPipe {
    constructor() {
        this.surveyTypes = {
            'STANDARD': {
                definitive: true,
                interpolated: false,
                acronym: 'D-S'
            },
            'POOR': {
                definitive: true,
                interpolated: false,
                acronym: 'D-P'
            },
            'DEFINITIVE_INTERPOLATED': {
                definitive: true,
                interpolated: true,
                acronym: 'D-I'
            },
            'BAD': {
                definitive: false,
                interpolated: false,
                acronym: 'X-B'
            },
            'ACC_CHECK': {
                definitive: false,
                interpolated: false,
                acronym: 'X-A'
            },
            'CHECKSHOT': {
                definitive: false,
                interpolated: false,
                acronym: 'X-C'
            },
            'INTERPOLATED': {
                definitive: false,
                interpolated: false,
                acronym: 'X-I'
            }
        };
    }
    /**
     * @param {?} surveys
     * @param {?=} includeInterpolated
     * @return {?}
     */
    transform(surveys, includeInterpolated) {
        if (surveys) {
            return surveys.filter((/**
             * @param {?} survey
             * @return {?}
             */
            survey => {
                if (includeInterpolated) {
                    return this.surveyTypes[survey.type] && this.surveyTypes[survey.type].definitive;
                }
                else {
                    return this.surveyTypes[survey.type] && this.surveyTypes[survey.type].definitive && !this.surveyTypes[survey.type].interpolated;
                }
            }));
        }
        else {
            return surveys;
        }
    }
}
DefinitiveSurveysPipe.decorators = [
    { type: Pipe, args: [{
                name: 'definitiveSurveys'
            },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
// Returns a string representation of the measure with proper decimal places and formatting.
class MeasurePipe {
    /**
     * @param {?} measure
     * @param {?=} displayUnit
     * @return {?}
     */
    transform(measure, displayUnit) {
        /** @type {?} */
        let value;
        /** @type {?} */
        let unit;
        // default: display units
        if (displayUnit === undefined) {
            displayUnit = true;
        }
        // Check if measure is undefined or unit is undefined
        if (measure === undefined || measure.unit === undefined) {
            return 'Unsupported Input for Measure Filter';
        }
        switch (measure.unit) {
            // Old Format
            case 'METER':
            case 'KILOMETER':
            case 'FOOT':
            case 'FOOT_US':
            case 'YARD':
            case 'MILE':
            // New Format
            case 'Meters':
            case 'Kilometers':
            case 'Feet':
            case 'ft US':
            case 'U.S. Survey Feet':
            case 'Yards':
            case 'Miles':
                value = new DistanceDecimalPipe().transform(measure.value, measure.unit);
                unit = ' ' + new DistanceUnitPipe().transform(measure.unit);
                break;
            // Old Format
            case 'DEGREE':
            case 'RADIAN':
            case 'GRADIAN':
            // New Format
            case 'Degrees':
            case '°':
            case 'Radians':
            case 'rad':
            case 'Gradians':
            case 'grad':
                value = new AngleDecimalPipe().transform(measure.value, measure.unit);
                unit = ' ' + new AngleUnitPipe().transform(measure.unit);
                break;
            // Old Format
            case 'M_PER_SEC_SQ':
            case 'FT_PER_SEC_SQ':
            case 'G':
            case 'MILLI_G':
            case 'G_98':
            case 'GAL':
            case 'MILLI_GAL':
            // New Format
            case 'Meters per Second Sq.':
            case 'Feet per Second Sq.':
            case 'm/s²':
            case 'f/s²':
            case 'mG':
            case 'G (9.8)':
            case 'Gal':
            case 'mGal':
                value = new GravityDecimalPipe().transform(measure.value, measure.unit);
                unit = ' ' + new GravityUnitPipe().transform(measure.unit);
                break;
            // Old Format
            case 'NANOTESLA':
            case 'MICROTESLA':
            case 'MILLITESLA':
            case 'TESLA':
            case 'GAUSS':
            case 'GAMMA':
            // New Format
            case 'Nanotesla':
            case 'nT':
            case 'Microtesla':
            case 'µT':
            case 'Millitesla':
            case 'mT':
            case 'Tesla':
            case 'T':
            case 'Gauss':
            case 'Gs':
            case 'Gamma':
            case 'γ':
                value = new MagneticDecimalPipe().transform(measure.value, measure.unit);
                unit = ' ' + new MagneticUnitPipe().transform(measure.unit);
                break;
            default:
                return 'Unsupported Unit: ' + measure.unit;
        }
        // if we are displaying units
        if (displayUnit) {
            return value + unit;
        }
        else { // if we aren't displaying units, just give the value
            return value;
        }
    }
}
MeasurePipe.decorators = [
    { type: Pipe, args: [{
                name: 'measure'
            },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
// Orders the surveys in ascending/descending order based on the Md value
class OrderSurveysByMdPipe {
    constructor() {
        this.filtered = [];
    }
    /**
     * @param {?} items
     * @param {?=} reverse
     * @return {?}
     */
    transform(items, reverse) {
        items.forEach((/**
         * @param {?} item
         * @return {?}
         */
        item => {
            this.filtered.push(item);
        }));
        this.filtered.sort((/**
         * @param {?} a
         * @param {?} b
         * @return {?}
         */
        (a, b) => {
            return (a.md.value > b.md.value ? 1 : -1);
        }));
        if (reverse) {
            this.filtered.reverse();
        }
        return this.filtered;
    }
}
OrderSurveysByMdPipe.decorators = [
    { type: Pipe, args: [{
                name: 'orderSurveysByMd'
            },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class OrderByPipe {
    /**
     * @param {?} array
     * @param {?} field
     * @return {?}
     */
    transform(array, field) {
        array.sort((/**
         * @param {?} a
         * @param {?} b
         * @return {?}
         */
        (a, b) => {
            if (a[field] < b[field]) {
                return -1;
            }
            else if (a[field] > b[field]) {
                return 1;
            }
            else {
                return 0;
            }
        }));
        return array;
    }
    /**
     * @param {?} array
     * @param {?} field
     * @param {?} topElement
     * @return {?}
     */
    transformSurveySet(array, field, topElement) {
        array.sort((/**
         * @param {?} a
         * @param {?} b
         * @return {?}
         */
        (a, b) => {
            if (a === topElement || a[field] < b[field]) {
                return -1;
            }
            else if (a[field] > b[field]) {
                return 1;
            }
            else {
                return 0;
            }
        }));
        return array;
    }
}
OrderByPipe.decorators = [
    { type: Pipe, args: [{ name: 'orderBy', pure: false },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
// Converts the seconds into human readable date
class InstantPipe extends DatePipe {
    /**
     * @param {?} dateObj
     * @return {?}
     */
    transform(dateObj) {
        return (dateObj !== null && dateObj !== undefined) ? (super.transform(dateObj.seconds * 1000, 'medium', 'UTC')) : null;
    }
}
InstantPipe.decorators = [
    { type: Pipe, args: [{
                name: 'instant'
            },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class TrajectoryTypePipe {
    /**
     * @param {?} value
     * @param {?=} args
     * @return {?}
     */
    transform(value, args) {
        switch (value) {
            case 'ACTUAL':
                return 'Actual';
            case 'DEFINITIVE':
                return 'Definitive';
            case 'PLAN':
                return 'Prototype';
            case 'PROTOTYPE':
                return 'Completed';
            case 'REPORTED':
                return 'Reported';
        }
    }
}
TrajectoryTypePipe.decorators = [
    { type: Pipe, args: [{
                name: 'trajectoryType'
            },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class FormatObjectPipe {
    /**
     * @param {?} unit
     * @param {?=} full
     * @return {?}
     */
    transform(unit, full) {
        /** @type {?} */
        let string = '';
        switch (unit) {
            case 'DEGREE':
                string = '°';
                break;
            case 'GRADIAN':
                string = 'gon';
                break;
            case 'RADIAN':
                string = 'rad';
                break;
            case 'NANOTESLA':
                string = 'nT';
                break;
            case 'MICROTESLA':
                string = 'mT';
                break;
            case 'TESLA':
                string = 'T';
                break;
            case 'GAUSS':
                string = 'Gauss';
                break;
            case 'KILOMETER':
                string = 'Kilometers';
                break;
            case 'GAMMA':
                string = 'Gamma';
                break;
            case 'M_PER_SEC_SQ':
                string = 'M Per Sec Sq';
                break;
            case 'FT_PER_SEC_SQ':
                string = 'Ft Per Sec Sq';
                break;
            case 'G':
                string = 'G';
                break;
            case 'MILLI_G':
                string = 'Milli G';
                break;
            case 'G_98':
                string = 'G 98';
                break;
            case 'GAL':
                string = 'Gal';
                break;
            case 'MILLI_GAL':
                string = 'Milli Gal';
                break;
            case 'FOOT':
                string = 'Feet';
                break;
            case 'FOOT_US':
                string = 'ft US';
                break;
            case 'METER':
                string = 'Meters';
                break;
            case 'KILOMETER':
                string = 'Kilometers';
                break;
            case 'MILE':
                string = 'Miles';
                break;
            case 'YARD':
                string = 'Yards';
                break;
            case 'STANDARD':
                string = 'D-S';
                break;
            case 'POOR':
                string = 'D-P';
                break;
            case 'DEFINITIVE_INTERPOLATED':
                string = 'D-I';
                break;
            case 'BAD':
                string = 'X-B';
                break;
            case 'ACC_CHECK':
                string = 'X-A';
                break;
            case 'CHECKSHOT':
                string = 'X-C';
                break;
            case 'INTERPOLATED':
                string = 'X-I';
                break;
            default:
                string = unit;
                break;
        }
        return string;
    }
}
FormatObjectPipe.decorators = [
    { type: Pipe, args: [{
                name: 'formatObject'
            },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class PipesModule {
}
PipesModule.decorators = [
    { type: NgModule, args: [{
                declarations: [
                    CapitalizePipe,
                    EllipsisizePipe,
                    WellboreStatusPipe,
                    GravityUnitPipe,
                    AngleUnitPipe,
                    MagneticUnitPipe,
                    DistanceUnitPipe,
                    BooleanOnOffPipe,
                    TimeAgoPipe,
                    SurveySetStatePipe,
                    DistanceDecimalPipe,
                    AngleDecimalPipe,
                    GravityDecimalPipe,
                    MagneticDecimalPipe,
                    ResolvedStatePipe,
                    SurveyTypePipe,
                    DefinitiveSurveysPipe,
                    MeasurePipe,
                    OrderSurveysByMdPipe,
                    OrderByPipe,
                    InstantPipe,
                    TrajectoryTypePipe,
                    FormatObjectPipe
                ],
                exports: [
                    CapitalizePipe,
                    EllipsisizePipe,
                    WellboreStatusPipe,
                    GravityUnitPipe,
                    AngleUnitPipe,
                    MagneticUnitPipe,
                    DistanceUnitPipe,
                    BooleanOnOffPipe,
                    TimeAgoPipe,
                    SurveySetStatePipe,
                    DistanceDecimalPipe,
                    AngleDecimalPipe,
                    GravityDecimalPipe,
                    MagneticDecimalPipe,
                    ResolvedStatePipe,
                    SurveyTypePipe,
                    DefinitiveSurveysPipe,
                    MeasurePipe,
                    OrderSurveysByMdPipe,
                    OrderByPipe,
                    InstantPipe,
                    TrajectoryTypePipe,
                    FormatObjectPipe
                ],
                imports: [
                    CommonModule
                ]
            },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
const rangeMap = {};
// DISTANCE UNITS -- acceptable ranges for distances in various units (ranges should be equivalent across units)
rangeMap['METER'] = {};
rangeMap['METER']['Standard'] = { min: 0, max: 6096000 };
rangeMap['METER']['md'] = { min: 0, max: 30480 };
rangeMap['METER']['tvd'] = { min: 0, max: 30480 };
rangeMap['METER']['local'] = { min: -60960000, max: 60960000 };
rangeMap['KILOMETER'] = {};
rangeMap['KILOMETER']['Standard'] = { min: 0, max: 6096 };
rangeMap['KILOMETER']['md'] = { min: 0, max: 30.48 };
rangeMap['KILOMETER']['tvd'] = { min: 0, max: 30.48 };
rangeMap['KILOMETER']['local'] = { min: -60096, max: 60096 };
rangeMap['FOOT'] = {};
rangeMap['FOOT']['Standard'] = { min: 0, max: 20000000 };
rangeMap['FOOT']['md'] = { min: 0, max: 100000 };
rangeMap['FOOT']['tvd'] = { min: 0, max: 100000 };
rangeMap['FOOT']['local'] = { min: -200000000, max: 200000000 };
rangeMap['FOOT_US'] = {};
rangeMap['FOOT_US']['Standard'] = { min: 0, max: 20000000 };
rangeMap['FOOT_US']['md'] = { min: 0, max: 99999.8 };
rangeMap['FOOT_US']['tvd'] = { min: 0, max: 99999.8 };
rangeMap['FOOT_US']['local'] = { min: -200000000, max: 200000000 };
rangeMap['YARD'] = {};
rangeMap['YARD']['Standard'] = { min: 0, max: 6666666.6667 };
rangeMap['YARD']['md'] = { min: 0, max: 33333.333 };
rangeMap['YARD']['tvd'] = { min: 0, max: 33333.333 };
rangeMap['YARD']['local'] = { min: -66666660.6667, max: 66666660.6667 };
rangeMap['MILE'] = {};
rangeMap['MILE']['Standard'] = { min: 0, max: 3787.8787879 };
rangeMap['MILE']['md'] = { min: 0, max: 18.939393939393938 };
rangeMap['MILE']['tvd'] = { min: 0, max: 18.939393939393938 };
rangeMap['MILE']['local'] = { min: -37870.8787879, max: 37870.8787879 };
// ANGLE UNITS -- acceptable ranges for angles in various units (ranges should be equivalent across units)
// NOTE: Angle units are sometimes from 0 -> 360, and sometimes from -180 -> 180. So all of the ranges for
// angle units are from the equivalent of -180 degrees to 360 degrees to allow for all input.
rangeMap['DEGREE'] = {};
rangeMap['DEGREE']['Standard'] = { min: -180, max: 360 };
rangeMap['DEGREE']['dip'] = { min: -90, max: 90 };
rangeMap['DEGREE']['dec'] = { min: -180, max: 180 };
rangeMap['DEGREE']['gridConvergence'] = { min: -179.9999999999, max: 180 };
rangeMap['DEGREE']['inc'] = { min: 0, max: 180 };
rangeMap['DEGREE']['azi'] = { min: 0, max: 359.999999999 };
rangeMap['DEGREE']['toolFace'] = { min: 0, max: 360 };
rangeMap['RADIAN'] = {};
rangeMap['RADIAN']['Standard'] = { min: -3.14159, max: 6.28319 };
rangeMap['RADIAN']['dip'] = { min: -1.5708, max: 1.5708 };
rangeMap['RADIAN']['dec'] = { min: -3.14159, max: 3.14159 };
rangeMap['RADIAN']['gridConvergence'] = { min: -3.14159265, max: 3.14159 };
rangeMap['RADIAN']['inc'] = { min: 0, max: 3.14159 };
rangeMap['RADIAN']['azi'] = { min: 0, max: 6.28319 };
rangeMap['RADIAN']['toolFace'] = { min: 0, max: 6.28319 };
rangeMap['GRADIAN'] = {};
rangeMap['GRADIAN']['Standard'] = { min: -200, max: 400 };
rangeMap['GRADIAN']['dip'] = { min: -100, max: 100 };
rangeMap['GRADIAN']['dec'] = { min: -200, max: 200 };
rangeMap['GRADIAN']['gridConvergence'] = { min: -199.999999999889, max: 200 };
rangeMap['GRADIAN']['inc'] = { min: 0, max: 200 };
rangeMap['GRADIAN']['azi'] = { min: 0, max: 400 };
rangeMap['GRADIAN']['toolFace'] = { min: 0, max: 400 };
// GRAVITY UNITS -- acceptable ranges for gravity in various units (ranges should be equivalent across units)
rangeMap['M_PER_SEC_SQ'] = {};
rangeMap['M_PER_SEC_SQ']['Standard'] = { min: -10.2969825, max: 10.2969825 };
rangeMap['FT_PER_SEC_SQ'] = {};
rangeMap['FT_PER_SEC_SQ']['Standard'] = { min: -33.78275098425197, max: 33.78275098425197 };
rangeMap['G'] = {};
rangeMap['G']['Standard'] = { min: -1.05, max: 1.05 };
rangeMap['MILLI_G'] = {};
rangeMap['MILLI_G']['Standard'] = { min: -1050, max: 1050 };
rangeMap['G_98'] = {};
rangeMap['G_98']['Standard'] = { min: -1.0507125, max: 1.0507125 };
rangeMap['GAL'] = {};
rangeMap['GAL']['Standard'] = { min: -1029.69825, max: 1029.69825 };
rangeMap['MILLI_GAL'] = {};
rangeMap['MILLI_GAL']['Standard'] = { min: -1029698.25, max: 1029698.25 };
// MAGNETIC UNITS -- acceptable ranges for magnetic values in various units (ranges should be equivalent across units)
rangeMap['NANOTESLA'] = {};
rangeMap['NANOTESLA']['Standard'] = { min: -200000, max: 200000 };
rangeMap['MICROTESLA'] = {};
rangeMap['MICROTESLA']['Standard'] = { min: -200, max: 200 };
rangeMap['MILLITESLA'] = {};
rangeMap['MILLITESLA']['Standard'] = { min: -0.2, max: 0.2 };
rangeMap['TESLA'] = {};
rangeMap['TESLA']['Standard'] = { min: -0.0002, max: 0.0002 };
rangeMap['GAUSS'] = {};
rangeMap['GAUSS']['Standard'] = { min: -2.0, max: 2.0 };
rangeMap['GAMMA'] = {};
rangeMap['GAMMA']['Standard'] = { min: -200000.0, max: 200000 };
/**
 * @param {?} value
 * @return {?}
 */
function filterFloat(value) {
    if (/^(\-|\+)?([0-9]+(\.[0-9]+)?|Infinity)$/.test(value)) {
        return Number(value);
    }
    return NaN;
}
/**
 * @param {?} unit
 * @param {?} type
 * @return {?}
 */
function validateInput(unit, type) {
    return (/**
     * @param {?} control
     * @return {?}
     */
    (control) => {
        /** @type {?} */
        const value = filterFloat(control.value);
        if (type === undefined || type === null) {
            type = 'Standard';
        }
        /** @type {?} */
        const range = rangeMap[unit][type];
        if (value['length'] === 0) {
            return { 'validateInput': { error: 'The value has not been set.' } };
        }
        else if (isNaN(value)) {
            return { 'validateInput': { error: 'The value is invalid for field ' } };
        }
        else if (value < range.min || value > range.max) {
            return { 'validateInput': { error: 'The value is outside range ' + range.min + ' ' + formatObject[unit] + ' to ' + range.max + ' ' + formatObject[unit] + '.' } };
        }
        else {
            return null;
        }
    });
}
/**
 * @param {?} unit
 * @param {?} type
 * @return {?}
 */
function validateInputWithEmptyOption(unit, type) {
    return (/**
     * @param {?} control
     * @return {?}
     */
    (control) => {
        /** @type {?} */
        const value = filterFloat(control.value);
        if (type === undefined || type === null) {
            type = 'Standard';
        }
        /** @type {?} */
        const range = rangeMap[unit][type];
        if (isNaN(value)) {
            return { 'validateInput': { error: 'If well has no Planned Total Depth please enter 0' } };
        }
        else if (value < range.min || value > range.max) {
            return { 'validateInput': { error: 'The value is outside range ' + range.min + ' ' + formatObject[unit] + ' to ' + range.max + ' ' + formatObject[unit] + '.' } };
        }
        else {
            return null;
        }
    });
}
/**
 * @param {?} ids
 * @return {?}
 */
function validateMagneticModelId(ids) {
    return (/**
     * @param {?} control
     * @return {?}
     */
    (control) => {
        if (ids.indexOf(control.value) === -1) {
            return { 'validateMagneticModelId': { error: 'Please choose valid option' } };
        }
        else {
            return null;
        }
    });
}
/**
 * @param {?} inputValue
 * @param {?} unit
 * @param {?} type
 * @return {?}
 */
function validateValue(inputValue, unit, type) {
    /** @type {?} */
    const value = filterFloat(inputValue);
    if (type === undefined || type === null) {
        type = 'Standard';
    }
    /** @type {?} */
    const range = rangeMap[unit][type];
    if (value['length'] === 0) {
        return {
            status: false,
            message: 'The value has not been set'
        };
    }
    else if (isNaN(value)) {
        return {
            status: false,
            message: 'The value is invalid for field'
        };
    }
    else if (value < range.min || value > range.max) {
        return {
            status: false,
            message: 'The value is outside range ' + range.min + ' ' + formatObject[unit] + ' to ' + range.max + ' ' + formatObject[unit] + '.'
        };
    }
    else {
        return {
            status: true
        };
    }
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class MaterialModule {
}
MaterialModule.decorators = [
    { type: NgModule, args: [{
                imports: [
                    MatAutocompleteModule,
                    MatButtonModule,
                    MatButtonToggleModule,
                    MatCardModule,
                    MatCheckboxModule,
                    MatChipsModule,
                    MatDatepickerModule,
                    MatDialogModule,
                    MatExpansionModule,
                    MatFormFieldModule,
                    MatGridListModule,
                    MatIconModule,
                    MatInputModule,
                    MatListModule,
                    MatMenuModule,
                    MatNativeDateModule,
                    MatPaginatorModule,
                    MatProgressBarModule,
                    MatProgressSpinnerModule,
                    MatRadioModule,
                    MatSelectModule,
                    MatSortModule,
                    MatSidenavModule,
                    MatSliderModule,
                    MatSlideToggleModule,
                    MatSnackBarModule,
                    MatTableModule,
                    MatTabsModule,
                    MatToolbarModule,
                    MatTooltipModule,
                ],
                exports: [
                    MatAutocompleteModule,
                    MatButtonModule,
                    MatButtonToggleModule,
                    MatCardModule,
                    MatCheckboxModule,
                    MatChipsModule,
                    MatDatepickerModule,
                    MatDialogModule,
                    MatExpansionModule,
                    MatFormFieldModule,
                    MatGridListModule,
                    MatIconModule,
                    MatInputModule,
                    MatListModule,
                    MatMenuModule,
                    MatNativeDateModule,
                    MatPaginatorModule,
                    MatProgressBarModule,
                    MatProgressSpinnerModule,
                    MatRadioModule,
                    MatSelectModule,
                    MatSortModule,
                    MatSidenavModule,
                    MatSliderModule,
                    MatSlideToggleModule,
                    MatSnackBarModule,
                    MatTableModule,
                    MatTabsModule,
                    MatToolbarModule,
                    MatTooltipModule,
                ]
            },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class HeaderModule {
}
HeaderModule.decorators = [
    { type: NgModule, args: [{
                imports: [
                    CommonModule,
                    MaterialModule,
                    RouterModule
                ],
                declarations: [HeaderComponent],
                exports: [HeaderComponent],
                providers: [],
                entryComponents: []
            },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
const moment$1 = moment_;
class TableDefaultsService {
    constructor() {
        this.tableFilterIcon = '<div class="column-filter-header-parent"> <span class="ag-icon ag-filter-icon"></span> <span>Refine Search</span> </div>';
        this.tableDefDefaults = {
            enableRangeSelection: true,
            enableColResize: true,
            enableSorting: true,
            enableFilter: true,
            floatingFilter: true,
            rowSelection: 'multiple',
            rowHeight: 45,
            suppressPropertyNamesCheck: true,
            headerHeight: 32,
            icons: {
                filter: this.tableFilterIcon
            },
            postProcessPopup: this.repositionFilterMenu
        };
        this.colDefDefaults = {
            filter: 'agTextColumnFilter',
            suppressFilter: false,
            suppressMenu: true,
            suppressMovable: true,
            suppressResize: true,
            lockPosition: true
        };
        this.actionDefaults = {
            cellClass: 'table-btn',
            pinned: 'right',
            suppressToolPanel: true,
            suppressMenu: true
        };
    }
    /**
     * @param {?} defaultType
     * @param {?=} contextMenuObj
     * @return {?}
     */
    getDefaults(defaultType, contextMenuObj) {
        if (defaultType === 'tableDefDefaults') {
            return cloneDeep(this.tableDefDefaults);
        }
        if (defaultType === 'colDefDefaults') {
            return cloneDeep(this.colDefDefaults);
        }
        if (defaultType === 'actionDefaults') {
            return cloneDeep(this.actionDefaults);
        }
        if (defaultType === 'contextMenu' && !contextMenuObj) {
            return this.getContextMenuItems;
        }
        if (defaultType === 'contextMenu' && contextMenuObj) {
            return contextMenuObj;
        }
    }
    /**
     * @param {?} filterDate
     * @param {?} cellValue
     * @return {?}
     */
    dateComparator(filterDate, cellValue) {
        // Cell value date is ISO 8601 (ish)
        // Remove time information from cell value as filter input is a date
        /** @type {?} */
        const cellDate = moment$1(cellValue).hour(0).minute(0).second(0).millisecond(0);
        /** @type {?} */
        const filter = moment$1(filterDate);
        // Now that both parameters are Date objects, we can compare
        if (cellDate.isBefore(filter)) {
            return -1;
        }
        else if (cellDate.isAfter(filter)) {
            return 1;
        }
        else {
            return 0;
        }
    }
    /**
     * @param {?} params
     * @return {?}
     */
    repositionFilterMenu(params) {
        if (params.type !== 'columnMenu') {
            return;
        }
        /** @type {?} */
        const columnId = params.column.getId();
        /** @type {?} */
        const internalColDefs = params.column.gridOptionsWrapper.gridOptions.columnDefs;
        /** @type {?} */
        const lastColFilterType = internalColDefs[internalColDefs.length - 2].filter;
        if (lastColFilterType === 'set' || lastColFilterType === 'date') {
            /** @type {?} */
            const ePopup = params.ePopup;
            /** @type {?} */
            let oldLeftStr = ePopup.style.left;
            // remove 'px' from the string (ag-Grid uses px positioning)
            oldLeftStr = oldLeftStr.substring(0, oldLeftStr.indexOf('px'));
            /** @type {?} */
            const oldLeft = parseInt(oldLeftStr, 10);
            /** @type {?} */
            let newLeft;
            // HACK -y as shit
            columnId === 'manifestation' || columnId === 'status'
                ? (newLeft = oldLeft - 16)
                : (newLeft = oldLeft - 50);
            ePopup.style.left = newLeft + 'px';
        }
    }
    /**
     * @param {?} params
     * @return {?}
     */
    updateNullValues(params) {
        return params.value === null || params.value === ''
            ? '(BLANK)'
            : params.value;
    }
    /**
     * @param {?} params
     * @return {?}
     */
    getContextMenuItems(params) {
        /** @type {?} */
        const contextMenuResults = [
            {
                name: 'Copy Single Cell',
                action: (/**
                 * @return {?}
                 */
                () => {
                    params.api.copySelectedRangeToClipboard();
                }),
                icon: '<i class="material-icons">content_copy</i>'
            },
            {
                name: 'Copy Selected Row(s)',
                action: (/**
                 * @return {?}
                 */
                () => {
                    params.context.tableTools.copySelectedRows(false);
                }),
                icon: '<i class="material-icons">content_copy</i>'
            },
            {
                name: 'Copy Selected Row(s) With Headers',
                action: (/**
                 * @return {?}
                 */
                () => {
                    params.context.tableTools.copySelectedRows(true);
                }),
                icon: '<i class="material-icons">content_copy</i>'
            },
            'separator',
            {
                name: params.api.isToolPanelShowing()
                    ? 'Hide Tool Panel'
                    : 'Show Tool Panel',
                action: (/**
                 * @return {?}
                 */
                () => {
                    params.context.tableTools.enhanceTableSearch();
                }),
                icon: '<i class="material-icons">build</i>'
            },
            {
                name: 'Export Table',
                icon: '<i class="material-icons">file_download</i>',
                subMenu: [
                    {
                        name: 'Export as XLS',
                        action: (/**
                         * @return {?}
                         */
                        () => {
                            params.context.tableTools.exportToExcel();
                        }),
                        icon: '<i class="material-icons">file_download</i>'
                    },
                    {
                        name: 'Export as CSV',
                        action: (/**
                         * @return {?}
                         */
                        () => {
                            params.context.tableTools.exportAsCSV();
                        }),
                        icon: '<i class="material-icons">file_download</i>'
                    }
                ]
            }
        ];
        return contextMenuResults;
    }
}
TableDefaultsService.decorators = [
    { type: Injectable }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class RowButtonComponent {
    /**
     * @param {?} activatedRoute
     * @param {?} router
     */
    constructor(activatedRoute, router) {
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.idOfMostRecentCommit = '';
    }
    /**
     * @param {?} params
     * @return {?}
     */
    agInit(params) {
        // Row data
        this.data = params.data;
        this.tableData = params.context._data;
        this.buttons = (/** @type {?} */ (params.colDef.buttons));
        this.setIcons();
        this.outputEmitter = params.context.buttonOutput;
    }
    /**
     * @private
     * @return {?}
     */
    setIcons() {
        for (const button of this.buttons) {
            switch (button.type) {
                // Dual table, add row to selection
                case 'add': {
                    button.icon = 'add_circle';
                    button.color = '#03A9F4';
                    break;
                }
                case 'delete': {
                    // delete is set to remove the most recent breadcrumb and is used
                    // to route user after use - use remove if this is undesirable
                    button.icon = 'delete';
                    button.color = '#ef5350';
                    break;
                }
                case 'edit': {
                    button.icon = 'mode_edit';
                    button.color = '#4caf50';
                    break;
                }
                case 'info': {
                    button.icon = 'info_outline';
                    button.color = '#03A9F4';
                    break;
                }
                // Dual table, remove row from selection
                case 'remove': {
                    button.icon = 'remove_circle';
                    button.color = '#F44336';
                    break;
                }
            }
        }
    }
    /**
     * @param {?} button
     * @return {?}
     */
    clicked(button) {
        if (button.navigateTo) {
            // interpolate URL
            /** @type {?} */
            let url = [];
            if (typeof button.navigateTo === 'string') {
                url = [this.interpolate(button.navigateTo)];
            }
            else if (button.navigateTo instanceof Array) {
                for (const s of button.navigateTo) {
                    url.push(this.interpolate(s));
                }
            }
        }
        /** @type {?} */
        const d = {
            type: button.type,
            rowData: this.data
        };
        this.outputEmitter.emit(d);
    }
    /**
     * @private
     * @param {?} target
     * @return {?}
     */
    interpolate(target) {
        /** @type {?} */
        const interpolation = /\$\{(\w+)\}/g;
        /** @type {?} */
        let match;
        while ((match = interpolation.exec(target))) {
            /** @type {?} */
            const key = match[1];
            // this checks for routing from within the tree table
            if (Array.isArray(this.data[key]) && this.data[key].length > 1) {
                target = target.replace('${' + key + '}', this.data[key][0]);
            }
            else {
                target = target.replace('${' + key + '}', this.data[key]);
            }
        }
        return target;
    }
    /**
     * @return {?}
     */
    refresh() {
        return false;
    }
}
RowButtonComponent.decorators = [
    { type: Component, args: [{
                selector: 'app-grid-row-button',
                template: `
    <button mat-icon-button
            [style.color]="button.color"
            (click)="clicked(button)"
            *ngFor="let button of buttons"
            class="grid-row-btn">
      <span class="material-icons" [ngClass]="button.icon">
        <mat-icon>{{button.icon}}</mat-icon>
      </span>
    </button>
    `
            }] }
];
/** @nocollapse */
RowButtonComponent.ctorParameters = () => [
    { type: ActivatedRoute },
    { type: Router }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class NewFileCellRendererComponent {
    constructor() { }
    /**
     * @param {?} params
     * @return {?}
     */
    agInit(params) {
        // Row data
        this.value = params.value;
        this.icon = params.colDef.iconCell;
    }
    /**
     * @return {?}
     */
    refresh() {
        return false;
    }
}
NewFileCellRendererComponent.decorators = [
    { type: Component, args: [{
                selector: 'app-new-file-cell-renderer',
                template: "<div class=\"new-file-flag-container\">\n  <span class=\"material-icons flag-icon\" *ngIf=\"value === 'Yes'\">\n    <mat-icon>{{icon}}</mat-icon>\n  </span>\n  {{value}}\n</div>",
                styles: ["@media print{.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-appearance-legacy.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-1.28122em) scale(.75);transform:translateY(-1.28122em) scale(.75)}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-form-field-autofill-control:-webkit-autofill+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.28121em) scale(.75);transform:translateY(-1.28121em) scale(.75)}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.2812em) scale(.75);transform:translateY(-1.2812em) scale(.75)}}.mat-badge-content{font-weight:600;font-size:12px;font-family:NunitoSans,sans-serif}.mat-badge-small .mat-badge-content{font-size:9px}.mat-badge-large .mat-badge-content{font-size:24px}.mat-h1,.mat-headline,.mat-typography h1{font:400 24px/32px NunitoSans,sans-serif;margin:0 0 16px}.mat-h2,.mat-title,.mat-typography h2{font:500 20px/32px NunitoSans,sans-serif;margin:0 0 16px}.mat-h3,.mat-subheading-2,.mat-typography h3{font:400 16px/28px NunitoSans,sans-serif;margin:0 0 16px}.mat-h4,.mat-subheading-1,.mat-typography h4{font:400 15px/24px NunitoSans,sans-serif;margin:0 0 16px}.mat-h5,.mat-h6,.mat-typography h5,.mat-typography h6{margin:0 0 12px}.mat-body-2,.mat-body-strong{font:500 14px/24px NunitoSans,sans-serif}.mat-body,.mat-body-1,.mat-typography{font:400 14px/20px NunitoSans,sans-serif}.mat-body p,.mat-body-1 p,.mat-typography p{margin:0 0 12px}.mat-caption,.mat-small{font:400 12px/20px NunitoSans,sans-serif}.mat-display-4,.mat-typography .mat-display-4{font:300 112px/112px NunitoSans,sans-serif;letter-spacing:-.05em;margin:0 0 56px}.mat-display-3,.mat-typography .mat-display-3{font:400 56px/56px NunitoSans,sans-serif;letter-spacing:-.02em;margin:0 0 64px}.mat-display-2,.mat-typography .mat-display-2{font:400 45px/48px NunitoSans,sans-serif;letter-spacing:-.005em;margin:0 0 64px}.mat-display-1,.mat-typography .mat-display-1{font:400 34px/40px NunitoSans,sans-serif;margin:0 0 64px}.mat-bottom-sheet-container{font:400 14px/20px NunitoSans,sans-serif}.mat-button,.mat-fab,.mat-flat-button,.mat-icon-button,.mat-mini-fab,.mat-raised-button,.mat-stroked-button{font-family:NunitoSans,sans-serif;font-size:14px;font-weight:500}.mat-button-toggle,.mat-card{font-family:NunitoSans,sans-serif}.mat-card-title{font-size:24px;font-weight:500}.mat-card-header .mat-card-title{font-size:20px}.mat-card-content,.mat-card-subtitle{font-size:14px}.mat-checkbox{font-family:NunitoSans,sans-serif}.mat-checkbox-layout .mat-checkbox-label{line-height:24px}.mat-chip{font-size:14px;font-weight:500}.mat-chip .mat-chip-remove.mat-icon,.mat-chip .mat-chip-trailing-icon.mat-icon{font-size:18px}.mat-table{font-family:NunitoSans,sans-serif}.mat-header-cell{font-size:12px;font-weight:500}.mat-cell,.mat-footer-cell{font-size:14px}.mat-calendar{font-family:NunitoSans,sans-serif}.mat-calendar-body{font-size:13px}.mat-calendar-body-label,.mat-calendar-period-button{font-size:14px;font-weight:500}.mat-calendar-table-header th{font-size:11px;font-weight:400}.mat-dialog-title{font:500 20px/32px NunitoSans,sans-serif}.mat-expansion-panel-header{font-family:NunitoSans,sans-serif;font-size:15px;font-weight:400}.mat-expansion-panel-content{font:400 14px/20px NunitoSans,sans-serif}.mat-form-field{font-size:inherit;font-weight:400;line-height:1.125;font-family:NunitoSans,sans-serif}.mat-form-field-wrapper{padding-bottom:1.34375em}.mat-form-field-prefix .mat-icon,.mat-form-field-suffix .mat-icon{font-size:150%;line-height:1.125}.mat-form-field-prefix .mat-icon-button,.mat-form-field-suffix .mat-icon-button{height:1.5em;width:1.5em}.mat-form-field-prefix .mat-icon-button .mat-icon,.mat-form-field-suffix .mat-icon-button .mat-icon{height:1.125em;line-height:1.125}.mat-form-field-infix{padding:.5em 0;border-top:.84375em solid transparent}.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-1.34373em) scale(.75);transform:translateY(-1.34373em) scale(.75);width:133.3333533333%}.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.34372em) scale(.75);transform:translateY(-1.34372em) scale(.75);width:133.3333633333%}.mat-form-field-label-wrapper{top:-.84375em;padding-top:.84375em}.mat-form-field-label{top:1.34375em}.mat-form-field-underline{bottom:1.34375em}.mat-form-field-subscript-wrapper{font-size:75%;margin-top:.6666666667em;top:calc(100% - 1.7916666667em)}.mat-form-field-appearance-legacy .mat-form-field-wrapper{padding-bottom:1.25em}.mat-form-field-appearance-legacy .mat-form-field-infix{padding:.4375em 0}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-appearance-legacy.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00106px);transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00106px);-ms-transform:translateY(-1.28119em) scale(.75);width:133.3333933333%}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-form-field-autofill-control:-webkit-autofill+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00107px);transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00107px);-ms-transform:translateY(-1.28118em) scale(.75);width:133.3334033333%}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00108px);transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00108px);-ms-transform:translateY(-1.28117em) scale(.75);width:133.3334133333%}.mat-form-field-appearance-legacy .mat-form-field-label{top:1.28125em}.mat-form-field-appearance-legacy .mat-form-field-underline{bottom:1.25em}.mat-form-field-appearance-legacy .mat-form-field-subscript-wrapper{margin-top:.5416666667em;top:calc(100% - 1.6666666667em)}@media print{.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-appearance-legacy.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-1.28116em) scale(.75);transform:translateY(-1.28116em) scale(.75)}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-form-field-autofill-control:-webkit-autofill+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.28115em) scale(.75);transform:translateY(-1.28115em) scale(.75)}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.28114em) scale(.75);transform:translateY(-1.28114em) scale(.75)}}.mat-form-field-appearance-fill .mat-form-field-infix{padding:.25em 0 .75em}.mat-form-field-appearance-fill .mat-form-field-label{top:1.09375em;margin-top:-.5em}.mat-form-field-appearance-fill.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-appearance-fill.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-.59373em) scale(.75);transform:translateY(-.59373em) scale(.75);width:133.3333533333%}.mat-form-field-appearance-fill.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-.59372em) scale(.75);transform:translateY(-.59372em) scale(.75);width:133.3333633333%}.mat-form-field-appearance-outline .mat-form-field-infix{padding:1em 0}.mat-form-field-appearance-outline .mat-form-field-label{top:1.84375em;margin-top:-.25em}.mat-form-field-appearance-outline.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-appearance-outline.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-1.59373em) scale(.75);transform:translateY(-1.59373em) scale(.75);width:133.3333533333%}.mat-form-field-appearance-outline.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.59372em) scale(.75);transform:translateY(-1.59372em) scale(.75);width:133.3333633333%}.mat-grid-tile-footer,.mat-grid-tile-header{font-size:14px}.mat-grid-tile-footer .mat-line,.mat-grid-tile-header .mat-line{white-space:nowrap;overflow:hidden;text-overflow:ellipsis;display:block;box-sizing:border-box}.mat-grid-tile-footer .mat-line:nth-child(n+2),.mat-grid-tile-header .mat-line:nth-child(n+2){font-size:12px}input.mat-input-element{margin-top:-.0625em}.mat-menu-item{font-family:NunitoSans,sans-serif;font-size:14px;font-weight:400}.mat-paginator,.mat-paginator-page-size .mat-select-trigger{font-family:NunitoSans,sans-serif;font-size:12px}.mat-radio-button,.mat-select{font-family:NunitoSans,sans-serif}.mat-select-trigger{height:1.125em}.mat-slide-toggle-content{font-family:NunitoSans,sans-serif}.mat-slider-thumb-label-text{font-family:NunitoSans,sans-serif;font-size:12px;font-weight:500}.mat-stepper-horizontal,.mat-stepper-vertical{font-family:NunitoSans,sans-serif}.mat-step-label{font-size:14px;font-weight:400}.mat-step-sub-label-error{font-weight:400}.mat-step-label-error{font-size:14px}.mat-step-label-selected{font-size:14px;font-weight:500}.mat-tab-group{font-family:NunitoSans,sans-serif}.mat-tab-label,.mat-tab-link{font-family:NunitoSans,sans-serif;font-size:14px;font-weight:500}.mat-toolbar,.mat-toolbar h1,.mat-toolbar h2,.mat-toolbar h3,.mat-toolbar h4,.mat-toolbar h5,.mat-toolbar h6{font:500 20px/32px NunitoSans,sans-serif;margin:0}.mat-tooltip{font-family:NunitoSans,sans-serif;font-size:10px;padding-top:6px;padding-bottom:6px}.mat-tooltip-handset{font-size:14px;padding-top:8px;padding-bottom:8px}.mat-list-item,.mat-list-option{font-family:NunitoSans,sans-serif}.mat-list-base .mat-list-item{font-size:16px}.mat-list-base .mat-list-item .mat-line{white-space:nowrap;overflow:hidden;text-overflow:ellipsis;display:block;box-sizing:border-box}.mat-list-base .mat-list-item .mat-line:nth-child(n+2){font-size:14px}.mat-list-base .mat-list-option{font-size:16px}.mat-list-base .mat-list-option .mat-line{white-space:nowrap;overflow:hidden;text-overflow:ellipsis;display:block;box-sizing:border-box}.mat-list-base .mat-list-option .mat-line:nth-child(n+2){font-size:14px}.mat-list-base .mat-subheader{font-family:NunitoSans,sans-serif;font-size:14px;font-weight:500}.mat-list-base[dense] .mat-list-item{font-size:12px}.mat-list-base[dense] .mat-list-item .mat-line{white-space:nowrap;overflow:hidden;text-overflow:ellipsis;display:block;box-sizing:border-box}.mat-list-base[dense] .mat-list-item .mat-line:nth-child(n+2),.mat-list-base[dense] .mat-list-option{font-size:12px}.mat-list-base[dense] .mat-list-option .mat-line{white-space:nowrap;overflow:hidden;text-overflow:ellipsis;display:block;box-sizing:border-box}.mat-list-base[dense] .mat-list-option .mat-line:nth-child(n+2){font-size:12px}.mat-list-base[dense] .mat-subheader{font-family:NunitoSans,sans-serif;font-size:12px;font-weight:500}.mat-option{font-family:NunitoSans,sans-serif;font-size:16px}.mat-optgroup-label{font:500 14px/24px NunitoSans,sans-serif}.mat-simple-snackbar{font-family:NunitoSans,sans-serif;font-size:14px}.mat-simple-snackbar-action{line-height:1;font-family:inherit;font-size:inherit;font-weight:500}.mat-tree{font-family:NunitoSans,sans-serif}.mat-nested-tree-node,.mat-tree-node{font-weight:400;font-size:14px}.mat-ripple{overflow:hidden;position:relative}.mat-ripple.mat-ripple-unbounded{overflow:visible}.mat-ripple-element{position:absolute;border-radius:50%;pointer-events:none;-webkit-transition:opacity,-webkit-transform cubic-bezier(0,0,.2,1);transition:opacity,transform cubic-bezier(0,0,.2,1),-webkit-transform cubic-bezier(0,0,.2,1);-webkit-transform:scale(0);transform:scale(0)}@media (-ms-high-contrast:active){.mat-ripple-element{display:none}}.cdk-visually-hidden{border:0;clip:rect(0 0 0 0);height:1px;margin:-1px;overflow:hidden;padding:0;position:absolute;width:1px;outline:0;-webkit-appearance:none;-moz-appearance:none}.cdk-global-overlay-wrapper,.cdk-overlay-container{pointer-events:none;top:0;left:0;height:100%;width:100%}.cdk-overlay-container{position:fixed;z-index:1000}.cdk-overlay-container:empty{display:none}.cdk-global-overlay-wrapper{display:-webkit-box;display:flex;position:absolute;z-index:1000}.cdk-overlay-pane{position:absolute;pointer-events:auto;box-sizing:border-box;z-index:1000;display:-webkit-box;display:flex;max-width:100%;max-height:100%}.cdk-overlay-backdrop{position:absolute;top:0;bottom:0;left:0;right:0;z-index:1000;pointer-events:auto;-webkit-tap-highlight-color:transparent;-webkit-transition:opacity .4s cubic-bezier(.25,.8,.25,1);transition:opacity .4s cubic-bezier(.25,.8,.25,1);opacity:0}.cdk-overlay-backdrop.cdk-overlay-backdrop-showing{opacity:1}@media screen and (-ms-high-contrast:active){.cdk-overlay-backdrop.cdk-overlay-backdrop-showing{opacity:.6}}.cdk-overlay-dark-backdrop{background:rgba(0,0,0,.32)}.cdk-overlay-transparent-backdrop,.cdk-overlay-transparent-backdrop.cdk-overlay-backdrop-showing{opacity:0}.cdk-overlay-connected-position-bounding-box{position:absolute;z-index:1000;display:-webkit-box;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;flex-direction:column;min-width:1px;min-height:1px}.cdk-global-scrollblock{position:fixed;width:100%;overflow-y:scroll}@-webkit-keyframes cdk-text-field-autofill-start{/*!*/}@keyframes cdk-text-field-autofill-start{/*!*/}@-webkit-keyframes cdk-text-field-autofill-end{/*!*/}@keyframes cdk-text-field-autofill-end{/*!*/}.cdk-text-field-autofill-monitored:-webkit-autofill{-webkit-animation-name:cdk-text-field-autofill-start;animation-name:cdk-text-field-autofill-start}.cdk-text-field-autofill-monitored:not(:-webkit-autofill){-webkit-animation-name:cdk-text-field-autofill-end;animation-name:cdk-text-field-autofill-end}textarea.cdk-textarea-autosize{resize:none}textarea.cdk-textarea-autosize-measuring{height:auto!important;overflow:hidden!important;padding:2px 0!important;box-sizing:content-box!important}.icon{color:#1e4e79;display:-webkit-box;display:flex}.new-file-flag-container{display:-webkit-box;display:flex;-webkit-box-align:center;align-items:center}"]
            }] }
];
/** @nocollapse */
NewFileCellRendererComponent.ctorParameters = () => [];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class AutomationCellRendererComponent {
    constructor() { }
    /**
     * @param {?} params
     * @return {?}
     */
    agInit(params) {
        // Row data
        this.value = params.value;
        this.data = params.data;
    }
    /**
     * @return {?}
     */
    refresh() {
        return false;
    }
}
AutomationCellRendererComponent.decorators = [
    { type: Component, args: [{
                selector: 'app-automation-cell-renderer',
                template: "<div class=\"automation-cell-container\">\n  {{value}}\n  <mat-icon *ngIf=\"data.automation === 'Off' && data.automateCondition\" matTooltip=\"{{data.automationConditionText}}\"\n    class=\"status-table-icon pause-icon\">pause</mat-icon>\n  <mat-icon *ngIf=\"data.automation === 'Off' && data.automateCondition\" matTooltip=\"{{data.automateConditionText}}\"\n    class=\"status-table-icon auto-off-ff-icon\">fast_forward</mat-icon>\n  <mat-icon *ngIf=\"data.automation === 'Off' && !data.automateCondition\" matTooltip=\"{{data.automationConditionText}}\"\n    class=\"status-table-icon stop-icon\">stop</mat-icon>\n  <mat-icon *ngIf=\"data.automation === 'On' && data.automateCondition\" matTooltip=\"{{data.automationConditionText}}\"\n    class=\"status-table-icon play-icon\">play_arrow</mat-icon>\n  <mat-icon *ngIf=\"data.automation === 'On' && data.automateCondition\" matTooltip=\"{{data.automateConditionText}}\"\n    class=\"status-table-icon ff-icon\">fast_forward</mat-icon>\n  <mat-icon *ngIf=\"data.automation === 'On' && !data.automateCondition\" matTooltip=\"{{data.automationConditionText}}\"\n    class=\"status-table-icon play-icon\">play_arrow</mat-icon>\n\n  <mat-icon *ngIf=\"data.manualCondition\" matTooltip=\"{{data.manualConditionText}}\" class=\"status-table-icon flair-icon\">flare</mat-icon>\n</div>\n",
                styles: ["@media print{.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-appearance-legacy.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-1.28122em) scale(.75);transform:translateY(-1.28122em) scale(.75)}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-form-field-autofill-control:-webkit-autofill+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.28121em) scale(.75);transform:translateY(-1.28121em) scale(.75)}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.2812em) scale(.75);transform:translateY(-1.2812em) scale(.75)}}.mat-badge-content{font-weight:600;font-size:12px;font-family:NunitoSans,sans-serif}.mat-badge-small .mat-badge-content{font-size:9px}.mat-badge-large .mat-badge-content{font-size:24px}.mat-h1,.mat-headline,.mat-typography h1{font:400 24px/32px NunitoSans,sans-serif;margin:0 0 16px}.mat-h2,.mat-title,.mat-typography h2{font:500 20px/32px NunitoSans,sans-serif;margin:0 0 16px}.mat-h3,.mat-subheading-2,.mat-typography h3{font:400 16px/28px NunitoSans,sans-serif;margin:0 0 16px}.mat-h4,.mat-subheading-1,.mat-typography h4{font:400 15px/24px NunitoSans,sans-serif;margin:0 0 16px}.mat-h5,.mat-h6,.mat-typography h5,.mat-typography h6{margin:0 0 12px}.mat-body-2,.mat-body-strong{font:500 14px/24px NunitoSans,sans-serif}.mat-body,.mat-body-1,.mat-typography{font:400 14px/20px NunitoSans,sans-serif}.mat-body p,.mat-body-1 p,.mat-typography p{margin:0 0 12px}.mat-caption,.mat-small{font:400 12px/20px NunitoSans,sans-serif}.mat-display-4,.mat-typography .mat-display-4{font:300 112px/112px NunitoSans,sans-serif;letter-spacing:-.05em;margin:0 0 56px}.mat-display-3,.mat-typography .mat-display-3{font:400 56px/56px NunitoSans,sans-serif;letter-spacing:-.02em;margin:0 0 64px}.mat-display-2,.mat-typography .mat-display-2{font:400 45px/48px NunitoSans,sans-serif;letter-spacing:-.005em;margin:0 0 64px}.mat-display-1,.mat-typography .mat-display-1{font:400 34px/40px NunitoSans,sans-serif;margin:0 0 64px}.mat-bottom-sheet-container{font:400 14px/20px NunitoSans,sans-serif}.mat-button,.mat-fab,.mat-flat-button,.mat-icon-button,.mat-mini-fab,.mat-raised-button,.mat-stroked-button{font-family:NunitoSans,sans-serif;font-size:14px;font-weight:500}.mat-button-toggle,.mat-card{font-family:NunitoSans,sans-serif}.mat-card-title{font-size:24px;font-weight:500}.mat-card-header .mat-card-title{font-size:20px}.mat-card-content,.mat-card-subtitle{font-size:14px}.mat-checkbox{font-family:NunitoSans,sans-serif}.mat-checkbox-layout .mat-checkbox-label{line-height:24px}.mat-chip{font-size:14px;font-weight:500}.mat-chip .mat-chip-remove.mat-icon,.mat-chip .mat-chip-trailing-icon.mat-icon{font-size:18px}.mat-table{font-family:NunitoSans,sans-serif}.mat-header-cell{font-size:12px;font-weight:500}.mat-cell,.mat-footer-cell{font-size:14px}.mat-calendar{font-family:NunitoSans,sans-serif}.mat-calendar-body{font-size:13px}.mat-calendar-body-label,.mat-calendar-period-button{font-size:14px;font-weight:500}.mat-calendar-table-header th{font-size:11px;font-weight:400}.mat-dialog-title{font:500 20px/32px NunitoSans,sans-serif}.mat-expansion-panel-header{font-family:NunitoSans,sans-serif;font-size:15px;font-weight:400}.mat-expansion-panel-content{font:400 14px/20px NunitoSans,sans-serif}.mat-form-field{font-size:inherit;font-weight:400;line-height:1.125;font-family:NunitoSans,sans-serif}.mat-form-field-wrapper{padding-bottom:1.34375em}.mat-form-field-prefix .mat-icon,.mat-form-field-suffix .mat-icon{font-size:150%;line-height:1.125}.mat-form-field-prefix .mat-icon-button,.mat-form-field-suffix .mat-icon-button{height:1.5em;width:1.5em}.mat-form-field-prefix .mat-icon-button .mat-icon,.mat-form-field-suffix .mat-icon-button .mat-icon{height:1.125em;line-height:1.125}.mat-form-field-infix{padding:.5em 0;border-top:.84375em solid transparent}.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-1.34373em) scale(.75);transform:translateY(-1.34373em) scale(.75);width:133.3333533333%}.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.34372em) scale(.75);transform:translateY(-1.34372em) scale(.75);width:133.3333633333%}.mat-form-field-label-wrapper{top:-.84375em;padding-top:.84375em}.mat-form-field-label{top:1.34375em}.mat-form-field-underline{bottom:1.34375em}.mat-form-field-subscript-wrapper{font-size:75%;margin-top:.6666666667em;top:calc(100% - 1.7916666667em)}.mat-form-field-appearance-legacy .mat-form-field-wrapper{padding-bottom:1.25em}.mat-form-field-appearance-legacy .mat-form-field-infix{padding:.4375em 0}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-appearance-legacy.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00106px);transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00106px);-ms-transform:translateY(-1.28119em) scale(.75);width:133.3333933333%}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-form-field-autofill-control:-webkit-autofill+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00107px);transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00107px);-ms-transform:translateY(-1.28118em) scale(.75);width:133.3334033333%}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00108px);transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00108px);-ms-transform:translateY(-1.28117em) scale(.75);width:133.3334133333%}.mat-form-field-appearance-legacy .mat-form-field-label{top:1.28125em}.mat-form-field-appearance-legacy .mat-form-field-underline{bottom:1.25em}.mat-form-field-appearance-legacy .mat-form-field-subscript-wrapper{margin-top:.5416666667em;top:calc(100% - 1.6666666667em)}@media print{.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-appearance-legacy.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-1.28116em) scale(.75);transform:translateY(-1.28116em) scale(.75)}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-form-field-autofill-control:-webkit-autofill+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.28115em) scale(.75);transform:translateY(-1.28115em) scale(.75)}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.28114em) scale(.75);transform:translateY(-1.28114em) scale(.75)}}.mat-form-field-appearance-fill .mat-form-field-infix{padding:.25em 0 .75em}.mat-form-field-appearance-fill .mat-form-field-label{top:1.09375em;margin-top:-.5em}.mat-form-field-appearance-fill.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-appearance-fill.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-.59373em) scale(.75);transform:translateY(-.59373em) scale(.75);width:133.3333533333%}.mat-form-field-appearance-fill.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-.59372em) scale(.75);transform:translateY(-.59372em) scale(.75);width:133.3333633333%}.mat-form-field-appearance-outline .mat-form-field-infix{padding:1em 0}.mat-form-field-appearance-outline .mat-form-field-label{top:1.84375em;margin-top:-.25em}.mat-form-field-appearance-outline.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-appearance-outline.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-1.59373em) scale(.75);transform:translateY(-1.59373em) scale(.75);width:133.3333533333%}.mat-form-field-appearance-outline.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.59372em) scale(.75);transform:translateY(-1.59372em) scale(.75);width:133.3333633333%}.mat-grid-tile-footer,.mat-grid-tile-header{font-size:14px}.mat-grid-tile-footer .mat-line,.mat-grid-tile-header .mat-line{white-space:nowrap;overflow:hidden;text-overflow:ellipsis;display:block;box-sizing:border-box}.mat-grid-tile-footer .mat-line:nth-child(n+2),.mat-grid-tile-header .mat-line:nth-child(n+2){font-size:12px}input.mat-input-element{margin-top:-.0625em}.mat-menu-item{font-family:NunitoSans,sans-serif;font-size:14px;font-weight:400}.mat-paginator,.mat-paginator-page-size .mat-select-trigger{font-family:NunitoSans,sans-serif;font-size:12px}.mat-radio-button,.mat-select{font-family:NunitoSans,sans-serif}.mat-select-trigger{height:1.125em}.mat-slide-toggle-content{font-family:NunitoSans,sans-serif}.mat-slider-thumb-label-text{font-family:NunitoSans,sans-serif;font-size:12px;font-weight:500}.mat-stepper-horizontal,.mat-stepper-vertical{font-family:NunitoSans,sans-serif}.mat-step-label{font-size:14px;font-weight:400}.mat-step-sub-label-error{font-weight:400}.mat-step-label-error{font-size:14px}.mat-step-label-selected{font-size:14px;font-weight:500}.mat-tab-group{font-family:NunitoSans,sans-serif}.mat-tab-label,.mat-tab-link{font-family:NunitoSans,sans-serif;font-size:14px;font-weight:500}.mat-toolbar,.mat-toolbar h1,.mat-toolbar h2,.mat-toolbar h3,.mat-toolbar h4,.mat-toolbar h5,.mat-toolbar h6{font:500 20px/32px NunitoSans,sans-serif;margin:0}.mat-tooltip{font-family:NunitoSans,sans-serif;font-size:10px;padding-top:6px;padding-bottom:6px}.mat-tooltip-handset{font-size:14px;padding-top:8px;padding-bottom:8px}.mat-list-item,.mat-list-option{font-family:NunitoSans,sans-serif}.mat-list-base .mat-list-item{font-size:16px}.mat-list-base .mat-list-item .mat-line{white-space:nowrap;overflow:hidden;text-overflow:ellipsis;display:block;box-sizing:border-box}.mat-list-base .mat-list-item .mat-line:nth-child(n+2){font-size:14px}.mat-list-base .mat-list-option{font-size:16px}.mat-list-base .mat-list-option .mat-line{white-space:nowrap;overflow:hidden;text-overflow:ellipsis;display:block;box-sizing:border-box}.mat-list-base .mat-list-option .mat-line:nth-child(n+2){font-size:14px}.mat-list-base .mat-subheader{font-family:NunitoSans,sans-serif;font-size:14px;font-weight:500}.mat-list-base[dense] .mat-list-item{font-size:12px}.mat-list-base[dense] .mat-list-item .mat-line{white-space:nowrap;overflow:hidden;text-overflow:ellipsis;display:block;box-sizing:border-box}.mat-list-base[dense] .mat-list-item .mat-line:nth-child(n+2),.mat-list-base[dense] .mat-list-option{font-size:12px}.mat-list-base[dense] .mat-list-option .mat-line{white-space:nowrap;overflow:hidden;text-overflow:ellipsis;display:block;box-sizing:border-box}.mat-list-base[dense] .mat-list-option .mat-line:nth-child(n+2){font-size:12px}.mat-list-base[dense] .mat-subheader{font-family:NunitoSans,sans-serif;font-size:12px;font-weight:500}.mat-option{font-family:NunitoSans,sans-serif;font-size:16px}.mat-optgroup-label{font:500 14px/24px NunitoSans,sans-serif}.mat-simple-snackbar{font-family:NunitoSans,sans-serif;font-size:14px}.mat-simple-snackbar-action{line-height:1;font-family:inherit;font-size:inherit;font-weight:500}.mat-tree{font-family:NunitoSans,sans-serif}.mat-nested-tree-node,.mat-tree-node{font-weight:400;font-size:14px}.mat-ripple{overflow:hidden;position:relative}.mat-ripple.mat-ripple-unbounded{overflow:visible}.mat-ripple-element{position:absolute;border-radius:50%;pointer-events:none;-webkit-transition:opacity,-webkit-transform cubic-bezier(0,0,.2,1);transition:opacity,transform cubic-bezier(0,0,.2,1),-webkit-transform cubic-bezier(0,0,.2,1);-webkit-transform:scale(0);transform:scale(0)}@media (-ms-high-contrast:active){.mat-ripple-element{display:none}}.cdk-visually-hidden{border:0;clip:rect(0 0 0 0);height:1px;margin:-1px;overflow:hidden;padding:0;position:absolute;width:1px;outline:0;-webkit-appearance:none;-moz-appearance:none}.cdk-global-overlay-wrapper,.cdk-overlay-container{pointer-events:none;top:0;left:0;height:100%;width:100%}.cdk-overlay-container{position:fixed;z-index:1000}.cdk-overlay-container:empty{display:none}.cdk-global-overlay-wrapper{display:-webkit-box;display:flex;position:absolute;z-index:1000}.cdk-overlay-pane{position:absolute;pointer-events:auto;box-sizing:border-box;z-index:1000;display:-webkit-box;display:flex;max-width:100%;max-height:100%}.cdk-overlay-backdrop{position:absolute;top:0;bottom:0;left:0;right:0;z-index:1000;pointer-events:auto;-webkit-tap-highlight-color:transparent;-webkit-transition:opacity .4s cubic-bezier(.25,.8,.25,1);transition:opacity .4s cubic-bezier(.25,.8,.25,1);opacity:0}.cdk-overlay-backdrop.cdk-overlay-backdrop-showing{opacity:1}@media screen and (-ms-high-contrast:active){.cdk-overlay-backdrop.cdk-overlay-backdrop-showing{opacity:.6}}.cdk-overlay-dark-backdrop{background:rgba(0,0,0,.32)}.cdk-overlay-transparent-backdrop,.cdk-overlay-transparent-backdrop.cdk-overlay-backdrop-showing{opacity:0}.cdk-overlay-connected-position-bounding-box{position:absolute;z-index:1000;display:-webkit-box;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;flex-direction:column;min-width:1px;min-height:1px}.cdk-global-scrollblock{position:fixed;width:100%;overflow-y:scroll}@-webkit-keyframes cdk-text-field-autofill-start{/*!*/}@keyframes cdk-text-field-autofill-start{/*!*/}@-webkit-keyframes cdk-text-field-autofill-end{/*!*/}@keyframes cdk-text-field-autofill-end{/*!*/}.cdk-text-field-autofill-monitored:-webkit-autofill{-webkit-animation-name:cdk-text-field-autofill-start;animation-name:cdk-text-field-autofill-start}.cdk-text-field-autofill-monitored:not(:-webkit-autofill){-webkit-animation-name:cdk-text-field-autofill-end;animation-name:cdk-text-field-autofill-end}textarea.cdk-textarea-autosize{resize:none}textarea.cdk-textarea-autosize-measuring{height:auto!important;overflow:hidden!important;padding:2px 0!important;box-sizing:content-box!important}.status-table-icon{margin-left:5px}.automation-cell-container{display:-webkit-box;display:flex;-webkit-box-align:center;align-items:center}.ff-icon,.play-icon{color:#69ff8f}.auto-off-ff-icon,.pause-icon{color:#1e88e5}.stop-icon{color:#ff5454}.flair-icon{color:#009688}"]
            }] }
];
/** @nocollapse */
AutomationCellRendererComponent.ctorParameters = () => [];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class ClickToolCellRendererComponent {
    constructor() {
        this.clearChatIcon = {
            material_icon: 'check_circle'
        };
    }
    /**
     * @param {?} params
     * @return {?}
     */
    agInit(params) {
        // Row data to make the icon useful
        this.clearChatIcon.value = params.value;
        this.clearChatIcon.iconTitle = params.colDef.iconTitle || '';
        this.clearChatIcon.boreId = params.data.wellboreId;
    }
    /**
     * @return {?}
     */
    refresh() {
        return false;
    }
}
ClickToolCellRendererComponent.decorators = [
    { type: Component, args: [{
                selector: 'app-click-tool-cell-renderer',
                template: "<div class=\"has-new-chat-container\">\n  <span class=\"material-icons\" class=\"icon flag\" *ngIf=\"clearChatIcon.value === 'Yes'\">\n    <mat-icon>flag</mat-icon>\n  </span>\n  {{clearChatIcon.value}}\n  <i class=\"material-icons icon tooling\"\n     aria-label=\"icon-button that clears the chat\" [id]=\"'clearChat_'+clearChatIcon.boreId\"\n     *ngIf=\"clearChatIcon.value === 'Yes'\" [title]=\"clearChatIcon.iconTitle\">{{clearChatIcon.material_icon}}</i>\n  <!-- optional button -->\n  <!--<button mat-button class=\"tooling-button\" *ngIf=\"chat.showClear\" [title]=\"chat.iconTitle\" (click)=\"clearIndicator($event, chat.data)\">\n    Clear Chat\n  </button>-->\n</div>\n",
                styles: ["@media print{.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-appearance-legacy.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-1.28122em) scale(.75);transform:translateY(-1.28122em) scale(.75)}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-form-field-autofill-control:-webkit-autofill+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.28121em) scale(.75);transform:translateY(-1.28121em) scale(.75)}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.2812em) scale(.75);transform:translateY(-1.2812em) scale(.75)}}.mat-badge-content{font-weight:600;font-size:12px;font-family:NunitoSans,sans-serif}.mat-badge-small .mat-badge-content{font-size:9px}.mat-badge-large .mat-badge-content{font-size:24px}.mat-h1,.mat-headline,.mat-typography h1{font:400 24px/32px NunitoSans,sans-serif;margin:0 0 16px}.mat-h2,.mat-title,.mat-typography h2{font:500 20px/32px NunitoSans,sans-serif;margin:0 0 16px}.mat-h3,.mat-subheading-2,.mat-typography h3{font:400 16px/28px NunitoSans,sans-serif;margin:0 0 16px}.mat-h4,.mat-subheading-1,.mat-typography h4{font:400 15px/24px NunitoSans,sans-serif;margin:0 0 16px}.mat-h5,.mat-h6,.mat-typography h5,.mat-typography h6{margin:0 0 12px}.mat-body-2,.mat-body-strong{font:500 14px/24px NunitoSans,sans-serif}.mat-body,.mat-body-1,.mat-typography{font:400 14px/20px NunitoSans,sans-serif}.mat-body p,.mat-body-1 p,.mat-typography p{margin:0 0 12px}.mat-caption,.mat-small{font:400 12px/20px NunitoSans,sans-serif}.mat-display-4,.mat-typography .mat-display-4{font:300 112px/112px NunitoSans,sans-serif;letter-spacing:-.05em;margin:0 0 56px}.mat-display-3,.mat-typography .mat-display-3{font:400 56px/56px NunitoSans,sans-serif;letter-spacing:-.02em;margin:0 0 64px}.mat-display-2,.mat-typography .mat-display-2{font:400 45px/48px NunitoSans,sans-serif;letter-spacing:-.005em;margin:0 0 64px}.mat-display-1,.mat-typography .mat-display-1{font:400 34px/40px NunitoSans,sans-serif;margin:0 0 64px}.mat-bottom-sheet-container{font:400 14px/20px NunitoSans,sans-serif}.mat-button,.mat-fab,.mat-flat-button,.mat-icon-button,.mat-mini-fab,.mat-raised-button,.mat-stroked-button{font-family:NunitoSans,sans-serif;font-size:14px;font-weight:500}.mat-button-toggle,.mat-card{font-family:NunitoSans,sans-serif}.mat-card-title{font-size:24px;font-weight:500}.mat-card-header .mat-card-title{font-size:20px}.mat-card-content,.mat-card-subtitle{font-size:14px}.mat-checkbox{font-family:NunitoSans,sans-serif}.mat-checkbox-layout .mat-checkbox-label{line-height:24px}.mat-chip{font-size:14px;font-weight:500}.mat-chip .mat-chip-remove.mat-icon,.mat-chip .mat-chip-trailing-icon.mat-icon{font-size:18px}.mat-table{font-family:NunitoSans,sans-serif}.mat-header-cell{font-size:12px;font-weight:500}.mat-cell,.mat-footer-cell{font-size:14px}.mat-calendar{font-family:NunitoSans,sans-serif}.mat-calendar-body{font-size:13px}.mat-calendar-body-label,.mat-calendar-period-button{font-size:14px;font-weight:500}.mat-calendar-table-header th{font-size:11px;font-weight:400}.mat-dialog-title{font:500 20px/32px NunitoSans,sans-serif}.mat-expansion-panel-header{font-family:NunitoSans,sans-serif;font-size:15px;font-weight:400}.mat-expansion-panel-content{font:400 14px/20px NunitoSans,sans-serif}.mat-form-field{font-size:inherit;font-weight:400;line-height:1.125;font-family:NunitoSans,sans-serif}.mat-form-field-wrapper{padding-bottom:1.34375em}.mat-form-field-prefix .mat-icon,.mat-form-field-suffix .mat-icon{font-size:150%;line-height:1.125}.mat-form-field-prefix .mat-icon-button,.mat-form-field-suffix .mat-icon-button{height:1.5em;width:1.5em}.mat-form-field-prefix .mat-icon-button .mat-icon,.mat-form-field-suffix .mat-icon-button .mat-icon{height:1.125em;line-height:1.125}.mat-form-field-infix{padding:.5em 0;border-top:.84375em solid transparent}.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-1.34373em) scale(.75);transform:translateY(-1.34373em) scale(.75);width:133.3333533333%}.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.34372em) scale(.75);transform:translateY(-1.34372em) scale(.75);width:133.3333633333%}.mat-form-field-label-wrapper{top:-.84375em;padding-top:.84375em}.mat-form-field-label{top:1.34375em}.mat-form-field-underline{bottom:1.34375em}.mat-form-field-subscript-wrapper{font-size:75%;margin-top:.6666666667em;top:calc(100% - 1.7916666667em)}.mat-form-field-appearance-legacy .mat-form-field-wrapper{padding-bottom:1.25em}.mat-form-field-appearance-legacy .mat-form-field-infix{padding:.4375em 0}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-appearance-legacy.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00106px);transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00106px);-ms-transform:translateY(-1.28119em) scale(.75);width:133.3333933333%}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-form-field-autofill-control:-webkit-autofill+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00107px);transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00107px);-ms-transform:translateY(-1.28118em) scale(.75);width:133.3334033333%}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00108px);transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00108px);-ms-transform:translateY(-1.28117em) scale(.75);width:133.3334133333%}.mat-form-field-appearance-legacy .mat-form-field-label{top:1.28125em}.mat-form-field-appearance-legacy .mat-form-field-underline{bottom:1.25em}.mat-form-field-appearance-legacy .mat-form-field-subscript-wrapper{margin-top:.5416666667em;top:calc(100% - 1.6666666667em)}@media print{.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-appearance-legacy.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-1.28116em) scale(.75);transform:translateY(-1.28116em) scale(.75)}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-form-field-autofill-control:-webkit-autofill+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.28115em) scale(.75);transform:translateY(-1.28115em) scale(.75)}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.28114em) scale(.75);transform:translateY(-1.28114em) scale(.75)}}.mat-form-field-appearance-fill .mat-form-field-infix{padding:.25em 0 .75em}.mat-form-field-appearance-fill .mat-form-field-label{top:1.09375em;margin-top:-.5em}.mat-form-field-appearance-fill.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-appearance-fill.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-.59373em) scale(.75);transform:translateY(-.59373em) scale(.75);width:133.3333533333%}.mat-form-field-appearance-fill.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-.59372em) scale(.75);transform:translateY(-.59372em) scale(.75);width:133.3333633333%}.mat-form-field-appearance-outline .mat-form-field-infix{padding:1em 0}.mat-form-field-appearance-outline .mat-form-field-label{top:1.84375em;margin-top:-.25em}.mat-form-field-appearance-outline.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-appearance-outline.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-1.59373em) scale(.75);transform:translateY(-1.59373em) scale(.75);width:133.3333533333%}.mat-form-field-appearance-outline.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.59372em) scale(.75);transform:translateY(-1.59372em) scale(.75);width:133.3333633333%}.mat-grid-tile-footer,.mat-grid-tile-header{font-size:14px}.mat-grid-tile-footer .mat-line,.mat-grid-tile-header .mat-line{white-space:nowrap;overflow:hidden;text-overflow:ellipsis;display:block;box-sizing:border-box}.mat-grid-tile-footer .mat-line:nth-child(n+2),.mat-grid-tile-header .mat-line:nth-child(n+2){font-size:12px}input.mat-input-element{margin-top:-.0625em}.mat-menu-item{font-family:NunitoSans,sans-serif;font-size:14px;font-weight:400}.mat-paginator,.mat-paginator-page-size .mat-select-trigger{font-family:NunitoSans,sans-serif;font-size:12px}.mat-radio-button,.mat-select{font-family:NunitoSans,sans-serif}.mat-select-trigger{height:1.125em}.mat-slide-toggle-content{font-family:NunitoSans,sans-serif}.mat-slider-thumb-label-text{font-family:NunitoSans,sans-serif;font-size:12px;font-weight:500}.mat-stepper-horizontal,.mat-stepper-vertical{font-family:NunitoSans,sans-serif}.mat-step-label{font-size:14px;font-weight:400}.mat-step-sub-label-error{font-weight:400}.mat-step-label-error{font-size:14px}.mat-step-label-selected{font-size:14px;font-weight:500}.mat-tab-group{font-family:NunitoSans,sans-serif}.mat-tab-label,.mat-tab-link{font-family:NunitoSans,sans-serif;font-size:14px;font-weight:500}.mat-toolbar,.mat-toolbar h1,.mat-toolbar h2,.mat-toolbar h3,.mat-toolbar h4,.mat-toolbar h5,.mat-toolbar h6{font:500 20px/32px NunitoSans,sans-serif;margin:0}.mat-tooltip{font-family:NunitoSans,sans-serif;font-size:10px;padding-top:6px;padding-bottom:6px}.mat-tooltip-handset{font-size:14px;padding-top:8px;padding-bottom:8px}.mat-list-item,.mat-list-option{font-family:NunitoSans,sans-serif}.mat-list-base .mat-list-item{font-size:16px}.mat-list-base .mat-list-item .mat-line{white-space:nowrap;overflow:hidden;text-overflow:ellipsis;display:block;box-sizing:border-box}.mat-list-base .mat-list-item .mat-line:nth-child(n+2){font-size:14px}.mat-list-base .mat-list-option{font-size:16px}.mat-list-base .mat-list-option .mat-line{white-space:nowrap;overflow:hidden;text-overflow:ellipsis;display:block;box-sizing:border-box}.mat-list-base .mat-list-option .mat-line:nth-child(n+2){font-size:14px}.mat-list-base .mat-subheader{font-family:NunitoSans,sans-serif;font-size:14px;font-weight:500}.mat-list-base[dense] .mat-list-item{font-size:12px}.mat-list-base[dense] .mat-list-item .mat-line{white-space:nowrap;overflow:hidden;text-overflow:ellipsis;display:block;box-sizing:border-box}.mat-list-base[dense] .mat-list-item .mat-line:nth-child(n+2),.mat-list-base[dense] .mat-list-option{font-size:12px}.mat-list-base[dense] .mat-list-option .mat-line{white-space:nowrap;overflow:hidden;text-overflow:ellipsis;display:block;box-sizing:border-box}.mat-list-base[dense] .mat-list-option .mat-line:nth-child(n+2){font-size:12px}.mat-list-base[dense] .mat-subheader{font-family:NunitoSans,sans-serif;font-size:12px;font-weight:500}.mat-option{font-family:NunitoSans,sans-serif;font-size:16px}.mat-optgroup-label{font:500 14px/24px NunitoSans,sans-serif}.mat-simple-snackbar{font-family:NunitoSans,sans-serif;font-size:14px}.mat-simple-snackbar-action{line-height:1;font-family:inherit;font-size:inherit;font-weight:500}.mat-tree{font-family:NunitoSans,sans-serif}.mat-nested-tree-node,.mat-tree-node{font-weight:400;font-size:14px}.mat-ripple{overflow:hidden;position:relative}.mat-ripple.mat-ripple-unbounded{overflow:visible}.mat-ripple-element{position:absolute;border-radius:50%;pointer-events:none;-webkit-transition:opacity,-webkit-transform cubic-bezier(0,0,.2,1);transition:opacity,transform cubic-bezier(0,0,.2,1),-webkit-transform cubic-bezier(0,0,.2,1);-webkit-transform:scale(0);transform:scale(0)}@media (-ms-high-contrast:active){.mat-ripple-element{display:none}}.cdk-visually-hidden{border:0;clip:rect(0 0 0 0);height:1px;margin:-1px;overflow:hidden;padding:0;position:absolute;width:1px;outline:0;-webkit-appearance:none;-moz-appearance:none}.cdk-global-overlay-wrapper,.cdk-overlay-container{pointer-events:none;top:0;left:0;height:100%;width:100%}.cdk-overlay-container{position:fixed;z-index:1000}.cdk-overlay-container:empty{display:none}.cdk-global-overlay-wrapper{display:-webkit-box;display:flex;position:absolute;z-index:1000}.cdk-overlay-pane{position:absolute;pointer-events:auto;box-sizing:border-box;z-index:1000;display:-webkit-box;display:flex;max-width:100%;max-height:100%}.cdk-overlay-backdrop{position:absolute;top:0;bottom:0;left:0;right:0;z-index:1000;pointer-events:auto;-webkit-tap-highlight-color:transparent;-webkit-transition:opacity .4s cubic-bezier(.25,.8,.25,1);transition:opacity .4s cubic-bezier(.25,.8,.25,1);opacity:0}.cdk-overlay-backdrop.cdk-overlay-backdrop-showing{opacity:1}@media screen and (-ms-high-contrast:active){.cdk-overlay-backdrop.cdk-overlay-backdrop-showing{opacity:.6}}.cdk-overlay-dark-backdrop{background:rgba(0,0,0,.32)}.cdk-overlay-transparent-backdrop,.cdk-overlay-transparent-backdrop.cdk-overlay-backdrop-showing{opacity:0}.cdk-overlay-connected-position-bounding-box{position:absolute;z-index:1000;display:-webkit-box;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;flex-direction:column;min-width:1px;min-height:1px}.cdk-global-scrollblock{position:fixed;width:100%;overflow-y:scroll}@-webkit-keyframes cdk-text-field-autofill-start{/*!*/}@keyframes cdk-text-field-autofill-start{/*!*/}@-webkit-keyframes cdk-text-field-autofill-end{/*!*/}@keyframes cdk-text-field-autofill-end{/*!*/}.cdk-text-field-autofill-monitored:-webkit-autofill{-webkit-animation-name:cdk-text-field-autofill-start;animation-name:cdk-text-field-autofill-start}.cdk-text-field-autofill-monitored:not(:-webkit-autofill){-webkit-animation-name:cdk-text-field-autofill-end;animation-name:cdk-text-field-autofill-end}textarea.cdk-textarea-autosize{resize:none}textarea.cdk-textarea-autosize-measuring{height:auto!important;overflow:hidden!important;padding:2px 0!important;box-sizing:content-box!important}.has-new-chat-container{display:-webkit-box;display:flex;-webkit-box-orient:horizontal;-webkit-box-direction:normal;flex-direction:row;flex-wrap:nowrap;-webkit-box-align:center;align-items:center;-webkit-box-pack:space-evenly;justify-content:space-evenly}.has-new-chat-container .icon{color:#ff2121;display:-webkit-box;display:flex}.has-new-chat-container .icon.flag{color:#ff2121}.has-new-chat-container .icon.tooling{color:#1976d2;padding:0 0 0 8px;cursor:pointer;-webkit-transition:color .3s ease-in-out;transition:color .3s ease-in-out}.has-new-chat-container .icon.tooling:hover{color:#64b5f6;-webkit-transition:color .3s ease-in-out;transition:color .3s ease-in-out}.has-new-chat-container .tooling-button{border:1px solid #ff2121;margin:0 10px;border-radius:3px;padding:1px 2px;color:#ff2121;background:#64b5f6;line-height:18px;cursor:pointer;font-size:.9em}.has-new-chat-container .tooling-button:hover{color:#64b5f6;-webkit-transition:color .3s ease-in-out;transition:color .3s ease-in-out}"]
            }] }
];
/** @nocollapse */
ClickToolCellRendererComponent.ctorParameters = () => [];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class DropdownCellRendererComponent {
    constructor() { }
    /**
     * @param {?} params
     * @return {?}
     */
    agInit(params) {
        // Row data
        this.value = params.value;
        this.icon = params.colDef.iconCell;
    }
    /**
     * @return {?}
     */
    refresh() {
        return false;
    }
}
DropdownCellRendererComponent.decorators = [
    { type: Component, args: [{
                selector: 'app-dropdown-cell-renderer',
                template: "<div class=\"dropdown-container\">\n  <span class=\"material-icons icon\">\n    <mat-icon [ngStyle]=\"!value ? {'margin-top': '10px'} : ''\">arrow_drop_down</mat-icon>\n  </span>\n  <span>{{value}}</span>\n</div>\n",
                styles: ["@media print{.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-appearance-legacy.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-1.28122em) scale(.75);transform:translateY(-1.28122em) scale(.75)}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-form-field-autofill-control:-webkit-autofill+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.28121em) scale(.75);transform:translateY(-1.28121em) scale(.75)}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.2812em) scale(.75);transform:translateY(-1.2812em) scale(.75)}}.mat-badge-content{font-weight:600;font-size:12px;font-family:NunitoSans,sans-serif}.mat-badge-small .mat-badge-content{font-size:9px}.mat-badge-large .mat-badge-content{font-size:24px}.mat-h1,.mat-headline,.mat-typography h1{font:400 24px/32px NunitoSans,sans-serif;margin:0 0 16px}.mat-h2,.mat-title,.mat-typography h2{font:500 20px/32px NunitoSans,sans-serif;margin:0 0 16px}.mat-h3,.mat-subheading-2,.mat-typography h3{font:400 16px/28px NunitoSans,sans-serif;margin:0 0 16px}.mat-h4,.mat-subheading-1,.mat-typography h4{font:400 15px/24px NunitoSans,sans-serif;margin:0 0 16px}.mat-h5,.mat-h6,.mat-typography h5,.mat-typography h6{margin:0 0 12px}.mat-body-2,.mat-body-strong{font:500 14px/24px NunitoSans,sans-serif}.mat-body,.mat-body-1,.mat-typography{font:400 14px/20px NunitoSans,sans-serif}.mat-body p,.mat-body-1 p,.mat-typography p{margin:0 0 12px}.mat-caption,.mat-small{font:400 12px/20px NunitoSans,sans-serif}.mat-display-4,.mat-typography .mat-display-4{font:300 112px/112px NunitoSans,sans-serif;letter-spacing:-.05em;margin:0 0 56px}.mat-display-3,.mat-typography .mat-display-3{font:400 56px/56px NunitoSans,sans-serif;letter-spacing:-.02em;margin:0 0 64px}.mat-display-2,.mat-typography .mat-display-2{font:400 45px/48px NunitoSans,sans-serif;letter-spacing:-.005em;margin:0 0 64px}.mat-display-1,.mat-typography .mat-display-1{font:400 34px/40px NunitoSans,sans-serif;margin:0 0 64px}.mat-bottom-sheet-container{font:400 14px/20px NunitoSans,sans-serif}.mat-button,.mat-fab,.mat-flat-button,.mat-icon-button,.mat-mini-fab,.mat-raised-button,.mat-stroked-button{font-family:NunitoSans,sans-serif;font-size:14px;font-weight:500}.mat-button-toggle,.mat-card{font-family:NunitoSans,sans-serif}.mat-card-title{font-size:24px;font-weight:500}.mat-card-header .mat-card-title{font-size:20px}.mat-card-content,.mat-card-subtitle{font-size:14px}.mat-checkbox{font-family:NunitoSans,sans-serif}.mat-checkbox-layout .mat-checkbox-label{line-height:24px}.mat-chip{font-size:14px;font-weight:500}.mat-chip .mat-chip-remove.mat-icon,.mat-chip .mat-chip-trailing-icon.mat-icon{font-size:18px}.mat-table{font-family:NunitoSans,sans-serif}.mat-header-cell{font-size:12px;font-weight:500}.mat-cell,.mat-footer-cell{font-size:14px}.mat-calendar{font-family:NunitoSans,sans-serif}.mat-calendar-body{font-size:13px}.mat-calendar-body-label,.mat-calendar-period-button{font-size:14px;font-weight:500}.mat-calendar-table-header th{font-size:11px;font-weight:400}.mat-dialog-title{font:500 20px/32px NunitoSans,sans-serif}.mat-expansion-panel-header{font-family:NunitoSans,sans-serif;font-size:15px;font-weight:400}.mat-expansion-panel-content{font:400 14px/20px NunitoSans,sans-serif}.mat-form-field{font-size:inherit;font-weight:400;line-height:1.125;font-family:NunitoSans,sans-serif}.mat-form-field-wrapper{padding-bottom:1.34375em}.mat-form-field-prefix .mat-icon,.mat-form-field-suffix .mat-icon{font-size:150%;line-height:1.125}.mat-form-field-prefix .mat-icon-button,.mat-form-field-suffix .mat-icon-button{height:1.5em;width:1.5em}.mat-form-field-prefix .mat-icon-button .mat-icon,.mat-form-field-suffix .mat-icon-button .mat-icon{height:1.125em;line-height:1.125}.mat-form-field-infix{padding:.5em 0;border-top:.84375em solid transparent}.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-1.34373em) scale(.75);transform:translateY(-1.34373em) scale(.75);width:133.3333533333%}.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.34372em) scale(.75);transform:translateY(-1.34372em) scale(.75);width:133.3333633333%}.mat-form-field-label-wrapper{top:-.84375em;padding-top:.84375em}.mat-form-field-label{top:1.34375em}.mat-form-field-underline{bottom:1.34375em}.mat-form-field-subscript-wrapper{font-size:75%;margin-top:.6666666667em;top:calc(100% - 1.7916666667em)}.mat-form-field-appearance-legacy .mat-form-field-wrapper{padding-bottom:1.25em}.mat-form-field-appearance-legacy .mat-form-field-infix{padding:.4375em 0}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-appearance-legacy.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00106px);transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00106px);-ms-transform:translateY(-1.28119em) scale(.75);width:133.3333933333%}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-form-field-autofill-control:-webkit-autofill+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00107px);transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00107px);-ms-transform:translateY(-1.28118em) scale(.75);width:133.3334033333%}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00108px);transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00108px);-ms-transform:translateY(-1.28117em) scale(.75);width:133.3334133333%}.mat-form-field-appearance-legacy .mat-form-field-label{top:1.28125em}.mat-form-field-appearance-legacy .mat-form-field-underline{bottom:1.25em}.mat-form-field-appearance-legacy .mat-form-field-subscript-wrapper{margin-top:.5416666667em;top:calc(100% - 1.6666666667em)}@media print{.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-appearance-legacy.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-1.28116em) scale(.75);transform:translateY(-1.28116em) scale(.75)}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-form-field-autofill-control:-webkit-autofill+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.28115em) scale(.75);transform:translateY(-1.28115em) scale(.75)}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.28114em) scale(.75);transform:translateY(-1.28114em) scale(.75)}}.mat-form-field-appearance-fill .mat-form-field-infix{padding:.25em 0 .75em}.mat-form-field-appearance-fill .mat-form-field-label{top:1.09375em;margin-top:-.5em}.mat-form-field-appearance-fill.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-appearance-fill.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-.59373em) scale(.75);transform:translateY(-.59373em) scale(.75);width:133.3333533333%}.mat-form-field-appearance-fill.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-.59372em) scale(.75);transform:translateY(-.59372em) scale(.75);width:133.3333633333%}.mat-form-field-appearance-outline .mat-form-field-infix{padding:1em 0}.mat-form-field-appearance-outline .mat-form-field-label{top:1.84375em;margin-top:-.25em}.mat-form-field-appearance-outline.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-appearance-outline.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-1.59373em) scale(.75);transform:translateY(-1.59373em) scale(.75);width:133.3333533333%}.mat-form-field-appearance-outline.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.59372em) scale(.75);transform:translateY(-1.59372em) scale(.75);width:133.3333633333%}.mat-grid-tile-footer,.mat-grid-tile-header{font-size:14px}.mat-grid-tile-footer .mat-line,.mat-grid-tile-header .mat-line{white-space:nowrap;overflow:hidden;text-overflow:ellipsis;display:block;box-sizing:border-box}.mat-grid-tile-footer .mat-line:nth-child(n+2),.mat-grid-tile-header .mat-line:nth-child(n+2){font-size:12px}input.mat-input-element{margin-top:-.0625em}.mat-menu-item{font-family:NunitoSans,sans-serif;font-size:14px;font-weight:400}.mat-paginator,.mat-paginator-page-size .mat-select-trigger{font-family:NunitoSans,sans-serif;font-size:12px}.mat-radio-button,.mat-select{font-family:NunitoSans,sans-serif}.mat-select-trigger{height:1.125em}.mat-slide-toggle-content{font-family:NunitoSans,sans-serif}.mat-slider-thumb-label-text{font-family:NunitoSans,sans-serif;font-size:12px;font-weight:500}.mat-stepper-horizontal,.mat-stepper-vertical{font-family:NunitoSans,sans-serif}.mat-step-label{font-size:14px;font-weight:400}.mat-step-sub-label-error{font-weight:400}.mat-step-label-error{font-size:14px}.mat-step-label-selected{font-size:14px;font-weight:500}.mat-tab-group{font-family:NunitoSans,sans-serif}.mat-tab-label,.mat-tab-link{font-family:NunitoSans,sans-serif;font-size:14px;font-weight:500}.mat-toolbar,.mat-toolbar h1,.mat-toolbar h2,.mat-toolbar h3,.mat-toolbar h4,.mat-toolbar h5,.mat-toolbar h6{font:500 20px/32px NunitoSans,sans-serif;margin:0}.mat-tooltip{font-family:NunitoSans,sans-serif;font-size:10px;padding-top:6px;padding-bottom:6px}.mat-tooltip-handset{font-size:14px;padding-top:8px;padding-bottom:8px}.mat-list-item,.mat-list-option{font-family:NunitoSans,sans-serif}.mat-list-base .mat-list-item{font-size:16px}.mat-list-base .mat-list-item .mat-line{white-space:nowrap;overflow:hidden;text-overflow:ellipsis;display:block;box-sizing:border-box}.mat-list-base .mat-list-item .mat-line:nth-child(n+2){font-size:14px}.mat-list-base .mat-list-option{font-size:16px}.mat-list-base .mat-list-option .mat-line{white-space:nowrap;overflow:hidden;text-overflow:ellipsis;display:block;box-sizing:border-box}.mat-list-base .mat-list-option .mat-line:nth-child(n+2){font-size:14px}.mat-list-base .mat-subheader{font-family:NunitoSans,sans-serif;font-size:14px;font-weight:500}.mat-list-base[dense] .mat-list-item{font-size:12px}.mat-list-base[dense] .mat-list-item .mat-line{white-space:nowrap;overflow:hidden;text-overflow:ellipsis;display:block;box-sizing:border-box}.mat-list-base[dense] .mat-list-item .mat-line:nth-child(n+2),.mat-list-base[dense] .mat-list-option{font-size:12px}.mat-list-base[dense] .mat-list-option .mat-line{white-space:nowrap;overflow:hidden;text-overflow:ellipsis;display:block;box-sizing:border-box}.mat-list-base[dense] .mat-list-option .mat-line:nth-child(n+2){font-size:12px}.mat-list-base[dense] .mat-subheader{font-family:NunitoSans,sans-serif;font-size:12px;font-weight:500}.mat-option{font-family:NunitoSans,sans-serif;font-size:16px}.mat-optgroup-label{font:500 14px/24px NunitoSans,sans-serif}.mat-simple-snackbar{font-family:NunitoSans,sans-serif;font-size:14px}.mat-simple-snackbar-action{line-height:1;font-family:inherit;font-size:inherit;font-weight:500}.mat-tree{font-family:NunitoSans,sans-serif}.mat-nested-tree-node,.mat-tree-node{font-weight:400;font-size:14px}.mat-ripple{overflow:hidden;position:relative}.mat-ripple.mat-ripple-unbounded{overflow:visible}.mat-ripple-element{position:absolute;border-radius:50%;pointer-events:none;-webkit-transition:opacity,-webkit-transform cubic-bezier(0,0,.2,1);transition:opacity,transform cubic-bezier(0,0,.2,1),-webkit-transform cubic-bezier(0,0,.2,1);-webkit-transform:scale(0);transform:scale(0)}@media (-ms-high-contrast:active){.mat-ripple-element{display:none}}.cdk-visually-hidden{border:0;clip:rect(0 0 0 0);height:1px;margin:-1px;overflow:hidden;padding:0;position:absolute;width:1px;outline:0;-webkit-appearance:none;-moz-appearance:none}.cdk-global-overlay-wrapper,.cdk-overlay-container{pointer-events:none;top:0;left:0;height:100%;width:100%}.cdk-overlay-container{position:fixed;z-index:1000}.cdk-overlay-container:empty{display:none}.cdk-global-overlay-wrapper{display:-webkit-box;display:flex;position:absolute;z-index:1000}.cdk-overlay-pane{position:absolute;pointer-events:auto;box-sizing:border-box;z-index:1000;display:-webkit-box;display:flex;max-width:100%;max-height:100%}.cdk-overlay-backdrop{position:absolute;top:0;bottom:0;left:0;right:0;z-index:1000;pointer-events:auto;-webkit-tap-highlight-color:transparent;-webkit-transition:opacity .4s cubic-bezier(.25,.8,.25,1);transition:opacity .4s cubic-bezier(.25,.8,.25,1);opacity:0}.cdk-overlay-backdrop.cdk-overlay-backdrop-showing{opacity:1}@media screen and (-ms-high-contrast:active){.cdk-overlay-backdrop.cdk-overlay-backdrop-showing{opacity:.6}}.cdk-overlay-dark-backdrop{background:rgba(0,0,0,.32)}.cdk-overlay-transparent-backdrop,.cdk-overlay-transparent-backdrop.cdk-overlay-backdrop-showing{opacity:0}.cdk-overlay-connected-position-bounding-box{position:absolute;z-index:1000;display:-webkit-box;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;flex-direction:column;min-width:1px;min-height:1px}.cdk-global-scrollblock{position:fixed;width:100%;overflow-y:scroll}@-webkit-keyframes cdk-text-field-autofill-start{/*!*/}@keyframes cdk-text-field-autofill-start{/*!*/}@-webkit-keyframes cdk-text-field-autofill-end{/*!*/}@keyframes cdk-text-field-autofill-end{/*!*/}.cdk-text-field-autofill-monitored:-webkit-autofill{-webkit-animation-name:cdk-text-field-autofill-start;animation-name:cdk-text-field-autofill-start}.cdk-text-field-autofill-monitored:not(:-webkit-autofill){-webkit-animation-name:cdk-text-field-autofill-end;animation-name:cdk-text-field-autofill-end}textarea.cdk-textarea-autosize{resize:none}textarea.cdk-textarea-autosize-measuring{height:auto!important;overflow:hidden!important;padding:2px 0!important;box-sizing:content-box!important}.flag-icon{color:#ff2121;display:-webkit-box;display:flex}.dropdown-container{display:-webkit-box;display:flex;-webkit-box-orient:horizontal;-webkit-box-direction:reverse;flex-direction:row-reverse;-webkit-box-align:center;align-items:center;justify-content:space-around}"]
            }] }
];
/** @nocollapse */
DropdownCellRendererComponent.ctorParameters = () => [];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class CellRendererFactory {
    /**
     * @param {?} column
     * @return {?}
     */
    setRenderer(column) {
        if (column.buttons) {
            column.cellRendererFramework = RowButtonComponent;
        }
        if (column.iconCell === 'flag') {
            column.cellRendererFramework = NewFileCellRendererComponent;
        }
        if (column.iconCell === 'click-tool') {
            column.cellRendererFramework = ClickToolCellRendererComponent;
        }
        if (column.iconCell === 'automation') {
            column.cellRendererFramework = AutomationCellRendererComponent;
        }
        if (column.iconCell === 'dropdown') {
            column.cellRendererFramework = DropdownCellRendererComponent;
        }
    }
}
CellRendererFactory.decorators = [
    { type: Injectable }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class CustomLoadingOverlayComponent {
    /**
     * @param {?} params
     * @return {?}
     */
    agInit(params) {
        this.params = params;
    }
}
CustomLoadingOverlayComponent.decorators = [
    { type: Component, args: [{
                selector: 'app-loading-overlay',
                template: "<div class=\"ag-overlay-loading-center\" style=\"height: 9%\">\n  <mat-spinner></mat-spinner>\n  <div>{{this.params.loadingMessage}}</div>\n</div>\n"
            }] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class DatePickerCellEditRendererComponent {
    /**
     * @param {?} timeService
     */
    constructor(timeService) {
        this.timeService = timeService;
    }
    /**
     * @return {?}
     */
    ngAfterViewInit() {
        this.picker.open();
    }
    /**
     * @return {?}
     */
    isPopup() {
        return false;
    }
    /**
     * @return {?}
     */
    isCancelBeforeStart() {
        return false;
    }
    /**
     * @return {?}
     */
    isCancelAfterEnd() {
        return false;
    }
    /**
     * @param {?} params
     * @return {?}
     */
    agInit(params) {
        this.params = params;
        this.value = params.value;
    }
    /**
     * @return {?}
     */
    getValue() {
        return this.timeService.standardDateFormatter(this.value, 'MM/DD/YYYY');
    }
    /**
     * @param {?} e
     * @return {?}
     */
    onSelectChange(e) {
        this.params.stopEditing();
    }
}
DatePickerCellEditRendererComponent.decorators = [
    { type: Component, args: [{
                selector: 'app-editor-cell',
                template: "<mat-form-field>\n  <input matInput [matDatepicker]=\"picker\" [(ngModel)]=\"value\" (dateInput)=\"onSelectChange($event)\">\n  <mat-datepicker-toggle matSuffix [for]=\"picker\"></mat-datepicker-toggle>\n  <mat-datepicker #picker></mat-datepicker>\n</mat-form-field>",
                styles: [".md-form-field{margin-top:-16px}"]
            }] }
];
/** @nocollapse */
DatePickerCellEditRendererComponent.ctorParameters = () => [
    { type: TimeService }
];
DatePickerCellEditRendererComponent.propDecorators = {
    picker: [{ type: ViewChild, args: ['picker', { read: MatDatepicker, static: true },] }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class TableComponent {
    // @ViewChild(TableToolsComponent) tableTools: TableToolsComponent;
    /**
     * @param {?} cellFactory
     * @param {?} defaults
     */
    constructor(cellFactory, defaults) {
        this.cellFactory = cellFactory;
        this.defaults = defaults;
        this.columnNames = [];
        this.filterableColumns = [];
        this.filterTerms = new Subject();
        this.gridOptions = {};
        this.toolPanelVisibility = 'hidden';
        this.overlayLoadingTemplate = '<span class="ag-overlay-loading-center" >Please wait while your rows are loading</span>';
        this.loadingText = 'Please Wait While Your Data is Loading';
        this._data = null;
        /* tslint:disable-next-line:no-output-rename */
        this.exportChangedValue = new EventEmitter();
        this.suppressContextMenu = false;
        /* tslint:disable-next-line:no-output-rename */
        this.searchTerms = new EventEmitter();
        /* tslint:disable-next-line:no-output-rename */
        this.exportClickedRow = new EventEmitter();
        /* tslint:disable-next-line:no-output-rename */
        this.exportClickedCell = new EventEmitter();
        this.draw = false;
        this.tableHeight = '';
        this.extraToolBtnUsed = new EventEmitter();
        /* tslint:disable-next-line:no-output-rename */
        this.buttonOutput = new EventEmitter();
        /* tslint:disable-next-line:no-output-rename */
        this.contextMenuOutput = new EventEmitter();
        /// used for tools in the chat cell, emits the
        /* tslint:disable-next-line:no-output-rename */
        this.exportToolClick = new EventEmitter();
    }
    /**
     * @param {?} theContextMenu
     * @return {?}
     */
    set getContextMenuItemsFromComponent(theContextMenu) {
        if (!theContextMenu) {
            return;
        }
        this.getContextMenuItems = theContextMenu;
    }
    /**
     * @param {?} search
     * @return {?}
     */
    set updateFilter(search) {
        if (search && Object.keys(search).length) {
            this.gridApi.setFilterModel(search);
        }
        else if (search === {}) {
            this.gridApi.setFilterModel(null);
        }
    }
    /**
     * @param {?} theData
     * @return {?}
     */
    set data(theData) {
        if (!theData && this.gridApi) {
            this.gridApi.showLoadingOverlay();
        }
        if (!theData) {
            return;
        }
        this._data = theData;
        if (!this.gridApi) {
            return;
        }
        this.gridApi.setRowData(this._data);
    }
    /**
     * @param {?} draw
     * @return {?}
     */
    set redrawRows(draw) {
        this.draw = draw;
        if (this.draw && this.gridApi) {
            this.gridApi.redrawRows();
            this.draw = false;
        }
    }
    /**
     * @param {?} tD
     * @return {?}
     */
    set tableDefinition(tD) {
        if (!tD) {
            return;
        }
        this._tableDef = cloneDeep(tD);
        this.gridOptions = this.createGridOptions();
        this.columnDefs = this.createColumnDefs();
        /** @type {?} */
        const actions = this.manageTableActions();
        if (actions) {
            this.columnDefs.push(actions);
        }
    }
    /**
     * @param {?} value
     * @return {?}
     */
    set height(value) {
        this.tableHeight = value;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.getContextMenuActions = (/**
         * @param {?} params
         * @param {?} eventType
         * @return {?}
         */
        (params, eventType) => this.contextMenuEvent(params, eventType));
        this.frameworkComponents = {
            customLoadingOverlayComponent: CustomLoadingOverlayComponent,
            datePicker: DatePickerCellEditRendererComponent,
            dateTimePicker: DateTimePickerComponent
        };
        this.loadingOverlayComponent = 'customLoadingOverlayComponent';
        this.loadingOverlayComponentParams = { loadingMessage: this.loadingText };
        // returns custom context menu
        if (!this.getContextMenuItems) {
            this.suppressContextMenu = true;
            // The below line can be added back in for a default context menu including, copy cell/line/with & without headers & export
            // this.getContextMenuItems = this.defaults.getDefaults('contextMenu', this.getContextMenuItems);
        }
        this.filterTerms.pipe(distinctUntilChanged()).subscribe((/**
         * @param {?} filterTerm
         * @return {?}
         */
        filterTerm => {
            // Always set the filter string if it changes
            if (!this.gridApi) {
                return;
            }
            this.gridApi.setRowData(this._data);
            this.gridApi.onFilterChanged();
        }));
    }
    /**
     * @param {?} event
     * @return {?}
     */
    onGridReady(event) {
        // this.autoSizeAll();
    }
    // **!! Unnecessary, but kept for posterity
    // autoSizeAll() {
    //   const allColumnIds = [];
    //   this.gridColumnApi.getAllColumns().forEach(function (column) {
    //     console.log('column', column)
    //     allColumnIds.push(column.colId);
    //   });
    //   this.gridColumnApi.autoSizeColumns(allColumnIds);
    // }
    /**
     * @param {?} event
     * @return {?}
     */
    apiReady(event) {
        this.gridApi = event.api;
        this.gridColumnApi = event.columnApi;
        this.gridExportPrefix = this.gridOptions.exportPrefix;
        // this.gridApi.onFilterChanged;
        if (!this._data) {
            this.gridApi.showLoadingOverlay();
        }
        else {
            this.gridApi.setRowData(this._data);
        }
        if (this.sortOnLoad) {
            /** @type {?} */
            const sortModel = [{ colId: this.sortOnLoad, sort: 'desc' }];
            this.gridApi.setSortModel(sortModel);
        }
    }
    /**
     * @return {?}
     */
    onFilterUpdate() {
        if (!this.exportSearchParams) {
            return;
        }
        else {
            /** @type {?} */
            const filteredSearchResults = [];
            this.gridApi.forEachNodeAfterFilter((/**
             * @param {?} rowNode
             * @return {?}
             */
            rowNode => {
                filteredSearchResults[filteredSearchResults.length] =
                    rowNode.data;
            }));
            /** @type {?} */
            const filterTerms = this.gridApi.getFilterModel();
            /** @type {?} */
            const results = {
                searchTerms: filterTerms,
                searchResults: filteredSearchResults
            };
            this.searchTerms.emit(results);
        }
    }
    /**
     * @private
     * @param {?} params
     * @param {?} eventType
     * @return {?}
     */
    contextMenuEvent(params, eventType) {
        /** @type {?} */
        const eventData = {};
        eventData[eventType] = cloneDeep(params.node.data);
        this.contextMenuOutput.emit(eventData);
    }
    /**
     * @param {?} row
     * @return {?}
     */
    onRowClicked(row) {
        switch (true) {
            /* this determines if a tool was clicked, vs a row or cell click*/
            case this.toolWasClicked(row.event.srcElement.id):
                this.exportToolClick.emit({ toolId: row.event.srcElement.id, wellbore: row.data });
                return;
        }
        // tool button was not clicked, proceed as normal
        if (!this.exportRowWhenClicked) {
            return;
        }
        else {
            this.exportClickedRow.emit(row.data);
        }
    }
    /**
     * @param {?} cell
     * @return {?}
     */
    onCellClicked(cell) {
        if (!cell) {
            return;
        }
        this.exportClickedCell.emit(cell);
    }
    /**
     * @param {?} event
     * @return {?}
     */
    onCellValueChanged(event) {
        if (!this.exportCellValueChange) {
            return;
        }
        /** @type {?} */
        const results = { data: event['data'] };
        this.exportChangedValue.emit(results);
    }
    /**
     * @private
     * @return {?}
     */
    createColumnDefs() {
        /** @type {?} */
        const table = this._tableDef;
        /** @type {?} */
        const columnDefs = [];
        for (const col of table.columnDefs) {
            // Set up fuzzy filter columns
            if (col.headerName) {
                this.filterableColumns.push((/** @type {?} */ ({
                    name: col.headerName,
                    field: col.field,
                    includeInSearch: col.includeInSearch
                })));
            }
            // For exporting
            if (col.field) {
                this.columnNames.push(col.field);
            }
            // TODO: checkboxSelection filters
            if (!col.filter && !col.checkboxSelection) {
                col.filter = 'agTextColumnFilter';
            }
            if (col.checkboxSelection === true) {
                col.suppressCopy = true;
            }
            if (col.filter === 'set') {
                col.filterParams = {
                    cellRenderer: this.defaults.updateNullValues
                };
            }
            if (col.filter === 'date') {
                col.filterParams = {
                    clearButton: true,
                    suppressAndOrCondition: true,
                    newRowsAction: 'keep',
                    filterOptions: [
                        'equals',
                        'notEqual',
                        'lessThanOrEqual',
                        'greaterThan',
                        'greaterThanOrEqual',
                        'inRange'
                    ],
                    defaultOption: 'equals',
                    browserDatePicker: true,
                    nullComparator: false,
                    inRangeInclusive: true,
                    comparator: this.defaults.dateComparator
                };
            }
            if (!col.floatingFilterComponentParams) {
                col.floatingFilterComponentParams = {
                    suppressFilterButton: true
                };
            }
            // Cell Renderers
            this.cellFactory.setRenderer(col);
            columnDefs.push(Object.assign({}, this.defaults.getDefaults('colDefDefaults'), col));
        }
        return columnDefs;
    }
    /**
     * @private
     * @return {?}
     */
    createGridOptions() {
        /** @type {?} */
        const defaultGridOptions = Object.assign({}, this.defaults.getDefaults('tableDefDefaults'), this._tableDef.gridOptions, { getRowStyle: this.rowStyleFn, getRowClass: this.rowClassFn, rowClassRules: this.rowClassRules, cellClassRules: this.cellClassRules, cellClassFn: this.cellClassFn });
        if (!defaultGridOptions.context) {
            defaultGridOptions.context = this;
        }
        return (/** @type {?} */ (defaultGridOptions));
    }
    /**
     * @private
     * @return {?}
     */
    manageTableActions() {
        /** @type {?} */
        let actionCol = this._tableDef.actions;
        if (!actionCol) {
            return;
        }
        if (!actionCol.width) {
            // buttons are 45px wide, with 10px of padding inbetween
            actionCol.width =
                45 * actionCol.buttons.length +
                    10 * actionCol.buttons.length -
                    1;
        }
        this.cellFactory.setRenderer(actionCol);
        actionCol = Object.assign({}, this.defaults.getDefaults('actionDefaults'), actionCol);
        return actionCol;
    }
    /**
     * @param {?} column
     * @param {?} event
     * @return {?}
     */
    updateColsToFilter(column, event) {
        event.stopPropagation();
        column.includeInSearch = !column.includeInSearch;
        this.gridOptions.api.setRowData(this._data);
        this.gridOptions.api.onFilterChanged();
    }
    /**
     * @param {?} term
     * @return {?}
     */
    filter(term) {
        this.filterTerms.next(term);
    }
    /**
     * @param {?} calledFromWhere
     * @return {?}
     */
    resizeGrid(calledFromWhere) {
        if (this.gridOptions.api) {
            this.gridOptions.api.sizeColumnsToFit();
        }
        else if (this.gridApi) {
            this.gridApi.sizeColumnsToFit();
        }
    }
    /**
     * @param {?} event
     * @return {?}
     */
    extraToolUsed(event) {
        this.extraToolBtnUsed.emit(event);
    }
    /**
     * @private
     * @param {?} id
     * @return {?}
     */
    toolWasClicked(id) {
        /** @type {?} */
        const chatPatt = new RegExp('((clearChat_)[A-z 0-9]{9,})');
        return chatPatt.test(id);
    }
    /**
     * @return {?}
     */
    ngOnDestroy() { }
}
TableComponent.decorators = [
    { type: Component, args: [{
                selector: 'app-table',
                template: "<mat-card *ngIf=\"gridOptions && columnDefs; else spinner\" class=\"card-for-grids\">\n    <!-- <div class=\"grid-tools\">\n        <div *ngIf=\"tableHeader\" [innerHTML]=\"tableHeader\" class=\"table-header\"></div>\n        <mat-form-field *ngIf=\"!hideGridFuzzyFilter\" class=\"fuzzy-search\">\n            <mat-icon matSuffix class=\"suffix-icon\" [matMenuTriggerFor]=\"fuzzyOptions\" matTooltip=\"Columns to Search\" matTooltipPosition=\"right\">filter_list</mat-icon>\n            <input matInput #filterInput (keyup)=\"filter(filterInput.value)\" placeholder=\"Search\">\n        </mat-form-field>\n        <table-tools [exportPrefix]=\"exportPrefix\" [columnNames]=\"columnNames\" [extraTableToolBtn]=\"extraTableToolBtn\" [gridApi]=\"gridApi\"\n            [gridColumnApi]=\"gridColumnApi\" [gridExportPrefix]=\"gridExportPrefix\" [toolPanelVisibility]=\"toolPanelVisibility\"\n            (extraToolBtnClick)=\"extraToolUsed($event)\" class=\"table-tools\">\n        </table-tools>\n        <mat-menu #fuzzyOptions=\"matMenu\">\n            <div *ngFor=\"let col of filterableColumns\" mat-menu-item (click)=\"updateColsToFilter(col, $event)\">\n                <mat-checkbox [checked]=\"col.includeInSearch\">{{col.name}}</mat-checkbox>\n            </div>\n        </mat-menu>\n    </div> -->\n    <div>\n        <ag-grid-angular\n            #agGrid class=\"ag-theme-material on-card\"\n            [gridOptions]=\"gridOptions\"\n            [columnDefs]=\"columnDefs\"\n            [treeData]=\"treeData\"\n            [getDataPath]=\"getDataPath\"\n            [animateRows]=\"animateRows\"\n            [groupDefaultExpanded]=\"groupDefaultExpanded\"\n            [suppressScrollOnNewData]=\"true\"\n            [autoGroupColumnDef]=\"autoGroupColumnDef\"\n            [icons]=\"icons\"\n            [rowDragManaged]=\"rowDragManaged\"\n            [pinnedTopRowData]=\"pinnedTopRowData\"\n            [suppressClipboardPaste]=\"suppressClipboardPaste\"\n            rowSelection=\"multiple\"\n            [suppressContextMenu]=\"suppressContextMenu\"\n            (gridReady)=\"apiReady($event)\"\n            (gridSizeChanged)=\"resizeGrid('gridSizeChanged')\"\n            (modelUpdated)=\"resizeGrid('modelUpdated')\"\n            [getContextMenuItems]=\"getContextMenuItems\"\n            (rowClicked)=\"onRowClicked($event)\"\n            (rowDoubleClicked)=\"onRowClicked($event)\"\n            (cellClicked)=\"onCellClicked($event)\"\n            (cellValueChanged)=\"onCellValueChanged($event)\"\n            (displayedColumnsChanged)=\"resizeGrid('hideColumns')\"\n            (filterChanged)=\"onFilterUpdate()\"\n            [rememberGroupStateWhenNewData]=\"rememberGroupStateWhenNewData\"\n            [frameworkComponents]=\"frameworkComponents\"\n            [loadingOverlayComponent]=\"loadingOverlayComponent\"\n            [loadingOverlayComponentParams]=\"loadingOverlayComponentParams\"\n            [ngStyle]=\"{'height': (tableHeight === 'auto') ? tableHeight : tableHeight + 'px'}\">\n        </ag-grid-angular>\n    </div>\n</mat-card>\n<ng-template #spinner>\n    <mat-spinner></mat-spinner>\n</ng-template>\n<!-- [overlayLoadingTemplate]=\"overlayLoadingTemplate\" -->\n",
                providers: [CellRendererFactory, TableDefaultsService],
                styles: [".dropdown-menu div.checkbox label{margin-left:10px}.grid-tools{-webkit-box-align:center;align-items:center;background-color:#f3f3f3;border-bottom:1px solid #c3c3c3;padding:5px 10px;display:-webkit-box;display:flex;-webkit-box-pack:justify;justify-content:space-between}.mat-card{padding:0}.on-card{width:100%}.suffix-icon{font-size:20px;cursor:pointer}.table-header{display:inline-block;text-align:center}.table-tools{margin-left:auto;padding-left:10px}"]
            }] }
];
/** @nocollapse */
TableComponent.ctorParameters = () => [
    { type: CellRendererFactory },
    { type: TableDefaultsService }
];
TableComponent.propDecorators = {
    rowClassFn: [{ type: Input }],
    rowClassRules: [{ type: Input }],
    cellClassRules: [{ type: Input }],
    cellClassFn: [{ type: Input }],
    rowStyleFn: [{ type: Input }],
    exportPrefix: [{ type: Input }],
    sortOnLoad: [{ type: Input }],
    tableHeader: [{ type: Input }],
    extraTableToolBtn: [{ type: Input }],
    rowDragManaged: [{ type: Input }],
    pinnedTopRowData: [{ type: Input }],
    suppressClipboardPaste: [{ type: Input }],
    exportCellValueChange: [{ type: Input }],
    exportChangedValue: [{ type: Output, args: ['exportChangedValue',] }],
    getContextMenuItemsFromComponent: [{ type: Input }],
    components: [{ type: Input }],
    groupDefaultExpanded: [{ type: Input }],
    rememberGroupStateWhenNewData: [{ type: Input }],
    animateRows: [{ type: Input }],
    treeData: [{ type: Input }],
    autoGroupColumnDef: [{ type: Input }],
    icons: [{ type: Input }],
    getDataPath: [{ type: Input }],
    suppressContextMenu: [{ type: Input }],
    exportSearchParams: [{ type: Input }],
    searchTerms: [{ type: Output, args: ['search-terms',] }],
    updateFilter: [{ type: Input }],
    exportRowWhenClicked: [{ type: Input }],
    exportClickedRow: [{ type: Output, args: ['clicked-row',] }],
    exportCellWhenClicked: [{ type: Input }],
    exportClickedCell: [{ type: Output, args: ['exportClickedCell',] }],
    data: [{ type: Input }],
    redrawRows: [{ type: Input }],
    tableDefinition: [{ type: Input }],
    height: [{ type: Input }],
    extraToolBtnUsed: [{ type: Output }],
    buttonOutput: [{ type: Output, args: ['row-button-click',] }],
    contextMenuOutput: [{ type: Output, args: ['context-menu-event',] }],
    exportToolClick: [{ type: Output, args: ['tool-click',] }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class FileTreeCellRendererComponent {
    constructor() {
    }
    /**
     * @param {?} params
     * @return {?}
     */
    agInit(params) {
        // Row data
        this.value = params.value;
        this.data = params.data;
        this.newCompClass = params.data['newCompany'];
        this.setIcon(params);
    }
    /**
     * @private
     * @param {?} file
     * @return {?}
     */
    setIcon(file) {
        if (file.data.commonName === 'SurveySet') {
            this.icon = 'texture';
        }
        else if (file.data.commonName === 'Trajectory') {
            this.icon = 'call_received';
        }
        else if (file.data.commonName === 'Field') {
            this.icon = 'map';
        }
        else if (file.data.commonName === 'Pad') {
            this.icon = 'location_on';
        }
        else if (file.data.commonName === 'Well') {
            this.icon = 'radio_button_unchecked';
        }
        else if (file.data.commonName === 'Wellbore') {
            this.icon = 'gps_fixed';
        }
        else {
            this.icon = 'business';
        }
    }
    /**
     * @return {?}
     */
    refresh() {
        return false;
    }
}
FileTreeCellRendererComponent.decorators = [
    { type: Component, args: [{
                selector: 'app-file-tree-cell-renderer',
                template: "<div class=\"{{newCompClass ? 'newComp admin-grid-folder-container' : 'admin-grid-folder-container'}}\">\n  <span class=\"material-icons folder-icon\">\n    <mat-icon>{{icon}}</mat-icon>\n  </span>\n  {{value}}\n</div>\n",
                styles: ["@media print{.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-appearance-legacy.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-1.28122em) scale(.75);transform:translateY(-1.28122em) scale(.75)}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-form-field-autofill-control:-webkit-autofill+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.28121em) scale(.75);transform:translateY(-1.28121em) scale(.75)}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.2812em) scale(.75);transform:translateY(-1.2812em) scale(.75)}}.mat-badge-content{font-weight:600;font-size:12px;font-family:NunitoSans,sans-serif}.mat-badge-small .mat-badge-content{font-size:9px}.mat-badge-large .mat-badge-content{font-size:24px}.mat-h1,.mat-headline,.mat-typography h1{font:400 24px/32px NunitoSans,sans-serif;margin:0 0 16px}.mat-h2,.mat-title,.mat-typography h2{font:500 20px/32px NunitoSans,sans-serif;margin:0 0 16px}.mat-h3,.mat-subheading-2,.mat-typography h3{font:400 16px/28px NunitoSans,sans-serif;margin:0 0 16px}.mat-h4,.mat-subheading-1,.mat-typography h4{font:400 15px/24px NunitoSans,sans-serif;margin:0 0 16px}.mat-h5,.mat-h6,.mat-typography h5,.mat-typography h6{margin:0 0 12px}.mat-body-2,.mat-body-strong{font:500 14px/24px NunitoSans,sans-serif}.mat-body,.mat-body-1,.mat-typography{font:400 14px/20px NunitoSans,sans-serif}.mat-body p,.mat-body-1 p,.mat-typography p{margin:0 0 12px}.mat-caption,.mat-small{font:400 12px/20px NunitoSans,sans-serif}.mat-display-4,.mat-typography .mat-display-4{font:300 112px/112px NunitoSans,sans-serif;letter-spacing:-.05em;margin:0 0 56px}.mat-display-3,.mat-typography .mat-display-3{font:400 56px/56px NunitoSans,sans-serif;letter-spacing:-.02em;margin:0 0 64px}.mat-display-2,.mat-typography .mat-display-2{font:400 45px/48px NunitoSans,sans-serif;letter-spacing:-.005em;margin:0 0 64px}.mat-display-1,.mat-typography .mat-display-1{font:400 34px/40px NunitoSans,sans-serif;margin:0 0 64px}.mat-bottom-sheet-container{font:400 14px/20px NunitoSans,sans-serif}.mat-button,.mat-fab,.mat-flat-button,.mat-icon-button,.mat-mini-fab,.mat-raised-button,.mat-stroked-button{font-family:NunitoSans,sans-serif;font-size:14px;font-weight:500}.mat-button-toggle,.mat-card{font-family:NunitoSans,sans-serif}.mat-card-title{font-size:24px;font-weight:500}.mat-card-header .mat-card-title{font-size:20px}.mat-card-content,.mat-card-subtitle{font-size:14px}.mat-checkbox{font-family:NunitoSans,sans-serif}.mat-checkbox-layout .mat-checkbox-label{line-height:24px}.mat-chip{font-size:14px;font-weight:500}.mat-chip .mat-chip-remove.mat-icon,.mat-chip .mat-chip-trailing-icon.mat-icon{font-size:18px}.mat-table{font-family:NunitoSans,sans-serif}.mat-header-cell{font-size:12px;font-weight:500}.mat-cell,.mat-footer-cell{font-size:14px}.mat-calendar{font-family:NunitoSans,sans-serif}.mat-calendar-body{font-size:13px}.mat-calendar-body-label,.mat-calendar-period-button{font-size:14px;font-weight:500}.mat-calendar-table-header th{font-size:11px;font-weight:400}.mat-dialog-title{font:500 20px/32px NunitoSans,sans-serif}.mat-expansion-panel-header{font-family:NunitoSans,sans-serif;font-size:15px;font-weight:400}.mat-expansion-panel-content{font:400 14px/20px NunitoSans,sans-serif}.mat-form-field{font-size:inherit;font-weight:400;line-height:1.125;font-family:NunitoSans,sans-serif}.mat-form-field-wrapper{padding-bottom:1.34375em}.mat-form-field-prefix .mat-icon,.mat-form-field-suffix .mat-icon{font-size:150%;line-height:1.125}.mat-form-field-prefix .mat-icon-button,.mat-form-field-suffix .mat-icon-button{height:1.5em;width:1.5em}.mat-form-field-prefix .mat-icon-button .mat-icon,.mat-form-field-suffix .mat-icon-button .mat-icon{height:1.125em;line-height:1.125}.mat-form-field-infix{padding:.5em 0;border-top:.84375em solid transparent}.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-1.34373em) scale(.75);transform:translateY(-1.34373em) scale(.75);width:133.3333533333%}.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.34372em) scale(.75);transform:translateY(-1.34372em) scale(.75);width:133.3333633333%}.mat-form-field-label-wrapper{top:-.84375em;padding-top:.84375em}.mat-form-field-label{top:1.34375em}.mat-form-field-underline{bottom:1.34375em}.mat-form-field-subscript-wrapper{font-size:75%;margin-top:.6666666667em;top:calc(100% - 1.7916666667em)}.mat-form-field-appearance-legacy .mat-form-field-wrapper{padding-bottom:1.25em}.mat-form-field-appearance-legacy .mat-form-field-infix{padding:.4375em 0}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-appearance-legacy.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00106px);transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00106px);-ms-transform:translateY(-1.28119em) scale(.75);width:133.3333933333%}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-form-field-autofill-control:-webkit-autofill+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00107px);transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00107px);-ms-transform:translateY(-1.28118em) scale(.75);width:133.3334033333%}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00108px);transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00108px);-ms-transform:translateY(-1.28117em) scale(.75);width:133.3334133333%}.mat-form-field-appearance-legacy .mat-form-field-label{top:1.28125em}.mat-form-field-appearance-legacy .mat-form-field-underline{bottom:1.25em}.mat-form-field-appearance-legacy .mat-form-field-subscript-wrapper{margin-top:.5416666667em;top:calc(100% - 1.6666666667em)}@media print{.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-appearance-legacy.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-1.28116em) scale(.75);transform:translateY(-1.28116em) scale(.75)}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-form-field-autofill-control:-webkit-autofill+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.28115em) scale(.75);transform:translateY(-1.28115em) scale(.75)}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.28114em) scale(.75);transform:translateY(-1.28114em) scale(.75)}}.mat-form-field-appearance-fill .mat-form-field-infix{padding:.25em 0 .75em}.mat-form-field-appearance-fill .mat-form-field-label{top:1.09375em;margin-top:-.5em}.mat-form-field-appearance-fill.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-appearance-fill.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-.59373em) scale(.75);transform:translateY(-.59373em) scale(.75);width:133.3333533333%}.mat-form-field-appearance-fill.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-.59372em) scale(.75);transform:translateY(-.59372em) scale(.75);width:133.3333633333%}.mat-form-field-appearance-outline .mat-form-field-infix{padding:1em 0}.mat-form-field-appearance-outline .mat-form-field-label{top:1.84375em;margin-top:-.25em}.mat-form-field-appearance-outline.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-appearance-outline.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-1.59373em) scale(.75);transform:translateY(-1.59373em) scale(.75);width:133.3333533333%}.mat-form-field-appearance-outline.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.59372em) scale(.75);transform:translateY(-1.59372em) scale(.75);width:133.3333633333%}.mat-grid-tile-footer,.mat-grid-tile-header{font-size:14px}.mat-grid-tile-footer .mat-line,.mat-grid-tile-header .mat-line{white-space:nowrap;overflow:hidden;text-overflow:ellipsis;display:block;box-sizing:border-box}.mat-grid-tile-footer .mat-line:nth-child(n+2),.mat-grid-tile-header .mat-line:nth-child(n+2){font-size:12px}input.mat-input-element{margin-top:-.0625em}.mat-menu-item{font-family:NunitoSans,sans-serif;font-size:14px;font-weight:400}.mat-paginator,.mat-paginator-page-size .mat-select-trigger{font-family:NunitoSans,sans-serif;font-size:12px}.mat-radio-button,.mat-select{font-family:NunitoSans,sans-serif}.mat-select-trigger{height:1.125em}.mat-slide-toggle-content{font-family:NunitoSans,sans-serif}.mat-slider-thumb-label-text{font-family:NunitoSans,sans-serif;font-size:12px;font-weight:500}.mat-stepper-horizontal,.mat-stepper-vertical{font-family:NunitoSans,sans-serif}.mat-step-label{font-size:14px;font-weight:400}.mat-step-sub-label-error{font-weight:400}.mat-step-label-error{font-size:14px}.mat-step-label-selected{font-size:14px;font-weight:500}.mat-tab-group{font-family:NunitoSans,sans-serif}.mat-tab-label,.mat-tab-link{font-family:NunitoSans,sans-serif;font-size:14px;font-weight:500}.mat-toolbar,.mat-toolbar h1,.mat-toolbar h2,.mat-toolbar h3,.mat-toolbar h4,.mat-toolbar h5,.mat-toolbar h6{font:500 20px/32px NunitoSans,sans-serif;margin:0}.mat-tooltip{font-family:NunitoSans,sans-serif;font-size:10px;padding-top:6px;padding-bottom:6px}.mat-tooltip-handset{font-size:14px;padding-top:8px;padding-bottom:8px}.mat-list-item,.mat-list-option{font-family:NunitoSans,sans-serif}.mat-list-base .mat-list-item{font-size:16px}.mat-list-base .mat-list-item .mat-line{white-space:nowrap;overflow:hidden;text-overflow:ellipsis;display:block;box-sizing:border-box}.mat-list-base .mat-list-item .mat-line:nth-child(n+2){font-size:14px}.mat-list-base .mat-list-option{font-size:16px}.mat-list-base .mat-list-option .mat-line{white-space:nowrap;overflow:hidden;text-overflow:ellipsis;display:block;box-sizing:border-box}.mat-list-base .mat-list-option .mat-line:nth-child(n+2){font-size:14px}.mat-list-base .mat-subheader{font-family:NunitoSans,sans-serif;font-size:14px;font-weight:500}.mat-list-base[dense] .mat-list-item{font-size:12px}.mat-list-base[dense] .mat-list-item .mat-line{white-space:nowrap;overflow:hidden;text-overflow:ellipsis;display:block;box-sizing:border-box}.mat-list-base[dense] .mat-list-item .mat-line:nth-child(n+2),.mat-list-base[dense] .mat-list-option{font-size:12px}.mat-list-base[dense] .mat-list-option .mat-line{white-space:nowrap;overflow:hidden;text-overflow:ellipsis;display:block;box-sizing:border-box}.mat-list-base[dense] .mat-list-option .mat-line:nth-child(n+2){font-size:12px}.mat-list-base[dense] .mat-subheader{font-family:NunitoSans,sans-serif;font-size:12px;font-weight:500}.mat-option{font-family:NunitoSans,sans-serif;font-size:16px}.mat-optgroup-label{font:500 14px/24px NunitoSans,sans-serif}.mat-simple-snackbar{font-family:NunitoSans,sans-serif;font-size:14px}.mat-simple-snackbar-action{line-height:1;font-family:inherit;font-size:inherit;font-weight:500}.mat-tree{font-family:NunitoSans,sans-serif}.mat-nested-tree-node,.mat-tree-node{font-weight:400;font-size:14px}.mat-ripple{overflow:hidden;position:relative}.mat-ripple.mat-ripple-unbounded{overflow:visible}.mat-ripple-element{position:absolute;border-radius:50%;pointer-events:none;-webkit-transition:opacity,-webkit-transform cubic-bezier(0,0,.2,1);transition:opacity,transform cubic-bezier(0,0,.2,1),-webkit-transform cubic-bezier(0,0,.2,1);-webkit-transform:scale(0);transform:scale(0)}@media (-ms-high-contrast:active){.mat-ripple-element{display:none}}.cdk-visually-hidden{border:0;clip:rect(0 0 0 0);height:1px;margin:-1px;overflow:hidden;padding:0;position:absolute;width:1px;outline:0;-webkit-appearance:none;-moz-appearance:none}.cdk-global-overlay-wrapper,.cdk-overlay-container{pointer-events:none;top:0;left:0;height:100%;width:100%}.cdk-overlay-container{position:fixed;z-index:1000}.cdk-overlay-container:empty{display:none}.cdk-global-overlay-wrapper{display:-webkit-box;display:flex;position:absolute;z-index:1000}.cdk-overlay-pane{position:absolute;pointer-events:auto;box-sizing:border-box;z-index:1000;display:-webkit-box;display:flex;max-width:100%;max-height:100%}.cdk-overlay-backdrop{position:absolute;top:0;bottom:0;left:0;right:0;z-index:1000;pointer-events:auto;-webkit-tap-highlight-color:transparent;-webkit-transition:opacity .4s cubic-bezier(.25,.8,.25,1);transition:opacity .4s cubic-bezier(.25,.8,.25,1);opacity:0}.cdk-overlay-backdrop.cdk-overlay-backdrop-showing{opacity:1}@media screen and (-ms-high-contrast:active){.cdk-overlay-backdrop.cdk-overlay-backdrop-showing{opacity:.6}}.cdk-overlay-dark-backdrop{background:rgba(0,0,0,.32)}.cdk-overlay-transparent-backdrop,.cdk-overlay-transparent-backdrop.cdk-overlay-backdrop-showing{opacity:0}.cdk-overlay-connected-position-bounding-box{position:absolute;z-index:1000;display:-webkit-box;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;flex-direction:column;min-width:1px;min-height:1px}.cdk-global-scrollblock{position:fixed;width:100%;overflow-y:scroll}@-webkit-keyframes cdk-text-field-autofill-start{/*!*/}@keyframes cdk-text-field-autofill-start{/*!*/}@-webkit-keyframes cdk-text-field-autofill-end{/*!*/}@keyframes cdk-text-field-autofill-end{/*!*/}.cdk-text-field-autofill-monitored:-webkit-autofill{-webkit-animation-name:cdk-text-field-autofill-start;animation-name:cdk-text-field-autofill-start}.cdk-text-field-autofill-monitored:not(:-webkit-autofill){-webkit-animation-name:cdk-text-field-autofill-end;animation-name:cdk-text-field-autofill-end}textarea.cdk-textarea-autosize{resize:none}textarea.cdk-textarea-autosize-measuring{height:auto!important;overflow:hidden!important;padding:2px 0!important;box-sizing:content-box!important}.admin-grid-folder-container{-webkit-box-align:center;align-items:center;display:-webkit-box;display:flex;padding-right:10px}.newComp{background-image:none!important;-webkit-animation:5s ease-in-out fadeIt;animation:5s ease-in-out fadeIt}.folder-icon{display:-webkit-box;display:flex;margin-right:10px;color:rgba(0,0,0,.54)}@-webkit-keyframes fadeIt{0%,100%{background-color:#fff}50%{background-color:#68a8f2}}@keyframes fadeIt{0%,100%{background-color:#fff}50%{background-color:#68a8f2}}"]
            }] }
];
/** @nocollapse */
FileTreeCellRendererComponent.ctorParameters = () => [];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class GridModule {
}
GridModule.decorators = [
    { type: NgModule, args: [{
                entryComponents: [TableComponent],
                imports: [
                    CommonModule,
                    AgGridModule.withComponents([
                        AutomationCellRendererComponent,
                        FileTreeCellRendererComponent,
                        NewFileCellRendererComponent,
                        RowButtonComponent,
                        CustomLoadingOverlayComponent,
                        ClickToolCellRendererComponent,
                        DatePickerCellEditRendererComponent,
                        DropdownCellRendererComponent
                    ]),
                    FormsModule,
                    MaterialModule,
                    PipesModule,
                ],
                exports: [
                    AgGridModule,
                    TableComponent,
                    DefinitiveTableErrorsComponent
                ],
                declarations: [
                    AutomationCellRendererComponent,
                    FileTreeCellRendererComponent,
                    NewFileCellRendererComponent,
                    RowButtonComponent,
                    TableComponent,
                    CustomLoadingOverlayComponent,
                    ClickToolCellRendererComponent,
                    DefinitiveTableErrorsComponent,
                    DatePickerCellEditRendererComponent,
                    DropdownCellRendererComponent
                ],
                providers: []
            },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class SaphiraSharedLibModule {
}
SaphiraSharedLibModule.decorators = [
    { type: NgModule, args: [{
                declarations: [
                    SplashComponent,
                    DateTimePickerComponent,
                    CompanyInfoComponent,
                    ConfirmationModalComponent,
                    ErrorModalComponent,
                    SuccessModalComponent
                ],
                imports: [
                    CommonModule,
                    FormsModule,
                    MaterialModule,
                    ReactiveFormsModule,
                    OwlDateTimeModule,
                    OwlNativeDateTimeModule,
                    GridModule
                ],
                exports: [
                    SplashComponent,
                    CompanyInfoComponent,
                    ConfirmationModalComponent,
                    ErrorModalComponent,
                    PipesModule,
                    SuccessModalComponent,
                    DateTimePickerComponent,
                    GridModule
                ],
                providers: [
                    AuthService,
                    ClientRequisitesService,
                    DataService,
                    PingService,
                    TitleFlasherService
                ],
                entryComponents: [
                    SplashComponent,
                    ConfirmationModalComponent,
                    ErrorModalComponent,
                    SuccessModalComponent,
                    DateTimePickerComponent
                ]
            },] }
];

export { AngleDecimalPipe, AngleUnitPipe, AuthService, AutomationCellRendererComponent, BooleanOnOffPipe, CapitalizePipe, ClickToolCellRendererComponent, ClientRequisitesService, CompanyInfoComponent, CompanyService, ConfirmationModalComponent, CustomLoadingOverlayComponent, DataService, DateTimePickerComponent, DefinitiveSurveysPipe, DefinitiveTableErrorsComponent, DistanceDecimalPipe, DistanceUnitPipe, DropdownCellRendererComponent, EllipsisizePipe, ErrorModalComponent, FileTreeCellRendererComponent, FormatObjectPipe, GravityDecimalPipe, GravityUnitPipe, GridModule, HeaderComponent, HeaderModule, InstantPipe, MagneticDecimalPipe, MagneticUnitPipe, MeasurePipe, NewFileCellRendererComponent, OrderByPipe, OrderSurveysByMdPipe, PingService, PipesModule, ResolvedStatePipe, RowButtonComponent, SaphiraSharedLibModule, SplashComponent, SuccessModalComponent, SurveySetStatePipe, SurveyTypePipe, TableComponent, TimeAgoPipe, TrajectoryTypePipe, WellboreStatusPipe, validateInput, validateInputWithEmptyOption, validateMagneticModelId, validateValue, EditCoordinationService as ɵa, TitleFlasherService as ɵb, BrowserTabService as ɵc, TimeService as ɵd, MaterialModule as ɵe, CellRendererFactory as ɵf, TableDefaultsService as ɵg, DatePickerCellEditRendererComponent as ɵh };
//# sourceMappingURL=hptech-saphira-shared-lib.js.map
