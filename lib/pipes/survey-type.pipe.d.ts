import { PipeTransform } from '@angular/core';
export declare class SurveyTypePipe implements PipeTransform {
    surveyTypes: {
        'STANDARD': {
            definitive: boolean;
            interpolated: boolean;
            acronym: string;
        };
        'POOR': {
            definitive: boolean;
            interpolated: boolean;
            acronym: string;
        };
        'DEFINITIVE_INTERPOLATED': {
            definitive: boolean;
            interpolated: boolean;
            acronym: string;
        };
        'BAD': {
            definitive: boolean;
            interpolated: boolean;
            acronym: string;
        };
        'ACC_CHECK': {
            definitive: boolean;
            interpolated: boolean;
            acronym: string;
        };
        'CHECKSHOT': {
            definitive: boolean;
            interpolated: boolean;
            acronym: string;
        };
        'INTERPOLATED': {
            definitive: boolean;
            interpolated: boolean;
            acronym: string;
        };
    };
    transform(value: string): any;
}
