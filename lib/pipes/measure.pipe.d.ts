import { PipeTransform } from '@angular/core';
export declare class MeasurePipe implements PipeTransform {
    transform(measure: any, displayUnit?: any): any;
}
