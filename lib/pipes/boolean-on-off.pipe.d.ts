import { PipeTransform } from '@angular/core';
export declare class BooleanOnOffPipe implements PipeTransform {
    transform(value: any): any;
}
