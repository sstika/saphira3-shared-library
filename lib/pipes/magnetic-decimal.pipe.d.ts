import { PipeTransform } from '@angular/core';
export declare class MagneticDecimalPipe implements PipeTransform {
    decimalMap: {
        NANOTESLA: number;
        MICROTESLA: number;
        MILLITESLA: number;
        TESLA: number;
        GAUSS: number;
        GAMMA: number;
        Nanotesla: number;
        nT: number;
        Microtesla: number;
        'µT': number;
        Millitesla: number;
        mT: number;
        Tesla: number;
        T: number;
        Gauss: number;
        Gs: number;
        Gamma: number;
        'γ': number;
    };
    transform(value: any, unit?: any): any;
    /**
     * isValidFloat:
     *
     * A stricter float filter than parseFloat().
     *
     * @param value Input value to be parsed as float
     * @returns float
     */
    isValidFloat(value: any): boolean;
    /**
      * formatNumber:
      *
      * Adds commas and removes the negative sign for zero values.
      *
      * @param number Input value to be formatted
      */
    formatNumber(number: any): any;
    /**
       * removeSignOnZero:
       *
       * If the value is zero, remove any negative signs. (converts -0.00 to 0.00)
       *
       * @param number Input value to be sanitized
       */
    removeSignOnZero(number: any): any;
    /**
       * addCommas:
       *
       * Helper function that adds in commas to a number for readability.
       *
       * @param number Input value to add commas
       * @returns string
       */
    addCommas(number: any): any;
}
