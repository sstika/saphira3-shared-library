import { PipeTransform } from '@angular/core';
export declare class SurveySetStatePipe implements PipeTransform {
    transform(value: string): any;
}
