import { PipeTransform } from '@angular/core';
export declare class DistanceDecimalPipe implements PipeTransform {
    decimalMap: {
        METER: number;
        KILOMETER: number;
        FOOT: number;
        FOOT_US: number;
        YARD: number;
        MILE: number;
        Meters: number;
        Kilometers: number;
        Feet: number;
        'ft US': number;
        'U.S. Survey Feet': number;
        Yards: number;
        Miles: number;
    };
    transform(value: any, unit?: any): any;
    /**
     * isValidFloat:
     *
     * A stricter float filter than parseFloat().
     *
     * @param value Input value to be parsed as float
     * @returns float
     */
    isValidFloat(value: any): boolean;
    /**
      * formatNumber:
      *
      * Adds commas and removes the negative sign for zero values.
      *
      * @param number Input value to be formatted
      */
    formatNumber(number: any): any;
    /**
       * removeSignOnZero:
       *
       * If the value is zero, remove any negative signs. (converts -0.00 to 0.00)
       *
       * @param number Input value to be sanitized
       */
    removeSignOnZero(number: any): any;
    /**
       * addCommas:
       *
       * Helper function that adds in commas to a number for readability.
       *
       * @param number Input value to add commas
       * @returns string
       */
    addCommas(number: any): any;
}
