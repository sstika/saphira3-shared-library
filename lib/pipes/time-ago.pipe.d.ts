import { PipeTransform } from '@angular/core';
export declare class TimeAgoPipe implements PipeTransform {
    transform(time: any): any;
}
