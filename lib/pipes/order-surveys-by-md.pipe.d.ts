import { PipeTransform } from '@angular/core';
export declare class OrderSurveysByMdPipe implements PipeTransform {
    filtered: any[];
    transform(items: any, reverse?: any): any;
}
