import { PipeTransform } from '@angular/core';
export declare class DistanceUnitPipe implements PipeTransform {
    transform(unit: any, full?: any): any;
}
