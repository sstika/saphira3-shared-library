import { PipeTransform } from '@angular/core';
export declare class TrajectoryTypePipe implements PipeTransform {
    transform(value: any, args?: any): any;
}
