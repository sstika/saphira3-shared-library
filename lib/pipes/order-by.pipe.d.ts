import { PipeTransform } from '@angular/core';
export declare class OrderByPipe implements PipeTransform {
    transform(array: any[], field: string): any[];
    transformSurveySet(array: any[], field: string, topElement: string): any[];
}
