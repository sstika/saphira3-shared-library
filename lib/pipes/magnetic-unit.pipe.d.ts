import { PipeTransform } from '@angular/core';
export declare class MagneticUnitPipe implements PipeTransform {
    transform(unit: any, full?: any): any;
}
