import { PipeTransform } from '@angular/core';
export declare class ResolvedStatePipe implements PipeTransform {
    transform(resolution: any, value?: any): any;
}
