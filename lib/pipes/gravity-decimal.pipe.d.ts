import { PipeTransform } from '@angular/core';
export declare class GravityDecimalPipe implements PipeTransform {
    decimalMap: {
        M_PER_SEC_SQ: number;
        FT_PER_SEC_SQ: number;
        G: number;
        MILLI_G: number;
        G_98: number;
        GAL: number;
        MILLI_GAL: number;
        'Meters per Second Sq.': number;
        'm/s²': number;
        'f/s²': number;
        mG: number;
        'G (9.8)': number;
        Gal: number;
        mGal: number;
    };
    transform(value: any, unit?: any): any;
    /**
     * isValidFloat:
     *
     * A stricter float filter than parseFloat().
     *
     * @param value Input value to be parsed as float
     * @returns float
     */
    isValidFloat(value: any): boolean;
    /**
      * formatNumber:
      *
      * Adds commas and removes the negative sign for zero values.
      *
      * @param number Input value to be formatted
      */
    formatNumber(number: any): any;
    /**
       * removeSignOnZero:
       *
       * If the value is zero, remove any negative signs. (converts -0.00 to 0.00)
       *
       * @param number Input value to be sanitized
       */
    removeSignOnZero(number: any): any;
    /**
       * addCommas:
       *
       * Helper function that adds in commas to a number for readability.
       *
       * @param number Input value to add commas
       * @returns string
       */
    addCommas(number: any): any;
}
