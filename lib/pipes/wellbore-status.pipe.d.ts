import { PipeTransform } from '@angular/core';
export declare class WellboreStatusPipe implements PipeTransform {
    transform(value: any, args?: any): any;
}
