import { PipeTransform } from '@angular/core';
import { DatePipe } from '@angular/common';
export declare class InstantPipe extends DatePipe implements PipeTransform {
    transform(dateObj: any): any;
}
