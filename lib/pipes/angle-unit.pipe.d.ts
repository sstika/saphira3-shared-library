import { PipeTransform } from '@angular/core';
export declare class AngleUnitPipe implements PipeTransform {
    transform(unit: any, full?: any): any;
}
