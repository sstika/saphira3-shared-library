import { PipeTransform } from '@angular/core';
export declare class FormatObjectPipe implements PipeTransform {
    transform(unit: any, full?: any): any;
}
