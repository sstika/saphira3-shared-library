import { PipeTransform } from '@angular/core';
export declare class EllipsisizePipe implements PipeTransform {
    transform(value: any, desiredLengthIncludingElip: number): any;
}
