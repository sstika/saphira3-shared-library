import { PipeTransform } from '@angular/core';
export declare class GravityUnitPipe implements PipeTransform {
    transform(unit: string, full?: any): string;
}
