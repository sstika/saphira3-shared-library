import { ValidatorFn } from '@angular/forms';
export declare function validateInput(unit: any, type: any): ValidatorFn;
export declare function validateInputWithEmptyOption(unit: any, type: any): ValidatorFn;
export declare function validateMagneticModelId(ids: string[]): ValidatorFn;
export declare function validateValue(inputValue: string, unit: string, type: string): any;
