import { EventEmitter, OnInit } from '@angular/core';
import { TimeService } from '../../services/time.service';
import { MatDialogRef } from '@angular/material/dialog';
export declare class DateTimePickerComponent implements OnInit {
    private timeService;
    dialogRef: MatDialogRef<DateTimePickerComponent>;
    data: any;
    dateEmitter: EventEmitter<{}>;
    value: any;
    constructor(timeService: TimeService, dialogRef: MatDialogRef<DateTimePickerComponent>, data: any);
    ngOnInit(): void;
    clearField(): void;
    setField(): void;
    closeField(): void;
}
