import { OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
export declare class ErrorModalComponent implements OnInit {
    dialogRef: MatDialogRef<ErrorModalComponent>;
    data: any;
    constructor(dialogRef: MatDialogRef<ErrorModalComponent>, data: any);
    ngOnInit(): void;
}
