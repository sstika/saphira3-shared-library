import { EventEmitter } from '@angular/core';
export declare class DefinitiveTableErrorsComponent {
    showTrackingIds: boolean;
    definitiveSurveys: any[];
    conflictLines: EventEmitter<number>;
    private _surveys;
    errors: any[];
    mdUnits: string;
    trackingIdErrors: {};
    trackingIdKeys: any[];
    constructor();
    private evaluateErrors;
}
