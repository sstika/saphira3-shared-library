import { OnInit, OnChanges } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { CompanyService } from '../../services/admin-services/company.service';
import { EditCoordinationService } from '../../services/edit-coordination.service';
export declare class CompanyInfoComponent implements OnInit, OnChanges {
    private companyService;
    private router;
    private _fb;
    private dialog;
    editCoordinationService: EditCoordinationService;
    company: {};
    parentApp: string;
    loading: boolean;
    distanceTypes: any[];
    distanceObject: {};
    formValidatorFloat: string;
    formValidatorInteger: string;
    companyForm: FormGroup;
    companyIsEditable: boolean;
    constructor(companyService: CompanyService, router: Router, _fb: FormBuilder, dialog: MatDialog, editCoordinationService: EditCoordinationService);
    ngOnInit(): void;
    ngOnChanges(): void;
    translateDistance(unit: any): string;
    toggleEdit(sectionName: any): void;
    cancelEdit(sectionName: any): void;
    checkTooltip(formStatus: any): "Save" | "Cannot save - certain fields are invalid.";
    saveEdit(sectionName: any): void;
    makeCompanyForm(): void;
    enableCompanyForm(): void;
    onSubmitCompany(): void;
    sendInformation(): void;
    checkEditButtonRenderStatus(): boolean;
    sendEditStatus(field: string, status: boolean): void;
}
