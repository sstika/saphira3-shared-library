import { OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
export declare class SuccessModalComponent implements OnInit {
    dialogRef: MatDialogRef<SuccessModalComponent>;
    data: any;
    constructor(dialogRef: MatDialogRef<SuccessModalComponent>, data: any);
    ngOnInit(): void;
}
