import { OnDestroy, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { PingService } from '../../services/ping.service';
import { BrowserTabService } from '../../services/browser-tab.service';
export declare class HeaderComponent implements OnInit, OnDestroy {
    private auth;
    ping: PingService;
    private router;
    private browserTabService;
    version: any;
    isAdmin: boolean;
    username: string;
    isConnected: string;
    connectedStatus: string;
    shouldPing: boolean;
    oldSaphiraAdmin: {
        dev: string;
        prod: string;
    };
    constructor(auth: AuthService, ping: PingService, router: Router, browserTabService: BrowserTabService);
    ngOnInit(): void;
    updateUsername(): void;
    sendEmail(): void;
    logout(): void;
    goToOldSaphiraAdmin(): void;
    ngOnDestroy(): void;
}
