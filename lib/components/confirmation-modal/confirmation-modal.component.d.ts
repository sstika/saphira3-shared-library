import { EventEmitter, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
export declare class ConfirmationModalComponent implements OnInit {
    dialogRef: MatDialogRef<ConfirmationModalComponent>;
    data: any;
    submitEmitter: EventEmitter<{}>;
    response: {};
    constructor(dialogRef: MatDialogRef<ConfirmationModalComponent>, data: any);
    ngOnInit(): void;
    onSubmit(): void;
    onCancel(): void;
}
