import { AgRendererComponent } from 'ag-grid-angular/main';
export declare class FileTreeCellRendererComponent implements AgRendererComponent {
    value: string;
    data: any;
    icon: string;
    newCompClass: string;
    constructor();
    agInit(params: any): void;
    private setIcon;
    refresh(): boolean;
}
