import { AgRendererComponent } from 'ag-grid-angular/main';
export declare class DropdownCellRendererComponent implements AgRendererComponent {
    value: string;
    icon: string;
    constructor();
    agInit(params: any): void;
    refresh(): boolean;
}
