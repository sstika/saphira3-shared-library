import { AgRendererComponent } from 'ag-grid-angular/main';
export declare class AutomationCellRendererComponent implements AgRendererComponent {
    value: string;
    data: any;
    constructor();
    agInit(params: any): void;
    refresh(): boolean;
}
