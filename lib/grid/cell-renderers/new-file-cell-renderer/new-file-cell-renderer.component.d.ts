import { AgRendererComponent } from 'ag-grid-angular/main';
export declare class NewFileCellRendererComponent implements AgRendererComponent {
    value: string;
    icon: string;
    constructor();
    agInit(params: any): void;
    refresh(): boolean;
}
