import { AfterViewInit } from '@angular/core';
import { ICellEditorParams } from 'ag-grid-community';
import { AgEditorComponent } from 'ag-grid-angular';
import { MatDatepicker } from '@angular/material';
import { TimeService } from '../../../services/time.service';
export declare class DatePickerCellEditRendererComponent implements AgEditorComponent, AfterViewInit {
    private timeService;
    params: ICellEditorParams;
    value: string;
    picker: MatDatepicker<Date>;
    constructor(timeService: TimeService);
    ngAfterViewInit(): void;
    isPopup(): boolean;
    isCancelBeforeStart(): boolean;
    isCancelAfterEnd(): boolean;
    agInit(params: any): void;
    getValue(): string;
    onSelectChange(e: any): void;
}
