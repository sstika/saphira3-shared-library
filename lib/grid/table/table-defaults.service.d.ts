export declare class TableDefaultsService {
    private tableFilterIcon;
    private tableDefDefaults;
    private colDefDefaults;
    private actionDefaults;
    getDefaults(defaultType: string, contextMenuObj?: any): any;
    dateComparator(filterDate: Date, cellValue: Date): number;
    repositionFilterMenu(params: any): void;
    updateNullValues(params: any): void;
    getContextMenuItems(params: any): (string | {
        name: string;
        action: () => void;
        icon: string;
        subMenu?: undefined;
    } | {
        name: string;
        icon: string;
        subMenu: {
            name: string;
            action: () => void;
            icon: string;
        }[];
        action?: undefined;
    })[];
}
