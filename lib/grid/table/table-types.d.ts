export interface CustomGridOptions {
    exportPrefix?: string;
}
export declare class FilterableColumn {
    name: string;
    field: string;
    includeInSearch: boolean;
}
