import { AgRendererComponent } from 'ag-grid-angular/main';
import { ActivatedRoute, Router } from '@angular/router';
export interface Button {
    type: string;
    icon: string;
    color: string;
    navigateTo?: string | string[];
    queryParams?: {
        [key: string]: string;
    };
    newWindow?: boolean;
}
export interface RowButtonEvent {
    type: string;
    rowData: any;
}
export declare class RowButtonComponent implements AgRendererComponent {
    activatedRoute: ActivatedRoute;
    private router;
    private data;
    private tableData;
    buttons: Button[];
    private idOfMostRecentCommit;
    private outputEmitter;
    constructor(activatedRoute: ActivatedRoute, router: Router);
    agInit(params: any): void;
    private setIcons;
    clicked(button: Button): void;
    private interpolate;
    refresh(): boolean;
}
