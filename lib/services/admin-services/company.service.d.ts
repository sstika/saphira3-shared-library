import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { DataService } from '../data.service';
export declare class CompanyService extends DataService {
    protected allCompanies: string;
    protected getAllUnderCompany: string;
    protected addComp: string;
    protected getOneComp: string;
    protected deleteComp: string;
    protected companySnapshot: string;
    protected restoreFromCompanySnapshot: string;
    protected downloadAtt: string;
    protected getOneMeta: string;
    protected update: string;
    protected lastModifiedDate: string;
    private currentCompany;
    companiesFiltered: any;
    constructor(http: HttpClient);
    getAllCompanies(): Observable<any>;
    getAllForCompany(compId: any): Observable<any>;
    getOne(compId: any): Observable<any>;
    getCurrent(): Observable<any>;
    delete(compId: any): Observable<any>;
    add(newCompObj: any, headerObj?: {}): Observable<string>;
    getCompanySnapshot(compId: any): Observable<any>;
    postCompanyUpdate(bodyObj: any, headerObj?: {}): Observable<any>;
    returnFileTreeCompanies(): Observable<any>;
    cleanCompanies(companies: any): any;
    returnFilteredComps(): any;
}
