import { Title } from '@angular/platform-browser';
export declare class TitleFlasherService {
    private titleService;
    private documentTitleFlash;
    private documentTitleFlashSubscription;
    private prevTitle;
    isAlreadyFlashingTitle: boolean;
    constructor(titleService: Title);
    startFlashing(interval: number, titleToFlash: string): void;
    stopFlashing(): void;
    setTitle(title: string): void;
    getTitle(): string;
}
