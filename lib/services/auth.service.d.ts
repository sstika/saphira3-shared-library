import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { DataService } from './data.service';
export declare class AuthService extends DataService {
    private router;
    private loginPath;
    private logoutPath;
    private userInfo;
    constructor(http: HttpClient, router: Router);
    login(username: string, password: string): Observable<any>;
    logout(): Observable<any>;
    /**
     * Called when a 401 status is encountered which means user is logged out
     */
    on401Error(): void;
    getUser(): Observable<any>;
    userhasPermission(perm2Match: string): boolean;
    userIsCustomer(): boolean;
}
