import * as moment_ from 'moment';
import 'moment-timezone';
export declare class TimeService {
    constructor();
    getMomentFromInstant: (instant: any) => moment_.Moment;
    getTwelveHourMomentFromInstant: (instant: any) => string;
    momentFromNow(timestamp: any): string;
    momentFromNowFormatter(timestamp: any): any;
    standardDateFormatter(timestamp: any, format: any): string;
    convertDatetoMoment(timestamp: any, format: any): moment_.Moment;
    dateComparator(momentA: moment_.Moment, momentB: moment_.Moment): number;
    getUTCTimestampFromInstant(instant: any): any;
    getTimeZoneAbbr(): {};
    convertToUtc(timestamp: any): moment_.Moment;
    getCurrentDate(format: any): string;
}
