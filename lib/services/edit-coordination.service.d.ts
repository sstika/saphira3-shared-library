export declare class EditCoordinationService {
    editStatus: {
        infoCompany: boolean;
        infoField: boolean;
        infoPad: boolean;
        infoWell: boolean;
        wellboreInfo: boolean;
        wellboreMagnetic: boolean;
        trajectoryInfo: boolean;
        trajectoryTieIn: boolean;
        trajectorySurveys: boolean;
        surveySetInfo: boolean;
        surveySetRigContact: boolean;
        surveySetGrid: boolean;
    };
    private editFieldStatus;
    getEditFieldStatus: import("rxjs").Observable<boolean>;
    constructor();
    toggleField(field: string, status: boolean): void;
    areEditFieldsClosed(): boolean;
    private setEditFieldStatus;
}
