/**
 * Opens new browser tabs.
 * Workaround for Chrome.
 * @see https://blog.chromium.org/2009/12/links-that-open-in-new-processes.html
 * @see https://stackoverflow.com/a/40909198
 */
export declare class BrowserTabService {
    private tag;
    anchorTag: {
        href: string;
        click: () => void;
    };
    constructor();
    /**
     * Opens a new browser tab
     */
    open(url: string): void;
    /**
     * Opens Wellbore Detail in a new browser tab
     */
    openWellboreDetail(wellboreId: string): void;
}
