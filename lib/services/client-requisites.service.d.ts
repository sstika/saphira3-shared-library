import { Observable, BehaviorSubject } from 'rxjs';
import { DataService } from './data.service';
import { HttpClient } from '@angular/common/http';
export declare class ClientRequisitesService extends DataService {
    protected clientRequisitesPath: string;
    cachedData: any;
    clientRequisites: BehaviorSubject<{}>;
    getClientReqs: Observable<{}>;
    constructor(http: HttpClient);
    getClientRequisites(reload?: boolean): Observable<any>;
    setClientRequisitesService(data: object): void;
    returnCachedField(requestedField: string): object;
    returnAllCached(): Observable<any>;
}
