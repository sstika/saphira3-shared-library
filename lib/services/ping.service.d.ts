import { DataService } from './data.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
export declare class PingService extends DataService {
    private ping;
    private pingSuffix;
    private pingStatus;
    /** Round trip time in ms */
    tripTime: number;
    /** Backend API version */
    apiVersion: string;
    constructor(http: HttpClient);
    start(): void;
    status(): Observable<boolean>;
}
