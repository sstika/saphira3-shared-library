import { Observable } from 'rxjs';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
export declare class DataService {
    protected http: HttpClient;
    protected urlRoot: string;
    constructor(http: HttpClient);
    /**
     * This function is for extracting the usable value from GET,POST,PUT,DELETE Response.
     * @param  res - {any} the response object (possibly) from a call;
     * @returns Observable<any> -  Returns an Observable<any> .
     */
    protected extractData(res: any): any;
    /**
     * Display a notification that informs the user that an HTTP error occurred.
     * Must be consumed by the child service, does NOT get called by default.
     * @param msg User friendly message to display to the user
     */
    protected displayErrorMsg(msg: string): void;
    /**
     * Sends a verbose error message to the console for further debugging
     * @param  error - HttpErrorResponse error object that is supplied via HttpClient
     * @param method - the HTTP method used
     * @returns `HttpError` that was passed in or a placeholder if none was provided
     */
    protected logError(error: HttpErrorResponse, method: 'GET' | 'POST' | 'PUT' | 'DELETE'): Observable<HttpErrorResponse>;
    /**
     * Maps `HttpHeaders` to simple JSON string for debugging
     */
    private headers2str;
    /**
     * Master GET function.
     * @param  path - string path of the endpoint;
     * @param  headers - custom headers supplied to the call;
     * @returns Observable<any> -  Returns an Observable<any> .
     */
    protected get(path: string, headers?: {
        [key: string]: string;
    }): Observable<any>;
    /**
     * Master PUT function.
     * @param  path - string path of the endpoint;
     * @param  obj - object for which you want to put;
     * @param  headers - custom headers supplied to the call;
     * @returns Observable<any> -  Returns an Observable<any> .
     */
    protected put(path: string, obj: any, headers?: {
        [key: string]: string;
    }): Observable<any>;
    /**
     * Master POST function.
     * @param  path - string path of the endpoint;
     * @param  obj - object for which you want to put;
     * @param  headers - custom headers supplied to the call;
     * @returns Observable<any> -  Returns an Observable<any> .
     */
    protected post(path: string, obj: any, headers?: {
        [key: string]: string;
    }): Observable<any>;
    /**
     * Master DELETE function.
     * @param  path - string path of the endpoint;
     * @param  headers - custom headers supplied to the call;
     * @returns Observable<any> -  Returns an Observable<any> .
     */
    protected delete(path: string, headers?: {
        [key: string]: string;
    }): Observable<any>;
}
