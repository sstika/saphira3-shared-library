/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { SplashComponent } from './components/splash/splash.component';
import { DateTimePickerComponent } from './components/date-time-picker/date-time-picker.component';
import { AuthService } from './services/auth.service';
import { ClientRequisitesService } from './services/client-requisites.service';
import { DataService } from './services/data.service';
import { PingService } from './services/ping.service';
import { TitleFlasherService } from './services/title-flasher-service';
import { PipesModule } from './pipes/pipes.module';
import { MaterialModule } from './material.module';
import { GridModule } from './grid/grid.module';
import { CompanyInfoComponent } from './components/company-info/company-info.component';
import { ErrorModalComponent } from './components/error-modal/error-modal.component';
import { ConfirmationModalComponent } from './components/confirmation-modal/confirmation-modal.component';
import { SuccessModalComponent } from './components/success-modal/success-modal.component';
export class SaphiraSharedLibModule {
}
SaphiraSharedLibModule.decorators = [
    { type: NgModule, args: [{
                declarations: [
                    SplashComponent,
                    DateTimePickerComponent,
                    CompanyInfoComponent,
                    ConfirmationModalComponent,
                    ErrorModalComponent,
                    SuccessModalComponent
                ],
                imports: [
                    CommonModule,
                    FormsModule,
                    MaterialModule,
                    ReactiveFormsModule,
                    OwlDateTimeModule,
                    OwlNativeDateTimeModule,
                    GridModule
                ],
                exports: [
                    SplashComponent,
                    CompanyInfoComponent,
                    ConfirmationModalComponent,
                    ErrorModalComponent,
                    PipesModule,
                    SuccessModalComponent,
                    DateTimePickerComponent,
                    GridModule
                ],
                providers: [
                    AuthService,
                    ClientRequisitesService,
                    DataService,
                    PingService,
                    TitleFlasherService
                ],
                entryComponents: [
                    SplashComponent,
                    ConfirmationModalComponent,
                    ErrorModalComponent,
                    SuccessModalComponent,
                    DateTimePickerComponent
                ]
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2FwaGlyYS1zaGFyZWQtbGliLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BocHRlY2gvc2FwaGlyYS1zaGFyZWQtbGliLyIsInNvdXJjZXMiOlsibGliL3NhcGhpcmEtc2hhcmVkLWxpYi5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQy9DLE9BQU8sRUFBRSxXQUFXLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUVsRSxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsdUJBQXVCLEVBQUUsTUFBTSxrQkFBa0IsQ0FBQztBQUU5RSxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sc0NBQXNDLENBQUM7QUFFdkUsT0FBTyxFQUFFLHVCQUF1QixFQUFFLE1BQU0sMERBQTBELENBQUM7QUFDbkcsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLHlCQUF5QixDQUFDO0FBQ3RELE9BQU8sRUFBRSx1QkFBdUIsRUFBRSxNQUFNLHNDQUFzQyxDQUFDO0FBQy9FLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSx5QkFBeUIsQ0FBQztBQUN0RCxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0seUJBQXlCLENBQUM7QUFDdEQsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sa0NBQWtDLENBQUM7QUFDdkUsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBRW5ELE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQztBQUNuRCxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sb0JBQW9CLENBQUM7QUFFaEQsT0FBTyxFQUFFLG9CQUFvQixFQUFFLE1BQU0sa0RBQWtELENBQUM7QUFDeEYsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sZ0RBQWdELENBQUM7QUFDckYsT0FBTyxFQUFFLDBCQUEwQixFQUFFLE1BQU0sOERBQThELENBQUM7QUFDMUcsT0FBTyxFQUFFLHFCQUFxQixFQUFFLE1BQU0sb0RBQW9ELENBQUM7QUE4QzNGLE1BQU0sT0FBTyxzQkFBc0I7OztZQTVDbEMsUUFBUSxTQUFDO2dCQUNSLFlBQVksRUFBRTtvQkFDWixlQUFlO29CQUNmLHVCQUF1QjtvQkFDdkIsb0JBQW9CO29CQUNwQiwwQkFBMEI7b0JBQzFCLG1CQUFtQjtvQkFDbkIscUJBQXFCO2lCQUN0QjtnQkFDRCxPQUFPLEVBQUU7b0JBQ1AsWUFBWTtvQkFDWixXQUFXO29CQUNYLGNBQWM7b0JBQ2QsbUJBQW1CO29CQUNuQixpQkFBaUI7b0JBQ2pCLHVCQUF1QjtvQkFDdkIsVUFBVTtpQkFDWDtnQkFDRCxPQUFPLEVBQUU7b0JBQ1AsZUFBZTtvQkFDZixvQkFBb0I7b0JBQ3BCLDBCQUEwQjtvQkFDMUIsbUJBQW1CO29CQUNuQixXQUFXO29CQUNYLHFCQUFxQjtvQkFDckIsdUJBQXVCO29CQUN2QixVQUFVO2lCQUNYO2dCQUVELFNBQVMsRUFBRTtvQkFDVCxXQUFXO29CQUNYLHVCQUF1QjtvQkFDdkIsV0FBVztvQkFDWCxXQUFXO29CQUNYLG1CQUFtQjtpQkFDcEI7Z0JBQ0QsZUFBZSxFQUFFO29CQUNmLGVBQWU7b0JBQ2YsMEJBQTBCO29CQUMxQixtQkFBbUI7b0JBQ25CLHFCQUFxQjtvQkFDckIsdUJBQXVCO2lCQUN4QjthQUNGIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IENvbW1vbk1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XG5pbXBvcnQgeyBGb3Jtc01vZHVsZSwgUmVhY3RpdmVGb3Jtc01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcblxuaW1wb3J0IHsgT3dsRGF0ZVRpbWVNb2R1bGUsIE93bE5hdGl2ZURhdGVUaW1lTW9kdWxlIH0gZnJvbSAnbmctcGljay1kYXRldGltZSc7XG5cbmltcG9ydCB7IFNwbGFzaENvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9zcGxhc2gvc3BsYXNoLmNvbXBvbmVudCc7XG5cbmltcG9ydCB7IERhdGVUaW1lUGlja2VyQ29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL2RhdGUtdGltZS1waWNrZXIvZGF0ZS10aW1lLXBpY2tlci5jb21wb25lbnQnO1xuaW1wb3J0IHsgQXV0aFNlcnZpY2UgfSBmcm9tICcuL3NlcnZpY2VzL2F1dGguc2VydmljZSc7XG5pbXBvcnQgeyBDbGllbnRSZXF1aXNpdGVzU2VydmljZSB9IGZyb20gJy4vc2VydmljZXMvY2xpZW50LXJlcXVpc2l0ZXMuc2VydmljZSc7XG5pbXBvcnQgeyBEYXRhU2VydmljZSB9IGZyb20gJy4vc2VydmljZXMvZGF0YS5zZXJ2aWNlJztcbmltcG9ydCB7IFBpbmdTZXJ2aWNlIH0gZnJvbSAnLi9zZXJ2aWNlcy9waW5nLnNlcnZpY2UnO1xuaW1wb3J0IHsgVGl0bGVGbGFzaGVyU2VydmljZSB9IGZyb20gJy4vc2VydmljZXMvdGl0bGUtZmxhc2hlci1zZXJ2aWNlJztcbmltcG9ydCB7IFBpcGVzTW9kdWxlIH0gZnJvbSAnLi9waXBlcy9waXBlcy5tb2R1bGUnO1xuXG5pbXBvcnQgeyBNYXRlcmlhbE1vZHVsZSB9IGZyb20gJy4vbWF0ZXJpYWwubW9kdWxlJztcbmltcG9ydCB7IEdyaWRNb2R1bGUgfSBmcm9tICcuL2dyaWQvZ3JpZC5tb2R1bGUnO1xuXG5pbXBvcnQgeyBDb21wYW55SW5mb0NvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9jb21wYW55LWluZm8vY29tcGFueS1pbmZvLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBFcnJvck1vZGFsQ29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL2Vycm9yLW1vZGFsL2Vycm9yLW1vZGFsLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBDb25maXJtYXRpb25Nb2RhbENvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9jb25maXJtYXRpb24tbW9kYWwvY29uZmlybWF0aW9uLW1vZGFsLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBTdWNjZXNzTW9kYWxDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvc3VjY2Vzcy1tb2RhbC9zdWNjZXNzLW1vZGFsLmNvbXBvbmVudCc7XG5cbkBOZ01vZHVsZSh7XG4gIGRlY2xhcmF0aW9uczogW1xuICAgIFNwbGFzaENvbXBvbmVudCxcbiAgICBEYXRlVGltZVBpY2tlckNvbXBvbmVudCxcbiAgICBDb21wYW55SW5mb0NvbXBvbmVudCxcbiAgICBDb25maXJtYXRpb25Nb2RhbENvbXBvbmVudCxcbiAgICBFcnJvck1vZGFsQ29tcG9uZW50LFxuICAgIFN1Y2Nlc3NNb2RhbENvbXBvbmVudFxuICBdLFxuICBpbXBvcnRzOiBbXG4gICAgQ29tbW9uTW9kdWxlLFxuICAgIEZvcm1zTW9kdWxlLFxuICAgIE1hdGVyaWFsTW9kdWxlLFxuICAgIFJlYWN0aXZlRm9ybXNNb2R1bGUsXG4gICAgT3dsRGF0ZVRpbWVNb2R1bGUsXG4gICAgT3dsTmF0aXZlRGF0ZVRpbWVNb2R1bGUsXG4gICAgR3JpZE1vZHVsZVxuICBdLFxuICBleHBvcnRzOiBbXG4gICAgU3BsYXNoQ29tcG9uZW50LFxuICAgIENvbXBhbnlJbmZvQ29tcG9uZW50LFxuICAgIENvbmZpcm1hdGlvbk1vZGFsQ29tcG9uZW50LFxuICAgIEVycm9yTW9kYWxDb21wb25lbnQsXG4gICAgUGlwZXNNb2R1bGUsXG4gICAgU3VjY2Vzc01vZGFsQ29tcG9uZW50LFxuICAgIERhdGVUaW1lUGlja2VyQ29tcG9uZW50LFxuICAgIEdyaWRNb2R1bGVcbiAgXSxcblxuICBwcm92aWRlcnM6IFtcbiAgICBBdXRoU2VydmljZSxcbiAgICBDbGllbnRSZXF1aXNpdGVzU2VydmljZSxcbiAgICBEYXRhU2VydmljZSxcbiAgICBQaW5nU2VydmljZSxcbiAgICBUaXRsZUZsYXNoZXJTZXJ2aWNlXG4gIF0sXG4gIGVudHJ5Q29tcG9uZW50czogW1xuICAgIFNwbGFzaENvbXBvbmVudCxcbiAgICBDb25maXJtYXRpb25Nb2RhbENvbXBvbmVudCxcbiAgICBFcnJvck1vZGFsQ29tcG9uZW50LFxuICAgIFN1Y2Nlc3NNb2RhbENvbXBvbmVudCxcbiAgICBEYXRlVGltZVBpY2tlckNvbXBvbmVudFxuICBdXG59KVxuZXhwb3J0IGNsYXNzIFNhcGhpcmFTaGFyZWRMaWJNb2R1bGUgeyB9XG4iXX0=