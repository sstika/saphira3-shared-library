/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Pipe } from '@angular/core';
// Returns the angle as a string with an amount of decimal places based on its unit.
// Also formats the string with commas for readability.
export class MagneticDecimalPipe {
    constructor() {
        // number of decimal places to display for various angle units
        this.decimalMap = {
            // Old Format
            NANOTESLA: 0,
            MICROTESLA: 4,
            MILLITESLA: 0,
            TESLA: 0,
            GAUSS: 5,
            GAMMA: 0,
            // New Format
            Nanotesla: 0,
            nT: 0,
            Microtesla: 4,
            'µT': 4,
            Millitesla: 0,
            mT: 0,
            Tesla: 0,
            T: 0,
            Gauss: 5,
            Gs: 5,
            Gamma: 0,
            'γ': 0
        };
    }
    /**
     * @param {?} value
     * @param {?=} unit
     * @return {?}
     */
    transform(value, unit) {
        if (!this.isValidFloat(value)) {
            return 'N/A';
        }
        else {
            value = parseFloat(value);
            if (typeof (this.decimalMap[unit]) !== 'undefined') {
                return this.formatNumber(value.toFixed(this.decimalMap[unit]));
            }
            else {
                return 'Unsupported Unit: ' + unit;
            }
        }
    }
    /**
     * isValidFloat:
     *
     * A stricter float filter than parseFloat().
     *
     * @param {?} value Input value to be parsed as float
     * @return {?} float
     */
    isValidFloat(value) {
        return !isNaN(parseFloat(value));
    }
    /**
     * formatNumber:
     *
     * Adds commas and removes the negative sign for zero values.
     *
     * @param {?} number Input value to be formatted
     * @return {?}
     */
    formatNumber(number) {
        return this.removeSignOnZero(this.addCommas(number));
    }
    /**
     * removeSignOnZero:
     *
     * If the value is zero, remove any negative signs. (converts -0.00 to 0.00)
     *
     * @param {?} number Input value to be sanitized
     * @return {?}
     */
    removeSignOnZero(number) {
        /** @type {?} */
        const split = number.toString().split('-');
        if (split.length === 2) {
            if (split[1] === 0) {
                return split[1];
            }
        }
        return number;
    }
    /**
     * addCommas:
     *
     * Helper function that adds in commas to a number for readability.
     *
     * @param {?} number Input value to add commas
     * @return {?} string
     */
    addCommas(number) {
        // check if we have a decimal
        /** @type {?} */
        const split = number.toString().split('.');
        // if we have a decimal
        if (split.length === 2) {
            // only add commas to the first half and return the second half
            return split[0].replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,') + '.' + split[1];
        }
        else {
            // add commas throughout
            return split[0].replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
        }
    }
}
MagneticDecimalPipe.decorators = [
    { type: Pipe, args: [{
                name: 'magneticDecimal'
            },] }
];
if (false) {
    /** @type {?} */
    MagneticDecimalPipe.prototype.decimalMap;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFnbmV0aWMtZGVjaW1hbC5waXBlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGhwdGVjaC9zYXBoaXJhLXNoYXJlZC1saWIvIiwic291cmNlcyI6WyJsaWIvcGlwZXMvbWFnbmV0aWMtZGVjaW1hbC5waXBlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsSUFBSSxFQUFpQixNQUFNLGVBQWUsQ0FBQztBQUtwRCxvRkFBb0Y7QUFDcEYsdURBQXVEO0FBQ3ZELE1BQU0sT0FBTyxtQkFBbUI7SUFMaEM7O1FBT0UsZUFBVSxHQUFHOztZQUVYLFNBQVMsRUFBRSxDQUFDO1lBQ1osVUFBVSxFQUFFLENBQUM7WUFDYixVQUFVLEVBQUUsQ0FBQztZQUNiLEtBQUssRUFBRSxDQUFDO1lBQ1IsS0FBSyxFQUFFLENBQUM7WUFDUixLQUFLLEVBQUUsQ0FBQzs7WUFFUixTQUFTLEVBQUUsQ0FBQztZQUNaLEVBQUUsRUFBRSxDQUFDO1lBQ0wsVUFBVSxFQUFFLENBQUM7WUFDYixJQUFJLEVBQUUsQ0FBQztZQUNQLFVBQVUsRUFBRSxDQUFDO1lBQ2IsRUFBRSxFQUFFLENBQUM7WUFDTCxLQUFLLEVBQUUsQ0FBQztZQUNSLENBQUMsRUFBRSxDQUFDO1lBQ0osS0FBSyxFQUFFLENBQUM7WUFDUixFQUFFLEVBQUUsQ0FBQztZQUNMLEtBQUssRUFBRSxDQUFDO1lBQ1IsR0FBRyxFQUFFLENBQUM7U0FDUCxDQUFDO0lBMkVKLENBQUM7Ozs7OztJQXpFQyxTQUFTLENBQUMsS0FBVSxFQUFFLElBQVU7UUFDOUIsSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLEVBQUU7WUFDN0IsT0FBTyxLQUFLLENBQUM7U0FDZDthQUFNO1lBQ0wsS0FBSyxHQUFHLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUMxQixJQUFJLE9BQU8sQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDLEtBQUssV0FBVyxFQUFFO2dCQUNsRCxPQUFPLElBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQzthQUNoRTtpQkFBTTtnQkFDTCxPQUFPLG9CQUFvQixHQUFHLElBQUksQ0FBQzthQUNwQztTQUNGO0lBQ0gsQ0FBQzs7Ozs7Ozs7O0lBVU0sWUFBWSxDQUFDLEtBQUs7UUFDdkIsT0FBTyxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztJQUNuQyxDQUFDOzs7Ozs7Ozs7SUFTTSxZQUFZLENBQUMsTUFBTTtRQUN4QixPQUFPLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7SUFDdkQsQ0FBQzs7Ozs7Ozs7O0lBU00sZ0JBQWdCLENBQUMsTUFBTTs7Y0FDdEIsS0FBSyxHQUFHLE1BQU0sQ0FBQyxRQUFRLEVBQUUsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDO1FBQzFDLElBQUksS0FBSyxDQUFDLE1BQU0sS0FBSyxDQUFDLEVBQUU7WUFDdEIsSUFBSSxLQUFLLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxFQUFFO2dCQUNsQixPQUFPLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQzthQUNqQjtTQUNGO1FBQ0QsT0FBTyxNQUFNLENBQUM7SUFDaEIsQ0FBQzs7Ozs7Ozs7O0lBVU0sU0FBUyxDQUFDLE1BQU07OztjQUVmLEtBQUssR0FBRyxNQUFNLENBQUMsUUFBUSxFQUFFLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQztRQUMxQyx1QkFBdUI7UUFDdkIsSUFBSSxLQUFLLENBQUMsTUFBTSxLQUFLLENBQUMsRUFBRTtZQUN0QiwrREFBK0Q7WUFDL0QsT0FBTyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLHlCQUF5QixFQUFFLEtBQUssQ0FBQyxHQUFHLEdBQUcsR0FBRyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7U0FDNUU7YUFBTTtZQUNMLHdCQUF3QjtZQUN4QixPQUFPLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMseUJBQXlCLEVBQUUsS0FBSyxDQUFDLENBQUM7U0FDM0Q7SUFDSCxDQUFDOzs7WUF0R0YsSUFBSSxTQUFDO2dCQUNKLElBQUksRUFBRSxpQkFBaUI7YUFDeEI7Ozs7SUFLQyx5Q0FxQkUiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBQaXBlLCBQaXBlVHJhbnNmb3JtIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBQaXBlKHtcbiAgbmFtZTogJ21hZ25ldGljRGVjaW1hbCdcbn0pXG4vLyBSZXR1cm5zIHRoZSBhbmdsZSBhcyBhIHN0cmluZyB3aXRoIGFuIGFtb3VudCBvZiBkZWNpbWFsIHBsYWNlcyBiYXNlZCBvbiBpdHMgdW5pdC5cbi8vIEFsc28gZm9ybWF0cyB0aGUgc3RyaW5nIHdpdGggY29tbWFzIGZvciByZWFkYWJpbGl0eS5cbmV4cG9ydCBjbGFzcyBNYWduZXRpY0RlY2ltYWxQaXBlIGltcGxlbWVudHMgUGlwZVRyYW5zZm9ybSB7XG4gIC8vIG51bWJlciBvZiBkZWNpbWFsIHBsYWNlcyB0byBkaXNwbGF5IGZvciB2YXJpb3VzIGFuZ2xlIHVuaXRzXG4gIGRlY2ltYWxNYXAgPSB7XG4gICAgLy8gT2xkIEZvcm1hdFxuICAgIE5BTk9URVNMQTogMCxcbiAgICBNSUNST1RFU0xBOiA0LFxuICAgIE1JTExJVEVTTEE6IDAsXG4gICAgVEVTTEE6IDAsXG4gICAgR0FVU1M6IDUsXG4gICAgR0FNTUE6IDAsXG4gICAgLy8gTmV3IEZvcm1hdFxuICAgIE5hbm90ZXNsYTogMCxcbiAgICBuVDogMCxcbiAgICBNaWNyb3Rlc2xhOiA0LFxuICAgICfCtVQnOiA0LFxuICAgIE1pbGxpdGVzbGE6IDAsXG4gICAgbVQ6IDAsXG4gICAgVGVzbGE6IDAsXG4gICAgVDogMCxcbiAgICBHYXVzczogNSxcbiAgICBHczogNSxcbiAgICBHYW1tYTogMCxcbiAgICAnzrMnOiAwXG4gIH07XG5cbiAgdHJhbnNmb3JtKHZhbHVlOiBhbnksIHVuaXQ/OiBhbnkpOiBhbnkge1xuICAgIGlmICghdGhpcy5pc1ZhbGlkRmxvYXQodmFsdWUpKSB7XG4gICAgICByZXR1cm4gJ04vQSc7XG4gICAgfSBlbHNlIHtcbiAgICAgIHZhbHVlID0gcGFyc2VGbG9hdCh2YWx1ZSk7XG4gICAgICBpZiAodHlwZW9mICh0aGlzLmRlY2ltYWxNYXBbdW5pdF0pICE9PSAndW5kZWZpbmVkJykge1xuICAgICAgICByZXR1cm4gdGhpcy5mb3JtYXROdW1iZXIodmFsdWUudG9GaXhlZCh0aGlzLmRlY2ltYWxNYXBbdW5pdF0pKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHJldHVybiAnVW5zdXBwb3J0ZWQgVW5pdDogJyArIHVuaXQ7XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgLyoqXG4gICAqIGlzVmFsaWRGbG9hdDpcbiAgICpcbiAgICogQSBzdHJpY3RlciBmbG9hdCBmaWx0ZXIgdGhhbiBwYXJzZUZsb2F0KCkuXG4gICAqXG4gICAqIEBwYXJhbSB2YWx1ZSBJbnB1dCB2YWx1ZSB0byBiZSBwYXJzZWQgYXMgZmxvYXRcbiAgICogQHJldHVybnMgZmxvYXRcbiAgICovXG4gIHB1YmxpYyBpc1ZhbGlkRmxvYXQodmFsdWUpIHtcbiAgICByZXR1cm4gIWlzTmFOKHBhcnNlRmxvYXQodmFsdWUpKTtcbiAgfVxuXG4gIC8qKlxuICAgICogZm9ybWF0TnVtYmVyOlxuICAgICpcbiAgICAqIEFkZHMgY29tbWFzIGFuZCByZW1vdmVzIHRoZSBuZWdhdGl2ZSBzaWduIGZvciB6ZXJvIHZhbHVlcy5cbiAgICAqXG4gICAgKiBAcGFyYW0gbnVtYmVyIElucHV0IHZhbHVlIHRvIGJlIGZvcm1hdHRlZFxuICAgICovXG4gIHB1YmxpYyBmb3JtYXROdW1iZXIobnVtYmVyKSB7XG4gICAgcmV0dXJuIHRoaXMucmVtb3ZlU2lnbk9uWmVybyh0aGlzLmFkZENvbW1hcyhudW1iZXIpKTtcbiAgfVxuXG4gIC8qKlxuICAgICAqIHJlbW92ZVNpZ25Pblplcm86XG4gICAgICpcbiAgICAgKiBJZiB0aGUgdmFsdWUgaXMgemVybywgcmVtb3ZlIGFueSBuZWdhdGl2ZSBzaWducy4gKGNvbnZlcnRzIC0wLjAwIHRvIDAuMDApXG4gICAgICpcbiAgICAgKiBAcGFyYW0gbnVtYmVyIElucHV0IHZhbHVlIHRvIGJlIHNhbml0aXplZFxuICAgICAqL1xuICBwdWJsaWMgcmVtb3ZlU2lnbk9uWmVybyhudW1iZXIpIHtcbiAgICBjb25zdCBzcGxpdCA9IG51bWJlci50b1N0cmluZygpLnNwbGl0KCctJyk7XG4gICAgaWYgKHNwbGl0Lmxlbmd0aCA9PT0gMikge1xuICAgICAgaWYgKHNwbGl0WzFdID09PSAwKSB7XG4gICAgICAgIHJldHVybiBzcGxpdFsxXTtcbiAgICAgIH1cbiAgICB9XG4gICAgcmV0dXJuIG51bWJlcjtcbiAgfVxuXG4gIC8qKlxuICAgICAqIGFkZENvbW1hczpcbiAgICAgKlxuICAgICAqIEhlbHBlciBmdW5jdGlvbiB0aGF0IGFkZHMgaW4gY29tbWFzIHRvIGEgbnVtYmVyIGZvciByZWFkYWJpbGl0eS5cbiAgICAgKlxuICAgICAqIEBwYXJhbSBudW1iZXIgSW5wdXQgdmFsdWUgdG8gYWRkIGNvbW1hc1xuICAgICAqIEByZXR1cm5zIHN0cmluZ1xuICAgICAqL1xuICBwdWJsaWMgYWRkQ29tbWFzKG51bWJlcikge1xuICAgIC8vIGNoZWNrIGlmIHdlIGhhdmUgYSBkZWNpbWFsXG4gICAgY29uc3Qgc3BsaXQgPSBudW1iZXIudG9TdHJpbmcoKS5zcGxpdCgnLicpO1xuICAgIC8vIGlmIHdlIGhhdmUgYSBkZWNpbWFsXG4gICAgaWYgKHNwbGl0Lmxlbmd0aCA9PT0gMikge1xuICAgICAgLy8gb25seSBhZGQgY29tbWFzIHRvIHRoZSBmaXJzdCBoYWxmIGFuZCByZXR1cm4gdGhlIHNlY29uZCBoYWxmXG4gICAgICByZXR1cm4gc3BsaXRbMF0ucmVwbGFjZSgvKFxcZCkoPz0oXFxkezN9KSsoPyFcXGQpKS9nLCAnJDEsJykgKyAnLicgKyBzcGxpdFsxXTtcbiAgICB9IGVsc2Uge1xuICAgICAgLy8gYWRkIGNvbW1hcyB0aHJvdWdob3V0XG4gICAgICByZXR1cm4gc3BsaXRbMF0ucmVwbGFjZSgvKFxcZCkoPz0oXFxkezN9KSsoPyFcXGQpKS9nLCAnJDEsJyk7XG4gICAgfVxuICB9XG59XG4iXX0=