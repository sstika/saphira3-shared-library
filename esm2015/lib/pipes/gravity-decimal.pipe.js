/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Pipe } from '@angular/core';
// Returns the gravity value as a string with an amount of decimal places based on its unit.
// Also formats the string with commas for readability.
export class GravityDecimalPipe {
    constructor() {
        // number of decimal places to display for various gravity units
        this.decimalMap = {
            M_PER_SEC_SQ: 3,
            FT_PER_SEC_SQ: 3,
            G: 4,
            MILLI_G: 2,
            G_98: 1,
            GAL: 4,
            MILLI_GAL: 1,
            'Meters per Second Sq.': 3,
            'm/s²': 3,
            'f/s²': 3,
            mG: 2,
            'G (9.8)': 1,
            Gal: 4,
            mGal: 1,
        };
    }
    /**
     * @param {?} value
     * @param {?=} unit
     * @return {?}
     */
    transform(value, unit) {
        if (!this.isValidFloat(value)) {
            return 'N/A';
        }
        else {
            value = parseFloat(value);
            if (typeof (this.decimalMap[unit]) !== 'undefined') {
                return this.formatNumber(value.toFixed(this.decimalMap[unit]));
            }
            else {
                return 'Unsupported Unit: ' + unit;
            }
        }
    }
    /**
     * isValidFloat:
     *
     * A stricter float filter than parseFloat().
     *
     * @param {?} value Input value to be parsed as float
     * @return {?} float
     */
    isValidFloat(value) {
        return !isNaN(parseFloat(value));
    }
    /**
     * formatNumber:
     *
     * Adds commas and removes the negative sign for zero values.
     *
     * @param {?} number Input value to be formatted
     * @return {?}
     */
    formatNumber(number) {
        return this.removeSignOnZero(this.addCommas(number));
    }
    /**
     * removeSignOnZero:
     *
     * If the value is zero, remove any negative signs. (converts -0.00 to 0.00)
     *
     * @param {?} number Input value to be sanitized
     * @return {?}
     */
    removeSignOnZero(number) {
        /** @type {?} */
        const split = number.toString().split('-');
        if (split.length === 2) {
            if (split[1] === 0) {
                return split[1];
            }
        }
        return number;
    }
    /**
     * addCommas:
     *
     * Helper function that adds in commas to a number for readability.
     *
     * @param {?} number Input value to add commas
     * @return {?} string
     */
    addCommas(number) {
        // check if we have a decimal
        /** @type {?} */
        const split = number.toString().split('.');
        // if we have a decimal
        if (split.length === 2) {
            // only add commas to the first half and return the second half
            return split[0].replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,') + '.' + split[1];
        }
        else {
            // add commas throughout
            return split[0].replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
        }
    }
}
GravityDecimalPipe.decorators = [
    { type: Pipe, args: [{
                name: 'gravityDecimal'
            },] }
];
if (false) {
    /** @type {?} */
    GravityDecimalPipe.prototype.decimalMap;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ3Jhdml0eS1kZWNpbWFsLnBpcGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AaHB0ZWNoL3NhcGhpcmEtc2hhcmVkLWxpYi8iLCJzb3VyY2VzIjpbImxpYi9waXBlcy9ncmF2aXR5LWRlY2ltYWwucGlwZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLElBQUksRUFBaUIsTUFBTSxlQUFlLENBQUM7QUFLcEQsNEZBQTRGO0FBQzVGLHVEQUF1RDtBQUN2RCxNQUFNLE9BQU8sa0JBQWtCO0lBTC9COztRQU9FLGVBQVUsR0FBRztZQUNYLFlBQVksRUFBRSxDQUFDO1lBQ2YsYUFBYSxFQUFFLENBQUM7WUFDaEIsQ0FBQyxFQUFFLENBQUM7WUFDSixPQUFPLEVBQUUsQ0FBQztZQUNWLElBQUksRUFBRSxDQUFDO1lBQ1AsR0FBRyxFQUFFLENBQUM7WUFDTixTQUFTLEVBQUUsQ0FBQztZQUNaLHVCQUF1QixFQUFFLENBQUM7WUFDMUIsTUFBTSxFQUFFLENBQUM7WUFDVCxNQUFNLEVBQUUsQ0FBQztZQUNULEVBQUUsRUFBRSxDQUFDO1lBQ0wsU0FBUyxFQUFFLENBQUM7WUFDWixHQUFHLEVBQUUsQ0FBQztZQUNOLElBQUksRUFBRSxDQUFDO1NBQ1IsQ0FBQztJQTRFSixDQUFDOzs7Ozs7SUExRUMsU0FBUyxDQUFDLEtBQVUsRUFBRSxJQUFVO1FBQzlCLElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssQ0FBQyxFQUFFO1lBQzdCLE9BQU8sS0FBSyxDQUFDO1NBQ2Q7YUFBTTtZQUNMLEtBQUssR0FBRyxVQUFVLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDMUIsSUFBSSxPQUFPLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQyxLQUFLLFdBQVcsRUFBRTtnQkFDbEQsT0FBTyxJQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7YUFDaEU7aUJBQU07Z0JBQ0wsT0FBTyxvQkFBb0IsR0FBRyxJQUFJLENBQUM7YUFDcEM7U0FDRjtJQUNILENBQUM7Ozs7Ozs7OztJQVVNLFlBQVksQ0FBQyxLQUFLO1FBQ3ZCLE9BQU8sQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7SUFDbkMsQ0FBQzs7Ozs7Ozs7O0lBU00sWUFBWSxDQUFDLE1BQU07UUFDeEIsT0FBTyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO0lBQ3ZELENBQUM7Ozs7Ozs7OztJQVNNLGdCQUFnQixDQUFDLE1BQU07O2NBQ3RCLEtBQUssR0FBRyxNQUFNLENBQUMsUUFBUSxFQUFFLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQztRQUMxQyxJQUFJLEtBQUssQ0FBQyxNQUFNLEtBQUssQ0FBQyxFQUFFO1lBQ3RCLElBQUksS0FBSyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsRUFBRTtnQkFDbEIsT0FBTyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7YUFDakI7U0FDRjtRQUNELE9BQU8sTUFBTSxDQUFDO0lBQ2hCLENBQUM7Ozs7Ozs7OztJQVVNLFNBQVMsQ0FBQyxNQUFNOzs7Y0FFZixLQUFLLEdBQUcsTUFBTSxDQUFDLFFBQVEsRUFBRSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUM7UUFDMUMsdUJBQXVCO1FBQ3ZCLElBQUksS0FBSyxDQUFDLE1BQU0sS0FBSyxDQUFDLEVBQUU7WUFDdEIsK0RBQStEO1lBQy9ELE9BQU8sS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyx5QkFBeUIsRUFBRSxLQUFLLENBQUMsR0FBRyxHQUFHLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO1NBQzVFO2FBQU07WUFDTCx3QkFBd0I7WUFDeEIsT0FBTyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLHlCQUF5QixFQUFFLEtBQUssQ0FBQyxDQUFDO1NBQzNEO0lBQ0gsQ0FBQzs7O1lBaEdGLElBQUksU0FBQztnQkFDSixJQUFJLEVBQUUsZ0JBQWdCO2FBQ3ZCOzs7O0lBS0Msd0NBZUUiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBQaXBlLCBQaXBlVHJhbnNmb3JtIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBQaXBlKHtcbiAgbmFtZTogJ2dyYXZpdHlEZWNpbWFsJ1xufSlcbi8vIFJldHVybnMgdGhlIGdyYXZpdHkgdmFsdWUgYXMgYSBzdHJpbmcgd2l0aCBhbiBhbW91bnQgb2YgZGVjaW1hbCBwbGFjZXMgYmFzZWQgb24gaXRzIHVuaXQuXG4vLyBBbHNvIGZvcm1hdHMgdGhlIHN0cmluZyB3aXRoIGNvbW1hcyBmb3IgcmVhZGFiaWxpdHkuXG5leHBvcnQgY2xhc3MgR3Jhdml0eURlY2ltYWxQaXBlIGltcGxlbWVudHMgUGlwZVRyYW5zZm9ybSB7XG4gIC8vIG51bWJlciBvZiBkZWNpbWFsIHBsYWNlcyB0byBkaXNwbGF5IGZvciB2YXJpb3VzIGdyYXZpdHkgdW5pdHNcbiAgZGVjaW1hbE1hcCA9IHtcbiAgICBNX1BFUl9TRUNfU1E6IDMsXG4gICAgRlRfUEVSX1NFQ19TUTogMyxcbiAgICBHOiA0LFxuICAgIE1JTExJX0c6IDIsXG4gICAgR185ODogMSxcbiAgICBHQUw6IDQsXG4gICAgTUlMTElfR0FMOiAxLFxuICAgICdNZXRlcnMgcGVyIFNlY29uZCBTcS4nOiAzLFxuICAgICdtL3PCsic6IDMsXG4gICAgJ2Yvc8KyJzogMyxcbiAgICBtRzogMixcbiAgICAnRyAoOS44KSc6IDEsXG4gICAgR2FsOiA0LFxuICAgIG1HYWw6IDEsXG4gIH07XG5cbiAgdHJhbnNmb3JtKHZhbHVlOiBhbnksIHVuaXQ/OiBhbnkpOiBhbnkge1xuICAgIGlmICghdGhpcy5pc1ZhbGlkRmxvYXQodmFsdWUpKSB7XG4gICAgICByZXR1cm4gJ04vQSc7XG4gICAgfSBlbHNlIHtcbiAgICAgIHZhbHVlID0gcGFyc2VGbG9hdCh2YWx1ZSk7XG4gICAgICBpZiAodHlwZW9mICh0aGlzLmRlY2ltYWxNYXBbdW5pdF0pICE9PSAndW5kZWZpbmVkJykge1xuICAgICAgICByZXR1cm4gdGhpcy5mb3JtYXROdW1iZXIodmFsdWUudG9GaXhlZCh0aGlzLmRlY2ltYWxNYXBbdW5pdF0pKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHJldHVybiAnVW5zdXBwb3J0ZWQgVW5pdDogJyArIHVuaXQ7XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgLyoqXG4gICAqIGlzVmFsaWRGbG9hdDpcbiAgICpcbiAgICogQSBzdHJpY3RlciBmbG9hdCBmaWx0ZXIgdGhhbiBwYXJzZUZsb2F0KCkuXG4gICAqXG4gICAqIEBwYXJhbSB2YWx1ZSBJbnB1dCB2YWx1ZSB0byBiZSBwYXJzZWQgYXMgZmxvYXRcbiAgICogQHJldHVybnMgZmxvYXRcbiAgICovXG4gIHB1YmxpYyBpc1ZhbGlkRmxvYXQodmFsdWUpIHtcbiAgICByZXR1cm4gIWlzTmFOKHBhcnNlRmxvYXQodmFsdWUpKTtcbiAgfVxuXG4gIC8qKlxuICAgICogZm9ybWF0TnVtYmVyOlxuICAgICpcbiAgICAqIEFkZHMgY29tbWFzIGFuZCByZW1vdmVzIHRoZSBuZWdhdGl2ZSBzaWduIGZvciB6ZXJvIHZhbHVlcy5cbiAgICAqXG4gICAgKiBAcGFyYW0gbnVtYmVyIElucHV0IHZhbHVlIHRvIGJlIGZvcm1hdHRlZFxuICAgICovXG4gIHB1YmxpYyBmb3JtYXROdW1iZXIobnVtYmVyKSB7XG4gICAgcmV0dXJuIHRoaXMucmVtb3ZlU2lnbk9uWmVybyh0aGlzLmFkZENvbW1hcyhudW1iZXIpKTtcbiAgfVxuXG4gIC8qKlxuICAgICAqIHJlbW92ZVNpZ25Pblplcm86XG4gICAgICpcbiAgICAgKiBJZiB0aGUgdmFsdWUgaXMgemVybywgcmVtb3ZlIGFueSBuZWdhdGl2ZSBzaWducy4gKGNvbnZlcnRzIC0wLjAwIHRvIDAuMDApXG4gICAgICpcbiAgICAgKiBAcGFyYW0gbnVtYmVyIElucHV0IHZhbHVlIHRvIGJlIHNhbml0aXplZFxuICAgICAqL1xuICBwdWJsaWMgcmVtb3ZlU2lnbk9uWmVybyhudW1iZXIpIHtcbiAgICBjb25zdCBzcGxpdCA9IG51bWJlci50b1N0cmluZygpLnNwbGl0KCctJyk7XG4gICAgaWYgKHNwbGl0Lmxlbmd0aCA9PT0gMikge1xuICAgICAgaWYgKHNwbGl0WzFdID09PSAwKSB7XG4gICAgICAgIHJldHVybiBzcGxpdFsxXTtcbiAgICAgIH1cbiAgICB9XG4gICAgcmV0dXJuIG51bWJlcjtcbiAgfVxuXG4gIC8qKlxuICAgICAqIGFkZENvbW1hczpcbiAgICAgKlxuICAgICAqIEhlbHBlciBmdW5jdGlvbiB0aGF0IGFkZHMgaW4gY29tbWFzIHRvIGEgbnVtYmVyIGZvciByZWFkYWJpbGl0eS5cbiAgICAgKlxuICAgICAqIEBwYXJhbSBudW1iZXIgSW5wdXQgdmFsdWUgdG8gYWRkIGNvbW1hc1xuICAgICAqIEByZXR1cm5zIHN0cmluZ1xuICAgICAqL1xuICBwdWJsaWMgYWRkQ29tbWFzKG51bWJlcikge1xuICAgIC8vIGNoZWNrIGlmIHdlIGhhdmUgYSBkZWNpbWFsXG4gICAgY29uc3Qgc3BsaXQgPSBudW1iZXIudG9TdHJpbmcoKS5zcGxpdCgnLicpO1xuICAgIC8vIGlmIHdlIGhhdmUgYSBkZWNpbWFsXG4gICAgaWYgKHNwbGl0Lmxlbmd0aCA9PT0gMikge1xuICAgICAgLy8gb25seSBhZGQgY29tbWFzIHRvIHRoZSBmaXJzdCBoYWxmIGFuZCByZXR1cm4gdGhlIHNlY29uZCBoYWxmXG4gICAgICByZXR1cm4gc3BsaXRbMF0ucmVwbGFjZSgvKFxcZCkoPz0oXFxkezN9KSsoPyFcXGQpKS9nLCAnJDEsJykgKyAnLicgKyBzcGxpdFsxXTtcbiAgICB9IGVsc2Uge1xuICAgICAgLy8gYWRkIGNvbW1hcyB0aHJvdWdob3V0XG4gICAgICByZXR1cm4gc3BsaXRbMF0ucmVwbGFjZSgvKFxcZCkoPz0oXFxkezN9KSsoPyFcXGQpKS9nLCAnJDEsJyk7XG4gICAgfVxuICB9XG5cbn1cbiJdfQ==