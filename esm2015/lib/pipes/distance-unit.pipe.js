/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Pipe } from '@angular/core';
export class DistanceUnitPipe {
    /**
     * @param {?} unit
     * @param {?=} full
     * @return {?}
     */
    transform(unit, full) {
        /** @type {?} */
        let string = '';
        switch (unit) {
            case 'METER':
                string = 'Meters';
                break;
            case 'KILOMETER':
                string = 'Kilometers';
                break;
            case 'FOOT':
                string = 'Feet';
                break;
            case 'FOOT_US':
                string = 'ft US';
                break;
            case 'YARD':
                string = 'Yards';
                break;
            case 'MILE':
                string = 'Miles';
                break;
            default:
                string = unit;
                break;
        }
        return string;
    }
}
DistanceUnitPipe.decorators = [
    { type: Pipe, args: [{
                name: 'distanceUnit'
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGlzdGFuY2UtdW5pdC5waXBlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGhwdGVjaC9zYXBoaXJhLXNoYXJlZC1saWIvIiwic291cmNlcyI6WyJsaWIvcGlwZXMvZGlzdGFuY2UtdW5pdC5waXBlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsSUFBSSxFQUFpQixNQUFNLGVBQWUsQ0FBQztBQUtwRCxNQUFNLE9BQU8sZ0JBQWdCOzs7Ozs7SUFFM0IsU0FBUyxDQUFDLElBQVMsRUFBRSxJQUFVOztZQUN6QixNQUFNLEdBQUcsRUFBRTtRQUNmLFFBQVEsSUFBSSxFQUFFO1lBQ1osS0FBSyxPQUFPO2dCQUNWLE1BQU0sR0FBRyxRQUFRLENBQUM7Z0JBQ2xCLE1BQU07WUFDUixLQUFLLFdBQVc7Z0JBQ2QsTUFBTSxHQUFHLFlBQVksQ0FBQztnQkFDdEIsTUFBTTtZQUNSLEtBQUssTUFBTTtnQkFDVCxNQUFNLEdBQUcsTUFBTSxDQUFDO2dCQUNoQixNQUFNO1lBQ1IsS0FBSyxTQUFTO2dCQUNaLE1BQU0sR0FBRyxPQUFPLENBQUM7Z0JBQ2pCLE1BQU07WUFDUixLQUFLLE1BQU07Z0JBQ1QsTUFBTSxHQUFHLE9BQU8sQ0FBQztnQkFDakIsTUFBTTtZQUNSLEtBQUssTUFBTTtnQkFDVCxNQUFNLEdBQUcsT0FBTyxDQUFDO2dCQUNqQixNQUFNO1lBQ1I7Z0JBQ0UsTUFBTSxHQUFHLElBQUksQ0FBQztnQkFDZCxNQUFNO1NBQ1Q7UUFDRCxPQUFPLE1BQU0sQ0FBQztJQUNoQixDQUFDOzs7WUEvQkYsSUFBSSxTQUFDO2dCQUNKLElBQUksRUFBRSxjQUFjO2FBQ3JCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgUGlwZSwgUGlwZVRyYW5zZm9ybSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5AUGlwZSh7XG4gIG5hbWU6ICdkaXN0YW5jZVVuaXQnXG59KVxuZXhwb3J0IGNsYXNzIERpc3RhbmNlVW5pdFBpcGUgaW1wbGVtZW50cyBQaXBlVHJhbnNmb3JtIHtcblxuICB0cmFuc2Zvcm0odW5pdDogYW55LCBmdWxsPzogYW55KTogYW55IHtcbiAgICBsZXQgc3RyaW5nID0gJyc7XG4gICAgc3dpdGNoICh1bml0KSB7XG4gICAgICBjYXNlICdNRVRFUic6XG4gICAgICAgIHN0cmluZyA9ICdNZXRlcnMnO1xuICAgICAgICBicmVhaztcbiAgICAgIGNhc2UgJ0tJTE9NRVRFUic6XG4gICAgICAgIHN0cmluZyA9ICdLaWxvbWV0ZXJzJztcbiAgICAgICAgYnJlYWs7XG4gICAgICBjYXNlICdGT09UJzpcbiAgICAgICAgc3RyaW5nID0gJ0ZlZXQnO1xuICAgICAgICBicmVhaztcbiAgICAgIGNhc2UgJ0ZPT1RfVVMnOlxuICAgICAgICBzdHJpbmcgPSAnZnQgVVMnO1xuICAgICAgICBicmVhaztcbiAgICAgIGNhc2UgJ1lBUkQnOlxuICAgICAgICBzdHJpbmcgPSAnWWFyZHMnO1xuICAgICAgICBicmVhaztcbiAgICAgIGNhc2UgJ01JTEUnOlxuICAgICAgICBzdHJpbmcgPSAnTWlsZXMnO1xuICAgICAgICBicmVhaztcbiAgICAgIGRlZmF1bHQ6XG4gICAgICAgIHN0cmluZyA9IHVuaXQ7XG4gICAgICAgIGJyZWFrO1xuICAgIH1cbiAgICByZXR1cm4gc3RyaW5nO1xuICB9XG5cbn1cbiJdfQ==