/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Pipe } from '@angular/core';
// Converts between survey types and abbreviations.
export class SurveyTypePipe {
    constructor() {
        this.surveyTypes = {
            'STANDARD': {
                definitive: true,
                interpolated: false,
                acronym: 'D-S'
            },
            'POOR': {
                definitive: true,
                interpolated: false,
                acronym: 'D-P'
            },
            'DEFINITIVE_INTERPOLATED': {
                definitive: true,
                interpolated: true,
                acronym: 'D-I'
            },
            'BAD': {
                definitive: false,
                interpolated: false,
                acronym: 'X-B'
            },
            'ACC_CHECK': {
                definitive: false,
                interpolated: false,
                acronym: 'X-A'
            },
            'CHECKSHOT': {
                definitive: false,
                interpolated: false,
                acronym: 'X-C'
            },
            'INTERPOLATED': {
                definitive: false,
                interpolated: false,
                acronym: 'X-I'
            }
        };
    }
    /**
     * @param {?} value
     * @return {?}
     */
    transform(value) {
        if (this.surveyTypes[value]) {
            return this.surveyTypes[value].acronym;
        }
        else {
            return 'Unsupported Type';
        }
    }
}
SurveyTypePipe.decorators = [
    { type: Pipe, args: [{
                name: 'surveyType'
            },] }
];
if (false) {
    /** @type {?} */
    SurveyTypePipe.prototype.surveyTypes;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3VydmV5LXR5cGUucGlwZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BocHRlY2gvc2FwaGlyYS1zaGFyZWQtbGliLyIsInNvdXJjZXMiOlsibGliL3BpcGVzL3N1cnZleS10eXBlLnBpcGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxJQUFJLEVBQWlCLE1BQU0sZUFBZSxDQUFDO0FBS3BELG1EQUFtRDtBQUNuRCxNQUFNLE9BQU8sY0FBYztJQUozQjtRQUtFLGdCQUFXLEdBQUc7WUFDWixVQUFVLEVBQUU7Z0JBQ1YsVUFBVSxFQUFFLElBQUk7Z0JBQ2hCLFlBQVksRUFBRSxLQUFLO2dCQUNuQixPQUFPLEVBQUUsS0FBSzthQUNmO1lBQ0QsTUFBTSxFQUFFO2dCQUNOLFVBQVUsRUFBRSxJQUFJO2dCQUNoQixZQUFZLEVBQUUsS0FBSztnQkFDbkIsT0FBTyxFQUFFLEtBQUs7YUFDZjtZQUNELHlCQUF5QixFQUFFO2dCQUN6QixVQUFVLEVBQUUsSUFBSTtnQkFDaEIsWUFBWSxFQUFFLElBQUk7Z0JBQ2xCLE9BQU8sRUFBRSxLQUFLO2FBQ2Y7WUFDRCxLQUFLLEVBQUU7Z0JBQ0wsVUFBVSxFQUFFLEtBQUs7Z0JBQ2pCLFlBQVksRUFBRSxLQUFLO2dCQUNuQixPQUFPLEVBQUUsS0FBSzthQUNmO1lBQ0QsV0FBVyxFQUFFO2dCQUNYLFVBQVUsRUFBRSxLQUFLO2dCQUNqQixZQUFZLEVBQUUsS0FBSztnQkFDbkIsT0FBTyxFQUFFLEtBQUs7YUFDZjtZQUNELFdBQVcsRUFBRTtnQkFDWCxVQUFVLEVBQUUsS0FBSztnQkFDakIsWUFBWSxFQUFFLEtBQUs7Z0JBQ25CLE9BQU8sRUFBRSxLQUFLO2FBQ2Y7WUFDRCxjQUFjLEVBQUU7Z0JBQ2QsVUFBVSxFQUFFLEtBQUs7Z0JBQ2pCLFlBQVksRUFBRSxLQUFLO2dCQUNuQixPQUFPLEVBQUUsS0FBSzthQUNmO1NBQ0YsQ0FBQztJQVNKLENBQUM7Ozs7O0lBUEMsU0FBUyxDQUFDLEtBQWE7UUFDckIsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxFQUFFO1lBQzNCLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxPQUFPLENBQUM7U0FDeEM7YUFBTTtZQUNMLE9BQU8sa0JBQWtCLENBQUM7U0FDM0I7SUFDSCxDQUFDOzs7WUFqREYsSUFBSSxTQUFDO2dCQUNKLElBQUksRUFBRSxZQUFZO2FBQ25COzs7O0lBR0MscUNBb0NFIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgUGlwZSwgUGlwZVRyYW5zZm9ybSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5AUGlwZSh7XG4gIG5hbWU6ICdzdXJ2ZXlUeXBlJ1xufSlcbi8vIENvbnZlcnRzIGJldHdlZW4gc3VydmV5IHR5cGVzIGFuZCBhYmJyZXZpYXRpb25zLlxuZXhwb3J0IGNsYXNzIFN1cnZleVR5cGVQaXBlIGltcGxlbWVudHMgUGlwZVRyYW5zZm9ybSB7XG4gIHN1cnZleVR5cGVzID0ge1xuICAgICdTVEFOREFSRCc6IHtcbiAgICAgIGRlZmluaXRpdmU6IHRydWUsXG4gICAgICBpbnRlcnBvbGF0ZWQ6IGZhbHNlLFxuICAgICAgYWNyb255bTogJ0QtUydcbiAgICB9LFxuICAgICdQT09SJzoge1xuICAgICAgZGVmaW5pdGl2ZTogdHJ1ZSxcbiAgICAgIGludGVycG9sYXRlZDogZmFsc2UsXG4gICAgICBhY3JvbnltOiAnRC1QJ1xuICAgIH0sXG4gICAgJ0RFRklOSVRJVkVfSU5URVJQT0xBVEVEJzoge1xuICAgICAgZGVmaW5pdGl2ZTogdHJ1ZSxcbiAgICAgIGludGVycG9sYXRlZDogdHJ1ZSxcbiAgICAgIGFjcm9ueW06ICdELUknXG4gICAgfSxcbiAgICAnQkFEJzoge1xuICAgICAgZGVmaW5pdGl2ZTogZmFsc2UsXG4gICAgICBpbnRlcnBvbGF0ZWQ6IGZhbHNlLFxuICAgICAgYWNyb255bTogJ1gtQidcbiAgICB9LFxuICAgICdBQ0NfQ0hFQ0snOiB7XG4gICAgICBkZWZpbml0aXZlOiBmYWxzZSxcbiAgICAgIGludGVycG9sYXRlZDogZmFsc2UsXG4gICAgICBhY3JvbnltOiAnWC1BJ1xuICAgIH0sXG4gICAgJ0NIRUNLU0hPVCc6IHtcbiAgICAgIGRlZmluaXRpdmU6IGZhbHNlLFxuICAgICAgaW50ZXJwb2xhdGVkOiBmYWxzZSxcbiAgICAgIGFjcm9ueW06ICdYLUMnXG4gICAgfSxcbiAgICAnSU5URVJQT0xBVEVEJzoge1xuICAgICAgZGVmaW5pdGl2ZTogZmFsc2UsXG4gICAgICBpbnRlcnBvbGF0ZWQ6IGZhbHNlLFxuICAgICAgYWNyb255bTogJ1gtSSdcbiAgICB9XG4gIH07XG5cbiAgdHJhbnNmb3JtKHZhbHVlOiBzdHJpbmcpOiBhbnkge1xuICAgIGlmICh0aGlzLnN1cnZleVR5cGVzW3ZhbHVlXSkge1xuICAgICAgcmV0dXJuIHRoaXMuc3VydmV5VHlwZXNbdmFsdWVdLmFjcm9ueW07XG4gICAgfSBlbHNlIHtcbiAgICAgIHJldHVybiAnVW5zdXBwb3J0ZWQgVHlwZSc7XG4gICAgfVxuICB9XG59XG4iXX0=