/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Pipe } from '@angular/core';
export class DefinitiveSurveysPipe {
    constructor() {
        this.surveyTypes = {
            'STANDARD': {
                definitive: true,
                interpolated: false,
                acronym: 'D-S'
            },
            'POOR': {
                definitive: true,
                interpolated: false,
                acronym: 'D-P'
            },
            'DEFINITIVE_INTERPOLATED': {
                definitive: true,
                interpolated: true,
                acronym: 'D-I'
            },
            'BAD': {
                definitive: false,
                interpolated: false,
                acronym: 'X-B'
            },
            'ACC_CHECK': {
                definitive: false,
                interpolated: false,
                acronym: 'X-A'
            },
            'CHECKSHOT': {
                definitive: false,
                interpolated: false,
                acronym: 'X-C'
            },
            'INTERPOLATED': {
                definitive: false,
                interpolated: false,
                acronym: 'X-I'
            }
        };
    }
    /**
     * @param {?} surveys
     * @param {?=} includeInterpolated
     * @return {?}
     */
    transform(surveys, includeInterpolated) {
        if (surveys) {
            return surveys.filter((/**
             * @param {?} survey
             * @return {?}
             */
            survey => {
                if (includeInterpolated) {
                    return this.surveyTypes[survey.type] && this.surveyTypes[survey.type].definitive;
                }
                else {
                    return this.surveyTypes[survey.type] && this.surveyTypes[survey.type].definitive && !this.surveyTypes[survey.type].interpolated;
                }
            }));
        }
        else {
            return surveys;
        }
    }
}
DefinitiveSurveysPipe.decorators = [
    { type: Pipe, args: [{
                name: 'definitiveSurveys'
            },] }
];
if (false) {
    /** @type {?} */
    DefinitiveSurveysPipe.prototype.surveyTypes;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGVmaW5pdGl2ZS1zdXJ2ZXlzLnBpcGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AaHB0ZWNoL3NhcGhpcmEtc2hhcmVkLWxpYi8iLCJzb3VyY2VzIjpbImxpYi9waXBlcy9kZWZpbml0aXZlLXN1cnZleXMucGlwZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLElBQUksRUFBaUIsTUFBTSxlQUFlLENBQUM7QUFLcEQsTUFBTSxPQUFPLHFCQUFxQjtJQUhsQztRQUlFLGdCQUFXLEdBQUc7WUFDWixVQUFVLEVBQUU7Z0JBQ1YsVUFBVSxFQUFFLElBQUk7Z0JBQ2hCLFlBQVksRUFBRSxLQUFLO2dCQUNuQixPQUFPLEVBQUUsS0FBSzthQUNmO1lBQ0QsTUFBTSxFQUFFO2dCQUNOLFVBQVUsRUFBRSxJQUFJO2dCQUNoQixZQUFZLEVBQUUsS0FBSztnQkFDbkIsT0FBTyxFQUFFLEtBQUs7YUFDZjtZQUNELHlCQUF5QixFQUFFO2dCQUN6QixVQUFVLEVBQUUsSUFBSTtnQkFDaEIsWUFBWSxFQUFFLElBQUk7Z0JBQ2xCLE9BQU8sRUFBRSxLQUFLO2FBQ2Y7WUFDRCxLQUFLLEVBQUU7Z0JBQ0wsVUFBVSxFQUFFLEtBQUs7Z0JBQ2pCLFlBQVksRUFBRSxLQUFLO2dCQUNuQixPQUFPLEVBQUUsS0FBSzthQUNmO1lBQ0QsV0FBVyxFQUFFO2dCQUNYLFVBQVUsRUFBRSxLQUFLO2dCQUNqQixZQUFZLEVBQUUsS0FBSztnQkFDbkIsT0FBTyxFQUFFLEtBQUs7YUFDZjtZQUNELFdBQVcsRUFBRTtnQkFDWCxVQUFVLEVBQUUsS0FBSztnQkFDakIsWUFBWSxFQUFFLEtBQUs7Z0JBQ25CLE9BQU8sRUFBRSxLQUFLO2FBQ2Y7WUFDRCxjQUFjLEVBQUU7Z0JBQ2QsVUFBVSxFQUFFLEtBQUs7Z0JBQ2pCLFlBQVksRUFBRSxLQUFLO2dCQUNuQixPQUFPLEVBQUUsS0FBSzthQUNmO1NBQ0YsQ0FBQztJQWVKLENBQUM7Ozs7OztJQWJDLFNBQVMsQ0FBQyxPQUFZLEVBQUUsbUJBQXlCO1FBQy9DLElBQUksT0FBTyxFQUFFO1lBQ1gsT0FBTyxPQUFPLENBQUMsTUFBTTs7OztZQUFDLE1BQU0sQ0FBQyxFQUFFO2dCQUM3QixJQUFJLG1CQUFtQixFQUFFO29CQUN2QixPQUFPLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDLFVBQVUsQ0FBQztpQkFDbEY7cUJBQU07b0JBQ0wsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQyxVQUFVLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQyxZQUFZLENBQUM7aUJBQ2pJO1lBQ0gsQ0FBQyxFQUFDLENBQUM7U0FDSjthQUFNO1lBQ0wsT0FBTyxPQUFPLENBQUM7U0FDaEI7SUFDSCxDQUFDOzs7WUF0REYsSUFBSSxTQUFDO2dCQUNKLElBQUksRUFBRSxtQkFBbUI7YUFDMUI7Ozs7SUFFQyw0Q0FvQ0UiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBQaXBlLCBQaXBlVHJhbnNmb3JtIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBQaXBlKHtcbiAgbmFtZTogJ2RlZmluaXRpdmVTdXJ2ZXlzJ1xufSlcbmV4cG9ydCBjbGFzcyBEZWZpbml0aXZlU3VydmV5c1BpcGUgaW1wbGVtZW50cyBQaXBlVHJhbnNmb3JtIHtcbiAgc3VydmV5VHlwZXMgPSB7XG4gICAgJ1NUQU5EQVJEJzoge1xuICAgICAgZGVmaW5pdGl2ZTogdHJ1ZSxcbiAgICAgIGludGVycG9sYXRlZDogZmFsc2UsXG4gICAgICBhY3JvbnltOiAnRC1TJ1xuICAgIH0sXG4gICAgJ1BPT1InOiB7XG4gICAgICBkZWZpbml0aXZlOiB0cnVlLFxuICAgICAgaW50ZXJwb2xhdGVkOiBmYWxzZSxcbiAgICAgIGFjcm9ueW06ICdELVAnXG4gICAgfSxcbiAgICAnREVGSU5JVElWRV9JTlRFUlBPTEFURUQnOiB7XG4gICAgICBkZWZpbml0aXZlOiB0cnVlLFxuICAgICAgaW50ZXJwb2xhdGVkOiB0cnVlLFxuICAgICAgYWNyb255bTogJ0QtSSdcbiAgICB9LFxuICAgICdCQUQnOiB7XG4gICAgICBkZWZpbml0aXZlOiBmYWxzZSxcbiAgICAgIGludGVycG9sYXRlZDogZmFsc2UsXG4gICAgICBhY3JvbnltOiAnWC1CJ1xuICAgIH0sXG4gICAgJ0FDQ19DSEVDSyc6IHtcbiAgICAgIGRlZmluaXRpdmU6IGZhbHNlLFxuICAgICAgaW50ZXJwb2xhdGVkOiBmYWxzZSxcbiAgICAgIGFjcm9ueW06ICdYLUEnXG4gICAgfSxcbiAgICAnQ0hFQ0tTSE9UJzoge1xuICAgICAgZGVmaW5pdGl2ZTogZmFsc2UsXG4gICAgICBpbnRlcnBvbGF0ZWQ6IGZhbHNlLFxuICAgICAgYWNyb255bTogJ1gtQydcbiAgICB9LFxuICAgICdJTlRFUlBPTEFURUQnOiB7XG4gICAgICBkZWZpbml0aXZlOiBmYWxzZSxcbiAgICAgIGludGVycG9sYXRlZDogZmFsc2UsXG4gICAgICBhY3JvbnltOiAnWC1JJ1xuICAgIH1cbiAgfTtcblxuICB0cmFuc2Zvcm0oc3VydmV5czogYW55LCBpbmNsdWRlSW50ZXJwb2xhdGVkPzogYW55KTogYW55IHtcbiAgICBpZiAoc3VydmV5cykge1xuICAgICAgcmV0dXJuIHN1cnZleXMuZmlsdGVyKHN1cnZleSA9PiB7XG4gICAgICAgIGlmIChpbmNsdWRlSW50ZXJwb2xhdGVkKSB7XG4gICAgICAgICAgcmV0dXJuIHRoaXMuc3VydmV5VHlwZXNbc3VydmV5LnR5cGVdICYmIHRoaXMuc3VydmV5VHlwZXNbc3VydmV5LnR5cGVdLmRlZmluaXRpdmU7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgcmV0dXJuIHRoaXMuc3VydmV5VHlwZXNbc3VydmV5LnR5cGVdICYmIHRoaXMuc3VydmV5VHlwZXNbc3VydmV5LnR5cGVdLmRlZmluaXRpdmUgJiYgIXRoaXMuc3VydmV5VHlwZXNbc3VydmV5LnR5cGVdLmludGVycG9sYXRlZDtcbiAgICAgICAgfVxuICAgICAgfSk7XG4gICAgfSBlbHNlIHtcbiAgICAgIHJldHVybiBzdXJ2ZXlzO1xuICAgIH1cbiAgfVxufVxuIl19