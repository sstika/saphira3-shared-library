/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CapitalizePipe } from './capitalize.pipe';
import { EllipsisizePipe } from './ellipsisize.pipe';
import { WellboreStatusPipe } from './wellbore-status.pipe';
import { GravityUnitPipe } from './gravity-unit.pipe';
import { AngleUnitPipe } from './angle-unit.pipe';
import { MagneticUnitPipe } from './magnetic-unit.pipe';
import { DistanceUnitPipe } from './distance-unit.pipe';
import { BooleanOnOffPipe } from './boolean-on-off.pipe';
import { TimeAgoPipe } from './time-ago.pipe';
import { SurveySetStatePipe } from './survey-set-state.pipe';
import { DistanceDecimalPipe } from './distance-decimal.pipe';
import { AngleDecimalPipe } from './angle-decimal.pipe';
import { GravityDecimalPipe } from './gravity-decimal.pipe';
import { MagneticDecimalPipe } from './magnetic-decimal.pipe';
import { ResolvedStatePipe } from './resolved-state.pipe';
import { SurveyTypePipe } from './survey-type.pipe';
import { DefinitiveSurveysPipe } from './definitive-surveys.pipe';
import { MeasurePipe } from './measure.pipe';
import { OrderSurveysByMdPipe } from './order-surveys-by-md.pipe';
import { OrderByPipe } from './order-by.pipe';
import { InstantPipe } from './instant.pipe';
import { TrajectoryTypePipe } from './trajectory-type.pipe';
import { FormatObjectPipe } from './format-object.pipe';
export class PipesModule {
}
PipesModule.decorators = [
    { type: NgModule, args: [{
                declarations: [
                    CapitalizePipe,
                    EllipsisizePipe,
                    WellboreStatusPipe,
                    GravityUnitPipe,
                    AngleUnitPipe,
                    MagneticUnitPipe,
                    DistanceUnitPipe,
                    BooleanOnOffPipe,
                    TimeAgoPipe,
                    SurveySetStatePipe,
                    DistanceDecimalPipe,
                    AngleDecimalPipe,
                    GravityDecimalPipe,
                    MagneticDecimalPipe,
                    ResolvedStatePipe,
                    SurveyTypePipe,
                    DefinitiveSurveysPipe,
                    MeasurePipe,
                    OrderSurveysByMdPipe,
                    OrderByPipe,
                    InstantPipe,
                    TrajectoryTypePipe,
                    FormatObjectPipe
                ],
                exports: [
                    CapitalizePipe,
                    EllipsisizePipe,
                    WellboreStatusPipe,
                    GravityUnitPipe,
                    AngleUnitPipe,
                    MagneticUnitPipe,
                    DistanceUnitPipe,
                    BooleanOnOffPipe,
                    TimeAgoPipe,
                    SurveySetStatePipe,
                    DistanceDecimalPipe,
                    AngleDecimalPipe,
                    GravityDecimalPipe,
                    MagneticDecimalPipe,
                    ResolvedStatePipe,
                    SurveyTypePipe,
                    DefinitiveSurveysPipe,
                    MeasurePipe,
                    OrderSurveysByMdPipe,
                    OrderByPipe,
                    InstantPipe,
                    TrajectoryTypePipe,
                    FormatObjectPipe
                ],
                imports: [
                    CommonModule
                ]
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGlwZXMubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGhwdGVjaC9zYXBoaXJhLXNoYXJlZC1saWIvIiwic291cmNlcyI6WyJsaWIvcGlwZXMvcGlwZXMubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUUvQyxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sbUJBQW1CLENBQUM7QUFDbkQsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLG9CQUFvQixDQUFDO0FBQ3JELE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLHdCQUF3QixDQUFDO0FBQzVELE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSxxQkFBcUIsQ0FBQztBQUN0RCxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sbUJBQW1CLENBQUM7QUFDbEQsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFDeEQsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFDeEQsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sdUJBQXVCLENBQUM7QUFDekQsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQzlDLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLHlCQUF5QixDQUFDO0FBQzdELE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLHlCQUF5QixDQUFDO0FBQzlELE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBQ3hELE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLHdCQUF3QixDQUFDO0FBQzVELE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLHlCQUF5QixDQUFDO0FBQzlELE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLHVCQUF1QixDQUFDO0FBQzFELE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSxvQkFBb0IsQ0FBQztBQUNwRCxPQUFPLEVBQUUscUJBQXFCLEVBQUUsTUFBTSwyQkFBMkIsQ0FBQztBQUNsRSxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFDN0MsT0FBTyxFQUFFLG9CQUFvQixFQUFFLE1BQU0sNEJBQTRCLENBQUM7QUFDbEUsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQzlDLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUM3QyxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSx3QkFBd0IsQ0FBQztBQUM1RCxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQTBEeEQsTUFBTSxPQUFPLFdBQVc7OztZQXZEdkIsUUFBUSxTQUFDO2dCQUNOLFlBQVksRUFBRTtvQkFDVixjQUFjO29CQUNkLGVBQWU7b0JBQ2Ysa0JBQWtCO29CQUNsQixlQUFlO29CQUNmLGFBQWE7b0JBQ2IsZ0JBQWdCO29CQUNoQixnQkFBZ0I7b0JBQ2hCLGdCQUFnQjtvQkFDaEIsV0FBVztvQkFDWCxrQkFBa0I7b0JBQ2xCLG1CQUFtQjtvQkFDbkIsZ0JBQWdCO29CQUNoQixrQkFBa0I7b0JBQ2xCLG1CQUFtQjtvQkFDbkIsaUJBQWlCO29CQUNqQixjQUFjO29CQUNkLHFCQUFxQjtvQkFDckIsV0FBVztvQkFDWCxvQkFBb0I7b0JBQ3BCLFdBQVc7b0JBQ1gsV0FBVztvQkFDWCxrQkFBa0I7b0JBQ2xCLGdCQUFnQjtpQkFDbkI7Z0JBQ0QsT0FBTyxFQUFFO29CQUNMLGNBQWM7b0JBQ2QsZUFBZTtvQkFDZixrQkFBa0I7b0JBQ2xCLGVBQWU7b0JBQ2YsYUFBYTtvQkFDYixnQkFBZ0I7b0JBQ2hCLGdCQUFnQjtvQkFDaEIsZ0JBQWdCO29CQUNoQixXQUFXO29CQUNYLGtCQUFrQjtvQkFDbEIsbUJBQW1CO29CQUNuQixnQkFBZ0I7b0JBQ2hCLGtCQUFrQjtvQkFDbEIsbUJBQW1CO29CQUNuQixpQkFBaUI7b0JBQ2pCLGNBQWM7b0JBQ2QscUJBQXFCO29CQUNyQixXQUFXO29CQUNYLG9CQUFvQjtvQkFDcEIsV0FBVztvQkFDWCxXQUFXO29CQUNYLGtCQUFrQjtvQkFDbEIsZ0JBQWdCO2lCQUNuQjtnQkFDRCxPQUFPLEVBQUU7b0JBQ0wsWUFBWTtpQkFDZjthQUNKIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IENvbW1vbk1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XG5cbmltcG9ydCB7IENhcGl0YWxpemVQaXBlIH0gZnJvbSAnLi9jYXBpdGFsaXplLnBpcGUnO1xuaW1wb3J0IHsgRWxsaXBzaXNpemVQaXBlIH0gZnJvbSAnLi9lbGxpcHNpc2l6ZS5waXBlJztcbmltcG9ydCB7IFdlbGxib3JlU3RhdHVzUGlwZSB9IGZyb20gJy4vd2VsbGJvcmUtc3RhdHVzLnBpcGUnO1xuaW1wb3J0IHsgR3Jhdml0eVVuaXRQaXBlIH0gZnJvbSAnLi9ncmF2aXR5LXVuaXQucGlwZSc7XG5pbXBvcnQgeyBBbmdsZVVuaXRQaXBlIH0gZnJvbSAnLi9hbmdsZS11bml0LnBpcGUnO1xuaW1wb3J0IHsgTWFnbmV0aWNVbml0UGlwZSB9IGZyb20gJy4vbWFnbmV0aWMtdW5pdC5waXBlJztcbmltcG9ydCB7IERpc3RhbmNlVW5pdFBpcGUgfSBmcm9tICcuL2Rpc3RhbmNlLXVuaXQucGlwZSc7XG5pbXBvcnQgeyBCb29sZWFuT25PZmZQaXBlIH0gZnJvbSAnLi9ib29sZWFuLW9uLW9mZi5waXBlJztcbmltcG9ydCB7IFRpbWVBZ29QaXBlIH0gZnJvbSAnLi90aW1lLWFnby5waXBlJztcbmltcG9ydCB7IFN1cnZleVNldFN0YXRlUGlwZSB9IGZyb20gJy4vc3VydmV5LXNldC1zdGF0ZS5waXBlJztcbmltcG9ydCB7IERpc3RhbmNlRGVjaW1hbFBpcGUgfSBmcm9tICcuL2Rpc3RhbmNlLWRlY2ltYWwucGlwZSc7XG5pbXBvcnQgeyBBbmdsZURlY2ltYWxQaXBlIH0gZnJvbSAnLi9hbmdsZS1kZWNpbWFsLnBpcGUnO1xuaW1wb3J0IHsgR3Jhdml0eURlY2ltYWxQaXBlIH0gZnJvbSAnLi9ncmF2aXR5LWRlY2ltYWwucGlwZSc7XG5pbXBvcnQgeyBNYWduZXRpY0RlY2ltYWxQaXBlIH0gZnJvbSAnLi9tYWduZXRpYy1kZWNpbWFsLnBpcGUnO1xuaW1wb3J0IHsgUmVzb2x2ZWRTdGF0ZVBpcGUgfSBmcm9tICcuL3Jlc29sdmVkLXN0YXRlLnBpcGUnO1xuaW1wb3J0IHsgU3VydmV5VHlwZVBpcGUgfSBmcm9tICcuL3N1cnZleS10eXBlLnBpcGUnO1xuaW1wb3J0IHsgRGVmaW5pdGl2ZVN1cnZleXNQaXBlIH0gZnJvbSAnLi9kZWZpbml0aXZlLXN1cnZleXMucGlwZSc7XG5pbXBvcnQgeyBNZWFzdXJlUGlwZSB9IGZyb20gJy4vbWVhc3VyZS5waXBlJztcbmltcG9ydCB7IE9yZGVyU3VydmV5c0J5TWRQaXBlIH0gZnJvbSAnLi9vcmRlci1zdXJ2ZXlzLWJ5LW1kLnBpcGUnO1xuaW1wb3J0IHsgT3JkZXJCeVBpcGUgfSBmcm9tICcuL29yZGVyLWJ5LnBpcGUnO1xuaW1wb3J0IHsgSW5zdGFudFBpcGUgfSBmcm9tICcuL2luc3RhbnQucGlwZSc7XG5pbXBvcnQgeyBUcmFqZWN0b3J5VHlwZVBpcGUgfSBmcm9tICcuL3RyYWplY3RvcnktdHlwZS5waXBlJztcbmltcG9ydCB7IEZvcm1hdE9iamVjdFBpcGUgfSBmcm9tICcuL2Zvcm1hdC1vYmplY3QucGlwZSc7XG5cblxuQE5nTW9kdWxlKHtcbiAgICBkZWNsYXJhdGlvbnM6IFtcbiAgICAgICAgQ2FwaXRhbGl6ZVBpcGUsXG4gICAgICAgIEVsbGlwc2lzaXplUGlwZSxcbiAgICAgICAgV2VsbGJvcmVTdGF0dXNQaXBlLFxuICAgICAgICBHcmF2aXR5VW5pdFBpcGUsXG4gICAgICAgIEFuZ2xlVW5pdFBpcGUsXG4gICAgICAgIE1hZ25ldGljVW5pdFBpcGUsXG4gICAgICAgIERpc3RhbmNlVW5pdFBpcGUsXG4gICAgICAgIEJvb2xlYW5Pbk9mZlBpcGUsXG4gICAgICAgIFRpbWVBZ29QaXBlLFxuICAgICAgICBTdXJ2ZXlTZXRTdGF0ZVBpcGUsXG4gICAgICAgIERpc3RhbmNlRGVjaW1hbFBpcGUsXG4gICAgICAgIEFuZ2xlRGVjaW1hbFBpcGUsXG4gICAgICAgIEdyYXZpdHlEZWNpbWFsUGlwZSxcbiAgICAgICAgTWFnbmV0aWNEZWNpbWFsUGlwZSxcbiAgICAgICAgUmVzb2x2ZWRTdGF0ZVBpcGUsXG4gICAgICAgIFN1cnZleVR5cGVQaXBlLFxuICAgICAgICBEZWZpbml0aXZlU3VydmV5c1BpcGUsXG4gICAgICAgIE1lYXN1cmVQaXBlLFxuICAgICAgICBPcmRlclN1cnZleXNCeU1kUGlwZSxcbiAgICAgICAgT3JkZXJCeVBpcGUsXG4gICAgICAgIEluc3RhbnRQaXBlLFxuICAgICAgICBUcmFqZWN0b3J5VHlwZVBpcGUsXG4gICAgICAgIEZvcm1hdE9iamVjdFBpcGVcbiAgICBdLFxuICAgIGV4cG9ydHM6IFtcbiAgICAgICAgQ2FwaXRhbGl6ZVBpcGUsXG4gICAgICAgIEVsbGlwc2lzaXplUGlwZSxcbiAgICAgICAgV2VsbGJvcmVTdGF0dXNQaXBlLFxuICAgICAgICBHcmF2aXR5VW5pdFBpcGUsXG4gICAgICAgIEFuZ2xlVW5pdFBpcGUsXG4gICAgICAgIE1hZ25ldGljVW5pdFBpcGUsXG4gICAgICAgIERpc3RhbmNlVW5pdFBpcGUsXG4gICAgICAgIEJvb2xlYW5Pbk9mZlBpcGUsXG4gICAgICAgIFRpbWVBZ29QaXBlLFxuICAgICAgICBTdXJ2ZXlTZXRTdGF0ZVBpcGUsXG4gICAgICAgIERpc3RhbmNlRGVjaW1hbFBpcGUsXG4gICAgICAgIEFuZ2xlRGVjaW1hbFBpcGUsXG4gICAgICAgIEdyYXZpdHlEZWNpbWFsUGlwZSxcbiAgICAgICAgTWFnbmV0aWNEZWNpbWFsUGlwZSxcbiAgICAgICAgUmVzb2x2ZWRTdGF0ZVBpcGUsXG4gICAgICAgIFN1cnZleVR5cGVQaXBlLFxuICAgICAgICBEZWZpbml0aXZlU3VydmV5c1BpcGUsXG4gICAgICAgIE1lYXN1cmVQaXBlLFxuICAgICAgICBPcmRlclN1cnZleXNCeU1kUGlwZSxcbiAgICAgICAgT3JkZXJCeVBpcGUsXG4gICAgICAgIEluc3RhbnRQaXBlLFxuICAgICAgICBUcmFqZWN0b3J5VHlwZVBpcGUsXG4gICAgICAgIEZvcm1hdE9iamVjdFBpcGVcbiAgICBdLFxuICAgIGltcG9ydHM6IFtcbiAgICAgICAgQ29tbW9uTW9kdWxlXG4gICAgXVxufSlcbmV4cG9ydCBjbGFzcyBQaXBlc01vZHVsZSB7XG59XG4iXX0=