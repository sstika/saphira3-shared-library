/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Pipe } from '@angular/core';
export class AngleUnitPipe {
    /**
     * @param {?} unit
     * @param {?=} full
     * @return {?}
     */
    transform(unit, full) {
        /** @type {?} */
        let string = '';
        switch (unit) {
            case 'DEGREE':
                string = (full === true ? 'Degrees' : decodeURI('%C2%B0'));
                break;
            case 'RADIAN':
                string = (full === true ? 'Radians' : 'rad');
                break;
            case 'GRADIAN':
                string = (full === true ? 'Gradians' : 'grad');
                break;
            default:
                string = unit;
                break;
        }
        return string;
    }
}
AngleUnitPipe.decorators = [
    { type: Pipe, args: [{
                name: 'angleUnit'
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYW5nbGUtdW5pdC5waXBlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGhwdGVjaC9zYXBoaXJhLXNoYXJlZC1saWIvIiwic291cmNlcyI6WyJsaWIvcGlwZXMvYW5nbGUtdW5pdC5waXBlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsSUFBSSxFQUFpQixNQUFNLGVBQWUsQ0FBQztBQUtwRCxNQUFNLE9BQU8sYUFBYTs7Ozs7O0lBRXhCLFNBQVMsQ0FBQyxJQUFTLEVBQUUsSUFBVTs7WUFDekIsTUFBTSxHQUFHLEVBQUU7UUFDZixRQUFRLElBQUksRUFBRTtZQUNaLEtBQUssUUFBUTtnQkFDWCxNQUFNLEdBQUcsQ0FBQyxJQUFJLEtBQUssSUFBSSxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO2dCQUMzRCxNQUFNO1lBQ1IsS0FBSyxRQUFRO2dCQUNYLE1BQU0sR0FBRyxDQUFDLElBQUksS0FBSyxJQUFJLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQzdDLE1BQU07WUFDUixLQUFLLFNBQVM7Z0JBQ1osTUFBTSxHQUFHLENBQUMsSUFBSSxLQUFLLElBQUksQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQztnQkFDL0MsTUFBTTtZQUNSO2dCQUNFLE1BQU0sR0FBRyxJQUFJLENBQUM7Z0JBQ2QsTUFBTTtTQUNUO1FBQ0QsT0FBTyxNQUFNLENBQUM7SUFDaEIsQ0FBQzs7O1lBdEJGLElBQUksU0FBQztnQkFDSixJQUFJLEVBQUUsV0FBVzthQUNsQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFBpcGUsIFBpcGVUcmFuc2Zvcm0gfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQFBpcGUoe1xuICBuYW1lOiAnYW5nbGVVbml0J1xufSlcbmV4cG9ydCBjbGFzcyBBbmdsZVVuaXRQaXBlIGltcGxlbWVudHMgUGlwZVRyYW5zZm9ybSB7XG5cbiAgdHJhbnNmb3JtKHVuaXQ6IGFueSwgZnVsbD86IGFueSk6IGFueSB7XG4gICAgbGV0IHN0cmluZyA9ICcnO1xuICAgIHN3aXRjaCAodW5pdCkge1xuICAgICAgY2FzZSAnREVHUkVFJzpcbiAgICAgICAgc3RyaW5nID0gKGZ1bGwgPT09IHRydWUgPyAnRGVncmVlcycgOiBkZWNvZGVVUkkoJyVDMiVCMCcpKTtcbiAgICAgICAgYnJlYWs7XG4gICAgICBjYXNlICdSQURJQU4nOlxuICAgICAgICBzdHJpbmcgPSAoZnVsbCA9PT0gdHJ1ZSA/ICdSYWRpYW5zJyA6ICdyYWQnKTtcbiAgICAgICAgYnJlYWs7XG4gICAgICBjYXNlICdHUkFESUFOJzpcbiAgICAgICAgc3RyaW5nID0gKGZ1bGwgPT09IHRydWUgPyAnR3JhZGlhbnMnIDogJ2dyYWQnKTtcbiAgICAgICAgYnJlYWs7XG4gICAgICBkZWZhdWx0OlxuICAgICAgICBzdHJpbmcgPSB1bml0O1xuICAgICAgICBicmVhaztcbiAgICB9XG4gICAgcmV0dXJuIHN0cmluZztcbiAgfVxufVxuIl19