/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Pipe } from '@angular/core';
// A simple filter that converts 'true' to 'On' and 'false' to 'Off'.
export class BooleanOnOffPipe {
    /**
     * @param {?} value
     * @return {?}
     */
    transform(value) {
        if (value) {
            return 'On';
        }
        else {
            return 'Off';
        }
    }
}
BooleanOnOffPipe.decorators = [
    { type: Pipe, args: [{
                name: 'booleanOnOff'
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYm9vbGVhbi1vbi1vZmYucGlwZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BocHRlY2gvc2FwaGlyYS1zaGFyZWQtbGliLyIsInNvdXJjZXMiOlsibGliL3BpcGVzL2Jvb2xlYW4tb24tb2ZmLnBpcGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxJQUFJLEVBQWlCLE1BQU0sZUFBZSxDQUFDO0FBS3BELHFFQUFxRTtBQUNyRSxNQUFNLE9BQU8sZ0JBQWdCOzs7OztJQUUzQixTQUFTLENBQUMsS0FBVTtRQUNsQixJQUFJLEtBQUssRUFBRTtZQUNULE9BQU8sSUFBSSxDQUFDO1NBQ2I7YUFBTTtZQUNMLE9BQU8sS0FBSyxDQUFDO1NBQ2Q7SUFDSCxDQUFDOzs7WUFaRixJQUFJLFNBQUM7Z0JBQ0osSUFBSSxFQUFFLGNBQWM7YUFDckIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBQaXBlLCBQaXBlVHJhbnNmb3JtIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBQaXBlKHtcbiAgbmFtZTogJ2Jvb2xlYW5Pbk9mZidcbn0pXG4vLyBBIHNpbXBsZSBmaWx0ZXIgdGhhdCBjb252ZXJ0cyAndHJ1ZScgdG8gJ09uJyBhbmQgJ2ZhbHNlJyB0byAnT2ZmJy5cbmV4cG9ydCBjbGFzcyBCb29sZWFuT25PZmZQaXBlIGltcGxlbWVudHMgUGlwZVRyYW5zZm9ybSB7XG5cbiAgdHJhbnNmb3JtKHZhbHVlOiBhbnkpOiBhbnkge1xuICAgIGlmICh2YWx1ZSkge1xuICAgICAgcmV0dXJuICdPbic7XG4gICAgfSBlbHNlIHtcbiAgICAgIHJldHVybiAnT2ZmJztcbiAgICB9XG4gIH1cbn1cbiJdfQ==