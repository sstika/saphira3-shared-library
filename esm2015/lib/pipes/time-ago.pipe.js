/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Pipe } from '@angular/core';
import * as moment_ from 'moment';
// A filter to convert a moment to a human readable string
// of the format "X seconds ago", "Y minutes ago", "4 months ago"
export class TimeAgoPipe {
    /**
     * @param {?} time
     * @return {?}
     */
    transform(time) {
        /** @type {?} */
        const moment = moment_;
        /** @type {?} */
        const now = moment();
        if (time && moment.isMoment(time)) {
            if (time > now) {
                return 'Now';
            }
            else {
                return time.fromNow();
            }
        }
        else {
            return 'Never';
        }
    }
}
TimeAgoPipe.decorators = [
    { type: Pipe, args: [{
                name: 'timeAgo'
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGltZS1hZ28ucGlwZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BocHRlY2gvc2FwaGlyYS1zaGFyZWQtbGliLyIsInNvdXJjZXMiOlsibGliL3BpcGVzL3RpbWUtYWdvLnBpcGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxJQUFJLEVBQWlCLE1BQU0sZUFBZSxDQUFDO0FBQ3BELE9BQU8sS0FBSyxPQUFPLE1BQU0sUUFBUSxDQUFDO0FBS2xDLDBEQUEwRDtBQUMxRCxpRUFBaUU7QUFDakUsTUFBTSxPQUFPLFdBQVc7Ozs7O0lBRXRCLFNBQVMsQ0FBQyxJQUFJOztjQUNOLE1BQU0sR0FBRyxPQUFPOztjQUNoQixHQUFHLEdBQUcsTUFBTSxFQUFFO1FBQ3BCLElBQUksSUFBSSxJQUFJLE1BQU0sQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEVBQUU7WUFDakMsSUFBSSxJQUFJLEdBQUcsR0FBRyxFQUFFO2dCQUNkLE9BQU8sS0FBSyxDQUFDO2FBQ2Q7aUJBQU07Z0JBQ0wsT0FBTyxJQUFJLENBQUMsT0FBTyxFQUFFLENBQUM7YUFDdkI7U0FDRjthQUFNO1lBQ0wsT0FBTyxPQUFPLENBQUM7U0FDaEI7SUFDSCxDQUFDOzs7WUFuQkYsSUFBSSxTQUFDO2dCQUNKLElBQUksRUFBRSxTQUFTO2FBQ2hCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgUGlwZSwgUGlwZVRyYW5zZm9ybSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0ICogYXMgbW9tZW50XyBmcm9tICdtb21lbnQnO1xuXG5AUGlwZSh7XG4gIG5hbWU6ICd0aW1lQWdvJ1xufSlcbi8vIEEgZmlsdGVyIHRvIGNvbnZlcnQgYSBtb21lbnQgdG8gYSBodW1hbiByZWFkYWJsZSBzdHJpbmdcbi8vIG9mIHRoZSBmb3JtYXQgXCJYIHNlY29uZHMgYWdvXCIsIFwiWSBtaW51dGVzIGFnb1wiLCBcIjQgbW9udGhzIGFnb1wiXG5leHBvcnQgY2xhc3MgVGltZUFnb1BpcGUgaW1wbGVtZW50cyBQaXBlVHJhbnNmb3JtIHtcblxuICB0cmFuc2Zvcm0odGltZSk6IGFueSB7XG4gICAgY29uc3QgbW9tZW50ID0gbW9tZW50XztcbiAgICBjb25zdCBub3cgPSBtb21lbnQoKTtcbiAgICBpZiAodGltZSAmJiBtb21lbnQuaXNNb21lbnQodGltZSkpIHtcbiAgICAgIGlmICh0aW1lID4gbm93KSB7XG4gICAgICAgIHJldHVybiAnTm93JztcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHJldHVybiB0aW1lLmZyb21Ob3coKTtcbiAgICAgIH1cbiAgICB9IGVsc2Uge1xuICAgICAgcmV0dXJuICdOZXZlcic7XG4gICAgfVxuICB9XG5cbn1cbiJdfQ==