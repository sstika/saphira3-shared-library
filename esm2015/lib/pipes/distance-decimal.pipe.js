/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Pipe } from '@angular/core';
// Returns the distance as a string with an amount of decimal places based on its unit.
// Also formats the string with commas for readability.
export class DistanceDecimalPipe {
    constructor() {
        // number of decimal places to display for various distance units.
        this.decimalMap = {
            // Old Format
            METER: 2,
            KILOMETER: 1,
            FOOT: 2,
            FOOT_US: 2,
            YARD: 2,
            MILE: 1,
            // New Format
            Meters: 2,
            Kilometers: 1,
            Feet: 2,
            'ft US': 2,
            'U.S. Survey Feet': 2,
            Yards: 2,
            Miles: 1,
        };
    }
    /**
     * @param {?} value
     * @param {?=} unit
     * @return {?}
     */
    transform(value, unit) {
        if (!this.isValidFloat(value)) {
            return 'N/A';
        }
        else {
            value = parseFloat(value);
            if (typeof (this.decimalMap[unit]) !== 'undefined') {
                return this.formatNumber(value.toFixed(this.decimalMap[unit]));
            }
            else {
                return 'Unsupported Unit: ' + unit;
            }
        }
    }
    /**
     * isValidFloat:
     *
     * A stricter float filter than parseFloat().
     *
     * @param {?} value Input value to be parsed as float
     * @return {?} float
     */
    isValidFloat(value) {
        return !isNaN(parseFloat(value));
    }
    /**
     * formatNumber:
     *
     * Adds commas and removes the negative sign for zero values.
     *
     * @param {?} number Input value to be formatted
     * @return {?}
     */
    formatNumber(number) {
        return this.removeSignOnZero(this.addCommas(number));
    }
    /**
     * removeSignOnZero:
     *
     * If the value is zero, remove any negative signs. (converts -0.00 to 0.00)
     *
     * @param {?} number Input value to be sanitized
     * @return {?}
     */
    removeSignOnZero(number) {
        /** @type {?} */
        const split = number.toString().split('-');
        if (split.length === 2) {
            if (split[1] === 0) {
                return split[1];
            }
        }
        return number;
    }
    /**
     * addCommas:
     *
     * Helper function that adds in commas to a number for readability.
     *
     * @param {?} number Input value to add commas
     * @return {?} string
     */
    addCommas(number) {
        // check if we have a decimal
        /** @type {?} */
        const split = number.toString().split('.');
        // if we have a decimal
        if (split.length === 2) {
            // only add commas to the first half and return the second half
            return split[0].replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,') + '.' + split[1];
        }
        else {
            // add commas throughout
            return split[0].replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
        }
    }
}
DistanceDecimalPipe.decorators = [
    { type: Pipe, args: [{
                name: 'distanceDecimal'
            },] }
];
if (false) {
    /** @type {?} */
    DistanceDecimalPipe.prototype.decimalMap;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGlzdGFuY2UtZGVjaW1hbC5waXBlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGhwdGVjaC9zYXBoaXJhLXNoYXJlZC1saWIvIiwic291cmNlcyI6WyJsaWIvcGlwZXMvZGlzdGFuY2UtZGVjaW1hbC5waXBlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsSUFBSSxFQUFpQixNQUFNLGVBQWUsQ0FBQztBQUtwRCx1RkFBdUY7QUFDdkYsdURBQXVEO0FBQ3ZELE1BQU0sT0FBTyxtQkFBbUI7SUFMaEM7O1FBT0UsZUFBVSxHQUFHOztZQUVYLEtBQUssRUFBRSxDQUFDO1lBQ1IsU0FBUyxFQUFFLENBQUM7WUFDWixJQUFJLEVBQUUsQ0FBQztZQUNQLE9BQU8sRUFBRSxDQUFDO1lBQ1YsSUFBSSxFQUFFLENBQUM7WUFDUCxJQUFJLEVBQUUsQ0FBQzs7WUFFUCxNQUFNLEVBQUUsQ0FBQztZQUNULFVBQVUsRUFBRSxDQUFDO1lBQ2IsSUFBSSxFQUFFLENBQUM7WUFDUCxPQUFPLEVBQUUsQ0FBQztZQUNWLGtCQUFrQixFQUFFLENBQUM7WUFDckIsS0FBSyxFQUFFLENBQUM7WUFDUixLQUFLLEVBQUUsQ0FBQztTQUNULENBQUM7SUEyRUosQ0FBQzs7Ozs7O0lBekVDLFNBQVMsQ0FBQyxLQUFVLEVBQUUsSUFBVTtRQUM5QixJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxLQUFLLENBQUMsRUFBRTtZQUM3QixPQUFPLEtBQUssQ0FBQztTQUNkO2FBQU07WUFDTCxLQUFLLEdBQUcsVUFBVSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQzFCLElBQUksT0FBTSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLENBQUMsS0FBSyxXQUFXLEVBQUU7Z0JBQ2pELE9BQU8sSUFBSSxDQUFDLFlBQVksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO2FBQ2hFO2lCQUFNO2dCQUNMLE9BQU8sb0JBQW9CLEdBQUcsSUFBSSxDQUFDO2FBQ3BDO1NBQ0Y7SUFDSCxDQUFDOzs7Ozs7Ozs7SUFVTSxZQUFZLENBQUMsS0FBSztRQUN2QixPQUFPLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO0lBQ25DLENBQUM7Ozs7Ozs7OztJQVNNLFlBQVksQ0FBQyxNQUFNO1FBQ3hCLE9BQU8sSUFBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztJQUN2RCxDQUFDOzs7Ozs7Ozs7SUFTTSxnQkFBZ0IsQ0FBQyxNQUFNOztjQUN0QixLQUFLLEdBQUcsTUFBTSxDQUFDLFFBQVEsRUFBRSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUM7UUFDMUMsSUFBSSxLQUFLLENBQUMsTUFBTSxLQUFLLENBQUMsRUFBRTtZQUN0QixJQUFJLEtBQUssQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLEVBQUU7Z0JBQ2xCLE9BQU8sS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO2FBQ2pCO1NBQ0Y7UUFDRCxPQUFPLE1BQU0sQ0FBQztJQUNoQixDQUFDOzs7Ozs7Ozs7SUFVTSxTQUFTLENBQUMsTUFBTTs7O2NBRWYsS0FBSyxHQUFHLE1BQU0sQ0FBQyxRQUFRLEVBQUUsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDO1FBQzFDLHVCQUF1QjtRQUN2QixJQUFJLEtBQUssQ0FBQyxNQUFNLEtBQUssQ0FBQyxFQUFFO1lBQ3RCLCtEQUErRDtZQUMvRCxPQUFPLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMseUJBQXlCLEVBQUUsS0FBSyxDQUFDLEdBQUcsR0FBRyxHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztTQUM1RTthQUFNO1lBQ0wsd0JBQXdCO1lBQ3hCLE9BQU8sS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyx5QkFBeUIsRUFBRSxLQUFLLENBQUMsQ0FBQztTQUMzRDtJQUNILENBQUM7OztZQWpHRixJQUFJLFNBQUM7Z0JBQ0osSUFBSSxFQUFFLGlCQUFpQjthQUN4Qjs7OztJQUtDLHlDQWdCRSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFBpcGUsIFBpcGVUcmFuc2Zvcm0gfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQFBpcGUoe1xuICBuYW1lOiAnZGlzdGFuY2VEZWNpbWFsJ1xufSlcbi8vIFJldHVybnMgdGhlIGRpc3RhbmNlIGFzIGEgc3RyaW5nIHdpdGggYW4gYW1vdW50IG9mIGRlY2ltYWwgcGxhY2VzIGJhc2VkIG9uIGl0cyB1bml0LlxuLy8gQWxzbyBmb3JtYXRzIHRoZSBzdHJpbmcgd2l0aCBjb21tYXMgZm9yIHJlYWRhYmlsaXR5LlxuZXhwb3J0IGNsYXNzIERpc3RhbmNlRGVjaW1hbFBpcGUgaW1wbGVtZW50cyBQaXBlVHJhbnNmb3JtIHtcbiAgLy8gbnVtYmVyIG9mIGRlY2ltYWwgcGxhY2VzIHRvIGRpc3BsYXkgZm9yIHZhcmlvdXMgZGlzdGFuY2UgdW5pdHMuXG4gIGRlY2ltYWxNYXAgPSB7XG4gICAgLy8gT2xkIEZvcm1hdFxuICAgIE1FVEVSOiAyLFxuICAgIEtJTE9NRVRFUjogMSxcbiAgICBGT09UOiAyLFxuICAgIEZPT1RfVVM6IDIsXG4gICAgWUFSRDogMixcbiAgICBNSUxFOiAxLFxuICAgIC8vIE5ldyBGb3JtYXRcbiAgICBNZXRlcnM6IDIsXG4gICAgS2lsb21ldGVyczogMSxcbiAgICBGZWV0OiAyLFxuICAgICdmdCBVUyc6IDIsXG4gICAgJ1UuUy4gU3VydmV5IEZlZXQnOiAyLFxuICAgIFlhcmRzOiAyLFxuICAgIE1pbGVzOiAxLFxuICB9O1xuXG4gIHRyYW5zZm9ybSh2YWx1ZTogYW55LCB1bml0PzogYW55KTogYW55IHtcbiAgICBpZiAoIXRoaXMuaXNWYWxpZEZsb2F0KHZhbHVlKSkge1xuICAgICAgcmV0dXJuICdOL0EnO1xuICAgIH0gZWxzZSB7XG4gICAgICB2YWx1ZSA9IHBhcnNlRmxvYXQodmFsdWUpO1xuICAgICAgaWYgKHR5cGVvZih0aGlzLmRlY2ltYWxNYXBbdW5pdF0pICE9PSAndW5kZWZpbmVkJykge1xuICAgICAgICByZXR1cm4gdGhpcy5mb3JtYXROdW1iZXIodmFsdWUudG9GaXhlZCh0aGlzLmRlY2ltYWxNYXBbdW5pdF0pKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHJldHVybiAnVW5zdXBwb3J0ZWQgVW5pdDogJyArIHVuaXQ7XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgLyoqXG4gICAqIGlzVmFsaWRGbG9hdDpcbiAgICpcbiAgICogQSBzdHJpY3RlciBmbG9hdCBmaWx0ZXIgdGhhbiBwYXJzZUZsb2F0KCkuXG4gICAqXG4gICAqIEBwYXJhbSB2YWx1ZSBJbnB1dCB2YWx1ZSB0byBiZSBwYXJzZWQgYXMgZmxvYXRcbiAgICogQHJldHVybnMgZmxvYXRcbiAgICovXG4gIHB1YmxpYyBpc1ZhbGlkRmxvYXQodmFsdWUpIHtcbiAgICByZXR1cm4gIWlzTmFOKHBhcnNlRmxvYXQodmFsdWUpKTtcbiAgfVxuXG4gIC8qKlxuICAgICogZm9ybWF0TnVtYmVyOlxuICAgICpcbiAgICAqIEFkZHMgY29tbWFzIGFuZCByZW1vdmVzIHRoZSBuZWdhdGl2ZSBzaWduIGZvciB6ZXJvIHZhbHVlcy5cbiAgICAqXG4gICAgKiBAcGFyYW0gbnVtYmVyIElucHV0IHZhbHVlIHRvIGJlIGZvcm1hdHRlZFxuICAgICovXG4gIHB1YmxpYyBmb3JtYXROdW1iZXIobnVtYmVyKSB7XG4gICAgcmV0dXJuIHRoaXMucmVtb3ZlU2lnbk9uWmVybyh0aGlzLmFkZENvbW1hcyhudW1iZXIpKTtcbiAgfVxuXG4gIC8qKlxuICAgICAqIHJlbW92ZVNpZ25Pblplcm86XG4gICAgICpcbiAgICAgKiBJZiB0aGUgdmFsdWUgaXMgemVybywgcmVtb3ZlIGFueSBuZWdhdGl2ZSBzaWducy4gKGNvbnZlcnRzIC0wLjAwIHRvIDAuMDApXG4gICAgICpcbiAgICAgKiBAcGFyYW0gbnVtYmVyIElucHV0IHZhbHVlIHRvIGJlIHNhbml0aXplZFxuICAgICAqL1xuICBwdWJsaWMgcmVtb3ZlU2lnbk9uWmVybyhudW1iZXIpIHtcbiAgICBjb25zdCBzcGxpdCA9IG51bWJlci50b1N0cmluZygpLnNwbGl0KCctJyk7XG4gICAgaWYgKHNwbGl0Lmxlbmd0aCA9PT0gMikge1xuICAgICAgaWYgKHNwbGl0WzFdID09PSAwKSB7XG4gICAgICAgIHJldHVybiBzcGxpdFsxXTtcbiAgICAgIH1cbiAgICB9XG4gICAgcmV0dXJuIG51bWJlcjtcbiAgfVxuXG4gIC8qKlxuICAgICAqIGFkZENvbW1hczpcbiAgICAgKlxuICAgICAqIEhlbHBlciBmdW5jdGlvbiB0aGF0IGFkZHMgaW4gY29tbWFzIHRvIGEgbnVtYmVyIGZvciByZWFkYWJpbGl0eS5cbiAgICAgKlxuICAgICAqIEBwYXJhbSBudW1iZXIgSW5wdXQgdmFsdWUgdG8gYWRkIGNvbW1hc1xuICAgICAqIEByZXR1cm5zIHN0cmluZ1xuICAgICAqL1xuICBwdWJsaWMgYWRkQ29tbWFzKG51bWJlcikge1xuICAgIC8vIGNoZWNrIGlmIHdlIGhhdmUgYSBkZWNpbWFsXG4gICAgY29uc3Qgc3BsaXQgPSBudW1iZXIudG9TdHJpbmcoKS5zcGxpdCgnLicpO1xuICAgIC8vIGlmIHdlIGhhdmUgYSBkZWNpbWFsXG4gICAgaWYgKHNwbGl0Lmxlbmd0aCA9PT0gMikge1xuICAgICAgLy8gb25seSBhZGQgY29tbWFzIHRvIHRoZSBmaXJzdCBoYWxmIGFuZCByZXR1cm4gdGhlIHNlY29uZCBoYWxmXG4gICAgICByZXR1cm4gc3BsaXRbMF0ucmVwbGFjZSgvKFxcZCkoPz0oXFxkezN9KSsoPyFcXGQpKS9nLCAnJDEsJykgKyAnLicgKyBzcGxpdFsxXTtcbiAgICB9IGVsc2Uge1xuICAgICAgLy8gYWRkIGNvbW1hcyB0aHJvdWdob3V0XG4gICAgICByZXR1cm4gc3BsaXRbMF0ucmVwbGFjZSgvKFxcZCkoPz0oXFxkezN9KSsoPyFcXGQpKS9nLCAnJDEsJyk7XG4gICAgfVxuICB9XG59XG4iXX0=