/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Pipe } from '@angular/core';
export class GravityUnitPipe {
    /**
     * @param {?} unit
     * @param {?=} full
     * @return {?}
     */
    transform(unit, full) {
        /** @type {?} */
        let string = '';
        switch (unit) {
            case 'M_PER_SEC_SQ':
                string = (full === true ? 'Meters per Second Sq.' : 'm/s' + decodeURI('%C2%B2'));
                break;
            case 'FT_PER_SEC_SQ':
                string = (full === true ? 'Feet per Second Sq.' : 'f/s' + decodeURI('%C2%B2'));
                break;
            case 'G':
                string = 'G';
                break;
            case 'MILLI_G':
                string = 'mG';
                break;
            case 'G_98':
                string = 'G (9.8)';
                break;
            case 'GAL':
                string = 'Gal';
                break;
            case 'MILLI_GAL':
                string = 'mGal';
                break;
            default:
                string = unit;
                break;
        }
        return string;
    }
}
GravityUnitPipe.decorators = [
    { type: Pipe, args: [{
                name: 'gravityUnit'
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ3Jhdml0eS11bml0LnBpcGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AaHB0ZWNoL3NhcGhpcmEtc2hhcmVkLWxpYi8iLCJzb3VyY2VzIjpbImxpYi9waXBlcy9ncmF2aXR5LXVuaXQucGlwZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLElBQUksRUFBaUIsTUFBTSxlQUFlLENBQUM7QUFLcEQsTUFBTSxPQUFPLGVBQWU7Ozs7OztJQUUxQixTQUFTLENBQUMsSUFBWSxFQUFFLElBQVU7O1lBQzVCLE1BQU0sR0FBRyxFQUFFO1FBQ2YsUUFBUSxJQUFJLEVBQUU7WUFDVixLQUFLLGNBQWM7Z0JBQ2YsTUFBTSxHQUFHLENBQUMsSUFBSSxLQUFLLElBQUksQ0FBQyxDQUFDLENBQUMsdUJBQXVCLENBQUMsQ0FBQyxDQUFDLEtBQUssR0FBRyxTQUFTLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztnQkFDakYsTUFBTTtZQUNWLEtBQUssZUFBZTtnQkFDaEIsTUFBTSxHQUFHLENBQUMsSUFBSSxLQUFLLElBQUksQ0FBQyxDQUFDLENBQUMscUJBQXFCLENBQUMsQ0FBQyxDQUFDLEtBQUssR0FBRyxTQUFTLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztnQkFDL0UsTUFBTTtZQUNWLEtBQUssR0FBRztnQkFDSixNQUFNLEdBQUcsR0FBRyxDQUFDO2dCQUNiLE1BQU07WUFDVixLQUFLLFNBQVM7Z0JBQ1YsTUFBTSxHQUFHLElBQUksQ0FBQztnQkFDZCxNQUFNO1lBQ1YsS0FBSyxNQUFNO2dCQUNQLE1BQU0sR0FBRyxTQUFTLENBQUM7Z0JBQ25CLE1BQU07WUFDVixLQUFLLEtBQUs7Z0JBQ04sTUFBTSxHQUFHLEtBQUssQ0FBQztnQkFDZixNQUFNO1lBQ1YsS0FBSyxXQUFXO2dCQUNaLE1BQU0sR0FBRyxNQUFNLENBQUM7Z0JBQ2hCLE1BQU07WUFDVjtnQkFDSSxNQUFNLEdBQUcsSUFBSSxDQUFDO2dCQUNkLE1BQU07U0FDYjtRQUNELE9BQU8sTUFBTSxDQUFDO0lBQ2hCLENBQUM7OztZQWxDRixJQUFJLFNBQUM7Z0JBQ0osSUFBSSxFQUFFLGFBQWE7YUFDcEIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBQaXBlLCBQaXBlVHJhbnNmb3JtIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBQaXBlKHtcbiAgbmFtZTogJ2dyYXZpdHlVbml0J1xufSlcbmV4cG9ydCBjbGFzcyBHcmF2aXR5VW5pdFBpcGUgaW1wbGVtZW50cyBQaXBlVHJhbnNmb3JtIHtcblxuICB0cmFuc2Zvcm0odW5pdDogc3RyaW5nLCBmdWxsPzogYW55KTogc3RyaW5nIHtcbiAgICBsZXQgc3RyaW5nID0gJyc7XG4gICAgc3dpdGNoICh1bml0KSB7XG4gICAgICAgIGNhc2UgJ01fUEVSX1NFQ19TUSc6XG4gICAgICAgICAgICBzdHJpbmcgPSAoZnVsbCA9PT0gdHJ1ZSA/ICdNZXRlcnMgcGVyIFNlY29uZCBTcS4nIDogJ20vcycgKyBkZWNvZGVVUkkoJyVDMiVCMicpKTtcbiAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICBjYXNlICdGVF9QRVJfU0VDX1NRJzpcbiAgICAgICAgICAgIHN0cmluZyA9IChmdWxsID09PSB0cnVlID8gJ0ZlZXQgcGVyIFNlY29uZCBTcS4nIDogJ2YvcycgKyBkZWNvZGVVUkkoJyVDMiVCMicpKTtcbiAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICBjYXNlICdHJzpcbiAgICAgICAgICAgIHN0cmluZyA9ICdHJztcbiAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICBjYXNlICdNSUxMSV9HJzpcbiAgICAgICAgICAgIHN0cmluZyA9ICdtRyc7XG4gICAgICAgICAgICBicmVhaztcbiAgICAgICAgY2FzZSAnR185OCc6XG4gICAgICAgICAgICBzdHJpbmcgPSAnRyAoOS44KSc7XG4gICAgICAgICAgICBicmVhaztcbiAgICAgICAgY2FzZSAnR0FMJzpcbiAgICAgICAgICAgIHN0cmluZyA9ICdHYWwnO1xuICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgIGNhc2UgJ01JTExJX0dBTCc6XG4gICAgICAgICAgICBzdHJpbmcgPSAnbUdhbCc7XG4gICAgICAgICAgICBicmVhaztcbiAgICAgICAgZGVmYXVsdDpcbiAgICAgICAgICAgIHN0cmluZyA9IHVuaXQ7XG4gICAgICAgICAgICBicmVhaztcbiAgICB9XG4gICAgcmV0dXJuIHN0cmluZztcbiAgfVxuXG59XG4iXX0=