/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Pipe } from '@angular/core';
export class EllipsisizePipe {
    /**
     * @param {?} value
     * @param {?} desiredLengthIncludingElip
     * @return {?}
     */
    transform(value, desiredLengthIncludingElip) {
        if (value && value.length > desiredLengthIncludingElip) {
            // TODO: need to make this clickable to show the whole string or something
            return value.substring(0, desiredLengthIncludingElip - 3) + '...';
        }
        return value;
    }
}
EllipsisizePipe.decorators = [
    { type: Pipe, args: [{ name: 'ellipsisize' },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZWxsaXBzaXNpemUucGlwZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BocHRlY2gvc2FwaGlyYS1zaGFyZWQtbGliLyIsInNvdXJjZXMiOlsibGliL3BpcGVzL2VsbGlwc2lzaXplLnBpcGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxJQUFJLEVBQWlCLE1BQU0sZUFBZSxDQUFDO0FBR3BELE1BQU0sT0FBTyxlQUFlOzs7Ozs7SUFDMUIsU0FBUyxDQUFDLEtBQVUsRUFBRSwwQkFBa0M7UUFDdEQsSUFBSSxLQUFLLElBQUksS0FBSyxDQUFDLE1BQU0sR0FBRywwQkFBMEIsRUFBRTtZQUN0RCwwRUFBMEU7WUFDMUUsT0FBTyxLQUFLLENBQUMsU0FBUyxDQUFDLENBQUMsRUFBRSwwQkFBMEIsR0FBRyxDQUFDLENBQUMsR0FBRyxLQUFLLENBQUM7U0FDbkU7UUFDRCxPQUFPLEtBQUssQ0FBQztJQUNmLENBQUM7OztZQVJGLElBQUksU0FBQyxFQUFDLElBQUksRUFBRSxhQUFhLEVBQUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBQaXBlLCBQaXBlVHJhbnNmb3JtIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBQaXBlKHtuYW1lOiAnZWxsaXBzaXNpemUnfSlcbmV4cG9ydCBjbGFzcyBFbGxpcHNpc2l6ZVBpcGUgaW1wbGVtZW50cyBQaXBlVHJhbnNmb3JtIHtcbiAgdHJhbnNmb3JtKHZhbHVlOiBhbnksIGRlc2lyZWRMZW5ndGhJbmNsdWRpbmdFbGlwOiBudW1iZXIpIHtcbiAgICBpZiAodmFsdWUgJiYgdmFsdWUubGVuZ3RoID4gZGVzaXJlZExlbmd0aEluY2x1ZGluZ0VsaXApIHtcbiAgICAgIC8vIFRPRE86IG5lZWQgdG8gbWFrZSB0aGlzIGNsaWNrYWJsZSB0byBzaG93IHRoZSB3aG9sZSBzdHJpbmcgb3Igc29tZXRoaW5nXG4gICAgICByZXR1cm4gdmFsdWUuc3Vic3RyaW5nKDAsIGRlc2lyZWRMZW5ndGhJbmNsdWRpbmdFbGlwIC0gMykgKyAnLi4uJztcbiAgICB9XG4gICAgcmV0dXJuIHZhbHVlO1xuICB9XG59XG4iXX0=