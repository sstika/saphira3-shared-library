/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Pipe } from '@angular/core';
export class TrajectoryTypePipe {
    /**
     * @param {?} value
     * @param {?=} args
     * @return {?}
     */
    transform(value, args) {
        switch (value) {
            case 'ACTUAL':
                return 'Actual';
            case 'DEFINITIVE':
                return 'Definitive';
            case 'PLAN':
                return 'Prototype';
            case 'PROTOTYPE':
                return 'Completed';
            case 'REPORTED':
                return 'Reported';
        }
    }
}
TrajectoryTypePipe.decorators = [
    { type: Pipe, args: [{
                name: 'trajectoryType'
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidHJhamVjdG9yeS10eXBlLnBpcGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AaHB0ZWNoL3NhcGhpcmEtc2hhcmVkLWxpYi8iLCJzb3VyY2VzIjpbImxpYi9waXBlcy90cmFqZWN0b3J5LXR5cGUucGlwZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLElBQUksRUFBaUIsTUFBTSxlQUFlLENBQUM7QUFLcEQsTUFBTSxPQUFPLGtCQUFrQjs7Ozs7O0lBRTdCLFNBQVMsQ0FBQyxLQUFVLEVBQUUsSUFBVTtRQUM5QixRQUFRLEtBQUssRUFBRTtZQUNiLEtBQUssUUFBUTtnQkFDWCxPQUFPLFFBQVEsQ0FBQztZQUNsQixLQUFLLFlBQVk7Z0JBQ2YsT0FBTyxZQUFZLENBQUM7WUFDdEIsS0FBSyxNQUFNO2dCQUNULE9BQU8sV0FBVyxDQUFDO1lBQ3JCLEtBQUssV0FBVztnQkFDZCxPQUFPLFdBQVcsQ0FBQztZQUNyQixLQUFLLFVBQVU7Z0JBQ2IsT0FBTyxVQUFVLENBQUM7U0FDckI7SUFDSCxDQUFDOzs7WUFsQkYsSUFBSSxTQUFDO2dCQUNKLElBQUksRUFBRSxnQkFBZ0I7YUFDdkIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBQaXBlLCBQaXBlVHJhbnNmb3JtIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBQaXBlKHtcbiAgbmFtZTogJ3RyYWplY3RvcnlUeXBlJ1xufSlcbmV4cG9ydCBjbGFzcyBUcmFqZWN0b3J5VHlwZVBpcGUgaW1wbGVtZW50cyBQaXBlVHJhbnNmb3JtIHtcblxuICB0cmFuc2Zvcm0odmFsdWU6IGFueSwgYXJncz86IGFueSk6IGFueSB7XG4gICAgc3dpdGNoICh2YWx1ZSkge1xuICAgICAgY2FzZSAnQUNUVUFMJzpcbiAgICAgICAgcmV0dXJuICdBY3R1YWwnO1xuICAgICAgY2FzZSAnREVGSU5JVElWRSc6XG4gICAgICAgIHJldHVybiAnRGVmaW5pdGl2ZSc7XG4gICAgICBjYXNlICdQTEFOJzpcbiAgICAgICAgcmV0dXJuICdQcm90b3R5cGUnO1xuICAgICAgY2FzZSAnUFJPVE9UWVBFJzpcbiAgICAgICAgcmV0dXJuICdDb21wbGV0ZWQnO1xuICAgICAgY2FzZSAnUkVQT1JURUQnOlxuICAgICAgICByZXR1cm4gJ1JlcG9ydGVkJztcbiAgICB9XG4gIH1cbn1cbiJdfQ==