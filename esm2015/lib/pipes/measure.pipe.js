/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Pipe } from '@angular/core';
import { DistanceDecimalPipe } from './distance-decimal.pipe';
import { DistanceUnitPipe } from './distance-unit.pipe';
import { AngleDecimalPipe } from './angle-decimal.pipe';
import { AngleUnitPipe } from './angle-unit.pipe';
import { GravityDecimalPipe } from './gravity-decimal.pipe';
import { GravityUnitPipe } from './gravity-unit.pipe';
import { MagneticDecimalPipe } from './magnetic-decimal.pipe';
import { MagneticUnitPipe } from './magnetic-unit.pipe';
// Returns a string representation of the measure with proper decimal places and formatting.
export class MeasurePipe {
    /**
     * @param {?} measure
     * @param {?=} displayUnit
     * @return {?}
     */
    transform(measure, displayUnit) {
        /** @type {?} */
        let value;
        /** @type {?} */
        let unit;
        // default: display units
        if (displayUnit === undefined) {
            displayUnit = true;
        }
        // Check if measure is undefined or unit is undefined
        if (measure === undefined || measure.unit === undefined) {
            return 'Unsupported Input for Measure Filter';
        }
        switch (measure.unit) {
            // Old Format
            case 'METER':
            case 'KILOMETER':
            case 'FOOT':
            case 'FOOT_US':
            case 'YARD':
            case 'MILE':
            // New Format
            case 'Meters':
            case 'Kilometers':
            case 'Feet':
            case 'ft US':
            case 'U.S. Survey Feet':
            case 'Yards':
            case 'Miles':
                value = new DistanceDecimalPipe().transform(measure.value, measure.unit);
                unit = ' ' + new DistanceUnitPipe().transform(measure.unit);
                break;
            // Old Format
            case 'DEGREE':
            case 'RADIAN':
            case 'GRADIAN':
            // New Format
            case 'Degrees':
            case '°':
            case 'Radians':
            case 'rad':
            case 'Gradians':
            case 'grad':
                value = new AngleDecimalPipe().transform(measure.value, measure.unit);
                unit = ' ' + new AngleUnitPipe().transform(measure.unit);
                break;
            // Old Format
            case 'M_PER_SEC_SQ':
            case 'FT_PER_SEC_SQ':
            case 'G':
            case 'MILLI_G':
            case 'G_98':
            case 'GAL':
            case 'MILLI_GAL':
            // New Format
            case 'Meters per Second Sq.':
            case 'Feet per Second Sq.':
            case 'm/s²':
            case 'f/s²':
            case 'mG':
            case 'G (9.8)':
            case 'Gal':
            case 'mGal':
                value = new GravityDecimalPipe().transform(measure.value, measure.unit);
                unit = ' ' + new GravityUnitPipe().transform(measure.unit);
                break;
            // Old Format
            case 'NANOTESLA':
            case 'MICROTESLA':
            case 'MILLITESLA':
            case 'TESLA':
            case 'GAUSS':
            case 'GAMMA':
            // New Format
            case 'Nanotesla':
            case 'nT':
            case 'Microtesla':
            case 'µT':
            case 'Millitesla':
            case 'mT':
            case 'Tesla':
            case 'T':
            case 'Gauss':
            case 'Gs':
            case 'Gamma':
            case 'γ':
                value = new MagneticDecimalPipe().transform(measure.value, measure.unit);
                unit = ' ' + new MagneticUnitPipe().transform(measure.unit);
                break;
            default:
                return 'Unsupported Unit: ' + measure.unit;
        }
        // if we are displaying units
        if (displayUnit) {
            return value + unit;
        }
        else { // if we aren't displaying units, just give the value
            return value;
        }
    }
}
MeasurePipe.decorators = [
    { type: Pipe, args: [{
                name: 'measure'
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWVhc3VyZS5waXBlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGhwdGVjaC9zYXBoaXJhLXNoYXJlZC1saWIvIiwic291cmNlcyI6WyJsaWIvcGlwZXMvbWVhc3VyZS5waXBlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsSUFBSSxFQUFpQixNQUFNLGVBQWUsQ0FBQztBQUNwRCxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSx5QkFBeUIsQ0FBQztBQUM5RCxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUN4RCxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUN4RCxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sbUJBQW1CLENBQUM7QUFDbEQsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0sd0JBQXdCLENBQUM7QUFDNUQsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLHFCQUFxQixDQUFDO0FBQ3RELE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLHlCQUF5QixDQUFDO0FBQzlELE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBS3hELDRGQUE0RjtBQUM1RixNQUFNLE9BQU8sV0FBVzs7Ozs7O0lBRXRCLFNBQVMsQ0FBQyxPQUFZLEVBQUUsV0FBaUI7O1lBQ25DLEtBQUs7O1lBQ0wsSUFBSTtRQUVSLHlCQUF5QjtRQUN6QixJQUFJLFdBQVcsS0FBSyxTQUFTLEVBQUU7WUFDN0IsV0FBVyxHQUFHLElBQUksQ0FBQztTQUNwQjtRQUNELHFEQUFxRDtRQUNyRCxJQUFJLE9BQU8sS0FBSyxTQUFTLElBQUksT0FBTyxDQUFDLElBQUksS0FBSyxTQUFTLEVBQUU7WUFDdkQsT0FBTyxzQ0FBc0MsQ0FBQztTQUMvQztRQUNELFFBQVEsT0FBTyxDQUFDLElBQUksRUFBRTtZQUNwQixhQUFhO1lBQ2IsS0FBSyxPQUFPLENBQUM7WUFDYixLQUFLLFdBQVcsQ0FBQztZQUNqQixLQUFLLE1BQU0sQ0FBQztZQUNaLEtBQUssU0FBUyxDQUFDO1lBQ2YsS0FBSyxNQUFNLENBQUM7WUFDWixLQUFLLE1BQU0sQ0FBQztZQUNaLGFBQWE7WUFDYixLQUFLLFFBQVEsQ0FBQztZQUNkLEtBQUssWUFBWSxDQUFDO1lBQ2xCLEtBQUssTUFBTSxDQUFDO1lBQ1osS0FBSyxPQUFPLENBQUM7WUFDYixLQUFLLGtCQUFrQixDQUFDO1lBQ3hCLEtBQUssT0FBTyxDQUFDO1lBQ2IsS0FBSyxPQUFPO2dCQUNWLEtBQUssR0FBRyxJQUFJLG1CQUFtQixFQUFFLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxLQUFLLEVBQUUsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUN6RSxJQUFJLEdBQUcsR0FBRyxHQUFHLElBQUksZ0JBQWdCLEVBQUUsQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUM1RCxNQUFNO1lBQ1IsYUFBYTtZQUNiLEtBQUssUUFBUSxDQUFDO1lBQ2QsS0FBSyxRQUFRLENBQUM7WUFDZCxLQUFLLFNBQVMsQ0FBQztZQUNmLGFBQWE7WUFDYixLQUFLLFNBQVMsQ0FBQztZQUNmLEtBQUssR0FBRyxDQUFDO1lBQ1QsS0FBSyxTQUFTLENBQUM7WUFDZixLQUFLLEtBQUssQ0FBQztZQUNYLEtBQUssVUFBVSxDQUFDO1lBQ2hCLEtBQUssTUFBTTtnQkFDVCxLQUFLLEdBQUcsSUFBSSxnQkFBZ0IsRUFBRSxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsS0FBSyxFQUFFLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDdEUsSUFBSSxHQUFHLEdBQUcsR0FBRyxJQUFJLGFBQWEsRUFBRSxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ3pELE1BQU07WUFDUixhQUFhO1lBQ2IsS0FBSyxjQUFjLENBQUM7WUFDcEIsS0FBSyxlQUFlLENBQUM7WUFDckIsS0FBSyxHQUFHLENBQUM7WUFDVCxLQUFLLFNBQVMsQ0FBQztZQUNmLEtBQUssTUFBTSxDQUFDO1lBQ1osS0FBSyxLQUFLLENBQUM7WUFDWCxLQUFLLFdBQVcsQ0FBQztZQUNqQixhQUFhO1lBQ2IsS0FBSyx1QkFBdUIsQ0FBQztZQUM3QixLQUFLLHFCQUFxQixDQUFDO1lBQzNCLEtBQUssTUFBTSxDQUFDO1lBQ1osS0FBSyxNQUFNLENBQUM7WUFDWixLQUFLLElBQUksQ0FBQztZQUNWLEtBQUssU0FBUyxDQUFDO1lBQ2YsS0FBSyxLQUFLLENBQUM7WUFDWCxLQUFLLE1BQU07Z0JBQ1QsS0FBSyxHQUFHLElBQUksa0JBQWtCLEVBQUUsQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLEtBQUssRUFBRSxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ3hFLElBQUksR0FBRyxHQUFHLEdBQUcsSUFBSSxlQUFlLEVBQUUsQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUMzRCxNQUFNO1lBQ1IsYUFBYTtZQUNiLEtBQUssV0FBVyxDQUFDO1lBQ2pCLEtBQUssWUFBWSxDQUFDO1lBQ2xCLEtBQUssWUFBWSxDQUFDO1lBQ2xCLEtBQUssT0FBTyxDQUFDO1lBQ2IsS0FBSyxPQUFPLENBQUM7WUFDYixLQUFLLE9BQU8sQ0FBQztZQUNiLGFBQWE7WUFDYixLQUFLLFdBQVcsQ0FBQztZQUNqQixLQUFLLElBQUksQ0FBQztZQUNWLEtBQUssWUFBWSxDQUFDO1lBQ2xCLEtBQUssSUFBSSxDQUFDO1lBQ1YsS0FBSyxZQUFZLENBQUM7WUFDbEIsS0FBSyxJQUFJLENBQUM7WUFDVixLQUFLLE9BQU8sQ0FBQztZQUNiLEtBQUssR0FBRyxDQUFDO1lBQ1QsS0FBSyxPQUFPLENBQUM7WUFDYixLQUFLLElBQUksQ0FBQztZQUNWLEtBQUssT0FBTyxDQUFDO1lBQ2IsS0FBSyxHQUFHO2dCQUNOLEtBQUssR0FBRyxJQUFJLG1CQUFtQixFQUFFLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxLQUFLLEVBQUUsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUN6RSxJQUFJLEdBQUcsR0FBRyxHQUFHLElBQUksZ0JBQWdCLEVBQUUsQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUM1RCxNQUFNO1lBQ1I7Z0JBQ0UsT0FBTyxvQkFBb0IsR0FBRyxPQUFPLENBQUMsSUFBSSxDQUFDO1NBQzlDO1FBQ0QsNkJBQTZCO1FBQzdCLElBQUksV0FBVyxFQUFFO1lBQ2YsT0FBTyxLQUFLLEdBQUcsSUFBSSxDQUFDO1NBQ3JCO2FBQU0sRUFBRSxxREFBcUQ7WUFDNUQsT0FBTyxLQUFLLENBQUM7U0FDZDtJQUNILENBQUM7OztZQXZHRixJQUFJLFNBQUM7Z0JBQ0osSUFBSSxFQUFFLFNBQVM7YUFDaEIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBQaXBlLCBQaXBlVHJhbnNmb3JtIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBEaXN0YW5jZURlY2ltYWxQaXBlIH0gZnJvbSAnLi9kaXN0YW5jZS1kZWNpbWFsLnBpcGUnO1xuaW1wb3J0IHsgRGlzdGFuY2VVbml0UGlwZSB9IGZyb20gJy4vZGlzdGFuY2UtdW5pdC5waXBlJztcbmltcG9ydCB7IEFuZ2xlRGVjaW1hbFBpcGUgfSBmcm9tICcuL2FuZ2xlLWRlY2ltYWwucGlwZSc7XG5pbXBvcnQgeyBBbmdsZVVuaXRQaXBlIH0gZnJvbSAnLi9hbmdsZS11bml0LnBpcGUnO1xuaW1wb3J0IHsgR3Jhdml0eURlY2ltYWxQaXBlIH0gZnJvbSAnLi9ncmF2aXR5LWRlY2ltYWwucGlwZSc7XG5pbXBvcnQgeyBHcmF2aXR5VW5pdFBpcGUgfSBmcm9tICcuL2dyYXZpdHktdW5pdC5waXBlJztcbmltcG9ydCB7IE1hZ25ldGljRGVjaW1hbFBpcGUgfSBmcm9tICcuL21hZ25ldGljLWRlY2ltYWwucGlwZSc7XG5pbXBvcnQgeyBNYWduZXRpY1VuaXRQaXBlIH0gZnJvbSAnLi9tYWduZXRpYy11bml0LnBpcGUnO1xuXG5AUGlwZSh7XG4gIG5hbWU6ICdtZWFzdXJlJ1xufSlcbi8vIFJldHVybnMgYSBzdHJpbmcgcmVwcmVzZW50YXRpb24gb2YgdGhlIG1lYXN1cmUgd2l0aCBwcm9wZXIgZGVjaW1hbCBwbGFjZXMgYW5kIGZvcm1hdHRpbmcuXG5leHBvcnQgY2xhc3MgTWVhc3VyZVBpcGUgaW1wbGVtZW50cyBQaXBlVHJhbnNmb3JtIHtcblxuICB0cmFuc2Zvcm0obWVhc3VyZTogYW55LCBkaXNwbGF5VW5pdD86IGFueSk6IGFueSB7XG4gICAgbGV0IHZhbHVlO1xuICAgIGxldCB1bml0O1xuXG4gICAgLy8gZGVmYXVsdDogZGlzcGxheSB1bml0c1xuICAgIGlmIChkaXNwbGF5VW5pdCA9PT0gdW5kZWZpbmVkKSB7XG4gICAgICBkaXNwbGF5VW5pdCA9IHRydWU7XG4gICAgfVxuICAgIC8vIENoZWNrIGlmIG1lYXN1cmUgaXMgdW5kZWZpbmVkIG9yIHVuaXQgaXMgdW5kZWZpbmVkXG4gICAgaWYgKG1lYXN1cmUgPT09IHVuZGVmaW5lZCB8fCBtZWFzdXJlLnVuaXQgPT09IHVuZGVmaW5lZCkge1xuICAgICAgcmV0dXJuICdVbnN1cHBvcnRlZCBJbnB1dCBmb3IgTWVhc3VyZSBGaWx0ZXInO1xuICAgIH1cbiAgICBzd2l0Y2ggKG1lYXN1cmUudW5pdCkge1xuICAgICAgLy8gT2xkIEZvcm1hdFxuICAgICAgY2FzZSAnTUVURVInOlxuICAgICAgY2FzZSAnS0lMT01FVEVSJzpcbiAgICAgIGNhc2UgJ0ZPT1QnOlxuICAgICAgY2FzZSAnRk9PVF9VUyc6XG4gICAgICBjYXNlICdZQVJEJzpcbiAgICAgIGNhc2UgJ01JTEUnOlxuICAgICAgLy8gTmV3IEZvcm1hdFxuICAgICAgY2FzZSAnTWV0ZXJzJzpcbiAgICAgIGNhc2UgJ0tpbG9tZXRlcnMnOlxuICAgICAgY2FzZSAnRmVldCc6XG4gICAgICBjYXNlICdmdCBVUyc6XG4gICAgICBjYXNlICdVLlMuIFN1cnZleSBGZWV0JzpcbiAgICAgIGNhc2UgJ1lhcmRzJzpcbiAgICAgIGNhc2UgJ01pbGVzJzpcbiAgICAgICAgdmFsdWUgPSBuZXcgRGlzdGFuY2VEZWNpbWFsUGlwZSgpLnRyYW5zZm9ybShtZWFzdXJlLnZhbHVlLCBtZWFzdXJlLnVuaXQpO1xuICAgICAgICB1bml0ID0gJyAnICsgbmV3IERpc3RhbmNlVW5pdFBpcGUoKS50cmFuc2Zvcm0obWVhc3VyZS51bml0KTtcbiAgICAgICAgYnJlYWs7XG4gICAgICAvLyBPbGQgRm9ybWF0XG4gICAgICBjYXNlICdERUdSRUUnOlxuICAgICAgY2FzZSAnUkFESUFOJzpcbiAgICAgIGNhc2UgJ0dSQURJQU4nOlxuICAgICAgLy8gTmV3IEZvcm1hdFxuICAgICAgY2FzZSAnRGVncmVlcyc6XG4gICAgICBjYXNlICfCsCc6XG4gICAgICBjYXNlICdSYWRpYW5zJzpcbiAgICAgIGNhc2UgJ3JhZCc6XG4gICAgICBjYXNlICdHcmFkaWFucyc6XG4gICAgICBjYXNlICdncmFkJzpcbiAgICAgICAgdmFsdWUgPSBuZXcgQW5nbGVEZWNpbWFsUGlwZSgpLnRyYW5zZm9ybShtZWFzdXJlLnZhbHVlLCBtZWFzdXJlLnVuaXQpO1xuICAgICAgICB1bml0ID0gJyAnICsgbmV3IEFuZ2xlVW5pdFBpcGUoKS50cmFuc2Zvcm0obWVhc3VyZS51bml0KTtcbiAgICAgICAgYnJlYWs7XG4gICAgICAvLyBPbGQgRm9ybWF0XG4gICAgICBjYXNlICdNX1BFUl9TRUNfU1EnOlxuICAgICAgY2FzZSAnRlRfUEVSX1NFQ19TUSc6XG4gICAgICBjYXNlICdHJzpcbiAgICAgIGNhc2UgJ01JTExJX0cnOlxuICAgICAgY2FzZSAnR185OCc6XG4gICAgICBjYXNlICdHQUwnOlxuICAgICAgY2FzZSAnTUlMTElfR0FMJzpcbiAgICAgIC8vIE5ldyBGb3JtYXRcbiAgICAgIGNhc2UgJ01ldGVycyBwZXIgU2Vjb25kIFNxLic6XG4gICAgICBjYXNlICdGZWV0IHBlciBTZWNvbmQgU3EuJzpcbiAgICAgIGNhc2UgJ20vc8KyJzpcbiAgICAgIGNhc2UgJ2Yvc8KyJzpcbiAgICAgIGNhc2UgJ21HJzpcbiAgICAgIGNhc2UgJ0cgKDkuOCknOlxuICAgICAgY2FzZSAnR2FsJzpcbiAgICAgIGNhc2UgJ21HYWwnOlxuICAgICAgICB2YWx1ZSA9IG5ldyBHcmF2aXR5RGVjaW1hbFBpcGUoKS50cmFuc2Zvcm0obWVhc3VyZS52YWx1ZSwgbWVhc3VyZS51bml0KTtcbiAgICAgICAgdW5pdCA9ICcgJyArIG5ldyBHcmF2aXR5VW5pdFBpcGUoKS50cmFuc2Zvcm0obWVhc3VyZS51bml0KTtcbiAgICAgICAgYnJlYWs7XG4gICAgICAvLyBPbGQgRm9ybWF0XG4gICAgICBjYXNlICdOQU5PVEVTTEEnOlxuICAgICAgY2FzZSAnTUlDUk9URVNMQSc6XG4gICAgICBjYXNlICdNSUxMSVRFU0xBJzpcbiAgICAgIGNhc2UgJ1RFU0xBJzpcbiAgICAgIGNhc2UgJ0dBVVNTJzpcbiAgICAgIGNhc2UgJ0dBTU1BJzpcbiAgICAgIC8vIE5ldyBGb3JtYXRcbiAgICAgIGNhc2UgJ05hbm90ZXNsYSc6XG4gICAgICBjYXNlICduVCc6XG4gICAgICBjYXNlICdNaWNyb3Rlc2xhJzpcbiAgICAgIGNhc2UgJ8K1VCc6XG4gICAgICBjYXNlICdNaWxsaXRlc2xhJzpcbiAgICAgIGNhc2UgJ21UJzpcbiAgICAgIGNhc2UgJ1Rlc2xhJzpcbiAgICAgIGNhc2UgJ1QnOlxuICAgICAgY2FzZSAnR2F1c3MnOlxuICAgICAgY2FzZSAnR3MnOlxuICAgICAgY2FzZSAnR2FtbWEnOlxuICAgICAgY2FzZSAnzrMnOlxuICAgICAgICB2YWx1ZSA9IG5ldyBNYWduZXRpY0RlY2ltYWxQaXBlKCkudHJhbnNmb3JtKG1lYXN1cmUudmFsdWUsIG1lYXN1cmUudW5pdCk7XG4gICAgICAgIHVuaXQgPSAnICcgKyBuZXcgTWFnbmV0aWNVbml0UGlwZSgpLnRyYW5zZm9ybShtZWFzdXJlLnVuaXQpO1xuICAgICAgICBicmVhaztcbiAgICAgIGRlZmF1bHQ6XG4gICAgICAgIHJldHVybiAnVW5zdXBwb3J0ZWQgVW5pdDogJyArIG1lYXN1cmUudW5pdDtcbiAgICB9XG4gICAgLy8gaWYgd2UgYXJlIGRpc3BsYXlpbmcgdW5pdHNcbiAgICBpZiAoZGlzcGxheVVuaXQpIHtcbiAgICAgIHJldHVybiB2YWx1ZSArIHVuaXQ7XG4gICAgfSBlbHNlIHsgLy8gaWYgd2UgYXJlbid0IGRpc3BsYXlpbmcgdW5pdHMsIGp1c3QgZ2l2ZSB0aGUgdmFsdWVcbiAgICAgIHJldHVybiB2YWx1ZTtcbiAgICB9XG4gIH1cblxufVxuIl19