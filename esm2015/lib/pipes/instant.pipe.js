/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Pipe } from '@angular/core';
import { DatePipe } from '@angular/common';
// Converts the seconds into human readable date
export class InstantPipe extends DatePipe {
    /**
     * @param {?} dateObj
     * @return {?}
     */
    transform(dateObj) {
        return (dateObj !== null && dateObj !== undefined) ? (super.transform(dateObj.seconds * 1000, 'medium', 'UTC')) : null;
    }
}
InstantPipe.decorators = [
    { type: Pipe, args: [{
                name: 'instant'
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5zdGFudC5waXBlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGhwdGVjaC9zYXBoaXJhLXNoYXJlZC1saWIvIiwic291cmNlcyI6WyJsaWIvcGlwZXMvaW5zdGFudC5waXBlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsSUFBSSxFQUFpQixNQUFNLGVBQWUsQ0FBQztBQUNwRCxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFLM0MsZ0RBQWdEO0FBQ2hELE1BQU0sT0FBTyxXQUFZLFNBQVEsUUFBUTs7Ozs7SUFFdkMsU0FBUyxDQUFDLE9BQVk7UUFDcEIsT0FBTyxDQUFDLE9BQU8sS0FBSyxJQUFJLElBQUksT0FBTyxLQUFLLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLE9BQU8sR0FBRyxJQUFJLEVBQUUsUUFBUSxFQUFFLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztJQUN6SCxDQUFDOzs7WUFSRixJQUFJLFNBQUM7Z0JBQ0osSUFBSSxFQUFFLFNBQVM7YUFDaEIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBQaXBlLCBQaXBlVHJhbnNmb3JtIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBEYXRlUGlwZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XG5cbkBQaXBlKHtcbiAgbmFtZTogJ2luc3RhbnQnXG59KVxuLy8gQ29udmVydHMgdGhlIHNlY29uZHMgaW50byBodW1hbiByZWFkYWJsZSBkYXRlXG5leHBvcnQgY2xhc3MgSW5zdGFudFBpcGUgZXh0ZW5kcyBEYXRlUGlwZSBpbXBsZW1lbnRzIFBpcGVUcmFuc2Zvcm0ge1xuXG4gIHRyYW5zZm9ybShkYXRlT2JqOiBhbnkpOiBhbnkge1xuICAgIHJldHVybiAoZGF0ZU9iaiAhPT0gbnVsbCAmJiBkYXRlT2JqICE9PSB1bmRlZmluZWQpID8gKHN1cGVyLnRyYW5zZm9ybShkYXRlT2JqLnNlY29uZHMgKiAxMDAwLCAnbWVkaXVtJywgJ1VUQycpKSA6IG51bGw7XG4gIH1cblxufVxuIl19