/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Pipe } from '@angular/core';
export class ResolvedStatePipe {
    // A simple filter that converts resolved state enums to friendlier string values.
    /**
     * @param {?} resolution
     * @param {?=} value
     * @return {?}
     */
    transform(resolution, value) {
        if (resolution === 'RESOLVED') {
            return 'Resolved';
        }
        else if (resolution === 'UNRESOLVED') {
            return 'Unresolved';
        }
        else {
            return 'Manually Set : ' + value;
        }
    }
}
ResolvedStatePipe.decorators = [
    { type: Pipe, args: [{
                name: 'resolvedState'
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmVzb2x2ZWQtc3RhdGUucGlwZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BocHRlY2gvc2FwaGlyYS1zaGFyZWQtbGliLyIsInNvdXJjZXMiOlsibGliL3BpcGVzL3Jlc29sdmVkLXN0YXRlLnBpcGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxJQUFJLEVBQWlCLE1BQU0sZUFBZSxDQUFDO0FBS3BELE1BQU0sT0FBTyxpQkFBaUI7Ozs7Ozs7SUFFNUIsU0FBUyxDQUFDLFVBQWUsRUFBRSxLQUFXO1FBQ3BDLElBQUksVUFBVSxLQUFLLFVBQVUsRUFBRTtZQUM3QixPQUFPLFVBQVUsQ0FBQztTQUNuQjthQUFNLElBQUksVUFBVSxLQUFLLFlBQVksRUFBRTtZQUN0QyxPQUFPLFlBQVksQ0FBQztTQUNyQjthQUFNO1lBQ0wsT0FBTyxpQkFBaUIsR0FBRyxLQUFLLENBQUM7U0FDbEM7SUFDSCxDQUFDOzs7WUFiRixJQUFJLFNBQUM7Z0JBQ0osSUFBSSxFQUFFLGVBQWU7YUFDdEIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBQaXBlLCBQaXBlVHJhbnNmb3JtIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBQaXBlKHtcbiAgbmFtZTogJ3Jlc29sdmVkU3RhdGUnXG59KVxuZXhwb3J0IGNsYXNzIFJlc29sdmVkU3RhdGVQaXBlIGltcGxlbWVudHMgUGlwZVRyYW5zZm9ybSB7XG4gIC8vIEEgc2ltcGxlIGZpbHRlciB0aGF0IGNvbnZlcnRzIHJlc29sdmVkIHN0YXRlIGVudW1zIHRvIGZyaWVuZGxpZXIgc3RyaW5nIHZhbHVlcy5cbiAgdHJhbnNmb3JtKHJlc29sdXRpb246IGFueSwgdmFsdWU/OiBhbnkpOiBhbnkge1xuICAgIGlmIChyZXNvbHV0aW9uID09PSAnUkVTT0xWRUQnKSB7XG4gICAgICByZXR1cm4gJ1Jlc29sdmVkJztcbiAgICB9IGVsc2UgaWYgKHJlc29sdXRpb24gPT09ICdVTlJFU09MVkVEJykge1xuICAgICAgcmV0dXJuICdVbnJlc29sdmVkJztcbiAgICB9IGVsc2Uge1xuICAgICAgcmV0dXJuICdNYW51YWxseSBTZXQgOiAnICsgdmFsdWU7XG4gICAgfVxuICB9XG59XG4iXX0=