/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Pipe } from '@angular/core';
// Orders the surveys in ascending/descending order based on the Md value
export class OrderSurveysByMdPipe {
    constructor() {
        this.filtered = [];
    }
    /**
     * @param {?} items
     * @param {?=} reverse
     * @return {?}
     */
    transform(items, reverse) {
        items.forEach((/**
         * @param {?} item
         * @return {?}
         */
        item => {
            this.filtered.push(item);
        }));
        this.filtered.sort((/**
         * @param {?} a
         * @param {?} b
         * @return {?}
         */
        (a, b) => {
            return (a.md.value > b.md.value ? 1 : -1);
        }));
        if (reverse) {
            this.filtered.reverse();
        }
        return this.filtered;
    }
}
OrderSurveysByMdPipe.decorators = [
    { type: Pipe, args: [{
                name: 'orderSurveysByMd'
            },] }
];
if (false) {
    /** @type {?} */
    OrderSurveysByMdPipe.prototype.filtered;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib3JkZXItc3VydmV5cy1ieS1tZC5waXBlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGhwdGVjaC9zYXBoaXJhLXNoYXJlZC1saWIvIiwic291cmNlcyI6WyJsaWIvcGlwZXMvb3JkZXItc3VydmV5cy1ieS1tZC5waXBlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsSUFBSSxFQUFpQixNQUFNLGVBQWUsQ0FBQztBQUtwRCx5RUFBeUU7QUFDekUsTUFBTSxPQUFPLG9CQUFvQjtJQUpqQztRQUtFLGFBQVEsR0FBRyxFQUFFLENBQUM7SUFhaEIsQ0FBQzs7Ozs7O0lBWkMsU0FBUyxDQUFDLEtBQVUsRUFBRSxPQUFhO1FBQ2pDLEtBQUssQ0FBQyxPQUFPOzs7O1FBQUMsSUFBSSxDQUFDLEVBQUU7WUFDbkIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDM0IsQ0FBQyxFQUFDLENBQUM7UUFDSCxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUk7Ozs7O1FBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFLEVBQUU7WUFDMUIsT0FBTyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsS0FBSyxHQUFHLENBQUMsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDNUMsQ0FBQyxFQUFDLENBQUM7UUFDSCxJQUFJLE9BQU8sRUFBRTtZQUNYLElBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxFQUFFLENBQUM7U0FDekI7UUFDRCxPQUFPLElBQUksQ0FBQyxRQUFRLENBQUM7SUFDdkIsQ0FBQzs7O1lBakJGLElBQUksU0FBQztnQkFDSixJQUFJLEVBQUUsa0JBQWtCO2FBQ3pCOzs7O0lBR0Msd0NBQWMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBQaXBlLCBQaXBlVHJhbnNmb3JtIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBQaXBlKHtcbiAgbmFtZTogJ29yZGVyU3VydmV5c0J5TWQnXG59KVxuLy8gT3JkZXJzIHRoZSBzdXJ2ZXlzIGluIGFzY2VuZGluZy9kZXNjZW5kaW5nIG9yZGVyIGJhc2VkIG9uIHRoZSBNZCB2YWx1ZVxuZXhwb3J0IGNsYXNzIE9yZGVyU3VydmV5c0J5TWRQaXBlIGltcGxlbWVudHMgUGlwZVRyYW5zZm9ybSB7XG4gIGZpbHRlcmVkID0gW107XG4gIHRyYW5zZm9ybShpdGVtczogYW55LCByZXZlcnNlPzogYW55KTogYW55IHtcbiAgICBpdGVtcy5mb3JFYWNoKGl0ZW0gPT4ge1xuICAgICAgdGhpcy5maWx0ZXJlZC5wdXNoKGl0ZW0pO1xuICAgIH0pO1xuICAgIHRoaXMuZmlsdGVyZWQuc29ydCgoYSwgYikgPT4ge1xuICAgICAgcmV0dXJuIChhLm1kLnZhbHVlID4gYi5tZC52YWx1ZSA/IDEgOiAtMSk7XG4gICAgfSk7XG4gICAgaWYgKHJldmVyc2UpIHtcbiAgICAgIHRoaXMuZmlsdGVyZWQucmV2ZXJzZSgpO1xuICAgIH1cbiAgICByZXR1cm4gdGhpcy5maWx0ZXJlZDtcbiAgfVxufVxuIl19