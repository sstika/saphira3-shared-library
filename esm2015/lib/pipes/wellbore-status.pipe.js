/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Pipe } from '@angular/core';
export class WellboreStatusPipe {
    /**
     * @param {?} value
     * @param {?=} args
     * @return {?}
     */
    transform(value, args) {
        switch (value) {
            case 'ACTIVE_BATCH_PROCESS':
                return 'Active - Batch Process';
            case 'ACTIVE_REALTIME':
                return 'Active - Realtime';
            case 'PRE_OPERATIONS':
                return 'Pre-Operations';
            case 'COMPLETED':
                return 'Completed';
            default:
                return 'Unknown status';
        }
    }
}
WellboreStatusPipe.decorators = [
    { type: Pipe, args: [{
                name: 'wellboreStatus'
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoid2VsbGJvcmUtc3RhdHVzLnBpcGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AaHB0ZWNoL3NhcGhpcmEtc2hhcmVkLWxpYi8iLCJzb3VyY2VzIjpbImxpYi9waXBlcy93ZWxsYm9yZS1zdGF0dXMucGlwZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLElBQUksRUFBaUIsTUFBTSxlQUFlLENBQUM7QUFLcEQsTUFBTSxPQUFPLGtCQUFrQjs7Ozs7O0lBRTdCLFNBQVMsQ0FBQyxLQUFVLEVBQUUsSUFBVTtRQUM5QixRQUFRLEtBQUssRUFBRTtZQUNYLEtBQUssc0JBQXNCO2dCQUN2QixPQUFPLHdCQUF3QixDQUFDO1lBQ3BDLEtBQUssaUJBQWlCO2dCQUNsQixPQUFPLG1CQUFtQixDQUFDO1lBQy9CLEtBQUssZ0JBQWdCO2dCQUNqQixPQUFPLGdCQUFnQixDQUFDO1lBQzVCLEtBQUssV0FBVztnQkFDWixPQUFPLFdBQVcsQ0FBQztZQUN2QjtnQkFDSSxPQUFPLGdCQUFnQixDQUFDO1NBQy9CO0lBQ0gsQ0FBQzs7O1lBbEJGLElBQUksU0FBQztnQkFDSixJQUFJLEVBQUUsZ0JBQWdCO2FBQ3ZCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgUGlwZSwgUGlwZVRyYW5zZm9ybSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5AUGlwZSh7XG4gIG5hbWU6ICd3ZWxsYm9yZVN0YXR1cydcbn0pXG5leHBvcnQgY2xhc3MgV2VsbGJvcmVTdGF0dXNQaXBlIGltcGxlbWVudHMgUGlwZVRyYW5zZm9ybSB7XG5cbiAgdHJhbnNmb3JtKHZhbHVlOiBhbnksIGFyZ3M/OiBhbnkpOiBhbnkge1xuICAgIHN3aXRjaCAodmFsdWUpIHtcbiAgICAgICAgY2FzZSAnQUNUSVZFX0JBVENIX1BST0NFU1MnOlxuICAgICAgICAgICAgcmV0dXJuICdBY3RpdmUgLSBCYXRjaCBQcm9jZXNzJztcbiAgICAgICAgY2FzZSAnQUNUSVZFX1JFQUxUSU1FJzpcbiAgICAgICAgICAgIHJldHVybiAnQWN0aXZlIC0gUmVhbHRpbWUnO1xuICAgICAgICBjYXNlICdQUkVfT1BFUkFUSU9OUyc6XG4gICAgICAgICAgICByZXR1cm4gJ1ByZS1PcGVyYXRpb25zJztcbiAgICAgICAgY2FzZSAnQ09NUExFVEVEJzpcbiAgICAgICAgICAgIHJldHVybiAnQ29tcGxldGVkJztcbiAgICAgICAgZGVmYXVsdDpcbiAgICAgICAgICAgIHJldHVybiAnVW5rbm93biBzdGF0dXMnO1xuICAgIH1cbiAgfVxuXG59XG4iXX0=