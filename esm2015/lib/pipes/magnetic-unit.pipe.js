/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Pipe } from '@angular/core';
export class MagneticUnitPipe {
    /**
     * @param {?} unit
     * @param {?=} full
     * @return {?}
     */
    transform(unit, full) {
        /** @type {?} */
        let string = '';
        switch (unit) {
            case 'NANOTESLA':
                string = (full === true ? 'Nanotesla' : 'nT');
                break;
            case 'MICROTESLA':
                string = (full === true ? 'Microtesla' : decodeURI('%C2%B5') + 'T');
                break;
            case 'MILLITESLA':
                string = (full === true ? 'Millitesla' : 'mT');
                break;
            case 'TESLA':
                string = (full === true ? 'Tesla' : 'T');
                break;
            case 'GAUSS':
                string = (full === true ? 'Gauss' : 'Gs');
                break;
            case 'GAMMA':
                string = (full === true ? 'Gamma' : decodeURI('%CE%B3'));
                break;
            default:
                string = unit;
                break;
        }
        return string;
    }
}
MagneticUnitPipe.decorators = [
    { type: Pipe, args: [{
                name: 'magneticUnit'
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFnbmV0aWMtdW5pdC5waXBlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGhwdGVjaC9zYXBoaXJhLXNoYXJlZC1saWIvIiwic291cmNlcyI6WyJsaWIvcGlwZXMvbWFnbmV0aWMtdW5pdC5waXBlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsSUFBSSxFQUFpQixNQUFNLGVBQWUsQ0FBQztBQUtwRCxNQUFNLE9BQU8sZ0JBQWdCOzs7Ozs7SUFFM0IsU0FBUyxDQUFDLElBQVMsRUFBRSxJQUFVOztZQUN6QixNQUFNLEdBQUcsRUFBRTtRQUNmLFFBQVEsSUFBSSxFQUFFO1lBQ1osS0FBSyxXQUFXO2dCQUNkLE1BQU0sR0FBRyxDQUFDLElBQUksS0FBSyxJQUFJLENBQUMsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQzlDLE1BQU07WUFDUixLQUFLLFlBQVk7Z0JBQ2YsTUFBTSxHQUFHLENBQUMsSUFBSSxLQUFLLElBQUksQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLEdBQUcsR0FBRyxDQUFDLENBQUM7Z0JBQ3BFLE1BQU07WUFDUixLQUFLLFlBQVk7Z0JBQ2YsTUFBTSxHQUFHLENBQUMsSUFBSSxLQUFLLElBQUksQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDL0MsTUFBTTtZQUNSLEtBQUssT0FBTztnQkFDVixNQUFNLEdBQUcsQ0FBQyxJQUFJLEtBQUssSUFBSSxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDO2dCQUN6QyxNQUFNO1lBQ1IsS0FBSyxPQUFPO2dCQUNWLE1BQU0sR0FBRyxDQUFDLElBQUksS0FBSyxJQUFJLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQzFDLE1BQU07WUFDUixLQUFLLE9BQU87Z0JBQ1YsTUFBTSxHQUFHLENBQUMsSUFBSSxLQUFLLElBQUksQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztnQkFDekQsTUFBTTtZQUNSO2dCQUNFLE1BQU0sR0FBRyxJQUFJLENBQUM7Z0JBQ2QsTUFBTTtTQUNUO1FBQ0QsT0FBTyxNQUFNLENBQUM7SUFDaEIsQ0FBQzs7O1lBL0JGLElBQUksU0FBQztnQkFDSixJQUFJLEVBQUUsY0FBYzthQUNyQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFBpcGUsIFBpcGVUcmFuc2Zvcm0gfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQFBpcGUoe1xuICBuYW1lOiAnbWFnbmV0aWNVbml0J1xufSlcbmV4cG9ydCBjbGFzcyBNYWduZXRpY1VuaXRQaXBlIGltcGxlbWVudHMgUGlwZVRyYW5zZm9ybSB7XG5cbiAgdHJhbnNmb3JtKHVuaXQ6IGFueSwgZnVsbD86IGFueSk6IGFueSB7XG4gICAgbGV0IHN0cmluZyA9ICcnO1xuICAgIHN3aXRjaCAodW5pdCkge1xuICAgICAgY2FzZSAnTkFOT1RFU0xBJzpcbiAgICAgICAgc3RyaW5nID0gKGZ1bGwgPT09IHRydWUgPyAnTmFub3Rlc2xhJyA6ICduVCcpO1xuICAgICAgICBicmVhaztcbiAgICAgIGNhc2UgJ01JQ1JPVEVTTEEnOlxuICAgICAgICBzdHJpbmcgPSAoZnVsbCA9PT0gdHJ1ZSA/ICdNaWNyb3Rlc2xhJyA6IGRlY29kZVVSSSgnJUMyJUI1JykgKyAnVCcpO1xuICAgICAgICBicmVhaztcbiAgICAgIGNhc2UgJ01JTExJVEVTTEEnOlxuICAgICAgICBzdHJpbmcgPSAoZnVsbCA9PT0gdHJ1ZSA/ICdNaWxsaXRlc2xhJyA6ICdtVCcpO1xuICAgICAgICBicmVhaztcbiAgICAgIGNhc2UgJ1RFU0xBJzpcbiAgICAgICAgc3RyaW5nID0gKGZ1bGwgPT09IHRydWUgPyAnVGVzbGEnIDogJ1QnKTtcbiAgICAgICAgYnJlYWs7XG4gICAgICBjYXNlICdHQVVTUyc6XG4gICAgICAgIHN0cmluZyA9IChmdWxsID09PSB0cnVlID8gJ0dhdXNzJyA6ICdHcycpO1xuICAgICAgICBicmVhaztcbiAgICAgIGNhc2UgJ0dBTU1BJzpcbiAgICAgICAgc3RyaW5nID0gKGZ1bGwgPT09IHRydWUgPyAnR2FtbWEnIDogZGVjb2RlVVJJKCclQ0UlQjMnKSk7XG4gICAgICAgIGJyZWFrO1xuICAgICAgZGVmYXVsdDpcbiAgICAgICAgc3RyaW5nID0gdW5pdDtcbiAgICAgICAgYnJlYWs7XG4gICAgfVxuICAgIHJldHVybiBzdHJpbmc7XG4gIH1cblxufVxuIl19