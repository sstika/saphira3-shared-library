/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Pipe } from '@angular/core';
export class SurveySetStatePipe {
    /**
     * @param {?} value
     * @return {?}
     */
    transform(value) {
        switch (value) {
            case 'WAITING_SURVEY_SUBMIT':
                return 'Waiting for a new survey';
            case 'WAITING_CORRECTION':
                return 'Waiting for a correction';
            case 'WAITING_VALIDATION':
                return 'Waiting for validation';
            default:
                return 'Unknown';
        }
    }
}
SurveySetStatePipe.decorators = [
    { type: Pipe, args: [{
                name: 'surveySetState'
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3VydmV5LXNldC1zdGF0ZS5waXBlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGhwdGVjaC9zYXBoaXJhLXNoYXJlZC1saWIvIiwic291cmNlcyI6WyJsaWIvcGlwZXMvc3VydmV5LXNldC1zdGF0ZS5waXBlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsSUFBSSxFQUFpQixNQUFNLGVBQWUsQ0FBQztBQUtwRCxNQUFNLE9BQU8sa0JBQWtCOzs7OztJQUU3QixTQUFTLENBQUMsS0FBYTtRQUNyQixRQUFRLEtBQUssRUFBRTtZQUNiLEtBQUssdUJBQXVCO2dCQUMxQixPQUFPLDBCQUEwQixDQUFDO1lBQ3BDLEtBQUssb0JBQW9CO2dCQUN2QixPQUFPLDBCQUEwQixDQUFDO1lBQ3BDLEtBQUssb0JBQW9CO2dCQUN2QixPQUFPLHdCQUF3QixDQUFDO1lBQ2xDO2dCQUNFLE9BQU8sU0FBUyxDQUFDO1NBQ3BCO0lBQ0gsQ0FBQzs7O1lBaEJGLElBQUksU0FBQztnQkFDSixJQUFJLEVBQUUsZ0JBQWdCO2FBQ3ZCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgUGlwZSwgUGlwZVRyYW5zZm9ybSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5AUGlwZSh7XG4gIG5hbWU6ICdzdXJ2ZXlTZXRTdGF0ZSdcbn0pXG5leHBvcnQgY2xhc3MgU3VydmV5U2V0U3RhdGVQaXBlIGltcGxlbWVudHMgUGlwZVRyYW5zZm9ybSB7XG5cbiAgdHJhbnNmb3JtKHZhbHVlOiBzdHJpbmcpOiBhbnkge1xuICAgIHN3aXRjaCAodmFsdWUpIHtcbiAgICAgIGNhc2UgJ1dBSVRJTkdfU1VSVkVZX1NVQk1JVCc6XG4gICAgICAgIHJldHVybiAnV2FpdGluZyBmb3IgYSBuZXcgc3VydmV5JztcbiAgICAgIGNhc2UgJ1dBSVRJTkdfQ09SUkVDVElPTic6XG4gICAgICAgIHJldHVybiAnV2FpdGluZyBmb3IgYSBjb3JyZWN0aW9uJztcbiAgICAgIGNhc2UgJ1dBSVRJTkdfVkFMSURBVElPTic6XG4gICAgICAgIHJldHVybiAnV2FpdGluZyBmb3IgdmFsaWRhdGlvbic7XG4gICAgICBkZWZhdWx0OlxuICAgICAgICByZXR1cm4gJ1Vua25vd24nO1xuICAgIH1cbiAgfVxufVxuIl19