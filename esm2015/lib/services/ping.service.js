/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { DataService } from './data.service';
import { HttpClient } from '@angular/common/http';
import { catchError, mergeMap, retry, tap } from 'rxjs/operators';
import { interval, BehaviorSubject, throwError, concat, EMPTY } from 'rxjs';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common/http";
export class PingService extends DataService {
    /**
     * @param {?} http
     */
    constructor(http) {
        super(http);
        this.pingSuffix = 'ping';
        this.pingStatus = new BehaviorSubject(true);
        /**
         * Round trip time in ms
         */
        this.tripTime = 0;
    }
    /**
     * @return {?}
     */
    start() {
        // only one ping
        if (this.ping) {
            return;
        }
        /** @type {?} */
        let startTime;
        /** @type {?} */
        const firstPing = super.get(this.pingSuffix).pipe(catchError((/**
         * @return {?}
         */
        () => {
            this.pingStatus.next(false);
            return EMPTY;
        })));
        /** @type {?} */
        const recurringPing = interval(10000).pipe(tap((/**
         * @return {?}
         */
        () => startTime = performance.now())), mergeMap((/**
         * @return {?}
         */
        () => super.get(this.pingSuffix))), catchError((/**
         * @param {?} error
         * @return {?}
         */
        error => {
            this.pingStatus.next(false);
            return throwError(error);
        })), retry() // DON'T STOP ME NOW!
        );
        this.ping = concat(firstPing, recurringPing)
            .subscribe((/**
         * @param {?} apiVersion
         * @return {?}
         */
        apiVersion => {
            this.tripTime = performance.now() - startTime;
            this.pingStatus.next(true);
            this.apiVersion = apiVersion;
        }));
    }
    /**
     * @return {?}
     */
    status() {
        return this.pingStatus.asObservable();
    }
}
PingService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
PingService.ctorParameters = () => [
    { type: HttpClient }
];
/** @nocollapse */ PingService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function PingService_Factory() { return new PingService(i0.ɵɵinject(i1.HttpClient)); }, token: PingService, providedIn: "root" });
if (false) {
    /**
     * @type {?}
     * @private
     */
    PingService.prototype.ping;
    /**
     * @type {?}
     * @private
     */
    PingService.prototype.pingSuffix;
    /**
     * @type {?}
     * @private
     */
    PingService.prototype.pingStatus;
    /**
     * Round trip time in ms
     * @type {?}
     */
    PingService.prototype.tripTime;
    /**
     * Backend API version
     * @type {?}
     */
    PingService.prototype.apiVersion;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGluZy5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGhwdGVjaC9zYXBoaXJhLXNoYXJlZC1saWIvIiwic291cmNlcyI6WyJsaWIvc2VydmljZXMvcGluZy5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUM3QyxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFDbEQsT0FBTyxFQUFFLFVBQVUsRUFBRSxRQUFRLEVBQUUsS0FBSyxFQUFFLEdBQUcsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQ2xFLE9BQU8sRUFBRSxRQUFRLEVBQWMsZUFBZSxFQUFFLFVBQVUsRUFBZ0IsTUFBTSxFQUFFLEtBQUssRUFBRSxNQUFNLE1BQU0sQ0FBQzs7O0FBS3RHLE1BQU0sT0FBTyxXQUFZLFNBQVEsV0FBVzs7OztJQVcxQyxZQUNFLElBQWdCO1FBRWhCLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztRQVpOLGVBQVUsR0FBRyxNQUFNLENBQUM7UUFDcEIsZUFBVSxHQUFHLElBQUksZUFBZSxDQUFDLElBQUksQ0FBQyxDQUFDOzs7O1FBR3hDLGFBQVEsR0FBRyxDQUFDLENBQUM7SUFTcEIsQ0FBQzs7OztJQUVELEtBQUs7UUFDSCxnQkFBZ0I7UUFDaEIsSUFBSyxJQUFJLENBQUMsSUFBSSxFQUFHO1lBQ2YsT0FBTztTQUNSOztZQUNHLFNBQVM7O2NBRVAsU0FBUyxHQUFHLEtBQUssQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLElBQUksQ0FDL0MsVUFBVTs7O1FBQUUsR0FBRyxFQUFFO1lBQ2YsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDNUIsT0FBTyxLQUFLLENBQUM7UUFDZixDQUFDLEVBQUMsQ0FDSDs7Y0FFSyxhQUFhLEdBQUcsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDLElBQUksQ0FDeEMsR0FBRzs7O1FBQUUsR0FBRyxFQUFFLENBQUMsU0FBUyxHQUFHLFdBQVcsQ0FBQyxHQUFHLEVBQUUsRUFBRSxFQUMxQyxRQUFROzs7UUFBRSxHQUFHLEVBQUUsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsRUFBRSxFQUM1QyxVQUFVOzs7O1FBQUUsS0FBSyxDQUFDLEVBQUU7WUFDbEIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDNUIsT0FBTyxVQUFVLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDM0IsQ0FBQyxFQUFDLEVBQ0YsS0FBSyxFQUFFLENBQUMscUJBQXFCO1NBQzlCO1FBRUQsSUFBSSxDQUFDLElBQUksR0FBRyxNQUFNLENBQUMsU0FBUyxFQUFFLGFBQWEsQ0FBQzthQUN6QyxTQUFTOzs7O1FBQUUsVUFBVSxDQUFDLEVBQUU7WUFDdkIsSUFBSSxDQUFDLFFBQVEsR0FBRyxXQUFXLENBQUMsR0FBRyxFQUFFLEdBQUcsU0FBUyxDQUFDO1lBQzlDLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQzNCLElBQUksQ0FBQyxVQUFVLEdBQUcsVUFBVSxDQUFDO1FBQy9CLENBQUMsRUFBQyxDQUFDO0lBQ1AsQ0FBQzs7OztJQUVELE1BQU07UUFDSixPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsWUFBWSxFQUFFLENBQUM7SUFDeEMsQ0FBQzs7O1lBdERGLFVBQVUsU0FBQztnQkFDVixVQUFVLEVBQUUsTUFBTTthQUNuQjs7OztZQU5RLFVBQVU7Ozs7Ozs7O0lBUWpCLDJCQUEyQjs7Ozs7SUFDM0IsaUNBQTRCOzs7OztJQUM1QixpQ0FBK0M7Ozs7O0lBRy9DLCtCQUFvQjs7Ozs7SUFHcEIsaUNBQTBCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgRGF0YVNlcnZpY2UgfSBmcm9tICcuL2RhdGEuc2VydmljZSc7XG5pbXBvcnQgeyBIdHRwQ2xpZW50IH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xuaW1wb3J0IHsgY2F0Y2hFcnJvciwgbWVyZ2VNYXAsIHJldHJ5LCB0YXAgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XG5pbXBvcnQgeyBpbnRlcnZhbCwgT2JzZXJ2YWJsZSwgQmVoYXZpb3JTdWJqZWN0LCB0aHJvd0Vycm9yLCBTdWJzY3JpcHRpb24sIGNvbmNhdCwgRU1QVFkgfSBmcm9tICdyeGpzJztcblxuQEluamVjdGFibGUoe1xuICBwcm92aWRlZEluOiAncm9vdCdcbn0pXG5leHBvcnQgY2xhc3MgUGluZ1NlcnZpY2UgZXh0ZW5kcyBEYXRhU2VydmljZSB7XG4gIHByaXZhdGUgcGluZzogU3Vic2NyaXB0aW9uO1xuICBwcml2YXRlIHBpbmdTdWZmaXggPSAncGluZyc7XG4gIHByaXZhdGUgcGluZ1N0YXR1cyA9IG5ldyBCZWhhdmlvclN1YmplY3QodHJ1ZSk7XG5cbiAgLyoqIFJvdW5kIHRyaXAgdGltZSBpbiBtcyAqL1xuICBwdWJsaWMgdHJpcFRpbWUgPSAwO1xuXG4gIC8qKiBCYWNrZW5kIEFQSSB2ZXJzaW9uICovXG4gIHB1YmxpYyBhcGlWZXJzaW9uOiBzdHJpbmc7XG5cbiAgY29uc3RydWN0b3IoXG4gICAgaHR0cDogSHR0cENsaWVudFxuICApIHtcbiAgICBzdXBlcihodHRwKTtcbiAgfVxuXG4gIHN0YXJ0KCk6IHZvaWQge1xuICAgIC8vIG9ubHkgb25lIHBpbmdcbiAgICBpZiAoIHRoaXMucGluZyApIHtcbiAgICAgIHJldHVybjtcbiAgICB9XG4gICAgbGV0IHN0YXJ0VGltZTtcblxuICAgIGNvbnN0IGZpcnN0UGluZyA9IHN1cGVyLmdldCh0aGlzLnBpbmdTdWZmaXgpLnBpcGUoXG4gICAgICBjYXRjaEVycm9yKCAoKSA9PiB7XG4gICAgICAgIHRoaXMucGluZ1N0YXR1cy5uZXh0KGZhbHNlKTtcbiAgICAgICAgcmV0dXJuIEVNUFRZO1xuICAgICAgfSlcbiAgICApO1xuXG4gICAgY29uc3QgcmVjdXJyaW5nUGluZyA9IGludGVydmFsKDEwMDAwKS5waXBlKFxuICAgICAgdGFwKCAoKSA9PiBzdGFydFRpbWUgPSBwZXJmb3JtYW5jZS5ub3coKSApLFxuICAgICAgbWVyZ2VNYXAoICgpID0+IHN1cGVyLmdldCh0aGlzLnBpbmdTdWZmaXgpICksXG4gICAgICBjYXRjaEVycm9yKCBlcnJvciA9PiB7XG4gICAgICAgIHRoaXMucGluZ1N0YXR1cy5uZXh0KGZhbHNlKTtcbiAgICAgICAgcmV0dXJuIHRocm93RXJyb3IoZXJyb3IpO1xuICAgICAgfSksXG4gICAgICByZXRyeSgpIC8vIERPTidUIFNUT1AgTUUgTk9XIVxuICAgICk7XG5cbiAgICB0aGlzLnBpbmcgPSBjb25jYXQoZmlyc3RQaW5nLCByZWN1cnJpbmdQaW5nKVxuICAgICAgLnN1YnNjcmliZSggYXBpVmVyc2lvbiA9PiB7XG4gICAgICAgIHRoaXMudHJpcFRpbWUgPSBwZXJmb3JtYW5jZS5ub3coKSAtIHN0YXJ0VGltZTtcbiAgICAgICAgdGhpcy5waW5nU3RhdHVzLm5leHQodHJ1ZSk7XG4gICAgICAgIHRoaXMuYXBpVmVyc2lvbiA9IGFwaVZlcnNpb247XG4gICAgICB9KTtcbiAgfVxuXG4gIHN0YXR1cygpOiBPYnNlcnZhYmxlPGJvb2xlYW4+IHtcbiAgICByZXR1cm4gdGhpcy5waW5nU3RhdHVzLmFzT2JzZXJ2YWJsZSgpO1xuICB9XG59XG4iXX0=