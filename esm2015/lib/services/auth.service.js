/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { DataService } from './data.service';
export class AuthService extends DataService {
    /**
     * @param {?} http
     * @param {?} router
     */
    constructor(http, router) {
        super(http);
        this.router = router;
        this.loginPath = 'login';
        this.logoutPath = 'logout';
        this.userInfo = null;
    }
    /**
     * @param {?} username
     * @param {?} password
     * @return {?}
     */
    login(username, password) {
        // token specific header, will need to be altered for usage
        /** @type {?} */
        const hdr = {
            'Authorization': 'Basic ' + window.btoa(username + ':' + password)
        };
        return super.get(this.loginPath, hdr)
            .pipe(tap((/**
         * @param {?} res
         * @return {?}
         */
        res => {
            if (res) {
                this.userInfo = res;
            }
        })), catchError((/**
         * @return {?}
         */
        () => of(null))));
    }
    /**
     * @return {?}
     */
    logout() {
        this.userInfo = null;
        this.router.navigate(['/login']);
        return this.post(this.logoutPath, null);
    }
    /**
     * Called when a 401 status is encountered which means user is logged out
     * @return {?}
     */
    on401Error() {
        this.userInfo = null;
        this.router.navigate(['/login']);
    }
    /**
     * @return {?}
     */
    getUser() {
        // Check our cache
        if (this.userInfo) {
            return of(this.userInfo);
        }
        // Try to get it from the server
        return super.get(this.loginPath);
    }
    // TODO: Make these permissions an Enum
    // TODO: Is this list complete?
    /* Permissions from legacy saphira
         CREATE,
         READ,
         UPDATE,
         DELETE,
         MSA,
         IMPORT_CSV, //Old
         SNAPSHOT, //Import
         EXPORT_FULL, //Exporting survey set only
         EXPORT_TRAJECTORY, //Exporting trajectory only
         EXPORT_MINIMAL, //Not used yet, kept for future use (surveyset export without msa sixaxis data?)
         METADATA,
         MWD_READER,
         ATTACHMENT,
         QC_PLOTS,
         QC_RAW,
         QC_CORR,
         QC_TOL,
         QC_INCAZI_CALC,
         QC_INCAZI_CORR,
         QC_VALIDATION_ERRORS,
         AUTOMATION,
         SHIFT_NOTES,
         CHAT,
         MODIFY_PERMISSIONS,
         ACTIVITY_LOG,
         DATA_VIEW,
         COORD_SYSTEM,
         NOTIFICATIONS,
         API_SURVEY_READ,
         API_SURVEY_SUBMIT;
      */
    // TODO: move into user model
    // TODO: this isn't quite correct
    /**
     * @param {?} perm2Match
     * @return {?}
     */
    userhasPermission(perm2Match) {
        /** @type {?} */
        const hasAuth = this.userInfo['authorities'].filter((/**
         * @param {?} auth
         * @return {?}
         */
        (auth) => {
            return auth['role'] === 'ROLE_MAGVAR_SUPERADMIN' ||
                auth['role'] === 'ROLE_crowd-administrators' ||
                auth['role'] === 'ROLE_MAGVAR_DEVELOPER' ||
                auth['role'].indexOf(perm2Match) > -1;
        }));
        return hasAuth.length > 0;
    }
    // TODO: move into model
    /**
     * @return {?}
     */
    userIsCustomer() {
        /** @type {?} */
        const hasAuth = this.userInfo['authorities'].filter((/**
         * @param {?} auth
         * @return {?}
         */
        (auth) => {
            return auth['role'] === 'ROLE_Rig' || auth['role'] === 'ROLE_Operators';
        }));
        return hasAuth.length > 0;
    }
}
AuthService.decorators = [
    { type: Injectable }
];
/** @nocollapse */
AuthService.ctorParameters = () => [
    { type: HttpClient },
    { type: Router }
];
if (false) {
    /**
     * @type {?}
     * @private
     */
    AuthService.prototype.loginPath;
    /**
     * @type {?}
     * @private
     */
    AuthService.prototype.logoutPath;
    /**
     * @type {?}
     * @private
     */
    AuthService.prototype.userInfo;
    /**
     * @type {?}
     * @private
     */
    AuthService.prototype.router;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0aC5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGhwdGVjaC9zYXBoaXJhLXNoYXJlZC1saWIvIiwic291cmNlcyI6WyJsaWIvc2VydmljZXMvYXV0aC5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUNsRCxPQUFPLEVBQUUsTUFBTSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFFekMsT0FBTyxFQUFjLEVBQUUsRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUN0QyxPQUFPLEVBQUUsVUFBVSxFQUFFLEdBQUcsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQ2pELE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUc3QyxNQUFNLE9BQU8sV0FBWSxTQUFRLFdBQVc7Ozs7O0lBTTFDLFlBQ0UsSUFBZ0IsRUFDUixNQUFjO1FBRXRCLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUZKLFdBQU0sR0FBTixNQUFNLENBQVE7UUFQaEIsY0FBUyxHQUFHLE9BQU8sQ0FBQztRQUNwQixlQUFVLEdBQUcsUUFBUSxDQUFDO1FBRXRCLGFBQVEsR0FBUSxJQUFJLENBQUM7SUFPN0IsQ0FBQzs7Ozs7O0lBRU0sS0FBSyxDQUFDLFFBQWdCLEVBQUUsUUFBZ0I7OztjQUV2QyxHQUFHLEdBQUc7WUFDVixlQUFlLEVBQUUsUUFBUSxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsUUFBUSxHQUFHLEdBQUcsR0FBRyxRQUFRLENBQUM7U0FDbkU7UUFFRCxPQUFPLEtBQUssQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxHQUFHLENBQUM7YUFDeEIsSUFBSSxDQUNILEdBQUc7Ozs7UUFBQyxHQUFHLENBQUMsRUFBRTtZQUNSLElBQUssR0FBRyxFQUFHO2dCQUNULElBQUksQ0FBQyxRQUFRLEdBQUcsR0FBRyxDQUFDO2FBQ3JCO1FBQ0gsQ0FBQyxFQUFDLEVBQ0YsVUFBVTs7O1FBQUUsR0FBRyxFQUFFLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQzdCLENBQUM7SUFDaEIsQ0FBQzs7OztJQUVNLE1BQU07UUFDWCxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztRQUNyQixJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7UUFDakMsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFDMUMsQ0FBQzs7Ozs7SUFLTSxVQUFVO1FBQ2YsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7UUFDckIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO0lBQ25DLENBQUM7Ozs7SUFFTSxPQUFPO1FBQ1osa0JBQWtCO1FBQ2xCLElBQUssSUFBSSxDQUFDLFFBQVEsRUFBRztZQUNuQixPQUFPLEVBQUUsQ0FBRSxJQUFJLENBQUMsUUFBUSxDQUFFLENBQUM7U0FDNUI7UUFFRCxnQ0FBZ0M7UUFDaEMsT0FBTyxLQUFLLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztJQUNuQyxDQUFDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztJQXVDTSxpQkFBaUIsQ0FBQyxVQUFrQjs7Y0FDbkMsT0FBTyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLENBQUMsTUFBTTs7OztRQUFDLENBQUMsSUFBSSxFQUFFLEVBQUU7WUFDM0QsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssd0JBQXdCO2dCQUM5QyxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssMkJBQTJCO2dCQUM1QyxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssdUJBQXVCO2dCQUN4QyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO1FBQzFDLENBQUMsRUFBQztRQUNGLE9BQU8sT0FBTyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUM7SUFDNUIsQ0FBQzs7Ozs7SUFHTSxjQUFjOztjQUNiLE9BQU8sR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxDQUFDLE1BQU07Ozs7UUFBQyxDQUFDLElBQUksRUFBRSxFQUFFO1lBQzNELE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLFVBQVUsSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssZ0JBQWdCLENBQUM7UUFDMUUsQ0FBQyxFQUFDO1FBQ0YsT0FBTyxPQUFPLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQztJQUM1QixDQUFDOzs7WUE1R0YsVUFBVTs7OztZQVBGLFVBQVU7WUFDVixNQUFNOzs7Ozs7O0lBUWIsZ0NBQTRCOzs7OztJQUM1QixpQ0FBOEI7Ozs7O0lBRTlCLCtCQUE2Qjs7Ozs7SUFJM0IsNkJBQXNCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgSHR0cENsaWVudCB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcbmltcG9ydCB7IFJvdXRlciB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XG5cbmltcG9ydCB7IE9ic2VydmFibGUsIG9mIH0gZnJvbSAncnhqcyc7XG5pbXBvcnQgeyBjYXRjaEVycm9yLCB0YXAgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XG5pbXBvcnQgeyBEYXRhU2VydmljZSB9IGZyb20gJy4vZGF0YS5zZXJ2aWNlJztcblxuQEluamVjdGFibGUoKVxuZXhwb3J0IGNsYXNzIEF1dGhTZXJ2aWNlIGV4dGVuZHMgRGF0YVNlcnZpY2Uge1xuICBwcml2YXRlIGxvZ2luUGF0aCA9ICdsb2dpbic7XG4gIHByaXZhdGUgbG9nb3V0UGF0aCA9ICdsb2dvdXQnO1xuXG4gIHByaXZhdGUgdXNlckluZm86IGFueSA9IG51bGw7XG5cbiAgY29uc3RydWN0b3IoXG4gICAgaHR0cDogSHR0cENsaWVudCxcbiAgICBwcml2YXRlIHJvdXRlcjogUm91dGVyXG4gICkge1xuICAgIHN1cGVyKGh0dHApO1xuICB9XG5cbiAgcHVibGljIGxvZ2luKHVzZXJuYW1lOiBzdHJpbmcsIHBhc3N3b3JkOiBzdHJpbmcpOiBPYnNlcnZhYmxlPGFueT4ge1xuICAgIC8vIHRva2VuIHNwZWNpZmljIGhlYWRlciwgd2lsbCBuZWVkIHRvIGJlIGFsdGVyZWQgZm9yIHVzYWdlXG4gICAgY29uc3QgaGRyID0ge1xuICAgICAgJ0F1dGhvcml6YXRpb24nOiAnQmFzaWMgJyArIHdpbmRvdy5idG9hKHVzZXJuYW1lICsgJzonICsgcGFzc3dvcmQpXG4gICAgfTtcblxuICAgIHJldHVybiBzdXBlci5nZXQodGhpcy5sb2dpblBhdGgsIGhkcilcbiAgICAgICAgICAgICAgICAucGlwZShcbiAgICAgICAgICAgICAgICAgIHRhcChyZXMgPT4ge1xuICAgICAgICAgICAgICAgICAgICBpZiAoIHJlcyApIHtcbiAgICAgICAgICAgICAgICAgICAgICB0aGlzLnVzZXJJbmZvID0gcmVzO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICB9KSxcbiAgICAgICAgICAgICAgICAgIGNhdGNoRXJyb3IoICgpID0+IG9mKG51bGwpIClcbiAgICAgICAgICAgICAgICApO1xuICB9XG5cbiAgcHVibGljIGxvZ291dCgpOiBPYnNlcnZhYmxlPGFueT4ge1xuICAgIHRoaXMudXNlckluZm8gPSBudWxsO1xuICAgIHRoaXMucm91dGVyLm5hdmlnYXRlKFsnL2xvZ2luJ10pO1xuICAgIHJldHVybiB0aGlzLnBvc3QodGhpcy5sb2dvdXRQYXRoLCBudWxsKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBDYWxsZWQgd2hlbiBhIDQwMSBzdGF0dXMgaXMgZW5jb3VudGVyZWQgd2hpY2ggbWVhbnMgdXNlciBpcyBsb2dnZWQgb3V0XG4gICAqL1xuICBwdWJsaWMgb240MDFFcnJvcigpOiB2b2lkIHtcbiAgICB0aGlzLnVzZXJJbmZvID0gbnVsbDtcbiAgICB0aGlzLnJvdXRlci5uYXZpZ2F0ZShbJy9sb2dpbiddKTtcbiAgfVxuXG4gIHB1YmxpYyBnZXRVc2VyKCk6IE9ic2VydmFibGU8YW55PiB7XG4gICAgLy8gQ2hlY2sgb3VyIGNhY2hlXG4gICAgaWYgKCB0aGlzLnVzZXJJbmZvICkge1xuICAgICAgcmV0dXJuIG9mKCB0aGlzLnVzZXJJbmZvICk7XG4gICAgfVxuXG4gICAgLy8gVHJ5IHRvIGdldCBpdCBmcm9tIHRoZSBzZXJ2ZXJcbiAgICByZXR1cm4gc3VwZXIuZ2V0KHRoaXMubG9naW5QYXRoKTtcbiAgfVxuXG4gIC8vIFRPRE86IE1ha2UgdGhlc2UgcGVybWlzc2lvbnMgYW4gRW51bVxuICAvLyBUT0RPOiBJcyB0aGlzIGxpc3QgY29tcGxldGU/XG4gIC8qIFBlcm1pc3Npb25zIGZyb20gbGVnYWN5IHNhcGhpcmFcbiAgICAgQ1JFQVRFLFxuICAgICBSRUFELFxuICAgICBVUERBVEUsXG4gICAgIERFTEVURSxcbiAgICAgTVNBLFxuICAgICBJTVBPUlRfQ1NWLCAvL09sZFxuICAgICBTTkFQU0hPVCwgLy9JbXBvcnRcbiAgICAgRVhQT1JUX0ZVTEwsIC8vRXhwb3J0aW5nIHN1cnZleSBzZXQgb25seVxuICAgICBFWFBPUlRfVFJBSkVDVE9SWSwgLy9FeHBvcnRpbmcgdHJhamVjdG9yeSBvbmx5XG4gICAgIEVYUE9SVF9NSU5JTUFMLCAvL05vdCB1c2VkIHlldCwga2VwdCBmb3IgZnV0dXJlIHVzZSAoc3VydmV5c2V0IGV4cG9ydCB3aXRob3V0IG1zYSBzaXhheGlzIGRhdGE/KVxuICAgICBNRVRBREFUQSxcbiAgICAgTVdEX1JFQURFUixcbiAgICAgQVRUQUNITUVOVCxcbiAgICAgUUNfUExPVFMsXG4gICAgIFFDX1JBVyxcbiAgICAgUUNfQ09SUixcbiAgICAgUUNfVE9MLFxuICAgICBRQ19JTkNBWklfQ0FMQyxcbiAgICAgUUNfSU5DQVpJX0NPUlIsXG4gICAgIFFDX1ZBTElEQVRJT05fRVJST1JTLFxuICAgICBBVVRPTUFUSU9OLFxuICAgICBTSElGVF9OT1RFUyxcbiAgICAgQ0hBVCxcbiAgICAgTU9ESUZZX1BFUk1JU1NJT05TLFxuICAgICBBQ1RJVklUWV9MT0csXG4gICAgIERBVEFfVklFVyxcbiAgICAgQ09PUkRfU1lTVEVNLFxuICAgICBOT1RJRklDQVRJT05TLFxuICAgICBBUElfU1VSVkVZX1JFQUQsXG4gICAgIEFQSV9TVVJWRVlfU1VCTUlUO1xuICAqL1xuXG4gIC8vIFRPRE86IG1vdmUgaW50byB1c2VyIG1vZGVsXG4gIC8vIFRPRE86IHRoaXMgaXNuJ3QgcXVpdGUgY29ycmVjdFxuICBwdWJsaWMgdXNlcmhhc1Blcm1pc3Npb24ocGVybTJNYXRjaDogc3RyaW5nKTogYm9vbGVhbiB7XG4gICAgY29uc3QgaGFzQXV0aCA9IHRoaXMudXNlckluZm9bJ2F1dGhvcml0aWVzJ10uZmlsdGVyKChhdXRoKSA9PiB7XG4gICAgICByZXR1cm4gYXV0aFsncm9sZSddID09PSAnUk9MRV9NQUdWQVJfU1VQRVJBRE1JTicgfHxcbiAgICAgICAgYXV0aFsncm9sZSddID09PSAnUk9MRV9jcm93ZC1hZG1pbmlzdHJhdG9ycycgfHxcbiAgICAgICAgYXV0aFsncm9sZSddID09PSAnUk9MRV9NQUdWQVJfREVWRUxPUEVSJyB8fFxuICAgICAgICBhdXRoWydyb2xlJ10uaW5kZXhPZihwZXJtMk1hdGNoKSA+IC0xO1xuICAgIH0pO1xuICAgIHJldHVybiBoYXNBdXRoLmxlbmd0aCA+IDA7XG4gIH1cblxuICAvLyBUT0RPOiBtb3ZlIGludG8gbW9kZWxcbiAgcHVibGljIHVzZXJJc0N1c3RvbWVyKCk6IGJvb2xlYW4ge1xuICAgIGNvbnN0IGhhc0F1dGggPSB0aGlzLnVzZXJJbmZvWydhdXRob3JpdGllcyddLmZpbHRlcigoYXV0aCkgPT4ge1xuICAgICAgcmV0dXJuIGF1dGhbJ3JvbGUnXSA9PT0gJ1JPTEVfUmlnJyB8fCBhdXRoWydyb2xlJ10gPT09ICdST0xFX09wZXJhdG9ycyc7XG4gICAgfSk7XG4gICAgcmV0dXJuIGhhc0F1dGgubGVuZ3RoID4gMDtcbiAgfVxufVxuIl19