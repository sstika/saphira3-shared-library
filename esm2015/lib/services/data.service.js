/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { isNullOrUndefined } from 'util';
import * as _ from 'lodash';
import * as moment_ from 'moment';
/*
 * NOTES
 *   Using withCredentials is a huge security no-no!
 *
 *   It leaks the user's username and password with every call, and while
 *   using HTTPS help mitigate the problem, it is generally regarded as a
 *   bad practice.
 *
 *   We would need to fix the back end to patch this vulernability.
 */
export class DataService {
    /**
     * @param {?} http
     */
    constructor(http) {
        this.http = http;
        this.urlRoot = environment.apiRoot + '/saphira/api/';
    }
    /**
     * This function is for extracting the usable value from GET,POST,PUT,DELETE Response.
     * @protected
     * @param {?} res - {any} the response object (possibly) from a call;
     * @return {?} Observable<any> -  Returns an Observable<any> .
     */
    extractData(res) {
        if (!res) {
            return null;
        }
        if (res.body === undefined) {
            return res;
        }
        return res.body;
    }
    /**
     * Display a notification that informs the user that an HTTP error occurred.
     * Must be consumed by the child service, does NOT get called by default.
     * @protected
     * @param {?} msg User friendly message to display to the user
     * @return {?}
     */
    displayErrorMsg(msg) {
        // TODO: this is a placeholder, a snackbar is what should be used
    }
    /**
     * Sends a verbose error message to the console for further debugging
     * @protected
     * @param {?} error - HttpErrorResponse error object that is supplied via HttpClient
     * @param {?} method - the HTTP method used
     * @return {?} `HttpError` that was passed in or a placeholder if none was provided
     */
    logError(error, method) {
        if (isNullOrUndefined(error) || _.isEmpty(error)) {
            /** @type {?} */
            const err = new Error('An unknown error occurred and no error was supplied to the handler');
            error = new HttpErrorResponse({ error: err });
        }
        /** @type {?} */
        const message = error.message || 'An error occurred and no message was supplied to the handler';
        /** @type {?} */
        const url = error.url || '???';
        // TODO: if we get a lot of ???'s we can pass in the URL
        /** @type {?} */
        const status = error.status || '???';
        /** @type {?} */
        const statusText = error.statusText || '???';
        /** @type {?} */
        const headers = this.headers2str(error.headers) || '???';
        /** @type {?} */
        const stack = error.error.stack || 'No Call Stack Provided';
        /** @type {?} */
        const moment = moment_;
        // TODO: remote logging would go here!
        // Error Message here!
        // Context: http://localhost:4200/#/
        // Action:  GET /saphira/api/ping
        // Status:  400 statusText
        // Time:    2019-04-23T15:28:00+8
        // Headers: {foo: bar}
        // call stack
        // <this line intentionally left blank>
        console.error(message + '\n' +
            `Context: ${window.location.href}\n` +
            `Action:  ${method} ${url}\n` +
            `Status:  ${status} ${statusText}\n` +
            `Time:    ${moment().toISOString(true)}\n` +
            `Headers: ${headers}\n` +
            stack + '\n\n');
        return throwError(error);
    }
    /**
     * Maps `HttpHeaders` to simple JSON string for debugging
     * @private
     * @param {?} headers
     * @return {?}
     */
    headers2str(headers) {
        if (isNullOrUndefined(headers)) {
            return null;
        }
        /** @type {?} */
        const json = {};
        for (const key of headers.keys()) {
            /** @type {?} */
            const values = headers.getAll(key);
            if (values.length === 1) {
                json[key] = values[0];
                continue;
            }
            json[key] = [...values];
        }
        return JSON.stringify(json, null, 2);
    }
    /**
     * Master GET function.
     * @protected
     * @param {?} path - string path of the endpoint;
     * @param {?=} headers - custom headers supplied to the call;
     * @return {?} Observable<any> -  Returns an Observable<any> .
     */
    get(path, headers) {
        /** @type {?} */
        const options = { withCredentials: true };
        if (headers) {
            options.headers = headers;
        }
        return this.http
            .get(this.urlRoot + path, options)
            .pipe(map(this.extractData), catchError((/**
         * @param {?} err
         * @return {?}
         */
        err => this.logError(err, 'GET'))));
    }
    /**
     * Master PUT function.
     * @protected
     * @param {?} path - string path of the endpoint;
     * @param {?} obj - object for which you want to put;
     * @param {?=} headers - custom headers supplied to the call;
     * @return {?} Observable<any> -  Returns an Observable<any> .
     */
    put(path, obj, headers) {
        /** @type {?} */
        const options = { withCredentials: true };
        if (headers) {
            options.headers = headers;
        }
        return this.http
            .put(this.urlRoot + path, obj, options)
            .pipe(map(this.extractData), catchError((/**
         * @param {?} err
         * @return {?}
         */
        err => this.logError(err, 'PUT'))));
    }
    /**
     * Master POST function.
     * @protected
     * @param {?} path - string path of the endpoint;
     * @param {?} obj - object for which you want to put;
     * @param {?=} headers - custom headers supplied to the call;
     * @return {?} Observable<any> -  Returns an Observable<any> .
     */
    post(path, obj, headers) {
        /** @type {?} */
        const options = { withCredentials: true };
        if (headers) {
            options.headers = headers;
        }
        return this.http
            .post(this.urlRoot + path, obj, options)
            .pipe(map(this.extractData), map((/**
         * @param {?} res
         * @return {?}
         */
        res => res || isNullOrUndefined(res))), // TODO: Need to investigate further
        catchError((/**
         * @param {?} err
         * @return {?}
         */
        err => this.logError(err, 'POST'))));
    }
    /**
     * Master DELETE function.
     * @protected
     * @param {?} path - string path of the endpoint;
     * @param {?=} headers - custom headers supplied to the call;
     * @return {?} Observable<any> -  Returns an Observable<any> .
     */
    delete(path, headers) {
        /** @type {?} */
        const options = { withCredentials: true };
        if (headers) {
            options.headers = headers;
        }
        return this.http
            .delete(this.urlRoot + path, options)
            .pipe(map(this.extractData), map((/**
         * @param {?} res
         * @return {?}
         */
        res => res || isNullOrUndefined(res))), // TODO: Need to investigate further
        catchError((/**
         * @param {?} err
         * @return {?}
         */
        err => this.logError(err, 'DELETE'))));
    }
}
DataService.decorators = [
    { type: Injectable }
];
/** @nocollapse */
DataService.ctorParameters = () => [
    { type: HttpClient }
];
if (false) {
    /**
     * @type {?}
     * @protected
     */
    DataService.prototype.urlRoot;
    /**
     * @type {?}
     * @protected
     */
    DataService.prototype.http;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGF0YS5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGhwdGVjaC9zYXBoaXJhLXNoYXJlZC1saWIvIiwic291cmNlcyI6WyJsaWIvc2VydmljZXMvZGF0YS5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQWMsVUFBVSxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBQzlDLE9BQU8sRUFBRSxVQUFVLEVBQUUsR0FBRyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFDakQsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMzQyxPQUFPLEVBQUUsVUFBVSxFQUFFLGlCQUFpQixFQUFlLE1BQU0sc0JBQXNCLENBQUM7QUFDbEYsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLGdDQUFnQyxDQUFDO0FBQzdELE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUN6QyxPQUFPLEtBQUssQ0FBQyxNQUFNLFFBQVEsQ0FBQztBQUM1QixPQUFPLEtBQUssT0FBTyxNQUFNLFFBQVEsQ0FBQzs7Ozs7Ozs7Ozs7QUFjbEMsTUFBTSxPQUFPLFdBQVc7Ozs7SUFHdEIsWUFBdUIsSUFBZ0I7UUFBaEIsU0FBSSxHQUFKLElBQUksQ0FBWTtRQUY3QixZQUFPLEdBQUcsV0FBVyxDQUFDLE9BQU8sR0FBRyxlQUFlLENBQUM7SUFFZCxDQUFDOzs7Ozs7O0lBT25DLFdBQVcsQ0FBQyxHQUFRO1FBQzVCLElBQUssQ0FBQyxHQUFHLEVBQUc7WUFDVixPQUFPLElBQUksQ0FBQztTQUNiO1FBRUQsSUFBSyxHQUFHLENBQUMsSUFBSSxLQUFLLFNBQVMsRUFBRztZQUM1QixPQUFPLEdBQUcsQ0FBQztTQUNaO1FBRUQsT0FBTyxHQUFHLENBQUMsSUFBSSxDQUFDO0lBQ2xCLENBQUM7Ozs7Ozs7O0lBT1MsZUFBZSxDQUFDLEdBQVc7UUFDbkMsaUVBQWlFO0lBQ25FLENBQUM7Ozs7Ozs7O0lBUVMsUUFBUSxDQUNSLEtBQXdCLEVBQ3hCLE1BQXlDO1FBRWpELElBQUssaUJBQWlCLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsRUFBRzs7a0JBQzVDLEdBQUcsR0FBRyxJQUFJLEtBQUssQ0FDbkIsb0VBQW9FLENBQ3JFO1lBQ0QsS0FBSyxHQUFHLElBQUksaUJBQWlCLENBQUMsRUFBRSxLQUFLLEVBQUUsR0FBRyxFQUFFLENBQUMsQ0FBQztTQUMvQzs7Y0FFSyxPQUFPLEdBQUcsS0FBSyxDQUFDLE9BQU8sSUFBSSw4REFBOEQ7O2NBQ3pGLEdBQUcsR0FBRyxLQUFLLENBQUMsR0FBRyxJQUFJLEtBQUs7OztjQUN4QixNQUFNLEdBQUcsS0FBSyxDQUFDLE1BQU0sSUFBSSxLQUFLOztjQUM5QixVQUFVLEdBQUcsS0FBSyxDQUFDLFVBQVUsSUFBSSxLQUFLOztjQUN0QyxPQUFPLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksS0FBSzs7Y0FDbEQsS0FBSyxHQUFHLEtBQUssQ0FBQyxLQUFLLENBQUMsS0FBSyxJQUFJLHdCQUF3Qjs7Y0FDckQsTUFBTSxHQUFHLE9BQU87UUFHdEIsc0NBQXNDO1FBRXRDLHNCQUFzQjtRQUN0QixvQ0FBb0M7UUFDcEMsaUNBQWlDO1FBQ2pDLDBCQUEwQjtRQUMxQixpQ0FBaUM7UUFDakMsc0JBQXNCO1FBQ3RCLGFBQWE7UUFDYix1Q0FBdUM7UUFFdkMsT0FBTyxDQUFDLEtBQUssQ0FDWCxPQUFPLEdBQUcsSUFBSTtZQUNkLFlBQVksTUFBTSxDQUFDLFFBQVEsQ0FBQyxJQUFJLElBQUk7WUFDcEMsWUFBWSxNQUFNLElBQUksR0FBRyxJQUFJO1lBQzdCLFlBQVksTUFBTSxJQUFJLFVBQVUsSUFBSTtZQUNwQyxZQUFZLE1BQU0sRUFBRSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSTtZQUMxQyxZQUFZLE9BQU8sSUFBSTtZQUN2QixLQUFLLEdBQUksTUFBTSxDQUNoQixDQUFDO1FBRUYsT0FBTyxVQUFVLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDM0IsQ0FBQzs7Ozs7OztJQUtPLFdBQVcsQ0FBRSxPQUFvQjtRQUN2QyxJQUFLLGlCQUFpQixDQUFDLE9BQU8sQ0FBQyxFQUFHO1lBQ2hDLE9BQU8sSUFBSSxDQUFDO1NBQ2I7O2NBRUssSUFBSSxHQUFHLEVBQUU7UUFDZixLQUFNLE1BQU0sR0FBRyxJQUFJLE9BQU8sQ0FBQyxJQUFJLEVBQUUsRUFBRzs7a0JBQzVCLE1BQU0sR0FBRyxPQUFPLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQztZQUNsQyxJQUFLLE1BQU0sQ0FBQyxNQUFNLEtBQUssQ0FBQyxFQUFHO2dCQUN6QixJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUN0QixTQUFTO2FBQ1Y7WUFFRCxJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBRSxHQUFHLE1BQU0sQ0FBRSxDQUFDO1NBQzNCO1FBR0QsT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDLENBQUM7SUFDdkMsQ0FBQzs7Ozs7Ozs7SUFRUyxHQUFHLENBQ0QsSUFBWSxFQUNaLE9BQW1DOztjQUV2QyxPQUFPLEdBQVEsRUFBRSxlQUFlLEVBQUUsSUFBSSxFQUFFO1FBRTlDLElBQUssT0FBTyxFQUFHO1lBQ2IsT0FBTyxDQUFDLE9BQU8sR0FBRyxPQUFPLENBQUM7U0FDM0I7UUFFRCxPQUFPLElBQUksQ0FBQyxJQUFJO2FBQ2IsR0FBRyxDQUFDLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxFQUFFLE9BQU8sQ0FBQzthQUNqQyxJQUFJLENBQ0gsR0FBRyxDQUFFLElBQUksQ0FBQyxXQUFXLENBQUUsRUFDdkIsVUFBVTs7OztRQUFFLEdBQUcsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLEVBQUUsS0FBSyxDQUFDLEVBQUUsQ0FDL0MsQ0FBQztJQUNOLENBQUM7Ozs7Ozs7OztJQVNTLEdBQUcsQ0FDRCxJQUFZLEVBQ1osR0FBUSxFQUNSLE9BQW1DOztjQUV2QyxPQUFPLEdBQVEsRUFBRSxlQUFlLEVBQUUsSUFBSSxFQUFFO1FBRTlDLElBQUssT0FBTyxFQUFHO1lBQ2IsT0FBTyxDQUFDLE9BQU8sR0FBRyxPQUFPLENBQUM7U0FDM0I7UUFFRCxPQUFPLElBQUksQ0FBQyxJQUFJO2FBQ2IsR0FBRyxDQUFDLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxFQUFFLEdBQUcsRUFBRSxPQUFPLENBQUM7YUFDdEMsSUFBSSxDQUNILEdBQUcsQ0FBRSxJQUFJLENBQUMsV0FBVyxDQUFFLEVBQ3ZCLFVBQVU7Ozs7UUFBRSxHQUFHLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxFQUFFLEtBQUssQ0FBQyxFQUFFLENBQy9DLENBQUM7SUFDTixDQUFDOzs7Ozs7Ozs7SUFTUyxJQUFJLENBQ0YsSUFBWSxFQUNaLEdBQVEsRUFDUixPQUFtQzs7Y0FFdkMsT0FBTyxHQUFRLEVBQUUsZUFBZSxFQUFFLElBQUksRUFBRTtRQUU5QyxJQUFLLE9BQU8sRUFBRztZQUNiLE9BQU8sQ0FBQyxPQUFPLEdBQUcsT0FBTyxDQUFDO1NBQzNCO1FBRUQsT0FBTyxJQUFJLENBQUMsSUFBSTthQUNiLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksRUFBRSxHQUFHLEVBQUUsT0FBTyxDQUFDO2FBQ3ZDLElBQUksQ0FDSCxHQUFHLENBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBRSxFQUN2QixHQUFHOzs7O1FBQUUsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksaUJBQWlCLENBQUMsR0FBRyxDQUFDLEVBQUUsRUFBRSxvQ0FBb0M7UUFDakYsVUFBVTs7OztRQUFFLEdBQUcsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLEVBQUUsTUFBTSxDQUFDLEVBQUUsQ0FDaEQsQ0FBQztJQUNOLENBQUM7Ozs7Ozs7O0lBUVMsTUFBTSxDQUNKLElBQVksRUFDWixPQUFtQzs7Y0FFdkMsT0FBTyxHQUFRLEVBQUUsZUFBZSxFQUFFLElBQUksRUFBRTtRQUU5QyxJQUFLLE9BQU8sRUFBRztZQUNiLE9BQU8sQ0FBQyxPQUFPLEdBQUcsT0FBTyxDQUFDO1NBQzNCO1FBRUQsT0FBTyxJQUFJLENBQUMsSUFBSTthQUNiLE1BQU0sQ0FBQyxJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksRUFBRSxPQUFPLENBQUU7YUFDckMsSUFBSSxDQUNILEdBQUcsQ0FBRSxJQUFJLENBQUMsV0FBVyxDQUFFLEVBQ3ZCLEdBQUc7Ozs7UUFBRSxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxpQkFBaUIsQ0FBQyxHQUFHLENBQUMsRUFBRSxFQUFFLG9DQUFvQztRQUNqRixVQUFVOzs7O1FBQUUsR0FBRyxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsRUFBRSxRQUFRLENBQUMsRUFBRSxDQUNsRCxDQUFDO0lBQ04sQ0FBQzs7O1lBN01GLFVBQVU7Ozs7WUFqQkYsVUFBVTs7Ozs7OztJQW1CakIsOEJBQTBEOzs7OztJQUU3QywyQkFBMEIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBPYnNlcnZhYmxlLCB0aHJvd0Vycm9yIH0gZnJvbSAncnhqcyc7XG5pbXBvcnQgeyBjYXRjaEVycm9yLCBtYXAgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XG5pbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBIdHRwQ2xpZW50LCBIdHRwRXJyb3JSZXNwb25zZSwgSHR0cEhlYWRlcnMgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XG5pbXBvcnQgeyBlbnZpcm9ubWVudCB9IGZyb20gJy4uLy4uL2Vudmlyb25tZW50cy9lbnZpcm9ubWVudCc7XG5pbXBvcnQgeyBpc051bGxPclVuZGVmaW5lZCB9IGZyb20gJ3V0aWwnO1xuaW1wb3J0ICogYXMgXyBmcm9tICdsb2Rhc2gnO1xuaW1wb3J0ICogYXMgbW9tZW50XyBmcm9tICdtb21lbnQnO1xuXG4vKlxuICogTk9URVNcbiAqICAgVXNpbmcgd2l0aENyZWRlbnRpYWxzIGlzIGEgaHVnZSBzZWN1cml0eSBuby1ubyFcbiAqXG4gKiAgIEl0IGxlYWtzIHRoZSB1c2VyJ3MgdXNlcm5hbWUgYW5kIHBhc3N3b3JkIHdpdGggZXZlcnkgY2FsbCwgYW5kIHdoaWxlXG4gKiAgIHVzaW5nIEhUVFBTIGhlbHAgbWl0aWdhdGUgdGhlIHByb2JsZW0sIGl0IGlzIGdlbmVyYWxseSByZWdhcmRlZCBhcyBhXG4gKiAgIGJhZCBwcmFjdGljZS5cbiAqXG4gKiAgIFdlIHdvdWxkIG5lZWQgdG8gZml4IHRoZSBiYWNrIGVuZCB0byBwYXRjaCB0aGlzIHZ1bGVybmFiaWxpdHkuXG4gKi9cblxuQEluamVjdGFibGUoKVxuZXhwb3J0IGNsYXNzIERhdGFTZXJ2aWNlIHtcbiAgcHJvdGVjdGVkIHVybFJvb3QgPSBlbnZpcm9ubWVudC5hcGlSb290ICsgJy9zYXBoaXJhL2FwaS8nO1xuXG4gIGNvbnN0cnVjdG9yKCBwcm90ZWN0ZWQgaHR0cDogSHR0cENsaWVudCApIHsgfVxuXG4gIC8qKlxuICAgKiBUaGlzIGZ1bmN0aW9uIGlzIGZvciBleHRyYWN0aW5nIHRoZSB1c2FibGUgdmFsdWUgZnJvbSBHRVQsUE9TVCxQVVQsREVMRVRFIFJlc3BvbnNlLlxuICAgKiBAcGFyYW0gIHJlcyAtIHthbnl9IHRoZSByZXNwb25zZSBvYmplY3QgKHBvc3NpYmx5KSBmcm9tIGEgY2FsbDtcbiAgICogQHJldHVybnMgT2JzZXJ2YWJsZTxhbnk+IC0gIFJldHVybnMgYW4gT2JzZXJ2YWJsZTxhbnk+IC5cbiAgICovXG4gIHByb3RlY3RlZCBleHRyYWN0RGF0YShyZXM6IGFueSk6IGFueSB7XG4gICAgaWYgKCAhcmVzICkge1xuICAgICAgcmV0dXJuIG51bGw7XG4gICAgfVxuXG4gICAgaWYgKCByZXMuYm9keSA9PT0gdW5kZWZpbmVkICkge1xuICAgICAgcmV0dXJuIHJlcztcbiAgICB9XG5cbiAgICByZXR1cm4gcmVzLmJvZHk7XG4gIH1cblxuICAvKipcbiAgICogRGlzcGxheSBhIG5vdGlmaWNhdGlvbiB0aGF0IGluZm9ybXMgdGhlIHVzZXIgdGhhdCBhbiBIVFRQIGVycm9yIG9jY3VycmVkLlxuICAgKiBNdXN0IGJlIGNvbnN1bWVkIGJ5IHRoZSBjaGlsZCBzZXJ2aWNlLCBkb2VzIE5PVCBnZXQgY2FsbGVkIGJ5IGRlZmF1bHQuXG4gICAqIEBwYXJhbSBtc2cgVXNlciBmcmllbmRseSBtZXNzYWdlIHRvIGRpc3BsYXkgdG8gdGhlIHVzZXJcbiAgICovXG4gIHByb3RlY3RlZCBkaXNwbGF5RXJyb3JNc2cobXNnOiBzdHJpbmcpOiB2b2lkIHtcbiAgICAvLyBUT0RPOiB0aGlzIGlzIGEgcGxhY2Vob2xkZXIsIGEgc25hY2tiYXIgaXMgd2hhdCBzaG91bGQgYmUgdXNlZFxuICB9XG5cbiAgLyoqXG4gICAqIFNlbmRzIGEgdmVyYm9zZSBlcnJvciBtZXNzYWdlIHRvIHRoZSBjb25zb2xlIGZvciBmdXJ0aGVyIGRlYnVnZ2luZ1xuICAgKiBAcGFyYW0gIGVycm9yIC0gSHR0cEVycm9yUmVzcG9uc2UgZXJyb3Igb2JqZWN0IHRoYXQgaXMgc3VwcGxpZWQgdmlhIEh0dHBDbGllbnRcbiAgICogQHBhcmFtIG1ldGhvZCAtIHRoZSBIVFRQIG1ldGhvZCB1c2VkXG4gICAqIEByZXR1cm5zIGBIdHRwRXJyb3JgIHRoYXQgd2FzIHBhc3NlZCBpbiBvciBhIHBsYWNlaG9sZGVyIGlmIG5vbmUgd2FzIHByb3ZpZGVkXG4gICAqL1xuICBwcm90ZWN0ZWQgbG9nRXJyb3IoXG4gICAgICAgICAgICBlcnJvcjogSHR0cEVycm9yUmVzcG9uc2UsXG4gICAgICAgICAgICBtZXRob2Q6ICdHRVQnIHwgJ1BPU1QnIHwgJ1BVVCcgfCAnREVMRVRFJ1xuICAgICAgICAgICk6IE9ic2VydmFibGU8SHR0cEVycm9yUmVzcG9uc2U+IHtcbiAgICBpZiAoIGlzTnVsbE9yVW5kZWZpbmVkKGVycm9yKSB8fCBfLmlzRW1wdHkoZXJyb3IpICkge1xuICAgICAgY29uc3QgZXJyID0gbmV3IEVycm9yKFxuICAgICAgICAnQW4gdW5rbm93biBlcnJvciBvY2N1cnJlZCBhbmQgbm8gZXJyb3Igd2FzIHN1cHBsaWVkIHRvIHRoZSBoYW5kbGVyJ1xuICAgICAgKTtcbiAgICAgIGVycm9yID0gbmV3IEh0dHBFcnJvclJlc3BvbnNlKHsgZXJyb3I6IGVyciB9KTtcbiAgICB9XG5cbiAgICBjb25zdCBtZXNzYWdlID0gZXJyb3IubWVzc2FnZSB8fCAnQW4gZXJyb3Igb2NjdXJyZWQgYW5kIG5vIG1lc3NhZ2Ugd2FzIHN1cHBsaWVkIHRvIHRoZSBoYW5kbGVyJztcbiAgICBjb25zdCB1cmwgPSBlcnJvci51cmwgfHwgJz8/Pyc7IC8vIFRPRE86IGlmIHdlIGdldCBhIGxvdCBvZiA/Pz8ncyB3ZSBjYW4gcGFzcyBpbiB0aGUgVVJMXG4gICAgY29uc3Qgc3RhdHVzID0gZXJyb3Iuc3RhdHVzIHx8ICc/Pz8nO1xuICAgIGNvbnN0IHN0YXR1c1RleHQgPSBlcnJvci5zdGF0dXNUZXh0IHx8ICc/Pz8nO1xuICAgIGNvbnN0IGhlYWRlcnMgPSB0aGlzLmhlYWRlcnMyc3RyKGVycm9yLmhlYWRlcnMpIHx8ICc/Pz8nO1xuICAgIGNvbnN0IHN0YWNrID0gZXJyb3IuZXJyb3Iuc3RhY2sgfHwgJ05vIENhbGwgU3RhY2sgUHJvdmlkZWQnO1xuICAgIGNvbnN0IG1vbWVudCA9IG1vbWVudF87XG5cblxuICAgIC8vIFRPRE86IHJlbW90ZSBsb2dnaW5nIHdvdWxkIGdvIGhlcmUhXG5cbiAgICAvLyBFcnJvciBNZXNzYWdlIGhlcmUhXG4gICAgLy8gQ29udGV4dDogaHR0cDovL2xvY2FsaG9zdDo0MjAwLyMvXG4gICAgLy8gQWN0aW9uOiAgR0VUIC9zYXBoaXJhL2FwaS9waW5nXG4gICAgLy8gU3RhdHVzOiAgNDAwIHN0YXR1c1RleHRcbiAgICAvLyBUaW1lOiAgICAyMDE5LTA0LTIzVDE1OjI4OjAwKzhcbiAgICAvLyBIZWFkZXJzOiB7Zm9vOiBiYXJ9XG4gICAgLy8gY2FsbCBzdGFja1xuICAgIC8vIDx0aGlzIGxpbmUgaW50ZW50aW9uYWxseSBsZWZ0IGJsYW5rPlxuXG4gICAgY29uc29sZS5lcnJvcihcbiAgICAgIG1lc3NhZ2UgKyAnXFxuJyArXG4gICAgICBgQ29udGV4dDogJHt3aW5kb3cubG9jYXRpb24uaHJlZn1cXG5gICtcbiAgICAgIGBBY3Rpb246ICAke21ldGhvZH0gJHt1cmx9XFxuYCArXG4gICAgICBgU3RhdHVzOiAgJHtzdGF0dXN9ICR7c3RhdHVzVGV4dH1cXG5gICtcbiAgICAgIGBUaW1lOiAgICAke21vbWVudCgpLnRvSVNPU3RyaW5nKHRydWUpfVxcbmAgK1xuICAgICAgYEhlYWRlcnM6ICR7aGVhZGVyc31cXG5gICtcbiAgICAgIHN0YWNrICsgICdcXG5cXG4nXG4gICAgKTtcblxuICAgIHJldHVybiB0aHJvd0Vycm9yKGVycm9yKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBNYXBzIGBIdHRwSGVhZGVyc2AgdG8gc2ltcGxlIEpTT04gc3RyaW5nIGZvciBkZWJ1Z2dpbmdcbiAgICovXG4gIHByaXZhdGUgaGVhZGVyczJzdHIoIGhlYWRlcnM6IEh0dHBIZWFkZXJzICk6IHN0cmluZyB7XG4gICAgaWYgKCBpc051bGxPclVuZGVmaW5lZChoZWFkZXJzKSApIHtcbiAgICAgIHJldHVybiBudWxsO1xuICAgIH1cblxuICAgIGNvbnN0IGpzb24gPSB7fTtcbiAgICBmb3IgKCBjb25zdCBrZXkgb2YgaGVhZGVycy5rZXlzKCkgKSB7XG4gICAgICBjb25zdCB2YWx1ZXMgPSBoZWFkZXJzLmdldEFsbChrZXkpO1xuICAgICAgaWYgKCB2YWx1ZXMubGVuZ3RoID09PSAxICkge1xuICAgICAgICBqc29uW2tleV0gPSB2YWx1ZXNbMF07XG4gICAgICAgIGNvbnRpbnVlO1xuICAgICAgfVxuXG4gICAgICBqc29uW2tleV0gPSBbIC4uLnZhbHVlcyBdO1xuICAgIH1cblxuXG4gICAgcmV0dXJuIEpTT04uc3RyaW5naWZ5KGpzb24sIG51bGwsIDIpO1xuICB9XG5cbiAgLyoqXG4gICAqIE1hc3RlciBHRVQgZnVuY3Rpb24uXG4gICAqIEBwYXJhbSAgcGF0aCAtIHN0cmluZyBwYXRoIG9mIHRoZSBlbmRwb2ludDtcbiAgICogQHBhcmFtICBoZWFkZXJzIC0gY3VzdG9tIGhlYWRlcnMgc3VwcGxpZWQgdG8gdGhlIGNhbGw7XG4gICAqIEByZXR1cm5zIE9ic2VydmFibGU8YW55PiAtICBSZXR1cm5zIGFuIE9ic2VydmFibGU8YW55PiAuXG4gICAqL1xuICBwcm90ZWN0ZWQgZ2V0KFxuICAgICAgICAgICAgICBwYXRoOiBzdHJpbmcsXG4gICAgICAgICAgICAgIGhlYWRlcnM/OiB7IFtrZXk6IHN0cmluZ106IHN0cmluZyB9XG4gICAgICAgICAgICApOiBPYnNlcnZhYmxlPGFueT4ge1xuICAgIGNvbnN0IG9wdGlvbnM6IGFueSA9IHsgd2l0aENyZWRlbnRpYWxzOiB0cnVlIH07XG5cbiAgICBpZiAoIGhlYWRlcnMgKSB7XG4gICAgICBvcHRpb25zLmhlYWRlcnMgPSBoZWFkZXJzO1xuICAgIH1cblxuICAgIHJldHVybiB0aGlzLmh0dHBcbiAgICAgIC5nZXQodGhpcy51cmxSb290ICsgcGF0aCwgb3B0aW9ucylcbiAgICAgIC5waXBlKFxuICAgICAgICBtYXAoIHRoaXMuZXh0cmFjdERhdGEgKSxcbiAgICAgICAgY2F0Y2hFcnJvciggZXJyID0+IHRoaXMubG9nRXJyb3IoZXJyLCAnR0VUJykgKVxuICAgICAgKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBNYXN0ZXIgUFVUIGZ1bmN0aW9uLlxuICAgKiBAcGFyYW0gIHBhdGggLSBzdHJpbmcgcGF0aCBvZiB0aGUgZW5kcG9pbnQ7XG4gICAqIEBwYXJhbSAgb2JqIC0gb2JqZWN0IGZvciB3aGljaCB5b3Ugd2FudCB0byBwdXQ7XG4gICAqIEBwYXJhbSAgaGVhZGVycyAtIGN1c3RvbSBoZWFkZXJzIHN1cHBsaWVkIHRvIHRoZSBjYWxsO1xuICAgKiBAcmV0dXJucyBPYnNlcnZhYmxlPGFueT4gLSAgUmV0dXJucyBhbiBPYnNlcnZhYmxlPGFueT4gLlxuICAgKi9cbiAgcHJvdGVjdGVkIHB1dChcbiAgICAgICAgICAgICAgcGF0aDogc3RyaW5nLFxuICAgICAgICAgICAgICBvYmo6IGFueSxcbiAgICAgICAgICAgICAgaGVhZGVycz86IHsgW2tleTogc3RyaW5nXTogc3RyaW5nIH1cbiAgICAgICAgICAgICk6IE9ic2VydmFibGU8YW55PiB7XG4gICAgY29uc3Qgb3B0aW9uczogYW55ID0geyB3aXRoQ3JlZGVudGlhbHM6IHRydWUgfTtcblxuICAgIGlmICggaGVhZGVycyApIHtcbiAgICAgIG9wdGlvbnMuaGVhZGVycyA9IGhlYWRlcnM7XG4gICAgfVxuXG4gICAgcmV0dXJuIHRoaXMuaHR0cFxuICAgICAgLnB1dCh0aGlzLnVybFJvb3QgKyBwYXRoLCBvYmosIG9wdGlvbnMpXG4gICAgICAucGlwZShcbiAgICAgICAgbWFwKCB0aGlzLmV4dHJhY3REYXRhICksXG4gICAgICAgIGNhdGNoRXJyb3IoIGVyciA9PiB0aGlzLmxvZ0Vycm9yKGVyciwgJ1BVVCcpIClcbiAgICAgICk7XG4gIH1cblxuICAvKipcbiAgICogTWFzdGVyIFBPU1QgZnVuY3Rpb24uXG4gICAqIEBwYXJhbSAgcGF0aCAtIHN0cmluZyBwYXRoIG9mIHRoZSBlbmRwb2ludDtcbiAgICogQHBhcmFtICBvYmogLSBvYmplY3QgZm9yIHdoaWNoIHlvdSB3YW50IHRvIHB1dDtcbiAgICogQHBhcmFtICBoZWFkZXJzIC0gY3VzdG9tIGhlYWRlcnMgc3VwcGxpZWQgdG8gdGhlIGNhbGw7XG4gICAqIEByZXR1cm5zIE9ic2VydmFibGU8YW55PiAtICBSZXR1cm5zIGFuIE9ic2VydmFibGU8YW55PiAuXG4gICAqL1xuICBwcm90ZWN0ZWQgcG9zdChcbiAgICAgICAgICAgICAgcGF0aDogc3RyaW5nLFxuICAgICAgICAgICAgICBvYmo6IGFueSxcbiAgICAgICAgICAgICAgaGVhZGVycz86IHsgW2tleTogc3RyaW5nXTogc3RyaW5nIH1cbiAgICAgICAgICAgICk6IE9ic2VydmFibGU8YW55PiB7XG4gICAgY29uc3Qgb3B0aW9uczogYW55ID0geyB3aXRoQ3JlZGVudGlhbHM6IHRydWUgfTtcblxuICAgIGlmICggaGVhZGVycyApIHtcbiAgICAgIG9wdGlvbnMuaGVhZGVycyA9IGhlYWRlcnM7XG4gICAgfVxuXG4gICAgcmV0dXJuIHRoaXMuaHR0cFxuICAgICAgLnBvc3QodGhpcy51cmxSb290ICsgcGF0aCwgb2JqLCBvcHRpb25zKVxuICAgICAgLnBpcGUoXG4gICAgICAgIG1hcCggdGhpcy5leHRyYWN0RGF0YSApLFxuICAgICAgICBtYXAoIHJlcyA9PiByZXMgfHwgaXNOdWxsT3JVbmRlZmluZWQocmVzKSApLCAvLyBUT0RPOiBOZWVkIHRvIGludmVzdGlnYXRlIGZ1cnRoZXJcbiAgICAgICAgY2F0Y2hFcnJvciggZXJyID0+IHRoaXMubG9nRXJyb3IoZXJyLCAnUE9TVCcpIClcbiAgICAgICk7XG4gIH1cblxuICAvKipcbiAgICogTWFzdGVyIERFTEVURSBmdW5jdGlvbi5cbiAgICogQHBhcmFtICBwYXRoIC0gc3RyaW5nIHBhdGggb2YgdGhlIGVuZHBvaW50O1xuICAgKiBAcGFyYW0gIGhlYWRlcnMgLSBjdXN0b20gaGVhZGVycyBzdXBwbGllZCB0byB0aGUgY2FsbDtcbiAgICogQHJldHVybnMgT2JzZXJ2YWJsZTxhbnk+IC0gIFJldHVybnMgYW4gT2JzZXJ2YWJsZTxhbnk+IC5cbiAgICovXG4gIHByb3RlY3RlZCBkZWxldGUoXG4gICAgICAgICAgICAgIHBhdGg6IHN0cmluZyxcbiAgICAgICAgICAgICAgaGVhZGVycz86IHsgW2tleTogc3RyaW5nXTogc3RyaW5nIH1cbiAgICAgICAgICAgICk6IE9ic2VydmFibGU8YW55PiB7XG4gICAgY29uc3Qgb3B0aW9uczogYW55ID0geyB3aXRoQ3JlZGVudGlhbHM6IHRydWUgfTtcblxuICAgIGlmICggaGVhZGVycyApIHtcbiAgICAgIG9wdGlvbnMuaGVhZGVycyA9IGhlYWRlcnM7XG4gICAgfVxuXG4gICAgcmV0dXJuIHRoaXMuaHR0cFxuICAgICAgLmRlbGV0ZSh0aGlzLnVybFJvb3QgKyBwYXRoLCBvcHRpb25zIClcbiAgICAgIC5waXBlKFxuICAgICAgICBtYXAoIHRoaXMuZXh0cmFjdERhdGEgKSxcbiAgICAgICAgbWFwKCByZXMgPT4gcmVzIHx8IGlzTnVsbE9yVW5kZWZpbmVkKHJlcykgKSwgLy8gVE9ETzogTmVlZCB0byBpbnZlc3RpZ2F0ZSBmdXJ0aGVyXG4gICAgICAgIGNhdGNoRXJyb3IoIGVyciA9PiB0aGlzLmxvZ0Vycm9yKGVyciwgJ0RFTEVURScpIClcbiAgICAgICk7XG4gIH1cblxufVxuIl19