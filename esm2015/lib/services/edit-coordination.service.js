/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import * as i0 from "@angular/core";
export class EditCoordinationService {
    constructor() {
        this.editStatus = {
            infoCompany: false,
            infoField: false,
            infoPad: false,
            infoWell: false,
            wellboreInfo: false,
            wellboreMagnetic: false,
            trajectoryInfo: false,
            trajectoryTieIn: false,
            trajectorySurveys: false,
            surveySetInfo: false,
            surveySetRigContact: false,
            surveySetGrid: false,
        };
        this.editFieldStatus = new BehaviorSubject(false);
        this.getEditFieldStatus = this.editFieldStatus.asObservable();
    }
    /**
     * @param {?} field
     * @param {?} status
     * @return {?}
     */
    toggleField(field, status) {
        this.editStatus[field] = status;
        this.setEditFieldStatus(this.editStatus);
    }
    /**
     * @return {?}
     */
    areEditFieldsClosed() {
        /** @type {?} */
        let currentlyEdited = 0;
        Object.keys(this.editStatus).forEach((/**
         * @param {?} es
         * @return {?}
         */
        es => {
            if (this.editStatus[es] === true) {
                currentlyEdited++;
            }
        }));
        return currentlyEdited === 0;
    }
    /**
     * @private
     * @param {?} set
     * @return {?}
     */
    setEditFieldStatus(set) {
        this.editFieldStatus.next(set);
    }
}
EditCoordinationService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
EditCoordinationService.ctorParameters = () => [];
/** @nocollapse */ EditCoordinationService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function EditCoordinationService_Factory() { return new EditCoordinationService(); }, token: EditCoordinationService, providedIn: "root" });
if (false) {
    /** @type {?} */
    EditCoordinationService.prototype.editStatus;
    /**
     * @type {?}
     * @private
     */
    EditCoordinationService.prototype.editFieldStatus;
    /** @type {?} */
    EditCoordinationService.prototype.getEditFieldStatus;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZWRpdC1jb29yZGluYXRpb24uc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BocHRlY2gvc2FwaGlyYS1zaGFyZWQtbGliLyIsInNvdXJjZXMiOlsibGliL3NlcnZpY2VzL2VkaXQtY29vcmRpbmF0aW9uLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLE1BQU0sQ0FBQzs7QUFLdkMsTUFBTSxPQUFPLHVCQUF1QjtJQW9CbEM7UUFsQkEsZUFBVSxHQUFHO1lBQ1gsV0FBVyxFQUFFLEtBQUs7WUFDbEIsU0FBUyxFQUFFLEtBQUs7WUFDaEIsT0FBTyxFQUFFLEtBQUs7WUFDZCxRQUFRLEVBQUUsS0FBSztZQUNmLFlBQVksRUFBRSxLQUFLO1lBQ25CLGdCQUFnQixFQUFFLEtBQUs7WUFDdkIsY0FBYyxFQUFFLEtBQUs7WUFDckIsZUFBZSxFQUFFLEtBQUs7WUFDdEIsaUJBQWlCLEVBQUUsS0FBSztZQUN4QixhQUFhLEVBQUUsS0FBSztZQUNwQixtQkFBbUIsRUFBRSxLQUFLO1lBQzFCLGFBQWEsRUFBRSxLQUFLO1NBQ3JCLENBQUM7UUFFTSxvQkFBZSxHQUFHLElBQUksZUFBZSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ3JELHVCQUFrQixHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsWUFBWSxFQUFFLENBQUM7SUFFekMsQ0FBQzs7Ozs7O0lBRWpCLFdBQVcsQ0FBQyxLQUFhLEVBQUUsTUFBZTtRQUN4QyxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxHQUFHLE1BQU0sQ0FBQztRQUNoQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO0lBQzNDLENBQUM7Ozs7SUFFRCxtQkFBbUI7O1lBQ2IsZUFBZSxHQUFHLENBQUM7UUFDdkIsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsT0FBTzs7OztRQUFDLEVBQUUsQ0FBQyxFQUFFO1lBQ3hDLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxFQUFFLENBQUMsS0FBSyxJQUFJLEVBQUU7Z0JBQ2hDLGVBQWUsRUFBRSxDQUFDO2FBQ25CO1FBQ0gsQ0FBQyxFQUFDLENBQUM7UUFDSCxPQUFPLGVBQWUsS0FBSyxDQUFDLENBQUM7SUFDL0IsQ0FBQzs7Ozs7O0lBRU8sa0JBQWtCLENBQUMsR0FBRztRQUM1QixJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztJQUNqQyxDQUFDOzs7WUExQ0YsVUFBVSxTQUFDO2dCQUNWLFVBQVUsRUFBRSxNQUFNO2FBQ25COzs7Ozs7O0lBR0MsNkNBYUU7Ozs7O0lBRUYsa0RBQXFEOztJQUNyRCxxREFBeUQiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBCZWhhdmlvclN1YmplY3QgfSBmcm9tICdyeGpzJztcblxuQEluamVjdGFibGUoe1xuICBwcm92aWRlZEluOiAncm9vdCdcbn0pXG5leHBvcnQgY2xhc3MgRWRpdENvb3JkaW5hdGlvblNlcnZpY2Uge1xuXG4gIGVkaXRTdGF0dXMgPSB7XG4gICAgaW5mb0NvbXBhbnk6IGZhbHNlLFxuICAgIGluZm9GaWVsZDogZmFsc2UsXG4gICAgaW5mb1BhZDogZmFsc2UsXG4gICAgaW5mb1dlbGw6IGZhbHNlLFxuICAgIHdlbGxib3JlSW5mbzogZmFsc2UsXG4gICAgd2VsbGJvcmVNYWduZXRpYzogZmFsc2UsXG4gICAgdHJhamVjdG9yeUluZm86IGZhbHNlLFxuICAgIHRyYWplY3RvcnlUaWVJbjogZmFsc2UsXG4gICAgdHJhamVjdG9yeVN1cnZleXM6IGZhbHNlLFxuICAgIHN1cnZleVNldEluZm86IGZhbHNlLFxuICAgIHN1cnZleVNldFJpZ0NvbnRhY3Q6IGZhbHNlLFxuICAgIHN1cnZleVNldEdyaWQ6IGZhbHNlLFxuICB9O1xuXG4gIHByaXZhdGUgZWRpdEZpZWxkU3RhdHVzID0gbmV3IEJlaGF2aW9yU3ViamVjdChmYWxzZSk7XG4gIGdldEVkaXRGaWVsZFN0YXR1cyA9IHRoaXMuZWRpdEZpZWxkU3RhdHVzLmFzT2JzZXJ2YWJsZSgpO1xuXG4gIGNvbnN0cnVjdG9yKCkgeyB9XG5cbiAgdG9nZ2xlRmllbGQoZmllbGQ6IHN0cmluZywgc3RhdHVzOiBib29sZWFuKTogdm9pZCB7XG4gICAgdGhpcy5lZGl0U3RhdHVzW2ZpZWxkXSA9IHN0YXR1cztcbiAgICB0aGlzLnNldEVkaXRGaWVsZFN0YXR1cyh0aGlzLmVkaXRTdGF0dXMpO1xuICB9XG5cbiAgYXJlRWRpdEZpZWxkc0Nsb3NlZCgpIHtcbiAgICBsZXQgY3VycmVudGx5RWRpdGVkID0gMDtcbiAgICBPYmplY3Qua2V5cyh0aGlzLmVkaXRTdGF0dXMpLmZvckVhY2goZXMgPT4ge1xuICAgICAgaWYgKHRoaXMuZWRpdFN0YXR1c1tlc10gPT09IHRydWUpIHtcbiAgICAgICAgY3VycmVudGx5RWRpdGVkKys7XG4gICAgICB9XG4gICAgfSk7XG4gICAgcmV0dXJuIGN1cnJlbnRseUVkaXRlZCA9PT0gMDtcbiAgfVxuXG4gIHByaXZhdGUgc2V0RWRpdEZpZWxkU3RhdHVzKHNldCkge1xuICAgIHRoaXMuZWRpdEZpZWxkU3RhdHVzLm5leHQoc2V0KTtcbiAgfVxufVxuIl19