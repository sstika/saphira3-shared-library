/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { of, BehaviorSubject } from 'rxjs';
import { catchError, tap, skipWhile } from 'rxjs/operators';
import { DataService } from '../data.service';
import { isNullOrUndefined } from 'util';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common/http";
export class CompanyService extends DataService {
    /**
     * @param {?} http
     */
    constructor(http) {
        super(http);
        this.allCompanies = 'repositories/company/getAll';
        this.getAllUnderCompany = 'repositories/company/getAllUnderCompany';
        this.addComp = 'repositories/company/add';
        this.getOneComp = 'repositories/company/getOne';
        this.deleteComp = 'repositories/company/delete';
        this.companySnapshot = 'utils/getCompanySnapshot';
        this.restoreFromCompanySnapshot = 'utils/restoreFromCompanySnapshot';
        this.downloadAtt = 'attachments/search/findById';
        this.getOneMeta = 'repositories/company/getOneMeta';
        this.update = 'repositories/company/update';
        this.lastModifiedDate = 'repositories/company/lastModifiedDate';
        this.currentCompany = new BehaviorSubject(null);
    }
    /**
     * @return {?}
     */
    getAllCompanies() {
        return super
            .get(this.allCompanies).pipe(catchError((/**
         * @param {?} err
         * @return {?}
         */
        err => of(false))));
    }
    /**
     * @param {?} compId
     * @return {?}
     */
    getAllForCompany(compId) {
        return super
            .get(`${this.getAllUnderCompany}?id=${compId}`).pipe(catchError((/**
         * @param {?} err
         * @return {?}
         */
        err => of(false))));
    }
    /**
     * @param {?} compId
     * @return {?}
     */
    getOne(compId) {
        return super
            .get(`${this.getOneComp}?id=${compId}`).pipe(tap((/**
         * @param {?} company
         * @return {?}
         */
        company => this.currentCompany.next(company))), catchError((/**
         * @param {?} err
         * @return {?}
         */
        err => of(false))));
    }
    /**
     * @return {?}
     */
    getCurrent() {
        return this.currentCompany
            .asObservable()
            .pipe(skipWhile((/**
         * @param {?} company
         * @return {?}
         */
        company => isNullOrUndefined(company))));
    }
    /**
     * @param {?} compId
     * @return {?}
     */
    delete(compId) {
        return super
            .delete(`${this.deleteComp}?id=${compId}`).pipe(catchError((/**
         * @param {?} err
         * @return {?}
         */
        err => of(false))));
    }
    /**
     * @param {?} newCompObj
     * @param {?=} headerObj
     * @return {?}
     */
    add(newCompObj, headerObj = {}) {
        return this
            .post(this.addComp, newCompObj, headerObj).pipe(catchError((/**
         * @param {?} err
         * @return {?}
         */
        err => of(false))));
    }
    /**
     * @param {?} compId
     * @return {?}
     */
    getCompanySnapshot(compId) {
        return super
            .get(`${this.companySnapshot}?companyId=${compId}`)
            .pipe(catchError((/**
         * @param {?} err
         * @return {?}
         */
        err => of(false))));
    }
    /**
     * @param {?} bodyObj
     * @param {?=} headerObj
     * @return {?}
     */
    postCompanyUpdate(bodyObj, headerObj = {}) {
        return super.post(this.update, bodyObj, headerObj).pipe(catchError((/**
         * @param {?} err
         * @return {?}
         */
        err => of(false))));
    }
    /**
     * @return {?}
     */
    returnFileTreeCompanies() {
        return super
            .get(this.allCompanies).pipe(tap((/**
         * @param {?} comps
         * @return {?}
         */
        comps => {
            return this.companiesFiltered = this.cleanCompanies(comps);
        })), catchError((/**
         * @param {?} err
         * @return {?}
         */
        err => of(false))));
    }
    /**
     * @param {?} companies
     * @return {?}
     */
    cleanCompanies(companies) {
        if (!companies) {
            return;
        }
        /** @type {?} */
        const uniqueCompanyNames = [];
        return companies.reduce((/**
         * @param {?} newArr
         * @param {?} comp
         * @return {?}
         */
        (newArr, comp) => {
            /** @type {?} */
            let incrementer;
            if (!uniqueCompanyNames.includes(comp.name)) {
                uniqueCompanyNames.push(comp.name);
                comp['hierarchy'] = [`${comp['commonName']} : ${comp['name']}`];
            }
            else {
                comp['hierarchy'] = [`${comp['commonName']} : ${comp['name']}${incrementer += 1}`];
            }
            newArr.push(comp);
            return newArr;
        }), []);
    }
    /**
     * @return {?}
     */
    returnFilteredComps() {
        return this.companiesFiltered;
    }
}
CompanyService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
CompanyService.ctorParameters = () => [
    { type: HttpClient }
];
/** @nocollapse */ CompanyService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function CompanyService_Factory() { return new CompanyService(i0.ɵɵinject(i1.HttpClient)); }, token: CompanyService, providedIn: "root" });
if (false) {
    /**
     * @type {?}
     * @protected
     */
    CompanyService.prototype.allCompanies;
    /**
     * @type {?}
     * @protected
     */
    CompanyService.prototype.getAllUnderCompany;
    /**
     * @type {?}
     * @protected
     */
    CompanyService.prototype.addComp;
    /**
     * @type {?}
     * @protected
     */
    CompanyService.prototype.getOneComp;
    /**
     * @type {?}
     * @protected
     */
    CompanyService.prototype.deleteComp;
    /**
     * @type {?}
     * @protected
     */
    CompanyService.prototype.companySnapshot;
    /**
     * @type {?}
     * @protected
     */
    CompanyService.prototype.restoreFromCompanySnapshot;
    /**
     * @type {?}
     * @protected
     */
    CompanyService.prototype.downloadAtt;
    /**
     * @type {?}
     * @protected
     */
    CompanyService.prototype.getOneMeta;
    /**
     * @type {?}
     * @protected
     */
    CompanyService.prototype.update;
    /**
     * @type {?}
     * @protected
     */
    CompanyService.prototype.lastModifiedDate;
    /**
     * @type {?}
     * @private
     */
    CompanyService.prototype.currentCompany;
    /** @type {?} */
    CompanyService.prototype.companiesFiltered;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tcGFueS5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGhwdGVjaC9zYXBoaXJhLXNoYXJlZC1saWIvIiwic291cmNlcyI6WyJsaWIvc2VydmljZXMvYWRtaW4tc2VydmljZXMvY29tcGFueS5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUNsRCxPQUFPLEVBQWMsRUFBRSxFQUFFLGVBQWUsRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUN2RCxPQUFPLEVBQUUsVUFBVSxFQUFPLEdBQUcsRUFBRSxTQUFTLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUVqRSxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFFOUMsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sTUFBTSxDQUFDOzs7QUFPekMsTUFBTSxPQUFPLGNBQWUsU0FBUSxXQUFXOzs7O0lBZ0I3QyxZQUFZLElBQWdCO1FBQzFCLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztRQWhCSixpQkFBWSxHQUFHLDZCQUE2QixDQUFDO1FBQzdDLHVCQUFrQixHQUFHLHlDQUF5QyxDQUFDO1FBQy9ELFlBQU8sR0FBRywwQkFBMEIsQ0FBQztRQUNyQyxlQUFVLEdBQUcsNkJBQTZCLENBQUM7UUFDM0MsZUFBVSxHQUFHLDZCQUE2QixDQUFDO1FBQzNDLG9CQUFlLEdBQUcsMEJBQTBCLENBQUM7UUFDN0MsK0JBQTBCLEdBQUcsa0NBQWtDLENBQUM7UUFDaEUsZ0JBQVcsR0FBRyw2QkFBNkIsQ0FBQztRQUM1QyxlQUFVLEdBQUcsaUNBQWlDLENBQUM7UUFDL0MsV0FBTSxHQUFHLDZCQUE2QixDQUFDO1FBQ3ZDLHFCQUFnQixHQUFHLHVDQUF1QyxDQUFDO1FBRTdELG1CQUFjLEdBQXlCLElBQUksZUFBZSxDQUFNLElBQUksQ0FBQyxDQUFDO0lBSzlFLENBQUM7Ozs7SUFHTSxlQUFlO1FBQ3BCLE9BQU8sS0FBSzthQUNULEdBQUcsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsSUFBSSxDQUMxQixVQUFVOzs7O1FBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLEVBQUMsQ0FDN0IsQ0FBQztJQUNOLENBQUM7Ozs7O0lBRU0sZ0JBQWdCLENBQUMsTUFBTTtRQUM1QixPQUFPLEtBQUs7YUFDVCxHQUFHLENBQUMsR0FBRyxJQUFJLENBQUMsa0JBQWtCLE9BQU8sTUFBTSxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQ2xELFVBQVU7Ozs7UUFBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsRUFBQyxDQUM3QixDQUFDO0lBQ04sQ0FBQzs7Ozs7SUFFTSxNQUFNLENBQUMsTUFBTTtRQUNsQixPQUFPLEtBQUs7YUFDVCxHQUFHLENBQUMsR0FBRyxJQUFJLENBQUMsVUFBVSxPQUFPLE1BQU0sRUFBRSxDQUFDLENBQUMsSUFBSSxDQUMxQyxHQUFHOzs7O1FBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsRUFBQyxFQUNqRCxVQUFVOzs7O1FBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLEVBQUMsQ0FDN0IsQ0FBQztJQUNOLENBQUM7Ozs7SUFFTSxVQUFVO1FBQ2YsT0FBTyxJQUFJLENBQUMsY0FBYzthQUNkLFlBQVksRUFBRTthQUNkLElBQUksQ0FDSCxTQUFTOzs7O1FBQUUsT0FBTyxDQUFDLEVBQUUsQ0FBQyxpQkFBaUIsQ0FBQyxPQUFPLENBQUMsRUFBRSxDQUNwRCxDQUFDO0lBQ2QsQ0FBQzs7Ozs7SUFFTSxNQUFNLENBQUMsTUFBTTtRQUNsQixPQUFPLEtBQUs7YUFDVCxNQUFNLENBQUMsR0FBRyxJQUFJLENBQUMsVUFBVSxPQUFPLE1BQU0sRUFBRSxDQUFDLENBQUMsSUFBSSxDQUM3QyxVQUFVOzs7O1FBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLEVBQUMsQ0FDN0IsQ0FBQztJQUNOLENBQUM7Ozs7OztJQUVNLEdBQUcsQ0FBQyxVQUFVLEVBQUUsU0FBUyxHQUFHLEVBQUU7UUFDbkMsT0FBTyxJQUFJO2FBQ1IsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsVUFBVSxFQUFFLFNBQVMsQ0FBQyxDQUFDLElBQUksQ0FDN0MsVUFBVTs7OztRQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxFQUFDLENBQzNCLENBQUM7SUFDUixDQUFDOzs7OztJQUVNLGtCQUFrQixDQUFDLE1BQU07UUFDOUIsT0FBTyxLQUFLO2FBQ1QsR0FBRyxDQUFDLEdBQUcsSUFBSSxDQUFDLGVBQWUsY0FBYyxNQUFNLEVBQUUsQ0FBQzthQUNsRCxJQUFJLENBQ0gsVUFBVTs7OztRQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxFQUFDLENBQzdCLENBQUM7SUFDTixDQUFDOzs7Ozs7SUFFTSxpQkFBaUIsQ0FBQyxPQUFPLEVBQUUsU0FBUyxHQUFHLEVBQUU7UUFDOUMsT0FBTyxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsT0FBTyxFQUFFLFNBQVMsQ0FBQyxDQUFDLElBQUksQ0FDckQsVUFBVTs7OztRQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxFQUFDLENBQzdCLENBQUM7SUFDSixDQUFDOzs7O0lBR00sdUJBQXVCO1FBQzVCLE9BQU8sS0FBSzthQUNULEdBQUcsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsSUFBSSxDQUMxQixHQUFHOzs7O1FBQUMsS0FBSyxDQUFDLEVBQUU7WUFDVixPQUFPLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQzdELENBQUMsRUFBQyxFQUNGLFVBQVU7Ozs7UUFBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsRUFBQyxDQUM3QixDQUFDO0lBQ04sQ0FBQzs7Ozs7SUFHTSxjQUFjLENBQUMsU0FBUztRQUM3QixJQUFJLENBQUMsU0FBUyxFQUFFO1lBQ2QsT0FBTztTQUNSOztjQUNLLGtCQUFrQixHQUFHLEVBQUU7UUFDN0IsT0FBTyxTQUFTLENBQUMsTUFBTTs7Ozs7UUFBQyxDQUFDLE1BQU0sRUFBRSxJQUFJLEVBQUUsRUFBRTs7Z0JBQ25DLFdBQW1CO1lBQ3ZCLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFO2dCQUMzQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNuQyxJQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxJQUFJLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxDQUFDO2FBQ2pFO2lCQUFNO2dCQUNMLElBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxXQUFXLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQzthQUNwRjtZQUNELE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDbEIsT0FBTyxNQUFNLENBQUM7UUFDaEIsQ0FBQyxHQUFFLEVBQUUsQ0FBQyxDQUFDO0lBQ1QsQ0FBQzs7OztJQUVNLG1CQUFtQjtRQUN4QixPQUFPLElBQUksQ0FBQyxpQkFBaUIsQ0FBQztJQUNoQyxDQUFDOzs7WUFsSEYsVUFBVSxTQUFDO2dCQUNWLFVBQVUsRUFBRSxNQUFNO2FBQ25COzs7O1lBWlEsVUFBVTs7Ozs7Ozs7SUFjakIsc0NBQXVEOzs7OztJQUN2RCw0Q0FBeUU7Ozs7O0lBQ3pFLGlDQUErQzs7Ozs7SUFDL0Msb0NBQXFEOzs7OztJQUNyRCxvQ0FBcUQ7Ozs7O0lBQ3JELHlDQUF1RDs7Ozs7SUFDdkQsb0RBQTBFOzs7OztJQUMxRSxxQ0FBc0Q7Ozs7O0lBQ3RELG9DQUF5RDs7Ozs7SUFDekQsZ0NBQWlEOzs7OztJQUNqRCwwQ0FBcUU7Ozs7O0lBRXJFLHdDQUE4RTs7SUFDOUUsMkNBQXlCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgSHR0cENsaWVudCB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcbmltcG9ydCB7IE9ic2VydmFibGUsIG9mLCBCZWhhdmlvclN1YmplY3QgfSBmcm9tICdyeGpzJztcbmltcG9ydCB7IGNhdGNoRXJyb3IsIG1hcCwgdGFwLCBza2lwV2hpbGUgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XG5cbmltcG9ydCB7IERhdGFTZXJ2aWNlIH0gZnJvbSAnLi4vZGF0YS5zZXJ2aWNlJztcbmltcG9ydCAqIGFzIF8gZnJvbSAnbG9kYXNoJztcbmltcG9ydCB7IGlzTnVsbE9yVW5kZWZpbmVkIH0gZnJvbSAndXRpbCc7XG5cblxuXG5ASW5qZWN0YWJsZSh7XG4gIHByb3ZpZGVkSW46ICdyb290J1xufSlcbmV4cG9ydCBjbGFzcyBDb21wYW55U2VydmljZSBleHRlbmRzIERhdGFTZXJ2aWNlIHtcbiAgcHJvdGVjdGVkIGFsbENvbXBhbmllcyA9ICdyZXBvc2l0b3JpZXMvY29tcGFueS9nZXRBbGwnO1xuICBwcm90ZWN0ZWQgZ2V0QWxsVW5kZXJDb21wYW55ID0gJ3JlcG9zaXRvcmllcy9jb21wYW55L2dldEFsbFVuZGVyQ29tcGFueSc7XG4gIHByb3RlY3RlZCBhZGRDb21wID0gJ3JlcG9zaXRvcmllcy9jb21wYW55L2FkZCc7XG4gIHByb3RlY3RlZCBnZXRPbmVDb21wID0gJ3JlcG9zaXRvcmllcy9jb21wYW55L2dldE9uZSc7XG4gIHByb3RlY3RlZCBkZWxldGVDb21wID0gJ3JlcG9zaXRvcmllcy9jb21wYW55L2RlbGV0ZSc7XG4gIHByb3RlY3RlZCBjb21wYW55U25hcHNob3QgPSAndXRpbHMvZ2V0Q29tcGFueVNuYXBzaG90JztcbiAgcHJvdGVjdGVkIHJlc3RvcmVGcm9tQ29tcGFueVNuYXBzaG90ID0gJ3V0aWxzL3Jlc3RvcmVGcm9tQ29tcGFueVNuYXBzaG90JztcbiAgcHJvdGVjdGVkIGRvd25sb2FkQXR0ID0gJ2F0dGFjaG1lbnRzL3NlYXJjaC9maW5kQnlJZCc7XG4gIHByb3RlY3RlZCBnZXRPbmVNZXRhID0gJ3JlcG9zaXRvcmllcy9jb21wYW55L2dldE9uZU1ldGEnO1xuICBwcm90ZWN0ZWQgdXBkYXRlID0gJ3JlcG9zaXRvcmllcy9jb21wYW55L3VwZGF0ZSc7XG4gIHByb3RlY3RlZCBsYXN0TW9kaWZpZWREYXRlID0gJ3JlcG9zaXRvcmllcy9jb21wYW55L2xhc3RNb2RpZmllZERhdGUnO1xuXG4gIHByaXZhdGUgY3VycmVudENvbXBhbnk6IEJlaGF2aW9yU3ViamVjdDxhbnk+ID0gbmV3IEJlaGF2aW9yU3ViamVjdDxhbnk+KG51bGwpO1xuICBwdWJsaWMgY29tcGFuaWVzRmlsdGVyZWQ7XG5cbiAgY29uc3RydWN0b3IoaHR0cDogSHR0cENsaWVudCkge1xuICAgIHN1cGVyKGh0dHApO1xuICB9XG5cblxuICBwdWJsaWMgZ2V0QWxsQ29tcGFuaWVzKCk6IE9ic2VydmFibGU8YW55PiB7XG4gICAgcmV0dXJuIHN1cGVyXG4gICAgICAuZ2V0KHRoaXMuYWxsQ29tcGFuaWVzKS5waXBlKFxuICAgICAgICBjYXRjaEVycm9yKGVyciA9PiBvZihmYWxzZSkpXG4gICAgICApO1xuICB9XG5cbiAgcHVibGljIGdldEFsbEZvckNvbXBhbnkoY29tcElkKTogT2JzZXJ2YWJsZTxhbnk+IHtcbiAgICByZXR1cm4gc3VwZXJcbiAgICAgIC5nZXQoYCR7dGhpcy5nZXRBbGxVbmRlckNvbXBhbnl9P2lkPSR7Y29tcElkfWApLnBpcGUoXG4gICAgICAgIGNhdGNoRXJyb3IoZXJyID0+IG9mKGZhbHNlKSlcbiAgICAgICk7XG4gIH1cblxuICBwdWJsaWMgZ2V0T25lKGNvbXBJZCk6IE9ic2VydmFibGU8YW55PiB7XG4gICAgcmV0dXJuIHN1cGVyXG4gICAgICAuZ2V0KGAke3RoaXMuZ2V0T25lQ29tcH0/aWQ9JHtjb21wSWR9YCkucGlwZShcbiAgICAgICAgdGFwKGNvbXBhbnkgPT4gdGhpcy5jdXJyZW50Q29tcGFueS5uZXh0KGNvbXBhbnkpKSxcbiAgICAgICAgY2F0Y2hFcnJvcihlcnIgPT4gb2YoZmFsc2UpKVxuICAgICAgKTtcbiAgfVxuXG4gIHB1YmxpYyBnZXRDdXJyZW50KCk6IE9ic2VydmFibGU8YW55PiB7XG4gICAgcmV0dXJuIHRoaXMuY3VycmVudENvbXBhbnlcbiAgICAgICAgICAgICAgIC5hc09ic2VydmFibGUoKVxuICAgICAgICAgICAgICAgLnBpcGUoXG4gICAgICAgICAgICAgICAgIHNraXBXaGlsZSggY29tcGFueSA9PiBpc051bGxPclVuZGVmaW5lZChjb21wYW55KSApXG4gICAgICAgICAgICAgICk7XG4gIH1cblxuICBwdWJsaWMgZGVsZXRlKGNvbXBJZCk6IE9ic2VydmFibGU8YW55PiB7XG4gICAgcmV0dXJuIHN1cGVyXG4gICAgICAuZGVsZXRlKGAke3RoaXMuZGVsZXRlQ29tcH0/aWQ9JHtjb21wSWR9YCkucGlwZShcbiAgICAgICAgY2F0Y2hFcnJvcihlcnIgPT4gb2YoZmFsc2UpKVxuICAgICAgKTtcbiAgfVxuXG4gIHB1YmxpYyBhZGQobmV3Q29tcE9iaiwgaGVhZGVyT2JqID0ge30pOiBPYnNlcnZhYmxlPHN0cmluZz4ge1xuICAgIHJldHVybiB0aGlzXG4gICAgICAucG9zdCh0aGlzLmFkZENvbXAsIG5ld0NvbXBPYmosIGhlYWRlck9iaikucGlwZShcbiAgICAgICAgY2F0Y2hFcnJvcihlcnIgPT4gb2YoZmFsc2UpKVxuICAgICAgICApO1xuICB9XG5cbiAgcHVibGljIGdldENvbXBhbnlTbmFwc2hvdChjb21wSWQpOiBPYnNlcnZhYmxlPGFueT4ge1xuICAgIHJldHVybiBzdXBlclxuICAgICAgLmdldChgJHt0aGlzLmNvbXBhbnlTbmFwc2hvdH0/Y29tcGFueUlkPSR7Y29tcElkfWApXG4gICAgICAucGlwZShcbiAgICAgICAgY2F0Y2hFcnJvcihlcnIgPT4gb2YoZmFsc2UpKVxuICAgICAgKTtcbiAgfVxuXG4gIHB1YmxpYyBwb3N0Q29tcGFueVVwZGF0ZShib2R5T2JqLCBoZWFkZXJPYmogPSB7fSkge1xuICAgIHJldHVybiBzdXBlci5wb3N0KHRoaXMudXBkYXRlLCBib2R5T2JqLCBoZWFkZXJPYmopLnBpcGUoXG4gICAgICBjYXRjaEVycm9yKGVyciA9PiBvZihmYWxzZSkpXG4gICAgKTtcbiAgfVxuXG5cbiAgcHVibGljIHJldHVybkZpbGVUcmVlQ29tcGFuaWVzKCkge1xuICAgIHJldHVybiBzdXBlclxuICAgICAgLmdldCh0aGlzLmFsbENvbXBhbmllcykucGlwZShcbiAgICAgICAgdGFwKGNvbXBzID0+IHtcbiAgICAgICAgICByZXR1cm4gdGhpcy5jb21wYW5pZXNGaWx0ZXJlZCA9IHRoaXMuY2xlYW5Db21wYW5pZXMoY29tcHMpO1xuICAgICAgICB9KSxcbiAgICAgICAgY2F0Y2hFcnJvcihlcnIgPT4gb2YoZmFsc2UpKVxuICAgICAgKTtcbiAgfVxuXG5cbiAgcHVibGljIGNsZWFuQ29tcGFuaWVzKGNvbXBhbmllcykge1xuICAgIGlmICghY29tcGFuaWVzKSB7XG4gICAgICByZXR1cm47XG4gICAgfVxuICAgIGNvbnN0IHVuaXF1ZUNvbXBhbnlOYW1lcyA9IFtdO1xuICAgIHJldHVybiBjb21wYW5pZXMucmVkdWNlKChuZXdBcnIsIGNvbXApID0+IHtcbiAgICAgIGxldCBpbmNyZW1lbnRlcjogbnVtYmVyO1xuICAgICAgaWYgKCF1bmlxdWVDb21wYW55TmFtZXMuaW5jbHVkZXMoY29tcC5uYW1lKSkge1xuICAgICAgICB1bmlxdWVDb21wYW55TmFtZXMucHVzaChjb21wLm5hbWUpO1xuICAgICAgICBjb21wWydoaWVyYXJjaHknXSA9IFtgJHtjb21wWydjb21tb25OYW1lJ119IDogJHtjb21wWyduYW1lJ119YF07XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBjb21wWydoaWVyYXJjaHknXSA9IFtgJHtjb21wWydjb21tb25OYW1lJ119IDogJHtjb21wWyduYW1lJ119JHtpbmNyZW1lbnRlciArPSAxfWBdO1xuICAgICAgfVxuICAgICAgbmV3QXJyLnB1c2goY29tcCk7XG4gICAgICByZXR1cm4gbmV3QXJyO1xuICAgIH0sIFtdKTtcbiAgfVxuXG4gIHB1YmxpYyByZXR1cm5GaWx0ZXJlZENvbXBzKCkge1xuICAgIHJldHVybiB0aGlzLmNvbXBhbmllc0ZpbHRlcmVkO1xuICB9XG5cbn1cbiJdfQ==