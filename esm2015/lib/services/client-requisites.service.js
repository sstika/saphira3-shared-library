/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { of, BehaviorSubject } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { DataService } from './data.service';
import { HttpClient } from '@angular/common/http';
export class ClientRequisitesService extends DataService {
    /*
      * Constructor
      */
    /**
     * @param {?} http
     */
    constructor(http) {
        super(http);
        this.clientRequisitesPath = 'required';
        this.clientRequisites = new BehaviorSubject({});
        this.getClientReqs = this.clientRequisites.asObservable();
    }
    /*
      * Get the client requisites. Only loaded once for now.
      * TODO implement some timeout on the cache so requisites are reloaded once every 10 minutes or something idk
      */
    /**
     * @param {?=} reload
     * @return {?}
     */
    getClientRequisites(reload) {
        if (!this.cachedData || reload) {
            return super
                .get(`${this.clientRequisitesPath}`).pipe(tap((/**
             * @param {?} requisites
             * @return {?}
             */
            requisites => this.cachedData = requisites)), catchError((/**
             * @param {?} err
             * @return {?}
             */
            err => of(false))));
        }
        else {
            return of(this.cachedData);
        }
    }
    /*
      * Set the client requisites
      */
    /**
     * @param {?} data
     * @return {?}
     */
    setClientRequisitesService(data) {
        this.clientRequisites.next(data);
        this.cachedData = data;
    }
    /*
      * Return the cached field
      */
    /**
     * @param {?} requestedField
     * @return {?}
     */
    returnCachedField(requestedField) {
        return this.cachedData[requestedField];
    }
    /*
      * Return all the cached data
      */
    /**
     * @return {?}
     */
    returnAllCached() {
        return of(this.cachedData);
    }
}
ClientRequisitesService.decorators = [
    { type: Injectable }
];
/** @nocollapse */
ClientRequisitesService.ctorParameters = () => [
    { type: HttpClient }
];
if (false) {
    /**
     * @type {?}
     * @protected
     */
    ClientRequisitesService.prototype.clientRequisitesPath;
    /** @type {?} */
    ClientRequisitesService.prototype.cachedData;
    /** @type {?} */
    ClientRequisitesService.prototype.clientRequisites;
    /** @type {?} */
    ClientRequisitesService.prototype.getClientReqs;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2xpZW50LXJlcXVpc2l0ZXMuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BocHRlY2gvc2FwaGlyYS1zaGFyZWQtbGliLyIsInNvdXJjZXMiOlsibGliL3NlcnZpY2VzL2NsaWVudC1yZXF1aXNpdGVzLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBYyxFQUFFLEVBQUUsZUFBZSxFQUFnQixNQUFNLE1BQU0sQ0FBQztBQUVyRSxPQUFPLEVBQUUsVUFBVSxFQUFFLEdBQUcsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQ2pELE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQzdDLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUdsRCxNQUFNLE9BQU8sdUJBQXdCLFNBQVEsV0FBVzs7Ozs7OztJQVV0RCxZQUFZLElBQWdCO1FBQzFCLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztRQVZKLHlCQUFvQixHQUFHLFVBQVUsQ0FBQztRQUdyQyxxQkFBZ0IsR0FBRyxJQUFJLGVBQWUsQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUNsRCxrQkFBYSxHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxZQUFZLEVBQUUsQ0FBQztJQU9yRCxDQUFDOzs7Ozs7Ozs7SUFNTSxtQkFBbUIsQ0FBQyxNQUFnQjtRQUN6QyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsSUFBSSxNQUFNLEVBQUU7WUFDOUIsT0FBTyxLQUFLO2lCQUNULEdBQUcsQ0FBQyxHQUFHLElBQUksQ0FBQyxvQkFBb0IsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUN2QyxHQUFHOzs7O1lBQUMsVUFBVSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBVSxHQUFHLFVBQVUsRUFBQyxFQUMvQyxVQUFVOzs7O1lBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLEVBQUMsQ0FBQyxDQUFDO1NBQ25DO2FBQU07WUFDTCxPQUFPLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7U0FDNUI7SUFDSCxDQUFDOzs7Ozs7OztJQUtNLDBCQUEwQixDQUFDLElBQVk7UUFDNUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNqQyxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQztJQUN6QixDQUFDOzs7Ozs7OztJQUtNLGlCQUFpQixDQUFDLGNBQXNCO1FBQzdDLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxjQUFjLENBQUMsQ0FBQztJQUN6QyxDQUFDOzs7Ozs7O0lBS00sZUFBZTtRQUNwQixPQUFPLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7SUFDN0IsQ0FBQzs7O1lBbERGLFVBQVU7Ozs7WUFGRixVQUFVOzs7Ozs7O0lBSWpCLHVEQUE0Qzs7SUFDNUMsNkNBQXVCOztJQUV2QixtREFBa0Q7O0lBQ2xELGdEQUFxRCIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE9ic2VydmFibGUsIG9mLCBCZWhhdmlvclN1YmplY3QsIFN1YnNjcmlwdGlvbiB9IGZyb20gJ3J4anMnO1xuXG5pbXBvcnQgeyBjYXRjaEVycm9yLCB0YXAgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XG5pbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBEYXRhU2VydmljZSB9IGZyb20gJy4vZGF0YS5zZXJ2aWNlJztcbmltcG9ydCB7IEh0dHBDbGllbnQgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XG5cbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBDbGllbnRSZXF1aXNpdGVzU2VydmljZSBleHRlbmRzIERhdGFTZXJ2aWNlIHtcbiAgcHJvdGVjdGVkIGNsaWVudFJlcXVpc2l0ZXNQYXRoID0gJ3JlcXVpcmVkJztcbiAgcHVibGljIGNhY2hlZERhdGE6IGFueTtcblxuICBwdWJsaWMgY2xpZW50UmVxdWlzaXRlcyA9IG5ldyBCZWhhdmlvclN1YmplY3Qoe30pO1xuICBnZXRDbGllbnRSZXFzID0gdGhpcy5jbGllbnRSZXF1aXNpdGVzLmFzT2JzZXJ2YWJsZSgpO1xuXG4gIC8qXG4gICogQ29uc3RydWN0b3JcbiAgKi9cbiAgY29uc3RydWN0b3IoaHR0cDogSHR0cENsaWVudCkge1xuICAgIHN1cGVyKGh0dHApO1xuICB9XG5cbiAgLypcbiAgKiBHZXQgdGhlIGNsaWVudCByZXF1aXNpdGVzLiBPbmx5IGxvYWRlZCBvbmNlIGZvciBub3cuXG4gICogVE9ETyBpbXBsZW1lbnQgc29tZSB0aW1lb3V0IG9uIHRoZSBjYWNoZSBzbyByZXF1aXNpdGVzIGFyZSByZWxvYWRlZCBvbmNlIGV2ZXJ5IDEwIG1pbnV0ZXMgb3Igc29tZXRoaW5nIGlka1xuICAqL1xuICBwdWJsaWMgZ2V0Q2xpZW50UmVxdWlzaXRlcyhyZWxvYWQ/OiBib29sZWFuKTogT2JzZXJ2YWJsZTxhbnk+IHtcbiAgICBpZiAoIXRoaXMuY2FjaGVkRGF0YSB8fCByZWxvYWQpIHtcbiAgICAgIHJldHVybiBzdXBlclxuICAgICAgICAuZ2V0KGAke3RoaXMuY2xpZW50UmVxdWlzaXRlc1BhdGh9YCkucGlwZShcbiAgICAgICAgICB0YXAocmVxdWlzaXRlcyA9PiB0aGlzLmNhY2hlZERhdGEgPSByZXF1aXNpdGVzKSxcbiAgICAgICAgICBjYXRjaEVycm9yKGVyciA9PiBvZihmYWxzZSkpKTtcbiAgICB9IGVsc2Uge1xuICAgICAgcmV0dXJuIG9mKHRoaXMuY2FjaGVkRGF0YSk7XG4gICAgfVxuICB9XG5cbiAgLypcbiAgKiBTZXQgdGhlIGNsaWVudCByZXF1aXNpdGVzXG4gICovXG4gIHB1YmxpYyBzZXRDbGllbnRSZXF1aXNpdGVzU2VydmljZShkYXRhOiBvYmplY3QpOiB2b2lkIHtcbiAgICB0aGlzLmNsaWVudFJlcXVpc2l0ZXMubmV4dChkYXRhKTtcbiAgICB0aGlzLmNhY2hlZERhdGEgPSBkYXRhO1xuICB9XG5cbiAgLypcbiAgKiBSZXR1cm4gdGhlIGNhY2hlZCBmaWVsZFxuICAqL1xuICBwdWJsaWMgcmV0dXJuQ2FjaGVkRmllbGQocmVxdWVzdGVkRmllbGQ6IHN0cmluZyk6IG9iamVjdCB7XG4gICAgcmV0dXJuIHRoaXMuY2FjaGVkRGF0YVtyZXF1ZXN0ZWRGaWVsZF07XG4gIH1cblxuICAvKlxuICAqIFJldHVybiBhbGwgdGhlIGNhY2hlZCBkYXRhXG4gICovXG4gIHB1YmxpYyByZXR1cm5BbGxDYWNoZWQoKTogT2JzZXJ2YWJsZTxhbnk+IHtcbiAgICByZXR1cm4gb2YodGhpcy5jYWNoZWREYXRhKTtcbiAgfVxufVxuIl19