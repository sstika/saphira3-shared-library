/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { timer } from 'rxjs';
import * as i0 from "@angular/core";
import * as i1 from "@angular/platform-browser";
// Service to flash titleof currect browser tab.
export class TitleFlasherService {
    /**
     * @param {?} titleService
     */
    constructor(titleService) {
        this.titleService = titleService;
    }
    // Method to start flashing document title
    // Interval: interval of flash
    // String to flash in browser title
    /**
     * @param {?} interval
     * @param {?} titleToFlash
     * @return {?}
     */
    startFlashing(interval, titleToFlash) {
        if (!this.isAlreadyFlashingTitle) {
            this.prevTitle = this.titleService.getTitle();
            this.documentTitleFlash = timer(0, interval);
            this.documentTitleFlashSubscription = this.documentTitleFlash.subscribe((/**
             * @param {?} data
             * @return {?}
             */
            (data) => {
                if (data % 2 === 0) {
                    this.setTitle(titleToFlash);
                }
                else {
                    this.setTitle(this.prevTitle);
                }
            }));
            this.isAlreadyFlashingTitle = true;
        }
    }
    /**
     * @return {?}
     */
    stopFlashing() {
        if (this.isAlreadyFlashingTitle) {
            this.documentTitleFlashSubscription.unsubscribe();
            this.setTitle(this.prevTitle);
            this.isAlreadyFlashingTitle = false;
        }
    }
    /**
     * @param {?} title
     * @return {?}
     */
    setTitle(title) {
        this.titleService.setTitle(title);
    }
    /**
     * @return {?}
     */
    getTitle() {
        return this.titleService.getTitle();
    }
}
TitleFlasherService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
TitleFlasherService.ctorParameters = () => [
    { type: Title }
];
/** @nocollapse */ TitleFlasherService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function TitleFlasherService_Factory() { return new TitleFlasherService(i0.ɵɵinject(i1.Title)); }, token: TitleFlasherService, providedIn: "root" });
if (false) {
    /**
     * @type {?}
     * @private
     */
    TitleFlasherService.prototype.documentTitleFlash;
    /**
     * @type {?}
     * @private
     */
    TitleFlasherService.prototype.documentTitleFlashSubscription;
    /**
     * @type {?}
     * @private
     */
    TitleFlasherService.prototype.prevTitle;
    /** @type {?} */
    TitleFlasherService.prototype.isAlreadyFlashingTitle;
    /**
     * @type {?}
     * @private
     */
    TitleFlasherService.prototype.titleService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGl0bGUtZmxhc2hlci1zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGhwdGVjaC9zYXBoaXJhLXNoYXJlZC1saWIvIiwic291cmNlcyI6WyJsaWIvc2VydmljZXMvdGl0bGUtZmxhc2hlci1zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFBRSxLQUFLLEVBQUUsTUFBTSwyQkFBMkIsQ0FBQztBQUNsRCxPQUFPLEVBQUUsS0FBSyxFQUE0QixNQUFNLE1BQU0sQ0FBQzs7O0FBTXZELGdEQUFnRDtBQUNoRCxNQUFNLE9BQU8sbUJBQW1COzs7O0lBSzlCLFlBQW9CLFlBQW1CO1FBQW5CLGlCQUFZLEdBQVosWUFBWSxDQUFPO0lBRXZDLENBQUM7Ozs7Ozs7OztJQUlELGFBQWEsQ0FBQyxRQUFnQixFQUFFLFlBQW9CO1FBQ2xELElBQUksQ0FBQyxJQUFJLENBQUMsc0JBQXNCLEVBQUU7WUFDaEMsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQzlDLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxLQUFLLENBQUMsQ0FBQyxFQUFFLFFBQVEsQ0FBQyxDQUFDO1lBQzdDLElBQUksQ0FBQyw4QkFBOEIsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsU0FBUzs7OztZQUFDLENBQUMsSUFBWSxFQUFFLEVBQUU7Z0JBQ3ZGLElBQUksSUFBSSxHQUFHLENBQUMsS0FBSyxDQUFDLEVBQUU7b0JBQ2xCLElBQUksQ0FBQyxRQUFRLENBQUMsWUFBWSxDQUFDLENBQUM7aUJBQzdCO3FCQUFNO29CQUNMLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO2lCQUMvQjtZQUNILENBQUMsRUFBQyxDQUFDO1lBQ0gsSUFBSSxDQUFDLHNCQUFzQixHQUFHLElBQUksQ0FBQztTQUNwQztJQUVILENBQUM7Ozs7SUFDRCxZQUFZO1FBQ1YsSUFBSSxJQUFJLENBQUMsc0JBQXNCLEVBQUU7WUFDL0IsSUFBSSxDQUFDLDhCQUE4QixDQUFDLFdBQVcsRUFBRSxDQUFDO1lBQ2xELElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBQzlCLElBQUksQ0FBQyxzQkFBc0IsR0FBRyxLQUFLLENBQUM7U0FDckM7SUFFSCxDQUFDOzs7OztJQUNELFFBQVEsQ0FBQyxLQUFhO1FBQ3BCLElBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBRXBDLENBQUM7Ozs7SUFDRCxRQUFRO1FBQ04sT0FBTyxJQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsRUFBRSxDQUFDO0lBQ3RDLENBQUM7OztZQTVDRixVQUFVLFNBQUM7Z0JBQ1YsVUFBVSxFQUFFLE1BQU07YUFDbkI7Ozs7WUFOUSxLQUFLOzs7Ozs7OztJQVNaLGlEQUErQzs7Ozs7SUFDL0MsNkRBQXFEOzs7OztJQUNyRCx3Q0FBMEI7O0lBQzFCLHFEQUF1Qzs7Ozs7SUFDM0IsMkNBQTJCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgVGl0bGUgfSBmcm9tICdAYW5ndWxhci9wbGF0Zm9ybS1icm93c2VyJztcbmltcG9ydCB7IHRpbWVyLCBPYnNlcnZhYmxlLCBTdWJzY3JpcHRpb24gfSBmcm9tICdyeGpzJztcblxuXG5ASW5qZWN0YWJsZSh7XG4gIHByb3ZpZGVkSW46ICdyb290J1xufSlcbi8vIFNlcnZpY2UgdG8gZmxhc2ggdGl0bGVvZiBjdXJyZWN0IGJyb3dzZXIgdGFiLlxuZXhwb3J0IGNsYXNzIFRpdGxlRmxhc2hlclNlcnZpY2Uge1xuICBwcml2YXRlIGRvY3VtZW50VGl0bGVGbGFzaDogT2JzZXJ2YWJsZTxudW1iZXI+O1xuICBwcml2YXRlIGRvY3VtZW50VGl0bGVGbGFzaFN1YnNjcmlwdGlvbjogU3Vic2NyaXB0aW9uO1xuICBwcml2YXRlIHByZXZUaXRsZTogc3RyaW5nO1xuICBwdWJsaWMgaXNBbHJlYWR5Rmxhc2hpbmdUaXRsZTogYm9vbGVhbjtcbiAgY29uc3RydWN0b3IocHJpdmF0ZSB0aXRsZVNlcnZpY2U6IFRpdGxlKSB7XG5cbiAgfVxuICAvLyBNZXRob2QgdG8gc3RhcnQgZmxhc2hpbmcgZG9jdW1lbnQgdGl0bGVcbiAgLy8gSW50ZXJ2YWw6IGludGVydmFsIG9mIGZsYXNoXG4gIC8vIFN0cmluZyB0byBmbGFzaCBpbiBicm93c2VyIHRpdGxlXG4gIHN0YXJ0Rmxhc2hpbmcoaW50ZXJ2YWw6IG51bWJlciwgdGl0bGVUb0ZsYXNoOiBzdHJpbmcpIHtcbiAgICBpZiAoIXRoaXMuaXNBbHJlYWR5Rmxhc2hpbmdUaXRsZSkge1xuICAgICAgdGhpcy5wcmV2VGl0bGUgPSB0aGlzLnRpdGxlU2VydmljZS5nZXRUaXRsZSgpO1xuICAgICAgdGhpcy5kb2N1bWVudFRpdGxlRmxhc2ggPSB0aW1lcigwLCBpbnRlcnZhbCk7XG4gICAgICB0aGlzLmRvY3VtZW50VGl0bGVGbGFzaFN1YnNjcmlwdGlvbiA9IHRoaXMuZG9jdW1lbnRUaXRsZUZsYXNoLnN1YnNjcmliZSgoZGF0YTogbnVtYmVyKSA9PiB7XG4gICAgICAgIGlmIChkYXRhICUgMiA9PT0gMCkge1xuICAgICAgICAgIHRoaXMuc2V0VGl0bGUodGl0bGVUb0ZsYXNoKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICB0aGlzLnNldFRpdGxlKHRoaXMucHJldlRpdGxlKTtcbiAgICAgICAgfVxuICAgICAgfSk7XG4gICAgICB0aGlzLmlzQWxyZWFkeUZsYXNoaW5nVGl0bGUgPSB0cnVlO1xuICAgIH1cblxuICB9XG4gIHN0b3BGbGFzaGluZygpIHtcbiAgICBpZiAodGhpcy5pc0FscmVhZHlGbGFzaGluZ1RpdGxlKSB7XG4gICAgICB0aGlzLmRvY3VtZW50VGl0bGVGbGFzaFN1YnNjcmlwdGlvbi51bnN1YnNjcmliZSgpO1xuICAgICAgdGhpcy5zZXRUaXRsZSh0aGlzLnByZXZUaXRsZSk7XG4gICAgICB0aGlzLmlzQWxyZWFkeUZsYXNoaW5nVGl0bGUgPSBmYWxzZTtcbiAgICB9XG5cbiAgfVxuICBzZXRUaXRsZSh0aXRsZTogc3RyaW5nKSB7XG4gICAgdGhpcy50aXRsZVNlcnZpY2Uuc2V0VGl0bGUodGl0bGUpO1xuXG4gIH1cbiAgZ2V0VGl0bGUoKSB7XG4gICAgcmV0dXJuIHRoaXMudGl0bGVTZXJ2aWNlLmdldFRpdGxlKCk7XG4gIH1cbn1cbiJdfQ==