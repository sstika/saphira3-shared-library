/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { MatAutocompleteModule, MatButtonModule, MatButtonToggleModule, MatCardModule, MatCheckboxModule, MatChipsModule, MatDatepickerModule, MatDialogModule, MatExpansionModule, MatGridListModule, MatInputModule, MatIconModule, MatListModule, MatMenuModule, MatNativeDateModule, MatProgressBarModule, MatProgressSpinnerModule, MatRadioModule, MatSelectModule, MatSidenavModule, MatSliderModule, MatSlideToggleModule, MatSnackBarModule, MatTabsModule, MatTooltipModule, MatToolbarModule, MatFormFieldModule, MatTableModule, MatPaginatorModule, MatSortModule } from '@angular/material';
export class MaterialModule {
}
MaterialModule.decorators = [
    { type: NgModule, args: [{
                imports: [
                    MatAutocompleteModule,
                    MatButtonModule,
                    MatButtonToggleModule,
                    MatCardModule,
                    MatCheckboxModule,
                    MatChipsModule,
                    MatDatepickerModule,
                    MatDialogModule,
                    MatExpansionModule,
                    MatFormFieldModule,
                    MatGridListModule,
                    MatIconModule,
                    MatInputModule,
                    MatListModule,
                    MatMenuModule,
                    MatNativeDateModule,
                    MatPaginatorModule,
                    MatProgressBarModule,
                    MatProgressSpinnerModule,
                    MatRadioModule,
                    MatSelectModule,
                    MatSortModule,
                    MatSidenavModule,
                    MatSliderModule,
                    MatSlideToggleModule,
                    MatSnackBarModule,
                    MatTableModule,
                    MatTabsModule,
                    MatToolbarModule,
                    MatTooltipModule,
                ],
                exports: [
                    MatAutocompleteModule,
                    MatButtonModule,
                    MatButtonToggleModule,
                    MatCardModule,
                    MatCheckboxModule,
                    MatChipsModule,
                    MatDatepickerModule,
                    MatDialogModule,
                    MatExpansionModule,
                    MatFormFieldModule,
                    MatGridListModule,
                    MatIconModule,
                    MatInputModule,
                    MatListModule,
                    MatMenuModule,
                    MatNativeDateModule,
                    MatPaginatorModule,
                    MatProgressBarModule,
                    MatProgressSpinnerModule,
                    MatRadioModule,
                    MatSelectModule,
                    MatSortModule,
                    MatSidenavModule,
                    MatSliderModule,
                    MatSlideToggleModule,
                    MatSnackBarModule,
                    MatTableModule,
                    MatTabsModule,
                    MatToolbarModule,
                    MatTooltipModule,
                ]
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWF0ZXJpYWwubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGhwdGVjaC9zYXBoaXJhLXNoYXJlZC1saWIvIiwic291cmNlcyI6WyJsaWIvbWF0ZXJpYWwubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFDTCxxQkFBcUIsRUFDckIsZUFBZSxFQUNmLHFCQUFxQixFQUNyQixhQUFhLEVBQ2IsaUJBQWlCLEVBQ2pCLGNBQWMsRUFDZCxtQkFBbUIsRUFDbkIsZUFBZSxFQUNmLGtCQUFrQixFQUNsQixpQkFBaUIsRUFDakIsY0FBYyxFQUNkLGFBQWEsRUFDYixhQUFhLEVBQ2IsYUFBYSxFQUNiLG1CQUFtQixFQUNuQixvQkFBb0IsRUFDcEIsd0JBQXdCLEVBQ3hCLGNBQWMsRUFDZCxlQUFlLEVBQ2YsZ0JBQWdCLEVBQ2hCLGVBQWUsRUFDZixvQkFBb0IsRUFDcEIsaUJBQWlCLEVBQ2pCLGFBQWEsRUFDYixnQkFBZ0IsRUFDaEIsZ0JBQWdCLEVBQ2hCLGtCQUFrQixFQUNsQixjQUFjLEVBQUUsa0JBQWtCLEVBQUUsYUFBYSxFQUNsRCxNQUFNLG1CQUFtQixDQUFDO0FBcUUzQixNQUFNLE9BQU8sY0FBYzs7O1lBbkUxQixRQUFRLFNBQUM7Z0JBQ04sT0FBTyxFQUFFO29CQUNQLHFCQUFxQjtvQkFDckIsZUFBZTtvQkFDZixxQkFBcUI7b0JBQ3JCLGFBQWE7b0JBQ2IsaUJBQWlCO29CQUNqQixjQUFjO29CQUNkLG1CQUFtQjtvQkFDbkIsZUFBZTtvQkFDZixrQkFBa0I7b0JBQ2xCLGtCQUFrQjtvQkFDbEIsaUJBQWlCO29CQUNqQixhQUFhO29CQUNiLGNBQWM7b0JBQ2QsYUFBYTtvQkFDYixhQUFhO29CQUNiLG1CQUFtQjtvQkFDbkIsa0JBQWtCO29CQUNsQixvQkFBb0I7b0JBQ3BCLHdCQUF3QjtvQkFDeEIsY0FBYztvQkFDZCxlQUFlO29CQUNmLGFBQWE7b0JBQ2IsZ0JBQWdCO29CQUNoQixlQUFlO29CQUNmLG9CQUFvQjtvQkFDcEIsaUJBQWlCO29CQUNqQixjQUFjO29CQUNkLGFBQWE7b0JBQ2IsZ0JBQWdCO29CQUNoQixnQkFBZ0I7aUJBRXJCO2dCQUNHLE9BQU8sRUFBRTtvQkFDUCxxQkFBcUI7b0JBQ3JCLGVBQWU7b0JBQ2YscUJBQXFCO29CQUNyQixhQUFhO29CQUNiLGlCQUFpQjtvQkFDakIsY0FBYztvQkFDZCxtQkFBbUI7b0JBQ25CLGVBQWU7b0JBQ2Ysa0JBQWtCO29CQUNsQixrQkFBa0I7b0JBQ2xCLGlCQUFpQjtvQkFDakIsYUFBYTtvQkFDYixjQUFjO29CQUNkLGFBQWE7b0JBQ2IsYUFBYTtvQkFDYixtQkFBbUI7b0JBQ25CLGtCQUFrQjtvQkFDbEIsb0JBQW9CO29CQUNwQix3QkFBd0I7b0JBQ3hCLGNBQWM7b0JBQ2QsZUFBZTtvQkFDZixhQUFhO29CQUNiLGdCQUFnQjtvQkFDaEIsZUFBZTtvQkFDZixvQkFBb0I7b0JBQ3BCLGlCQUFpQjtvQkFDakIsY0FBYztvQkFDZCxhQUFhO29CQUNiLGdCQUFnQjtvQkFDaEIsZ0JBQWdCO2lCQUNqQjthQUNKIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7XG4gIE1hdEF1dG9jb21wbGV0ZU1vZHVsZSxcbiAgTWF0QnV0dG9uTW9kdWxlLFxuICBNYXRCdXR0b25Ub2dnbGVNb2R1bGUsXG4gIE1hdENhcmRNb2R1bGUsXG4gIE1hdENoZWNrYm94TW9kdWxlLFxuICBNYXRDaGlwc01vZHVsZSxcbiAgTWF0RGF0ZXBpY2tlck1vZHVsZSxcbiAgTWF0RGlhbG9nTW9kdWxlLFxuICBNYXRFeHBhbnNpb25Nb2R1bGUsXG4gIE1hdEdyaWRMaXN0TW9kdWxlLFxuICBNYXRJbnB1dE1vZHVsZSxcbiAgTWF0SWNvbk1vZHVsZSxcbiAgTWF0TGlzdE1vZHVsZSxcbiAgTWF0TWVudU1vZHVsZSxcbiAgTWF0TmF0aXZlRGF0ZU1vZHVsZSxcbiAgTWF0UHJvZ3Jlc3NCYXJNb2R1bGUsXG4gIE1hdFByb2dyZXNzU3Bpbm5lck1vZHVsZSxcbiAgTWF0UmFkaW9Nb2R1bGUsXG4gIE1hdFNlbGVjdE1vZHVsZSxcbiAgTWF0U2lkZW5hdk1vZHVsZSxcbiAgTWF0U2xpZGVyTW9kdWxlLFxuICBNYXRTbGlkZVRvZ2dsZU1vZHVsZSxcbiAgTWF0U25hY2tCYXJNb2R1bGUsXG4gIE1hdFRhYnNNb2R1bGUsXG4gIE1hdFRvb2x0aXBNb2R1bGUsXG4gIE1hdFRvb2xiYXJNb2R1bGUsXG4gIE1hdEZvcm1GaWVsZE1vZHVsZSxcbiAgTWF0VGFibGVNb2R1bGUsIE1hdFBhZ2luYXRvck1vZHVsZSwgTWF0U29ydE1vZHVsZVxufSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbCc7XG5cbkBOZ01vZHVsZSh7XG4gICAgaW1wb3J0czogW1xuICAgICAgTWF0QXV0b2NvbXBsZXRlTW9kdWxlLFxuICAgICAgTWF0QnV0dG9uTW9kdWxlLFxuICAgICAgTWF0QnV0dG9uVG9nZ2xlTW9kdWxlLFxuICAgICAgTWF0Q2FyZE1vZHVsZSxcbiAgICAgIE1hdENoZWNrYm94TW9kdWxlLFxuICAgICAgTWF0Q2hpcHNNb2R1bGUsXG4gICAgICBNYXREYXRlcGlja2VyTW9kdWxlLFxuICAgICAgTWF0RGlhbG9nTW9kdWxlLFxuICAgICAgTWF0RXhwYW5zaW9uTW9kdWxlLFxuICAgICAgTWF0Rm9ybUZpZWxkTW9kdWxlLFxuICAgICAgTWF0R3JpZExpc3RNb2R1bGUsXG4gICAgICBNYXRJY29uTW9kdWxlLFxuICAgICAgTWF0SW5wdXRNb2R1bGUsXG4gICAgICBNYXRMaXN0TW9kdWxlLFxuICAgICAgTWF0TWVudU1vZHVsZSxcbiAgICAgIE1hdE5hdGl2ZURhdGVNb2R1bGUsXG4gICAgICBNYXRQYWdpbmF0b3JNb2R1bGUsXG4gICAgICBNYXRQcm9ncmVzc0Jhck1vZHVsZSxcbiAgICAgIE1hdFByb2dyZXNzU3Bpbm5lck1vZHVsZSxcbiAgICAgIE1hdFJhZGlvTW9kdWxlLFxuICAgICAgTWF0U2VsZWN0TW9kdWxlLFxuICAgICAgTWF0U29ydE1vZHVsZSxcbiAgICAgIE1hdFNpZGVuYXZNb2R1bGUsXG4gICAgICBNYXRTbGlkZXJNb2R1bGUsXG4gICAgICBNYXRTbGlkZVRvZ2dsZU1vZHVsZSxcbiAgICAgIE1hdFNuYWNrQmFyTW9kdWxlLFxuICAgICAgTWF0VGFibGVNb2R1bGUsXG4gICAgICBNYXRUYWJzTW9kdWxlLFxuICAgICAgTWF0VG9vbGJhck1vZHVsZSxcbiAgICAgIE1hdFRvb2x0aXBNb2R1bGUsXG5cbl0sXG4gICAgZXhwb3J0czogW1xuICAgICAgTWF0QXV0b2NvbXBsZXRlTW9kdWxlLFxuICAgICAgTWF0QnV0dG9uTW9kdWxlLFxuICAgICAgTWF0QnV0dG9uVG9nZ2xlTW9kdWxlLFxuICAgICAgTWF0Q2FyZE1vZHVsZSxcbiAgICAgIE1hdENoZWNrYm94TW9kdWxlLFxuICAgICAgTWF0Q2hpcHNNb2R1bGUsXG4gICAgICBNYXREYXRlcGlja2VyTW9kdWxlLFxuICAgICAgTWF0RGlhbG9nTW9kdWxlLFxuICAgICAgTWF0RXhwYW5zaW9uTW9kdWxlLFxuICAgICAgTWF0Rm9ybUZpZWxkTW9kdWxlLFxuICAgICAgTWF0R3JpZExpc3RNb2R1bGUsXG4gICAgICBNYXRJY29uTW9kdWxlLFxuICAgICAgTWF0SW5wdXRNb2R1bGUsXG4gICAgICBNYXRMaXN0TW9kdWxlLFxuICAgICAgTWF0TWVudU1vZHVsZSxcbiAgICAgIE1hdE5hdGl2ZURhdGVNb2R1bGUsXG4gICAgICBNYXRQYWdpbmF0b3JNb2R1bGUsXG4gICAgICBNYXRQcm9ncmVzc0Jhck1vZHVsZSxcbiAgICAgIE1hdFByb2dyZXNzU3Bpbm5lck1vZHVsZSxcbiAgICAgIE1hdFJhZGlvTW9kdWxlLFxuICAgICAgTWF0U2VsZWN0TW9kdWxlLFxuICAgICAgTWF0U29ydE1vZHVsZSxcbiAgICAgIE1hdFNpZGVuYXZNb2R1bGUsXG4gICAgICBNYXRTbGlkZXJNb2R1bGUsXG4gICAgICBNYXRTbGlkZVRvZ2dsZU1vZHVsZSxcbiAgICAgIE1hdFNuYWNrQmFyTW9kdWxlLFxuICAgICAgTWF0VGFibGVNb2R1bGUsXG4gICAgICBNYXRUYWJzTW9kdWxlLFxuICAgICAgTWF0VG9vbGJhck1vZHVsZSxcbiAgICAgIE1hdFRvb2x0aXBNb2R1bGUsXG4gICAgXVxufSlcbmV4cG9ydCBjbGFzcyBNYXRlcmlhbE1vZHVsZSB7fVxuIl19