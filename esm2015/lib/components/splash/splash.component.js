/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { PingService } from '../../services/ping.service';
import { takeWhile } from 'rxjs/operators';
import { ClientRequisitesService } from '../../services/client-requisites.service';
import { MatDialog } from '@angular/material';
import { TitleFlasherService } from '../../services/title-flasher-service';
export class SplashComponent {
    /**
     * @param {?} auth
     * @param {?} matDialog
     * @param {?} router
     * @param {?} ping
     * @param {?} requisiteServ
     * @param {?} title
     */
    constructor(auth, matDialog, router, ping, requisiteServ, title) {
        this.auth = auth;
        this.matDialog = matDialog;
        this.router = router;
        this.ping = ping;
        this.requisiteServ = requisiteServ;
        this.title = title;
        this.loginFailed = false;
        this.connectedStatus = 'Not connected';
        this.statusClass = 'not-connected login-support-text login-connection-status';
        this.shouldPing = true;
        this.afterLoginRoute = ['/'];
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.matDialog.closeAll();
        this.title.setTitle('Saphira 3.0');
        this.ping
            .status()
            .pipe(takeWhile((/**
         * @return {?}
         */
        () => this.shouldPing)))
            .subscribe((/**
         * @param {?} connected
         * @return {?}
         */
        connected => {
            if (connected) {
                this.statusClass = 'connected login-support-text login-connection-status';
                this.connectedStatus = 'Connected';
            }
            else {
                this.statusClass = 'not-connected login-support-text login-connection-status';
                this.connectedStatus = 'Not connected';
            }
        }));
    }
    /**
     * @param {?} passwordLength
     * @return {?}
     */
    inputChanged(passwordLength) {
        if (passwordLength.length) {
            this.loginFailed = false;
        }
    }
    /**
     * @return {?}
     */
    sendEmail() {
        location.href = 'mailto:surveys@magvar.com';
    }
    /**
     * @return {?}
     */
    login() {
        this.auth
            .login(this.username, this.password)
            .subscribe((/**
         * @param {?} res
         * @return {?}
         */
        res => {
            if (res) {
                this.router.navigate(this.afterLoginRoute);
            }
            else {
                this.password = null;
                this.loginFailed = true;
            }
        }), (/**
         * @param {?} err
         * @return {?}
         */
        err => {
            this.password = null;
            this.loginFailed = true;
        }));
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        this.shouldPing = false;
    }
}
SplashComponent.decorators = [
    { type: Component, args: [{
                selector: 'app-splash',
                template: "<div class=\"background\">\n\n    <!--top-->\n    <div class=\"material-masthead-login\">\n      <div class=\"material-inner-masthead\">\n        <div class=\"logo-max\">\n          <img class=\"login-logo\" src=\"assets/saphira-logo-secondary.png\">\n          <h4 class=\"login-support-text\">Survey Management</h4>\n          <h4 [class]=\"this.statusClass\">{{connectedStatus}}</h4>\n        </div>\n      </div>\n    </div>\n\n    <div class=\"login-flex\" style=\"float: right;\">\n      <div class=\"login-box\">\n        <form (ngSubmit)=\"login()\" #loginForm=\"ngForm\" class=\"login-form\" id=\"loginme\">\n          <div class=\"input-container\">\n            <mat-form-field color=\"primary\">\n              <input matInput type=\"text\" id=\"username\" required [(ngModel)]=\"username\" name=\"username\" placeholder=\"Username\">\n            </mat-form-field>\n            <mat-form-field color=\"primary\">\n              <input matInput type=\"password\" id=\"password\" required [(ngModel)]=\"password\" (ngModelChange)=\"inputChanged($event)\"\n                placeholder=\"Password\" name=\"password\">\n            </mat-form-field>\n          </div>\n          <div class=\"login-btn-container\">\n            <button mat-raised-button class=\"login-btn\" [ngClass]=\"{'valid': loginForm.form.valid}\" type=\"submit\"\n              [disabled]=\"!loginForm.form.valid\" id=\"login-button\">\n              <span class=\"mat-button-wrapper\">Login</span>\n              <div class=\"mat-button-ripple mat-ripple\">\n              </div>\n              <div class=\"mat-button-focus-overlay\">\n              </div>\n            </button>\n          </div>\n        </form>\n        <span class=\"login-credentials-error\" *ngIf=\"loginFailed\">Invalid credentials, please try again</span>\n      </div>\n    </div>\n    <div class=\"login-support\">\n      <h4 class=\"login-support-text\">MagVAR Real-Time Center</h4>\n      <button class=\"menu-button phone-icon\" mat-icon-button matTooltip=\"Direct: 303-264-6380, Cell: 720-384-7649\">\n        <mat-icon>phone</mat-icon>\n      </button>\n      <button class=\"menu-button email-icon\" mat-icon-button matTooltip=\"Email directly at: surveys@magvar.com\" (click)=\"sendEmail()\">\n        <mat-icon>email</mat-icon>\n      </button>\n    </div>\n    <div class=\"copyright-container\">\n      <p class=\"copyright-text\">Copyright \u00A9 Magnetic Variations Services LLC 2019</p>\n    </div>\n  </div>\n",
                styles: ["@media print{.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-appearance-legacy.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-1.28122em) scale(.75);transform:translateY(-1.28122em) scale(.75)}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-form-field-autofill-control:-webkit-autofill+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.28121em) scale(.75);transform:translateY(-1.28121em) scale(.75)}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.2812em) scale(.75);transform:translateY(-1.2812em) scale(.75)}}.mat-badge-content{font-weight:600;font-size:12px;font-family:NunitoSans,sans-serif}.mat-badge-small .mat-badge-content{font-size:9px}.mat-badge-large .mat-badge-content{font-size:24px}.mat-h1,.mat-headline,.mat-typography h1{font:400 24px/32px NunitoSans,sans-serif;margin:0 0 16px}.mat-h2,.mat-title,.mat-typography h2{font:500 20px/32px NunitoSans,sans-serif;margin:0 0 16px}.mat-h3,.mat-subheading-2,.mat-typography h3{font:400 16px/28px NunitoSans,sans-serif;margin:0 0 16px}.mat-h4,.mat-subheading-1,.mat-typography h4{font:400 15px/24px NunitoSans,sans-serif;margin:0 0 16px}.mat-h5,.mat-h6,.mat-typography h5,.mat-typography h6{margin:0 0 12px}.mat-body-2,.mat-body-strong{font:500 14px/24px NunitoSans,sans-serif}.mat-body,.mat-body-1,.mat-typography{font:400 14px/20px NunitoSans,sans-serif}.mat-body p,.mat-body-1 p,.mat-typography p{margin:0 0 12px}.mat-caption,.mat-small{font:400 12px/20px NunitoSans,sans-serif}.mat-display-4,.mat-typography .mat-display-4{font:300 112px/112px NunitoSans,sans-serif;letter-spacing:-.05em;margin:0 0 56px}.mat-display-3,.mat-typography .mat-display-3{font:400 56px/56px NunitoSans,sans-serif;letter-spacing:-.02em;margin:0 0 64px}.mat-display-2,.mat-typography .mat-display-2{font:400 45px/48px NunitoSans,sans-serif;letter-spacing:-.005em;margin:0 0 64px}.mat-display-1,.mat-typography .mat-display-1{font:400 34px/40px NunitoSans,sans-serif;margin:0 0 64px}.mat-bottom-sheet-container{font:400 14px/20px NunitoSans,sans-serif}.mat-button,.mat-fab,.mat-flat-button,.mat-icon-button,.mat-mini-fab,.mat-raised-button,.mat-stroked-button{font-family:NunitoSans,sans-serif;font-size:14px;font-weight:500}.mat-button-toggle,.mat-card{font-family:NunitoSans,sans-serif}.mat-card-title{font-size:24px;font-weight:500}.mat-card-header .mat-card-title{font-size:20px}.mat-card-content,.mat-card-subtitle{font-size:14px}.mat-checkbox{font-family:NunitoSans,sans-serif}.mat-checkbox-layout .mat-checkbox-label{line-height:24px}.mat-chip{font-size:14px;font-weight:500}.mat-chip .mat-chip-remove.mat-icon,.mat-chip .mat-chip-trailing-icon.mat-icon{font-size:18px}.mat-table{font-family:NunitoSans,sans-serif}.mat-header-cell{font-size:12px;font-weight:500}.mat-cell,.mat-footer-cell{font-size:14px}.mat-calendar{font-family:NunitoSans,sans-serif}.mat-calendar-body{font-size:13px}.mat-calendar-body-label,.mat-calendar-period-button{font-size:14px;font-weight:500}.mat-calendar-table-header th{font-size:11px;font-weight:400}.mat-dialog-title{font:500 20px/32px NunitoSans,sans-serif}.mat-expansion-panel-header{font-family:NunitoSans,sans-serif;font-size:15px;font-weight:400}.mat-expansion-panel-content{font:400 14px/20px NunitoSans,sans-serif}.mat-form-field{font-size:inherit;font-weight:400;line-height:1.125;font-family:NunitoSans,sans-serif}.mat-form-field-wrapper{padding-bottom:1.34375em}.mat-form-field-prefix .mat-icon,.mat-form-field-suffix .mat-icon{font-size:150%;line-height:1.125}.mat-form-field-prefix .mat-icon-button,.mat-form-field-suffix .mat-icon-button{height:1.5em;width:1.5em}.mat-form-field-prefix .mat-icon-button .mat-icon,.mat-form-field-suffix .mat-icon-button .mat-icon{height:1.125em;line-height:1.125}.mat-form-field-infix{padding:.5em 0;border-top:.84375em solid transparent}.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-1.34373em) scale(.75);transform:translateY(-1.34373em) scale(.75);width:133.3333533333%}.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.34372em) scale(.75);transform:translateY(-1.34372em) scale(.75);width:133.3333633333%}.mat-form-field-label-wrapper{top:-.84375em;padding-top:.84375em}.mat-form-field-label{top:1.34375em}.mat-form-field-underline{bottom:1.34375em}.mat-form-field-subscript-wrapper{font-size:75%;margin-top:.6666666667em;top:calc(100% - 1.7916666667em)}.mat-form-field-appearance-legacy .mat-form-field-wrapper{padding-bottom:1.25em}.mat-form-field-appearance-legacy .mat-form-field-infix{padding:.4375em 0}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-appearance-legacy.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00106px);transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00106px);-ms-transform:translateY(-1.28119em) scale(.75);width:133.3333933333%}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-form-field-autofill-control:-webkit-autofill+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00107px);transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00107px);-ms-transform:translateY(-1.28118em) scale(.75);width:133.3334033333%}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00108px);transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00108px);-ms-transform:translateY(-1.28117em) scale(.75);width:133.3334133333%}.mat-form-field-appearance-legacy .mat-form-field-label{top:1.28125em}.mat-form-field-appearance-legacy .mat-form-field-underline{bottom:1.25em}.mat-form-field-appearance-legacy .mat-form-field-subscript-wrapper{margin-top:.5416666667em;top:calc(100% - 1.6666666667em)}@media print{.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-appearance-legacy.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-1.28116em) scale(.75);transform:translateY(-1.28116em) scale(.75)}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-form-field-autofill-control:-webkit-autofill+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.28115em) scale(.75);transform:translateY(-1.28115em) scale(.75)}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.28114em) scale(.75);transform:translateY(-1.28114em) scale(.75)}}.mat-form-field-appearance-fill .mat-form-field-infix{padding:.25em 0 .75em}.mat-form-field-appearance-fill .mat-form-field-label{top:1.09375em;margin-top:-.5em}.mat-form-field-appearance-fill.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-appearance-fill.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-.59373em) scale(.75);transform:translateY(-.59373em) scale(.75);width:133.3333533333%}.mat-form-field-appearance-fill.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-.59372em) scale(.75);transform:translateY(-.59372em) scale(.75);width:133.3333633333%}.mat-form-field-appearance-outline .mat-form-field-infix{padding:1em 0}.mat-form-field-appearance-outline .mat-form-field-label{top:1.84375em;margin-top:-.25em}.mat-form-field-appearance-outline.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-appearance-outline.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-1.59373em) scale(.75);transform:translateY(-1.59373em) scale(.75);width:133.3333533333%}.mat-form-field-appearance-outline.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.59372em) scale(.75);transform:translateY(-1.59372em) scale(.75);width:133.3333633333%}.mat-grid-tile-footer,.mat-grid-tile-header{font-size:14px}.mat-grid-tile-footer .mat-line,.mat-grid-tile-header .mat-line{white-space:nowrap;overflow:hidden;text-overflow:ellipsis;display:block;box-sizing:border-box}.mat-grid-tile-footer .mat-line:nth-child(n+2),.mat-grid-tile-header .mat-line:nth-child(n+2){font-size:12px}input.mat-input-element{margin-top:-.0625em}.mat-menu-item{font-family:NunitoSans,sans-serif;font-size:14px;font-weight:400}.mat-paginator,.mat-paginator-page-size .mat-select-trigger{font-family:NunitoSans,sans-serif;font-size:12px}.mat-radio-button,.mat-select{font-family:NunitoSans,sans-serif}.mat-select-trigger{height:1.125em}.mat-slide-toggle-content{font-family:NunitoSans,sans-serif}.mat-slider-thumb-label-text{font-family:NunitoSans,sans-serif;font-size:12px;font-weight:500}.mat-stepper-horizontal,.mat-stepper-vertical{font-family:NunitoSans,sans-serif}.mat-step-label{font-size:14px;font-weight:400}.mat-step-sub-label-error{font-weight:400}.mat-step-label-error{font-size:14px}.mat-step-label-selected{font-size:14px;font-weight:500}.mat-tab-group{font-family:NunitoSans,sans-serif}.mat-tab-label,.mat-tab-link{font-family:NunitoSans,sans-serif;font-size:14px;font-weight:500}.mat-toolbar,.mat-toolbar h1,.mat-toolbar h2,.mat-toolbar h3,.mat-toolbar h4,.mat-toolbar h5,.mat-toolbar h6{font:500 20px/32px NunitoSans,sans-serif;margin:0}.mat-tooltip{font-family:NunitoSans,sans-serif;font-size:10px;padding-top:6px;padding-bottom:6px}.mat-tooltip-handset{font-size:14px;padding-top:8px;padding-bottom:8px}.mat-list-item,.mat-list-option{font-family:NunitoSans,sans-serif}.mat-list-base .mat-list-item{font-size:16px}.mat-list-base .mat-list-item .mat-line{white-space:nowrap;overflow:hidden;text-overflow:ellipsis;display:block;box-sizing:border-box}.mat-list-base .mat-list-item .mat-line:nth-child(n+2){font-size:14px}.mat-list-base .mat-list-option{font-size:16px}.mat-list-base .mat-list-option .mat-line{white-space:nowrap;overflow:hidden;text-overflow:ellipsis;display:block;box-sizing:border-box}.mat-list-base .mat-list-option .mat-line:nth-child(n+2){font-size:14px}.mat-list-base .mat-subheader{font-family:NunitoSans,sans-serif;font-size:14px;font-weight:500}.mat-list-base[dense] .mat-list-item{font-size:12px}.mat-list-base[dense] .mat-list-item .mat-line{white-space:nowrap;overflow:hidden;text-overflow:ellipsis;display:block;box-sizing:border-box}.mat-list-base[dense] .mat-list-item .mat-line:nth-child(n+2),.mat-list-base[dense] .mat-list-option{font-size:12px}.mat-list-base[dense] .mat-list-option .mat-line{white-space:nowrap;overflow:hidden;text-overflow:ellipsis;display:block;box-sizing:border-box}.mat-list-base[dense] .mat-list-option .mat-line:nth-child(n+2){font-size:12px}.mat-list-base[dense] .mat-subheader{font-family:NunitoSans,sans-serif;font-size:12px;font-weight:500}.mat-option{font-family:NunitoSans,sans-serif;font-size:16px}.mat-optgroup-label{font:500 14px/24px NunitoSans,sans-serif}.mat-simple-snackbar{font-family:NunitoSans,sans-serif;font-size:14px}.mat-simple-snackbar-action{line-height:1;font-family:inherit;font-size:inherit;font-weight:500}.mat-tree{font-family:NunitoSans,sans-serif}.mat-nested-tree-node,.mat-tree-node{font-weight:400;font-size:14px}.mat-ripple{overflow:hidden;position:relative}.mat-ripple.mat-ripple-unbounded{overflow:visible}.mat-ripple-element{position:absolute;border-radius:50%;pointer-events:none;-webkit-transition:opacity,-webkit-transform cubic-bezier(0,0,.2,1);transition:opacity,transform cubic-bezier(0,0,.2,1),-webkit-transform cubic-bezier(0,0,.2,1);-webkit-transform:scale(0);transform:scale(0)}@media (-ms-high-contrast:active){.mat-ripple-element{display:none}}.cdk-visually-hidden{border:0;clip:rect(0 0 0 0);height:1px;margin:-1px;overflow:hidden;padding:0;position:absolute;width:1px;outline:0;-webkit-appearance:none;-moz-appearance:none}.cdk-global-overlay-wrapper,.cdk-overlay-container{pointer-events:none;top:0;left:0;height:100%;width:100%}.cdk-overlay-container{position:fixed;z-index:1000}.cdk-overlay-container:empty{display:none}.cdk-global-overlay-wrapper{display:-webkit-box;display:flex;position:absolute;z-index:1000}.cdk-overlay-pane{position:absolute;pointer-events:auto;box-sizing:border-box;z-index:1000;display:-webkit-box;display:flex;max-width:100%;max-height:100%}.cdk-overlay-backdrop{position:absolute;top:0;bottom:0;left:0;right:0;z-index:1000;pointer-events:auto;-webkit-tap-highlight-color:transparent;-webkit-transition:opacity .4s cubic-bezier(.25,.8,.25,1);transition:opacity .4s cubic-bezier(.25,.8,.25,1);opacity:0}.cdk-overlay-backdrop.cdk-overlay-backdrop-showing{opacity:1}@media screen and (-ms-high-contrast:active){.cdk-overlay-backdrop.cdk-overlay-backdrop-showing{opacity:.6}}.cdk-overlay-dark-backdrop{background:rgba(0,0,0,.32)}.cdk-overlay-transparent-backdrop,.cdk-overlay-transparent-backdrop.cdk-overlay-backdrop-showing{opacity:0}.cdk-overlay-connected-position-bounding-box{position:absolute;z-index:1000;display:-webkit-box;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;flex-direction:column;min-width:1px;min-height:1px}.cdk-global-scrollblock{position:fixed;width:100%;overflow-y:scroll}@-webkit-keyframes cdk-text-field-autofill-start{/*!*/}@keyframes cdk-text-field-autofill-start{/*!*/}@-webkit-keyframes cdk-text-field-autofill-end{/*!*/}@keyframes cdk-text-field-autofill-end{/*!*/}.cdk-text-field-autofill-monitored:-webkit-autofill{-webkit-animation-name:cdk-text-field-autofill-start;animation-name:cdk-text-field-autofill-start}.cdk-text-field-autofill-monitored:not(:-webkit-autofill){-webkit-animation-name:cdk-text-field-autofill-end;animation-name:cdk-text-field-autofill-end}textarea.cdk-textarea-autosize{resize:none}textarea.cdk-textarea-autosize-measuring{height:auto!important;overflow:hidden!important;padding:2px 0!important;box-sizing:content-box!important}input:-webkit-autofill,input:-webkit-autofill:active,input:-webkit-autofill:focus,input:-webkit-autofill:hover{-webkit-transition:background-color 5000s ease-in-out 500s;transition:background-color 5000s ease-in-out 500s;background-color:#fff;font-size:.8em;padding:5px}.material-inner-masthead{padding:15px}.logo-max{position:absolute;margin:auto;padding:135px 0 0;text-align:center}.logo-custom-login{position:absolute;top:43px;left:229px}.symphony-version-login{color:#fff;z-index:9887;position:fixed;font-size:1.2em;margin:28px 0 0 163px}.input-container{display:-webkit-box;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;flex-direction:column}.login-h1{font-size:2.2em;color:#fff;line-height:2em;font-weight:300;font-family:Poppins,sans-serif}.login-h2{font-size:3em;color:#bdbdbd;font-weight:300;list-style-type:none;line-height:1.2em}.login-h3{color:#bdbdbd;font-weight:300;font-size:1.4em;list-style-type:none;line-height:1.5em}.login-flex{display:-webkit-box;display:flex;-webkit-box-align:center;align-items:center;-webkit-box-pack:center;justify-content:center;margin-top:135px}.login-box{z-index:2;border-radius:6px;line-height:2em;font-size:1.45em;width:285px;height:270px;display:-webkit-box;display:flex;padding:0 40px 30px 0;-webkit-box-orient:vertical;-webkit-box-direction:normal;flex-direction:column;-webkit-box-pack:right;justify-content:right}.login-form{display:-webkit-box;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;flex-direction:column;-webkit-box-align:center;align-items:center}.login-btn-container{float:right;padding:20px 0 0}.login-btn{background-color:#1e4e79;color:#fff;float:right;font-weight:800;letter-spacing:1px;text-transform:uppercase}.login-btn .mat-button-wrapper{font-size:14px!important}.login-btn.valid:hover{background:rgba(189,189,189,.75);color:#233538;text-decoration:none}.login-label{color:#000;font-size:.75em;font-weight:300;text-decoration:none;float:right;margin:10px 50px 0 0;display:-webkit-box;display:flex}.login-credentials-error{font-size:.65em;color:#48abd6;border-radius:5px;font-weight:600;padding:0;width:100%;font-style:italic;float:right;display:block;text-align:center}.background{width:700px;height:600px;background-color:#fff;background-size:cover;padding:0;overflow:hidden;margin:0 auto;box-shadow:-6px 6px 15px 2px rgba(0,0,0,.4)}.background-overlay{display:-webkit-box;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;flex-direction:column;border-radius:8px;bottom:0;width:100%;height:100%}.mat-button-wrapper{font-size:12px!important}.mat-accent.mat-input-element ::ng-deep .mat-input-placeholder{color:#143450}.mat-accent.mat-input-element ::ng-deep .mat-form-field-required-marker{color:#143450}.mat-accent.mat-input-element ::ng-deep .mat-form-field-ripple{background-color:#143450}.mat-input-container.ng-touched.ng-invalid ::ng-deep .mat-input-placeholder{color:#143450}.mat-input-container.ng-touched.ng-invalid ::ng-deep .mat-form-field-required-marker{color:#143450}.mat-input-container.ng-touched.ng-invalid ::ng-deep .mat-form-field-ripple{background-color:#143450}:host ::ng-deep .mat-input-placeholder{font-size:large;font-family:Poppins,sans-serif}.login-support{display:-webkit-inline-box;display:inline-flex;-webkit-box-align:center;align-items:center;right:0;width:100%;-webkit-box-pack:center;justify-content:center}.login-support-text{font-family:NunitoSans,sans-serif;font-weight:200}.login-logo{width:370px}.connected{background-color:#00ff40;color:#000;width:94px;text-align:center;margin:10px auto;border-radius:12px;padding:4px}.not-connected{background-color:#ff5454;color:#000;width:120px;text-align:center;margin:10px auto;border-radius:12px;padding:4px}.copyright-container{display:-webkit-box;display:flex;-webkit-box-pack:center;justify-content:center;height:81px;-webkit-box-align:end;align-items:flex-end}.copyright-text{font-family:NunitoSans;font-size:12px;letter-spacing:.2px;color:rgba(0,0,0,.7)}"]
            }] }
];
/** @nocollapse */
SplashComponent.ctorParameters = () => [
    { type: AuthService },
    { type: MatDialog },
    { type: Router },
    { type: PingService },
    { type: ClientRequisitesService },
    { type: TitleFlasherService }
];
if (false) {
    /** @type {?} */
    SplashComponent.prototype.username;
    /** @type {?} */
    SplashComponent.prototype.password;
    /** @type {?} */
    SplashComponent.prototype.versionName;
    /** @type {?} */
    SplashComponent.prototype.logoPath;
    /** @type {?} */
    SplashComponent.prototype.loginFailed;
    /** @type {?} */
    SplashComponent.prototype.connectedStatus;
    /** @type {?} */
    SplashComponent.prototype.statusClass;
    /** @type {?} */
    SplashComponent.prototype.shouldPing;
    /**
     * @type {?}
     * @private
     */
    SplashComponent.prototype.afterLoginRoute;
    /**
     * @type {?}
     * @private
     */
    SplashComponent.prototype.auth;
    /**
     * @type {?}
     * @private
     */
    SplashComponent.prototype.matDialog;
    /**
     * @type {?}
     * @private
     */
    SplashComponent.prototype.router;
    /**
     * @type {?}
     * @private
     */
    SplashComponent.prototype.ping;
    /**
     * @type {?}
     * @private
     */
    SplashComponent.prototype.requisiteServ;
    /**
     * @type {?}
     * @private
     */
    SplashComponent.prototype.title;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3BsYXNoLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BocHRlY2gvc2FwaGlyYS1zaGFyZWQtbGliLyIsInNvdXJjZXMiOlsibGliL2NvbXBvbmVudHMvc3BsYXNoL3NwbGFzaC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQXFCLE1BQU0sZUFBZSxDQUFDO0FBQzdELE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSw2QkFBNkIsQ0FBQztBQUMxRCxPQUFPLEVBQUUsTUFBTSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFDekMsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLDZCQUE2QixDQUFDO0FBQzFELE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUMzQyxPQUFPLEVBQUUsdUJBQXVCLEVBQUUsTUFBTSwwQ0FBMEMsQ0FBQztBQUNuRixPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sbUJBQW1CLENBQUM7QUFDOUMsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sc0NBQXNDLENBQUM7QUFPM0UsTUFBTSxPQUFPLGVBQWU7Ozs7Ozs7OztJQWExQixZQUFvQixJQUFpQixFQUNqQixTQUFvQixFQUNwQixNQUFjLEVBQ2QsSUFBaUIsRUFDakIsYUFBc0MsRUFDdEMsS0FBMEI7UUFMMUIsU0FBSSxHQUFKLElBQUksQ0FBYTtRQUNqQixjQUFTLEdBQVQsU0FBUyxDQUFXO1FBQ3BCLFdBQU0sR0FBTixNQUFNLENBQVE7UUFDZCxTQUFJLEdBQUosSUFBSSxDQUFhO1FBQ2pCLGtCQUFhLEdBQWIsYUFBYSxDQUF5QjtRQUN0QyxVQUFLLEdBQUwsS0FBSyxDQUFxQjtRQWJ2QyxnQkFBVyxHQUFHLEtBQUssQ0FBQztRQUNwQixvQkFBZSxHQUFHLGVBQWUsQ0FBQztRQUNsQyxnQkFBVyxHQUFHLDBEQUEwRCxDQUFDO1FBRXpFLGVBQVUsR0FBRyxJQUFJLENBQUM7UUFFakIsb0JBQWUsR0FBYSxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBUTNCLENBQUM7Ozs7SUFFaEIsUUFBUTtRQUNOLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxFQUFFLENBQUM7UUFDMUIsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLENBQUM7UUFFbkMsSUFBSSxDQUFDLElBQUk7YUFDSixNQUFNLEVBQUU7YUFDUixJQUFJLENBQ0gsU0FBUzs7O1FBQUUsR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRSxDQUNuQzthQUNBLFNBQVM7Ozs7UUFBRSxTQUFTLENBQUMsRUFBRTtZQUN0QixJQUFLLFNBQVMsRUFBRztnQkFDZixJQUFJLENBQUMsV0FBVyxHQUFHLHNEQUFzRCxDQUFDO2dCQUMxRSxJQUFJLENBQUMsZUFBZSxHQUFHLFdBQVcsQ0FBQzthQUNwQztpQkFBTTtnQkFDTCxJQUFJLENBQUMsV0FBVyxHQUFHLDBEQUEwRCxDQUFDO2dCQUM5RSxJQUFJLENBQUMsZUFBZSxHQUFHLGVBQWUsQ0FBQzthQUN4QztRQUNILENBQUMsRUFBQyxDQUFDO0lBQ1AsQ0FBQzs7Ozs7SUFFSCxZQUFZLENBQUMsY0FBYztRQUN6QixJQUFJLGNBQWMsQ0FBQyxNQUFNLEVBQUU7WUFDekIsSUFBSSxDQUFDLFdBQVcsR0FBRyxLQUFLLENBQUM7U0FDMUI7SUFDSCxDQUFDOzs7O0lBRU0sU0FBUztRQUNkLFFBQVEsQ0FBQyxJQUFJLEdBQUcsMkJBQTJCLENBQUM7SUFDOUMsQ0FBQzs7OztJQUVELEtBQUs7UUFDSCxJQUFJLENBQUMsSUFBSTthQUNKLEtBQUssQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxRQUFRLENBQUM7YUFDbkMsU0FBUzs7OztRQUNSLEdBQUcsQ0FBQyxFQUFFO1lBQ0osSUFBSSxHQUFHLEVBQUU7Z0JBQ1AsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDO2FBQzVDO2lCQUFNO2dCQUNMLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO2dCQUNyQixJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQzthQUN6QjtRQUNILENBQUM7Ozs7UUFDRCxHQUFHLENBQUMsRUFBRTtZQUNKLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO1lBQ3JCLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDO1FBQzFCLENBQUMsRUFDRixDQUFDO0lBQ1IsQ0FBQzs7OztJQUVELFdBQVc7UUFDVCxJQUFJLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQztJQUMxQixDQUFDOzs7WUE3RUYsU0FBUyxTQUFDO2dCQUNULFFBQVEsRUFBRSxZQUFZO2dCQUN0QiwrN0VBQXNDOzthQUV2Qzs7OztZQVpRLFdBQVc7WUFLWCxTQUFTO1lBSlQsTUFBTTtZQUNOLFdBQVc7WUFFWCx1QkFBdUI7WUFFdkIsbUJBQW1COzs7O0lBUTFCLG1DQUF3Qjs7SUFDeEIsbUNBQXdCOztJQUN4QixzQ0FBMkI7O0lBQzNCLG1DQUF3Qjs7SUFDeEIsc0NBQTJCOztJQUMzQiwwQ0FBeUM7O0lBQ3pDLHNDQUFnRjs7SUFFaEYscUNBQXlCOzs7OztJQUV6QiwwQ0FBMEM7Ozs7O0lBRTlCLCtCQUF5Qjs7Ozs7SUFDekIsb0NBQTRCOzs7OztJQUM1QixpQ0FBc0I7Ozs7O0lBQ3RCLCtCQUF5Qjs7Ozs7SUFDekIsd0NBQThDOzs7OztJQUM5QyxnQ0FBa0MiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgT25EZXN0cm95IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBBdXRoU2VydmljZSB9IGZyb20gJy4uLy4uL3NlcnZpY2VzL2F1dGguc2VydmljZSc7XG5pbXBvcnQgeyBSb3V0ZXIgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xuaW1wb3J0IHsgUGluZ1NlcnZpY2UgfSBmcm9tICcuLi8uLi9zZXJ2aWNlcy9waW5nLnNlcnZpY2UnO1xuaW1wb3J0IHsgdGFrZVdoaWxlIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xuaW1wb3J0IHsgQ2xpZW50UmVxdWlzaXRlc1NlcnZpY2UgfSBmcm9tICcuLi8uLi9zZXJ2aWNlcy9jbGllbnQtcmVxdWlzaXRlcy5zZXJ2aWNlJztcbmltcG9ydCB7IE1hdERpYWxvZyB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsJztcbmltcG9ydCB7IFRpdGxlRmxhc2hlclNlcnZpY2UgfSBmcm9tICcuLi8uLi9zZXJ2aWNlcy90aXRsZS1mbGFzaGVyLXNlcnZpY2UnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdhcHAtc3BsYXNoJyxcbiAgdGVtcGxhdGVVcmw6ICcuL3NwbGFzaC5jb21wb25lbnQuaHRtbCcsXG4gIHN0eWxlVXJsczogWycuL3NwbGFzaC5jb21wb25lbnQuc2NzcyddXG59KVxuZXhwb3J0IGNsYXNzIFNwbGFzaENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgT25EZXN0cm95IHtcbiAgcHVibGljIHVzZXJuYW1lOiBzdHJpbmc7XG4gIHB1YmxpYyBwYXNzd29yZDogc3RyaW5nO1xuICBwdWJsaWMgdmVyc2lvbk5hbWU6IHN0cmluZztcbiAgcHVibGljIGxvZ29QYXRoOiBzdHJpbmc7XG4gIHB1YmxpYyBsb2dpbkZhaWxlZCA9IGZhbHNlO1xuICBwdWJsaWMgY29ubmVjdGVkU3RhdHVzID0gJ05vdCBjb25uZWN0ZWQnO1xuICBwdWJsaWMgc3RhdHVzQ2xhc3MgPSAnbm90LWNvbm5lY3RlZCBsb2dpbi1zdXBwb3J0LXRleHQgbG9naW4tY29ubmVjdGlvbi1zdGF0dXMnO1xuXG4gIHB1YmxpYyBzaG91bGRQaW5nID0gdHJ1ZTtcblxuICBwcml2YXRlIGFmdGVyTG9naW5Sb3V0ZTogc3RyaW5nW10gPSBbJy8nXTtcblxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIGF1dGg6IEF1dGhTZXJ2aWNlLFxuICAgICAgICAgICAgICBwcml2YXRlIG1hdERpYWxvZzogTWF0RGlhbG9nLFxuICAgICAgICAgICAgICBwcml2YXRlIHJvdXRlcjogUm91dGVyLFxuICAgICAgICAgICAgICBwcml2YXRlIHBpbmc6IFBpbmdTZXJ2aWNlLFxuICAgICAgICAgICAgICBwcml2YXRlIHJlcXVpc2l0ZVNlcnY6IENsaWVudFJlcXVpc2l0ZXNTZXJ2aWNlLFxuICAgICAgICAgICAgICBwcml2YXRlIHRpdGxlOiBUaXRsZUZsYXNoZXJTZXJ2aWNlXG4gICAgICAgICAgICAgICkge31cblxuICBuZ09uSW5pdCgpIHtcbiAgICB0aGlzLm1hdERpYWxvZy5jbG9zZUFsbCgpO1xuICAgIHRoaXMudGl0bGUuc2V0VGl0bGUoJ1NhcGhpcmEgMy4wJyk7XG5cbiAgICB0aGlzLnBpbmdcbiAgICAgICAgLnN0YXR1cygpXG4gICAgICAgIC5waXBlKFxuICAgICAgICAgIHRha2VXaGlsZSggKCkgPT4gdGhpcy5zaG91bGRQaW5nIClcbiAgICAgICAgKVxuICAgICAgICAuc3Vic2NyaWJlKCBjb25uZWN0ZWQgPT4ge1xuICAgICAgICAgIGlmICggY29ubmVjdGVkICkge1xuICAgICAgICAgICAgdGhpcy5zdGF0dXNDbGFzcyA9ICdjb25uZWN0ZWQgbG9naW4tc3VwcG9ydC10ZXh0IGxvZ2luLWNvbm5lY3Rpb24tc3RhdHVzJztcbiAgICAgICAgICAgIHRoaXMuY29ubmVjdGVkU3RhdHVzID0gJ0Nvbm5lY3RlZCc7XG4gICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHRoaXMuc3RhdHVzQ2xhc3MgPSAnbm90LWNvbm5lY3RlZCBsb2dpbi1zdXBwb3J0LXRleHQgbG9naW4tY29ubmVjdGlvbi1zdGF0dXMnO1xuICAgICAgICAgICAgdGhpcy5jb25uZWN0ZWRTdGF0dXMgPSAnTm90IGNvbm5lY3RlZCc7XG4gICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICB9XG5cbiAgaW5wdXRDaGFuZ2VkKHBhc3N3b3JkTGVuZ3RoKTogdm9pZCB7XG4gICAgaWYgKHBhc3N3b3JkTGVuZ3RoLmxlbmd0aCkge1xuICAgICAgdGhpcy5sb2dpbkZhaWxlZCA9IGZhbHNlO1xuICAgIH1cbiAgfVxuXG4gIHB1YmxpYyBzZW5kRW1haWwoKSB7XG4gICAgbG9jYXRpb24uaHJlZiA9ICdtYWlsdG86c3VydmV5c0BtYWd2YXIuY29tJztcbiAgfVxuXG4gIGxvZ2luKCk6IHZvaWQge1xuICAgIHRoaXMuYXV0aFxuICAgICAgICAubG9naW4odGhpcy51c2VybmFtZSwgdGhpcy5wYXNzd29yZClcbiAgICAgICAgLnN1YnNjcmliZShcbiAgICAgICAgICByZXMgPT4ge1xuICAgICAgICAgICAgaWYgKHJlcykge1xuICAgICAgICAgICAgICB0aGlzLnJvdXRlci5uYXZpZ2F0ZSh0aGlzLmFmdGVyTG9naW5Sb3V0ZSk7XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICB0aGlzLnBhc3N3b3JkID0gbnVsbDtcbiAgICAgICAgICAgICAgdGhpcy5sb2dpbkZhaWxlZCA9IHRydWU7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfSxcbiAgICAgICAgICBlcnIgPT4ge1xuICAgICAgICAgICAgdGhpcy5wYXNzd29yZCA9IG51bGw7XG4gICAgICAgICAgICB0aGlzLmxvZ2luRmFpbGVkID0gdHJ1ZTtcbiAgICAgICAgICB9XG4gICAgICAgICk7XG4gIH1cblxuICBuZ09uRGVzdHJveSgpOiB2b2lkIHtcbiAgICB0aGlzLnNob3VsZFBpbmcgPSBmYWxzZTtcbiAgfVxufVxuIl19