/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MaterialModule } from '../material.module';
import { AgGridModule } from 'ag-grid-angular/main';
import { TableComponent } from './table/table.component';
import { RowButtonComponent } from './row-button.components';
import { PipesModule } from '../pipes/pipes.module';
import { NewFileCellRendererComponent } from './cell-renderers/new-file-cell-renderer/new-file-cell-renderer.component';
import { AutomationCellRendererComponent } from './cell-renderers/automation-cell-renderer/automation-cell-renderer.component';
import { ClickToolCellRendererComponent } from './cell-renderers/click-tool-cell-renderer/click-tool-cell-renderer.component';
import { FileTreeCellRendererComponent } from './cell-renderers/file-tree-cell-renderer/file-tree-cell-renderer.component';
import { CustomLoadingOverlayComponent } from './table/custom-loading-overlay.component';
import { DefinitiveTableErrorsComponent } from '../components/definitive-table-errors/definitive-table-errors.component';
import { DatePickerCellEditRendererComponent } from './cell-renderers/date-picker-cell-edit-renderer/date-picker-cell-edit-renderer.component';
import { DropdownCellRendererComponent } from './cell-renderers/dropdown-cell-renderer/dropdown-cell-renderer.component';
export class GridModule {
}
GridModule.decorators = [
    { type: NgModule, args: [{
                entryComponents: [TableComponent],
                imports: [
                    CommonModule,
                    AgGridModule.withComponents([
                        AutomationCellRendererComponent,
                        FileTreeCellRendererComponent,
                        NewFileCellRendererComponent,
                        RowButtonComponent,
                        CustomLoadingOverlayComponent,
                        ClickToolCellRendererComponent,
                        DatePickerCellEditRendererComponent,
                        DropdownCellRendererComponent
                    ]),
                    FormsModule,
                    MaterialModule,
                    PipesModule,
                ],
                exports: [
                    AgGridModule,
                    TableComponent,
                    DefinitiveTableErrorsComponent
                ],
                declarations: [
                    AutomationCellRendererComponent,
                    FileTreeCellRendererComponent,
                    NewFileCellRendererComponent,
                    RowButtonComponent,
                    TableComponent,
                    CustomLoadingOverlayComponent,
                    ClickToolCellRendererComponent,
                    DefinitiveTableErrorsComponent,
                    DatePickerCellEditRendererComponent,
                    DropdownCellRendererComponent
                ],
                providers: []
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ3JpZC5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AaHB0ZWNoL3NhcGhpcmEtc2hhcmVkLWxpYi8iLCJzb3VyY2VzIjpbImxpYi9ncmlkL2dyaWQubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUMvQyxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFDN0MsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLG9CQUFvQixDQUFDO0FBRXBELE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUVwRCxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0seUJBQXlCLENBQUM7QUFDekQsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0seUJBQXlCLENBQUM7QUFDN0QsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLHVCQUF1QixDQUFDO0FBQ3BELE9BQU8sRUFBRSw0QkFBNEIsRUFBRSxNQUFNLDBFQUEwRSxDQUFDO0FBQ3hILE9BQU8sRUFBRSwrQkFBK0IsRUFBRSxNQUFNLDhFQUE4RSxDQUFDO0FBQy9ILE9BQU8sRUFBRSw4QkFBOEIsRUFBRSxNQUFNLDhFQUE4RSxDQUFDO0FBQzlILE9BQU8sRUFBRSw2QkFBNkIsRUFBRSxNQUFNLDRFQUE0RSxDQUFDO0FBQzNILE9BQU8sRUFBRSw2QkFBNkIsRUFBRSxNQUFNLDBDQUEwQyxDQUFDO0FBQ3pGLE9BQU8sRUFBRSw4QkFBOEIsRUFBRSxNQUFNLHlFQUF5RSxDQUFDO0FBQ3pILE9BQU8sRUFBRSxtQ0FBbUMsRUFBRSxNQUFNLDBGQUEwRixDQUFDO0FBQy9JLE9BQU8sRUFBRSw2QkFBNkIsRUFBRSxNQUFNLDBFQUEwRSxDQUFDO0FBdUN6SCxNQUFNLE9BQU8sVUFBVTs7O1lBckN0QixRQUFRLFNBQUM7Z0JBQ1IsZUFBZSxFQUFFLENBQUMsY0FBYyxDQUFDO2dCQUNqQyxPQUFPLEVBQUU7b0JBQ1AsWUFBWTtvQkFDWixZQUFZLENBQUMsY0FBYyxDQUFDO3dCQUMxQiwrQkFBK0I7d0JBQy9CLDZCQUE2Qjt3QkFDN0IsNEJBQTRCO3dCQUM1QixrQkFBa0I7d0JBQ2xCLDZCQUE2Qjt3QkFDN0IsOEJBQThCO3dCQUM5QixtQ0FBbUM7d0JBQ25DLDZCQUE2QjtxQkFDOUIsQ0FBQztvQkFDRixXQUFXO29CQUNYLGNBQWM7b0JBQ2QsV0FBVztpQkFDWjtnQkFDRCxPQUFPLEVBQUU7b0JBQ1AsWUFBWTtvQkFDWixjQUFjO29CQUNkLDhCQUE4QjtpQkFDL0I7Z0JBQ0QsWUFBWSxFQUFFO29CQUNaLCtCQUErQjtvQkFDL0IsNkJBQTZCO29CQUM3Qiw0QkFBNEI7b0JBQzVCLGtCQUFrQjtvQkFDbEIsY0FBYztvQkFDZCw2QkFBNkI7b0JBQzdCLDhCQUE4QjtvQkFDOUIsOEJBQThCO29CQUM5QixtQ0FBbUM7b0JBQ25DLDZCQUE2QjtpQkFDOUI7Z0JBQ0QsU0FBUyxFQUFFLEVBQUU7YUFDZCIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xuaW1wb3J0IHsgRm9ybXNNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XG5pbXBvcnQgeyBNYXRlcmlhbE1vZHVsZSB9IGZyb20gJy4uL21hdGVyaWFsLm1vZHVsZSc7XG5cbmltcG9ydCB7IEFnR3JpZE1vZHVsZSB9IGZyb20gJ2FnLWdyaWQtYW5ndWxhci9tYWluJztcblxuaW1wb3J0IHsgVGFibGVDb21wb25lbnQgfSBmcm9tICcuL3RhYmxlL3RhYmxlLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBSb3dCdXR0b25Db21wb25lbnQgfSBmcm9tICcuL3Jvdy1idXR0b24uY29tcG9uZW50cyc7XG5pbXBvcnQgeyBQaXBlc01vZHVsZSB9IGZyb20gJy4uL3BpcGVzL3BpcGVzLm1vZHVsZSc7XG5pbXBvcnQgeyBOZXdGaWxlQ2VsbFJlbmRlcmVyQ29tcG9uZW50IH0gZnJvbSAnLi9jZWxsLXJlbmRlcmVycy9uZXctZmlsZS1jZWxsLXJlbmRlcmVyL25ldy1maWxlLWNlbGwtcmVuZGVyZXIuY29tcG9uZW50JztcbmltcG9ydCB7IEF1dG9tYXRpb25DZWxsUmVuZGVyZXJDb21wb25lbnQgfSBmcm9tICcuL2NlbGwtcmVuZGVyZXJzL2F1dG9tYXRpb24tY2VsbC1yZW5kZXJlci9hdXRvbWF0aW9uLWNlbGwtcmVuZGVyZXIuY29tcG9uZW50JztcbmltcG9ydCB7IENsaWNrVG9vbENlbGxSZW5kZXJlckNvbXBvbmVudCB9IGZyb20gJy4vY2VsbC1yZW5kZXJlcnMvY2xpY2stdG9vbC1jZWxsLXJlbmRlcmVyL2NsaWNrLXRvb2wtY2VsbC1yZW5kZXJlci5jb21wb25lbnQnO1xuaW1wb3J0IHsgRmlsZVRyZWVDZWxsUmVuZGVyZXJDb21wb25lbnQgfSBmcm9tICcuL2NlbGwtcmVuZGVyZXJzL2ZpbGUtdHJlZS1jZWxsLXJlbmRlcmVyL2ZpbGUtdHJlZS1jZWxsLXJlbmRlcmVyLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBDdXN0b21Mb2FkaW5nT3ZlcmxheUNvbXBvbmVudCB9IGZyb20gJy4vdGFibGUvY3VzdG9tLWxvYWRpbmctb3ZlcmxheS5jb21wb25lbnQnO1xuaW1wb3J0IHsgRGVmaW5pdGl2ZVRhYmxlRXJyb3JzQ29tcG9uZW50IH0gZnJvbSAnLi4vY29tcG9uZW50cy9kZWZpbml0aXZlLXRhYmxlLWVycm9ycy9kZWZpbml0aXZlLXRhYmxlLWVycm9ycy5jb21wb25lbnQnO1xuaW1wb3J0IHsgRGF0ZVBpY2tlckNlbGxFZGl0UmVuZGVyZXJDb21wb25lbnQgfSBmcm9tICcuL2NlbGwtcmVuZGVyZXJzL2RhdGUtcGlja2VyLWNlbGwtZWRpdC1yZW5kZXJlci9kYXRlLXBpY2tlci1jZWxsLWVkaXQtcmVuZGVyZXIuY29tcG9uZW50JztcbmltcG9ydCB7IERyb3Bkb3duQ2VsbFJlbmRlcmVyQ29tcG9uZW50IH0gZnJvbSAnLi9jZWxsLXJlbmRlcmVycy9kcm9wZG93bi1jZWxsLXJlbmRlcmVyL2Ryb3Bkb3duLWNlbGwtcmVuZGVyZXIuY29tcG9uZW50JztcblxuQE5nTW9kdWxlKHtcbiAgZW50cnlDb21wb25lbnRzOiBbVGFibGVDb21wb25lbnRdLFxuICBpbXBvcnRzOiBbXG4gICAgQ29tbW9uTW9kdWxlLFxuICAgIEFnR3JpZE1vZHVsZS53aXRoQ29tcG9uZW50cyhbXG4gICAgICBBdXRvbWF0aW9uQ2VsbFJlbmRlcmVyQ29tcG9uZW50LFxuICAgICAgRmlsZVRyZWVDZWxsUmVuZGVyZXJDb21wb25lbnQsXG4gICAgICBOZXdGaWxlQ2VsbFJlbmRlcmVyQ29tcG9uZW50LFxuICAgICAgUm93QnV0dG9uQ29tcG9uZW50LFxuICAgICAgQ3VzdG9tTG9hZGluZ092ZXJsYXlDb21wb25lbnQsXG4gICAgICBDbGlja1Rvb2xDZWxsUmVuZGVyZXJDb21wb25lbnQsXG4gICAgICBEYXRlUGlja2VyQ2VsbEVkaXRSZW5kZXJlckNvbXBvbmVudCxcbiAgICAgIERyb3Bkb3duQ2VsbFJlbmRlcmVyQ29tcG9uZW50XG4gICAgXSksXG4gICAgRm9ybXNNb2R1bGUsXG4gICAgTWF0ZXJpYWxNb2R1bGUsXG4gICAgUGlwZXNNb2R1bGUsXG4gIF0sXG4gIGV4cG9ydHM6IFtcbiAgICBBZ0dyaWRNb2R1bGUsXG4gICAgVGFibGVDb21wb25lbnQsXG4gICAgRGVmaW5pdGl2ZVRhYmxlRXJyb3JzQ29tcG9uZW50XG4gIF0sXG4gIGRlY2xhcmF0aW9uczogW1xuICAgIEF1dG9tYXRpb25DZWxsUmVuZGVyZXJDb21wb25lbnQsXG4gICAgRmlsZVRyZWVDZWxsUmVuZGVyZXJDb21wb25lbnQsXG4gICAgTmV3RmlsZUNlbGxSZW5kZXJlckNvbXBvbmVudCxcbiAgICBSb3dCdXR0b25Db21wb25lbnQsXG4gICAgVGFibGVDb21wb25lbnQsXG4gICAgQ3VzdG9tTG9hZGluZ092ZXJsYXlDb21wb25lbnQsXG4gICAgQ2xpY2tUb29sQ2VsbFJlbmRlcmVyQ29tcG9uZW50LFxuICAgIERlZmluaXRpdmVUYWJsZUVycm9yc0NvbXBvbmVudCxcbiAgICBEYXRlUGlja2VyQ2VsbEVkaXRSZW5kZXJlckNvbXBvbmVudCxcbiAgICBEcm9wZG93bkNlbGxSZW5kZXJlckNvbXBvbmVudFxuICBdLFxuICBwcm92aWRlcnM6IFtdXG59KVxuZXhwb3J0IGNsYXNzIEdyaWRNb2R1bGUge1xufVxuIl19