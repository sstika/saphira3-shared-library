/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component } from '@angular/core';
export class CustomLoadingOverlayComponent {
    /**
     * @param {?} params
     * @return {?}
     */
    agInit(params) {
        this.params = params;
    }
}
CustomLoadingOverlayComponent.decorators = [
    { type: Component, args: [{
                selector: 'app-loading-overlay',
                template: "<div class=\"ag-overlay-loading-center\" style=\"height: 9%\">\n  <mat-spinner></mat-spinner>\n  <div>{{this.params.loadingMessage}}</div>\n</div>\n"
            }] }
];
if (false) {
    /** @type {?} */
    CustomLoadingOverlayComponent.prototype.params;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY3VzdG9tLWxvYWRpbmctb3ZlcmxheS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AaHB0ZWNoL3NhcGhpcmEtc2hhcmVkLWxpYi8iLCJzb3VyY2VzIjpbImxpYi9ncmlkL3RhYmxlL2N1c3RvbS1sb2FkaW5nLW92ZXJsYXkuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFTLE1BQU0sZUFBZSxDQUFDO0FBT2pELE1BQU0sT0FBTyw2QkFBNkI7Ozs7O0lBSXRDLE1BQU0sQ0FBQyxNQUFNO1FBQ1QsSUFBSSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUM7SUFDekIsQ0FBQzs7O1lBVkosU0FBUyxTQUFDO2dCQUNQLFFBQVEsRUFBRSxxQkFBcUI7Z0JBQy9CLGdLQUFzRDthQUN6RDs7OztJQUdHLCtDQUFtQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgSW5wdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IElMb2FkaW5nT3ZlcmxheUFuZ3VsYXJDb21wIH0gZnJvbSAnYWctZ3JpZC1hbmd1bGFyJztcblxuQENvbXBvbmVudCh7XG4gICAgc2VsZWN0b3I6ICdhcHAtbG9hZGluZy1vdmVybGF5JyxcbiAgICB0ZW1wbGF0ZVVybDogJy4vY3VzdG9tLWxvYWRpbmctb3ZlcmxheS5jb21wb25lbnQuaHRtbCdcbn0pXG5leHBvcnQgY2xhc3MgQ3VzdG9tTG9hZGluZ092ZXJsYXlDb21wb25lbnQgaW1wbGVtZW50cyBJTG9hZGluZ092ZXJsYXlBbmd1bGFyQ29tcCB7XG5cbiAgICBwdWJsaWMgcGFyYW1zOiBhbnk7XG5cbiAgICBhZ0luaXQocGFyYW1zKTogdm9pZCB7XG4gICAgICAgIHRoaXMucGFyYW1zID0gcGFyYW1zO1xuICAgIH1cbn1cbiJdfQ==