/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { distinctUntilChanged } from 'rxjs/operators';
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Subject } from 'rxjs';
import * as _ from 'lodash';
import { TableDefaultsService } from './table-defaults.service';
import { CellRendererFactory } from './cell-renderer.factory';
import { CustomLoadingOverlayComponent } from './custom-loading-overlay.component';
import { DatePickerCellEditRendererComponent } from '../cell-renderers/date-picker-cell-edit-renderer/date-picker-cell-edit-renderer.component';
import { DateTimePickerComponent } from '../../components/date-time-picker/date-time-picker.component';
export class TableComponent {
    // @ViewChild(TableToolsComponent) tableTools: TableToolsComponent;
    /**
     * @param {?} cellFactory
     * @param {?} defaults
     */
    constructor(cellFactory, defaults) {
        this.cellFactory = cellFactory;
        this.defaults = defaults;
        this.columnNames = [];
        this.filterableColumns = [];
        this.filterTerms = new Subject();
        this.gridOptions = {};
        this.toolPanelVisibility = 'hidden';
        this.overlayLoadingTemplate = '<span class="ag-overlay-loading-center" >Please wait while your rows are loading</span>';
        this.loadingText = 'Please Wait While Your Data is Loading';
        this._data = null;
        /* tslint:disable-next-line:no-output-rename */
        this.exportChangedValue = new EventEmitter();
        this.suppressContextMenu = false;
        /* tslint:disable-next-line:no-output-rename */
        this.searchTerms = new EventEmitter();
        /* tslint:disable-next-line:no-output-rename */
        this.exportClickedRow = new EventEmitter();
        /* tslint:disable-next-line:no-output-rename */
        this.exportClickedCell = new EventEmitter();
        this.draw = false;
        this.tableHeight = '';
        this.extraToolBtnUsed = new EventEmitter();
        /* tslint:disable-next-line:no-output-rename */
        this.buttonOutput = new EventEmitter();
        /* tslint:disable-next-line:no-output-rename */
        this.contextMenuOutput = new EventEmitter();
        /// used for tools in the chat cell, emits the
        /* tslint:disable-next-line:no-output-rename */
        this.exportToolClick = new EventEmitter();
    }
    /**
     * @param {?} theContextMenu
     * @return {?}
     */
    set getContextMenuItemsFromComponent(theContextMenu) {
        if (!theContextMenu) {
            return;
        }
        this.getContextMenuItems = theContextMenu;
    }
    /**
     * @param {?} search
     * @return {?}
     */
    set updateFilter(search) {
        if (search && Object.keys(search).length) {
            this.gridApi.setFilterModel(search);
        }
        else if (search === {}) {
            this.gridApi.setFilterModel(null);
        }
    }
    /**
     * @param {?} theData
     * @return {?}
     */
    set data(theData) {
        if (!theData && this.gridApi) {
            this.gridApi.showLoadingOverlay();
        }
        if (!theData) {
            return;
        }
        this._data = theData;
        if (!this.gridApi) {
            return;
        }
        this.gridApi.setRowData(this._data);
    }
    /**
     * @param {?} draw
     * @return {?}
     */
    set redrawRows(draw) {
        this.draw = draw;
        if (this.draw && this.gridApi) {
            this.gridApi.redrawRows();
            this.draw = false;
        }
    }
    /**
     * @param {?} tD
     * @return {?}
     */
    set tableDefinition(tD) {
        if (!tD) {
            return;
        }
        this._tableDef = _.cloneDeep(tD);
        this.gridOptions = this.createGridOptions();
        this.columnDefs = this.createColumnDefs();
        /** @type {?} */
        const actions = this.manageTableActions();
        if (actions) {
            this.columnDefs.push(actions);
        }
    }
    /**
     * @param {?} value
     * @return {?}
     */
    set height(value) {
        this.tableHeight = value;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.getContextMenuActions = (/**
         * @param {?} params
         * @param {?} eventType
         * @return {?}
         */
        (params, eventType) => this.contextMenuEvent(params, eventType));
        this.frameworkComponents = {
            customLoadingOverlayComponent: CustomLoadingOverlayComponent,
            datePicker: DatePickerCellEditRendererComponent,
            dateTimePicker: DateTimePickerComponent
        };
        this.loadingOverlayComponent = 'customLoadingOverlayComponent';
        this.loadingOverlayComponentParams = { loadingMessage: this.loadingText };
        // returns custom context menu
        if (!this.getContextMenuItems) {
            this.suppressContextMenu = true;
            // The below line can be added back in for a default context menu including, copy cell/line/with & without headers & export
            // this.getContextMenuItems = this.defaults.getDefaults('contextMenu', this.getContextMenuItems);
        }
        this.filterTerms.pipe(distinctUntilChanged()).subscribe((/**
         * @param {?} filterTerm
         * @return {?}
         */
        filterTerm => {
            // Always set the filter string if it changes
            if (!this.gridApi) {
                return;
            }
            this.gridApi.setRowData(this._data);
            this.gridApi.onFilterChanged();
        }));
    }
    /**
     * @param {?} event
     * @return {?}
     */
    onGridReady(event) {
        // this.autoSizeAll();
    }
    // **!! Unnecessary, but kept for posterity
    // autoSizeAll() {
    //   const allColumnIds = [];
    //   this.gridColumnApi.getAllColumns().forEach(function (column) {
    //     console.log('column', column)
    //     allColumnIds.push(column.colId);
    //   });
    //   this.gridColumnApi.autoSizeColumns(allColumnIds);
    // }
    /**
     * @param {?} event
     * @return {?}
     */
    apiReady(event) {
        this.gridApi = event.api;
        this.gridColumnApi = event.columnApi;
        this.gridExportPrefix = this.gridOptions.exportPrefix;
        // this.gridApi.onFilterChanged;
        if (!this._data) {
            this.gridApi.showLoadingOverlay();
        }
        else {
            this.gridApi.setRowData(this._data);
        }
        if (this.sortOnLoad) {
            /** @type {?} */
            const sortModel = [{ colId: this.sortOnLoad, sort: 'desc' }];
            this.gridApi.setSortModel(sortModel);
        }
    }
    /**
     * @return {?}
     */
    onFilterUpdate() {
        if (!this.exportSearchParams) {
            return;
        }
        else {
            /** @type {?} */
            const filteredSearchResults = [];
            this.gridApi.forEachNodeAfterFilter((/**
             * @param {?} rowNode
             * @return {?}
             */
            rowNode => {
                filteredSearchResults[filteredSearchResults.length] =
                    rowNode.data;
            }));
            /** @type {?} */
            const filterTerms = this.gridApi.getFilterModel();
            /** @type {?} */
            const results = {
                searchTerms: filterTerms,
                searchResults: filteredSearchResults
            };
            this.searchTerms.emit(results);
        }
    }
    /**
     * @private
     * @param {?} params
     * @param {?} eventType
     * @return {?}
     */
    contextMenuEvent(params, eventType) {
        /** @type {?} */
        const eventData = {};
        eventData[eventType] = _.cloneDeep(params.node.data);
        this.contextMenuOutput.emit(eventData);
    }
    /**
     * @param {?} row
     * @return {?}
     */
    onRowClicked(row) {
        switch (true) {
            /* this determines if a tool was clicked, vs a row or cell click*/
            case this.toolWasClicked(row.event.srcElement.id):
                this.exportToolClick.emit({ toolId: row.event.srcElement.id, wellbore: row.data });
                return;
        }
        // tool button was not clicked, proceed as normal
        if (!this.exportRowWhenClicked) {
            return;
        }
        else {
            this.exportClickedRow.emit(row.data);
        }
    }
    /**
     * @param {?} cell
     * @return {?}
     */
    onCellClicked(cell) {
        if (!cell) {
            return;
        }
        this.exportClickedCell.emit(cell);
    }
    /**
     * @param {?} event
     * @return {?}
     */
    onCellValueChanged(event) {
        if (!this.exportCellValueChange) {
            return;
        }
        /** @type {?} */
        const results = { data: event['data'] };
        this.exportChangedValue.emit(results);
    }
    /**
     * @private
     * @return {?}
     */
    createColumnDefs() {
        /** @type {?} */
        const table = this._tableDef;
        /** @type {?} */
        const columnDefs = [];
        for (const col of table.columnDefs) {
            // Set up fuzzy filter columns
            if (col.headerName) {
                this.filterableColumns.push((/** @type {?} */ ({
                    name: col.headerName,
                    field: col.field,
                    includeInSearch: col.includeInSearch
                })));
            }
            // For exporting
            if (col.field) {
                this.columnNames.push(col.field);
            }
            // TODO: checkboxSelection filters
            if (!col.filter && !col.checkboxSelection) {
                col.filter = 'agTextColumnFilter';
            }
            if (col.checkboxSelection === true) {
                col.suppressCopy = true;
            }
            if (col.filter === 'set') {
                col.filterParams = {
                    cellRenderer: this.defaults.updateNullValues
                };
            }
            if (col.filter === 'date') {
                col.filterParams = {
                    clearButton: true,
                    suppressAndOrCondition: true,
                    newRowsAction: 'keep',
                    filterOptions: [
                        'equals',
                        'notEqual',
                        'lessThanOrEqual',
                        'greaterThan',
                        'greaterThanOrEqual',
                        'inRange'
                    ],
                    defaultOption: 'equals',
                    browserDatePicker: true,
                    nullComparator: false,
                    inRangeInclusive: true,
                    comparator: this.defaults.dateComparator
                };
            }
            if (!col.floatingFilterComponentParams) {
                col.floatingFilterComponentParams = {
                    suppressFilterButton: true
                };
            }
            // Cell Renderers
            this.cellFactory.setRenderer(col);
            columnDefs.push(Object.assign({}, this.defaults.getDefaults('colDefDefaults'), col));
        }
        return columnDefs;
    }
    /**
     * @private
     * @return {?}
     */
    createGridOptions() {
        /** @type {?} */
        const defaultGridOptions = Object.assign({}, this.defaults.getDefaults('tableDefDefaults'), this._tableDef.gridOptions, { getRowStyle: this.rowStyleFn, getRowClass: this.rowClassFn, rowClassRules: this.rowClassRules, cellClassRules: this.cellClassRules, cellClassFn: this.cellClassFn });
        if (!defaultGridOptions.context) {
            defaultGridOptions.context = this;
        }
        return (/** @type {?} */ (defaultGridOptions));
    }
    /**
     * @private
     * @return {?}
     */
    manageTableActions() {
        /** @type {?} */
        let actionCol = this._tableDef.actions;
        if (!actionCol) {
            return;
        }
        if (!actionCol.width) {
            // buttons are 45px wide, with 10px of padding inbetween
            actionCol.width =
                45 * actionCol.buttons.length +
                    10 * actionCol.buttons.length -
                    1;
        }
        this.cellFactory.setRenderer(actionCol);
        actionCol = Object.assign({}, this.defaults.getDefaults('actionDefaults'), actionCol);
        return actionCol;
    }
    /**
     * @param {?} column
     * @param {?} event
     * @return {?}
     */
    updateColsToFilter(column, event) {
        event.stopPropagation();
        column.includeInSearch = !column.includeInSearch;
        this.gridOptions.api.setRowData(this._data);
        this.gridOptions.api.onFilterChanged();
    }
    /**
     * @param {?} term
     * @return {?}
     */
    filter(term) {
        this.filterTerms.next(term);
    }
    /**
     * @param {?} calledFromWhere
     * @return {?}
     */
    resizeGrid(calledFromWhere) {
        if (this.gridOptions.api) {
            this.gridOptions.api.sizeColumnsToFit();
        }
        else if (this.gridApi) {
            this.gridApi.sizeColumnsToFit();
        }
    }
    /**
     * @param {?} event
     * @return {?}
     */
    extraToolUsed(event) {
        this.extraToolBtnUsed.emit(event);
    }
    /**
     * @private
     * @param {?} id
     * @return {?}
     */
    toolWasClicked(id) {
        /** @type {?} */
        const chatPatt = new RegExp('((clearChat_)[A-z 0-9]{9,})');
        return chatPatt.test(id);
    }
    /**
     * @return {?}
     */
    ngOnDestroy() { }
}
TableComponent.decorators = [
    { type: Component, args: [{
                selector: 'app-table',
                template: "<mat-card *ngIf=\"gridOptions && columnDefs; else spinner\" class=\"card-for-grids\">\n    <!-- <div class=\"grid-tools\">\n        <div *ngIf=\"tableHeader\" [innerHTML]=\"tableHeader\" class=\"table-header\"></div>\n        <mat-form-field *ngIf=\"!hideGridFuzzyFilter\" class=\"fuzzy-search\">\n            <mat-icon matSuffix class=\"suffix-icon\" [matMenuTriggerFor]=\"fuzzyOptions\" matTooltip=\"Columns to Search\" matTooltipPosition=\"right\">filter_list</mat-icon>\n            <input matInput #filterInput (keyup)=\"filter(filterInput.value)\" placeholder=\"Search\">\n        </mat-form-field>\n        <table-tools [exportPrefix]=\"exportPrefix\" [columnNames]=\"columnNames\" [extraTableToolBtn]=\"extraTableToolBtn\" [gridApi]=\"gridApi\"\n            [gridColumnApi]=\"gridColumnApi\" [gridExportPrefix]=\"gridExportPrefix\" [toolPanelVisibility]=\"toolPanelVisibility\"\n            (extraToolBtnClick)=\"extraToolUsed($event)\" class=\"table-tools\">\n        </table-tools>\n        <mat-menu #fuzzyOptions=\"matMenu\">\n            <div *ngFor=\"let col of filterableColumns\" mat-menu-item (click)=\"updateColsToFilter(col, $event)\">\n                <mat-checkbox [checked]=\"col.includeInSearch\">{{col.name}}</mat-checkbox>\n            </div>\n        </mat-menu>\n    </div> -->\n    <div>\n        <ag-grid-angular\n            #agGrid class=\"ag-theme-material on-card\"\n            [gridOptions]=\"gridOptions\"\n            [columnDefs]=\"columnDefs\"\n            [treeData]=\"treeData\"\n            [getDataPath]=\"getDataPath\"\n            [animateRows]=\"animateRows\"\n            [groupDefaultExpanded]=\"groupDefaultExpanded\"\n            [suppressScrollOnNewData]=\"true\"\n            [autoGroupColumnDef]=\"autoGroupColumnDef\"\n            [icons]=\"icons\"\n            [rowDragManaged]=\"rowDragManaged\"\n            [pinnedTopRowData]=\"pinnedTopRowData\"\n            [suppressClipboardPaste]=\"suppressClipboardPaste\"\n            rowSelection=\"multiple\"\n            [suppressContextMenu]=\"suppressContextMenu\"\n            (gridReady)=\"apiReady($event)\"\n            (gridSizeChanged)=\"resizeGrid('gridSizeChanged')\"\n            (modelUpdated)=\"resizeGrid('modelUpdated')\"\n            [getContextMenuItems]=\"getContextMenuItems\"\n            (rowClicked)=\"onRowClicked($event)\"\n            (rowDoubleClicked)=\"onRowClicked($event)\"\n            (cellClicked)=\"onCellClicked($event)\"\n            (cellValueChanged)=\"onCellValueChanged($event)\"\n            (displayedColumnsChanged)=\"resizeGrid('hideColumns')\"\n            (filterChanged)=\"onFilterUpdate()\"\n            [rememberGroupStateWhenNewData]=\"rememberGroupStateWhenNewData\"\n            [frameworkComponents]=\"frameworkComponents\"\n            [loadingOverlayComponent]=\"loadingOverlayComponent\"\n            [loadingOverlayComponentParams]=\"loadingOverlayComponentParams\"\n            [ngStyle]=\"{'height': (tableHeight === 'auto') ? tableHeight : tableHeight + 'px'}\">\n        </ag-grid-angular>\n    </div>\n</mat-card>\n<ng-template #spinner>\n    <mat-spinner></mat-spinner>\n</ng-template>\n<!-- [overlayLoadingTemplate]=\"overlayLoadingTemplate\" -->\n",
                providers: [CellRendererFactory, TableDefaultsService],
                styles: [".dropdown-menu div.checkbox label{margin-left:10px}.grid-tools{-webkit-box-align:center;align-items:center;background-color:#f3f3f3;border-bottom:1px solid #c3c3c3;padding:5px 10px;display:-webkit-box;display:flex;-webkit-box-pack:justify;justify-content:space-between}.mat-card{padding:0}.on-card{width:100%}.suffix-icon{font-size:20px;cursor:pointer}.table-header{display:inline-block;text-align:center}.table-tools{margin-left:auto;padding-left:10px}"]
            }] }
];
/** @nocollapse */
TableComponent.ctorParameters = () => [
    { type: CellRendererFactory },
    { type: TableDefaultsService }
];
TableComponent.propDecorators = {
    rowClassFn: [{ type: Input }],
    rowClassRules: [{ type: Input }],
    cellClassRules: [{ type: Input }],
    cellClassFn: [{ type: Input }],
    rowStyleFn: [{ type: Input }],
    exportPrefix: [{ type: Input }],
    sortOnLoad: [{ type: Input }],
    tableHeader: [{ type: Input }],
    extraTableToolBtn: [{ type: Input }],
    rowDragManaged: [{ type: Input }],
    pinnedTopRowData: [{ type: Input }],
    suppressClipboardPaste: [{ type: Input }],
    exportCellValueChange: [{ type: Input }],
    exportChangedValue: [{ type: Output, args: ['exportChangedValue',] }],
    getContextMenuItemsFromComponent: [{ type: Input }],
    components: [{ type: Input }],
    groupDefaultExpanded: [{ type: Input }],
    rememberGroupStateWhenNewData: [{ type: Input }],
    animateRows: [{ type: Input }],
    treeData: [{ type: Input }],
    autoGroupColumnDef: [{ type: Input }],
    icons: [{ type: Input }],
    getDataPath: [{ type: Input }],
    suppressContextMenu: [{ type: Input }],
    exportSearchParams: [{ type: Input }],
    searchTerms: [{ type: Output, args: ['search-terms',] }],
    updateFilter: [{ type: Input }],
    exportRowWhenClicked: [{ type: Input }],
    exportClickedRow: [{ type: Output, args: ['clicked-row',] }],
    exportCellWhenClicked: [{ type: Input }],
    exportClickedCell: [{ type: Output, args: ['exportClickedCell',] }],
    data: [{ type: Input }],
    redrawRows: [{ type: Input }],
    tableDefinition: [{ type: Input }],
    height: [{ type: Input }],
    extraToolBtnUsed: [{ type: Output }],
    buttonOutput: [{ type: Output, args: ['row-button-click',] }],
    contextMenuOutput: [{ type: Output, args: ['context-menu-event',] }],
    exportToolClick: [{ type: Output, args: ['tool-click',] }]
};
if (false) {
    /** @type {?} */
    TableComponent.prototype.columnDefs;
    /** @type {?} */
    TableComponent.prototype.columnNames;
    /** @type {?} */
    TableComponent.prototype.filterableColumns;
    /**
     * @type {?}
     * @private
     */
    TableComponent.prototype.filterTerms;
    /** @type {?} */
    TableComponent.prototype.gridOptions;
    /** @type {?} */
    TableComponent.prototype.toolPanelVisibility;
    /** @type {?} */
    TableComponent.prototype.overlayLoadingTemplate;
    /** @type {?} */
    TableComponent.prototype.getContextMenuItems;
    /** @type {?} */
    TableComponent.prototype.getContextMenuActions;
    /** @type {?} */
    TableComponent.prototype.loadingOverlayComponent;
    /** @type {?} */
    TableComponent.prototype.loadingOverlayComponentParams;
    /** @type {?} */
    TableComponent.prototype.frameworkComponents;
    /** @type {?} */
    TableComponent.prototype.loadingText;
    /**
     * @type {?}
     * @private
     */
    TableComponent.prototype._data;
    /** @type {?} */
    TableComponent.prototype._tableDef;
    /** @type {?} */
    TableComponent.prototype.gridApi;
    /** @type {?} */
    TableComponent.prototype.gridColumnApi;
    /** @type {?} */
    TableComponent.prototype.gridExportPrefix;
    /** @type {?} */
    TableComponent.prototype.rowClassFn;
    /** @type {?} */
    TableComponent.prototype.rowClassRules;
    /** @type {?} */
    TableComponent.prototype.cellClassRules;
    /** @type {?} */
    TableComponent.prototype.cellClassFn;
    /** @type {?} */
    TableComponent.prototype.rowStyleFn;
    /** @type {?} */
    TableComponent.prototype.exportPrefix;
    /** @type {?} */
    TableComponent.prototype.sortOnLoad;
    /** @type {?} */
    TableComponent.prototype.tableHeader;
    /** @type {?} */
    TableComponent.prototype.extraTableToolBtn;
    /** @type {?} */
    TableComponent.prototype.rowDragManaged;
    /** @type {?} */
    TableComponent.prototype.pinnedTopRowData;
    /** @type {?} */
    TableComponent.prototype.suppressClipboardPaste;
    /** @type {?} */
    TableComponent.prototype.exportCellValueChange;
    /** @type {?} */
    TableComponent.prototype.exportChangedValue;
    /** @type {?} */
    TableComponent.prototype.components;
    /** @type {?} */
    TableComponent.prototype.groupDefaultExpanded;
    /** @type {?} */
    TableComponent.prototype.rememberGroupStateWhenNewData;
    /** @type {?} */
    TableComponent.prototype.animateRows;
    /** @type {?} */
    TableComponent.prototype.treeData;
    /** @type {?} */
    TableComponent.prototype.autoGroupColumnDef;
    /** @type {?} */
    TableComponent.prototype.icons;
    /** @type {?} */
    TableComponent.prototype.getDataPath;
    /** @type {?} */
    TableComponent.prototype.suppressContextMenu;
    /** @type {?} */
    TableComponent.prototype.exportSearchParams;
    /** @type {?} */
    TableComponent.prototype.searchTerms;
    /** @type {?} */
    TableComponent.prototype.exportRowWhenClicked;
    /** @type {?} */
    TableComponent.prototype.exportClickedRow;
    /** @type {?} */
    TableComponent.prototype.exportCellWhenClicked;
    /** @type {?} */
    TableComponent.prototype.exportClickedCell;
    /** @type {?} */
    TableComponent.prototype.draw;
    /** @type {?} */
    TableComponent.prototype.tableHeight;
    /** @type {?} */
    TableComponent.prototype.extraToolBtnUsed;
    /** @type {?} */
    TableComponent.prototype.buttonOutput;
    /** @type {?} */
    TableComponent.prototype.contextMenuOutput;
    /** @type {?} */
    TableComponent.prototype.exportToolClick;
    /**
     * @type {?}
     * @private
     */
    TableComponent.prototype.cellFactory;
    /** @type {?} */
    TableComponent.prototype.defaults;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGFibGUuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGhwdGVjaC9zYXBoaXJhLXNoYXJlZC1saWIvIiwic291cmNlcyI6WyJsaWIvZ3JpZC90YWJsZS90YWJsZS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQ3RELE9BQU8sRUFBRSxTQUFTLEVBQUUsWUFBWSxFQUFFLEtBQUssRUFBcUIsTUFBTSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBSTFGLE9BQU8sRUFBRSxPQUFPLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFDL0IsT0FBTyxLQUFLLENBQUMsTUFBTSxRQUFRLENBQUM7QUFJNUIsT0FBTyxFQUFFLG9CQUFvQixFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFFaEUsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0seUJBQXlCLENBQUM7QUFDOUQsT0FBTyxFQUFFLDZCQUE2QixFQUFFLE1BQU0sb0NBQW9DLENBQUM7QUFDbkYsT0FBTyxFQUFFLG1DQUFtQyxFQUFFLE1BQU0sMkZBQTJGLENBQUM7QUFDaEosT0FBTyxFQUFFLHVCQUF1QixFQUFFLE1BQU0sOERBQThELENBQUM7QUFldkcsTUFBTSxPQUFPLGNBQWM7Ozs7OztJQThKekIsWUFDVSxXQUFnQyxFQUNqQyxRQUE4QjtRQUQ3QixnQkFBVyxHQUFYLFdBQVcsQ0FBcUI7UUFDakMsYUFBUSxHQUFSLFFBQVEsQ0FBc0I7UUE5SmhDLGdCQUFXLEdBQVUsRUFBRSxDQUFDO1FBQ3hCLHNCQUFpQixHQUF1QixFQUFFLENBQUM7UUFDMUMsZ0JBQVcsR0FBRyxJQUFJLE9BQU8sRUFBVSxDQUFDO1FBQ3JDLGdCQUFXLEdBQWlCLEVBQUUsQ0FBQztRQUMvQix3QkFBbUIsR0FBRyxRQUFRLENBQUM7UUFDL0IsMkJBQXNCLEdBQUcseUZBQXlGLENBQUM7UUFRbkgsZ0JBQVcsR0FBRyx3Q0FBd0MsQ0FBQztRQUd0RCxVQUFLLEdBQVMsSUFBSSxDQUFDOztRQTJCM0IsdUJBQWtCLEdBQXNCLElBQUksWUFBWSxFQUFPLENBQUM7UUFxQnZELHdCQUFtQixHQUFHLEtBQUssQ0FBQzs7UUFJckMsZ0JBQVcsR0FBc0IsSUFBSSxZQUFZLEVBQU8sQ0FBQzs7UUFjekQscUJBQWdCLEdBQXNCLElBQUksWUFBWSxFQUFPLENBQUM7O1FBSzlELHNCQUFpQixHQUFzQixJQUFJLFlBQVksRUFBTyxDQUFDO1FBbUIvRCxTQUFJLEdBQUcsS0FBSyxDQUFDO1FBMkJOLGdCQUFXLEdBQVcsRUFBRSxDQUFDO1FBTXRCLHFCQUFnQixHQUFzQixJQUFJLFlBQVksRUFBTyxDQUFDOztRQUl4RSxpQkFBWSxHQUFpQyxJQUFJLFlBQVksRUFBa0IsQ0FBQzs7UUFJaEYsc0JBQWlCLEdBQWlDLElBQUksWUFBWSxFQUFrQixDQUFDOzs7UUFLckYsb0JBQWUsR0FBc0IsSUFBSSxZQUFZLEVBQU8sQ0FBQztJQVE3RCxDQUFDOzs7OztJQWxIRCxJQUNJLGdDQUFnQyxDQUFDLGNBQWM7UUFDakQsSUFBSSxDQUFDLGNBQWMsRUFBRTtZQUNuQixPQUFPO1NBQ1I7UUFDRCxJQUFJLENBQUMsbUJBQW1CLEdBQUcsY0FBYyxDQUFDO0lBQzVDLENBQUM7Ozs7O0lBa0JELElBQ0ksWUFBWSxDQUFDLE1BQU07UUFDckIsSUFBSSxNQUFNLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxNQUFNLEVBQUU7WUFDeEMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFDLENBQUM7U0FDckM7YUFBTSxJQUFJLE1BQU0sS0FBSyxFQUFFLEVBQUU7WUFDeEIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDbkM7SUFDSCxDQUFDOzs7OztJQVlELElBQ0ksSUFBSSxDQUFDLE9BQWE7UUFDcEIsSUFBSSxDQUFDLE9BQU8sSUFBSSxJQUFJLENBQUMsT0FBTyxFQUFFO1lBQzVCLElBQUksQ0FBQyxPQUFPLENBQUMsa0JBQWtCLEVBQUUsQ0FBQztTQUNuQztRQUNELElBQUksQ0FBQyxPQUFPLEVBQUU7WUFDWixPQUFPO1NBQ1I7UUFDRCxJQUFJLENBQUMsS0FBSyxHQUFHLE9BQU8sQ0FBQztRQUVyQixJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRTtZQUNqQixPQUFPO1NBQ1I7UUFFRCxJQUFJLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDdEMsQ0FBQzs7Ozs7SUFHRCxJQUNJLFVBQVUsQ0FBQyxJQUFJO1FBQ2pCLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDO1FBQ2pCLElBQUksSUFBSSxDQUFDLElBQUksSUFBSSxJQUFJLENBQUMsT0FBTyxFQUFFO1lBQzdCLElBQUksQ0FBQyxPQUFPLENBQUMsVUFBVSxFQUFFLENBQUM7WUFDMUIsSUFBSSxDQUFDLElBQUksR0FBRyxLQUFLLENBQUM7U0FDbkI7SUFDSCxDQUFDOzs7OztJQUVELElBQ0ksZUFBZSxDQUFDLEVBQVk7UUFDOUIsSUFBSSxDQUFDLEVBQUUsRUFBRTtZQUNQLE9BQU87U0FDUjtRQUNELElBQUksQ0FBQyxTQUFTLEdBQUcsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUVqQyxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO1FBRTVDLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixFQUFFLENBQUM7O2NBRXBDLE9BQU8sR0FBRyxJQUFJLENBQUMsa0JBQWtCLEVBQUU7UUFDekMsSUFBSSxPQUFPLEVBQUU7WUFDWCxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztTQUMvQjtJQUNILENBQUM7Ozs7O0lBR0QsSUFDSSxNQUFNLENBQUMsS0FBSztRQUNkLElBQUksQ0FBQyxXQUFXLEdBQUcsS0FBSyxDQUFDO0lBQzNCLENBQUM7Ozs7SUF5QkQsUUFBUTtRQUNOLElBQUksQ0FBQyxxQkFBcUI7Ozs7O1FBQUcsQ0FBQyxNQUFNLEVBQUUsU0FBUyxFQUFFLEVBQUUsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxFQUFFLFNBQVMsQ0FBQyxDQUFBLENBQUM7UUFDN0YsSUFBSSxDQUFDLG1CQUFtQixHQUFHO1lBQ3pCLDZCQUE2QixFQUFFLDZCQUE2QjtZQUM1RCxVQUFVLEVBQUUsbUNBQW1DO1lBQy9DLGNBQWMsRUFBRSx1QkFBdUI7U0FDeEMsQ0FBQztRQUNGLElBQUksQ0FBQyx1QkFBdUIsR0FBRywrQkFBK0IsQ0FBQztRQUMvRCxJQUFJLENBQUMsNkJBQTZCLEdBQUcsRUFBQyxjQUFjLEVBQUUsSUFBSSxDQUFDLFdBQVcsRUFBQyxDQUFDO1FBQ3hFLDhCQUE4QjtRQUM5QixJQUFJLENBQUMsSUFBSSxDQUFDLG1CQUFtQixFQUFFO1lBQzdCLElBQUksQ0FBQyxtQkFBbUIsR0FBRyxJQUFJLENBQUM7WUFDaEMsMkhBQTJIO1lBQzNILGlHQUFpRztTQUNsRztRQUNELElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLG9CQUFvQixFQUFFLENBQUMsQ0FBQyxTQUFTOzs7O1FBQUMsVUFBVSxDQUFDLEVBQUU7WUFDbkUsNkNBQTZDO1lBRTdDLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFO2dCQUNqQixPQUFPO2FBQ1I7WUFFRCxJQUFJLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDcEMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxlQUFlLEVBQUUsQ0FBQztRQUNqQyxDQUFDLEVBQUMsQ0FBQztJQUNMLENBQUM7Ozs7O0lBRUQsV0FBVyxDQUFDLEtBQUs7UUFFZixzQkFBc0I7SUFDeEIsQ0FBQzs7Ozs7Ozs7Ozs7Ozs7SUFZRCxRQUFRLENBQUMsS0FBa0I7UUFDekIsSUFBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUMsR0FBRyxDQUFDO1FBQ3pCLElBQUksQ0FBQyxhQUFhLEdBQUcsS0FBSyxDQUFDLFNBQVMsQ0FBQztRQUNyQyxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUM7UUFFdEQsZ0NBQWdDO1FBQ2hDLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFFO1lBQ2YsSUFBSSxDQUFDLE9BQU8sQ0FBQyxrQkFBa0IsRUFBRSxDQUFDO1NBQ25DO2FBQU07WUFDTCxJQUFJLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDckM7UUFDRCxJQUFJLElBQUksQ0FBQyxVQUFVLEVBQUU7O2tCQUNiLFNBQVMsR0FBRyxDQUFDLEVBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxVQUFVLEVBQUUsSUFBSSxFQUFFLE1BQU0sRUFBQyxDQUFDO1lBQzFELElBQUksQ0FBQyxPQUFPLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1NBQ3RDO0lBQ0gsQ0FBQzs7OztJQUVNLGNBQWM7UUFDbkIsSUFBSSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsRUFBRTtZQUM1QixPQUFPO1NBQ1I7YUFBTTs7a0JBQ0MscUJBQXFCLEdBQUcsRUFBRTtZQUNoQyxJQUFJLENBQUMsT0FBTyxDQUFDLHNCQUFzQjs7OztZQUFDLE9BQU8sQ0FBQyxFQUFFO2dCQUM1QyxxQkFBcUIsQ0FBQyxxQkFBcUIsQ0FBQyxNQUFNLENBQUM7b0JBQ2pELE9BQU8sQ0FBQyxJQUFJLENBQUM7WUFDakIsQ0FBQyxFQUFDLENBQUM7O2tCQUNHLFdBQVcsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLGNBQWMsRUFBRTs7a0JBQzNDLE9BQU8sR0FBRztnQkFDZCxXQUFXLEVBQUUsV0FBVztnQkFDeEIsYUFBYSxFQUFFLHFCQUFxQjthQUNyQztZQUNELElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1NBQ2hDO0lBQ0gsQ0FBQzs7Ozs7OztJQUVPLGdCQUFnQixDQUFDLE1BQU0sRUFBRSxTQUFTOztjQUNsQyxTQUFTLEdBQVEsRUFBRTtRQUN6QixTQUFTLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3JELElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7SUFDekMsQ0FBQzs7Ozs7SUFFTSxZQUFZLENBQUMsR0FBRztRQUNyQixRQUFRLElBQUksRUFBRTtZQUNaLGtFQUFrRTtZQUNsRSxLQUFLLElBQUksQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsRUFBRSxDQUFDO2dCQUMvQyxJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxFQUFDLE1BQU0sRUFBRSxHQUFHLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxFQUFFLEVBQUUsUUFBUSxFQUFFLEdBQUcsQ0FBQyxJQUFJLEVBQUMsQ0FBQyxDQUFDO2dCQUNqRixPQUFPO1NBQ1Y7UUFFRCxpREFBaUQ7UUFDakQsSUFBSSxDQUFDLElBQUksQ0FBQyxvQkFBb0IsRUFBRTtZQUM5QixPQUFPO1NBQ1I7YUFBTTtZQUNMLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQ3RDO0lBQ0gsQ0FBQzs7Ozs7SUFFTSxhQUFhLENBQUMsSUFBSTtRQUN2QixJQUFJLENBQUMsSUFBSSxFQUFFO1lBQ1QsT0FBTztTQUNSO1FBQ0QsSUFBSSxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUNwQyxDQUFDOzs7OztJQUVNLGtCQUFrQixDQUFDLEtBQUs7UUFDN0IsSUFBSSxDQUFDLElBQUksQ0FBQyxxQkFBcUIsRUFBRTtZQUMvQixPQUFPO1NBQ1I7O2NBQ0ssT0FBTyxHQUFHLEVBQUMsSUFBSSxFQUFFLEtBQUssQ0FBQyxNQUFNLENBQUMsRUFBQztRQUNyQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO0lBQ3hDLENBQUM7Ozs7O0lBRU8sZ0JBQWdCOztjQUNoQixLQUFLLEdBQUcsSUFBSSxDQUFDLFNBQVM7O2NBRXRCLFVBQVUsR0FBRyxFQUFFO1FBQ3JCLEtBQUssTUFBTSxHQUFHLElBQUksS0FBSyxDQUFDLFVBQVUsRUFBRTtZQUNsQyw4QkFBOEI7WUFDOUIsSUFBSSxHQUFHLENBQUMsVUFBVSxFQUFFO2dCQUNsQixJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLG1CQUFtQjtvQkFDN0MsSUFBSSxFQUFFLEdBQUcsQ0FBQyxVQUFVO29CQUNwQixLQUFLLEVBQUUsR0FBRyxDQUFDLEtBQUs7b0JBQ2hCLGVBQWUsRUFBRSxHQUFHLENBQUMsZUFBZTtpQkFDckMsRUFBQSxDQUFDLENBQUM7YUFDSjtZQUVELGdCQUFnQjtZQUNoQixJQUFJLEdBQUcsQ0FBQyxLQUFLLEVBQUU7Z0JBQ2IsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDO2FBQ2xDO1lBRUQsa0NBQWtDO1lBQ2xDLElBQUksQ0FBQyxHQUFHLENBQUMsTUFBTSxJQUFJLENBQUMsR0FBRyxDQUFDLGlCQUFpQixFQUFFO2dCQUN6QyxHQUFHLENBQUMsTUFBTSxHQUFHLG9CQUFvQixDQUFDO2FBQ25DO1lBQ0QsSUFBSSxHQUFHLENBQUMsaUJBQWlCLEtBQUssSUFBSSxFQUFFO2dCQUNsQyxHQUFHLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQzthQUN6QjtZQUVELElBQUksR0FBRyxDQUFDLE1BQU0sS0FBSyxLQUFLLEVBQUU7Z0JBQ3hCLEdBQUcsQ0FBQyxZQUFZLEdBQUc7b0JBQ2pCLFlBQVksRUFBRSxJQUFJLENBQUMsUUFBUSxDQUFDLGdCQUFnQjtpQkFDN0MsQ0FBQzthQUNIO1lBQ0QsSUFBSSxHQUFHLENBQUMsTUFBTSxLQUFLLE1BQU0sRUFBRTtnQkFDekIsR0FBRyxDQUFDLFlBQVksR0FBRztvQkFDakIsV0FBVyxFQUFFLElBQUk7b0JBQ2pCLHNCQUFzQixFQUFFLElBQUk7b0JBQzVCLGFBQWEsRUFBRSxNQUFNO29CQUNyQixhQUFhLEVBQUU7d0JBQ2IsUUFBUTt3QkFDUixVQUFVO3dCQUNWLGlCQUFpQjt3QkFDakIsYUFBYTt3QkFDYixvQkFBb0I7d0JBQ3BCLFNBQVM7cUJBQ1Y7b0JBQ0QsYUFBYSxFQUFFLFFBQVE7b0JBQ3ZCLGlCQUFpQixFQUFFLElBQUk7b0JBQ3ZCLGNBQWMsRUFBRSxLQUFLO29CQUNyQixnQkFBZ0IsRUFBRSxJQUFJO29CQUN0QixVQUFVLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxjQUFjO2lCQUN6QyxDQUFDO2FBQ0g7WUFDRCxJQUFJLENBQUMsR0FBRyxDQUFDLDZCQUE2QixFQUFFO2dCQUN0QyxHQUFHLENBQUMsNkJBQTZCLEdBQUc7b0JBQ2xDLG9CQUFvQixFQUFFLElBQUk7aUJBQzNCLENBQUM7YUFDSDtZQUVELGlCQUFpQjtZQUNqQixJQUFJLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUVsQyxVQUFVLENBQUMsSUFBSSxtQkFDVixJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxnQkFBZ0IsQ0FBQyxFQUMzQyxHQUFHLEVBQ04sQ0FBQztTQUNKO1FBQ0QsT0FBTyxVQUFVLENBQUM7SUFDcEIsQ0FBQzs7Ozs7SUFFTyxpQkFBaUI7O2NBQ2pCLGtCQUFrQixxQkFDbkIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsa0JBQWtCLENBQUMsRUFDN0MsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLElBQzdCLFdBQVcsRUFBRSxJQUFJLENBQUMsVUFBVSxFQUM1QixXQUFXLEVBQUUsSUFBSSxDQUFDLFVBQVUsRUFDNUIsYUFBYSxFQUFFLElBQUksQ0FBQyxhQUFhLEVBQ2pDLGNBQWMsRUFBRSxJQUFJLENBQUMsY0FBYyxFQUNuQyxXQUFXLEVBQUUsSUFBSSxDQUFDLFdBQVcsR0FDOUI7UUFDRCxJQUFJLENBQUMsa0JBQWtCLENBQUMsT0FBTyxFQUFFO1lBQy9CLGtCQUFrQixDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7U0FDbkM7UUFDRCxPQUFPLG1CQUFlLGtCQUFrQixFQUFBLENBQUM7SUFDM0MsQ0FBQzs7Ozs7SUFFTyxrQkFBa0I7O1lBQ3BCLFNBQVMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLE9BQU87UUFFdEMsSUFBSSxDQUFDLFNBQVMsRUFBRTtZQUNkLE9BQU87U0FDUjtRQUVELElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxFQUFFO1lBQ3BCLHdEQUF3RDtZQUN4RCxTQUFTLENBQUMsS0FBSztnQkFDYixFQUFFLEdBQUcsU0FBUyxDQUFDLE9BQU8sQ0FBQyxNQUFNO29CQUM3QixFQUFFLEdBQUcsU0FBUyxDQUFDLE9BQU8sQ0FBQyxNQUFNO29CQUM3QixDQUFDLENBQUM7U0FDTDtRQUVELElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBRXhDLFNBQVMscUJBQ0osSUFBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsZ0JBQWdCLENBQUMsRUFDM0MsU0FBUyxDQUNiLENBQUM7UUFFRixPQUFPLFNBQVMsQ0FBQztJQUNuQixDQUFDOzs7Ozs7SUFFTSxrQkFBa0IsQ0FDdkIsTUFBd0IsRUFDeEIsS0FBb0I7UUFFcEIsS0FBSyxDQUFDLGVBQWUsRUFBRSxDQUFDO1FBQ3hCLE1BQU0sQ0FBQyxlQUFlLEdBQUcsQ0FBQyxNQUFNLENBQUMsZUFBZSxDQUFDO1FBRWpELElBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDNUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsZUFBZSxFQUFFLENBQUM7SUFDekMsQ0FBQzs7Ozs7SUFFTSxNQUFNLENBQUMsSUFBWTtRQUN4QixJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUM5QixDQUFDOzs7OztJQUVNLFVBQVUsQ0FBQyxlQUF1QjtRQUN2QyxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxFQUFFO1lBQ3hCLElBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLGdCQUFnQixFQUFFLENBQUM7U0FDekM7YUFBTSxJQUFJLElBQUksQ0FBQyxPQUFPLEVBQUU7WUFDdkIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO1NBQ2pDO0lBQ0gsQ0FBQzs7Ozs7SUFFTSxhQUFhLENBQUMsS0FBSztRQUN4QixJQUFJLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ3BDLENBQUM7Ozs7OztJQUVPLGNBQWMsQ0FBQyxFQUFVOztjQUN6QixRQUFRLEdBQUcsSUFBSSxNQUFNLENBQUMsNkJBQTZCLENBQUM7UUFDMUQsT0FBTyxRQUFRLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDO0lBQzNCLENBQUM7Ozs7SUFFRCxXQUFXLEtBQUksQ0FBQzs7O1lBM2FqQixTQUFTLFNBQUM7Z0JBQ1QsUUFBUSxFQUFFLFdBQVc7Z0JBQ3JCLHlwR0FBcUM7Z0JBR3JDLFNBQVMsRUFBRSxDQUFDLG1CQUFtQixFQUFFLG9CQUFvQixDQUFDOzthQUN2RDs7OztZQWpCUSxtQkFBbUI7WUFGbkIsb0JBQW9COzs7eUJBNkMxQixLQUFLOzRCQUNMLEtBQUs7NkJBQ0wsS0FBSzswQkFDTCxLQUFLO3lCQUNMLEtBQUs7MkJBQ0wsS0FBSzt5QkFDTCxLQUFLOzBCQUNMLEtBQUs7Z0NBQ0wsS0FBSzs2QkFDTCxLQUFLOytCQUNMLEtBQUs7cUNBQ0wsS0FBSztvQ0FNTCxLQUFLO2lDQUVMLE1BQU0sU0FBQyxvQkFBb0I7K0NBSTNCLEtBQUs7eUJBU0wsS0FBSzttQ0FDTCxLQUFLOzRDQUNMLEtBQUs7MEJBQ0wsS0FBSzt1QkFDTCxLQUFLO2lDQUNMLEtBQUs7b0JBQ0wsS0FBSzswQkFDTCxLQUFLO2tDQUVMLEtBQUs7aUNBQ0wsS0FBSzswQkFFTCxNQUFNLFNBQUMsY0FBYzsyQkFHckIsS0FBSzttQ0FTTCxLQUFLOytCQUVMLE1BQU0sU0FBQyxhQUFhO29DQUdwQixLQUFLO2dDQUVMLE1BQU0sU0FBQyxtQkFBbUI7bUJBRzFCLEtBQUs7eUJBa0JMLEtBQUs7OEJBU0wsS0FBSztxQkFrQkwsS0FBSzsrQkFLTCxNQUFNOzJCQUdOLE1BQU0sU0FBQyxrQkFBa0I7Z0NBSXpCLE1BQU0sU0FBQyxvQkFBb0I7OEJBSzNCLE1BQU0sU0FBQyxZQUFZOzs7O0lBeEpwQixvQ0FBeUI7O0lBQ3pCLHFDQUErQjs7SUFDL0IsMkNBQWtEOzs7OztJQUNsRCxxQ0FBNEM7O0lBQzVDLHFDQUFzQzs7SUFDdEMsNkNBQXNDOztJQUN0QyxnREFBMEg7O0lBQzFILDZDQUEyQjs7SUFDM0IsK0NBQTZCOztJQUc3QixpREFBK0I7O0lBQy9CLHVEQUFxQzs7SUFDckMsNkNBQTJCOztJQUMzQixxQ0FBOEQ7Ozs7O0lBRzlELCtCQUEyQjs7SUFDM0IsbUNBQTJCOztJQUUzQixpQ0FBZTs7SUFDZix1Q0FBcUI7O0lBQ3JCLDBDQUF3Qjs7SUFFeEIsb0NBQTJDOztJQUMzQyx1Q0FBOEM7O0lBQzlDLHdDQUErQzs7SUFDL0MscUNBQTRDOztJQUM1QyxvQ0FBMkM7O0lBQzNDLHNDQUErQjs7SUFDL0Isb0NBQTZCOztJQUM3QixxQ0FBOEI7O0lBQzlCLDJDQUFvQzs7SUFDcEMsd0NBQWlDOztJQUNqQywwQ0FBbUM7O0lBQ25DLGdEQUEwQzs7SUFNMUMsK0NBQXlDOztJQUV6Qyw0Q0FDZ0U7O0lBWWhFLG9DQUEwQjs7SUFDMUIsOENBQXVDOztJQUN2Qyx1REFBaUQ7O0lBQ2pELHFDQUErQjs7SUFDL0Isa0NBQTRCOztJQUM1Qiw0Q0FBa0M7O0lBQ2xDLCtCQUFxQjs7SUFDckIscUNBQTRDOztJQUU1Qyw2Q0FBcUM7O0lBQ3JDLDRDQUE0Qjs7SUFFNUIscUNBQ3lEOztJQVd6RCw4Q0FBOEI7O0lBRTlCLDBDQUM4RDs7SUFFOUQsK0NBQStCOztJQUUvQiwyQ0FDK0Q7O0lBbUIvRCw4QkFBYTs7SUEyQmIscUNBQWdDOztJQU1oQywwQ0FBd0U7O0lBR3hFLHNDQUNnRjs7SUFHaEYsMkNBQ3FGOztJQUlyRix5Q0FDNkQ7Ozs7O0lBSzNELHFDQUF3Qzs7SUFDeEMsa0NBQXFDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgZGlzdGluY3RVbnRpbENoYW5nZWQgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XG5pbXBvcnQgeyBDb21wb25lbnQsIEV2ZW50RW1pdHRlciwgSW5wdXQsIE9uRGVzdHJveSwgT25Jbml0LCBPdXRwdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuaW1wb3J0IHsgR3JpZE9wdGlvbnMsIEFnR3JpZEV2ZW50IH0gZnJvbSAnYWctZ3JpZC1jb21tdW5pdHkvbWFpbic7XG5cbmltcG9ydCB7IFN1YmplY3QgfSBmcm9tICdyeGpzJztcbmltcG9ydCAqIGFzIF8gZnJvbSAnbG9kYXNoJztcblxuaW1wb3J0IHsgQ3VzdG9tR3JpZE9wdGlvbnMsIEZpbHRlcmFibGVDb2x1bW4gfSBmcm9tICcuL3RhYmxlLXR5cGVzJztcblxuaW1wb3J0IHsgVGFibGVEZWZhdWx0c1NlcnZpY2UgfSBmcm9tICcuL3RhYmxlLWRlZmF1bHRzLnNlcnZpY2UnO1xuaW1wb3J0IHsgUm93QnV0dG9uRXZlbnQgfSBmcm9tICcuLi9yb3ctYnV0dG9uLmNvbXBvbmVudHMnO1xuaW1wb3J0IHsgQ2VsbFJlbmRlcmVyRmFjdG9yeSB9IGZyb20gJy4vY2VsbC1yZW5kZXJlci5mYWN0b3J5JztcbmltcG9ydCB7IEN1c3RvbUxvYWRpbmdPdmVybGF5Q29tcG9uZW50IH0gZnJvbSAnLi9jdXN0b20tbG9hZGluZy1vdmVybGF5LmNvbXBvbmVudCc7XG5pbXBvcnQgeyBEYXRlUGlja2VyQ2VsbEVkaXRSZW5kZXJlckNvbXBvbmVudCB9IGZyb20gJy4uL2NlbGwtcmVuZGVyZXJzL2RhdGUtcGlja2VyLWNlbGwtZWRpdC1yZW5kZXJlci9kYXRlLXBpY2tlci1jZWxsLWVkaXQtcmVuZGVyZXIuY29tcG9uZW50JztcbmltcG9ydCB7IERhdGVUaW1lUGlja2VyQ29tcG9uZW50IH0gZnJvbSAnLi4vLi4vY29tcG9uZW50cy9kYXRlLXRpbWUtcGlja2VyL2RhdGUtdGltZS1waWNrZXIuY29tcG9uZW50JztcblxuZXhwb3J0IHR5cGUgVGFibGVPcHRpb25zID0gR3JpZE9wdGlvbnMgJiBDdXN0b21HcmlkT3B0aW9ucztcblxuZXhwb3J0IHR5cGUgQWN0aW9ucyA9IGFueTtcbmV4cG9ydCB0eXBlIFRhYmxlRGVmID0gYW55O1xuZXhwb3J0IHR5cGUgRGF0YSA9IGFueVtdO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdhcHAtdGFibGUnLFxuICB0ZW1wbGF0ZVVybDogJy4vdGFibGUuY29tcG9uZW50Lmh0bWwnLFxuICBzdHlsZVVybHM6IFsnLi90YWJsZS5jb21wb25lbnQuc2NzcyddLFxuXG4gIHByb3ZpZGVyczogW0NlbGxSZW5kZXJlckZhY3RvcnksIFRhYmxlRGVmYXVsdHNTZXJ2aWNlXVxufSlcbmV4cG9ydCBjbGFzcyBUYWJsZUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgT25EZXN0cm95IHtcbiAgcHVibGljIGNvbHVtbkRlZnM6IGFueVtdO1xuICBwdWJsaWMgY29sdW1uTmFtZXM6IGFueVtdID0gW107XG4gIHB1YmxpYyBmaWx0ZXJhYmxlQ29sdW1uczogRmlsdGVyYWJsZUNvbHVtbltdID0gW107XG4gIHByaXZhdGUgZmlsdGVyVGVybXMgPSBuZXcgU3ViamVjdDxzdHJpbmc+KCk7XG4gIHB1YmxpYyBncmlkT3B0aW9uczogVGFibGVPcHRpb25zID0ge307XG4gIHB1YmxpYyB0b29sUGFuZWxWaXNpYmlsaXR5ID0gJ2hpZGRlbic7XG4gIHB1YmxpYyBvdmVybGF5TG9hZGluZ1RlbXBsYXRlID0gJzxzcGFuIGNsYXNzPVwiYWctb3ZlcmxheS1sb2FkaW5nLWNlbnRlclwiID5QbGVhc2Ugd2FpdCB3aGlsZSB5b3VyIHJvd3MgYXJlIGxvYWRpbmc8L3NwYW4+JztcbiAgcHVibGljIGdldENvbnRleHRNZW51SXRlbXM7XG4gIHB1YmxpYyBnZXRDb250ZXh0TWVudUFjdGlvbnM7XG5cbiAgLy8gYmVsb3cgaXMgZm9yIGxvYWRpbmcgb3ZlcmxheVxuICBwdWJsaWMgbG9hZGluZ092ZXJsYXlDb21wb25lbnQ7XG4gIHB1YmxpYyBsb2FkaW5nT3ZlcmxheUNvbXBvbmVudFBhcmFtcztcbiAgcHVibGljIGZyYW1ld29ya0NvbXBvbmVudHM7XG4gIHB1YmxpYyBsb2FkaW5nVGV4dCA9ICdQbGVhc2UgV2FpdCBXaGlsZSBZb3VyIERhdGEgaXMgTG9hZGluZyc7XG5cblxuICBwcml2YXRlIF9kYXRhOiBEYXRhID0gbnVsbDtcbiAgcHVibGljIF90YWJsZURlZjogVGFibGVEZWY7XG5cbiAgcHVibGljIGdyaWRBcGk7XG4gIHB1YmxpYyBncmlkQ29sdW1uQXBpO1xuICBwdWJsaWMgZ3JpZEV4cG9ydFByZWZpeDtcblxuICBASW5wdXQoKSByb3dDbGFzc0ZuPzogKHBhcmFtczogYW55KSA9PiBhbnk7XG4gIEBJbnB1dCgpIHJvd0NsYXNzUnVsZXM/OiAocGFyYW1zOiBhbnkpID0+IGFueTtcbiAgQElucHV0KCkgY2VsbENsYXNzUnVsZXM/OiAocGFyYW1zOiBhbnkpID0+IGFueTtcbiAgQElucHV0KCkgY2VsbENsYXNzRm4/OiAocGFyYW1zOiBhbnkpID0+IGFueTtcbiAgQElucHV0KCkgcm93U3R5bGVGbj86IChwYXJhbXM6IGFueSkgPT4gYW55O1xuICBASW5wdXQoKSBleHBvcnRQcmVmaXg/OiBzdHJpbmc7XG4gIEBJbnB1dCgpIHNvcnRPbkxvYWQ/OiBzdHJpbmc7XG4gIEBJbnB1dCgpIHRhYmxlSGVhZGVyPzogc3RyaW5nO1xuICBASW5wdXQoKSBleHRyYVRhYmxlVG9vbEJ0bj86IHN0cmluZztcbiAgQElucHV0KCkgcm93RHJhZ01hbmFnZWQ/OiBzdHJpbmc7XG4gIEBJbnB1dCgpIHBpbm5lZFRvcFJvd0RhdGE/OiBzdHJpbmc7XG4gIEBJbnB1dCgpIHN1cHByZXNzQ2xpcGJvYXJkUGFzdGU/OiBib29sZWFuO1xuXG4gIC8vIHVzaW5nIGV4cG9ydENlbGxWYWx1ZUNoYW5nZSA9IHRydWUgaW4gYSB0YWJsZSB3aWxsIGFsbG93IGluZm9ybWF0aW9uXG4gIC8vIHRvIGJlIGV4cG9ydGVkIHVwb24gZXZlcnkgY2hhbmdlIHRocm91Z2ggKGV4cG9ydC1jaGFuZ2VkLXZhbHVlKS5cbiAgLy8gZXhwb3J0ZWQgZGF0YSBpcyBpbiB0aGUgZm9ybSBvZiBhbiBvYmplY3QgY29udGFpbmluZyB0aGUgcm93IGFuZCBhbGxcbiAgLy8gcHJlc2VudCBkYXRhXG4gIEBJbnB1dCgpIGV4cG9ydENlbGxWYWx1ZUNoYW5nZT86IGJvb2xlYW47XG4gIC8qIHRzbGludDpkaXNhYmxlLW5leHQtbGluZTpuby1vdXRwdXQtcmVuYW1lICovXG4gIEBPdXRwdXQoJ2V4cG9ydENoYW5nZWRWYWx1ZScpXG4gIGV4cG9ydENoYW5nZWRWYWx1ZTogRXZlbnRFbWl0dGVyPGFueT4gPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcblxuXG4gIEBJbnB1dCgpXG4gIHNldCBnZXRDb250ZXh0TWVudUl0ZW1zRnJvbUNvbXBvbmVudCh0aGVDb250ZXh0TWVudSkge1xuICAgIGlmICghdGhlQ29udGV4dE1lbnUpIHtcbiAgICAgIHJldHVybjtcbiAgICB9XG4gICAgdGhpcy5nZXRDb250ZXh0TWVudUl0ZW1zID0gdGhlQ29udGV4dE1lbnU7XG4gIH1cblxuICAvLyBmb3IgdHJlZSBmb3JtYXQgdGFibGVzXG4gIEBJbnB1dCgpIGNvbXBvbmVudHM/OiBhbnk7XG4gIEBJbnB1dCgpIGdyb3VwRGVmYXVsdEV4cGFuZGVkPzogbnVtYmVyO1xuICBASW5wdXQoKSByZW1lbWJlckdyb3VwU3RhdGVXaGVuTmV3RGF0YT86IGJvb2xlYW47XG4gIEBJbnB1dCgpIGFuaW1hdGVSb3dzPzogYm9vbGVhbjtcbiAgQElucHV0KCkgdHJlZURhdGE/OiBib29sZWFuO1xuICBASW5wdXQoKSBhdXRvR3JvdXBDb2x1bW5EZWY/OiBhbnk7XG4gIEBJbnB1dCgpIGljb25zPzogYW55O1xuICBASW5wdXQoKSBnZXREYXRhUGF0aD86IChwYXJhbXM6IGFueSkgPT4gYW55O1xuXG4gIEBJbnB1dCgpIHN1cHByZXNzQ29udGV4dE1lbnUgPSBmYWxzZTtcbiAgQElucHV0KCkgZXhwb3J0U2VhcmNoUGFyYW1zO1xuICAvKiB0c2xpbnQ6ZGlzYWJsZS1uZXh0LWxpbmU6bm8tb3V0cHV0LXJlbmFtZSAqL1xuICBAT3V0cHV0KCdzZWFyY2gtdGVybXMnKVxuICBzZWFyY2hUZXJtczogRXZlbnRFbWl0dGVyPGFueT4gPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcblxuICBASW5wdXQoKVxuICBzZXQgdXBkYXRlRmlsdGVyKHNlYXJjaCkge1xuICAgIGlmIChzZWFyY2ggJiYgT2JqZWN0LmtleXMoc2VhcmNoKS5sZW5ndGgpIHtcbiAgICAgIHRoaXMuZ3JpZEFwaS5zZXRGaWx0ZXJNb2RlbChzZWFyY2gpO1xuICAgIH0gZWxzZSBpZiAoc2VhcmNoID09PSB7fSkge1xuICAgICAgdGhpcy5ncmlkQXBpLnNldEZpbHRlck1vZGVsKG51bGwpO1xuICAgIH1cbiAgfVxuXG4gIEBJbnB1dCgpIGV4cG9ydFJvd1doZW5DbGlja2VkO1xuICAvKiB0c2xpbnQ6ZGlzYWJsZS1uZXh0LWxpbmU6bm8tb3V0cHV0LXJlbmFtZSAqL1xuICBAT3V0cHV0KCdjbGlja2VkLXJvdycpXG4gIGV4cG9ydENsaWNrZWRSb3c6IEV2ZW50RW1pdHRlcjxhbnk+ID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XG5cbiAgQElucHV0KCkgZXhwb3J0Q2VsbFdoZW5DbGlja2VkO1xuICAvKiB0c2xpbnQ6ZGlzYWJsZS1uZXh0LWxpbmU6bm8tb3V0cHV0LXJlbmFtZSAqL1xuICBAT3V0cHV0KCdleHBvcnRDbGlja2VkQ2VsbCcpXG4gIGV4cG9ydENsaWNrZWRDZWxsOiBFdmVudEVtaXR0ZXI8YW55PiA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xuXG4gIEBJbnB1dCgpXG4gIHNldCBkYXRhKHRoZURhdGE6IERhdGEpIHtcbiAgICBpZiAoIXRoZURhdGEgJiYgdGhpcy5ncmlkQXBpKSB7XG4gICAgICB0aGlzLmdyaWRBcGkuc2hvd0xvYWRpbmdPdmVybGF5KCk7XG4gICAgfVxuICAgIGlmICghdGhlRGF0YSkge1xuICAgICAgcmV0dXJuO1xuICAgIH1cbiAgICB0aGlzLl9kYXRhID0gdGhlRGF0YTtcblxuICAgIGlmICghdGhpcy5ncmlkQXBpKSB7XG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgdGhpcy5ncmlkQXBpLnNldFJvd0RhdGEodGhpcy5fZGF0YSk7XG4gIH1cblxuICBkcmF3ID0gZmFsc2U7XG4gIEBJbnB1dCgpXG4gIHNldCByZWRyYXdSb3dzKGRyYXcpIHtcbiAgICB0aGlzLmRyYXcgPSBkcmF3O1xuICAgIGlmICh0aGlzLmRyYXcgJiYgdGhpcy5ncmlkQXBpKSB7XG4gICAgICB0aGlzLmdyaWRBcGkucmVkcmF3Um93cygpO1xuICAgICAgdGhpcy5kcmF3ID0gZmFsc2U7XG4gICAgfVxuICB9XG5cbiAgQElucHV0KClcbiAgc2V0IHRhYmxlRGVmaW5pdGlvbih0RDogVGFibGVEZWYpIHtcbiAgICBpZiAoIXREKSB7XG4gICAgICByZXR1cm47XG4gICAgfVxuICAgIHRoaXMuX3RhYmxlRGVmID0gXy5jbG9uZURlZXAodEQpO1xuXG4gICAgdGhpcy5ncmlkT3B0aW9ucyA9IHRoaXMuY3JlYXRlR3JpZE9wdGlvbnMoKTtcblxuICAgIHRoaXMuY29sdW1uRGVmcyA9IHRoaXMuY3JlYXRlQ29sdW1uRGVmcygpO1xuXG4gICAgY29uc3QgYWN0aW9ucyA9IHRoaXMubWFuYWdlVGFibGVBY3Rpb25zKCk7XG4gICAgaWYgKGFjdGlvbnMpIHtcbiAgICAgIHRoaXMuY29sdW1uRGVmcy5wdXNoKGFjdGlvbnMpO1xuICAgIH1cbiAgfVxuXG4gIHB1YmxpYyB0YWJsZUhlaWdodDogU3RyaW5nID0gJyc7XG4gIEBJbnB1dCgpXG4gIHNldCBoZWlnaHQodmFsdWUpIHtcbiAgICB0aGlzLnRhYmxlSGVpZ2h0ID0gdmFsdWU7XG4gIH1cblxuICBAT3V0cHV0KCkgZXh0cmFUb29sQnRuVXNlZDogRXZlbnRFbWl0dGVyPGFueT4gPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcblxuICAvKiB0c2xpbnQ6ZGlzYWJsZS1uZXh0LWxpbmU6bm8tb3V0cHV0LXJlbmFtZSAqL1xuICBAT3V0cHV0KCdyb3ctYnV0dG9uLWNsaWNrJylcbiAgYnV0dG9uT3V0cHV0OiBFdmVudEVtaXR0ZXI8Um93QnV0dG9uRXZlbnQ+ID0gbmV3IEV2ZW50RW1pdHRlcjxSb3dCdXR0b25FdmVudD4oKTtcblxuICAvKiB0c2xpbnQ6ZGlzYWJsZS1uZXh0LWxpbmU6bm8tb3V0cHV0LXJlbmFtZSAqL1xuICBAT3V0cHV0KCdjb250ZXh0LW1lbnUtZXZlbnQnKVxuICBjb250ZXh0TWVudU91dHB1dDogRXZlbnRFbWl0dGVyPFJvd0J1dHRvbkV2ZW50PiA9IG5ldyBFdmVudEVtaXR0ZXI8Um93QnV0dG9uRXZlbnQ+KCk7XG5cbiAgLy8vIHVzZWQgZm9yIHRvb2xzIGluIHRoZSBjaGF0IGNlbGwsIGVtaXRzIHRoZVxuICAvKiB0c2xpbnQ6ZGlzYWJsZS1uZXh0LWxpbmU6bm8tb3V0cHV0LXJlbmFtZSAqL1xuICBAT3V0cHV0KCd0b29sLWNsaWNrJylcbiAgZXhwb3J0VG9vbENsaWNrOiBFdmVudEVtaXR0ZXI8YW55PiA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xuXG4gIC8vIEBWaWV3Q2hpbGQoVGFibGVUb29sc0NvbXBvbmVudCkgdGFibGVUb29sczogVGFibGVUb29sc0NvbXBvbmVudDtcblxuICBjb25zdHJ1Y3RvcihcbiAgICBwcml2YXRlIGNlbGxGYWN0b3J5OiBDZWxsUmVuZGVyZXJGYWN0b3J5LFxuICAgIHB1YmxpYyBkZWZhdWx0czogVGFibGVEZWZhdWx0c1NlcnZpY2VcbiAgKSB7XG4gIH1cblxuICBuZ09uSW5pdCgpIHtcbiAgICB0aGlzLmdldENvbnRleHRNZW51QWN0aW9ucyA9IChwYXJhbXMsIGV2ZW50VHlwZSkgPT4gdGhpcy5jb250ZXh0TWVudUV2ZW50KHBhcmFtcywgZXZlbnRUeXBlKTtcbiAgICB0aGlzLmZyYW1ld29ya0NvbXBvbmVudHMgPSB7XG4gICAgICBjdXN0b21Mb2FkaW5nT3ZlcmxheUNvbXBvbmVudDogQ3VzdG9tTG9hZGluZ092ZXJsYXlDb21wb25lbnQsXG4gICAgICBkYXRlUGlja2VyOiBEYXRlUGlja2VyQ2VsbEVkaXRSZW5kZXJlckNvbXBvbmVudCxcbiAgICAgIGRhdGVUaW1lUGlja2VyOiBEYXRlVGltZVBpY2tlckNvbXBvbmVudFxuICAgIH07XG4gICAgdGhpcy5sb2FkaW5nT3ZlcmxheUNvbXBvbmVudCA9ICdjdXN0b21Mb2FkaW5nT3ZlcmxheUNvbXBvbmVudCc7XG4gICAgdGhpcy5sb2FkaW5nT3ZlcmxheUNvbXBvbmVudFBhcmFtcyA9IHtsb2FkaW5nTWVzc2FnZTogdGhpcy5sb2FkaW5nVGV4dH07XG4gICAgLy8gcmV0dXJucyBjdXN0b20gY29udGV4dCBtZW51XG4gICAgaWYgKCF0aGlzLmdldENvbnRleHRNZW51SXRlbXMpIHtcbiAgICAgIHRoaXMuc3VwcHJlc3NDb250ZXh0TWVudSA9IHRydWU7XG4gICAgICAvLyBUaGUgYmVsb3cgbGluZSBjYW4gYmUgYWRkZWQgYmFjayBpbiBmb3IgYSBkZWZhdWx0IGNvbnRleHQgbWVudSBpbmNsdWRpbmcsIGNvcHkgY2VsbC9saW5lL3dpdGggJiB3aXRob3V0IGhlYWRlcnMgJiBleHBvcnRcbiAgICAgIC8vIHRoaXMuZ2V0Q29udGV4dE1lbnVJdGVtcyA9IHRoaXMuZGVmYXVsdHMuZ2V0RGVmYXVsdHMoJ2NvbnRleHRNZW51JywgdGhpcy5nZXRDb250ZXh0TWVudUl0ZW1zKTtcbiAgICB9XG4gICAgdGhpcy5maWx0ZXJUZXJtcy5waXBlKGRpc3RpbmN0VW50aWxDaGFuZ2VkKCkpLnN1YnNjcmliZShmaWx0ZXJUZXJtID0+IHtcbiAgICAgIC8vIEFsd2F5cyBzZXQgdGhlIGZpbHRlciBzdHJpbmcgaWYgaXQgY2hhbmdlc1xuXG4gICAgICBpZiAoIXRoaXMuZ3JpZEFwaSkge1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG5cbiAgICAgIHRoaXMuZ3JpZEFwaS5zZXRSb3dEYXRhKHRoaXMuX2RhdGEpO1xuICAgICAgdGhpcy5ncmlkQXBpLm9uRmlsdGVyQ2hhbmdlZCgpO1xuICAgIH0pO1xuICB9XG5cbiAgb25HcmlkUmVhZHkoZXZlbnQpIHtcblxuICAgIC8vIHRoaXMuYXV0b1NpemVBbGwoKTtcbiAgfVxuXG4gIC8vICoqISEgVW5uZWNlc3NhcnksIGJ1dCBrZXB0IGZvciBwb3N0ZXJpdHlcbiAgLy8gYXV0b1NpemVBbGwoKSB7XG4gIC8vICAgY29uc3QgYWxsQ29sdW1uSWRzID0gW107XG4gIC8vICAgdGhpcy5ncmlkQ29sdW1uQXBpLmdldEFsbENvbHVtbnMoKS5mb3JFYWNoKGZ1bmN0aW9uIChjb2x1bW4pIHtcbiAgLy8gICAgIGNvbnNvbGUubG9nKCdjb2x1bW4nLCBjb2x1bW4pXG4gIC8vICAgICBhbGxDb2x1bW5JZHMucHVzaChjb2x1bW4uY29sSWQpO1xuICAvLyAgIH0pO1xuICAvLyAgIHRoaXMuZ3JpZENvbHVtbkFwaS5hdXRvU2l6ZUNvbHVtbnMoYWxsQ29sdW1uSWRzKTtcbiAgLy8gfVxuXG4gIGFwaVJlYWR5KGV2ZW50OiBBZ0dyaWRFdmVudCkge1xuICAgIHRoaXMuZ3JpZEFwaSA9IGV2ZW50LmFwaTtcbiAgICB0aGlzLmdyaWRDb2x1bW5BcGkgPSBldmVudC5jb2x1bW5BcGk7XG4gICAgdGhpcy5ncmlkRXhwb3J0UHJlZml4ID0gdGhpcy5ncmlkT3B0aW9ucy5leHBvcnRQcmVmaXg7XG5cbiAgICAvLyB0aGlzLmdyaWRBcGkub25GaWx0ZXJDaGFuZ2VkO1xuICAgIGlmICghdGhpcy5fZGF0YSkge1xuICAgICAgdGhpcy5ncmlkQXBpLnNob3dMb2FkaW5nT3ZlcmxheSgpO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLmdyaWRBcGkuc2V0Um93RGF0YSh0aGlzLl9kYXRhKTtcbiAgICB9XG4gICAgaWYgKHRoaXMuc29ydE9uTG9hZCkge1xuICAgICAgY29uc3Qgc29ydE1vZGVsID0gW3tjb2xJZDogdGhpcy5zb3J0T25Mb2FkLCBzb3J0OiAnZGVzYyd9XTtcbiAgICAgIHRoaXMuZ3JpZEFwaS5zZXRTb3J0TW9kZWwoc29ydE1vZGVsKTtcbiAgICB9XG4gIH1cblxuICBwdWJsaWMgb25GaWx0ZXJVcGRhdGUoKSB7XG4gICAgaWYgKCF0aGlzLmV4cG9ydFNlYXJjaFBhcmFtcykge1xuICAgICAgcmV0dXJuO1xuICAgIH0gZWxzZSB7XG4gICAgICBjb25zdCBmaWx0ZXJlZFNlYXJjaFJlc3VsdHMgPSBbXTtcbiAgICAgIHRoaXMuZ3JpZEFwaS5mb3JFYWNoTm9kZUFmdGVyRmlsdGVyKHJvd05vZGUgPT4ge1xuICAgICAgICBmaWx0ZXJlZFNlYXJjaFJlc3VsdHNbZmlsdGVyZWRTZWFyY2hSZXN1bHRzLmxlbmd0aF0gPVxuICAgICAgICAgIHJvd05vZGUuZGF0YTtcbiAgICAgIH0pO1xuICAgICAgY29uc3QgZmlsdGVyVGVybXMgPSB0aGlzLmdyaWRBcGkuZ2V0RmlsdGVyTW9kZWwoKTtcbiAgICAgIGNvbnN0IHJlc3VsdHMgPSB7XG4gICAgICAgIHNlYXJjaFRlcm1zOiBmaWx0ZXJUZXJtcyxcbiAgICAgICAgc2VhcmNoUmVzdWx0czogZmlsdGVyZWRTZWFyY2hSZXN1bHRzXG4gICAgICB9O1xuICAgICAgdGhpcy5zZWFyY2hUZXJtcy5lbWl0KHJlc3VsdHMpO1xuICAgIH1cbiAgfVxuXG4gIHByaXZhdGUgY29udGV4dE1lbnVFdmVudChwYXJhbXMsIGV2ZW50VHlwZSkge1xuICAgIGNvbnN0IGV2ZW50RGF0YTogYW55ID0ge307XG4gICAgZXZlbnREYXRhW2V2ZW50VHlwZV0gPSBfLmNsb25lRGVlcChwYXJhbXMubm9kZS5kYXRhKTtcbiAgICB0aGlzLmNvbnRleHRNZW51T3V0cHV0LmVtaXQoZXZlbnREYXRhKTtcbiAgfVxuXG4gIHB1YmxpYyBvblJvd0NsaWNrZWQocm93KSB7XG4gICAgc3dpdGNoICh0cnVlKSB7XG4gICAgICAvKiB0aGlzIGRldGVybWluZXMgaWYgYSB0b29sIHdhcyBjbGlja2VkLCB2cyBhIHJvdyBvciBjZWxsIGNsaWNrKi9cbiAgICAgIGNhc2UgdGhpcy50b29sV2FzQ2xpY2tlZChyb3cuZXZlbnQuc3JjRWxlbWVudC5pZCk6XG4gICAgICAgIHRoaXMuZXhwb3J0VG9vbENsaWNrLmVtaXQoe3Rvb2xJZDogcm93LmV2ZW50LnNyY0VsZW1lbnQuaWQsIHdlbGxib3JlOiByb3cuZGF0YX0pO1xuICAgICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgLy8gdG9vbCBidXR0b24gd2FzIG5vdCBjbGlja2VkLCBwcm9jZWVkIGFzIG5vcm1hbFxuICAgIGlmICghdGhpcy5leHBvcnRSb3dXaGVuQ2xpY2tlZCkge1xuICAgICAgcmV0dXJuO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLmV4cG9ydENsaWNrZWRSb3cuZW1pdChyb3cuZGF0YSk7XG4gICAgfVxuICB9XG5cbiAgcHVibGljIG9uQ2VsbENsaWNrZWQoY2VsbCkge1xuICAgIGlmICghY2VsbCkge1xuICAgICAgcmV0dXJuO1xuICAgIH1cbiAgICB0aGlzLmV4cG9ydENsaWNrZWRDZWxsLmVtaXQoY2VsbCk7XG4gIH1cblxuICBwdWJsaWMgb25DZWxsVmFsdWVDaGFuZ2VkKGV2ZW50KSB7XG4gICAgaWYgKCF0aGlzLmV4cG9ydENlbGxWYWx1ZUNoYW5nZSkge1xuICAgICAgcmV0dXJuO1xuICAgIH1cbiAgICBjb25zdCByZXN1bHRzID0ge2RhdGE6IGV2ZW50WydkYXRhJ119O1xuICAgIHRoaXMuZXhwb3J0Q2hhbmdlZFZhbHVlLmVtaXQocmVzdWx0cyk7XG4gIH1cblxuICBwcml2YXRlIGNyZWF0ZUNvbHVtbkRlZnMoKTogYW55IHtcbiAgICBjb25zdCB0YWJsZSA9IHRoaXMuX3RhYmxlRGVmO1xuXG4gICAgY29uc3QgY29sdW1uRGVmcyA9IFtdO1xuICAgIGZvciAoY29uc3QgY29sIG9mIHRhYmxlLmNvbHVtbkRlZnMpIHtcbiAgICAgIC8vIFNldCB1cCBmdXp6eSBmaWx0ZXIgY29sdW1uc1xuICAgICAgaWYgKGNvbC5oZWFkZXJOYW1lKSB7XG4gICAgICAgIHRoaXMuZmlsdGVyYWJsZUNvbHVtbnMucHVzaCg8RmlsdGVyYWJsZUNvbHVtbj4ge1xuICAgICAgICAgIG5hbWU6IGNvbC5oZWFkZXJOYW1lLFxuICAgICAgICAgIGZpZWxkOiBjb2wuZmllbGQsXG4gICAgICAgICAgaW5jbHVkZUluU2VhcmNoOiBjb2wuaW5jbHVkZUluU2VhcmNoXG4gICAgICAgIH0pO1xuICAgICAgfVxuXG4gICAgICAvLyBGb3IgZXhwb3J0aW5nXG4gICAgICBpZiAoY29sLmZpZWxkKSB7XG4gICAgICAgIHRoaXMuY29sdW1uTmFtZXMucHVzaChjb2wuZmllbGQpO1xuICAgICAgfVxuXG4gICAgICAvLyBUT0RPOiBjaGVja2JveFNlbGVjdGlvbiBmaWx0ZXJzXG4gICAgICBpZiAoIWNvbC5maWx0ZXIgJiYgIWNvbC5jaGVja2JveFNlbGVjdGlvbikge1xuICAgICAgICBjb2wuZmlsdGVyID0gJ2FnVGV4dENvbHVtbkZpbHRlcic7XG4gICAgICB9XG4gICAgICBpZiAoY29sLmNoZWNrYm94U2VsZWN0aW9uID09PSB0cnVlKSB7XG4gICAgICAgIGNvbC5zdXBwcmVzc0NvcHkgPSB0cnVlO1xuICAgICAgfVxuXG4gICAgICBpZiAoY29sLmZpbHRlciA9PT0gJ3NldCcpIHtcbiAgICAgICAgY29sLmZpbHRlclBhcmFtcyA9IHtcbiAgICAgICAgICBjZWxsUmVuZGVyZXI6IHRoaXMuZGVmYXVsdHMudXBkYXRlTnVsbFZhbHVlc1xuICAgICAgICB9O1xuICAgICAgfVxuICAgICAgaWYgKGNvbC5maWx0ZXIgPT09ICdkYXRlJykge1xuICAgICAgICBjb2wuZmlsdGVyUGFyYW1zID0ge1xuICAgICAgICAgIGNsZWFyQnV0dG9uOiB0cnVlLFxuICAgICAgICAgIHN1cHByZXNzQW5kT3JDb25kaXRpb246IHRydWUsXG4gICAgICAgICAgbmV3Um93c0FjdGlvbjogJ2tlZXAnLFxuICAgICAgICAgIGZpbHRlck9wdGlvbnM6IFtcbiAgICAgICAgICAgICdlcXVhbHMnLFxuICAgICAgICAgICAgJ25vdEVxdWFsJyxcbiAgICAgICAgICAgICdsZXNzVGhhbk9yRXF1YWwnLFxuICAgICAgICAgICAgJ2dyZWF0ZXJUaGFuJyxcbiAgICAgICAgICAgICdncmVhdGVyVGhhbk9yRXF1YWwnLFxuICAgICAgICAgICAgJ2luUmFuZ2UnXG4gICAgICAgICAgXSxcbiAgICAgICAgICBkZWZhdWx0T3B0aW9uOiAnZXF1YWxzJyxcbiAgICAgICAgICBicm93c2VyRGF0ZVBpY2tlcjogdHJ1ZSxcbiAgICAgICAgICBudWxsQ29tcGFyYXRvcjogZmFsc2UsXG4gICAgICAgICAgaW5SYW5nZUluY2x1c2l2ZTogdHJ1ZSxcbiAgICAgICAgICBjb21wYXJhdG9yOiB0aGlzLmRlZmF1bHRzLmRhdGVDb21wYXJhdG9yXG4gICAgICAgIH07XG4gICAgICB9XG4gICAgICBpZiAoIWNvbC5mbG9hdGluZ0ZpbHRlckNvbXBvbmVudFBhcmFtcykge1xuICAgICAgICBjb2wuZmxvYXRpbmdGaWx0ZXJDb21wb25lbnRQYXJhbXMgPSB7XG4gICAgICAgICAgc3VwcHJlc3NGaWx0ZXJCdXR0b246IHRydWVcbiAgICAgICAgfTtcbiAgICAgIH1cblxuICAgICAgLy8gQ2VsbCBSZW5kZXJlcnNcbiAgICAgIHRoaXMuY2VsbEZhY3Rvcnkuc2V0UmVuZGVyZXIoY29sKTtcblxuICAgICAgY29sdW1uRGVmcy5wdXNoKHtcbiAgICAgICAgLi4udGhpcy5kZWZhdWx0cy5nZXREZWZhdWx0cygnY29sRGVmRGVmYXVsdHMnKSxcbiAgICAgICAgLi4uY29sXG4gICAgICB9KTtcbiAgICB9XG4gICAgcmV0dXJuIGNvbHVtbkRlZnM7XG4gIH1cblxuICBwcml2YXRlIGNyZWF0ZUdyaWRPcHRpb25zKCk6IFRhYmxlT3B0aW9ucyB7XG4gICAgY29uc3QgZGVmYXVsdEdyaWRPcHRpb25zID0ge1xuICAgICAgLi4udGhpcy5kZWZhdWx0cy5nZXREZWZhdWx0cygndGFibGVEZWZEZWZhdWx0cycpLFxuICAgICAgLi4udGhpcy5fdGFibGVEZWYuZ3JpZE9wdGlvbnMsXG4gICAgICBnZXRSb3dTdHlsZTogdGhpcy5yb3dTdHlsZUZuLFxuICAgICAgZ2V0Um93Q2xhc3M6IHRoaXMucm93Q2xhc3NGbixcbiAgICAgIHJvd0NsYXNzUnVsZXM6IHRoaXMucm93Q2xhc3NSdWxlcyxcbiAgICAgIGNlbGxDbGFzc1J1bGVzOiB0aGlzLmNlbGxDbGFzc1J1bGVzLFxuICAgICAgY2VsbENsYXNzRm46IHRoaXMuY2VsbENsYXNzRm5cbiAgICB9O1xuICAgIGlmICghZGVmYXVsdEdyaWRPcHRpb25zLmNvbnRleHQpIHtcbiAgICAgIGRlZmF1bHRHcmlkT3B0aW9ucy5jb250ZXh0ID0gdGhpcztcbiAgICB9XG4gICAgcmV0dXJuIDxUYWJsZU9wdGlvbnM+IGRlZmF1bHRHcmlkT3B0aW9ucztcbiAgfVxuXG4gIHByaXZhdGUgbWFuYWdlVGFibGVBY3Rpb25zKCk6IGFueSB7XG4gICAgbGV0IGFjdGlvbkNvbCA9IHRoaXMuX3RhYmxlRGVmLmFjdGlvbnM7XG5cbiAgICBpZiAoIWFjdGlvbkNvbCkge1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIGlmICghYWN0aW9uQ29sLndpZHRoKSB7XG4gICAgICAvLyBidXR0b25zIGFyZSA0NXB4IHdpZGUsIHdpdGggMTBweCBvZiBwYWRkaW5nIGluYmV0d2VlblxuICAgICAgYWN0aW9uQ29sLndpZHRoID1cbiAgICAgICAgNDUgKiBhY3Rpb25Db2wuYnV0dG9ucy5sZW5ndGggK1xuICAgICAgICAxMCAqIGFjdGlvbkNvbC5idXR0b25zLmxlbmd0aCAtXG4gICAgICAgIDE7XG4gICAgfVxuXG4gICAgdGhpcy5jZWxsRmFjdG9yeS5zZXRSZW5kZXJlcihhY3Rpb25Db2wpO1xuXG4gICAgYWN0aW9uQ29sID0ge1xuICAgICAgLi4udGhpcy5kZWZhdWx0cy5nZXREZWZhdWx0cygnYWN0aW9uRGVmYXVsdHMnKSxcbiAgICAgIC4uLmFjdGlvbkNvbFxuICAgIH07XG5cbiAgICByZXR1cm4gYWN0aW9uQ29sO1xuICB9XG5cbiAgcHVibGljIHVwZGF0ZUNvbHNUb0ZpbHRlcihcbiAgICBjb2x1bW46IEZpbHRlcmFibGVDb2x1bW4sXG4gICAgZXZlbnQ6IEtleWJvYXJkRXZlbnRcbiAgKTogdm9pZCB7XG4gICAgZXZlbnQuc3RvcFByb3BhZ2F0aW9uKCk7XG4gICAgY29sdW1uLmluY2x1ZGVJblNlYXJjaCA9ICFjb2x1bW4uaW5jbHVkZUluU2VhcmNoO1xuXG4gICAgdGhpcy5ncmlkT3B0aW9ucy5hcGkuc2V0Um93RGF0YSh0aGlzLl9kYXRhKTtcbiAgICB0aGlzLmdyaWRPcHRpb25zLmFwaS5vbkZpbHRlckNoYW5nZWQoKTtcbiAgfVxuXG4gIHB1YmxpYyBmaWx0ZXIodGVybTogc3RyaW5nKTogdm9pZCB7XG4gICAgdGhpcy5maWx0ZXJUZXJtcy5uZXh0KHRlcm0pO1xuICB9XG5cbiAgcHVibGljIHJlc2l6ZUdyaWQoY2FsbGVkRnJvbVdoZXJlOiBzdHJpbmcpOiB2b2lkIHtcbiAgICBpZiAodGhpcy5ncmlkT3B0aW9ucy5hcGkpIHtcbiAgICAgIHRoaXMuZ3JpZE9wdGlvbnMuYXBpLnNpemVDb2x1bW5zVG9GaXQoKTtcbiAgICB9IGVsc2UgaWYgKHRoaXMuZ3JpZEFwaSkge1xuICAgICAgdGhpcy5ncmlkQXBpLnNpemVDb2x1bW5zVG9GaXQoKTtcbiAgICB9XG4gIH1cblxuICBwdWJsaWMgZXh0cmFUb29sVXNlZChldmVudCkge1xuICAgIHRoaXMuZXh0cmFUb29sQnRuVXNlZC5lbWl0KGV2ZW50KTtcbiAgfVxuXG4gIHByaXZhdGUgdG9vbFdhc0NsaWNrZWQoaWQ6IHN0cmluZyk6IGJvb2xlYW4ge1xuICAgIGNvbnN0IGNoYXRQYXR0ID0gbmV3IFJlZ0V4cCgnKChjbGVhckNoYXRfKVtBLXogMC05XXs5LH0pJyk7XG4gICAgcmV0dXJuIGNoYXRQYXR0LnRlc3QoaWQpO1xuICB9XG5cbiAgbmdPbkRlc3Ryb3koKSB7fVxufVxuIl19