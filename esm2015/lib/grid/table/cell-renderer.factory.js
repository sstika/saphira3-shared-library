/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { RowButtonComponent } from '../row-button.components';
import { NewFileCellRendererComponent } from '../cell-renderers/new-file-cell-renderer/new-file-cell-renderer.component';
import { AutomationCellRendererComponent } from '../cell-renderers/automation-cell-renderer/automation-cell-renderer.component';
import { ClickToolCellRendererComponent } from '../cell-renderers/click-tool-cell-renderer/click-tool-cell-renderer.component';
import { DropdownCellRendererComponent } from '../cell-renderers/dropdown-cell-renderer/dropdown-cell-renderer.component';
export class CellRendererFactory {
    /**
     * @param {?} column
     * @return {?}
     */
    setRenderer(column) {
        if (column.buttons) {
            column.cellRendererFramework = RowButtonComponent;
        }
        if (column.iconCell === 'flag') {
            column.cellRendererFramework = NewFileCellRendererComponent;
        }
        if (column.iconCell === 'click-tool') {
            column.cellRendererFramework = ClickToolCellRendererComponent;
        }
        if (column.iconCell === 'automation') {
            column.cellRendererFramework = AutomationCellRendererComponent;
        }
        if (column.iconCell === 'dropdown') {
            column.cellRendererFramework = DropdownCellRendererComponent;
        }
    }
}
CellRendererFactory.decorators = [
    { type: Injectable }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2VsbC1yZW5kZXJlci5mYWN0b3J5LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGhwdGVjaC9zYXBoaXJhLXNoYXJlZC1saWIvIiwic291cmNlcyI6WyJsaWIvZ3JpZC90YWJsZS9jZWxsLXJlbmRlcmVyLmZhY3RvcnkudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFFM0MsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFDOUQsT0FBTyxFQUFFLDRCQUE0QixFQUFFLE1BQU0sMkVBQTJFLENBQUM7QUFDekgsT0FBTyxFQUFFLCtCQUErQixFQUFFLE1BQU0sK0VBQStFLENBQUM7QUFDaEksT0FBTyxFQUFFLDhCQUE4QixFQUFFLE1BQU0sK0VBQStFLENBQUM7QUFDL0gsT0FBTyxFQUFFLDZCQUE2QixFQUFFLE1BQU0sMkVBQTJFLENBQUM7QUFHMUgsTUFBTSxPQUFPLG1CQUFtQjs7Ozs7SUFDdkIsV0FBVyxDQUFDLE1BQVc7UUFDNUIsSUFBSSxNQUFNLENBQUMsT0FBTyxFQUFFO1lBQ2xCLE1BQU0sQ0FBQyxxQkFBcUIsR0FBRyxrQkFBa0IsQ0FBQztTQUNuRDtRQUNELElBQUksTUFBTSxDQUFDLFFBQVEsS0FBSyxNQUFNLEVBQUU7WUFDOUIsTUFBTSxDQUFDLHFCQUFxQixHQUFHLDRCQUE0QixDQUFDO1NBQzdEO1FBQ0QsSUFBSSxNQUFNLENBQUMsUUFBUSxLQUFLLFlBQVksRUFBRTtZQUNwQyxNQUFNLENBQUMscUJBQXFCLEdBQUcsOEJBQThCLENBQUM7U0FDL0Q7UUFDRCxJQUFJLE1BQU0sQ0FBQyxRQUFRLEtBQUssWUFBWSxFQUFFO1lBQ3BDLE1BQU0sQ0FBQyxxQkFBcUIsR0FBRywrQkFBK0IsQ0FBQztTQUNoRTtRQUNELElBQUksTUFBTSxDQUFDLFFBQVEsS0FBSyxVQUFVLEVBQUU7WUFDbEMsTUFBTSxDQUFDLHFCQUFxQixHQUFHLDZCQUE2QixDQUFDO1NBQzlEO0lBQ0gsQ0FBQzs7O1lBbEJGLFVBQVUiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbmltcG9ydCB7IFJvd0J1dHRvbkNvbXBvbmVudCB9IGZyb20gJy4uL3Jvdy1idXR0b24uY29tcG9uZW50cyc7XG5pbXBvcnQgeyBOZXdGaWxlQ2VsbFJlbmRlcmVyQ29tcG9uZW50IH0gZnJvbSAnLi4vY2VsbC1yZW5kZXJlcnMvbmV3LWZpbGUtY2VsbC1yZW5kZXJlci9uZXctZmlsZS1jZWxsLXJlbmRlcmVyLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBBdXRvbWF0aW9uQ2VsbFJlbmRlcmVyQ29tcG9uZW50IH0gZnJvbSAnLi4vY2VsbC1yZW5kZXJlcnMvYXV0b21hdGlvbi1jZWxsLXJlbmRlcmVyL2F1dG9tYXRpb24tY2VsbC1yZW5kZXJlci5jb21wb25lbnQnO1xuaW1wb3J0IHsgQ2xpY2tUb29sQ2VsbFJlbmRlcmVyQ29tcG9uZW50IH0gZnJvbSAnLi4vY2VsbC1yZW5kZXJlcnMvY2xpY2stdG9vbC1jZWxsLXJlbmRlcmVyL2NsaWNrLXRvb2wtY2VsbC1yZW5kZXJlci5jb21wb25lbnQnO1xuaW1wb3J0IHsgRHJvcGRvd25DZWxsUmVuZGVyZXJDb21wb25lbnQgfSBmcm9tICcuLi9jZWxsLXJlbmRlcmVycy9kcm9wZG93bi1jZWxsLXJlbmRlcmVyL2Ryb3Bkb3duLWNlbGwtcmVuZGVyZXIuY29tcG9uZW50JztcblxuQEluamVjdGFibGUoKVxuZXhwb3J0IGNsYXNzIENlbGxSZW5kZXJlckZhY3Rvcnkge1xuICBwdWJsaWMgc2V0UmVuZGVyZXIoY29sdW1uOiBhbnkpOiB2b2lkIHtcbiAgICBpZiAoY29sdW1uLmJ1dHRvbnMpIHtcbiAgICAgIGNvbHVtbi5jZWxsUmVuZGVyZXJGcmFtZXdvcmsgPSBSb3dCdXR0b25Db21wb25lbnQ7XG4gICAgfVxuICAgIGlmIChjb2x1bW4uaWNvbkNlbGwgPT09ICdmbGFnJykge1xuICAgICAgY29sdW1uLmNlbGxSZW5kZXJlckZyYW1ld29yayA9IE5ld0ZpbGVDZWxsUmVuZGVyZXJDb21wb25lbnQ7XG4gICAgfVxuICAgIGlmIChjb2x1bW4uaWNvbkNlbGwgPT09ICdjbGljay10b29sJykge1xuICAgICAgY29sdW1uLmNlbGxSZW5kZXJlckZyYW1ld29yayA9IENsaWNrVG9vbENlbGxSZW5kZXJlckNvbXBvbmVudDtcbiAgICB9XG4gICAgaWYgKGNvbHVtbi5pY29uQ2VsbCA9PT0gJ2F1dG9tYXRpb24nKSB7XG4gICAgICBjb2x1bW4uY2VsbFJlbmRlcmVyRnJhbWV3b3JrID0gQXV0b21hdGlvbkNlbGxSZW5kZXJlckNvbXBvbmVudDtcbiAgICB9XG4gICAgaWYgKGNvbHVtbi5pY29uQ2VsbCA9PT0gJ2Ryb3Bkb3duJykge1xuICAgICAgY29sdW1uLmNlbGxSZW5kZXJlckZyYW1ld29yayA9IERyb3Bkb3duQ2VsbFJlbmRlcmVyQ29tcG9uZW50O1xuICAgIH1cbiAgfVxufVxuIl19