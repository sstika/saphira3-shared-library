/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, ViewChild } from '@angular/core';
import { MatDatepicker } from '@angular/material';
import { TimeService } from '../../../services/time.service';
export class DatePickerCellEditRendererComponent {
    /**
     * @param {?} timeService
     */
    constructor(timeService) {
        this.timeService = timeService;
    }
    /**
     * @return {?}
     */
    ngAfterViewInit() {
        this.picker.open();
    }
    /**
     * @return {?}
     */
    isPopup() {
        return false;
    }
    /**
     * @return {?}
     */
    isCancelBeforeStart() {
        return false;
    }
    /**
     * @return {?}
     */
    isCancelAfterEnd() {
        return false;
    }
    /**
     * @param {?} params
     * @return {?}
     */
    agInit(params) {
        this.params = params;
        this.value = params.value;
    }
    /**
     * @return {?}
     */
    getValue() {
        return this.timeService.standardDateFormatter(this.value, 'MM/DD/YYYY');
    }
    /**
     * @param {?} e
     * @return {?}
     */
    onSelectChange(e) {
        this.params.stopEditing();
    }
}
DatePickerCellEditRendererComponent.decorators = [
    { type: Component, args: [{
                selector: 'app-editor-cell',
                template: "<mat-form-field>\n  <input matInput [matDatepicker]=\"picker\" [(ngModel)]=\"value\" (dateInput)=\"onSelectChange($event)\">\n  <mat-datepicker-toggle matSuffix [for]=\"picker\"></mat-datepicker-toggle>\n  <mat-datepicker #picker></mat-datepicker>\n</mat-form-field>",
                styles: [".md-form-field{margin-top:-16px}"]
            }] }
];
/** @nocollapse */
DatePickerCellEditRendererComponent.ctorParameters = () => [
    { type: TimeService }
];
DatePickerCellEditRendererComponent.propDecorators = {
    picker: [{ type: ViewChild, args: ['picker', { read: MatDatepicker, static: true },] }]
};
if (false) {
    /** @type {?} */
    DatePickerCellEditRendererComponent.prototype.params;
    /** @type {?} */
    DatePickerCellEditRendererComponent.prototype.value;
    /** @type {?} */
    DatePickerCellEditRendererComponent.prototype.picker;
    /**
     * @type {?}
     * @private
     */
    DatePickerCellEditRendererComponent.prototype.timeService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGF0ZS1waWNrZXItY2VsbC1lZGl0LXJlbmRlcmVyLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BocHRlY2gvc2FwaGlyYS1zaGFyZWQtbGliLyIsInNvdXJjZXMiOlsibGliL2dyaWQvY2VsbC1yZW5kZXJlcnMvZGF0ZS1waWNrZXItY2VsbC1lZGl0LXJlbmRlcmVyL2RhdGUtcGlja2VyLWNlbGwtZWRpdC1yZW5kZXJlci5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsU0FBUyxFQUFpQixNQUFNLGVBQWUsQ0FBQztBQUdwRSxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sbUJBQW1CLENBQUM7QUFDbEQsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLGdDQUFnQyxDQUFDO0FBTzdELE1BQU0sT0FBTyxtQ0FBbUM7Ozs7SUFLOUMsWUFBb0IsV0FBd0I7UUFBeEIsZ0JBQVcsR0FBWCxXQUFXLENBQWE7SUFDNUMsQ0FBQzs7OztJQUVELGVBQWU7UUFDYixJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksRUFBRSxDQUFDO0lBQ3JCLENBQUM7Ozs7SUFFRCxPQUFPO1FBQ0wsT0FBTyxLQUFLLENBQUM7SUFDZixDQUFDOzs7O0lBRUQsbUJBQW1CO1FBQ2pCLE9BQU8sS0FBSyxDQUFDO0lBQ2YsQ0FBQzs7OztJQUVELGdCQUFnQjtRQUNkLE9BQU8sS0FBSyxDQUFDO0lBQ2YsQ0FBQzs7Ozs7SUFFRCxNQUFNLENBQUMsTUFBVztRQUNoQixJQUFJLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQztRQUNyQixJQUFJLENBQUMsS0FBSyxHQUFHLE1BQU0sQ0FBQyxLQUFLLENBQUM7SUFDNUIsQ0FBQzs7OztJQUVELFFBQVE7UUFDTixPQUFPLElBQUksQ0FBQyxXQUFXLENBQUMscUJBQXFCLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRSxZQUFZLENBQUMsQ0FBQztJQUMxRSxDQUFDOzs7OztJQUVELGNBQWMsQ0FBQyxDQUFDO1FBQ2QsSUFBSSxDQUFDLE1BQU0sQ0FBQyxXQUFXLEVBQUUsQ0FBQztJQUM1QixDQUFDOzs7WUF4Q0YsU0FBUyxTQUFDO2dCQUNULFFBQVEsRUFBRSxpQkFBaUI7Z0JBQzNCLHNSQUE4RDs7YUFFL0Q7Ozs7WUFOUSxXQUFXOzs7cUJBVWpCLFNBQVMsU0FBQyxRQUFRLEVBQUUsRUFBRSxJQUFJLEVBQUUsYUFBYSxFQUFFLE1BQU0sRUFBRSxJQUFJLEVBQUU7Ozs7SUFGMUQscURBQTBCOztJQUMxQixvREFBYzs7SUFDZCxxREFBd0Y7Ozs7O0lBRTVFLDBEQUFnQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgVmlld0NoaWxkLCBBZnRlclZpZXdJbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBJQ2VsbEVkaXRvclBhcmFtcyB9IGZyb20gJ2FnLWdyaWQtY29tbXVuaXR5JztcbmltcG9ydCB7IEFnRWRpdG9yQ29tcG9uZW50IH0gZnJvbSAnYWctZ3JpZC1hbmd1bGFyJztcbmltcG9ydCB7IE1hdERhdGVwaWNrZXIgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbCc7XG5pbXBvcnQgeyBUaW1lU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL3NlcnZpY2VzL3RpbWUuc2VydmljZSc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2FwcC1lZGl0b3ItY2VsbCcsXG4gIHRlbXBsYXRlVXJsOiAnLi9kYXRlLXBpY2tlci1jZWxsLWVkaXQtcmVuZGVyZXIuY29tcG9uZW50Lmh0bWwnLFxuICBzdHlsZVVybHM6IFsnLi9kYXRlLXBpY2tlci1jZWxsLWVkaXQtcmVuZGVyZXIuY29tcG9uZW50LnNjc3MnXVxufSlcbmV4cG9ydCBjbGFzcyBEYXRlUGlja2VyQ2VsbEVkaXRSZW5kZXJlckNvbXBvbmVudCBpbXBsZW1lbnRzIEFnRWRpdG9yQ29tcG9uZW50LCBBZnRlclZpZXdJbml0IHtcbiAgcGFyYW1zOiBJQ2VsbEVkaXRvclBhcmFtcztcbiAgdmFsdWU6IHN0cmluZztcbiAgQFZpZXdDaGlsZCgncGlja2VyJywgeyByZWFkOiBNYXREYXRlcGlja2VyLCBzdGF0aWM6IHRydWUgfSkgcGlja2VyOiBNYXREYXRlcGlja2VyPERhdGU+O1xuXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgdGltZVNlcnZpY2U6IFRpbWVTZXJ2aWNlKSB7XG4gIH1cblxuICBuZ0FmdGVyVmlld0luaXQoKSB7XG4gICAgdGhpcy5waWNrZXIub3BlbigpO1xuICB9XG5cbiAgaXNQb3B1cCgpOiBib29sZWFuIHtcbiAgICByZXR1cm4gZmFsc2U7XG4gIH1cblxuICBpc0NhbmNlbEJlZm9yZVN0YXJ0KCk6IGJvb2xlYW4ge1xuICAgIHJldHVybiBmYWxzZTtcbiAgfVxuXG4gIGlzQ2FuY2VsQWZ0ZXJFbmQoKTogYm9vbGVhbiB7XG4gICAgcmV0dXJuIGZhbHNlO1xuICB9XG5cbiAgYWdJbml0KHBhcmFtczogYW55KTogdm9pZCB7XG4gICAgdGhpcy5wYXJhbXMgPSBwYXJhbXM7XG4gICAgdGhpcy52YWx1ZSA9IHBhcmFtcy52YWx1ZTtcbiAgfVxuXG4gIGdldFZhbHVlKCk6IHN0cmluZyB7XG4gICAgcmV0dXJuIHRoaXMudGltZVNlcnZpY2Uuc3RhbmRhcmREYXRlRm9ybWF0dGVyKHRoaXMudmFsdWUsICdNTS9ERC9ZWVlZJyk7XG4gIH1cblxuICBvblNlbGVjdENoYW5nZShlKTogdm9pZCB7XG4gICAgdGhpcy5wYXJhbXMuc3RvcEVkaXRpbmcoKTtcbiAgfVxufVxuIl19