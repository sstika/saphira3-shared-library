/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
/**
 * @record
 */
export function Button() { }
if (false) {
    /** @type {?} */
    Button.prototype.type;
    /** @type {?} */
    Button.prototype.icon;
    /** @type {?} */
    Button.prototype.color;
    /** @type {?|undefined} */
    Button.prototype.navigateTo;
    /** @type {?|undefined} */
    Button.prototype.queryParams;
    /** @type {?|undefined} */
    Button.prototype.newWindow;
}
/**
 * @record
 */
export function RowButtonEvent() { }
if (false) {
    /** @type {?} */
    RowButtonEvent.prototype.type;
    /** @type {?} */
    RowButtonEvent.prototype.rowData;
}
export class RowButtonComponent {
    /**
     * @param {?} activatedRoute
     * @param {?} router
     */
    constructor(activatedRoute, router) {
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.idOfMostRecentCommit = '';
    }
    /**
     * @param {?} params
     * @return {?}
     */
    agInit(params) {
        // Row data
        this.data = params.data;
        this.tableData = params.context._data;
        this.buttons = (/** @type {?} */ (params.colDef.buttons));
        this.setIcons();
        this.outputEmitter = params.context.buttonOutput;
    }
    /**
     * @private
     * @return {?}
     */
    setIcons() {
        for (const button of this.buttons) {
            switch (button.type) {
                // Dual table, add row to selection
                case 'add': {
                    button.icon = 'add_circle';
                    button.color = '#03A9F4';
                    break;
                }
                case 'delete': {
                    // delete is set to remove the most recent breadcrumb and is used
                    // to route user after use - use remove if this is undesirable
                    button.icon = 'delete';
                    button.color = '#ef5350';
                    break;
                }
                case 'edit': {
                    button.icon = 'mode_edit';
                    button.color = '#4caf50';
                    break;
                }
                case 'info': {
                    button.icon = 'info_outline';
                    button.color = '#03A9F4';
                    break;
                }
                // Dual table, remove row from selection
                case 'remove': {
                    button.icon = 'remove_circle';
                    button.color = '#F44336';
                    break;
                }
            }
        }
    }
    /**
     * @param {?} button
     * @return {?}
     */
    clicked(button) {
        if (button.navigateTo) {
            // interpolate URL
            /** @type {?} */
            let url = [];
            if (typeof button.navigateTo === 'string') {
                url = [this.interpolate(button.navigateTo)];
            }
            else if (button.navigateTo instanceof Array) {
                for (const s of button.navigateTo) {
                    url.push(this.interpolate(s));
                }
            }
        }
        /** @type {?} */
        const d = {
            type: button.type,
            rowData: this.data
        };
        this.outputEmitter.emit(d);
    }
    /**
     * @private
     * @param {?} target
     * @return {?}
     */
    interpolate(target) {
        /** @type {?} */
        const interpolation = /\$\{(\w+)\}/g;
        /** @type {?} */
        let match;
        while ((match = interpolation.exec(target))) {
            /** @type {?} */
            const key = match[1];
            // this checks for routing from within the tree table
            if (Array.isArray(this.data[key]) && this.data[key].length > 1) {
                target = target.replace('${' + key + '}', this.data[key][0]);
            }
            else {
                target = target.replace('${' + key + '}', this.data[key]);
            }
        }
        return target;
    }
    /**
     * @return {?}
     */
    refresh() {
        return false;
    }
}
RowButtonComponent.decorators = [
    { type: Component, args: [{
                selector: 'app-grid-row-button',
                template: `
    <button mat-icon-button
            [style.color]="button.color"
            (click)="clicked(button)"
            *ngFor="let button of buttons"
            class="grid-row-btn">
      <span class="material-icons" [ngClass]="button.icon">
        <mat-icon>{{button.icon}}</mat-icon>
      </span>
    </button>
    `
            }] }
];
/** @nocollapse */
RowButtonComponent.ctorParameters = () => [
    { type: ActivatedRoute },
    { type: Router }
];
if (false) {
    /**
     * @type {?}
     * @private
     */
    RowButtonComponent.prototype.data;
    /**
     * @type {?}
     * @private
     */
    RowButtonComponent.prototype.tableData;
    /** @type {?} */
    RowButtonComponent.prototype.buttons;
    /**
     * @type {?}
     * @private
     */
    RowButtonComponent.prototype.idOfMostRecentCommit;
    /**
     * @type {?}
     * @private
     */
    RowButtonComponent.prototype.outputEmitter;
    /** @type {?} */
    RowButtonComponent.prototype.activatedRoute;
    /**
     * @type {?}
     * @private
     */
    RowButtonComponent.prototype.router;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicm93LWJ1dHRvbi5jb21wb25lbnRzLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGhwdGVjaC9zYXBoaXJhLXNoYXJlZC1saWIvIiwic291cmNlcyI6WyJsaWIvZ3JpZC9yb3ctYnV0dG9uLmNvbXBvbmVudHMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQWdCLE1BQU0sZUFBZSxDQUFDO0FBRXhELE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSxFQUFFLE1BQU0saUJBQWlCLENBQUM7Ozs7QUFFekQsNEJBT0M7OztJQU5DLHNCQUFhOztJQUNiLHNCQUFhOztJQUNiLHVCQUFjOztJQUNkLDRCQUErQjs7SUFDL0IsNkJBQXdDOztJQUN4QywyQkFBb0I7Ozs7O0FBR3RCLG9DQUErRDs7O0lBQTdCLDhCQUFhOztJQUFDLGlDQUFhOztBQWdCN0QsTUFBTSxPQUFPLGtCQUFrQjs7Ozs7SUFPN0IsWUFDUyxjQUE4QixFQUM3QixNQUFjO1FBRGYsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBQzdCLFdBQU0sR0FBTixNQUFNLENBQVE7UUFMaEIseUJBQW9CLEdBQUcsRUFBRSxDQUFDO0lBTTlCLENBQUM7Ozs7O0lBRUwsTUFBTSxDQUFDLE1BQVc7UUFDaEIsV0FBVztRQUNYLElBQUksQ0FBQyxJQUFJLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQztRQUN4QixJQUFJLENBQUMsU0FBUyxHQUFHLE1BQU0sQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDO1FBRXRDLElBQUksQ0FBQyxPQUFPLEdBQUcsbUJBQUEsTUFBTSxDQUFDLE1BQU0sQ0FBQyxPQUFPLEVBQVksQ0FBQztRQUNqRCxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7UUFFaEIsSUFBSSxDQUFDLGFBQWEsR0FBRyxNQUFNLENBQUMsT0FBTyxDQUFDLFlBQVksQ0FBQztJQUNuRCxDQUFDOzs7OztJQUVPLFFBQVE7UUFDZCxLQUFLLE1BQU0sTUFBTSxJQUFJLElBQUksQ0FBQyxPQUFPLEVBQUU7WUFDakMsUUFBUSxNQUFNLENBQUMsSUFBSSxFQUFFO2dCQUNuQixtQ0FBbUM7Z0JBQ25DLEtBQUssS0FBSyxDQUFDLENBQUM7b0JBQ1YsTUFBTSxDQUFDLElBQUksR0FBRyxZQUFZLENBQUM7b0JBQzNCLE1BQU0sQ0FBQyxLQUFLLEdBQUcsU0FBUyxDQUFDO29CQUN6QixNQUFNO2lCQUNQO2dCQUNELEtBQUssUUFBUSxDQUFDLENBQUM7b0JBQ2IsaUVBQWlFO29CQUNqRSw4REFBOEQ7b0JBQzlELE1BQU0sQ0FBQyxJQUFJLEdBQUcsUUFBUSxDQUFDO29CQUN2QixNQUFNLENBQUMsS0FBSyxHQUFHLFNBQVMsQ0FBQztvQkFDekIsTUFBTTtpQkFDUDtnQkFDRCxLQUFLLE1BQU0sQ0FBQyxDQUFDO29CQUNYLE1BQU0sQ0FBQyxJQUFJLEdBQUcsV0FBVyxDQUFDO29CQUMxQixNQUFNLENBQUMsS0FBSyxHQUFHLFNBQVMsQ0FBQztvQkFDekIsTUFBTTtpQkFDUDtnQkFDRCxLQUFLLE1BQU0sQ0FBQyxDQUFDO29CQUNYLE1BQU0sQ0FBQyxJQUFJLEdBQUcsY0FBYyxDQUFDO29CQUM3QixNQUFNLENBQUMsS0FBSyxHQUFHLFNBQVMsQ0FBQztvQkFDekIsTUFBTTtpQkFDUDtnQkFDRCx3Q0FBd0M7Z0JBQ3hDLEtBQUssUUFBUSxDQUFDLENBQUM7b0JBQ2IsTUFBTSxDQUFDLElBQUksR0FBRyxlQUFlLENBQUM7b0JBQzlCLE1BQU0sQ0FBQyxLQUFLLEdBQUcsU0FBUyxDQUFDO29CQUN6QixNQUFNO2lCQUNQO2FBQ0Y7U0FDRjtJQUNILENBQUM7Ozs7O0lBRU0sT0FBTyxDQUFDLE1BQWM7UUFDM0IsSUFBSSxNQUFNLENBQUMsVUFBVSxFQUFFOzs7Z0JBRWpCLEdBQUcsR0FBYSxFQUFFO1lBQ3RCLElBQUksT0FBTyxNQUFNLENBQUMsVUFBVSxLQUFLLFFBQVEsRUFBRTtnQkFDekMsR0FBRyxHQUFHLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQzthQUM3QztpQkFBTSxJQUFJLE1BQU0sQ0FBQyxVQUFVLFlBQVksS0FBSyxFQUFFO2dCQUM3QyxLQUFLLE1BQU0sQ0FBQyxJQUFJLE1BQU0sQ0FBQyxVQUFVLEVBQUU7b0JBQ2pDLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO2lCQUMvQjthQUNGO1NBRUY7O2NBQ0ssQ0FBQyxHQUFtQjtZQUN4QixJQUFJLEVBQUUsTUFBTSxDQUFDLElBQUk7WUFDakIsT0FBTyxFQUFFLElBQUksQ0FBQyxJQUFJO1NBQ25CO1FBQ0QsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDN0IsQ0FBQzs7Ozs7O0lBRU8sV0FBVyxDQUFDLE1BQWM7O2NBQzFCLGFBQWEsR0FBRyxjQUFjOztZQUNoQyxLQUFLO1FBQ1QsT0FBTyxDQUFDLEtBQUssR0FBRyxhQUFhLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLEVBQUU7O2tCQUNyQyxHQUFHLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQztZQUNwQixxREFBcUQ7WUFDckQsSUFBSSxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7Z0JBQzlELE1BQU0sR0FBRyxNQUFNLENBQUMsT0FBTyxDQUFDLElBQUksR0FBRyxHQUFHLEdBQUcsR0FBRyxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQzthQUM5RDtpQkFBTTtnQkFDTCxNQUFNLEdBQUcsTUFBTSxDQUFDLE9BQU8sQ0FBQyxJQUFJLEdBQUcsR0FBRyxHQUFHLEdBQUcsRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7YUFDM0Q7U0FDRjtRQUVELE9BQU8sTUFBTSxDQUFDO0lBQ2hCLENBQUM7Ozs7SUFFRCxPQUFPO1FBQ0wsT0FBTyxLQUFLLENBQUM7SUFDZixDQUFDOzs7WUEvR0YsU0FBUyxTQUFDO2dCQUNULFFBQVEsRUFBRSxxQkFBcUI7Z0JBQy9CLFFBQVEsRUFBRTs7Ozs7Ozs7OztLQVVQO2FBQ0o7Ozs7WUExQlEsY0FBYztZQUFFLE1BQU07Ozs7Ozs7SUE0QjdCLGtDQUFrQjs7Ozs7SUFDbEIsdUNBQXVCOztJQUN2QixxQ0FBeUI7Ozs7O0lBQ3pCLGtEQUFrQzs7Ozs7SUFDbEMsMkNBQW9EOztJQUdsRCw0Q0FBcUM7Ozs7O0lBQ3JDLG9DQUFzQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgRXZlbnRFbWl0dGVyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBBZ1JlbmRlcmVyQ29tcG9uZW50IH0gZnJvbSAnYWctZ3JpZC1hbmd1bGFyL21haW4nO1xuaW1wb3J0IHsgQWN0aXZhdGVkUm91dGUsIFJvdXRlciB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XG5cbmV4cG9ydCBpbnRlcmZhY2UgQnV0dG9uIHtcbiAgdHlwZTogc3RyaW5nO1xuICBpY29uOiBzdHJpbmc7XG4gIGNvbG9yOiBzdHJpbmc7XG4gIG5hdmlnYXRlVG8/OiBzdHJpbmcgfCBzdHJpbmdbXTtcbiAgcXVlcnlQYXJhbXM/OiB7IFtrZXk6IHN0cmluZ106IHN0cmluZyB9O1xuICBuZXdXaW5kb3c/OiBib29sZWFuO1xufVxuXG5leHBvcnQgaW50ZXJmYWNlIFJvd0J1dHRvbkV2ZW50IHsgdHlwZTogc3RyaW5nOyByb3dEYXRhOiBhbnk7IH1cblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnYXBwLWdyaWQtcm93LWJ1dHRvbicsXG4gIHRlbXBsYXRlOiBgXG4gICAgPGJ1dHRvbiBtYXQtaWNvbi1idXR0b25cbiAgICAgICAgICAgIFtzdHlsZS5jb2xvcl09XCJidXR0b24uY29sb3JcIlxuICAgICAgICAgICAgKGNsaWNrKT1cImNsaWNrZWQoYnV0dG9uKVwiXG4gICAgICAgICAgICAqbmdGb3I9XCJsZXQgYnV0dG9uIG9mIGJ1dHRvbnNcIlxuICAgICAgICAgICAgY2xhc3M9XCJncmlkLXJvdy1idG5cIj5cbiAgICAgIDxzcGFuIGNsYXNzPVwibWF0ZXJpYWwtaWNvbnNcIiBbbmdDbGFzc109XCJidXR0b24uaWNvblwiPlxuICAgICAgICA8bWF0LWljb24+e3tidXR0b24uaWNvbn19PC9tYXQtaWNvbj5cbiAgICAgIDwvc3Bhbj5cbiAgICA8L2J1dHRvbj5cbiAgICBgXG59KVxuZXhwb3J0IGNsYXNzIFJvd0J1dHRvbkNvbXBvbmVudCBpbXBsZW1lbnRzIEFnUmVuZGVyZXJDb21wb25lbnQge1xuICBwcml2YXRlIGRhdGE6IGFueTtcbiAgcHJpdmF0ZSB0YWJsZURhdGE6IGFueTtcbiAgcHVibGljIGJ1dHRvbnM6IEJ1dHRvbltdO1xuICBwcml2YXRlIGlkT2ZNb3N0UmVjZW50Q29tbWl0ID0gJyc7XG4gIHByaXZhdGUgb3V0cHV0RW1pdHRlcjogRXZlbnRFbWl0dGVyPFJvd0J1dHRvbkV2ZW50PjtcblxuICBjb25zdHJ1Y3RvcihcbiAgICBwdWJsaWMgYWN0aXZhdGVkUm91dGU6IEFjdGl2YXRlZFJvdXRlLFxuICAgIHByaXZhdGUgcm91dGVyOiBSb3V0ZXJcbiAgKSB7IH1cblxuICBhZ0luaXQocGFyYW1zOiBhbnkpOiB2b2lkIHtcbiAgICAvLyBSb3cgZGF0YVxuICAgIHRoaXMuZGF0YSA9IHBhcmFtcy5kYXRhO1xuICAgIHRoaXMudGFibGVEYXRhID0gcGFyYW1zLmNvbnRleHQuX2RhdGE7XG5cbiAgICB0aGlzLmJ1dHRvbnMgPSBwYXJhbXMuY29sRGVmLmJ1dHRvbnMgYXMgQnV0dG9uW107XG4gICAgdGhpcy5zZXRJY29ucygpO1xuXG4gICAgdGhpcy5vdXRwdXRFbWl0dGVyID0gcGFyYW1zLmNvbnRleHQuYnV0dG9uT3V0cHV0O1xuICB9XG5cbiAgcHJpdmF0ZSBzZXRJY29ucygpOiB2b2lkIHtcbiAgICBmb3IgKGNvbnN0IGJ1dHRvbiBvZiB0aGlzLmJ1dHRvbnMpIHtcbiAgICAgIHN3aXRjaCAoYnV0dG9uLnR5cGUpIHtcbiAgICAgICAgLy8gRHVhbCB0YWJsZSwgYWRkIHJvdyB0byBzZWxlY3Rpb25cbiAgICAgICAgY2FzZSAnYWRkJzoge1xuICAgICAgICAgIGJ1dHRvbi5pY29uID0gJ2FkZF9jaXJjbGUnO1xuICAgICAgICAgIGJ1dHRvbi5jb2xvciA9ICcjMDNBOUY0JztcbiAgICAgICAgICBicmVhaztcbiAgICAgICAgfVxuICAgICAgICBjYXNlICdkZWxldGUnOiB7XG4gICAgICAgICAgLy8gZGVsZXRlIGlzIHNldCB0byByZW1vdmUgdGhlIG1vc3QgcmVjZW50IGJyZWFkY3J1bWIgYW5kIGlzIHVzZWRcbiAgICAgICAgICAvLyB0byByb3V0ZSB1c2VyIGFmdGVyIHVzZSAtIHVzZSByZW1vdmUgaWYgdGhpcyBpcyB1bmRlc2lyYWJsZVxuICAgICAgICAgIGJ1dHRvbi5pY29uID0gJ2RlbGV0ZSc7XG4gICAgICAgICAgYnV0dG9uLmNvbG9yID0gJyNlZjUzNTAnO1xuICAgICAgICAgIGJyZWFrO1xuICAgICAgICB9XG4gICAgICAgIGNhc2UgJ2VkaXQnOiB7XG4gICAgICAgICAgYnV0dG9uLmljb24gPSAnbW9kZV9lZGl0JztcbiAgICAgICAgICBidXR0b24uY29sb3IgPSAnIzRjYWY1MCc7XG4gICAgICAgICAgYnJlYWs7XG4gICAgICAgIH1cbiAgICAgICAgY2FzZSAnaW5mbyc6IHtcbiAgICAgICAgICBidXR0b24uaWNvbiA9ICdpbmZvX291dGxpbmUnO1xuICAgICAgICAgIGJ1dHRvbi5jb2xvciA9ICcjMDNBOUY0JztcbiAgICAgICAgICBicmVhaztcbiAgICAgICAgfVxuICAgICAgICAvLyBEdWFsIHRhYmxlLCByZW1vdmUgcm93IGZyb20gc2VsZWN0aW9uXG4gICAgICAgIGNhc2UgJ3JlbW92ZSc6IHtcbiAgICAgICAgICBidXR0b24uaWNvbiA9ICdyZW1vdmVfY2lyY2xlJztcbiAgICAgICAgICBidXR0b24uY29sb3IgPSAnI0Y0NDMzNic7XG4gICAgICAgICAgYnJlYWs7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICBwdWJsaWMgY2xpY2tlZChidXR0b246IEJ1dHRvbik6IHZvaWQge1xuICAgIGlmIChidXR0b24ubmF2aWdhdGVUbykge1xuICAgICAgLy8gaW50ZXJwb2xhdGUgVVJMXG4gICAgICBsZXQgdXJsOiBzdHJpbmdbXSA9IFtdO1xuICAgICAgaWYgKHR5cGVvZiBidXR0b24ubmF2aWdhdGVUbyA9PT0gJ3N0cmluZycpIHtcbiAgICAgICAgdXJsID0gW3RoaXMuaW50ZXJwb2xhdGUoYnV0dG9uLm5hdmlnYXRlVG8pXTtcbiAgICAgIH0gZWxzZSBpZiAoYnV0dG9uLm5hdmlnYXRlVG8gaW5zdGFuY2VvZiBBcnJheSkge1xuICAgICAgICBmb3IgKGNvbnN0IHMgb2YgYnV0dG9uLm5hdmlnYXRlVG8pIHtcbiAgICAgICAgICB1cmwucHVzaCh0aGlzLmludGVycG9sYXRlKHMpKTtcbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgfVxuICAgIGNvbnN0IGQ6IFJvd0J1dHRvbkV2ZW50ID0ge1xuICAgICAgdHlwZTogYnV0dG9uLnR5cGUsXG4gICAgICByb3dEYXRhOiB0aGlzLmRhdGFcbiAgICB9O1xuICAgIHRoaXMub3V0cHV0RW1pdHRlci5lbWl0KGQpO1xuICB9XG5cbiAgcHJpdmF0ZSBpbnRlcnBvbGF0ZSh0YXJnZXQ6IHN0cmluZyk6IHN0cmluZyB7XG4gICAgY29uc3QgaW50ZXJwb2xhdGlvbiA9IC9cXCRcXHsoXFx3KylcXH0vZztcbiAgICBsZXQgbWF0Y2g7XG4gICAgd2hpbGUgKChtYXRjaCA9IGludGVycG9sYXRpb24uZXhlYyh0YXJnZXQpKSkge1xuICAgICAgY29uc3Qga2V5ID0gbWF0Y2hbMV07XG4gICAgICAvLyB0aGlzIGNoZWNrcyBmb3Igcm91dGluZyBmcm9tIHdpdGhpbiB0aGUgdHJlZSB0YWJsZVxuICAgICAgaWYgKEFycmF5LmlzQXJyYXkodGhpcy5kYXRhW2tleV0pICYmIHRoaXMuZGF0YVtrZXldLmxlbmd0aCA+IDEpIHtcbiAgICAgICAgdGFyZ2V0ID0gdGFyZ2V0LnJlcGxhY2UoJyR7JyArIGtleSArICd9JywgdGhpcy5kYXRhW2tleV1bMF0pO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgdGFyZ2V0ID0gdGFyZ2V0LnJlcGxhY2UoJyR7JyArIGtleSArICd9JywgdGhpcy5kYXRhW2tleV0pO1xuICAgICAgfVxuICAgIH1cblxuICAgIHJldHVybiB0YXJnZXQ7XG4gIH1cblxuICByZWZyZXNoKCk6IGJvb2xlYW4ge1xuICAgIHJldHVybiBmYWxzZTtcbiAgfVxufVxuIl19