export declare const environment: {
    production: boolean;
    googleAPIKey: string;
    apiRoot: string;
};
