/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function CustomGridOptions() { }
if (false) {
    /** @type {?|undefined} */
    CustomGridOptions.prototype.exportPrefix;
}
var FilterableColumn = /** @class */ (function () {
    function FilterableColumn() {
    }
    return FilterableColumn;
}());
export { FilterableColumn };
if (false) {
    /** @type {?} */
    FilterableColumn.prototype.name;
    /** @type {?} */
    FilterableColumn.prototype.field;
    /** @type {?} */
    FilterableColumn.prototype.includeInSearch;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGFibGUtdHlwZXMuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AaHB0ZWNoL3NhcGhpcmEtc2hhcmVkLWxpYi8iLCJzb3VyY2VzIjpbImxpYi9ncmlkL3RhYmxlL3RhYmxlLXR5cGVzLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7QUFBQSx1Q0FFQzs7O0lBREcseUNBQXNCOztBQUcxQjtJQUFBO0lBSUEsQ0FBQztJQUFELHVCQUFDO0FBQUQsQ0FBQyxBQUpELElBSUM7Ozs7SUFIRyxnQ0FBYTs7SUFDYixpQ0FBYzs7SUFDZCwyQ0FBeUIiLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgaW50ZXJmYWNlIEN1c3RvbUdyaWRPcHRpb25zIHtcbiAgICBleHBvcnRQcmVmaXg/OiBzdHJpbmc7XG59XG5cbmV4cG9ydCBjbGFzcyBGaWx0ZXJhYmxlQ29sdW1uIHtcbiAgICBuYW1lOiBzdHJpbmc7XG4gICAgZmllbGQ6IHN0cmluZztcbiAgICBpbmNsdWRlSW5TZWFyY2g6IGJvb2xlYW47XG59XG4iXX0=