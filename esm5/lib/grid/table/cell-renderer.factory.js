/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { RowButtonComponent } from '../row-button.components';
import { NewFileCellRendererComponent } from '../cell-renderers/new-file-cell-renderer/new-file-cell-renderer.component';
import { AutomationCellRendererComponent } from '../cell-renderers/automation-cell-renderer/automation-cell-renderer.component';
import { ClickToolCellRendererComponent } from '../cell-renderers/click-tool-cell-renderer/click-tool-cell-renderer.component';
import { DropdownCellRendererComponent } from '../cell-renderers/dropdown-cell-renderer/dropdown-cell-renderer.component';
var CellRendererFactory = /** @class */ (function () {
    function CellRendererFactory() {
    }
    /**
     * @param {?} column
     * @return {?}
     */
    CellRendererFactory.prototype.setRenderer = /**
     * @param {?} column
     * @return {?}
     */
    function (column) {
        if (column.buttons) {
            column.cellRendererFramework = RowButtonComponent;
        }
        if (column.iconCell === 'flag') {
            column.cellRendererFramework = NewFileCellRendererComponent;
        }
        if (column.iconCell === 'click-tool') {
            column.cellRendererFramework = ClickToolCellRendererComponent;
        }
        if (column.iconCell === 'automation') {
            column.cellRendererFramework = AutomationCellRendererComponent;
        }
        if (column.iconCell === 'dropdown') {
            column.cellRendererFramework = DropdownCellRendererComponent;
        }
    };
    CellRendererFactory.decorators = [
        { type: Injectable }
    ];
    return CellRendererFactory;
}());
export { CellRendererFactory };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2VsbC1yZW5kZXJlci5mYWN0b3J5LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGhwdGVjaC9zYXBoaXJhLXNoYXJlZC1saWIvIiwic291cmNlcyI6WyJsaWIvZ3JpZC90YWJsZS9jZWxsLXJlbmRlcmVyLmZhY3RvcnkudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFFM0MsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFDOUQsT0FBTyxFQUFFLDRCQUE0QixFQUFFLE1BQU0sMkVBQTJFLENBQUM7QUFDekgsT0FBTyxFQUFFLCtCQUErQixFQUFFLE1BQU0sK0VBQStFLENBQUM7QUFDaEksT0FBTyxFQUFFLDhCQUE4QixFQUFFLE1BQU0sK0VBQStFLENBQUM7QUFDL0gsT0FBTyxFQUFFLDZCQUE2QixFQUFFLE1BQU0sMkVBQTJFLENBQUM7QUFFMUg7SUFBQTtJQW1CQSxDQUFDOzs7OztJQWpCUSx5Q0FBVzs7OztJQUFsQixVQUFtQixNQUFXO1FBQzVCLElBQUksTUFBTSxDQUFDLE9BQU8sRUFBRTtZQUNsQixNQUFNLENBQUMscUJBQXFCLEdBQUcsa0JBQWtCLENBQUM7U0FDbkQ7UUFDRCxJQUFJLE1BQU0sQ0FBQyxRQUFRLEtBQUssTUFBTSxFQUFFO1lBQzlCLE1BQU0sQ0FBQyxxQkFBcUIsR0FBRyw0QkFBNEIsQ0FBQztTQUM3RDtRQUNELElBQUksTUFBTSxDQUFDLFFBQVEsS0FBSyxZQUFZLEVBQUU7WUFDcEMsTUFBTSxDQUFDLHFCQUFxQixHQUFHLDhCQUE4QixDQUFDO1NBQy9EO1FBQ0QsSUFBSSxNQUFNLENBQUMsUUFBUSxLQUFLLFlBQVksRUFBRTtZQUNwQyxNQUFNLENBQUMscUJBQXFCLEdBQUcsK0JBQStCLENBQUM7U0FDaEU7UUFDRCxJQUFJLE1BQU0sQ0FBQyxRQUFRLEtBQUssVUFBVSxFQUFFO1lBQ2xDLE1BQU0sQ0FBQyxxQkFBcUIsR0FBRyw2QkFBNkIsQ0FBQztTQUM5RDtJQUNILENBQUM7O2dCQWxCRixVQUFVOztJQW1CWCwwQkFBQztDQUFBLEFBbkJELElBbUJDO1NBbEJZLG1CQUFtQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuaW1wb3J0IHsgUm93QnV0dG9uQ29tcG9uZW50IH0gZnJvbSAnLi4vcm93LWJ1dHRvbi5jb21wb25lbnRzJztcbmltcG9ydCB7IE5ld0ZpbGVDZWxsUmVuZGVyZXJDb21wb25lbnQgfSBmcm9tICcuLi9jZWxsLXJlbmRlcmVycy9uZXctZmlsZS1jZWxsLXJlbmRlcmVyL25ldy1maWxlLWNlbGwtcmVuZGVyZXIuY29tcG9uZW50JztcbmltcG9ydCB7IEF1dG9tYXRpb25DZWxsUmVuZGVyZXJDb21wb25lbnQgfSBmcm9tICcuLi9jZWxsLXJlbmRlcmVycy9hdXRvbWF0aW9uLWNlbGwtcmVuZGVyZXIvYXV0b21hdGlvbi1jZWxsLXJlbmRlcmVyLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBDbGlja1Rvb2xDZWxsUmVuZGVyZXJDb21wb25lbnQgfSBmcm9tICcuLi9jZWxsLXJlbmRlcmVycy9jbGljay10b29sLWNlbGwtcmVuZGVyZXIvY2xpY2stdG9vbC1jZWxsLXJlbmRlcmVyLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBEcm9wZG93bkNlbGxSZW5kZXJlckNvbXBvbmVudCB9IGZyb20gJy4uL2NlbGwtcmVuZGVyZXJzL2Ryb3Bkb3duLWNlbGwtcmVuZGVyZXIvZHJvcGRvd24tY2VsbC1yZW5kZXJlci5jb21wb25lbnQnO1xuXG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgY2xhc3MgQ2VsbFJlbmRlcmVyRmFjdG9yeSB7XG4gIHB1YmxpYyBzZXRSZW5kZXJlcihjb2x1bW46IGFueSk6IHZvaWQge1xuICAgIGlmIChjb2x1bW4uYnV0dG9ucykge1xuICAgICAgY29sdW1uLmNlbGxSZW5kZXJlckZyYW1ld29yayA9IFJvd0J1dHRvbkNvbXBvbmVudDtcbiAgICB9XG4gICAgaWYgKGNvbHVtbi5pY29uQ2VsbCA9PT0gJ2ZsYWcnKSB7XG4gICAgICBjb2x1bW4uY2VsbFJlbmRlcmVyRnJhbWV3b3JrID0gTmV3RmlsZUNlbGxSZW5kZXJlckNvbXBvbmVudDtcbiAgICB9XG4gICAgaWYgKGNvbHVtbi5pY29uQ2VsbCA9PT0gJ2NsaWNrLXRvb2wnKSB7XG4gICAgICBjb2x1bW4uY2VsbFJlbmRlcmVyRnJhbWV3b3JrID0gQ2xpY2tUb29sQ2VsbFJlbmRlcmVyQ29tcG9uZW50O1xuICAgIH1cbiAgICBpZiAoY29sdW1uLmljb25DZWxsID09PSAnYXV0b21hdGlvbicpIHtcbiAgICAgIGNvbHVtbi5jZWxsUmVuZGVyZXJGcmFtZXdvcmsgPSBBdXRvbWF0aW9uQ2VsbFJlbmRlcmVyQ29tcG9uZW50O1xuICAgIH1cbiAgICBpZiAoY29sdW1uLmljb25DZWxsID09PSAnZHJvcGRvd24nKSB7XG4gICAgICBjb2x1bW4uY2VsbFJlbmRlcmVyRnJhbWV3b3JrID0gRHJvcGRvd25DZWxsUmVuZGVyZXJDb21wb25lbnQ7XG4gICAgfVxuICB9XG59XG4iXX0=