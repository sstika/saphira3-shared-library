/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component } from '@angular/core';
var CustomLoadingOverlayComponent = /** @class */ (function () {
    function CustomLoadingOverlayComponent() {
    }
    /**
     * @param {?} params
     * @return {?}
     */
    CustomLoadingOverlayComponent.prototype.agInit = /**
     * @param {?} params
     * @return {?}
     */
    function (params) {
        this.params = params;
    };
    CustomLoadingOverlayComponent.decorators = [
        { type: Component, args: [{
                    selector: 'app-loading-overlay',
                    template: "<div class=\"ag-overlay-loading-center\" style=\"height: 9%\">\n  <mat-spinner></mat-spinner>\n  <div>{{this.params.loadingMessage}}</div>\n</div>\n"
                }] }
    ];
    return CustomLoadingOverlayComponent;
}());
export { CustomLoadingOverlayComponent };
if (false) {
    /** @type {?} */
    CustomLoadingOverlayComponent.prototype.params;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY3VzdG9tLWxvYWRpbmctb3ZlcmxheS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AaHB0ZWNoL3NhcGhpcmEtc2hhcmVkLWxpYi8iLCJzb3VyY2VzIjpbImxpYi9ncmlkL3RhYmxlL2N1c3RvbS1sb2FkaW5nLW92ZXJsYXkuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFTLE1BQU0sZUFBZSxDQUFDO0FBR2pEO0lBQUE7SUFXQSxDQUFDOzs7OztJQUhHLDhDQUFNOzs7O0lBQU4sVUFBTyxNQUFNO1FBQ1QsSUFBSSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUM7SUFDekIsQ0FBQzs7Z0JBVkosU0FBUyxTQUFDO29CQUNQLFFBQVEsRUFBRSxxQkFBcUI7b0JBQy9CLGdLQUFzRDtpQkFDekQ7O0lBUUQsb0NBQUM7Q0FBQSxBQVhELElBV0M7U0FQWSw2QkFBNkI7OztJQUV0QywrQ0FBbUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIElucHV0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBJTG9hZGluZ092ZXJsYXlBbmd1bGFyQ29tcCB9IGZyb20gJ2FnLWdyaWQtYW5ndWxhcic7XG5cbkBDb21wb25lbnQoe1xuICAgIHNlbGVjdG9yOiAnYXBwLWxvYWRpbmctb3ZlcmxheScsXG4gICAgdGVtcGxhdGVVcmw6ICcuL2N1c3RvbS1sb2FkaW5nLW92ZXJsYXkuY29tcG9uZW50Lmh0bWwnXG59KVxuZXhwb3J0IGNsYXNzIEN1c3RvbUxvYWRpbmdPdmVybGF5Q29tcG9uZW50IGltcGxlbWVudHMgSUxvYWRpbmdPdmVybGF5QW5ndWxhckNvbXAge1xuXG4gICAgcHVibGljIHBhcmFtczogYW55O1xuXG4gICAgYWdJbml0KHBhcmFtcyk6IHZvaWQge1xuICAgICAgICB0aGlzLnBhcmFtcyA9IHBhcmFtcztcbiAgICB9XG59XG4iXX0=