/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { distinctUntilChanged } from 'rxjs/operators';
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Subject } from 'rxjs';
import * as _ from 'lodash';
import { TableDefaultsService } from './table-defaults.service';
import { CellRendererFactory } from './cell-renderer.factory';
import { CustomLoadingOverlayComponent } from './custom-loading-overlay.component';
import { DatePickerCellEditRendererComponent } from '../cell-renderers/date-picker-cell-edit-renderer/date-picker-cell-edit-renderer.component';
import { DateTimePickerComponent } from '../../components/date-time-picker/date-time-picker.component';
var TableComponent = /** @class */ (function () {
    // @ViewChild(TableToolsComponent) tableTools: TableToolsComponent;
    function TableComponent(cellFactory, defaults) {
        this.cellFactory = cellFactory;
        this.defaults = defaults;
        this.columnNames = [];
        this.filterableColumns = [];
        this.filterTerms = new Subject();
        this.gridOptions = {};
        this.toolPanelVisibility = 'hidden';
        this.overlayLoadingTemplate = '<span class="ag-overlay-loading-center" >Please wait while your rows are loading</span>';
        this.loadingText = 'Please Wait While Your Data is Loading';
        this._data = null;
        /* tslint:disable-next-line:no-output-rename */
        this.exportChangedValue = new EventEmitter();
        this.suppressContextMenu = false;
        /* tslint:disable-next-line:no-output-rename */
        this.searchTerms = new EventEmitter();
        /* tslint:disable-next-line:no-output-rename */
        this.exportClickedRow = new EventEmitter();
        /* tslint:disable-next-line:no-output-rename */
        this.exportClickedCell = new EventEmitter();
        this.draw = false;
        this.tableHeight = '';
        this.extraToolBtnUsed = new EventEmitter();
        /* tslint:disable-next-line:no-output-rename */
        this.buttonOutput = new EventEmitter();
        /* tslint:disable-next-line:no-output-rename */
        this.contextMenuOutput = new EventEmitter();
        /// used for tools in the chat cell, emits the
        /* tslint:disable-next-line:no-output-rename */
        this.exportToolClick = new EventEmitter();
    }
    Object.defineProperty(TableComponent.prototype, "getContextMenuItemsFromComponent", {
        set: /**
         * @param {?} theContextMenu
         * @return {?}
         */
        function (theContextMenu) {
            if (!theContextMenu) {
                return;
            }
            this.getContextMenuItems = theContextMenu;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TableComponent.prototype, "updateFilter", {
        set: /**
         * @param {?} search
         * @return {?}
         */
        function (search) {
            if (search && Object.keys(search).length) {
                this.gridApi.setFilterModel(search);
            }
            else if (search === {}) {
                this.gridApi.setFilterModel(null);
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TableComponent.prototype, "data", {
        set: /**
         * @param {?} theData
         * @return {?}
         */
        function (theData) {
            if (!theData && this.gridApi) {
                this.gridApi.showLoadingOverlay();
            }
            if (!theData) {
                return;
            }
            this._data = theData;
            if (!this.gridApi) {
                return;
            }
            this.gridApi.setRowData(this._data);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TableComponent.prototype, "redrawRows", {
        set: /**
         * @param {?} draw
         * @return {?}
         */
        function (draw) {
            this.draw = draw;
            if (this.draw && this.gridApi) {
                this.gridApi.redrawRows();
                this.draw = false;
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TableComponent.prototype, "tableDefinition", {
        set: /**
         * @param {?} tD
         * @return {?}
         */
        function (tD) {
            if (!tD) {
                return;
            }
            this._tableDef = _.cloneDeep(tD);
            this.gridOptions = this.createGridOptions();
            this.columnDefs = this.createColumnDefs();
            /** @type {?} */
            var actions = this.manageTableActions();
            if (actions) {
                this.columnDefs.push(actions);
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TableComponent.prototype, "height", {
        set: /**
         * @param {?} value
         * @return {?}
         */
        function (value) {
            this.tableHeight = value;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @return {?}
     */
    TableComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        var _this = this;
        this.getContextMenuActions = (/**
         * @param {?} params
         * @param {?} eventType
         * @return {?}
         */
        function (params, eventType) { return _this.contextMenuEvent(params, eventType); });
        this.frameworkComponents = {
            customLoadingOverlayComponent: CustomLoadingOverlayComponent,
            datePicker: DatePickerCellEditRendererComponent,
            dateTimePicker: DateTimePickerComponent
        };
        this.loadingOverlayComponent = 'customLoadingOverlayComponent';
        this.loadingOverlayComponentParams = { loadingMessage: this.loadingText };
        // returns custom context menu
        if (!this.getContextMenuItems) {
            this.suppressContextMenu = true;
            // The below line can be added back in for a default context menu including, copy cell/line/with & without headers & export
            // this.getContextMenuItems = this.defaults.getDefaults('contextMenu', this.getContextMenuItems);
        }
        this.filterTerms.pipe(distinctUntilChanged()).subscribe((/**
         * @param {?} filterTerm
         * @return {?}
         */
        function (filterTerm) {
            // Always set the filter string if it changes
            if (!_this.gridApi) {
                return;
            }
            _this.gridApi.setRowData(_this._data);
            _this.gridApi.onFilterChanged();
        }));
    };
    /**
     * @param {?} event
     * @return {?}
     */
    TableComponent.prototype.onGridReady = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        // this.autoSizeAll();
    };
    // **!! Unnecessary, but kept for posterity
    // autoSizeAll() {
    //   const allColumnIds = [];
    //   this.gridColumnApi.getAllColumns().forEach(function (column) {
    //     console.log('column', column)
    //     allColumnIds.push(column.colId);
    //   });
    //   this.gridColumnApi.autoSizeColumns(allColumnIds);
    // }
    // **!! Unnecessary, but kept for posterity
    // autoSizeAll() {
    //   const allColumnIds = [];
    //   this.gridColumnApi.getAllColumns().forEach(function (column) {
    //     console.log('column', column)
    //     allColumnIds.push(column.colId);
    //   });
    //   this.gridColumnApi.autoSizeColumns(allColumnIds);
    // }
    /**
     * @param {?} event
     * @return {?}
     */
    TableComponent.prototype.apiReady = 
    // **!! Unnecessary, but kept for posterity
    // autoSizeAll() {
    //   const allColumnIds = [];
    //   this.gridColumnApi.getAllColumns().forEach(function (column) {
    //     console.log('column', column)
    //     allColumnIds.push(column.colId);
    //   });
    //   this.gridColumnApi.autoSizeColumns(allColumnIds);
    // }
    /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        this.gridApi = event.api;
        this.gridColumnApi = event.columnApi;
        this.gridExportPrefix = this.gridOptions.exportPrefix;
        // this.gridApi.onFilterChanged;
        if (!this._data) {
            this.gridApi.showLoadingOverlay();
        }
        else {
            this.gridApi.setRowData(this._data);
        }
        if (this.sortOnLoad) {
            /** @type {?} */
            var sortModel = [{ colId: this.sortOnLoad, sort: 'desc' }];
            this.gridApi.setSortModel(sortModel);
        }
    };
    /**
     * @return {?}
     */
    TableComponent.prototype.onFilterUpdate = /**
     * @return {?}
     */
    function () {
        if (!this.exportSearchParams) {
            return;
        }
        else {
            /** @type {?} */
            var filteredSearchResults_1 = [];
            this.gridApi.forEachNodeAfterFilter((/**
             * @param {?} rowNode
             * @return {?}
             */
            function (rowNode) {
                filteredSearchResults_1[filteredSearchResults_1.length] =
                    rowNode.data;
            }));
            /** @type {?} */
            var filterTerms = this.gridApi.getFilterModel();
            /** @type {?} */
            var results = {
                searchTerms: filterTerms,
                searchResults: filteredSearchResults_1
            };
            this.searchTerms.emit(results);
        }
    };
    /**
     * @private
     * @param {?} params
     * @param {?} eventType
     * @return {?}
     */
    TableComponent.prototype.contextMenuEvent = /**
     * @private
     * @param {?} params
     * @param {?} eventType
     * @return {?}
     */
    function (params, eventType) {
        /** @type {?} */
        var eventData = {};
        eventData[eventType] = _.cloneDeep(params.node.data);
        this.contextMenuOutput.emit(eventData);
    };
    /**
     * @param {?} row
     * @return {?}
     */
    TableComponent.prototype.onRowClicked = /**
     * @param {?} row
     * @return {?}
     */
    function (row) {
        switch (true) {
            /* this determines if a tool was clicked, vs a row or cell click*/
            case this.toolWasClicked(row.event.srcElement.id):
                this.exportToolClick.emit({ toolId: row.event.srcElement.id, wellbore: row.data });
                return;
        }
        // tool button was not clicked, proceed as normal
        if (!this.exportRowWhenClicked) {
            return;
        }
        else {
            this.exportClickedRow.emit(row.data);
        }
    };
    /**
     * @param {?} cell
     * @return {?}
     */
    TableComponent.prototype.onCellClicked = /**
     * @param {?} cell
     * @return {?}
     */
    function (cell) {
        if (!cell) {
            return;
        }
        this.exportClickedCell.emit(cell);
    };
    /**
     * @param {?} event
     * @return {?}
     */
    TableComponent.prototype.onCellValueChanged = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        if (!this.exportCellValueChange) {
            return;
        }
        /** @type {?} */
        var results = { data: event['data'] };
        this.exportChangedValue.emit(results);
    };
    /**
     * @private
     * @return {?}
     */
    TableComponent.prototype.createColumnDefs = /**
     * @private
     * @return {?}
     */
    function () {
        var e_1, _a;
        /** @type {?} */
        var table = this._tableDef;
        /** @type {?} */
        var columnDefs = [];
        try {
            for (var _b = tslib_1.__values(table.columnDefs), _c = _b.next(); !_c.done; _c = _b.next()) {
                var col = _c.value;
                // Set up fuzzy filter columns
                if (col.headerName) {
                    this.filterableColumns.push((/** @type {?} */ ({
                        name: col.headerName,
                        field: col.field,
                        includeInSearch: col.includeInSearch
                    })));
                }
                // For exporting
                if (col.field) {
                    this.columnNames.push(col.field);
                }
                // TODO: checkboxSelection filters
                if (!col.filter && !col.checkboxSelection) {
                    col.filter = 'agTextColumnFilter';
                }
                if (col.checkboxSelection === true) {
                    col.suppressCopy = true;
                }
                if (col.filter === 'set') {
                    col.filterParams = {
                        cellRenderer: this.defaults.updateNullValues
                    };
                }
                if (col.filter === 'date') {
                    col.filterParams = {
                        clearButton: true,
                        suppressAndOrCondition: true,
                        newRowsAction: 'keep',
                        filterOptions: [
                            'equals',
                            'notEqual',
                            'lessThanOrEqual',
                            'greaterThan',
                            'greaterThanOrEqual',
                            'inRange'
                        ],
                        defaultOption: 'equals',
                        browserDatePicker: true,
                        nullComparator: false,
                        inRangeInclusive: true,
                        comparator: this.defaults.dateComparator
                    };
                }
                if (!col.floatingFilterComponentParams) {
                    col.floatingFilterComponentParams = {
                        suppressFilterButton: true
                    };
                }
                // Cell Renderers
                this.cellFactory.setRenderer(col);
                columnDefs.push(tslib_1.__assign({}, this.defaults.getDefaults('colDefDefaults'), col));
            }
        }
        catch (e_1_1) { e_1 = { error: e_1_1 }; }
        finally {
            try {
                if (_c && !_c.done && (_a = _b.return)) _a.call(_b);
            }
            finally { if (e_1) throw e_1.error; }
        }
        return columnDefs;
    };
    /**
     * @private
     * @return {?}
     */
    TableComponent.prototype.createGridOptions = /**
     * @private
     * @return {?}
     */
    function () {
        /** @type {?} */
        var defaultGridOptions = tslib_1.__assign({}, this.defaults.getDefaults('tableDefDefaults'), this._tableDef.gridOptions, { getRowStyle: this.rowStyleFn, getRowClass: this.rowClassFn, rowClassRules: this.rowClassRules, cellClassRules: this.cellClassRules, cellClassFn: this.cellClassFn });
        if (!defaultGridOptions.context) {
            defaultGridOptions.context = this;
        }
        return (/** @type {?} */ (defaultGridOptions));
    };
    /**
     * @private
     * @return {?}
     */
    TableComponent.prototype.manageTableActions = /**
     * @private
     * @return {?}
     */
    function () {
        /** @type {?} */
        var actionCol = this._tableDef.actions;
        if (!actionCol) {
            return;
        }
        if (!actionCol.width) {
            // buttons are 45px wide, with 10px of padding inbetween
            actionCol.width =
                45 * actionCol.buttons.length +
                    10 * actionCol.buttons.length -
                    1;
        }
        this.cellFactory.setRenderer(actionCol);
        actionCol = tslib_1.__assign({}, this.defaults.getDefaults('actionDefaults'), actionCol);
        return actionCol;
    };
    /**
     * @param {?} column
     * @param {?} event
     * @return {?}
     */
    TableComponent.prototype.updateColsToFilter = /**
     * @param {?} column
     * @param {?} event
     * @return {?}
     */
    function (column, event) {
        event.stopPropagation();
        column.includeInSearch = !column.includeInSearch;
        this.gridOptions.api.setRowData(this._data);
        this.gridOptions.api.onFilterChanged();
    };
    /**
     * @param {?} term
     * @return {?}
     */
    TableComponent.prototype.filter = /**
     * @param {?} term
     * @return {?}
     */
    function (term) {
        this.filterTerms.next(term);
    };
    /**
     * @param {?} calledFromWhere
     * @return {?}
     */
    TableComponent.prototype.resizeGrid = /**
     * @param {?} calledFromWhere
     * @return {?}
     */
    function (calledFromWhere) {
        if (this.gridOptions.api) {
            this.gridOptions.api.sizeColumnsToFit();
        }
        else if (this.gridApi) {
            this.gridApi.sizeColumnsToFit();
        }
    };
    /**
     * @param {?} event
     * @return {?}
     */
    TableComponent.prototype.extraToolUsed = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        this.extraToolBtnUsed.emit(event);
    };
    /**
     * @private
     * @param {?} id
     * @return {?}
     */
    TableComponent.prototype.toolWasClicked = /**
     * @private
     * @param {?} id
     * @return {?}
     */
    function (id) {
        /** @type {?} */
        var chatPatt = new RegExp('((clearChat_)[A-z 0-9]{9,})');
        return chatPatt.test(id);
    };
    /**
     * @return {?}
     */
    TableComponent.prototype.ngOnDestroy = /**
     * @return {?}
     */
    function () { };
    TableComponent.decorators = [
        { type: Component, args: [{
                    selector: 'app-table',
                    template: "<mat-card *ngIf=\"gridOptions && columnDefs; else spinner\" class=\"card-for-grids\">\n    <!-- <div class=\"grid-tools\">\n        <div *ngIf=\"tableHeader\" [innerHTML]=\"tableHeader\" class=\"table-header\"></div>\n        <mat-form-field *ngIf=\"!hideGridFuzzyFilter\" class=\"fuzzy-search\">\n            <mat-icon matSuffix class=\"suffix-icon\" [matMenuTriggerFor]=\"fuzzyOptions\" matTooltip=\"Columns to Search\" matTooltipPosition=\"right\">filter_list</mat-icon>\n            <input matInput #filterInput (keyup)=\"filter(filterInput.value)\" placeholder=\"Search\">\n        </mat-form-field>\n        <table-tools [exportPrefix]=\"exportPrefix\" [columnNames]=\"columnNames\" [extraTableToolBtn]=\"extraTableToolBtn\" [gridApi]=\"gridApi\"\n            [gridColumnApi]=\"gridColumnApi\" [gridExportPrefix]=\"gridExportPrefix\" [toolPanelVisibility]=\"toolPanelVisibility\"\n            (extraToolBtnClick)=\"extraToolUsed($event)\" class=\"table-tools\">\n        </table-tools>\n        <mat-menu #fuzzyOptions=\"matMenu\">\n            <div *ngFor=\"let col of filterableColumns\" mat-menu-item (click)=\"updateColsToFilter(col, $event)\">\n                <mat-checkbox [checked]=\"col.includeInSearch\">{{col.name}}</mat-checkbox>\n            </div>\n        </mat-menu>\n    </div> -->\n    <div>\n        <ag-grid-angular\n            #agGrid class=\"ag-theme-material on-card\"\n            [gridOptions]=\"gridOptions\"\n            [columnDefs]=\"columnDefs\"\n            [treeData]=\"treeData\"\n            [getDataPath]=\"getDataPath\"\n            [animateRows]=\"animateRows\"\n            [groupDefaultExpanded]=\"groupDefaultExpanded\"\n            [suppressScrollOnNewData]=\"true\"\n            [autoGroupColumnDef]=\"autoGroupColumnDef\"\n            [icons]=\"icons\"\n            [rowDragManaged]=\"rowDragManaged\"\n            [pinnedTopRowData]=\"pinnedTopRowData\"\n            [suppressClipboardPaste]=\"suppressClipboardPaste\"\n            rowSelection=\"multiple\"\n            [suppressContextMenu]=\"suppressContextMenu\"\n            (gridReady)=\"apiReady($event)\"\n            (gridSizeChanged)=\"resizeGrid('gridSizeChanged')\"\n            (modelUpdated)=\"resizeGrid('modelUpdated')\"\n            [getContextMenuItems]=\"getContextMenuItems\"\n            (rowClicked)=\"onRowClicked($event)\"\n            (rowDoubleClicked)=\"onRowClicked($event)\"\n            (cellClicked)=\"onCellClicked($event)\"\n            (cellValueChanged)=\"onCellValueChanged($event)\"\n            (displayedColumnsChanged)=\"resizeGrid('hideColumns')\"\n            (filterChanged)=\"onFilterUpdate()\"\n            [rememberGroupStateWhenNewData]=\"rememberGroupStateWhenNewData\"\n            [frameworkComponents]=\"frameworkComponents\"\n            [loadingOverlayComponent]=\"loadingOverlayComponent\"\n            [loadingOverlayComponentParams]=\"loadingOverlayComponentParams\"\n            [ngStyle]=\"{'height': (tableHeight === 'auto') ? tableHeight : tableHeight + 'px'}\">\n        </ag-grid-angular>\n    </div>\n</mat-card>\n<ng-template #spinner>\n    <mat-spinner></mat-spinner>\n</ng-template>\n<!-- [overlayLoadingTemplate]=\"overlayLoadingTemplate\" -->\n",
                    providers: [CellRendererFactory, TableDefaultsService],
                    styles: [".dropdown-menu div.checkbox label{margin-left:10px}.grid-tools{-webkit-box-align:center;align-items:center;background-color:#f3f3f3;border-bottom:1px solid #c3c3c3;padding:5px 10px;display:-webkit-box;display:flex;-webkit-box-pack:justify;justify-content:space-between}.mat-card{padding:0}.on-card{width:100%}.suffix-icon{font-size:20px;cursor:pointer}.table-header{display:inline-block;text-align:center}.table-tools{margin-left:auto;padding-left:10px}"]
                }] }
    ];
    /** @nocollapse */
    TableComponent.ctorParameters = function () { return [
        { type: CellRendererFactory },
        { type: TableDefaultsService }
    ]; };
    TableComponent.propDecorators = {
        rowClassFn: [{ type: Input }],
        rowClassRules: [{ type: Input }],
        cellClassRules: [{ type: Input }],
        cellClassFn: [{ type: Input }],
        rowStyleFn: [{ type: Input }],
        exportPrefix: [{ type: Input }],
        sortOnLoad: [{ type: Input }],
        tableHeader: [{ type: Input }],
        extraTableToolBtn: [{ type: Input }],
        rowDragManaged: [{ type: Input }],
        pinnedTopRowData: [{ type: Input }],
        suppressClipboardPaste: [{ type: Input }],
        exportCellValueChange: [{ type: Input }],
        exportChangedValue: [{ type: Output, args: ['exportChangedValue',] }],
        getContextMenuItemsFromComponent: [{ type: Input }],
        components: [{ type: Input }],
        groupDefaultExpanded: [{ type: Input }],
        rememberGroupStateWhenNewData: [{ type: Input }],
        animateRows: [{ type: Input }],
        treeData: [{ type: Input }],
        autoGroupColumnDef: [{ type: Input }],
        icons: [{ type: Input }],
        getDataPath: [{ type: Input }],
        suppressContextMenu: [{ type: Input }],
        exportSearchParams: [{ type: Input }],
        searchTerms: [{ type: Output, args: ['search-terms',] }],
        updateFilter: [{ type: Input }],
        exportRowWhenClicked: [{ type: Input }],
        exportClickedRow: [{ type: Output, args: ['clicked-row',] }],
        exportCellWhenClicked: [{ type: Input }],
        exportClickedCell: [{ type: Output, args: ['exportClickedCell',] }],
        data: [{ type: Input }],
        redrawRows: [{ type: Input }],
        tableDefinition: [{ type: Input }],
        height: [{ type: Input }],
        extraToolBtnUsed: [{ type: Output }],
        buttonOutput: [{ type: Output, args: ['row-button-click',] }],
        contextMenuOutput: [{ type: Output, args: ['context-menu-event',] }],
        exportToolClick: [{ type: Output, args: ['tool-click',] }]
    };
    return TableComponent;
}());
export { TableComponent };
if (false) {
    /** @type {?} */
    TableComponent.prototype.columnDefs;
    /** @type {?} */
    TableComponent.prototype.columnNames;
    /** @type {?} */
    TableComponent.prototype.filterableColumns;
    /**
     * @type {?}
     * @private
     */
    TableComponent.prototype.filterTerms;
    /** @type {?} */
    TableComponent.prototype.gridOptions;
    /** @type {?} */
    TableComponent.prototype.toolPanelVisibility;
    /** @type {?} */
    TableComponent.prototype.overlayLoadingTemplate;
    /** @type {?} */
    TableComponent.prototype.getContextMenuItems;
    /** @type {?} */
    TableComponent.prototype.getContextMenuActions;
    /** @type {?} */
    TableComponent.prototype.loadingOverlayComponent;
    /** @type {?} */
    TableComponent.prototype.loadingOverlayComponentParams;
    /** @type {?} */
    TableComponent.prototype.frameworkComponents;
    /** @type {?} */
    TableComponent.prototype.loadingText;
    /**
     * @type {?}
     * @private
     */
    TableComponent.prototype._data;
    /** @type {?} */
    TableComponent.prototype._tableDef;
    /** @type {?} */
    TableComponent.prototype.gridApi;
    /** @type {?} */
    TableComponent.prototype.gridColumnApi;
    /** @type {?} */
    TableComponent.prototype.gridExportPrefix;
    /** @type {?} */
    TableComponent.prototype.rowClassFn;
    /** @type {?} */
    TableComponent.prototype.rowClassRules;
    /** @type {?} */
    TableComponent.prototype.cellClassRules;
    /** @type {?} */
    TableComponent.prototype.cellClassFn;
    /** @type {?} */
    TableComponent.prototype.rowStyleFn;
    /** @type {?} */
    TableComponent.prototype.exportPrefix;
    /** @type {?} */
    TableComponent.prototype.sortOnLoad;
    /** @type {?} */
    TableComponent.prototype.tableHeader;
    /** @type {?} */
    TableComponent.prototype.extraTableToolBtn;
    /** @type {?} */
    TableComponent.prototype.rowDragManaged;
    /** @type {?} */
    TableComponent.prototype.pinnedTopRowData;
    /** @type {?} */
    TableComponent.prototype.suppressClipboardPaste;
    /** @type {?} */
    TableComponent.prototype.exportCellValueChange;
    /** @type {?} */
    TableComponent.prototype.exportChangedValue;
    /** @type {?} */
    TableComponent.prototype.components;
    /** @type {?} */
    TableComponent.prototype.groupDefaultExpanded;
    /** @type {?} */
    TableComponent.prototype.rememberGroupStateWhenNewData;
    /** @type {?} */
    TableComponent.prototype.animateRows;
    /** @type {?} */
    TableComponent.prototype.treeData;
    /** @type {?} */
    TableComponent.prototype.autoGroupColumnDef;
    /** @type {?} */
    TableComponent.prototype.icons;
    /** @type {?} */
    TableComponent.prototype.getDataPath;
    /** @type {?} */
    TableComponent.prototype.suppressContextMenu;
    /** @type {?} */
    TableComponent.prototype.exportSearchParams;
    /** @type {?} */
    TableComponent.prototype.searchTerms;
    /** @type {?} */
    TableComponent.prototype.exportRowWhenClicked;
    /** @type {?} */
    TableComponent.prototype.exportClickedRow;
    /** @type {?} */
    TableComponent.prototype.exportCellWhenClicked;
    /** @type {?} */
    TableComponent.prototype.exportClickedCell;
    /** @type {?} */
    TableComponent.prototype.draw;
    /** @type {?} */
    TableComponent.prototype.tableHeight;
    /** @type {?} */
    TableComponent.prototype.extraToolBtnUsed;
    /** @type {?} */
    TableComponent.prototype.buttonOutput;
    /** @type {?} */
    TableComponent.prototype.contextMenuOutput;
    /** @type {?} */
    TableComponent.prototype.exportToolClick;
    /**
     * @type {?}
     * @private
     */
    TableComponent.prototype.cellFactory;
    /** @type {?} */
    TableComponent.prototype.defaults;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGFibGUuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGhwdGVjaC9zYXBoaXJhLXNoYXJlZC1saWIvIiwic291cmNlcyI6WyJsaWIvZ3JpZC90YWJsZS90YWJsZS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxPQUFPLEVBQUUsb0JBQW9CLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUN0RCxPQUFPLEVBQUUsU0FBUyxFQUFFLFlBQVksRUFBRSxLQUFLLEVBQXFCLE1BQU0sRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUkxRixPQUFPLEVBQUUsT0FBTyxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBQy9CLE9BQU8sS0FBSyxDQUFDLE1BQU0sUUFBUSxDQUFDO0FBSTVCLE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLDBCQUEwQixDQUFDO0FBRWhFLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLHlCQUF5QixDQUFDO0FBQzlELE9BQU8sRUFBRSw2QkFBNkIsRUFBRSxNQUFNLG9DQUFvQyxDQUFDO0FBQ25GLE9BQU8sRUFBRSxtQ0FBbUMsRUFBRSxNQUFNLDJGQUEyRixDQUFDO0FBQ2hKLE9BQU8sRUFBRSx1QkFBdUIsRUFBRSxNQUFNLDhEQUE4RCxDQUFDO0FBUXZHO0lBbUtFLG1FQUFtRTtJQUVuRSx3QkFDVSxXQUFnQyxFQUNqQyxRQUE4QjtRQUQ3QixnQkFBVyxHQUFYLFdBQVcsQ0FBcUI7UUFDakMsYUFBUSxHQUFSLFFBQVEsQ0FBc0I7UUE5SmhDLGdCQUFXLEdBQVUsRUFBRSxDQUFDO1FBQ3hCLHNCQUFpQixHQUF1QixFQUFFLENBQUM7UUFDMUMsZ0JBQVcsR0FBRyxJQUFJLE9BQU8sRUFBVSxDQUFDO1FBQ3JDLGdCQUFXLEdBQWlCLEVBQUUsQ0FBQztRQUMvQix3QkFBbUIsR0FBRyxRQUFRLENBQUM7UUFDL0IsMkJBQXNCLEdBQUcseUZBQXlGLENBQUM7UUFRbkgsZ0JBQVcsR0FBRyx3Q0FBd0MsQ0FBQztRQUd0RCxVQUFLLEdBQVMsSUFBSSxDQUFDOztRQTJCM0IsdUJBQWtCLEdBQXNCLElBQUksWUFBWSxFQUFPLENBQUM7UUFxQnZELHdCQUFtQixHQUFHLEtBQUssQ0FBQzs7UUFJckMsZ0JBQVcsR0FBc0IsSUFBSSxZQUFZLEVBQU8sQ0FBQzs7UUFjekQscUJBQWdCLEdBQXNCLElBQUksWUFBWSxFQUFPLENBQUM7O1FBSzlELHNCQUFpQixHQUFzQixJQUFJLFlBQVksRUFBTyxDQUFDO1FBbUIvRCxTQUFJLEdBQUcsS0FBSyxDQUFDO1FBMkJOLGdCQUFXLEdBQVcsRUFBRSxDQUFDO1FBTXRCLHFCQUFnQixHQUFzQixJQUFJLFlBQVksRUFBTyxDQUFDOztRQUl4RSxpQkFBWSxHQUFpQyxJQUFJLFlBQVksRUFBa0IsQ0FBQzs7UUFJaEYsc0JBQWlCLEdBQWlDLElBQUksWUFBWSxFQUFrQixDQUFDOzs7UUFLckYsb0JBQWUsR0FBc0IsSUFBSSxZQUFZLEVBQU8sQ0FBQztJQVE3RCxDQUFDO0lBbEhELHNCQUNJLDREQUFnQzs7Ozs7UUFEcEMsVUFDcUMsY0FBYztZQUNqRCxJQUFJLENBQUMsY0FBYyxFQUFFO2dCQUNuQixPQUFPO2FBQ1I7WUFDRCxJQUFJLENBQUMsbUJBQW1CLEdBQUcsY0FBYyxDQUFDO1FBQzVDLENBQUM7OztPQUFBO0lBa0JELHNCQUNJLHdDQUFZOzs7OztRQURoQixVQUNpQixNQUFNO1lBQ3JCLElBQUksTUFBTSxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsTUFBTSxFQUFFO2dCQUN4QyxJQUFJLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsQ0FBQzthQUNyQztpQkFBTSxJQUFJLE1BQU0sS0FBSyxFQUFFLEVBQUU7Z0JBQ3hCLElBQUksQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxDQUFDO2FBQ25DO1FBQ0gsQ0FBQzs7O09BQUE7SUFZRCxzQkFDSSxnQ0FBSTs7Ozs7UUFEUixVQUNTLE9BQWE7WUFDcEIsSUFBSSxDQUFDLE9BQU8sSUFBSSxJQUFJLENBQUMsT0FBTyxFQUFFO2dCQUM1QixJQUFJLENBQUMsT0FBTyxDQUFDLGtCQUFrQixFQUFFLENBQUM7YUFDbkM7WUFDRCxJQUFJLENBQUMsT0FBTyxFQUFFO2dCQUNaLE9BQU87YUFDUjtZQUNELElBQUksQ0FBQyxLQUFLLEdBQUcsT0FBTyxDQUFDO1lBRXJCLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFO2dCQUNqQixPQUFPO2FBQ1I7WUFFRCxJQUFJLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDdEMsQ0FBQzs7O09BQUE7SUFHRCxzQkFDSSxzQ0FBVTs7Ozs7UUFEZCxVQUNlLElBQUk7WUFDakIsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUM7WUFDakIsSUFBSSxJQUFJLENBQUMsSUFBSSxJQUFJLElBQUksQ0FBQyxPQUFPLEVBQUU7Z0JBQzdCLElBQUksQ0FBQyxPQUFPLENBQUMsVUFBVSxFQUFFLENBQUM7Z0JBQzFCLElBQUksQ0FBQyxJQUFJLEdBQUcsS0FBSyxDQUFDO2FBQ25CO1FBQ0gsQ0FBQzs7O09BQUE7SUFFRCxzQkFDSSwyQ0FBZTs7Ozs7UUFEbkIsVUFDb0IsRUFBWTtZQUM5QixJQUFJLENBQUMsRUFBRSxFQUFFO2dCQUNQLE9BQU87YUFDUjtZQUNELElBQUksQ0FBQyxTQUFTLEdBQUcsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsQ0FBQztZQUVqQyxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO1lBRTVDLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixFQUFFLENBQUM7O2dCQUVwQyxPQUFPLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixFQUFFO1lBQ3pDLElBQUksT0FBTyxFQUFFO2dCQUNYLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO2FBQy9CO1FBQ0gsQ0FBQzs7O09BQUE7SUFHRCxzQkFDSSxrQ0FBTTs7Ozs7UUFEVixVQUNXLEtBQUs7WUFDZCxJQUFJLENBQUMsV0FBVyxHQUFHLEtBQUssQ0FBQztRQUMzQixDQUFDOzs7T0FBQTs7OztJQXlCRCxpQ0FBUTs7O0lBQVI7UUFBQSxpQkF5QkM7UUF4QkMsSUFBSSxDQUFDLHFCQUFxQjs7Ozs7UUFBRyxVQUFDLE1BQU0sRUFBRSxTQUFTLElBQUssT0FBQSxLQUFJLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxFQUFFLFNBQVMsQ0FBQyxFQUF4QyxDQUF3QyxDQUFBLENBQUM7UUFDN0YsSUFBSSxDQUFDLG1CQUFtQixHQUFHO1lBQ3pCLDZCQUE2QixFQUFFLDZCQUE2QjtZQUM1RCxVQUFVLEVBQUUsbUNBQW1DO1lBQy9DLGNBQWMsRUFBRSx1QkFBdUI7U0FDeEMsQ0FBQztRQUNGLElBQUksQ0FBQyx1QkFBdUIsR0FBRywrQkFBK0IsQ0FBQztRQUMvRCxJQUFJLENBQUMsNkJBQTZCLEdBQUcsRUFBQyxjQUFjLEVBQUUsSUFBSSxDQUFDLFdBQVcsRUFBQyxDQUFDO1FBQ3hFLDhCQUE4QjtRQUM5QixJQUFJLENBQUMsSUFBSSxDQUFDLG1CQUFtQixFQUFFO1lBQzdCLElBQUksQ0FBQyxtQkFBbUIsR0FBRyxJQUFJLENBQUM7WUFDaEMsMkhBQTJIO1lBQzNILGlHQUFpRztTQUNsRztRQUNELElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLG9CQUFvQixFQUFFLENBQUMsQ0FBQyxTQUFTOzs7O1FBQUMsVUFBQSxVQUFVO1lBQ2hFLDZDQUE2QztZQUU3QyxJQUFJLENBQUMsS0FBSSxDQUFDLE9BQU8sRUFBRTtnQkFDakIsT0FBTzthQUNSO1lBRUQsS0FBSSxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsS0FBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ3BDLEtBQUksQ0FBQyxPQUFPLENBQUMsZUFBZSxFQUFFLENBQUM7UUFDakMsQ0FBQyxFQUFDLENBQUM7SUFDTCxDQUFDOzs7OztJQUVELG9DQUFXOzs7O0lBQVgsVUFBWSxLQUFLO1FBRWYsc0JBQXNCO0lBQ3hCLENBQUM7SUFFRCwyQ0FBMkM7SUFDM0Msa0JBQWtCO0lBQ2xCLDZCQUE2QjtJQUM3QixtRUFBbUU7SUFDbkUsb0NBQW9DO0lBQ3BDLHVDQUF1QztJQUN2QyxRQUFRO0lBQ1Isc0RBQXNEO0lBQ3RELElBQUk7Ozs7Ozs7Ozs7Ozs7O0lBRUosaUNBQVE7Ozs7Ozs7Ozs7Ozs7O0lBQVIsVUFBUyxLQUFrQjtRQUN6QixJQUFJLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQyxHQUFHLENBQUM7UUFDekIsSUFBSSxDQUFDLGFBQWEsR0FBRyxLQUFLLENBQUMsU0FBUyxDQUFDO1FBQ3JDLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLFlBQVksQ0FBQztRQUV0RCxnQ0FBZ0M7UUFDaEMsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUU7WUFDZixJQUFJLENBQUMsT0FBTyxDQUFDLGtCQUFrQixFQUFFLENBQUM7U0FDbkM7YUFBTTtZQUNMLElBQUksQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUNyQztRQUNELElBQUksSUFBSSxDQUFDLFVBQVUsRUFBRTs7Z0JBQ2IsU0FBUyxHQUFHLENBQUMsRUFBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLFVBQVUsRUFBRSxJQUFJLEVBQUUsTUFBTSxFQUFDLENBQUM7WUFDMUQsSUFBSSxDQUFDLE9BQU8sQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDLENBQUM7U0FDdEM7SUFDSCxDQUFDOzs7O0lBRU0sdUNBQWM7OztJQUFyQjtRQUNFLElBQUksQ0FBQyxJQUFJLENBQUMsa0JBQWtCLEVBQUU7WUFDNUIsT0FBTztTQUNSO2FBQU07O2dCQUNDLHVCQUFxQixHQUFHLEVBQUU7WUFDaEMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxzQkFBc0I7Ozs7WUFBQyxVQUFBLE9BQU87Z0JBQ3pDLHVCQUFxQixDQUFDLHVCQUFxQixDQUFDLE1BQU0sQ0FBQztvQkFDakQsT0FBTyxDQUFDLElBQUksQ0FBQztZQUNqQixDQUFDLEVBQUMsQ0FBQzs7Z0JBQ0csV0FBVyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsY0FBYyxFQUFFOztnQkFDM0MsT0FBTyxHQUFHO2dCQUNkLFdBQVcsRUFBRSxXQUFXO2dCQUN4QixhQUFhLEVBQUUsdUJBQXFCO2FBQ3JDO1lBQ0QsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7U0FDaEM7SUFDSCxDQUFDOzs7Ozs7O0lBRU8seUNBQWdCOzs7Ozs7SUFBeEIsVUFBeUIsTUFBTSxFQUFFLFNBQVM7O1lBQ2xDLFNBQVMsR0FBUSxFQUFFO1FBQ3pCLFNBQVMsQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDckQsSUFBSSxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztJQUN6QyxDQUFDOzs7OztJQUVNLHFDQUFZOzs7O0lBQW5CLFVBQW9CLEdBQUc7UUFDckIsUUFBUSxJQUFJLEVBQUU7WUFDWixrRUFBa0U7WUFDbEUsS0FBSyxJQUFJLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLEVBQUUsQ0FBQztnQkFDL0MsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsRUFBQyxNQUFNLEVBQUUsR0FBRyxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsRUFBRSxFQUFFLFFBQVEsRUFBRSxHQUFHLENBQUMsSUFBSSxFQUFDLENBQUMsQ0FBQztnQkFDakYsT0FBTztTQUNWO1FBRUQsaURBQWlEO1FBQ2pELElBQUksQ0FBQyxJQUFJLENBQUMsb0JBQW9CLEVBQUU7WUFDOUIsT0FBTztTQUNSO2FBQU07WUFDTCxJQUFJLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUN0QztJQUNILENBQUM7Ozs7O0lBRU0sc0NBQWE7Ozs7SUFBcEIsVUFBcUIsSUFBSTtRQUN2QixJQUFJLENBQUMsSUFBSSxFQUFFO1lBQ1QsT0FBTztTQUNSO1FBQ0QsSUFBSSxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUNwQyxDQUFDOzs7OztJQUVNLDJDQUFrQjs7OztJQUF6QixVQUEwQixLQUFLO1FBQzdCLElBQUksQ0FBQyxJQUFJLENBQUMscUJBQXFCLEVBQUU7WUFDL0IsT0FBTztTQUNSOztZQUNLLE9BQU8sR0FBRyxFQUFDLElBQUksRUFBRSxLQUFLLENBQUMsTUFBTSxDQUFDLEVBQUM7UUFDckMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUN4QyxDQUFDOzs7OztJQUVPLHlDQUFnQjs7OztJQUF4Qjs7O1lBQ1EsS0FBSyxHQUFHLElBQUksQ0FBQyxTQUFTOztZQUV0QixVQUFVLEdBQUcsRUFBRTs7WUFDckIsS0FBa0IsSUFBQSxLQUFBLGlCQUFBLEtBQUssQ0FBQyxVQUFVLENBQUEsZ0JBQUEsNEJBQUU7Z0JBQS9CLElBQU0sR0FBRyxXQUFBO2dCQUNaLDhCQUE4QjtnQkFDOUIsSUFBSSxHQUFHLENBQUMsVUFBVSxFQUFFO29CQUNsQixJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLG1CQUFtQjt3QkFDN0MsSUFBSSxFQUFFLEdBQUcsQ0FBQyxVQUFVO3dCQUNwQixLQUFLLEVBQUUsR0FBRyxDQUFDLEtBQUs7d0JBQ2hCLGVBQWUsRUFBRSxHQUFHLENBQUMsZUFBZTtxQkFDckMsRUFBQSxDQUFDLENBQUM7aUJBQ0o7Z0JBRUQsZ0JBQWdCO2dCQUNoQixJQUFJLEdBQUcsQ0FBQyxLQUFLLEVBQUU7b0JBQ2IsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDO2lCQUNsQztnQkFFRCxrQ0FBa0M7Z0JBQ2xDLElBQUksQ0FBQyxHQUFHLENBQUMsTUFBTSxJQUFJLENBQUMsR0FBRyxDQUFDLGlCQUFpQixFQUFFO29CQUN6QyxHQUFHLENBQUMsTUFBTSxHQUFHLG9CQUFvQixDQUFDO2lCQUNuQztnQkFDRCxJQUFJLEdBQUcsQ0FBQyxpQkFBaUIsS0FBSyxJQUFJLEVBQUU7b0JBQ2xDLEdBQUcsQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDO2lCQUN6QjtnQkFFRCxJQUFJLEdBQUcsQ0FBQyxNQUFNLEtBQUssS0FBSyxFQUFFO29CQUN4QixHQUFHLENBQUMsWUFBWSxHQUFHO3dCQUNqQixZQUFZLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxnQkFBZ0I7cUJBQzdDLENBQUM7aUJBQ0g7Z0JBQ0QsSUFBSSxHQUFHLENBQUMsTUFBTSxLQUFLLE1BQU0sRUFBRTtvQkFDekIsR0FBRyxDQUFDLFlBQVksR0FBRzt3QkFDakIsV0FBVyxFQUFFLElBQUk7d0JBQ2pCLHNCQUFzQixFQUFFLElBQUk7d0JBQzVCLGFBQWEsRUFBRSxNQUFNO3dCQUNyQixhQUFhLEVBQUU7NEJBQ2IsUUFBUTs0QkFDUixVQUFVOzRCQUNWLGlCQUFpQjs0QkFDakIsYUFBYTs0QkFDYixvQkFBb0I7NEJBQ3BCLFNBQVM7eUJBQ1Y7d0JBQ0QsYUFBYSxFQUFFLFFBQVE7d0JBQ3ZCLGlCQUFpQixFQUFFLElBQUk7d0JBQ3ZCLGNBQWMsRUFBRSxLQUFLO3dCQUNyQixnQkFBZ0IsRUFBRSxJQUFJO3dCQUN0QixVQUFVLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxjQUFjO3FCQUN6QyxDQUFDO2lCQUNIO2dCQUNELElBQUksQ0FBQyxHQUFHLENBQUMsNkJBQTZCLEVBQUU7b0JBQ3RDLEdBQUcsQ0FBQyw2QkFBNkIsR0FBRzt3QkFDbEMsb0JBQW9CLEVBQUUsSUFBSTtxQkFDM0IsQ0FBQztpQkFDSDtnQkFFRCxpQkFBaUI7Z0JBQ2pCLElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxDQUFDO2dCQUVsQyxVQUFVLENBQUMsSUFBSSxzQkFDVixJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxnQkFBZ0IsQ0FBQyxFQUMzQyxHQUFHLEVBQ04sQ0FBQzthQUNKOzs7Ozs7Ozs7UUFDRCxPQUFPLFVBQVUsQ0FBQztJQUNwQixDQUFDOzs7OztJQUVPLDBDQUFpQjs7OztJQUF6Qjs7WUFDUSxrQkFBa0Isd0JBQ25CLElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLGtCQUFrQixDQUFDLEVBQzdDLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxJQUM3QixXQUFXLEVBQUUsSUFBSSxDQUFDLFVBQVUsRUFDNUIsV0FBVyxFQUFFLElBQUksQ0FBQyxVQUFVLEVBQzVCLGFBQWEsRUFBRSxJQUFJLENBQUMsYUFBYSxFQUNqQyxjQUFjLEVBQUUsSUFBSSxDQUFDLGNBQWMsRUFDbkMsV0FBVyxFQUFFLElBQUksQ0FBQyxXQUFXLEdBQzlCO1FBQ0QsSUFBSSxDQUFDLGtCQUFrQixDQUFDLE9BQU8sRUFBRTtZQUMvQixrQkFBa0IsQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDO1NBQ25DO1FBQ0QsT0FBTyxtQkFBZSxrQkFBa0IsRUFBQSxDQUFDO0lBQzNDLENBQUM7Ozs7O0lBRU8sMkNBQWtCOzs7O0lBQTFCOztZQUNNLFNBQVMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLE9BQU87UUFFdEMsSUFBSSxDQUFDLFNBQVMsRUFBRTtZQUNkLE9BQU87U0FDUjtRQUVELElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxFQUFFO1lBQ3BCLHdEQUF3RDtZQUN4RCxTQUFTLENBQUMsS0FBSztnQkFDYixFQUFFLEdBQUcsU0FBUyxDQUFDLE9BQU8sQ0FBQyxNQUFNO29CQUM3QixFQUFFLEdBQUcsU0FBUyxDQUFDLE9BQU8sQ0FBQyxNQUFNO29CQUM3QixDQUFDLENBQUM7U0FDTDtRQUVELElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBRXhDLFNBQVMsd0JBQ0osSUFBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsZ0JBQWdCLENBQUMsRUFDM0MsU0FBUyxDQUNiLENBQUM7UUFFRixPQUFPLFNBQVMsQ0FBQztJQUNuQixDQUFDOzs7Ozs7SUFFTSwyQ0FBa0I7Ozs7O0lBQXpCLFVBQ0UsTUFBd0IsRUFDeEIsS0FBb0I7UUFFcEIsS0FBSyxDQUFDLGVBQWUsRUFBRSxDQUFDO1FBQ3hCLE1BQU0sQ0FBQyxlQUFlLEdBQUcsQ0FBQyxNQUFNLENBQUMsZUFBZSxDQUFDO1FBRWpELElBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDNUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsZUFBZSxFQUFFLENBQUM7SUFDekMsQ0FBQzs7Ozs7SUFFTSwrQkFBTTs7OztJQUFiLFVBQWMsSUFBWTtRQUN4QixJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUM5QixDQUFDOzs7OztJQUVNLG1DQUFVOzs7O0lBQWpCLFVBQWtCLGVBQXVCO1FBQ3ZDLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLEVBQUU7WUFDeEIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztTQUN6QzthQUFNLElBQUksSUFBSSxDQUFDLE9BQU8sRUFBRTtZQUN2QixJQUFJLENBQUMsT0FBTyxDQUFDLGdCQUFnQixFQUFFLENBQUM7U0FDakM7SUFDSCxDQUFDOzs7OztJQUVNLHNDQUFhOzs7O0lBQXBCLFVBQXFCLEtBQUs7UUFDeEIsSUFBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUNwQyxDQUFDOzs7Ozs7SUFFTyx1Q0FBYzs7Ozs7SUFBdEIsVUFBdUIsRUFBVTs7WUFDekIsUUFBUSxHQUFHLElBQUksTUFBTSxDQUFDLDZCQUE2QixDQUFDO1FBQzFELE9BQU8sUUFBUSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQztJQUMzQixDQUFDOzs7O0lBRUQsb0NBQVc7OztJQUFYLGNBQWUsQ0FBQzs7Z0JBM2FqQixTQUFTLFNBQUM7b0JBQ1QsUUFBUSxFQUFFLFdBQVc7b0JBQ3JCLHlwR0FBcUM7b0JBR3JDLFNBQVMsRUFBRSxDQUFDLG1CQUFtQixFQUFFLG9CQUFvQixDQUFDOztpQkFDdkQ7Ozs7Z0JBakJRLG1CQUFtQjtnQkFGbkIsb0JBQW9COzs7NkJBNkMxQixLQUFLO2dDQUNMLEtBQUs7aUNBQ0wsS0FBSzs4QkFDTCxLQUFLOzZCQUNMLEtBQUs7K0JBQ0wsS0FBSzs2QkFDTCxLQUFLOzhCQUNMLEtBQUs7b0NBQ0wsS0FBSztpQ0FDTCxLQUFLO21DQUNMLEtBQUs7eUNBQ0wsS0FBSzt3Q0FNTCxLQUFLO3FDQUVMLE1BQU0sU0FBQyxvQkFBb0I7bURBSTNCLEtBQUs7NkJBU0wsS0FBSzt1Q0FDTCxLQUFLO2dEQUNMLEtBQUs7OEJBQ0wsS0FBSzsyQkFDTCxLQUFLO3FDQUNMLEtBQUs7d0JBQ0wsS0FBSzs4QkFDTCxLQUFLO3NDQUVMLEtBQUs7cUNBQ0wsS0FBSzs4QkFFTCxNQUFNLFNBQUMsY0FBYzsrQkFHckIsS0FBSzt1Q0FTTCxLQUFLO21DQUVMLE1BQU0sU0FBQyxhQUFhO3dDQUdwQixLQUFLO29DQUVMLE1BQU0sU0FBQyxtQkFBbUI7dUJBRzFCLEtBQUs7NkJBa0JMLEtBQUs7a0NBU0wsS0FBSzt5QkFrQkwsS0FBSzttQ0FLTCxNQUFNOytCQUdOLE1BQU0sU0FBQyxrQkFBa0I7b0NBSXpCLE1BQU0sU0FBQyxvQkFBb0I7a0NBSzNCLE1BQU0sU0FBQyxZQUFZOztJQTRRdEIscUJBQUM7Q0FBQSxBQTVhRCxJQTRhQztTQXJhWSxjQUFjOzs7SUFDekIsb0NBQXlCOztJQUN6QixxQ0FBK0I7O0lBQy9CLDJDQUFrRDs7Ozs7SUFDbEQscUNBQTRDOztJQUM1QyxxQ0FBc0M7O0lBQ3RDLDZDQUFzQzs7SUFDdEMsZ0RBQTBIOztJQUMxSCw2Q0FBMkI7O0lBQzNCLCtDQUE2Qjs7SUFHN0IsaURBQStCOztJQUMvQix1REFBcUM7O0lBQ3JDLDZDQUEyQjs7SUFDM0IscUNBQThEOzs7OztJQUc5RCwrQkFBMkI7O0lBQzNCLG1DQUEyQjs7SUFFM0IsaUNBQWU7O0lBQ2YsdUNBQXFCOztJQUNyQiwwQ0FBd0I7O0lBRXhCLG9DQUEyQzs7SUFDM0MsdUNBQThDOztJQUM5Qyx3Q0FBK0M7O0lBQy9DLHFDQUE0Qzs7SUFDNUMsb0NBQTJDOztJQUMzQyxzQ0FBK0I7O0lBQy9CLG9DQUE2Qjs7SUFDN0IscUNBQThCOztJQUM5QiwyQ0FBb0M7O0lBQ3BDLHdDQUFpQzs7SUFDakMsMENBQW1DOztJQUNuQyxnREFBMEM7O0lBTTFDLCtDQUF5Qzs7SUFFekMsNENBQ2dFOztJQVloRSxvQ0FBMEI7O0lBQzFCLDhDQUF1Qzs7SUFDdkMsdURBQWlEOztJQUNqRCxxQ0FBK0I7O0lBQy9CLGtDQUE0Qjs7SUFDNUIsNENBQWtDOztJQUNsQywrQkFBcUI7O0lBQ3JCLHFDQUE0Qzs7SUFFNUMsNkNBQXFDOztJQUNyQyw0Q0FBNEI7O0lBRTVCLHFDQUN5RDs7SUFXekQsOENBQThCOztJQUU5QiwwQ0FDOEQ7O0lBRTlELCtDQUErQjs7SUFFL0IsMkNBQytEOztJQW1CL0QsOEJBQWE7O0lBMkJiLHFDQUFnQzs7SUFNaEMsMENBQXdFOztJQUd4RSxzQ0FDZ0Y7O0lBR2hGLDJDQUNxRjs7SUFJckYseUNBQzZEOzs7OztJQUszRCxxQ0FBd0M7O0lBQ3hDLGtDQUFxQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IGRpc3RpbmN0VW50aWxDaGFuZ2VkIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xuaW1wb3J0IHsgQ29tcG9uZW50LCBFdmVudEVtaXR0ZXIsIElucHV0LCBPbkRlc3Ryb3ksIE9uSW5pdCwgT3V0cHV0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbmltcG9ydCB7IEdyaWRPcHRpb25zLCBBZ0dyaWRFdmVudCB9IGZyb20gJ2FnLWdyaWQtY29tbXVuaXR5L21haW4nO1xuXG5pbXBvcnQgeyBTdWJqZWN0IH0gZnJvbSAncnhqcyc7XG5pbXBvcnQgKiBhcyBfIGZyb20gJ2xvZGFzaCc7XG5cbmltcG9ydCB7IEN1c3RvbUdyaWRPcHRpb25zLCBGaWx0ZXJhYmxlQ29sdW1uIH0gZnJvbSAnLi90YWJsZS10eXBlcyc7XG5cbmltcG9ydCB7IFRhYmxlRGVmYXVsdHNTZXJ2aWNlIH0gZnJvbSAnLi90YWJsZS1kZWZhdWx0cy5zZXJ2aWNlJztcbmltcG9ydCB7IFJvd0J1dHRvbkV2ZW50IH0gZnJvbSAnLi4vcm93LWJ1dHRvbi5jb21wb25lbnRzJztcbmltcG9ydCB7IENlbGxSZW5kZXJlckZhY3RvcnkgfSBmcm9tICcuL2NlbGwtcmVuZGVyZXIuZmFjdG9yeSc7XG5pbXBvcnQgeyBDdXN0b21Mb2FkaW5nT3ZlcmxheUNvbXBvbmVudCB9IGZyb20gJy4vY3VzdG9tLWxvYWRpbmctb3ZlcmxheS5jb21wb25lbnQnO1xuaW1wb3J0IHsgRGF0ZVBpY2tlckNlbGxFZGl0UmVuZGVyZXJDb21wb25lbnQgfSBmcm9tICcuLi9jZWxsLXJlbmRlcmVycy9kYXRlLXBpY2tlci1jZWxsLWVkaXQtcmVuZGVyZXIvZGF0ZS1waWNrZXItY2VsbC1lZGl0LXJlbmRlcmVyLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBEYXRlVGltZVBpY2tlckNvbXBvbmVudCB9IGZyb20gJy4uLy4uL2NvbXBvbmVudHMvZGF0ZS10aW1lLXBpY2tlci9kYXRlLXRpbWUtcGlja2VyLmNvbXBvbmVudCc7XG5cbmV4cG9ydCB0eXBlIFRhYmxlT3B0aW9ucyA9IEdyaWRPcHRpb25zICYgQ3VzdG9tR3JpZE9wdGlvbnM7XG5cbmV4cG9ydCB0eXBlIEFjdGlvbnMgPSBhbnk7XG5leHBvcnQgdHlwZSBUYWJsZURlZiA9IGFueTtcbmV4cG9ydCB0eXBlIERhdGEgPSBhbnlbXTtcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnYXBwLXRhYmxlJyxcbiAgdGVtcGxhdGVVcmw6ICcuL3RhYmxlLmNvbXBvbmVudC5odG1sJyxcbiAgc3R5bGVVcmxzOiBbJy4vdGFibGUuY29tcG9uZW50LnNjc3MnXSxcblxuICBwcm92aWRlcnM6IFtDZWxsUmVuZGVyZXJGYWN0b3J5LCBUYWJsZURlZmF1bHRzU2VydmljZV1cbn0pXG5leHBvcnQgY2xhc3MgVGFibGVDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIE9uRGVzdHJveSB7XG4gIHB1YmxpYyBjb2x1bW5EZWZzOiBhbnlbXTtcbiAgcHVibGljIGNvbHVtbk5hbWVzOiBhbnlbXSA9IFtdO1xuICBwdWJsaWMgZmlsdGVyYWJsZUNvbHVtbnM6IEZpbHRlcmFibGVDb2x1bW5bXSA9IFtdO1xuICBwcml2YXRlIGZpbHRlclRlcm1zID0gbmV3IFN1YmplY3Q8c3RyaW5nPigpO1xuICBwdWJsaWMgZ3JpZE9wdGlvbnM6IFRhYmxlT3B0aW9ucyA9IHt9O1xuICBwdWJsaWMgdG9vbFBhbmVsVmlzaWJpbGl0eSA9ICdoaWRkZW4nO1xuICBwdWJsaWMgb3ZlcmxheUxvYWRpbmdUZW1wbGF0ZSA9ICc8c3BhbiBjbGFzcz1cImFnLW92ZXJsYXktbG9hZGluZy1jZW50ZXJcIiA+UGxlYXNlIHdhaXQgd2hpbGUgeW91ciByb3dzIGFyZSBsb2FkaW5nPC9zcGFuPic7XG4gIHB1YmxpYyBnZXRDb250ZXh0TWVudUl0ZW1zO1xuICBwdWJsaWMgZ2V0Q29udGV4dE1lbnVBY3Rpb25zO1xuXG4gIC8vIGJlbG93IGlzIGZvciBsb2FkaW5nIG92ZXJsYXlcbiAgcHVibGljIGxvYWRpbmdPdmVybGF5Q29tcG9uZW50O1xuICBwdWJsaWMgbG9hZGluZ092ZXJsYXlDb21wb25lbnRQYXJhbXM7XG4gIHB1YmxpYyBmcmFtZXdvcmtDb21wb25lbnRzO1xuICBwdWJsaWMgbG9hZGluZ1RleHQgPSAnUGxlYXNlIFdhaXQgV2hpbGUgWW91ciBEYXRhIGlzIExvYWRpbmcnO1xuXG5cbiAgcHJpdmF0ZSBfZGF0YTogRGF0YSA9IG51bGw7XG4gIHB1YmxpYyBfdGFibGVEZWY6IFRhYmxlRGVmO1xuXG4gIHB1YmxpYyBncmlkQXBpO1xuICBwdWJsaWMgZ3JpZENvbHVtbkFwaTtcbiAgcHVibGljIGdyaWRFeHBvcnRQcmVmaXg7XG5cbiAgQElucHV0KCkgcm93Q2xhc3NGbj86IChwYXJhbXM6IGFueSkgPT4gYW55O1xuICBASW5wdXQoKSByb3dDbGFzc1J1bGVzPzogKHBhcmFtczogYW55KSA9PiBhbnk7XG4gIEBJbnB1dCgpIGNlbGxDbGFzc1J1bGVzPzogKHBhcmFtczogYW55KSA9PiBhbnk7XG4gIEBJbnB1dCgpIGNlbGxDbGFzc0ZuPzogKHBhcmFtczogYW55KSA9PiBhbnk7XG4gIEBJbnB1dCgpIHJvd1N0eWxlRm4/OiAocGFyYW1zOiBhbnkpID0+IGFueTtcbiAgQElucHV0KCkgZXhwb3J0UHJlZml4Pzogc3RyaW5nO1xuICBASW5wdXQoKSBzb3J0T25Mb2FkPzogc3RyaW5nO1xuICBASW5wdXQoKSB0YWJsZUhlYWRlcj86IHN0cmluZztcbiAgQElucHV0KCkgZXh0cmFUYWJsZVRvb2xCdG4/OiBzdHJpbmc7XG4gIEBJbnB1dCgpIHJvd0RyYWdNYW5hZ2VkPzogc3RyaW5nO1xuICBASW5wdXQoKSBwaW5uZWRUb3BSb3dEYXRhPzogc3RyaW5nO1xuICBASW5wdXQoKSBzdXBwcmVzc0NsaXBib2FyZFBhc3RlPzogYm9vbGVhbjtcblxuICAvLyB1c2luZyBleHBvcnRDZWxsVmFsdWVDaGFuZ2UgPSB0cnVlIGluIGEgdGFibGUgd2lsbCBhbGxvdyBpbmZvcm1hdGlvblxuICAvLyB0byBiZSBleHBvcnRlZCB1cG9uIGV2ZXJ5IGNoYW5nZSB0aHJvdWdoIChleHBvcnQtY2hhbmdlZC12YWx1ZSkuXG4gIC8vIGV4cG9ydGVkIGRhdGEgaXMgaW4gdGhlIGZvcm0gb2YgYW4gb2JqZWN0IGNvbnRhaW5pbmcgdGhlIHJvdyBhbmQgYWxsXG4gIC8vIHByZXNlbnQgZGF0YVxuICBASW5wdXQoKSBleHBvcnRDZWxsVmFsdWVDaGFuZ2U/OiBib29sZWFuO1xuICAvKiB0c2xpbnQ6ZGlzYWJsZS1uZXh0LWxpbmU6bm8tb3V0cHV0LXJlbmFtZSAqL1xuICBAT3V0cHV0KCdleHBvcnRDaGFuZ2VkVmFsdWUnKVxuICBleHBvcnRDaGFuZ2VkVmFsdWU6IEV2ZW50RW1pdHRlcjxhbnk+ID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XG5cblxuICBASW5wdXQoKVxuICBzZXQgZ2V0Q29udGV4dE1lbnVJdGVtc0Zyb21Db21wb25lbnQodGhlQ29udGV4dE1lbnUpIHtcbiAgICBpZiAoIXRoZUNvbnRleHRNZW51KSB7XG4gICAgICByZXR1cm47XG4gICAgfVxuICAgIHRoaXMuZ2V0Q29udGV4dE1lbnVJdGVtcyA9IHRoZUNvbnRleHRNZW51O1xuICB9XG5cbiAgLy8gZm9yIHRyZWUgZm9ybWF0IHRhYmxlc1xuICBASW5wdXQoKSBjb21wb25lbnRzPzogYW55O1xuICBASW5wdXQoKSBncm91cERlZmF1bHRFeHBhbmRlZD86IG51bWJlcjtcbiAgQElucHV0KCkgcmVtZW1iZXJHcm91cFN0YXRlV2hlbk5ld0RhdGE/OiBib29sZWFuO1xuICBASW5wdXQoKSBhbmltYXRlUm93cz86IGJvb2xlYW47XG4gIEBJbnB1dCgpIHRyZWVEYXRhPzogYm9vbGVhbjtcbiAgQElucHV0KCkgYXV0b0dyb3VwQ29sdW1uRGVmPzogYW55O1xuICBASW5wdXQoKSBpY29ucz86IGFueTtcbiAgQElucHV0KCkgZ2V0RGF0YVBhdGg/OiAocGFyYW1zOiBhbnkpID0+IGFueTtcblxuICBASW5wdXQoKSBzdXBwcmVzc0NvbnRleHRNZW51ID0gZmFsc2U7XG4gIEBJbnB1dCgpIGV4cG9ydFNlYXJjaFBhcmFtcztcbiAgLyogdHNsaW50OmRpc2FibGUtbmV4dC1saW5lOm5vLW91dHB1dC1yZW5hbWUgKi9cbiAgQE91dHB1dCgnc2VhcmNoLXRlcm1zJylcbiAgc2VhcmNoVGVybXM6IEV2ZW50RW1pdHRlcjxhbnk+ID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XG5cbiAgQElucHV0KClcbiAgc2V0IHVwZGF0ZUZpbHRlcihzZWFyY2gpIHtcbiAgICBpZiAoc2VhcmNoICYmIE9iamVjdC5rZXlzKHNlYXJjaCkubGVuZ3RoKSB7XG4gICAgICB0aGlzLmdyaWRBcGkuc2V0RmlsdGVyTW9kZWwoc2VhcmNoKTtcbiAgICB9IGVsc2UgaWYgKHNlYXJjaCA9PT0ge30pIHtcbiAgICAgIHRoaXMuZ3JpZEFwaS5zZXRGaWx0ZXJNb2RlbChudWxsKTtcbiAgICB9XG4gIH1cblxuICBASW5wdXQoKSBleHBvcnRSb3dXaGVuQ2xpY2tlZDtcbiAgLyogdHNsaW50OmRpc2FibGUtbmV4dC1saW5lOm5vLW91dHB1dC1yZW5hbWUgKi9cbiAgQE91dHB1dCgnY2xpY2tlZC1yb3cnKVxuICBleHBvcnRDbGlja2VkUm93OiBFdmVudEVtaXR0ZXI8YW55PiA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xuXG4gIEBJbnB1dCgpIGV4cG9ydENlbGxXaGVuQ2xpY2tlZDtcbiAgLyogdHNsaW50OmRpc2FibGUtbmV4dC1saW5lOm5vLW91dHB1dC1yZW5hbWUgKi9cbiAgQE91dHB1dCgnZXhwb3J0Q2xpY2tlZENlbGwnKVxuICBleHBvcnRDbGlja2VkQ2VsbDogRXZlbnRFbWl0dGVyPGFueT4gPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcblxuICBASW5wdXQoKVxuICBzZXQgZGF0YSh0aGVEYXRhOiBEYXRhKSB7XG4gICAgaWYgKCF0aGVEYXRhICYmIHRoaXMuZ3JpZEFwaSkge1xuICAgICAgdGhpcy5ncmlkQXBpLnNob3dMb2FkaW5nT3ZlcmxheSgpO1xuICAgIH1cbiAgICBpZiAoIXRoZURhdGEpIHtcbiAgICAgIHJldHVybjtcbiAgICB9XG4gICAgdGhpcy5fZGF0YSA9IHRoZURhdGE7XG5cbiAgICBpZiAoIXRoaXMuZ3JpZEFwaSkge1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIHRoaXMuZ3JpZEFwaS5zZXRSb3dEYXRhKHRoaXMuX2RhdGEpO1xuICB9XG5cbiAgZHJhdyA9IGZhbHNlO1xuICBASW5wdXQoKVxuICBzZXQgcmVkcmF3Um93cyhkcmF3KSB7XG4gICAgdGhpcy5kcmF3ID0gZHJhdztcbiAgICBpZiAodGhpcy5kcmF3ICYmIHRoaXMuZ3JpZEFwaSkge1xuICAgICAgdGhpcy5ncmlkQXBpLnJlZHJhd1Jvd3MoKTtcbiAgICAgIHRoaXMuZHJhdyA9IGZhbHNlO1xuICAgIH1cbiAgfVxuXG4gIEBJbnB1dCgpXG4gIHNldCB0YWJsZURlZmluaXRpb24odEQ6IFRhYmxlRGVmKSB7XG4gICAgaWYgKCF0RCkge1xuICAgICAgcmV0dXJuO1xuICAgIH1cbiAgICB0aGlzLl90YWJsZURlZiA9IF8uY2xvbmVEZWVwKHREKTtcblxuICAgIHRoaXMuZ3JpZE9wdGlvbnMgPSB0aGlzLmNyZWF0ZUdyaWRPcHRpb25zKCk7XG5cbiAgICB0aGlzLmNvbHVtbkRlZnMgPSB0aGlzLmNyZWF0ZUNvbHVtbkRlZnMoKTtcblxuICAgIGNvbnN0IGFjdGlvbnMgPSB0aGlzLm1hbmFnZVRhYmxlQWN0aW9ucygpO1xuICAgIGlmIChhY3Rpb25zKSB7XG4gICAgICB0aGlzLmNvbHVtbkRlZnMucHVzaChhY3Rpb25zKTtcbiAgICB9XG4gIH1cblxuICBwdWJsaWMgdGFibGVIZWlnaHQ6IFN0cmluZyA9ICcnO1xuICBASW5wdXQoKVxuICBzZXQgaGVpZ2h0KHZhbHVlKSB7XG4gICAgdGhpcy50YWJsZUhlaWdodCA9IHZhbHVlO1xuICB9XG5cbiAgQE91dHB1dCgpIGV4dHJhVG9vbEJ0blVzZWQ6IEV2ZW50RW1pdHRlcjxhbnk+ID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XG5cbiAgLyogdHNsaW50OmRpc2FibGUtbmV4dC1saW5lOm5vLW91dHB1dC1yZW5hbWUgKi9cbiAgQE91dHB1dCgncm93LWJ1dHRvbi1jbGljaycpXG4gIGJ1dHRvbk91dHB1dDogRXZlbnRFbWl0dGVyPFJvd0J1dHRvbkV2ZW50PiA9IG5ldyBFdmVudEVtaXR0ZXI8Um93QnV0dG9uRXZlbnQ+KCk7XG5cbiAgLyogdHNsaW50OmRpc2FibGUtbmV4dC1saW5lOm5vLW91dHB1dC1yZW5hbWUgKi9cbiAgQE91dHB1dCgnY29udGV4dC1tZW51LWV2ZW50JylcbiAgY29udGV4dE1lbnVPdXRwdXQ6IEV2ZW50RW1pdHRlcjxSb3dCdXR0b25FdmVudD4gPSBuZXcgRXZlbnRFbWl0dGVyPFJvd0J1dHRvbkV2ZW50PigpO1xuXG4gIC8vLyB1c2VkIGZvciB0b29scyBpbiB0aGUgY2hhdCBjZWxsLCBlbWl0cyB0aGVcbiAgLyogdHNsaW50OmRpc2FibGUtbmV4dC1saW5lOm5vLW91dHB1dC1yZW5hbWUgKi9cbiAgQE91dHB1dCgndG9vbC1jbGljaycpXG4gIGV4cG9ydFRvb2xDbGljazogRXZlbnRFbWl0dGVyPGFueT4gPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcblxuICAvLyBAVmlld0NoaWxkKFRhYmxlVG9vbHNDb21wb25lbnQpIHRhYmxlVG9vbHM6IFRhYmxlVG9vbHNDb21wb25lbnQ7XG5cbiAgY29uc3RydWN0b3IoXG4gICAgcHJpdmF0ZSBjZWxsRmFjdG9yeTogQ2VsbFJlbmRlcmVyRmFjdG9yeSxcbiAgICBwdWJsaWMgZGVmYXVsdHM6IFRhYmxlRGVmYXVsdHNTZXJ2aWNlXG4gICkge1xuICB9XG5cbiAgbmdPbkluaXQoKSB7XG4gICAgdGhpcy5nZXRDb250ZXh0TWVudUFjdGlvbnMgPSAocGFyYW1zLCBldmVudFR5cGUpID0+IHRoaXMuY29udGV4dE1lbnVFdmVudChwYXJhbXMsIGV2ZW50VHlwZSk7XG4gICAgdGhpcy5mcmFtZXdvcmtDb21wb25lbnRzID0ge1xuICAgICAgY3VzdG9tTG9hZGluZ092ZXJsYXlDb21wb25lbnQ6IEN1c3RvbUxvYWRpbmdPdmVybGF5Q29tcG9uZW50LFxuICAgICAgZGF0ZVBpY2tlcjogRGF0ZVBpY2tlckNlbGxFZGl0UmVuZGVyZXJDb21wb25lbnQsXG4gICAgICBkYXRlVGltZVBpY2tlcjogRGF0ZVRpbWVQaWNrZXJDb21wb25lbnRcbiAgICB9O1xuICAgIHRoaXMubG9hZGluZ092ZXJsYXlDb21wb25lbnQgPSAnY3VzdG9tTG9hZGluZ092ZXJsYXlDb21wb25lbnQnO1xuICAgIHRoaXMubG9hZGluZ092ZXJsYXlDb21wb25lbnRQYXJhbXMgPSB7bG9hZGluZ01lc3NhZ2U6IHRoaXMubG9hZGluZ1RleHR9O1xuICAgIC8vIHJldHVybnMgY3VzdG9tIGNvbnRleHQgbWVudVxuICAgIGlmICghdGhpcy5nZXRDb250ZXh0TWVudUl0ZW1zKSB7XG4gICAgICB0aGlzLnN1cHByZXNzQ29udGV4dE1lbnUgPSB0cnVlO1xuICAgICAgLy8gVGhlIGJlbG93IGxpbmUgY2FuIGJlIGFkZGVkIGJhY2sgaW4gZm9yIGEgZGVmYXVsdCBjb250ZXh0IG1lbnUgaW5jbHVkaW5nLCBjb3B5IGNlbGwvbGluZS93aXRoICYgd2l0aG91dCBoZWFkZXJzICYgZXhwb3J0XG4gICAgICAvLyB0aGlzLmdldENvbnRleHRNZW51SXRlbXMgPSB0aGlzLmRlZmF1bHRzLmdldERlZmF1bHRzKCdjb250ZXh0TWVudScsIHRoaXMuZ2V0Q29udGV4dE1lbnVJdGVtcyk7XG4gICAgfVxuICAgIHRoaXMuZmlsdGVyVGVybXMucGlwZShkaXN0aW5jdFVudGlsQ2hhbmdlZCgpKS5zdWJzY3JpYmUoZmlsdGVyVGVybSA9PiB7XG4gICAgICAvLyBBbHdheXMgc2V0IHRoZSBmaWx0ZXIgc3RyaW5nIGlmIGl0IGNoYW5nZXNcblxuICAgICAgaWYgKCF0aGlzLmdyaWRBcGkpIHtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuXG4gICAgICB0aGlzLmdyaWRBcGkuc2V0Um93RGF0YSh0aGlzLl9kYXRhKTtcbiAgICAgIHRoaXMuZ3JpZEFwaS5vbkZpbHRlckNoYW5nZWQoKTtcbiAgICB9KTtcbiAgfVxuXG4gIG9uR3JpZFJlYWR5KGV2ZW50KSB7XG5cbiAgICAvLyB0aGlzLmF1dG9TaXplQWxsKCk7XG4gIH1cblxuICAvLyAqKiEhIFVubmVjZXNzYXJ5LCBidXQga2VwdCBmb3IgcG9zdGVyaXR5XG4gIC8vIGF1dG9TaXplQWxsKCkge1xuICAvLyAgIGNvbnN0IGFsbENvbHVtbklkcyA9IFtdO1xuICAvLyAgIHRoaXMuZ3JpZENvbHVtbkFwaS5nZXRBbGxDb2x1bW5zKCkuZm9yRWFjaChmdW5jdGlvbiAoY29sdW1uKSB7XG4gIC8vICAgICBjb25zb2xlLmxvZygnY29sdW1uJywgY29sdW1uKVxuICAvLyAgICAgYWxsQ29sdW1uSWRzLnB1c2goY29sdW1uLmNvbElkKTtcbiAgLy8gICB9KTtcbiAgLy8gICB0aGlzLmdyaWRDb2x1bW5BcGkuYXV0b1NpemVDb2x1bW5zKGFsbENvbHVtbklkcyk7XG4gIC8vIH1cblxuICBhcGlSZWFkeShldmVudDogQWdHcmlkRXZlbnQpIHtcbiAgICB0aGlzLmdyaWRBcGkgPSBldmVudC5hcGk7XG4gICAgdGhpcy5ncmlkQ29sdW1uQXBpID0gZXZlbnQuY29sdW1uQXBpO1xuICAgIHRoaXMuZ3JpZEV4cG9ydFByZWZpeCA9IHRoaXMuZ3JpZE9wdGlvbnMuZXhwb3J0UHJlZml4O1xuXG4gICAgLy8gdGhpcy5ncmlkQXBpLm9uRmlsdGVyQ2hhbmdlZDtcbiAgICBpZiAoIXRoaXMuX2RhdGEpIHtcbiAgICAgIHRoaXMuZ3JpZEFwaS5zaG93TG9hZGluZ092ZXJsYXkoKTtcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5ncmlkQXBpLnNldFJvd0RhdGEodGhpcy5fZGF0YSk7XG4gICAgfVxuICAgIGlmICh0aGlzLnNvcnRPbkxvYWQpIHtcbiAgICAgIGNvbnN0IHNvcnRNb2RlbCA9IFt7Y29sSWQ6IHRoaXMuc29ydE9uTG9hZCwgc29ydDogJ2Rlc2MnfV07XG4gICAgICB0aGlzLmdyaWRBcGkuc2V0U29ydE1vZGVsKHNvcnRNb2RlbCk7XG4gICAgfVxuICB9XG5cbiAgcHVibGljIG9uRmlsdGVyVXBkYXRlKCkge1xuICAgIGlmICghdGhpcy5leHBvcnRTZWFyY2hQYXJhbXMpIHtcbiAgICAgIHJldHVybjtcbiAgICB9IGVsc2Uge1xuICAgICAgY29uc3QgZmlsdGVyZWRTZWFyY2hSZXN1bHRzID0gW107XG4gICAgICB0aGlzLmdyaWRBcGkuZm9yRWFjaE5vZGVBZnRlckZpbHRlcihyb3dOb2RlID0+IHtcbiAgICAgICAgZmlsdGVyZWRTZWFyY2hSZXN1bHRzW2ZpbHRlcmVkU2VhcmNoUmVzdWx0cy5sZW5ndGhdID1cbiAgICAgICAgICByb3dOb2RlLmRhdGE7XG4gICAgICB9KTtcbiAgICAgIGNvbnN0IGZpbHRlclRlcm1zID0gdGhpcy5ncmlkQXBpLmdldEZpbHRlck1vZGVsKCk7XG4gICAgICBjb25zdCByZXN1bHRzID0ge1xuICAgICAgICBzZWFyY2hUZXJtczogZmlsdGVyVGVybXMsXG4gICAgICAgIHNlYXJjaFJlc3VsdHM6IGZpbHRlcmVkU2VhcmNoUmVzdWx0c1xuICAgICAgfTtcbiAgICAgIHRoaXMuc2VhcmNoVGVybXMuZW1pdChyZXN1bHRzKTtcbiAgICB9XG4gIH1cblxuICBwcml2YXRlIGNvbnRleHRNZW51RXZlbnQocGFyYW1zLCBldmVudFR5cGUpIHtcbiAgICBjb25zdCBldmVudERhdGE6IGFueSA9IHt9O1xuICAgIGV2ZW50RGF0YVtldmVudFR5cGVdID0gXy5jbG9uZURlZXAocGFyYW1zLm5vZGUuZGF0YSk7XG4gICAgdGhpcy5jb250ZXh0TWVudU91dHB1dC5lbWl0KGV2ZW50RGF0YSk7XG4gIH1cblxuICBwdWJsaWMgb25Sb3dDbGlja2VkKHJvdykge1xuICAgIHN3aXRjaCAodHJ1ZSkge1xuICAgICAgLyogdGhpcyBkZXRlcm1pbmVzIGlmIGEgdG9vbCB3YXMgY2xpY2tlZCwgdnMgYSByb3cgb3IgY2VsbCBjbGljayovXG4gICAgICBjYXNlIHRoaXMudG9vbFdhc0NsaWNrZWQocm93LmV2ZW50LnNyY0VsZW1lbnQuaWQpOlxuICAgICAgICB0aGlzLmV4cG9ydFRvb2xDbGljay5lbWl0KHt0b29sSWQ6IHJvdy5ldmVudC5zcmNFbGVtZW50LmlkLCB3ZWxsYm9yZTogcm93LmRhdGF9KTtcbiAgICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIC8vIHRvb2wgYnV0dG9uIHdhcyBub3QgY2xpY2tlZCwgcHJvY2VlZCBhcyBub3JtYWxcbiAgICBpZiAoIXRoaXMuZXhwb3J0Um93V2hlbkNsaWNrZWQpIHtcbiAgICAgIHJldHVybjtcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5leHBvcnRDbGlja2VkUm93LmVtaXQocm93LmRhdGEpO1xuICAgIH1cbiAgfVxuXG4gIHB1YmxpYyBvbkNlbGxDbGlja2VkKGNlbGwpIHtcbiAgICBpZiAoIWNlbGwpIHtcbiAgICAgIHJldHVybjtcbiAgICB9XG4gICAgdGhpcy5leHBvcnRDbGlja2VkQ2VsbC5lbWl0KGNlbGwpO1xuICB9XG5cbiAgcHVibGljIG9uQ2VsbFZhbHVlQ2hhbmdlZChldmVudCkge1xuICAgIGlmICghdGhpcy5leHBvcnRDZWxsVmFsdWVDaGFuZ2UpIHtcbiAgICAgIHJldHVybjtcbiAgICB9XG4gICAgY29uc3QgcmVzdWx0cyA9IHtkYXRhOiBldmVudFsnZGF0YSddfTtcbiAgICB0aGlzLmV4cG9ydENoYW5nZWRWYWx1ZS5lbWl0KHJlc3VsdHMpO1xuICB9XG5cbiAgcHJpdmF0ZSBjcmVhdGVDb2x1bW5EZWZzKCk6IGFueSB7XG4gICAgY29uc3QgdGFibGUgPSB0aGlzLl90YWJsZURlZjtcblxuICAgIGNvbnN0IGNvbHVtbkRlZnMgPSBbXTtcbiAgICBmb3IgKGNvbnN0IGNvbCBvZiB0YWJsZS5jb2x1bW5EZWZzKSB7XG4gICAgICAvLyBTZXQgdXAgZnV6enkgZmlsdGVyIGNvbHVtbnNcbiAgICAgIGlmIChjb2wuaGVhZGVyTmFtZSkge1xuICAgICAgICB0aGlzLmZpbHRlcmFibGVDb2x1bW5zLnB1c2goPEZpbHRlcmFibGVDb2x1bW4+IHtcbiAgICAgICAgICBuYW1lOiBjb2wuaGVhZGVyTmFtZSxcbiAgICAgICAgICBmaWVsZDogY29sLmZpZWxkLFxuICAgICAgICAgIGluY2x1ZGVJblNlYXJjaDogY29sLmluY2x1ZGVJblNlYXJjaFxuICAgICAgICB9KTtcbiAgICAgIH1cblxuICAgICAgLy8gRm9yIGV4cG9ydGluZ1xuICAgICAgaWYgKGNvbC5maWVsZCkge1xuICAgICAgICB0aGlzLmNvbHVtbk5hbWVzLnB1c2goY29sLmZpZWxkKTtcbiAgICAgIH1cblxuICAgICAgLy8gVE9ETzogY2hlY2tib3hTZWxlY3Rpb24gZmlsdGVyc1xuICAgICAgaWYgKCFjb2wuZmlsdGVyICYmICFjb2wuY2hlY2tib3hTZWxlY3Rpb24pIHtcbiAgICAgICAgY29sLmZpbHRlciA9ICdhZ1RleHRDb2x1bW5GaWx0ZXInO1xuICAgICAgfVxuICAgICAgaWYgKGNvbC5jaGVja2JveFNlbGVjdGlvbiA9PT0gdHJ1ZSkge1xuICAgICAgICBjb2wuc3VwcHJlc3NDb3B5ID0gdHJ1ZTtcbiAgICAgIH1cblxuICAgICAgaWYgKGNvbC5maWx0ZXIgPT09ICdzZXQnKSB7XG4gICAgICAgIGNvbC5maWx0ZXJQYXJhbXMgPSB7XG4gICAgICAgICAgY2VsbFJlbmRlcmVyOiB0aGlzLmRlZmF1bHRzLnVwZGF0ZU51bGxWYWx1ZXNcbiAgICAgICAgfTtcbiAgICAgIH1cbiAgICAgIGlmIChjb2wuZmlsdGVyID09PSAnZGF0ZScpIHtcbiAgICAgICAgY29sLmZpbHRlclBhcmFtcyA9IHtcbiAgICAgICAgICBjbGVhckJ1dHRvbjogdHJ1ZSxcbiAgICAgICAgICBzdXBwcmVzc0FuZE9yQ29uZGl0aW9uOiB0cnVlLFxuICAgICAgICAgIG5ld1Jvd3NBY3Rpb246ICdrZWVwJyxcbiAgICAgICAgICBmaWx0ZXJPcHRpb25zOiBbXG4gICAgICAgICAgICAnZXF1YWxzJyxcbiAgICAgICAgICAgICdub3RFcXVhbCcsXG4gICAgICAgICAgICAnbGVzc1RoYW5PckVxdWFsJyxcbiAgICAgICAgICAgICdncmVhdGVyVGhhbicsXG4gICAgICAgICAgICAnZ3JlYXRlclRoYW5PckVxdWFsJyxcbiAgICAgICAgICAgICdpblJhbmdlJ1xuICAgICAgICAgIF0sXG4gICAgICAgICAgZGVmYXVsdE9wdGlvbjogJ2VxdWFscycsXG4gICAgICAgICAgYnJvd3NlckRhdGVQaWNrZXI6IHRydWUsXG4gICAgICAgICAgbnVsbENvbXBhcmF0b3I6IGZhbHNlLFxuICAgICAgICAgIGluUmFuZ2VJbmNsdXNpdmU6IHRydWUsXG4gICAgICAgICAgY29tcGFyYXRvcjogdGhpcy5kZWZhdWx0cy5kYXRlQ29tcGFyYXRvclxuICAgICAgICB9O1xuICAgICAgfVxuICAgICAgaWYgKCFjb2wuZmxvYXRpbmdGaWx0ZXJDb21wb25lbnRQYXJhbXMpIHtcbiAgICAgICAgY29sLmZsb2F0aW5nRmlsdGVyQ29tcG9uZW50UGFyYW1zID0ge1xuICAgICAgICAgIHN1cHByZXNzRmlsdGVyQnV0dG9uOiB0cnVlXG4gICAgICAgIH07XG4gICAgICB9XG5cbiAgICAgIC8vIENlbGwgUmVuZGVyZXJzXG4gICAgICB0aGlzLmNlbGxGYWN0b3J5LnNldFJlbmRlcmVyKGNvbCk7XG5cbiAgICAgIGNvbHVtbkRlZnMucHVzaCh7XG4gICAgICAgIC4uLnRoaXMuZGVmYXVsdHMuZ2V0RGVmYXVsdHMoJ2NvbERlZkRlZmF1bHRzJyksXG4gICAgICAgIC4uLmNvbFxuICAgICAgfSk7XG4gICAgfVxuICAgIHJldHVybiBjb2x1bW5EZWZzO1xuICB9XG5cbiAgcHJpdmF0ZSBjcmVhdGVHcmlkT3B0aW9ucygpOiBUYWJsZU9wdGlvbnMge1xuICAgIGNvbnN0IGRlZmF1bHRHcmlkT3B0aW9ucyA9IHtcbiAgICAgIC4uLnRoaXMuZGVmYXVsdHMuZ2V0RGVmYXVsdHMoJ3RhYmxlRGVmRGVmYXVsdHMnKSxcbiAgICAgIC4uLnRoaXMuX3RhYmxlRGVmLmdyaWRPcHRpb25zLFxuICAgICAgZ2V0Um93U3R5bGU6IHRoaXMucm93U3R5bGVGbixcbiAgICAgIGdldFJvd0NsYXNzOiB0aGlzLnJvd0NsYXNzRm4sXG4gICAgICByb3dDbGFzc1J1bGVzOiB0aGlzLnJvd0NsYXNzUnVsZXMsXG4gICAgICBjZWxsQ2xhc3NSdWxlczogdGhpcy5jZWxsQ2xhc3NSdWxlcyxcbiAgICAgIGNlbGxDbGFzc0ZuOiB0aGlzLmNlbGxDbGFzc0ZuXG4gICAgfTtcbiAgICBpZiAoIWRlZmF1bHRHcmlkT3B0aW9ucy5jb250ZXh0KSB7XG4gICAgICBkZWZhdWx0R3JpZE9wdGlvbnMuY29udGV4dCA9IHRoaXM7XG4gICAgfVxuICAgIHJldHVybiA8VGFibGVPcHRpb25zPiBkZWZhdWx0R3JpZE9wdGlvbnM7XG4gIH1cblxuICBwcml2YXRlIG1hbmFnZVRhYmxlQWN0aW9ucygpOiBhbnkge1xuICAgIGxldCBhY3Rpb25Db2wgPSB0aGlzLl90YWJsZURlZi5hY3Rpb25zO1xuXG4gICAgaWYgKCFhY3Rpb25Db2wpIHtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICBpZiAoIWFjdGlvbkNvbC53aWR0aCkge1xuICAgICAgLy8gYnV0dG9ucyBhcmUgNDVweCB3aWRlLCB3aXRoIDEwcHggb2YgcGFkZGluZyBpbmJldHdlZW5cbiAgICAgIGFjdGlvbkNvbC53aWR0aCA9XG4gICAgICAgIDQ1ICogYWN0aW9uQ29sLmJ1dHRvbnMubGVuZ3RoICtcbiAgICAgICAgMTAgKiBhY3Rpb25Db2wuYnV0dG9ucy5sZW5ndGggLVxuICAgICAgICAxO1xuICAgIH1cblxuICAgIHRoaXMuY2VsbEZhY3Rvcnkuc2V0UmVuZGVyZXIoYWN0aW9uQ29sKTtcblxuICAgIGFjdGlvbkNvbCA9IHtcbiAgICAgIC4uLnRoaXMuZGVmYXVsdHMuZ2V0RGVmYXVsdHMoJ2FjdGlvbkRlZmF1bHRzJyksXG4gICAgICAuLi5hY3Rpb25Db2xcbiAgICB9O1xuXG4gICAgcmV0dXJuIGFjdGlvbkNvbDtcbiAgfVxuXG4gIHB1YmxpYyB1cGRhdGVDb2xzVG9GaWx0ZXIoXG4gICAgY29sdW1uOiBGaWx0ZXJhYmxlQ29sdW1uLFxuICAgIGV2ZW50OiBLZXlib2FyZEV2ZW50XG4gICk6IHZvaWQge1xuICAgIGV2ZW50LnN0b3BQcm9wYWdhdGlvbigpO1xuICAgIGNvbHVtbi5pbmNsdWRlSW5TZWFyY2ggPSAhY29sdW1uLmluY2x1ZGVJblNlYXJjaDtcblxuICAgIHRoaXMuZ3JpZE9wdGlvbnMuYXBpLnNldFJvd0RhdGEodGhpcy5fZGF0YSk7XG4gICAgdGhpcy5ncmlkT3B0aW9ucy5hcGkub25GaWx0ZXJDaGFuZ2VkKCk7XG4gIH1cblxuICBwdWJsaWMgZmlsdGVyKHRlcm06IHN0cmluZyk6IHZvaWQge1xuICAgIHRoaXMuZmlsdGVyVGVybXMubmV4dCh0ZXJtKTtcbiAgfVxuXG4gIHB1YmxpYyByZXNpemVHcmlkKGNhbGxlZEZyb21XaGVyZTogc3RyaW5nKTogdm9pZCB7XG4gICAgaWYgKHRoaXMuZ3JpZE9wdGlvbnMuYXBpKSB7XG4gICAgICB0aGlzLmdyaWRPcHRpb25zLmFwaS5zaXplQ29sdW1uc1RvRml0KCk7XG4gICAgfSBlbHNlIGlmICh0aGlzLmdyaWRBcGkpIHtcbiAgICAgIHRoaXMuZ3JpZEFwaS5zaXplQ29sdW1uc1RvRml0KCk7XG4gICAgfVxuICB9XG5cbiAgcHVibGljIGV4dHJhVG9vbFVzZWQoZXZlbnQpIHtcbiAgICB0aGlzLmV4dHJhVG9vbEJ0blVzZWQuZW1pdChldmVudCk7XG4gIH1cblxuICBwcml2YXRlIHRvb2xXYXNDbGlja2VkKGlkOiBzdHJpbmcpOiBib29sZWFuIHtcbiAgICBjb25zdCBjaGF0UGF0dCA9IG5ldyBSZWdFeHAoJygoY2xlYXJDaGF0XylbQS16IDAtOV17OSx9KScpO1xuICAgIHJldHVybiBjaGF0UGF0dC50ZXN0KGlkKTtcbiAgfVxuXG4gIG5nT25EZXN0cm95KCkge31cbn1cbiJdfQ==