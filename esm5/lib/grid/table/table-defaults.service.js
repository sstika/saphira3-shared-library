/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import * as _ from 'lodash';
import * as moment_ from 'moment';
/** @type {?} */
var moment = moment_;
var TableDefaultsService = /** @class */ (function () {
    function TableDefaultsService() {
        this.tableFilterIcon = '<div class="column-filter-header-parent"> <span class="ag-icon ag-filter-icon"></span> <span>Refine Search</span> </div>';
        this.tableDefDefaults = {
            enableRangeSelection: true,
            enableColResize: true,
            enableSorting: true,
            enableFilter: true,
            floatingFilter: true,
            rowSelection: 'multiple',
            rowHeight: 45,
            suppressPropertyNamesCheck: true,
            headerHeight: 32,
            icons: {
                filter: this.tableFilterIcon
            },
            postProcessPopup: this.repositionFilterMenu
        };
        this.colDefDefaults = {
            filter: 'agTextColumnFilter',
            suppressFilter: false,
            suppressMenu: true,
            suppressMovable: true,
            suppressResize: true,
            lockPosition: true
        };
        this.actionDefaults = {
            cellClass: 'table-btn',
            pinned: 'right',
            suppressToolPanel: true,
            suppressMenu: true
        };
    }
    /**
     * @param {?} defaultType
     * @param {?=} contextMenuObj
     * @return {?}
     */
    TableDefaultsService.prototype.getDefaults = /**
     * @param {?} defaultType
     * @param {?=} contextMenuObj
     * @return {?}
     */
    function (defaultType, contextMenuObj) {
        if (defaultType === 'tableDefDefaults') {
            return _.cloneDeep(this.tableDefDefaults);
        }
        if (defaultType === 'colDefDefaults') {
            return _.cloneDeep(this.colDefDefaults);
        }
        if (defaultType === 'actionDefaults') {
            return _.cloneDeep(this.actionDefaults);
        }
        if (defaultType === 'contextMenu' && !contextMenuObj) {
            return this.getContextMenuItems;
        }
        if (defaultType === 'contextMenu' && contextMenuObj) {
            return contextMenuObj;
        }
    };
    /**
     * @param {?} filterDate
     * @param {?} cellValue
     * @return {?}
     */
    TableDefaultsService.prototype.dateComparator = /**
     * @param {?} filterDate
     * @param {?} cellValue
     * @return {?}
     */
    function (filterDate, cellValue) {
        // Cell value date is ISO 8601 (ish)
        // Remove time information from cell value as filter input is a date
        /** @type {?} */
        var cellDate = moment(cellValue).hour(0).minute(0).second(0).millisecond(0);
        /** @type {?} */
        var filter = moment(filterDate);
        // Now that both parameters are Date objects, we can compare
        if (cellDate.isBefore(filter)) {
            return -1;
        }
        else if (cellDate.isAfter(filter)) {
            return 1;
        }
        else {
            return 0;
        }
    };
    /**
     * @param {?} params
     * @return {?}
     */
    TableDefaultsService.prototype.repositionFilterMenu = /**
     * @param {?} params
     * @return {?}
     */
    function (params) {
        if (params.type !== 'columnMenu') {
            return;
        }
        /** @type {?} */
        var columnId = params.column.getId();
        /** @type {?} */
        var internalColDefs = params.column.gridOptionsWrapper.gridOptions.columnDefs;
        /** @type {?} */
        var lastColFilterType = internalColDefs[internalColDefs.length - 2].filter;
        if (lastColFilterType === 'set' || lastColFilterType === 'date') {
            /** @type {?} */
            var ePopup = params.ePopup;
            /** @type {?} */
            var oldLeftStr = ePopup.style.left;
            // remove 'px' from the string (ag-Grid uses px positioning)
            oldLeftStr = oldLeftStr.substring(0, oldLeftStr.indexOf('px'));
            /** @type {?} */
            var oldLeft = parseInt(oldLeftStr, 10);
            /** @type {?} */
            var newLeft = void 0;
            // HACK -y as shit
            columnId === 'manifestation' || columnId === 'status'
                ? (newLeft = oldLeft - 16)
                : (newLeft = oldLeft - 50);
            ePopup.style.left = newLeft + 'px';
        }
    };
    /**
     * @param {?} params
     * @return {?}
     */
    TableDefaultsService.prototype.updateNullValues = /**
     * @param {?} params
     * @return {?}
     */
    function (params) {
        return params.value === null || params.value === ''
            ? '(BLANK)'
            : params.value;
    };
    /**
     * @param {?} params
     * @return {?}
     */
    TableDefaultsService.prototype.getContextMenuItems = /**
     * @param {?} params
     * @return {?}
     */
    function (params) {
        /** @type {?} */
        var contextMenuResults = [
            {
                name: 'Copy Single Cell',
                action: (/**
                 * @return {?}
                 */
                function () {
                    params.api.copySelectedRangeToClipboard();
                }),
                icon: '<i class="material-icons">content_copy</i>'
            },
            {
                name: 'Copy Selected Row(s)',
                action: (/**
                 * @return {?}
                 */
                function () {
                    params.context.tableTools.copySelectedRows(false);
                }),
                icon: '<i class="material-icons">content_copy</i>'
            },
            {
                name: 'Copy Selected Row(s) With Headers',
                action: (/**
                 * @return {?}
                 */
                function () {
                    params.context.tableTools.copySelectedRows(true);
                }),
                icon: '<i class="material-icons">content_copy</i>'
            },
            'separator',
            {
                name: params.api.isToolPanelShowing()
                    ? 'Hide Tool Panel'
                    : 'Show Tool Panel',
                action: (/**
                 * @return {?}
                 */
                function () {
                    params.context.tableTools.enhanceTableSearch();
                }),
                icon: '<i class="material-icons">build</i>'
            },
            {
                name: 'Export Table',
                icon: '<i class="material-icons">file_download</i>',
                subMenu: [
                    {
                        name: 'Export as XLS',
                        action: (/**
                         * @return {?}
                         */
                        function () {
                            params.context.tableTools.exportToExcel();
                        }),
                        icon: '<i class="material-icons">file_download</i>'
                    },
                    {
                        name: 'Export as CSV',
                        action: (/**
                         * @return {?}
                         */
                        function () {
                            params.context.tableTools.exportAsCSV();
                        }),
                        icon: '<i class="material-icons">file_download</i>'
                    }
                ]
            }
        ];
        return contextMenuResults;
    };
    TableDefaultsService.decorators = [
        { type: Injectable }
    ];
    return TableDefaultsService;
}());
export { TableDefaultsService };
if (false) {
    /**
     * @type {?}
     * @private
     */
    TableDefaultsService.prototype.tableFilterIcon;
    /**
     * @type {?}
     * @private
     */
    TableDefaultsService.prototype.tableDefDefaults;
    /**
     * @type {?}
     * @private
     */
    TableDefaultsService.prototype.colDefDefaults;
    /**
     * @type {?}
     * @private
     */
    TableDefaultsService.prototype.actionDefaults;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGFibGUtZGVmYXVsdHMuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BocHRlY2gvc2FwaGlyYS1zaGFyZWQtbGliLyIsInNvdXJjZXMiOlsibGliL2dyaWQvdGFibGUvdGFibGUtZGVmYXVsdHMuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUUzQyxPQUFPLEtBQUssQ0FBQyxNQUFNLFFBQVEsQ0FBQztBQUM1QixPQUFPLEtBQUssT0FBTyxNQUFNLFFBQVEsQ0FBQzs7SUFFNUIsTUFBTSxHQUFHLE9BQU87QUFFdEI7SUFBQTtRQUVVLG9CQUFlLEdBQ3JCLDBIQUEwSCxDQUFDO1FBQ3JILHFCQUFnQixHQUFHO1lBQ3pCLG9CQUFvQixFQUFFLElBQUk7WUFDMUIsZUFBZSxFQUFFLElBQUk7WUFDckIsYUFBYSxFQUFFLElBQUk7WUFDbkIsWUFBWSxFQUFFLElBQUk7WUFDbEIsY0FBYyxFQUFFLElBQUk7WUFDcEIsWUFBWSxFQUFFLFVBQVU7WUFDeEIsU0FBUyxFQUFFLEVBQUU7WUFDYiwwQkFBMEIsRUFBRSxJQUFJO1lBRWhDLFlBQVksRUFBRSxFQUFFO1lBQ2hCLEtBQUssRUFBRTtnQkFDTCxNQUFNLEVBQUUsSUFBSSxDQUFDLGVBQWU7YUFDN0I7WUFDRCxnQkFBZ0IsRUFBRSxJQUFJLENBQUMsb0JBQW9CO1NBQzVDLENBQUM7UUFFTSxtQkFBYyxHQUFHO1lBQ3ZCLE1BQU0sRUFBRSxvQkFBb0I7WUFDNUIsY0FBYyxFQUFFLEtBQUs7WUFDckIsWUFBWSxFQUFFLElBQUk7WUFDbEIsZUFBZSxFQUFFLElBQUk7WUFDckIsY0FBYyxFQUFFLElBQUk7WUFDcEIsWUFBWSxFQUFFLElBQUk7U0FDbkIsQ0FBQztRQUVNLG1CQUFjLEdBQUc7WUFDdkIsU0FBUyxFQUFFLFdBQVc7WUFDdEIsTUFBTSxFQUFFLE9BQU87WUFDZixpQkFBaUIsRUFBRSxJQUFJO1lBQ3ZCLFlBQVksRUFBRSxJQUFJO1NBQ25CLENBQUM7SUEySEosQ0FBQzs7Ozs7O0lBekhRLDBDQUFXOzs7OztJQUFsQixVQUFtQixXQUFtQixFQUFFLGNBQW9CO1FBQzFELElBQUksV0FBVyxLQUFLLGtCQUFrQixFQUFFO1lBQ3RDLE9BQU8sQ0FBQyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztTQUMzQztRQUNELElBQUksV0FBVyxLQUFLLGdCQUFnQixFQUFFO1lBQ3BDLE9BQU8sQ0FBQyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUM7U0FDekM7UUFDRCxJQUFJLFdBQVcsS0FBSyxnQkFBZ0IsRUFBRTtZQUNwQyxPQUFPLENBQUMsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDO1NBQ3pDO1FBQ0QsSUFBSSxXQUFXLEtBQUssYUFBYSxJQUFJLENBQUMsY0FBYyxFQUFFO1lBQ3BELE9BQU8sSUFBSSxDQUFDLG1CQUFtQixDQUFDO1NBQ2pDO1FBQ0QsSUFBSSxXQUFXLEtBQUssYUFBYSxJQUFJLGNBQWMsRUFBRTtZQUNuRCxPQUFPLGNBQWMsQ0FBQztTQUN2QjtJQUNILENBQUM7Ozs7OztJQUVNLDZDQUFjOzs7OztJQUFyQixVQUFzQixVQUFnQixFQUFFLFNBQWU7Ozs7WUFHL0MsUUFBUSxHQUFHLE1BQU0sQ0FBQyxTQUFTLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDOztZQUN2RSxNQUFNLEdBQUcsTUFBTSxDQUFDLFVBQVUsQ0FBQztRQUVqQyw0REFBNEQ7UUFDNUQsSUFBSSxRQUFRLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxFQUFFO1lBQzdCLE9BQU8sQ0FBQyxDQUFDLENBQUM7U0FDWDthQUFNLElBQUksUUFBUSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsRUFBRTtZQUNuQyxPQUFPLENBQUMsQ0FBQztTQUNWO2FBQU07WUFDTCxPQUFPLENBQUMsQ0FBQztTQUNWO0lBQ0gsQ0FBQzs7Ozs7SUFFTSxtREFBb0I7Ozs7SUFBM0IsVUFBNEIsTUFBTTtRQUNoQyxJQUFJLE1BQU0sQ0FBQyxJQUFJLEtBQUssWUFBWSxFQUFFO1lBQ2hDLE9BQU87U0FDUjs7WUFDSyxRQUFRLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxLQUFLLEVBQUU7O1lBQ2hDLGVBQWUsR0FDbkIsTUFBTSxDQUFDLE1BQU0sQ0FBQyxrQkFBa0IsQ0FBQyxXQUFXLENBQUMsVUFBVTs7WUFDbkQsaUJBQWlCLEdBQ3JCLGVBQWUsQ0FBQyxlQUFlLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDLE1BQU07UUFDcEQsSUFBSSxpQkFBaUIsS0FBSyxLQUFLLElBQUksaUJBQWlCLEtBQUssTUFBTSxFQUFFOztnQkFDekQsTUFBTSxHQUFHLE1BQU0sQ0FBQyxNQUFNOztnQkFFeEIsVUFBVSxHQUFHLE1BQU0sQ0FBQyxLQUFLLENBQUMsSUFBSTtZQUNsQyw0REFBNEQ7WUFDNUQsVUFBVSxHQUFHLFVBQVUsQ0FBQyxTQUFTLENBQUMsQ0FBQyxFQUFFLFVBQVUsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQzs7Z0JBQ3pELE9BQU8sR0FBRyxRQUFRLENBQUMsVUFBVSxFQUFFLEVBQUUsQ0FBQzs7Z0JBQ3BDLE9BQU8sU0FBQTtZQUNYLGtCQUFrQjtZQUNsQixRQUFRLEtBQUssZUFBZSxJQUFJLFFBQVEsS0FBSyxRQUFRO2dCQUNuRCxDQUFDLENBQUMsQ0FBQyxPQUFPLEdBQUcsT0FBTyxHQUFHLEVBQUUsQ0FBQztnQkFDMUIsQ0FBQyxDQUFDLENBQUMsT0FBTyxHQUFHLE9BQU8sR0FBRyxFQUFFLENBQUMsQ0FBQztZQUM3QixNQUFNLENBQUMsS0FBSyxDQUFDLElBQUksR0FBRyxPQUFPLEdBQUcsSUFBSSxDQUFDO1NBQ3BDO0lBQ0gsQ0FBQzs7Ozs7SUFFTSwrQ0FBZ0I7Ozs7SUFBdkIsVUFBd0IsTUFBTTtRQUM1QixPQUFPLE1BQU0sQ0FBQyxLQUFLLEtBQUssSUFBSSxJQUFJLE1BQU0sQ0FBQyxLQUFLLEtBQUssRUFBRTtZQUNqRCxDQUFDLENBQUMsU0FBUztZQUNYLENBQUMsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDO0lBQ25CLENBQUM7Ozs7O0lBRU0sa0RBQW1COzs7O0lBQTFCLFVBQTJCLE1BQU07O1lBQ3pCLGtCQUFrQixHQUFHO1lBQ3pCO2dCQUNFLElBQUksRUFBRSxrQkFBa0I7Z0JBQ3hCLE1BQU07OztnQkFBRTtvQkFDTixNQUFNLENBQUMsR0FBRyxDQUFDLDRCQUE0QixFQUFFLENBQUM7Z0JBQzVDLENBQUMsQ0FBQTtnQkFDRCxJQUFJLEVBQUUsNENBQTRDO2FBQ25EO1lBQ0Q7Z0JBQ0UsSUFBSSxFQUFFLHNCQUFzQjtnQkFDNUIsTUFBTTs7O2dCQUFFO29CQUNOLE1BQU0sQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDLGdCQUFnQixDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUNwRCxDQUFDLENBQUE7Z0JBQ0QsSUFBSSxFQUFFLDRDQUE0QzthQUNuRDtZQUNEO2dCQUNFLElBQUksRUFBRSxtQ0FBbUM7Z0JBQ3pDLE1BQU07OztnQkFBRTtvQkFDTixNQUFNLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDbkQsQ0FBQyxDQUFBO2dCQUNELElBQUksRUFBRSw0Q0FBNEM7YUFDbkQ7WUFDRCxXQUFXO1lBQ1g7Z0JBQ0UsSUFBSSxFQUFFLE1BQU0sQ0FBQyxHQUFHLENBQUMsa0JBQWtCLEVBQUU7b0JBQ25DLENBQUMsQ0FBQyxpQkFBaUI7b0JBQ25CLENBQUMsQ0FBQyxpQkFBaUI7Z0JBQ3JCLE1BQU07OztnQkFBRTtvQkFDTixNQUFNLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxrQkFBa0IsRUFBRSxDQUFDO2dCQUNqRCxDQUFDLENBQUE7Z0JBQ0QsSUFBSSxFQUFFLHFDQUFxQzthQUM1QztZQUNEO2dCQUNFLElBQUksRUFBRSxjQUFjO2dCQUNwQixJQUFJLEVBQUUsNkNBQTZDO2dCQUNuRCxPQUFPLEVBQUU7b0JBQ1A7d0JBQ0UsSUFBSSxFQUFFLGVBQWU7d0JBQ3JCLE1BQU07Ozt3QkFBRTs0QkFDTixNQUFNLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxhQUFhLEVBQUUsQ0FBQzt3QkFDNUMsQ0FBQyxDQUFBO3dCQUNELElBQUksRUFBRSw2Q0FBNkM7cUJBQ3BEO29CQUNEO3dCQUNFLElBQUksRUFBRSxlQUFlO3dCQUNyQixNQUFNOzs7d0JBQUU7NEJBQ04sTUFBTSxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsV0FBVyxFQUFFLENBQUM7d0JBQzFDLENBQUMsQ0FBQTt3QkFDRCxJQUFJLEVBQUUsNkNBQTZDO3FCQUNwRDtpQkFDRjthQUNGO1NBQ0Y7UUFDRCxPQUFPLGtCQUFrQixDQUFDO0lBQzVCLENBQUM7O2dCQTdKRixVQUFVOztJQThKWCwyQkFBQztDQUFBLEFBOUpELElBOEpDO1NBN0pZLG9CQUFvQjs7Ozs7O0lBQy9CLCtDQUM2SDs7Ozs7SUFDN0gsZ0RBZUU7Ozs7O0lBRUYsOENBT0U7Ozs7O0lBRUYsOENBS0UiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbmltcG9ydCAqIGFzIF8gZnJvbSAnbG9kYXNoJztcbmltcG9ydCAqIGFzIG1vbWVudF8gZnJvbSAnbW9tZW50JztcblxuY29uc3QgbW9tZW50ID0gbW9tZW50XztcblxuQEluamVjdGFibGUoKVxuZXhwb3J0IGNsYXNzIFRhYmxlRGVmYXVsdHNTZXJ2aWNlIHtcbiAgcHJpdmF0ZSB0YWJsZUZpbHRlckljb24gPVxuICAgICc8ZGl2IGNsYXNzPVwiY29sdW1uLWZpbHRlci1oZWFkZXItcGFyZW50XCI+IDxzcGFuIGNsYXNzPVwiYWctaWNvbiBhZy1maWx0ZXItaWNvblwiPjwvc3Bhbj4gPHNwYW4+UmVmaW5lIFNlYXJjaDwvc3Bhbj4gPC9kaXY+JztcbiAgcHJpdmF0ZSB0YWJsZURlZkRlZmF1bHRzID0ge1xuICAgIGVuYWJsZVJhbmdlU2VsZWN0aW9uOiB0cnVlLFxuICAgIGVuYWJsZUNvbFJlc2l6ZTogdHJ1ZSxcbiAgICBlbmFibGVTb3J0aW5nOiB0cnVlLFxuICAgIGVuYWJsZUZpbHRlcjogdHJ1ZSxcbiAgICBmbG9hdGluZ0ZpbHRlcjogdHJ1ZSxcbiAgICByb3dTZWxlY3Rpb246ICdtdWx0aXBsZScsXG4gICAgcm93SGVpZ2h0OiA0NSxcbiAgICBzdXBwcmVzc1Byb3BlcnR5TmFtZXNDaGVjazogdHJ1ZSxcblxuICAgIGhlYWRlckhlaWdodDogMzIsXG4gICAgaWNvbnM6IHtcbiAgICAgIGZpbHRlcjogdGhpcy50YWJsZUZpbHRlckljb25cbiAgICB9LFxuICAgIHBvc3RQcm9jZXNzUG9wdXA6IHRoaXMucmVwb3NpdGlvbkZpbHRlck1lbnVcbiAgfTtcblxuICBwcml2YXRlIGNvbERlZkRlZmF1bHRzID0ge1xuICAgIGZpbHRlcjogJ2FnVGV4dENvbHVtbkZpbHRlcicsXG4gICAgc3VwcHJlc3NGaWx0ZXI6IGZhbHNlLFxuICAgIHN1cHByZXNzTWVudTogdHJ1ZSxcbiAgICBzdXBwcmVzc01vdmFibGU6IHRydWUsXG4gICAgc3VwcHJlc3NSZXNpemU6IHRydWUsXG4gICAgbG9ja1Bvc2l0aW9uOiB0cnVlXG4gIH07XG5cbiAgcHJpdmF0ZSBhY3Rpb25EZWZhdWx0cyA9IHtcbiAgICBjZWxsQ2xhc3M6ICd0YWJsZS1idG4nLFxuICAgIHBpbm5lZDogJ3JpZ2h0JyxcbiAgICBzdXBwcmVzc1Rvb2xQYW5lbDogdHJ1ZSxcbiAgICBzdXBwcmVzc01lbnU6IHRydWVcbiAgfTtcblxuICBwdWJsaWMgZ2V0RGVmYXVsdHMoZGVmYXVsdFR5cGU6IHN0cmluZywgY29udGV4dE1lbnVPYmo/OiBhbnkpOiBhbnkge1xuICAgIGlmIChkZWZhdWx0VHlwZSA9PT0gJ3RhYmxlRGVmRGVmYXVsdHMnKSB7XG4gICAgICByZXR1cm4gXy5jbG9uZURlZXAodGhpcy50YWJsZURlZkRlZmF1bHRzKTtcbiAgICB9XG4gICAgaWYgKGRlZmF1bHRUeXBlID09PSAnY29sRGVmRGVmYXVsdHMnKSB7XG4gICAgICByZXR1cm4gXy5jbG9uZURlZXAodGhpcy5jb2xEZWZEZWZhdWx0cyk7XG4gICAgfVxuICAgIGlmIChkZWZhdWx0VHlwZSA9PT0gJ2FjdGlvbkRlZmF1bHRzJykge1xuICAgICAgcmV0dXJuIF8uY2xvbmVEZWVwKHRoaXMuYWN0aW9uRGVmYXVsdHMpO1xuICAgIH1cbiAgICBpZiAoZGVmYXVsdFR5cGUgPT09ICdjb250ZXh0TWVudScgJiYgIWNvbnRleHRNZW51T2JqKSB7XG4gICAgICByZXR1cm4gdGhpcy5nZXRDb250ZXh0TWVudUl0ZW1zO1xuICAgIH1cbiAgICBpZiAoZGVmYXVsdFR5cGUgPT09ICdjb250ZXh0TWVudScgJiYgY29udGV4dE1lbnVPYmopIHtcbiAgICAgIHJldHVybiBjb250ZXh0TWVudU9iajtcbiAgICB9XG4gIH1cblxuICBwdWJsaWMgZGF0ZUNvbXBhcmF0b3IoZmlsdGVyRGF0ZTogRGF0ZSwgY2VsbFZhbHVlOiBEYXRlKTogbnVtYmVyIHtcbiAgICAvLyBDZWxsIHZhbHVlIGRhdGUgaXMgSVNPIDg2MDEgKGlzaClcbiAgICAvLyBSZW1vdmUgdGltZSBpbmZvcm1hdGlvbiBmcm9tIGNlbGwgdmFsdWUgYXMgZmlsdGVyIGlucHV0IGlzIGEgZGF0ZVxuICAgIGNvbnN0IGNlbGxEYXRlID0gbW9tZW50KGNlbGxWYWx1ZSkuaG91cigwKS5taW51dGUoMCkuc2Vjb25kKDApLm1pbGxpc2Vjb25kKDApO1xuICAgIGNvbnN0IGZpbHRlciA9IG1vbWVudChmaWx0ZXJEYXRlKTtcblxuICAgIC8vIE5vdyB0aGF0IGJvdGggcGFyYW1ldGVycyBhcmUgRGF0ZSBvYmplY3RzLCB3ZSBjYW4gY29tcGFyZVxuICAgIGlmIChjZWxsRGF0ZS5pc0JlZm9yZShmaWx0ZXIpKSB7XG4gICAgICByZXR1cm4gLTE7XG4gICAgfSBlbHNlIGlmIChjZWxsRGF0ZS5pc0FmdGVyKGZpbHRlcikpIHtcbiAgICAgIHJldHVybiAxO1xuICAgIH0gZWxzZSB7XG4gICAgICByZXR1cm4gMDtcbiAgICB9XG4gIH1cblxuICBwdWJsaWMgcmVwb3NpdGlvbkZpbHRlck1lbnUocGFyYW1zKSB7XG4gICAgaWYgKHBhcmFtcy50eXBlICE9PSAnY29sdW1uTWVudScpIHtcbiAgICAgIHJldHVybjtcbiAgICB9XG4gICAgY29uc3QgY29sdW1uSWQgPSBwYXJhbXMuY29sdW1uLmdldElkKCk7XG4gICAgY29uc3QgaW50ZXJuYWxDb2xEZWZzID1cbiAgICAgIHBhcmFtcy5jb2x1bW4uZ3JpZE9wdGlvbnNXcmFwcGVyLmdyaWRPcHRpb25zLmNvbHVtbkRlZnM7XG4gICAgY29uc3QgbGFzdENvbEZpbHRlclR5cGUgPVxuICAgICAgaW50ZXJuYWxDb2xEZWZzW2ludGVybmFsQ29sRGVmcy5sZW5ndGggLSAyXS5maWx0ZXI7XG4gICAgaWYgKGxhc3RDb2xGaWx0ZXJUeXBlID09PSAnc2V0JyB8fCBsYXN0Q29sRmlsdGVyVHlwZSA9PT0gJ2RhdGUnKSB7XG4gICAgICBjb25zdCBlUG9wdXAgPSBwYXJhbXMuZVBvcHVwO1xuXG4gICAgICBsZXQgb2xkTGVmdFN0ciA9IGVQb3B1cC5zdHlsZS5sZWZ0O1xuICAgICAgLy8gcmVtb3ZlICdweCcgZnJvbSB0aGUgc3RyaW5nIChhZy1HcmlkIHVzZXMgcHggcG9zaXRpb25pbmcpXG4gICAgICBvbGRMZWZ0U3RyID0gb2xkTGVmdFN0ci5zdWJzdHJpbmcoMCwgb2xkTGVmdFN0ci5pbmRleE9mKCdweCcpKTtcbiAgICAgIGNvbnN0IG9sZExlZnQgPSBwYXJzZUludChvbGRMZWZ0U3RyLCAxMCk7XG4gICAgICBsZXQgbmV3TGVmdDtcbiAgICAgIC8vIEhBQ0sgLXkgYXMgc2hpdFxuICAgICAgY29sdW1uSWQgPT09ICdtYW5pZmVzdGF0aW9uJyB8fCBjb2x1bW5JZCA9PT0gJ3N0YXR1cydcbiAgICAgICAgPyAobmV3TGVmdCA9IG9sZExlZnQgLSAxNilcbiAgICAgICAgOiAobmV3TGVmdCA9IG9sZExlZnQgLSA1MCk7XG4gICAgICBlUG9wdXAuc3R5bGUubGVmdCA9IG5ld0xlZnQgKyAncHgnO1xuICAgIH1cbiAgfVxuXG4gIHB1YmxpYyB1cGRhdGVOdWxsVmFsdWVzKHBhcmFtcyk6IHZvaWQge1xuICAgIHJldHVybiBwYXJhbXMudmFsdWUgPT09IG51bGwgfHwgcGFyYW1zLnZhbHVlID09PSAnJ1xuICAgICAgPyAnKEJMQU5LKSdcbiAgICAgIDogcGFyYW1zLnZhbHVlO1xuICB9XG5cbiAgcHVibGljIGdldENvbnRleHRNZW51SXRlbXMocGFyYW1zKSB7XG4gICAgY29uc3QgY29udGV4dE1lbnVSZXN1bHRzID0gW1xuICAgICAge1xuICAgICAgICBuYW1lOiAnQ29weSBTaW5nbGUgQ2VsbCcsXG4gICAgICAgIGFjdGlvbjogKCkgPT4ge1xuICAgICAgICAgIHBhcmFtcy5hcGkuY29weVNlbGVjdGVkUmFuZ2VUb0NsaXBib2FyZCgpO1xuICAgICAgICB9LFxuICAgICAgICBpY29uOiAnPGkgY2xhc3M9XCJtYXRlcmlhbC1pY29uc1wiPmNvbnRlbnRfY29weTwvaT4nXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICBuYW1lOiAnQ29weSBTZWxlY3RlZCBSb3cocyknLFxuICAgICAgICBhY3Rpb246ICgpID0+IHtcbiAgICAgICAgICBwYXJhbXMuY29udGV4dC50YWJsZVRvb2xzLmNvcHlTZWxlY3RlZFJvd3MoZmFsc2UpO1xuICAgICAgICB9LFxuICAgICAgICBpY29uOiAnPGkgY2xhc3M9XCJtYXRlcmlhbC1pY29uc1wiPmNvbnRlbnRfY29weTwvaT4nXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICBuYW1lOiAnQ29weSBTZWxlY3RlZCBSb3cocykgV2l0aCBIZWFkZXJzJyxcbiAgICAgICAgYWN0aW9uOiAoKSA9PiB7XG4gICAgICAgICAgcGFyYW1zLmNvbnRleHQudGFibGVUb29scy5jb3B5U2VsZWN0ZWRSb3dzKHRydWUpO1xuICAgICAgICB9LFxuICAgICAgICBpY29uOiAnPGkgY2xhc3M9XCJtYXRlcmlhbC1pY29uc1wiPmNvbnRlbnRfY29weTwvaT4nXG4gICAgICB9LFxuICAgICAgJ3NlcGFyYXRvcicsXG4gICAgICB7XG4gICAgICAgIG5hbWU6IHBhcmFtcy5hcGkuaXNUb29sUGFuZWxTaG93aW5nKClcbiAgICAgICAgICA/ICdIaWRlIFRvb2wgUGFuZWwnXG4gICAgICAgICAgOiAnU2hvdyBUb29sIFBhbmVsJyxcbiAgICAgICAgYWN0aW9uOiAoKSA9PiB7XG4gICAgICAgICAgcGFyYW1zLmNvbnRleHQudGFibGVUb29scy5lbmhhbmNlVGFibGVTZWFyY2goKTtcbiAgICAgICAgfSxcbiAgICAgICAgaWNvbjogJzxpIGNsYXNzPVwibWF0ZXJpYWwtaWNvbnNcIj5idWlsZDwvaT4nXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICBuYW1lOiAnRXhwb3J0IFRhYmxlJyxcbiAgICAgICAgaWNvbjogJzxpIGNsYXNzPVwibWF0ZXJpYWwtaWNvbnNcIj5maWxlX2Rvd25sb2FkPC9pPicsXG4gICAgICAgIHN1Yk1lbnU6IFtcbiAgICAgICAgICB7XG4gICAgICAgICAgICBuYW1lOiAnRXhwb3J0IGFzIFhMUycsXG4gICAgICAgICAgICBhY3Rpb246ICgpID0+IHtcbiAgICAgICAgICAgICAgcGFyYW1zLmNvbnRleHQudGFibGVUb29scy5leHBvcnRUb0V4Y2VsKCk7XG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgaWNvbjogJzxpIGNsYXNzPVwibWF0ZXJpYWwtaWNvbnNcIj5maWxlX2Rvd25sb2FkPC9pPidcbiAgICAgICAgICB9LFxuICAgICAgICAgIHtcbiAgICAgICAgICAgIG5hbWU6ICdFeHBvcnQgYXMgQ1NWJyxcbiAgICAgICAgICAgIGFjdGlvbjogKCkgPT4ge1xuICAgICAgICAgICAgICBwYXJhbXMuY29udGV4dC50YWJsZVRvb2xzLmV4cG9ydEFzQ1NWKCk7XG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgaWNvbjogJzxpIGNsYXNzPVwibWF0ZXJpYWwtaWNvbnNcIj5maWxlX2Rvd25sb2FkPC9pPidcbiAgICAgICAgICB9XG4gICAgICAgIF1cbiAgICAgIH1cbiAgICBdO1xuICAgIHJldHVybiBjb250ZXh0TWVudVJlc3VsdHM7XG4gIH1cbn1cbiJdfQ==