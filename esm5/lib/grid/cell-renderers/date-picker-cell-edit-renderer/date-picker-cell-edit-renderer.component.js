/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, ViewChild } from '@angular/core';
import { MatDatepicker } from '@angular/material';
import { TimeService } from '../../../services/time.service';
var DatePickerCellEditRendererComponent = /** @class */ (function () {
    function DatePickerCellEditRendererComponent(timeService) {
        this.timeService = timeService;
    }
    /**
     * @return {?}
     */
    DatePickerCellEditRendererComponent.prototype.ngAfterViewInit = /**
     * @return {?}
     */
    function () {
        this.picker.open();
    };
    /**
     * @return {?}
     */
    DatePickerCellEditRendererComponent.prototype.isPopup = /**
     * @return {?}
     */
    function () {
        return false;
    };
    /**
     * @return {?}
     */
    DatePickerCellEditRendererComponent.prototype.isCancelBeforeStart = /**
     * @return {?}
     */
    function () {
        return false;
    };
    /**
     * @return {?}
     */
    DatePickerCellEditRendererComponent.prototype.isCancelAfterEnd = /**
     * @return {?}
     */
    function () {
        return false;
    };
    /**
     * @param {?} params
     * @return {?}
     */
    DatePickerCellEditRendererComponent.prototype.agInit = /**
     * @param {?} params
     * @return {?}
     */
    function (params) {
        this.params = params;
        this.value = params.value;
    };
    /**
     * @return {?}
     */
    DatePickerCellEditRendererComponent.prototype.getValue = /**
     * @return {?}
     */
    function () {
        return this.timeService.standardDateFormatter(this.value, 'MM/DD/YYYY');
    };
    /**
     * @param {?} e
     * @return {?}
     */
    DatePickerCellEditRendererComponent.prototype.onSelectChange = /**
     * @param {?} e
     * @return {?}
     */
    function (e) {
        this.params.stopEditing();
    };
    DatePickerCellEditRendererComponent.decorators = [
        { type: Component, args: [{
                    selector: 'app-editor-cell',
                    template: "<mat-form-field>\n  <input matInput [matDatepicker]=\"picker\" [(ngModel)]=\"value\" (dateInput)=\"onSelectChange($event)\">\n  <mat-datepicker-toggle matSuffix [for]=\"picker\"></mat-datepicker-toggle>\n  <mat-datepicker #picker></mat-datepicker>\n</mat-form-field>",
                    styles: [".md-form-field{margin-top:-16px}"]
                }] }
    ];
    /** @nocollapse */
    DatePickerCellEditRendererComponent.ctorParameters = function () { return [
        { type: TimeService }
    ]; };
    DatePickerCellEditRendererComponent.propDecorators = {
        picker: [{ type: ViewChild, args: ['picker', { read: MatDatepicker, static: true },] }]
    };
    return DatePickerCellEditRendererComponent;
}());
export { DatePickerCellEditRendererComponent };
if (false) {
    /** @type {?} */
    DatePickerCellEditRendererComponent.prototype.params;
    /** @type {?} */
    DatePickerCellEditRendererComponent.prototype.value;
    /** @type {?} */
    DatePickerCellEditRendererComponent.prototype.picker;
    /**
     * @type {?}
     * @private
     */
    DatePickerCellEditRendererComponent.prototype.timeService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGF0ZS1waWNrZXItY2VsbC1lZGl0LXJlbmRlcmVyLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BocHRlY2gvc2FwaGlyYS1zaGFyZWQtbGliLyIsInNvdXJjZXMiOlsibGliL2dyaWQvY2VsbC1yZW5kZXJlcnMvZGF0ZS1waWNrZXItY2VsbC1lZGl0LXJlbmRlcmVyL2RhdGUtcGlja2VyLWNlbGwtZWRpdC1yZW5kZXJlci5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsU0FBUyxFQUFpQixNQUFNLGVBQWUsQ0FBQztBQUdwRSxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sbUJBQW1CLENBQUM7QUFDbEQsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLGdDQUFnQyxDQUFDO0FBRTdEO0lBVUUsNkNBQW9CLFdBQXdCO1FBQXhCLGdCQUFXLEdBQVgsV0FBVyxDQUFhO0lBQzVDLENBQUM7Ozs7SUFFRCw2REFBZTs7O0lBQWY7UUFDRSxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksRUFBRSxDQUFDO0lBQ3JCLENBQUM7Ozs7SUFFRCxxREFBTzs7O0lBQVA7UUFDRSxPQUFPLEtBQUssQ0FBQztJQUNmLENBQUM7Ozs7SUFFRCxpRUFBbUI7OztJQUFuQjtRQUNFLE9BQU8sS0FBSyxDQUFDO0lBQ2YsQ0FBQzs7OztJQUVELDhEQUFnQjs7O0lBQWhCO1FBQ0UsT0FBTyxLQUFLLENBQUM7SUFDZixDQUFDOzs7OztJQUVELG9EQUFNOzs7O0lBQU4sVUFBTyxNQUFXO1FBQ2hCLElBQUksQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDO1FBQ3JCLElBQUksQ0FBQyxLQUFLLEdBQUcsTUFBTSxDQUFDLEtBQUssQ0FBQztJQUM1QixDQUFDOzs7O0lBRUQsc0RBQVE7OztJQUFSO1FBQ0UsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDLHFCQUFxQixDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUsWUFBWSxDQUFDLENBQUM7SUFDMUUsQ0FBQzs7Ozs7SUFFRCw0REFBYzs7OztJQUFkLFVBQWUsQ0FBQztRQUNkLElBQUksQ0FBQyxNQUFNLENBQUMsV0FBVyxFQUFFLENBQUM7SUFDNUIsQ0FBQzs7Z0JBeENGLFNBQVMsU0FBQztvQkFDVCxRQUFRLEVBQUUsaUJBQWlCO29CQUMzQixzUkFBOEQ7O2lCQUUvRDs7OztnQkFOUSxXQUFXOzs7eUJBVWpCLFNBQVMsU0FBQyxRQUFRLEVBQUUsRUFBRSxJQUFJLEVBQUUsYUFBYSxFQUFFLE1BQU0sRUFBRSxJQUFJLEVBQUU7O0lBaUM1RCwwQ0FBQztDQUFBLEFBekNELElBeUNDO1NBcENZLG1DQUFtQzs7O0lBQzlDLHFEQUEwQjs7SUFDMUIsb0RBQWM7O0lBQ2QscURBQXdGOzs7OztJQUU1RSwwREFBZ0MiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIFZpZXdDaGlsZCwgQWZ0ZXJWaWV3SW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgSUNlbGxFZGl0b3JQYXJhbXMgfSBmcm9tICdhZy1ncmlkLWNvbW11bml0eSc7XG5pbXBvcnQgeyBBZ0VkaXRvckNvbXBvbmVudCB9IGZyb20gJ2FnLWdyaWQtYW5ndWxhcic7XG5pbXBvcnQgeyBNYXREYXRlcGlja2VyIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwnO1xuaW1wb3J0IHsgVGltZVNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9zZXJ2aWNlcy90aW1lLnNlcnZpY2UnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdhcHAtZWRpdG9yLWNlbGwnLFxuICB0ZW1wbGF0ZVVybDogJy4vZGF0ZS1waWNrZXItY2VsbC1lZGl0LXJlbmRlcmVyLmNvbXBvbmVudC5odG1sJyxcbiAgc3R5bGVVcmxzOiBbJy4vZGF0ZS1waWNrZXItY2VsbC1lZGl0LXJlbmRlcmVyLmNvbXBvbmVudC5zY3NzJ11cbn0pXG5leHBvcnQgY2xhc3MgRGF0ZVBpY2tlckNlbGxFZGl0UmVuZGVyZXJDb21wb25lbnQgaW1wbGVtZW50cyBBZ0VkaXRvckNvbXBvbmVudCwgQWZ0ZXJWaWV3SW5pdCB7XG4gIHBhcmFtczogSUNlbGxFZGl0b3JQYXJhbXM7XG4gIHZhbHVlOiBzdHJpbmc7XG4gIEBWaWV3Q2hpbGQoJ3BpY2tlcicsIHsgcmVhZDogTWF0RGF0ZXBpY2tlciwgc3RhdGljOiB0cnVlIH0pIHBpY2tlcjogTWF0RGF0ZXBpY2tlcjxEYXRlPjtcblxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIHRpbWVTZXJ2aWNlOiBUaW1lU2VydmljZSkge1xuICB9XG5cbiAgbmdBZnRlclZpZXdJbml0KCkge1xuICAgIHRoaXMucGlja2VyLm9wZW4oKTtcbiAgfVxuXG4gIGlzUG9wdXAoKTogYm9vbGVhbiB7XG4gICAgcmV0dXJuIGZhbHNlO1xuICB9XG5cbiAgaXNDYW5jZWxCZWZvcmVTdGFydCgpOiBib29sZWFuIHtcbiAgICByZXR1cm4gZmFsc2U7XG4gIH1cblxuICBpc0NhbmNlbEFmdGVyRW5kKCk6IGJvb2xlYW4ge1xuICAgIHJldHVybiBmYWxzZTtcbiAgfVxuXG4gIGFnSW5pdChwYXJhbXM6IGFueSk6IHZvaWQge1xuICAgIHRoaXMucGFyYW1zID0gcGFyYW1zO1xuICAgIHRoaXMudmFsdWUgPSBwYXJhbXMudmFsdWU7XG4gIH1cblxuICBnZXRWYWx1ZSgpOiBzdHJpbmcge1xuICAgIHJldHVybiB0aGlzLnRpbWVTZXJ2aWNlLnN0YW5kYXJkRGF0ZUZvcm1hdHRlcih0aGlzLnZhbHVlLCAnTU0vREQvWVlZWScpO1xuICB9XG5cbiAgb25TZWxlY3RDaGFuZ2UoZSk6IHZvaWQge1xuICAgIHRoaXMucGFyYW1zLnN0b3BFZGl0aW5nKCk7XG4gIH1cbn1cbiJdfQ==