/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
/**
 * @record
 */
export function Button() { }
if (false) {
    /** @type {?} */
    Button.prototype.type;
    /** @type {?} */
    Button.prototype.icon;
    /** @type {?} */
    Button.prototype.color;
    /** @type {?|undefined} */
    Button.prototype.navigateTo;
    /** @type {?|undefined} */
    Button.prototype.queryParams;
    /** @type {?|undefined} */
    Button.prototype.newWindow;
}
/**
 * @record
 */
export function RowButtonEvent() { }
if (false) {
    /** @type {?} */
    RowButtonEvent.prototype.type;
    /** @type {?} */
    RowButtonEvent.prototype.rowData;
}
var RowButtonComponent = /** @class */ (function () {
    function RowButtonComponent(activatedRoute, router) {
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.idOfMostRecentCommit = '';
    }
    /**
     * @param {?} params
     * @return {?}
     */
    RowButtonComponent.prototype.agInit = /**
     * @param {?} params
     * @return {?}
     */
    function (params) {
        // Row data
        this.data = params.data;
        this.tableData = params.context._data;
        this.buttons = (/** @type {?} */ (params.colDef.buttons));
        this.setIcons();
        this.outputEmitter = params.context.buttonOutput;
    };
    /**
     * @private
     * @return {?}
     */
    RowButtonComponent.prototype.setIcons = /**
     * @private
     * @return {?}
     */
    function () {
        var e_1, _a;
        try {
            for (var _b = tslib_1.__values(this.buttons), _c = _b.next(); !_c.done; _c = _b.next()) {
                var button = _c.value;
                switch (button.type) {
                    // Dual table, add row to selection
                    case 'add': {
                        button.icon = 'add_circle';
                        button.color = '#03A9F4';
                        break;
                    }
                    case 'delete': {
                        // delete is set to remove the most recent breadcrumb and is used
                        // to route user after use - use remove if this is undesirable
                        button.icon = 'delete';
                        button.color = '#ef5350';
                        break;
                    }
                    case 'edit': {
                        button.icon = 'mode_edit';
                        button.color = '#4caf50';
                        break;
                    }
                    case 'info': {
                        button.icon = 'info_outline';
                        button.color = '#03A9F4';
                        break;
                    }
                    // Dual table, remove row from selection
                    case 'remove': {
                        button.icon = 'remove_circle';
                        button.color = '#F44336';
                        break;
                    }
                }
            }
        }
        catch (e_1_1) { e_1 = { error: e_1_1 }; }
        finally {
            try {
                if (_c && !_c.done && (_a = _b.return)) _a.call(_b);
            }
            finally { if (e_1) throw e_1.error; }
        }
    };
    /**
     * @param {?} button
     * @return {?}
     */
    RowButtonComponent.prototype.clicked = /**
     * @param {?} button
     * @return {?}
     */
    function (button) {
        var e_2, _a;
        if (button.navigateTo) {
            // interpolate URL
            /** @type {?} */
            var url = [];
            if (typeof button.navigateTo === 'string') {
                url = [this.interpolate(button.navigateTo)];
            }
            else if (button.navigateTo instanceof Array) {
                try {
                    for (var _b = tslib_1.__values(button.navigateTo), _c = _b.next(); !_c.done; _c = _b.next()) {
                        var s = _c.value;
                        url.push(this.interpolate(s));
                    }
                }
                catch (e_2_1) { e_2 = { error: e_2_1 }; }
                finally {
                    try {
                        if (_c && !_c.done && (_a = _b.return)) _a.call(_b);
                    }
                    finally { if (e_2) throw e_2.error; }
                }
            }
        }
        /** @type {?} */
        var d = {
            type: button.type,
            rowData: this.data
        };
        this.outputEmitter.emit(d);
    };
    /**
     * @private
     * @param {?} target
     * @return {?}
     */
    RowButtonComponent.prototype.interpolate = /**
     * @private
     * @param {?} target
     * @return {?}
     */
    function (target) {
        /** @type {?} */
        var interpolation = /\$\{(\w+)\}/g;
        /** @type {?} */
        var match;
        while ((match = interpolation.exec(target))) {
            /** @type {?} */
            var key = match[1];
            // this checks for routing from within the tree table
            if (Array.isArray(this.data[key]) && this.data[key].length > 1) {
                target = target.replace('${' + key + '}', this.data[key][0]);
            }
            else {
                target = target.replace('${' + key + '}', this.data[key]);
            }
        }
        return target;
    };
    /**
     * @return {?}
     */
    RowButtonComponent.prototype.refresh = /**
     * @return {?}
     */
    function () {
        return false;
    };
    RowButtonComponent.decorators = [
        { type: Component, args: [{
                    selector: 'app-grid-row-button',
                    template: "\n    <button mat-icon-button\n            [style.color]=\"button.color\"\n            (click)=\"clicked(button)\"\n            *ngFor=\"let button of buttons\"\n            class=\"grid-row-btn\">\n      <span class=\"material-icons\" [ngClass]=\"button.icon\">\n        <mat-icon>{{button.icon}}</mat-icon>\n      </span>\n    </button>\n    "
                }] }
    ];
    /** @nocollapse */
    RowButtonComponent.ctorParameters = function () { return [
        { type: ActivatedRoute },
        { type: Router }
    ]; };
    return RowButtonComponent;
}());
export { RowButtonComponent };
if (false) {
    /**
     * @type {?}
     * @private
     */
    RowButtonComponent.prototype.data;
    /**
     * @type {?}
     * @private
     */
    RowButtonComponent.prototype.tableData;
    /** @type {?} */
    RowButtonComponent.prototype.buttons;
    /**
     * @type {?}
     * @private
     */
    RowButtonComponent.prototype.idOfMostRecentCommit;
    /**
     * @type {?}
     * @private
     */
    RowButtonComponent.prototype.outputEmitter;
    /** @type {?} */
    RowButtonComponent.prototype.activatedRoute;
    /**
     * @type {?}
     * @private
     */
    RowButtonComponent.prototype.router;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicm93LWJ1dHRvbi5jb21wb25lbnRzLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGhwdGVjaC9zYXBoaXJhLXNoYXJlZC1saWIvIiwic291cmNlcyI6WyJsaWIvZ3JpZC9yb3ctYnV0dG9uLmNvbXBvbmVudHMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFnQixNQUFNLGVBQWUsQ0FBQztBQUV4RCxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sRUFBRSxNQUFNLGlCQUFpQixDQUFDOzs7O0FBRXpELDRCQU9DOzs7SUFOQyxzQkFBYTs7SUFDYixzQkFBYTs7SUFDYix1QkFBYzs7SUFDZCw0QkFBK0I7O0lBQy9CLDZCQUF3Qzs7SUFDeEMsMkJBQW9COzs7OztBQUd0QixvQ0FBK0Q7OztJQUE3Qiw4QkFBYTs7SUFBQyxpQ0FBYTs7QUFFN0Q7SUFxQkUsNEJBQ1MsY0FBOEIsRUFDN0IsTUFBYztRQURmLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQUM3QixXQUFNLEdBQU4sTUFBTSxDQUFRO1FBTGhCLHlCQUFvQixHQUFHLEVBQUUsQ0FBQztJQU05QixDQUFDOzs7OztJQUVMLG1DQUFNOzs7O0lBQU4sVUFBTyxNQUFXO1FBQ2hCLFdBQVc7UUFDWCxJQUFJLENBQUMsSUFBSSxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUM7UUFDeEIsSUFBSSxDQUFDLFNBQVMsR0FBRyxNQUFNLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQztRQUV0QyxJQUFJLENBQUMsT0FBTyxHQUFHLG1CQUFBLE1BQU0sQ0FBQyxNQUFNLENBQUMsT0FBTyxFQUFZLENBQUM7UUFDakQsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO1FBRWhCLElBQUksQ0FBQyxhQUFhLEdBQUcsTUFBTSxDQUFDLE9BQU8sQ0FBQyxZQUFZLENBQUM7SUFDbkQsQ0FBQzs7Ozs7SUFFTyxxQ0FBUTs7OztJQUFoQjs7O1lBQ0UsS0FBcUIsSUFBQSxLQUFBLGlCQUFBLElBQUksQ0FBQyxPQUFPLENBQUEsZ0JBQUEsNEJBQUU7Z0JBQTlCLElBQU0sTUFBTSxXQUFBO2dCQUNmLFFBQVEsTUFBTSxDQUFDLElBQUksRUFBRTtvQkFDbkIsbUNBQW1DO29CQUNuQyxLQUFLLEtBQUssQ0FBQyxDQUFDO3dCQUNWLE1BQU0sQ0FBQyxJQUFJLEdBQUcsWUFBWSxDQUFDO3dCQUMzQixNQUFNLENBQUMsS0FBSyxHQUFHLFNBQVMsQ0FBQzt3QkFDekIsTUFBTTtxQkFDUDtvQkFDRCxLQUFLLFFBQVEsQ0FBQyxDQUFDO3dCQUNiLGlFQUFpRTt3QkFDakUsOERBQThEO3dCQUM5RCxNQUFNLENBQUMsSUFBSSxHQUFHLFFBQVEsQ0FBQzt3QkFDdkIsTUFBTSxDQUFDLEtBQUssR0FBRyxTQUFTLENBQUM7d0JBQ3pCLE1BQU07cUJBQ1A7b0JBQ0QsS0FBSyxNQUFNLENBQUMsQ0FBQzt3QkFDWCxNQUFNLENBQUMsSUFBSSxHQUFHLFdBQVcsQ0FBQzt3QkFDMUIsTUFBTSxDQUFDLEtBQUssR0FBRyxTQUFTLENBQUM7d0JBQ3pCLE1BQU07cUJBQ1A7b0JBQ0QsS0FBSyxNQUFNLENBQUMsQ0FBQzt3QkFDWCxNQUFNLENBQUMsSUFBSSxHQUFHLGNBQWMsQ0FBQzt3QkFDN0IsTUFBTSxDQUFDLEtBQUssR0FBRyxTQUFTLENBQUM7d0JBQ3pCLE1BQU07cUJBQ1A7b0JBQ0Qsd0NBQXdDO29CQUN4QyxLQUFLLFFBQVEsQ0FBQyxDQUFDO3dCQUNiLE1BQU0sQ0FBQyxJQUFJLEdBQUcsZUFBZSxDQUFDO3dCQUM5QixNQUFNLENBQUMsS0FBSyxHQUFHLFNBQVMsQ0FBQzt3QkFDekIsTUFBTTtxQkFDUDtpQkFDRjthQUNGOzs7Ozs7Ozs7SUFDSCxDQUFDOzs7OztJQUVNLG9DQUFPOzs7O0lBQWQsVUFBZSxNQUFjOztRQUMzQixJQUFJLE1BQU0sQ0FBQyxVQUFVLEVBQUU7OztnQkFFakIsR0FBRyxHQUFhLEVBQUU7WUFDdEIsSUFBSSxPQUFPLE1BQU0sQ0FBQyxVQUFVLEtBQUssUUFBUSxFQUFFO2dCQUN6QyxHQUFHLEdBQUcsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDO2FBQzdDO2lCQUFNLElBQUksTUFBTSxDQUFDLFVBQVUsWUFBWSxLQUFLLEVBQUU7O29CQUM3QyxLQUFnQixJQUFBLEtBQUEsaUJBQUEsTUFBTSxDQUFDLFVBQVUsQ0FBQSxnQkFBQSw0QkFBRTt3QkFBOUIsSUFBTSxDQUFDLFdBQUE7d0JBQ1YsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7cUJBQy9COzs7Ozs7Ozs7YUFDRjtTQUVGOztZQUNLLENBQUMsR0FBbUI7WUFDeEIsSUFBSSxFQUFFLE1BQU0sQ0FBQyxJQUFJO1lBQ2pCLE9BQU8sRUFBRSxJQUFJLENBQUMsSUFBSTtTQUNuQjtRQUNELElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQzdCLENBQUM7Ozs7OztJQUVPLHdDQUFXOzs7OztJQUFuQixVQUFvQixNQUFjOztZQUMxQixhQUFhLEdBQUcsY0FBYzs7WUFDaEMsS0FBSztRQUNULE9BQU8sQ0FBQyxLQUFLLEdBQUcsYUFBYSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxFQUFFOztnQkFDckMsR0FBRyxHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUM7WUFDcEIscURBQXFEO1lBQ3JELElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO2dCQUM5RCxNQUFNLEdBQUcsTUFBTSxDQUFDLE9BQU8sQ0FBQyxJQUFJLEdBQUcsR0FBRyxHQUFHLEdBQUcsRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7YUFDOUQ7aUJBQU07Z0JBQ0wsTUFBTSxHQUFHLE1BQU0sQ0FBQyxPQUFPLENBQUMsSUFBSSxHQUFHLEdBQUcsR0FBRyxHQUFHLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO2FBQzNEO1NBQ0Y7UUFFRCxPQUFPLE1BQU0sQ0FBQztJQUNoQixDQUFDOzs7O0lBRUQsb0NBQU87OztJQUFQO1FBQ0UsT0FBTyxLQUFLLENBQUM7SUFDZixDQUFDOztnQkEvR0YsU0FBUyxTQUFDO29CQUNULFFBQVEsRUFBRSxxQkFBcUI7b0JBQy9CLFFBQVEsRUFBRSwwVkFVUDtpQkFDSjs7OztnQkExQlEsY0FBYztnQkFBRSxNQUFNOztJQTZIL0IseUJBQUM7Q0FBQSxBQWhIRCxJQWdIQztTQWxHWSxrQkFBa0I7Ozs7OztJQUM3QixrQ0FBa0I7Ozs7O0lBQ2xCLHVDQUF1Qjs7SUFDdkIscUNBQXlCOzs7OztJQUN6QixrREFBa0M7Ozs7O0lBQ2xDLDJDQUFvRDs7SUFHbEQsNENBQXFDOzs7OztJQUNyQyxvQ0FBc0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIEV2ZW50RW1pdHRlciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgQWdSZW5kZXJlckNvbXBvbmVudCB9IGZyb20gJ2FnLWdyaWQtYW5ndWxhci9tYWluJztcbmltcG9ydCB7IEFjdGl2YXRlZFJvdXRlLCBSb3V0ZXIgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xuXG5leHBvcnQgaW50ZXJmYWNlIEJ1dHRvbiB7XG4gIHR5cGU6IHN0cmluZztcbiAgaWNvbjogc3RyaW5nO1xuICBjb2xvcjogc3RyaW5nO1xuICBuYXZpZ2F0ZVRvPzogc3RyaW5nIHwgc3RyaW5nW107XG4gIHF1ZXJ5UGFyYW1zPzogeyBba2V5OiBzdHJpbmddOiBzdHJpbmcgfTtcbiAgbmV3V2luZG93PzogYm9vbGVhbjtcbn1cblxuZXhwb3J0IGludGVyZmFjZSBSb3dCdXR0b25FdmVudCB7IHR5cGU6IHN0cmluZzsgcm93RGF0YTogYW55OyB9XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2FwcC1ncmlkLXJvdy1idXR0b24nLFxuICB0ZW1wbGF0ZTogYFxuICAgIDxidXR0b24gbWF0LWljb24tYnV0dG9uXG4gICAgICAgICAgICBbc3R5bGUuY29sb3JdPVwiYnV0dG9uLmNvbG9yXCJcbiAgICAgICAgICAgIChjbGljayk9XCJjbGlja2VkKGJ1dHRvbilcIlxuICAgICAgICAgICAgKm5nRm9yPVwibGV0IGJ1dHRvbiBvZiBidXR0b25zXCJcbiAgICAgICAgICAgIGNsYXNzPVwiZ3JpZC1yb3ctYnRuXCI+XG4gICAgICA8c3BhbiBjbGFzcz1cIm1hdGVyaWFsLWljb25zXCIgW25nQ2xhc3NdPVwiYnV0dG9uLmljb25cIj5cbiAgICAgICAgPG1hdC1pY29uPnt7YnV0dG9uLmljb259fTwvbWF0LWljb24+XG4gICAgICA8L3NwYW4+XG4gICAgPC9idXR0b24+XG4gICAgYFxufSlcbmV4cG9ydCBjbGFzcyBSb3dCdXR0b25Db21wb25lbnQgaW1wbGVtZW50cyBBZ1JlbmRlcmVyQ29tcG9uZW50IHtcbiAgcHJpdmF0ZSBkYXRhOiBhbnk7XG4gIHByaXZhdGUgdGFibGVEYXRhOiBhbnk7XG4gIHB1YmxpYyBidXR0b25zOiBCdXR0b25bXTtcbiAgcHJpdmF0ZSBpZE9mTW9zdFJlY2VudENvbW1pdCA9ICcnO1xuICBwcml2YXRlIG91dHB1dEVtaXR0ZXI6IEV2ZW50RW1pdHRlcjxSb3dCdXR0b25FdmVudD47XG5cbiAgY29uc3RydWN0b3IoXG4gICAgcHVibGljIGFjdGl2YXRlZFJvdXRlOiBBY3RpdmF0ZWRSb3V0ZSxcbiAgICBwcml2YXRlIHJvdXRlcjogUm91dGVyXG4gICkgeyB9XG5cbiAgYWdJbml0KHBhcmFtczogYW55KTogdm9pZCB7XG4gICAgLy8gUm93IGRhdGFcbiAgICB0aGlzLmRhdGEgPSBwYXJhbXMuZGF0YTtcbiAgICB0aGlzLnRhYmxlRGF0YSA9IHBhcmFtcy5jb250ZXh0Ll9kYXRhO1xuXG4gICAgdGhpcy5idXR0b25zID0gcGFyYW1zLmNvbERlZi5idXR0b25zIGFzIEJ1dHRvbltdO1xuICAgIHRoaXMuc2V0SWNvbnMoKTtcblxuICAgIHRoaXMub3V0cHV0RW1pdHRlciA9IHBhcmFtcy5jb250ZXh0LmJ1dHRvbk91dHB1dDtcbiAgfVxuXG4gIHByaXZhdGUgc2V0SWNvbnMoKTogdm9pZCB7XG4gICAgZm9yIChjb25zdCBidXR0b24gb2YgdGhpcy5idXR0b25zKSB7XG4gICAgICBzd2l0Y2ggKGJ1dHRvbi50eXBlKSB7XG4gICAgICAgIC8vIER1YWwgdGFibGUsIGFkZCByb3cgdG8gc2VsZWN0aW9uXG4gICAgICAgIGNhc2UgJ2FkZCc6IHtcbiAgICAgICAgICBidXR0b24uaWNvbiA9ICdhZGRfY2lyY2xlJztcbiAgICAgICAgICBidXR0b24uY29sb3IgPSAnIzAzQTlGNCc7XG4gICAgICAgICAgYnJlYWs7XG4gICAgICAgIH1cbiAgICAgICAgY2FzZSAnZGVsZXRlJzoge1xuICAgICAgICAgIC8vIGRlbGV0ZSBpcyBzZXQgdG8gcmVtb3ZlIHRoZSBtb3N0IHJlY2VudCBicmVhZGNydW1iIGFuZCBpcyB1c2VkXG4gICAgICAgICAgLy8gdG8gcm91dGUgdXNlciBhZnRlciB1c2UgLSB1c2UgcmVtb3ZlIGlmIHRoaXMgaXMgdW5kZXNpcmFibGVcbiAgICAgICAgICBidXR0b24uaWNvbiA9ICdkZWxldGUnO1xuICAgICAgICAgIGJ1dHRvbi5jb2xvciA9ICcjZWY1MzUwJztcbiAgICAgICAgICBicmVhaztcbiAgICAgICAgfVxuICAgICAgICBjYXNlICdlZGl0Jzoge1xuICAgICAgICAgIGJ1dHRvbi5pY29uID0gJ21vZGVfZWRpdCc7XG4gICAgICAgICAgYnV0dG9uLmNvbG9yID0gJyM0Y2FmNTAnO1xuICAgICAgICAgIGJyZWFrO1xuICAgICAgICB9XG4gICAgICAgIGNhc2UgJ2luZm8nOiB7XG4gICAgICAgICAgYnV0dG9uLmljb24gPSAnaW5mb19vdXRsaW5lJztcbiAgICAgICAgICBidXR0b24uY29sb3IgPSAnIzAzQTlGNCc7XG4gICAgICAgICAgYnJlYWs7XG4gICAgICAgIH1cbiAgICAgICAgLy8gRHVhbCB0YWJsZSwgcmVtb3ZlIHJvdyBmcm9tIHNlbGVjdGlvblxuICAgICAgICBjYXNlICdyZW1vdmUnOiB7XG4gICAgICAgICAgYnV0dG9uLmljb24gPSAncmVtb3ZlX2NpcmNsZSc7XG4gICAgICAgICAgYnV0dG9uLmNvbG9yID0gJyNGNDQzMzYnO1xuICAgICAgICAgIGJyZWFrO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgcHVibGljIGNsaWNrZWQoYnV0dG9uOiBCdXR0b24pOiB2b2lkIHtcbiAgICBpZiAoYnV0dG9uLm5hdmlnYXRlVG8pIHtcbiAgICAgIC8vIGludGVycG9sYXRlIFVSTFxuICAgICAgbGV0IHVybDogc3RyaW5nW10gPSBbXTtcbiAgICAgIGlmICh0eXBlb2YgYnV0dG9uLm5hdmlnYXRlVG8gPT09ICdzdHJpbmcnKSB7XG4gICAgICAgIHVybCA9IFt0aGlzLmludGVycG9sYXRlKGJ1dHRvbi5uYXZpZ2F0ZVRvKV07XG4gICAgICB9IGVsc2UgaWYgKGJ1dHRvbi5uYXZpZ2F0ZVRvIGluc3RhbmNlb2YgQXJyYXkpIHtcbiAgICAgICAgZm9yIChjb25zdCBzIG9mIGJ1dHRvbi5uYXZpZ2F0ZVRvKSB7XG4gICAgICAgICAgdXJsLnB1c2godGhpcy5pbnRlcnBvbGF0ZShzKSk7XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgIH1cbiAgICBjb25zdCBkOiBSb3dCdXR0b25FdmVudCA9IHtcbiAgICAgIHR5cGU6IGJ1dHRvbi50eXBlLFxuICAgICAgcm93RGF0YTogdGhpcy5kYXRhXG4gICAgfTtcbiAgICB0aGlzLm91dHB1dEVtaXR0ZXIuZW1pdChkKTtcbiAgfVxuXG4gIHByaXZhdGUgaW50ZXJwb2xhdGUodGFyZ2V0OiBzdHJpbmcpOiBzdHJpbmcge1xuICAgIGNvbnN0IGludGVycG9sYXRpb24gPSAvXFwkXFx7KFxcdyspXFx9L2c7XG4gICAgbGV0IG1hdGNoO1xuICAgIHdoaWxlICgobWF0Y2ggPSBpbnRlcnBvbGF0aW9uLmV4ZWModGFyZ2V0KSkpIHtcbiAgICAgIGNvbnN0IGtleSA9IG1hdGNoWzFdO1xuICAgICAgLy8gdGhpcyBjaGVja3MgZm9yIHJvdXRpbmcgZnJvbSB3aXRoaW4gdGhlIHRyZWUgdGFibGVcbiAgICAgIGlmIChBcnJheS5pc0FycmF5KHRoaXMuZGF0YVtrZXldKSAmJiB0aGlzLmRhdGFba2V5XS5sZW5ndGggPiAxKSB7XG4gICAgICAgIHRhcmdldCA9IHRhcmdldC5yZXBsYWNlKCckeycgKyBrZXkgKyAnfScsIHRoaXMuZGF0YVtrZXldWzBdKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHRhcmdldCA9IHRhcmdldC5yZXBsYWNlKCckeycgKyBrZXkgKyAnfScsIHRoaXMuZGF0YVtrZXldKTtcbiAgICAgIH1cbiAgICB9XG5cbiAgICByZXR1cm4gdGFyZ2V0O1xuICB9XG5cbiAgcmVmcmVzaCgpOiBib29sZWFuIHtcbiAgICByZXR1cm4gZmFsc2U7XG4gIH1cbn1cbiJdfQ==