/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { DataService } from './data.service';
var AuthService = /** @class */ (function (_super) {
    tslib_1.__extends(AuthService, _super);
    function AuthService(http, router) {
        var _this = _super.call(this, http) || this;
        _this.router = router;
        _this.loginPath = 'login';
        _this.logoutPath = 'logout';
        _this.userInfo = null;
        return _this;
    }
    /**
     * @param {?} username
     * @param {?} password
     * @return {?}
     */
    AuthService.prototype.login = /**
     * @param {?} username
     * @param {?} password
     * @return {?}
     */
    function (username, password) {
        var _this = this;
        // token specific header, will need to be altered for usage
        /** @type {?} */
        var hdr = {
            'Authorization': 'Basic ' + window.btoa(username + ':' + password)
        };
        return _super.prototype.get.call(this, this.loginPath, hdr)
            .pipe(tap((/**
         * @param {?} res
         * @return {?}
         */
        function (res) {
            if (res) {
                _this.userInfo = res;
            }
        })), catchError((/**
         * @return {?}
         */
        function () { return of(null); })));
    };
    /**
     * @return {?}
     */
    AuthService.prototype.logout = /**
     * @return {?}
     */
    function () {
        this.userInfo = null;
        this.router.navigate(['/login']);
        return this.post(this.logoutPath, null);
    };
    /**
     * Called when a 401 status is encountered which means user is logged out
     */
    /**
     * Called when a 401 status is encountered which means user is logged out
     * @return {?}
     */
    AuthService.prototype.on401Error = /**
     * Called when a 401 status is encountered which means user is logged out
     * @return {?}
     */
    function () {
        this.userInfo = null;
        this.router.navigate(['/login']);
    };
    /**
     * @return {?}
     */
    AuthService.prototype.getUser = /**
     * @return {?}
     */
    function () {
        // Check our cache
        if (this.userInfo) {
            return of(this.userInfo);
        }
        // Try to get it from the server
        return _super.prototype.get.call(this, this.loginPath);
    };
    // TODO: Make these permissions an Enum
    // TODO: Is this list complete?
    /* Permissions from legacy saphira
       CREATE,
       READ,
       UPDATE,
       DELETE,
       MSA,
       IMPORT_CSV, //Old
       SNAPSHOT, //Import
       EXPORT_FULL, //Exporting survey set only
       EXPORT_TRAJECTORY, //Exporting trajectory only
       EXPORT_MINIMAL, //Not used yet, kept for future use (surveyset export without msa sixaxis data?)
       METADATA,
       MWD_READER,
       ATTACHMENT,
       QC_PLOTS,
       QC_RAW,
       QC_CORR,
       QC_TOL,
       QC_INCAZI_CALC,
       QC_INCAZI_CORR,
       QC_VALIDATION_ERRORS,
       AUTOMATION,
       SHIFT_NOTES,
       CHAT,
       MODIFY_PERMISSIONS,
       ACTIVITY_LOG,
       DATA_VIEW,
       COORD_SYSTEM,
       NOTIFICATIONS,
       API_SURVEY_READ,
       API_SURVEY_SUBMIT;
    */
    // TODO: move into user model
    // TODO: this isn't quite correct
    // TODO: Make these permissions an Enum
    // TODO: Is this list complete?
    /* Permissions from legacy saphira
         CREATE,
         READ,
         UPDATE,
         DELETE,
         MSA,
         IMPORT_CSV, //Old
         SNAPSHOT, //Import
         EXPORT_FULL, //Exporting survey set only
         EXPORT_TRAJECTORY, //Exporting trajectory only
         EXPORT_MINIMAL, //Not used yet, kept for future use (surveyset export without msa sixaxis data?)
         METADATA,
         MWD_READER,
         ATTACHMENT,
         QC_PLOTS,
         QC_RAW,
         QC_CORR,
         QC_TOL,
         QC_INCAZI_CALC,
         QC_INCAZI_CORR,
         QC_VALIDATION_ERRORS,
         AUTOMATION,
         SHIFT_NOTES,
         CHAT,
         MODIFY_PERMISSIONS,
         ACTIVITY_LOG,
         DATA_VIEW,
         COORD_SYSTEM,
         NOTIFICATIONS,
         API_SURVEY_READ,
         API_SURVEY_SUBMIT;
      */
    // TODO: move into user model
    // TODO: this isn't quite correct
    /**
     * @param {?} perm2Match
     * @return {?}
     */
    AuthService.prototype.userhasPermission = 
    // TODO: Make these permissions an Enum
    // TODO: Is this list complete?
    /* Permissions from legacy saphira
         CREATE,
         READ,
         UPDATE,
         DELETE,
         MSA,
         IMPORT_CSV, //Old
         SNAPSHOT, //Import
         EXPORT_FULL, //Exporting survey set only
         EXPORT_TRAJECTORY, //Exporting trajectory only
         EXPORT_MINIMAL, //Not used yet, kept for future use (surveyset export without msa sixaxis data?)
         METADATA,
         MWD_READER,
         ATTACHMENT,
         QC_PLOTS,
         QC_RAW,
         QC_CORR,
         QC_TOL,
         QC_INCAZI_CALC,
         QC_INCAZI_CORR,
         QC_VALIDATION_ERRORS,
         AUTOMATION,
         SHIFT_NOTES,
         CHAT,
         MODIFY_PERMISSIONS,
         ACTIVITY_LOG,
         DATA_VIEW,
         COORD_SYSTEM,
         NOTIFICATIONS,
         API_SURVEY_READ,
         API_SURVEY_SUBMIT;
      */
    // TODO: move into user model
    // TODO: this isn't quite correct
    /**
     * @param {?} perm2Match
     * @return {?}
     */
    function (perm2Match) {
        /** @type {?} */
        var hasAuth = this.userInfo['authorities'].filter((/**
         * @param {?} auth
         * @return {?}
         */
        function (auth) {
            return auth['role'] === 'ROLE_MAGVAR_SUPERADMIN' ||
                auth['role'] === 'ROLE_crowd-administrators' ||
                auth['role'] === 'ROLE_MAGVAR_DEVELOPER' ||
                auth['role'].indexOf(perm2Match) > -1;
        }));
        return hasAuth.length > 0;
    };
    // TODO: move into model
    // TODO: move into model
    /**
     * @return {?}
     */
    AuthService.prototype.userIsCustomer = 
    // TODO: move into model
    /**
     * @return {?}
     */
    function () {
        /** @type {?} */
        var hasAuth = this.userInfo['authorities'].filter((/**
         * @param {?} auth
         * @return {?}
         */
        function (auth) {
            return auth['role'] === 'ROLE_Rig' || auth['role'] === 'ROLE_Operators';
        }));
        return hasAuth.length > 0;
    };
    AuthService.decorators = [
        { type: Injectable }
    ];
    /** @nocollapse */
    AuthService.ctorParameters = function () { return [
        { type: HttpClient },
        { type: Router }
    ]; };
    return AuthService;
}(DataService));
export { AuthService };
if (false) {
    /**
     * @type {?}
     * @private
     */
    AuthService.prototype.loginPath;
    /**
     * @type {?}
     * @private
     */
    AuthService.prototype.logoutPath;
    /**
     * @type {?}
     * @private
     */
    AuthService.prototype.userInfo;
    /**
     * @type {?}
     * @private
     */
    AuthService.prototype.router;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0aC5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGhwdGVjaC9zYXBoaXJhLXNoYXJlZC1saWIvIiwic291cmNlcyI6WyJsaWIvc2VydmljZXMvYXV0aC5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMzQyxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFDbEQsT0FBTyxFQUFFLE1BQU0sRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBRXpDLE9BQU8sRUFBYyxFQUFFLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFDdEMsT0FBTyxFQUFFLFVBQVUsRUFBRSxHQUFHLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUNqRCxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFFN0M7SUFDaUMsdUNBQVc7SUFNMUMscUJBQ0UsSUFBZ0IsRUFDUixNQUFjO1FBRnhCLFlBSUUsa0JBQU0sSUFBSSxDQUFDLFNBQ1o7UUFIUyxZQUFNLEdBQU4sTUFBTSxDQUFRO1FBUGhCLGVBQVMsR0FBRyxPQUFPLENBQUM7UUFDcEIsZ0JBQVUsR0FBRyxRQUFRLENBQUM7UUFFdEIsY0FBUSxHQUFRLElBQUksQ0FBQzs7SUFPN0IsQ0FBQzs7Ozs7O0lBRU0sMkJBQUs7Ozs7O0lBQVosVUFBYSxRQUFnQixFQUFFLFFBQWdCO1FBQS9DLGlCQWVDOzs7WUFiTyxHQUFHLEdBQUc7WUFDVixlQUFlLEVBQUUsUUFBUSxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsUUFBUSxHQUFHLEdBQUcsR0FBRyxRQUFRLENBQUM7U0FDbkU7UUFFRCxPQUFPLGlCQUFNLEdBQUcsWUFBQyxJQUFJLENBQUMsU0FBUyxFQUFFLEdBQUcsQ0FBQzthQUN4QixJQUFJLENBQ0gsR0FBRzs7OztRQUFDLFVBQUEsR0FBRztZQUNMLElBQUssR0FBRyxFQUFHO2dCQUNULEtBQUksQ0FBQyxRQUFRLEdBQUcsR0FBRyxDQUFDO2FBQ3JCO1FBQ0gsQ0FBQyxFQUFDLEVBQ0YsVUFBVTs7O1FBQUUsY0FBTSxPQUFBLEVBQUUsQ0FBQyxJQUFJLENBQUMsRUFBUixDQUFRLEVBQUUsQ0FDN0IsQ0FBQztJQUNoQixDQUFDOzs7O0lBRU0sNEJBQU07OztJQUFiO1FBQ0UsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7UUFDckIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO1FBQ2pDLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQzFDLENBQUM7SUFFRDs7T0FFRzs7Ozs7SUFDSSxnQ0FBVTs7OztJQUFqQjtRQUNFLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO1FBQ3JCLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztJQUNuQyxDQUFDOzs7O0lBRU0sNkJBQU87OztJQUFkO1FBQ0Usa0JBQWtCO1FBQ2xCLElBQUssSUFBSSxDQUFDLFFBQVEsRUFBRztZQUNuQixPQUFPLEVBQUUsQ0FBRSxJQUFJLENBQUMsUUFBUSxDQUFFLENBQUM7U0FDNUI7UUFFRCxnQ0FBZ0M7UUFDaEMsT0FBTyxpQkFBTSxHQUFHLFlBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO0lBQ25DLENBQUM7SUFFRCx1Q0FBdUM7SUFDdkMsK0JBQStCO0lBQy9COzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O01BK0JFO0lBRUYsNkJBQTZCO0lBQzdCLGlDQUFpQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7SUFDMUIsdUNBQWlCOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztJQUF4QixVQUF5QixVQUFrQjs7WUFDbkMsT0FBTyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLENBQUMsTUFBTTs7OztRQUFDLFVBQUMsSUFBSTtZQUN2RCxPQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyx3QkFBd0I7Z0JBQzlDLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSywyQkFBMkI7Z0JBQzVDLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyx1QkFBdUI7Z0JBQ3hDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7UUFDMUMsQ0FBQyxFQUFDO1FBQ0YsT0FBTyxPQUFPLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQztJQUM1QixDQUFDO0lBRUQsd0JBQXdCOzs7OztJQUNqQixvQ0FBYzs7Ozs7SUFBckI7O1lBQ1EsT0FBTyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLENBQUMsTUFBTTs7OztRQUFDLFVBQUMsSUFBSTtZQUN2RCxPQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxVQUFVLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLGdCQUFnQixDQUFDO1FBQzFFLENBQUMsRUFBQztRQUNGLE9BQU8sT0FBTyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUM7SUFDNUIsQ0FBQzs7Z0JBNUdGLFVBQVU7Ozs7Z0JBUEYsVUFBVTtnQkFDVixNQUFNOztJQW1IZixrQkFBQztDQUFBLEFBN0dELENBQ2lDLFdBQVcsR0E0RzNDO1NBNUdZLFdBQVc7Ozs7OztJQUN0QixnQ0FBNEI7Ozs7O0lBQzVCLGlDQUE4Qjs7Ozs7SUFFOUIsK0JBQTZCOzs7OztJQUkzQiw2QkFBc0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBIdHRwQ2xpZW50IH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xuaW1wb3J0IHsgUm91dGVyIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcblxuaW1wb3J0IHsgT2JzZXJ2YWJsZSwgb2YgfSBmcm9tICdyeGpzJztcbmltcG9ydCB7IGNhdGNoRXJyb3IsIHRhcCB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcbmltcG9ydCB7IERhdGFTZXJ2aWNlIH0gZnJvbSAnLi9kYXRhLnNlcnZpY2UnO1xuXG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgY2xhc3MgQXV0aFNlcnZpY2UgZXh0ZW5kcyBEYXRhU2VydmljZSB7XG4gIHByaXZhdGUgbG9naW5QYXRoID0gJ2xvZ2luJztcbiAgcHJpdmF0ZSBsb2dvdXRQYXRoID0gJ2xvZ291dCc7XG5cbiAgcHJpdmF0ZSB1c2VySW5mbzogYW55ID0gbnVsbDtcblxuICBjb25zdHJ1Y3RvcihcbiAgICBodHRwOiBIdHRwQ2xpZW50LFxuICAgIHByaXZhdGUgcm91dGVyOiBSb3V0ZXJcbiAgKSB7XG4gICAgc3VwZXIoaHR0cCk7XG4gIH1cblxuICBwdWJsaWMgbG9naW4odXNlcm5hbWU6IHN0cmluZywgcGFzc3dvcmQ6IHN0cmluZyk6IE9ic2VydmFibGU8YW55PiB7XG4gICAgLy8gdG9rZW4gc3BlY2lmaWMgaGVhZGVyLCB3aWxsIG5lZWQgdG8gYmUgYWx0ZXJlZCBmb3IgdXNhZ2VcbiAgICBjb25zdCBoZHIgPSB7XG4gICAgICAnQXV0aG9yaXphdGlvbic6ICdCYXNpYyAnICsgd2luZG93LmJ0b2EodXNlcm5hbWUgKyAnOicgKyBwYXNzd29yZClcbiAgICB9O1xuXG4gICAgcmV0dXJuIHN1cGVyLmdldCh0aGlzLmxvZ2luUGF0aCwgaGRyKVxuICAgICAgICAgICAgICAgIC5waXBlKFxuICAgICAgICAgICAgICAgICAgdGFwKHJlcyA9PiB7XG4gICAgICAgICAgICAgICAgICAgIGlmICggcmVzICkge1xuICAgICAgICAgICAgICAgICAgICAgIHRoaXMudXNlckluZm8gPSByZXM7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgIH0pLFxuICAgICAgICAgICAgICAgICAgY2F0Y2hFcnJvciggKCkgPT4gb2YobnVsbCkgKVxuICAgICAgICAgICAgICAgICk7XG4gIH1cblxuICBwdWJsaWMgbG9nb3V0KCk6IE9ic2VydmFibGU8YW55PiB7XG4gICAgdGhpcy51c2VySW5mbyA9IG51bGw7XG4gICAgdGhpcy5yb3V0ZXIubmF2aWdhdGUoWycvbG9naW4nXSk7XG4gICAgcmV0dXJuIHRoaXMucG9zdCh0aGlzLmxvZ291dFBhdGgsIG51bGwpO1xuICB9XG5cbiAgLyoqXG4gICAqIENhbGxlZCB3aGVuIGEgNDAxIHN0YXR1cyBpcyBlbmNvdW50ZXJlZCB3aGljaCBtZWFucyB1c2VyIGlzIGxvZ2dlZCBvdXRcbiAgICovXG4gIHB1YmxpYyBvbjQwMUVycm9yKCk6IHZvaWQge1xuICAgIHRoaXMudXNlckluZm8gPSBudWxsO1xuICAgIHRoaXMucm91dGVyLm5hdmlnYXRlKFsnL2xvZ2luJ10pO1xuICB9XG5cbiAgcHVibGljIGdldFVzZXIoKTogT2JzZXJ2YWJsZTxhbnk+IHtcbiAgICAvLyBDaGVjayBvdXIgY2FjaGVcbiAgICBpZiAoIHRoaXMudXNlckluZm8gKSB7XG4gICAgICByZXR1cm4gb2YoIHRoaXMudXNlckluZm8gKTtcbiAgICB9XG5cbiAgICAvLyBUcnkgdG8gZ2V0IGl0IGZyb20gdGhlIHNlcnZlclxuICAgIHJldHVybiBzdXBlci5nZXQodGhpcy5sb2dpblBhdGgpO1xuICB9XG5cbiAgLy8gVE9ETzogTWFrZSB0aGVzZSBwZXJtaXNzaW9ucyBhbiBFbnVtXG4gIC8vIFRPRE86IElzIHRoaXMgbGlzdCBjb21wbGV0ZT9cbiAgLyogUGVybWlzc2lvbnMgZnJvbSBsZWdhY3kgc2FwaGlyYVxuICAgICBDUkVBVEUsXG4gICAgIFJFQUQsXG4gICAgIFVQREFURSxcbiAgICAgREVMRVRFLFxuICAgICBNU0EsXG4gICAgIElNUE9SVF9DU1YsIC8vT2xkXG4gICAgIFNOQVBTSE9ULCAvL0ltcG9ydFxuICAgICBFWFBPUlRfRlVMTCwgLy9FeHBvcnRpbmcgc3VydmV5IHNldCBvbmx5XG4gICAgIEVYUE9SVF9UUkFKRUNUT1JZLCAvL0V4cG9ydGluZyB0cmFqZWN0b3J5IG9ubHlcbiAgICAgRVhQT1JUX01JTklNQUwsIC8vTm90IHVzZWQgeWV0LCBrZXB0IGZvciBmdXR1cmUgdXNlIChzdXJ2ZXlzZXQgZXhwb3J0IHdpdGhvdXQgbXNhIHNpeGF4aXMgZGF0YT8pXG4gICAgIE1FVEFEQVRBLFxuICAgICBNV0RfUkVBREVSLFxuICAgICBBVFRBQ0hNRU5ULFxuICAgICBRQ19QTE9UUyxcbiAgICAgUUNfUkFXLFxuICAgICBRQ19DT1JSLFxuICAgICBRQ19UT0wsXG4gICAgIFFDX0lOQ0FaSV9DQUxDLFxuICAgICBRQ19JTkNBWklfQ09SUixcbiAgICAgUUNfVkFMSURBVElPTl9FUlJPUlMsXG4gICAgIEFVVE9NQVRJT04sXG4gICAgIFNISUZUX05PVEVTLFxuICAgICBDSEFULFxuICAgICBNT0RJRllfUEVSTUlTU0lPTlMsXG4gICAgIEFDVElWSVRZX0xPRyxcbiAgICAgREFUQV9WSUVXLFxuICAgICBDT09SRF9TWVNURU0sXG4gICAgIE5PVElGSUNBVElPTlMsXG4gICAgIEFQSV9TVVJWRVlfUkVBRCxcbiAgICAgQVBJX1NVUlZFWV9TVUJNSVQ7XG4gICovXG5cbiAgLy8gVE9ETzogbW92ZSBpbnRvIHVzZXIgbW9kZWxcbiAgLy8gVE9ETzogdGhpcyBpc24ndCBxdWl0ZSBjb3JyZWN0XG4gIHB1YmxpYyB1c2VyaGFzUGVybWlzc2lvbihwZXJtMk1hdGNoOiBzdHJpbmcpOiBib29sZWFuIHtcbiAgICBjb25zdCBoYXNBdXRoID0gdGhpcy51c2VySW5mb1snYXV0aG9yaXRpZXMnXS5maWx0ZXIoKGF1dGgpID0+IHtcbiAgICAgIHJldHVybiBhdXRoWydyb2xlJ10gPT09ICdST0xFX01BR1ZBUl9TVVBFUkFETUlOJyB8fFxuICAgICAgICBhdXRoWydyb2xlJ10gPT09ICdST0xFX2Nyb3dkLWFkbWluaXN0cmF0b3JzJyB8fFxuICAgICAgICBhdXRoWydyb2xlJ10gPT09ICdST0xFX01BR1ZBUl9ERVZFTE9QRVInIHx8XG4gICAgICAgIGF1dGhbJ3JvbGUnXS5pbmRleE9mKHBlcm0yTWF0Y2gpID4gLTE7XG4gICAgfSk7XG4gICAgcmV0dXJuIGhhc0F1dGgubGVuZ3RoID4gMDtcbiAgfVxuXG4gIC8vIFRPRE86IG1vdmUgaW50byBtb2RlbFxuICBwdWJsaWMgdXNlcklzQ3VzdG9tZXIoKTogYm9vbGVhbiB7XG4gICAgY29uc3QgaGFzQXV0aCA9IHRoaXMudXNlckluZm9bJ2F1dGhvcml0aWVzJ10uZmlsdGVyKChhdXRoKSA9PiB7XG4gICAgICByZXR1cm4gYXV0aFsncm9sZSddID09PSAnUk9MRV9SaWcnIHx8IGF1dGhbJ3JvbGUnXSA9PT0gJ1JPTEVfT3BlcmF0b3JzJztcbiAgICB9KTtcbiAgICByZXR1cm4gaGFzQXV0aC5sZW5ndGggPiAwO1xuICB9XG59XG4iXX0=