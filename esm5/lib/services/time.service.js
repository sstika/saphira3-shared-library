/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import * as moment_ from 'moment';
import * as i0 from "@angular/core";
/** @type {?} */
var moment = moment_;
import 'moment-timezone';
import * as _ from 'lodash';
var TimeService = /** @class */ (function () {
    function TimeService() {
        this.getMomentFromInstant = (/**
         * @param {?} instant
         * @return {?}
         */
        function (instant) {
            // convert the nanos to seconds, add to seconds, and multiply by 1000 to get milliseconds,
            // required by the moment constructor.
            return moment(1000.0 * (instant.seconds + (instant.nanos / 1000000000.0)));
        });
        this.getTwelveHourMomentFromInstant = (/**
         * @param {?} instant
         * @return {?}
         */
        function (instant) {
            // convert the nanos to seconds, add to seconds, and multiply by 1000 to get milliseconds,
            // required by the moment constructor.
            return moment(1000.0 * (instant.seconds + (instant.nanos / 1000000000.0))).tz('UTC').format('MMM Do YYYY, h:mm:ss a');
        });
    }
    /**
     * @param {?} timestamp
     * @return {?}
     */
    TimeService.prototype.momentFromNow = /**
     * @param {?} timestamp
     * @return {?}
     */
    function (timestamp) {
        /** @type {?} */
        var now = moment();
        if (timestamp && moment.isMoment(timestamp)) {
            if (timestamp > now) {
                return 'Now';
            }
            else {
                return timestamp.fromNow();
            }
        }
        else {
            return 'Never';
        }
    };
    /**
     * @param {?} timestamp
     * @return {?}
     */
    TimeService.prototype.momentFromNowFormatter = /**
     * @param {?} timestamp
     * @return {?}
     */
    function (timestamp) {
        /** @type {?} */
        var now = moment();
        if (timestamp.value && moment.isMoment(timestamp.value)) {
            if (timestamp.value > now) {
                return 'Now';
            }
            else {
                return timestamp.value.fromNow();
            }
        }
        else {
            return 'Never';
        }
    };
    // format date
    // format date
    /**
     * @param {?} timestamp
     * @param {?} format
     * @return {?}
     */
    TimeService.prototype.standardDateFormatter = 
    // format date
    /**
     * @param {?} timestamp
     * @param {?} format
     * @return {?}
     */
    function (timestamp, format) {
        return moment(timestamp).format(format);
    };
    // parse date based on format provided
    // parse date based on format provided
    /**
     * @param {?} timestamp
     * @param {?} format
     * @return {?}
     */
    TimeService.prototype.convertDatetoMoment = 
    // parse date based on format provided
    /**
     * @param {?} timestamp
     * @param {?} format
     * @return {?}
     */
    function (timestamp, format) {
        return moment(timestamp, format);
    };
    // changed from date1/date2  to momentA/momentB resp, since its confusing
    // changed from date1/date2  to momentA/momentB resp, since its confusing
    /**
     * @param {?} momentA
     * @param {?} momentB
     * @return {?}
     */
    TimeService.prototype.dateComparator = 
    // changed from date1/date2  to momentA/momentB resp, since its confusing
    /**
     * @param {?} momentA
     * @param {?} momentB
     * @return {?}
     */
    function (momentA, momentB) {
        if (momentA && momentB) {
            if (momentA.isBefore(momentB)) {
                return 1;
            }
            else if (momentB.isBefore(momentA)) {
                return -1;
            }
            else {
                return 0;
            }
        }
        else if (momentA) {
            return -1;
        }
        else if (momentB) {
            return 1;
        }
        else {
            return 0;
        }
    };
    /**
     * @param {?} instant
     * @return {?}
     */
    TimeService.prototype.getUTCTimestampFromInstant = /**
     * @param {?} instant
     * @return {?}
     */
    function (instant) {
        // convert the nanos to seconds, add to seconds, and multiply by 1000 to get milliseconds,
        // required by the moment constructor.
        if (!instant) {
            return instant;
        }
        return moment.utc(1000.0 * (instant.seconds + (instant.nanos / 1000000000.0))).format('YYYY[-]MM[-]DD[T]HH[:]mm[:]ss.SSS[Z]');
    };
    /**
     * @return {?}
     */
    TimeService.prototype.getTimeZoneAbbr = /**
     * @return {?}
     */
    function () {
        /** @type {?} */
        var tz = {};
        tz['server'] = 'UTC';
        /** @type {?} */
        var tzClient = moment.tz(moment.tz.guess()).zoneAbbr();
        tz['client'] = (!_.isNull(tzClient) && !_.isUndefined(tzClient)) ? tzClient : 'Local TimeZone';
        return tz;
    };
    // convert to UTC
    // convert to UTC
    /**
     * @param {?} timestamp
     * @return {?}
     */
    TimeService.prototype.convertToUtc = 
    // convert to UTC
    /**
     * @param {?} timestamp
     * @return {?}
     */
    function (timestamp) {
        return moment.utc(timestamp);
    };
    // get current date in specified format
    // get current date in specified format
    /**
     * @param {?} format
     * @return {?}
     */
    TimeService.prototype.getCurrentDate = 
    // get current date in specified format
    /**
     * @param {?} format
     * @return {?}
     */
    function (format) {
        return moment().format(format);
    };
    TimeService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    TimeService.ctorParameters = function () { return []; };
    /** @nocollapse */ TimeService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function TimeService_Factory() { return new TimeService(); }, token: TimeService, providedIn: "root" });
    return TimeService;
}());
export { TimeService };
if (false) {
    /** @type {?} */
    TimeService.prototype.getMomentFromInstant;
    /** @type {?} */
    TimeService.prototype.getTwelveHourMomentFromInstant;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGltZS5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGhwdGVjaC9zYXBoaXJhLXNoYXJlZC1saWIvIiwic291cmNlcyI6WyJsaWIvc2VydmljZXMvdGltZS5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBRTNDLE9BQU8sS0FBSyxPQUFPLE1BQU0sUUFBUSxDQUFDOzs7SUFDNUIsTUFBTSxHQUFHLE9BQU87QUFDdEIsT0FBTyxpQkFBaUIsQ0FBQztBQUN6QixPQUFPLEtBQUssQ0FBQyxNQUFNLFFBQVEsQ0FBQztBQUc1QjtJQU9FO1FBRU8seUJBQW9COzs7O1FBQUcsVUFBQyxPQUFPO1lBQ3BDLDBGQUEwRjtZQUMxRixzQ0FBc0M7WUFDdEMsT0FBTyxNQUFNLENBQUMsTUFBTSxHQUFHLENBQUMsT0FBTyxDQUFDLE9BQU8sR0FBRyxDQUFDLE9BQU8sQ0FBQyxLQUFLLEdBQUcsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBRTdFLENBQUMsRUFBQTtRQUVNLG1DQUE4Qjs7OztRQUFHLFVBQUMsT0FBTztZQUM5QywwRkFBMEY7WUFDMUYsc0NBQXNDO1lBQ3RDLE9BQU8sTUFBTSxDQUFDLE1BQU0sR0FBRyxDQUFDLE9BQU8sQ0FBQyxPQUFPLEdBQUcsQ0FBQyxPQUFPLENBQUMsS0FBSyxHQUFHLFlBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLENBQUMsTUFBTSxDQUFDLHdCQUF3QixDQUFDLENBQUM7UUFFeEgsQ0FBQyxFQUFBO0lBZGUsQ0FBQzs7Ozs7SUFnQlYsbUNBQWE7Ozs7SUFBcEIsVUFBcUIsU0FBUzs7WUFDdEIsR0FBRyxHQUFHLE1BQU0sRUFBRTtRQUNwQixJQUFJLFNBQVMsSUFBSSxNQUFNLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxFQUFFO1lBQzNDLElBQUksU0FBUyxHQUFHLEdBQUcsRUFBRTtnQkFDbkIsT0FBTyxLQUFLLENBQUM7YUFDZDtpQkFBTTtnQkFDTCxPQUFPLFNBQVMsQ0FBQyxPQUFPLEVBQUUsQ0FBQzthQUM1QjtTQUNGO2FBQU07WUFDTCxPQUFPLE9BQU8sQ0FBQztTQUNoQjtJQUNILENBQUM7Ozs7O0lBRU0sNENBQXNCOzs7O0lBQTdCLFVBQThCLFNBQVM7O1lBQy9CLEdBQUcsR0FBRyxNQUFNLEVBQUU7UUFDcEIsSUFBSSxTQUFTLENBQUMsS0FBSyxJQUFJLE1BQU0sQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxFQUFFO1lBQ3ZELElBQUksU0FBUyxDQUFDLEtBQUssR0FBRyxHQUFHLEVBQUU7Z0JBQ3pCLE9BQU8sS0FBSyxDQUFDO2FBQ2Q7aUJBQU07Z0JBQ0wsT0FBTyxTQUFTLENBQUMsS0FBSyxDQUFDLE9BQU8sRUFBRSxDQUFDO2FBQ2xDO1NBQ0Y7YUFBTTtZQUNMLE9BQU8sT0FBTyxDQUFDO1NBQ2hCO0lBQ0gsQ0FBQztJQUVELGNBQWM7Ozs7Ozs7SUFDUCwyQ0FBcUI7Ozs7Ozs7SUFBNUIsVUFBNkIsU0FBUyxFQUFFLE1BQU07UUFDNUMsT0FBTyxNQUFNLENBQUMsU0FBUyxDQUFDLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQzFDLENBQUM7SUFFRCxzQ0FBc0M7Ozs7Ozs7SUFDL0IseUNBQW1COzs7Ozs7O0lBQTFCLFVBQTJCLFNBQVMsRUFBRSxNQUFNO1FBQzFDLE9BQU8sTUFBTSxDQUFDLFNBQVMsRUFBRSxNQUFNLENBQUMsQ0FBQztJQUNuQyxDQUFDO0lBRUQseUVBQXlFOzs7Ozs7O0lBQ2xFLG9DQUFjOzs7Ozs7O0lBQXJCLFVBQXNCLE9BQXVCLEVBQUUsT0FBdUI7UUFDcEUsSUFBSSxPQUFPLElBQUksT0FBTyxFQUFFO1lBQ3RCLElBQUksT0FBTyxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsRUFBRTtnQkFDN0IsT0FBTyxDQUFDLENBQUM7YUFDVjtpQkFBTSxJQUFJLE9BQU8sQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLEVBQUU7Z0JBQ3BDLE9BQU8sQ0FBQyxDQUFDLENBQUM7YUFDWDtpQkFBTTtnQkFDTCxPQUFPLENBQUMsQ0FBQzthQUNWO1NBQ0Y7YUFBTSxJQUFJLE9BQU8sRUFBRTtZQUNsQixPQUFPLENBQUMsQ0FBQyxDQUFDO1NBQ1g7YUFBTSxJQUFJLE9BQU8sRUFBRTtZQUNsQixPQUFPLENBQUMsQ0FBQztTQUNWO2FBQU07WUFDTCxPQUFPLENBQUMsQ0FBQztTQUNWO0lBQ0gsQ0FBQzs7Ozs7SUFFTSxnREFBMEI7Ozs7SUFBakMsVUFBa0MsT0FBTztRQUN2QywwRkFBMEY7UUFDMUYsc0NBQXNDO1FBQ3RDLElBQUksQ0FBQyxPQUFPLEVBQUU7WUFDWixPQUFPLE9BQU8sQ0FBQztTQUNoQjtRQUNELE9BQU8sTUFBTSxDQUFDLEdBQUcsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxPQUFPLENBQUMsT0FBTyxHQUFHLENBQUMsT0FBTyxDQUFDLEtBQUssR0FBRyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLHNDQUFzQyxDQUFDLENBQUM7SUFDaEksQ0FBQzs7OztJQUVNLHFDQUFlOzs7SUFBdEI7O1lBQ1EsRUFBRSxHQUFHLEVBQUU7UUFDYixFQUFFLENBQUMsUUFBUSxDQUFDLEdBQUcsS0FBSyxDQUFDOztZQUVmLFFBQVEsR0FBRyxNQUFNLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQyxRQUFRLEVBQUU7UUFDeEQsRUFBRSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLGdCQUFnQixDQUFDO1FBQy9GLE9BQU8sRUFBRSxDQUFDO0lBQ1osQ0FBQztJQUVELGlCQUFpQjs7Ozs7O0lBQ1Ysa0NBQVk7Ozs7OztJQUFuQixVQUFvQixTQUFTO1FBQzNCLE9BQU8sTUFBTSxDQUFDLEdBQUcsQ0FBQyxTQUFTLENBQUMsQ0FBQztJQUMvQixDQUFDO0lBRUQsdUNBQXVDOzs7Ozs7SUFDaEMsb0NBQWM7Ozs7OztJQUFyQixVQUFzQixNQUFNO1FBQzFCLE9BQU8sTUFBTSxFQUFFLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQ2pDLENBQUM7O2dCQXhHRixVQUFVLFNBQUM7b0JBQ1YsVUFBVSxFQUFFLE1BQU07aUJBQ25COzs7OztzQkFWRDtDQWlIQyxBQXpHRCxJQXlHQztTQXRHWSxXQUFXOzs7SUFNdEIsMkNBS0M7O0lBRUQscURBS0MiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbmltcG9ydCAqIGFzIG1vbWVudF8gZnJvbSAnbW9tZW50JztcbmNvbnN0IG1vbWVudCA9IG1vbWVudF87XG5pbXBvcnQgJ21vbWVudC10aW1lem9uZSc7XG5pbXBvcnQgKiBhcyBfIGZyb20gJ2xvZGFzaCc7XG5cblxuQEluamVjdGFibGUoe1xuICBwcm92aWRlZEluOiAncm9vdCdcbn0pXG5leHBvcnQgY2xhc3MgVGltZVNlcnZpY2Uge1xuXG5cblxuICBjb25zdHJ1Y3RvcigpIHsgfVxuXG4gIHB1YmxpYyBnZXRNb21lbnRGcm9tSW5zdGFudCA9IChpbnN0YW50KSA9PiB7XG4gICAgLy8gY29udmVydCB0aGUgbmFub3MgdG8gc2Vjb25kcywgYWRkIHRvIHNlY29uZHMsIGFuZCBtdWx0aXBseSBieSAxMDAwIHRvIGdldCBtaWxsaXNlY29uZHMsXG4gICAgLy8gcmVxdWlyZWQgYnkgdGhlIG1vbWVudCBjb25zdHJ1Y3Rvci5cbiAgICByZXR1cm4gbW9tZW50KDEwMDAuMCAqIChpbnN0YW50LnNlY29uZHMgKyAoaW5zdGFudC5uYW5vcyAvIDEwMDAwMDAwMDAuMCkpKTtcblxuICB9XG5cbiAgcHVibGljIGdldFR3ZWx2ZUhvdXJNb21lbnRGcm9tSW5zdGFudCA9IChpbnN0YW50KSA9PiB7XG4gICAgLy8gY29udmVydCB0aGUgbmFub3MgdG8gc2Vjb25kcywgYWRkIHRvIHNlY29uZHMsIGFuZCBtdWx0aXBseSBieSAxMDAwIHRvIGdldCBtaWxsaXNlY29uZHMsXG4gICAgLy8gcmVxdWlyZWQgYnkgdGhlIG1vbWVudCBjb25zdHJ1Y3Rvci5cbiAgICByZXR1cm4gbW9tZW50KDEwMDAuMCAqIChpbnN0YW50LnNlY29uZHMgKyAoaW5zdGFudC5uYW5vcyAvIDEwMDAwMDAwMDAuMCkpKS50eignVVRDJykuZm9ybWF0KCdNTU0gRG8gWVlZWSwgaDptbTpzcyBhJyk7XG5cbiAgfVxuXG4gIHB1YmxpYyBtb21lbnRGcm9tTm93KHRpbWVzdGFtcCkge1xuICAgIGNvbnN0IG5vdyA9IG1vbWVudCgpO1xuICAgIGlmICh0aW1lc3RhbXAgJiYgbW9tZW50LmlzTW9tZW50KHRpbWVzdGFtcCkpIHtcbiAgICAgIGlmICh0aW1lc3RhbXAgPiBub3cpIHtcbiAgICAgICAgcmV0dXJuICdOb3cnO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgcmV0dXJuIHRpbWVzdGFtcC5mcm9tTm93KCk7XG4gICAgICB9XG4gICAgfSBlbHNlIHtcbiAgICAgIHJldHVybiAnTmV2ZXInO1xuICAgIH1cbiAgfVxuXG4gIHB1YmxpYyBtb21lbnRGcm9tTm93Rm9ybWF0dGVyKHRpbWVzdGFtcCkge1xuICAgIGNvbnN0IG5vdyA9IG1vbWVudCgpO1xuICAgIGlmICh0aW1lc3RhbXAudmFsdWUgJiYgbW9tZW50LmlzTW9tZW50KHRpbWVzdGFtcC52YWx1ZSkpIHtcbiAgICAgIGlmICh0aW1lc3RhbXAudmFsdWUgPiBub3cpIHtcbiAgICAgICAgcmV0dXJuICdOb3cnO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgcmV0dXJuIHRpbWVzdGFtcC52YWx1ZS5mcm9tTm93KCk7XG4gICAgICB9XG4gICAgfSBlbHNlIHtcbiAgICAgIHJldHVybiAnTmV2ZXInO1xuICAgIH1cbiAgfVxuXG4gIC8vIGZvcm1hdCBkYXRlXG4gIHB1YmxpYyBzdGFuZGFyZERhdGVGb3JtYXR0ZXIodGltZXN0YW1wLCBmb3JtYXQpIHtcbiAgICByZXR1cm4gbW9tZW50KHRpbWVzdGFtcCkuZm9ybWF0KGZvcm1hdCk7XG4gIH1cblxuICAvLyBwYXJzZSBkYXRlIGJhc2VkIG9uIGZvcm1hdCBwcm92aWRlZFxuICBwdWJsaWMgY29udmVydERhdGV0b01vbWVudCh0aW1lc3RhbXAsIGZvcm1hdCk6IG1vbWVudF8uTW9tZW50IHtcbiAgICByZXR1cm4gbW9tZW50KHRpbWVzdGFtcCwgZm9ybWF0KTtcbiAgfVxuXG4gIC8vIGNoYW5nZWQgZnJvbSBkYXRlMS9kYXRlMiAgdG8gbW9tZW50QS9tb21lbnRCIHJlc3AsIHNpbmNlIGl0cyBjb25mdXNpbmdcbiAgcHVibGljIGRhdGVDb21wYXJhdG9yKG1vbWVudEE6IG1vbWVudF8uTW9tZW50LCBtb21lbnRCOiBtb21lbnRfLk1vbWVudCk6IG51bWJlciB7XG4gICAgaWYgKG1vbWVudEEgJiYgbW9tZW50Qikge1xuICAgICAgaWYgKG1vbWVudEEuaXNCZWZvcmUobW9tZW50QikpIHtcbiAgICAgICAgcmV0dXJuIDE7XG4gICAgICB9IGVsc2UgaWYgKG1vbWVudEIuaXNCZWZvcmUobW9tZW50QSkpIHtcbiAgICAgICAgcmV0dXJuIC0xO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgcmV0dXJuIDA7XG4gICAgICB9XG4gICAgfSBlbHNlIGlmIChtb21lbnRBKSB7XG4gICAgICByZXR1cm4gLTE7XG4gICAgfSBlbHNlIGlmIChtb21lbnRCKSB7XG4gICAgICByZXR1cm4gMTtcbiAgICB9IGVsc2Uge1xuICAgICAgcmV0dXJuIDA7XG4gICAgfVxuICB9XG5cbiAgcHVibGljIGdldFVUQ1RpbWVzdGFtcEZyb21JbnN0YW50KGluc3RhbnQpIHtcbiAgICAvLyBjb252ZXJ0IHRoZSBuYW5vcyB0byBzZWNvbmRzLCBhZGQgdG8gc2Vjb25kcywgYW5kIG11bHRpcGx5IGJ5IDEwMDAgdG8gZ2V0IG1pbGxpc2Vjb25kcyxcbiAgICAvLyByZXF1aXJlZCBieSB0aGUgbW9tZW50IGNvbnN0cnVjdG9yLlxuICAgIGlmICghaW5zdGFudCkge1xuICAgICAgcmV0dXJuIGluc3RhbnQ7XG4gICAgfVxuICAgIHJldHVybiBtb21lbnQudXRjKDEwMDAuMCAqIChpbnN0YW50LnNlY29uZHMgKyAoaW5zdGFudC5uYW5vcyAvIDEwMDAwMDAwMDAuMCkpKS5mb3JtYXQoJ1lZWVlbLV1NTVstXUREW1RdSEhbOl1tbVs6XXNzLlNTU1taXScpO1xuICB9XG5cbiAgcHVibGljIGdldFRpbWVab25lQWJicigpIHtcbiAgICBjb25zdCB0eiA9IHt9O1xuICAgIHR6WydzZXJ2ZXInXSA9ICdVVEMnO1xuXG4gICAgY29uc3QgdHpDbGllbnQgPSBtb21lbnQudHoobW9tZW50LnR6Lmd1ZXNzKCkpLnpvbmVBYmJyKCk7XG4gICAgdHpbJ2NsaWVudCddID0gKCFfLmlzTnVsbCh0ekNsaWVudCkgJiYgIV8uaXNVbmRlZmluZWQodHpDbGllbnQpKSA/IHR6Q2xpZW50IDogJ0xvY2FsIFRpbWVab25lJztcbiAgICByZXR1cm4gdHo7XG4gIH1cblxuICAvLyBjb252ZXJ0IHRvIFVUQ1xuICBwdWJsaWMgY29udmVydFRvVXRjKHRpbWVzdGFtcCk6IG1vbWVudF8uTW9tZW50IHtcbiAgICByZXR1cm4gbW9tZW50LnV0Yyh0aW1lc3RhbXApO1xuICB9XG5cbiAgLy8gZ2V0IGN1cnJlbnQgZGF0ZSBpbiBzcGVjaWZpZWQgZm9ybWF0XG4gIHB1YmxpYyBnZXRDdXJyZW50RGF0ZShmb3JtYXQpIHtcbiAgICByZXR1cm4gbW9tZW50KCkuZm9ybWF0KGZvcm1hdCk7XG4gIH1cbn1cbiJdfQ==