/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { of, BehaviorSubject } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { DataService } from './data.service';
import { HttpClient } from '@angular/common/http';
var ClientRequisitesService = /** @class */ (function (_super) {
    tslib_1.__extends(ClientRequisitesService, _super);
    /*
    * Constructor
    */
    function ClientRequisitesService(http) {
        var _this = _super.call(this, http) || this;
        _this.clientRequisitesPath = 'required';
        _this.clientRequisites = new BehaviorSubject({});
        _this.getClientReqs = _this.clientRequisites.asObservable();
        return _this;
    }
    /*
    * Get the client requisites. Only loaded once for now.
    * TODO implement some timeout on the cache so requisites are reloaded once every 10 minutes or something idk
    */
    /*
      * Get the client requisites. Only loaded once for now.
      * TODO implement some timeout on the cache so requisites are reloaded once every 10 minutes or something idk
      */
    /**
     * @param {?=} reload
     * @return {?}
     */
    ClientRequisitesService.prototype.getClientRequisites = /*
      * Get the client requisites. Only loaded once for now.
      * TODO implement some timeout on the cache so requisites are reloaded once every 10 minutes or something idk
      */
    /**
     * @param {?=} reload
     * @return {?}
     */
    function (reload) {
        var _this = this;
        if (!this.cachedData || reload) {
            return _super.prototype.get.call(this, "" + this.clientRequisitesPath).pipe(tap((/**
             * @param {?} requisites
             * @return {?}
             */
            function (requisites) { return _this.cachedData = requisites; })), catchError((/**
             * @param {?} err
             * @return {?}
             */
            function (err) { return of(false); })));
        }
        else {
            return of(this.cachedData);
        }
    };
    /*
    * Set the client requisites
    */
    /*
      * Set the client requisites
      */
    /**
     * @param {?} data
     * @return {?}
     */
    ClientRequisitesService.prototype.setClientRequisitesService = /*
      * Set the client requisites
      */
    /**
     * @param {?} data
     * @return {?}
     */
    function (data) {
        this.clientRequisites.next(data);
        this.cachedData = data;
    };
    /*
    * Return the cached field
    */
    /*
      * Return the cached field
      */
    /**
     * @param {?} requestedField
     * @return {?}
     */
    ClientRequisitesService.prototype.returnCachedField = /*
      * Return the cached field
      */
    /**
     * @param {?} requestedField
     * @return {?}
     */
    function (requestedField) {
        return this.cachedData[requestedField];
    };
    /*
    * Return all the cached data
    */
    /*
      * Return all the cached data
      */
    /**
     * @return {?}
     */
    ClientRequisitesService.prototype.returnAllCached = /*
      * Return all the cached data
      */
    /**
     * @return {?}
     */
    function () {
        return of(this.cachedData);
    };
    ClientRequisitesService.decorators = [
        { type: Injectable }
    ];
    /** @nocollapse */
    ClientRequisitesService.ctorParameters = function () { return [
        { type: HttpClient }
    ]; };
    return ClientRequisitesService;
}(DataService));
export { ClientRequisitesService };
if (false) {
    /**
     * @type {?}
     * @protected
     */
    ClientRequisitesService.prototype.clientRequisitesPath;
    /** @type {?} */
    ClientRequisitesService.prototype.cachedData;
    /** @type {?} */
    ClientRequisitesService.prototype.clientRequisites;
    /** @type {?} */
    ClientRequisitesService.prototype.getClientReqs;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2xpZW50LXJlcXVpc2l0ZXMuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BocHRlY2gvc2FwaGlyYS1zaGFyZWQtbGliLyIsInNvdXJjZXMiOlsibGliL3NlcnZpY2VzL2NsaWVudC1yZXF1aXNpdGVzLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxPQUFPLEVBQWMsRUFBRSxFQUFFLGVBQWUsRUFBZ0IsTUFBTSxNQUFNLENBQUM7QUFFckUsT0FBTyxFQUFFLFVBQVUsRUFBRSxHQUFHLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUNqRCxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUM3QyxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFFbEQ7SUFDNkMsbURBQVc7SUFPdEQ7O01BRUU7SUFDRixpQ0FBWSxJQUFnQjtRQUE1QixZQUNFLGtCQUFNLElBQUksQ0FBQyxTQUNaO1FBWFMsMEJBQW9CLEdBQUcsVUFBVSxDQUFDO1FBR3JDLHNCQUFnQixHQUFHLElBQUksZUFBZSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1FBQ2xELG1CQUFhLEdBQUcsS0FBSSxDQUFDLGdCQUFnQixDQUFDLFlBQVksRUFBRSxDQUFDOztJQU9yRCxDQUFDO0lBRUQ7OztNQUdFOzs7Ozs7Ozs7SUFDSyxxREFBbUI7Ozs7Ozs7O0lBQTFCLFVBQTJCLE1BQWdCO1FBQTNDLGlCQVNDO1FBUkMsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLElBQUksTUFBTSxFQUFFO1lBQzlCLE9BQU8saUJBQ0osR0FBRyxZQUFDLEtBQUcsSUFBSSxDQUFDLG9CQUFzQixDQUFDLENBQUMsSUFBSSxDQUN2QyxHQUFHOzs7O1lBQUMsVUFBQSxVQUFVLElBQUksT0FBQSxLQUFJLENBQUMsVUFBVSxHQUFHLFVBQVUsRUFBNUIsQ0FBNEIsRUFBQyxFQUMvQyxVQUFVOzs7O1lBQUMsVUFBQSxHQUFHLElBQUksT0FBQSxFQUFFLENBQUMsS0FBSyxDQUFDLEVBQVQsQ0FBUyxFQUFDLENBQUMsQ0FBQztTQUNuQzthQUFNO1lBQ0wsT0FBTyxFQUFFLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1NBQzVCO0lBQ0gsQ0FBQztJQUVEOztNQUVFOzs7Ozs7OztJQUNLLDREQUEwQjs7Ozs7OztJQUFqQyxVQUFrQyxJQUFZO1FBQzVDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDakMsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7SUFDekIsQ0FBQztJQUVEOztNQUVFOzs7Ozs7OztJQUNLLG1EQUFpQjs7Ozs7OztJQUF4QixVQUF5QixjQUFzQjtRQUM3QyxPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsY0FBYyxDQUFDLENBQUM7SUFDekMsQ0FBQztJQUVEOztNQUVFOzs7Ozs7O0lBQ0ssaURBQWU7Ozs7OztJQUF0QjtRQUNFLE9BQU8sRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztJQUM3QixDQUFDOztnQkFsREYsVUFBVTs7OztnQkFGRixVQUFVOztJQXFEbkIsOEJBQUM7Q0FBQSxBQW5ERCxDQUM2QyxXQUFXLEdBa0R2RDtTQWxEWSx1QkFBdUI7Ozs7OztJQUNsQyx1REFBNEM7O0lBQzVDLDZDQUF1Qjs7SUFFdkIsbURBQWtEOztJQUNsRCxnREFBcUQiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBPYnNlcnZhYmxlLCBvZiwgQmVoYXZpb3JTdWJqZWN0LCBTdWJzY3JpcHRpb24gfSBmcm9tICdyeGpzJztcblxuaW1wb3J0IHsgY2F0Y2hFcnJvciwgdGFwIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xuaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgRGF0YVNlcnZpY2UgfSBmcm9tICcuL2RhdGEuc2VydmljZSc7XG5pbXBvcnQgeyBIdHRwQ2xpZW50IH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xuXG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgY2xhc3MgQ2xpZW50UmVxdWlzaXRlc1NlcnZpY2UgZXh0ZW5kcyBEYXRhU2VydmljZSB7XG4gIHByb3RlY3RlZCBjbGllbnRSZXF1aXNpdGVzUGF0aCA9ICdyZXF1aXJlZCc7XG4gIHB1YmxpYyBjYWNoZWREYXRhOiBhbnk7XG5cbiAgcHVibGljIGNsaWVudFJlcXVpc2l0ZXMgPSBuZXcgQmVoYXZpb3JTdWJqZWN0KHt9KTtcbiAgZ2V0Q2xpZW50UmVxcyA9IHRoaXMuY2xpZW50UmVxdWlzaXRlcy5hc09ic2VydmFibGUoKTtcblxuICAvKlxuICAqIENvbnN0cnVjdG9yXG4gICovXG4gIGNvbnN0cnVjdG9yKGh0dHA6IEh0dHBDbGllbnQpIHtcbiAgICBzdXBlcihodHRwKTtcbiAgfVxuXG4gIC8qXG4gICogR2V0IHRoZSBjbGllbnQgcmVxdWlzaXRlcy4gT25seSBsb2FkZWQgb25jZSBmb3Igbm93LlxuICAqIFRPRE8gaW1wbGVtZW50IHNvbWUgdGltZW91dCBvbiB0aGUgY2FjaGUgc28gcmVxdWlzaXRlcyBhcmUgcmVsb2FkZWQgb25jZSBldmVyeSAxMCBtaW51dGVzIG9yIHNvbWV0aGluZyBpZGtcbiAgKi9cbiAgcHVibGljIGdldENsaWVudFJlcXVpc2l0ZXMocmVsb2FkPzogYm9vbGVhbik6IE9ic2VydmFibGU8YW55PiB7XG4gICAgaWYgKCF0aGlzLmNhY2hlZERhdGEgfHwgcmVsb2FkKSB7XG4gICAgICByZXR1cm4gc3VwZXJcbiAgICAgICAgLmdldChgJHt0aGlzLmNsaWVudFJlcXVpc2l0ZXNQYXRofWApLnBpcGUoXG4gICAgICAgICAgdGFwKHJlcXVpc2l0ZXMgPT4gdGhpcy5jYWNoZWREYXRhID0gcmVxdWlzaXRlcyksXG4gICAgICAgICAgY2F0Y2hFcnJvcihlcnIgPT4gb2YoZmFsc2UpKSk7XG4gICAgfSBlbHNlIHtcbiAgICAgIHJldHVybiBvZih0aGlzLmNhY2hlZERhdGEpO1xuICAgIH1cbiAgfVxuXG4gIC8qXG4gICogU2V0IHRoZSBjbGllbnQgcmVxdWlzaXRlc1xuICAqL1xuICBwdWJsaWMgc2V0Q2xpZW50UmVxdWlzaXRlc1NlcnZpY2UoZGF0YTogb2JqZWN0KTogdm9pZCB7XG4gICAgdGhpcy5jbGllbnRSZXF1aXNpdGVzLm5leHQoZGF0YSk7XG4gICAgdGhpcy5jYWNoZWREYXRhID0gZGF0YTtcbiAgfVxuXG4gIC8qXG4gICogUmV0dXJuIHRoZSBjYWNoZWQgZmllbGRcbiAgKi9cbiAgcHVibGljIHJldHVybkNhY2hlZEZpZWxkKHJlcXVlc3RlZEZpZWxkOiBzdHJpbmcpOiBvYmplY3Qge1xuICAgIHJldHVybiB0aGlzLmNhY2hlZERhdGFbcmVxdWVzdGVkRmllbGRdO1xuICB9XG5cbiAgLypcbiAgKiBSZXR1cm4gYWxsIHRoZSBjYWNoZWQgZGF0YVxuICAqL1xuICBwdWJsaWMgcmV0dXJuQWxsQ2FjaGVkKCk6IE9ic2VydmFibGU8YW55PiB7XG4gICAgcmV0dXJuIG9mKHRoaXMuY2FjaGVkRGF0YSk7XG4gIH1cbn1cbiJdfQ==