/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import * as i0 from "@angular/core";
/**
 * Opens new browser tabs.
 * Workaround for Chrome.
 * @see https://blog.chromium.org/2009/12/links-that-open-in-new-processes.html
 * @see https://stackoverflow.com/a/40909198
 */
var BrowserTabService = /** @class */ (function () {
    function BrowserTabService() {
    }
    Object.defineProperty(BrowserTabService.prototype, "anchorTag", {
        set: /**
         * @param {?} tag
         * @return {?}
         */
        function (tag) {
            if (!tag) {
                return;
            }
            this.tag = tag;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * Opens a new browser tab
     */
    /**
     * Opens a new browser tab
     * @param {?} url
     * @return {?}
     */
    BrowserTabService.prototype.open = /**
     * Opens a new browser tab
     * @param {?} url
     * @return {?}
     */
    function (url) {
        if (!url) {
            return;
        }
        if (!this.tag) {
            return;
        }
        this.tag.href = url;
        this.tag.click();
    };
    /**
     * Opens Wellbore Detail in a new browser tab
     */
    /**
     * Opens Wellbore Detail in a new browser tab
     * @param {?} wellboreId
     * @return {?}
     */
    BrowserTabService.prototype.openWellboreDetail = /**
     * Opens Wellbore Detail in a new browser tab
     * @param {?} wellboreId
     * @return {?}
     */
    function (wellboreId) {
        if (!wellboreId) {
            return;
        }
        /** @type {?} */
        var url = window.location.origin
            + window.location.pathname
            + '#/wellbore-detail/'
            + wellboreId;
        this.open(url);
    };
    BrowserTabService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    BrowserTabService.ctorParameters = function () { return []; };
    /** @nocollapse */ BrowserTabService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function BrowserTabService_Factory() { return new BrowserTabService(); }, token: BrowserTabService, providedIn: "root" });
    return BrowserTabService;
}());
export { BrowserTabService };
if (false) {
    /**
     * @type {?}
     * @private
     */
    BrowserTabService.prototype.tag;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYnJvd3Nlci10YWIuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BocHRlY2gvc2FwaGlyYS1zaGFyZWQtbGliLyIsInNvdXJjZXMiOlsibGliL3NlcnZpY2VzL2Jyb3dzZXItdGFiLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7Ozs7Ozs7O0FBTzNDO0lBYUU7SUFBZ0IsQ0FBQztJQVJqQixzQkFBSSx3Q0FBUzs7Ozs7UUFBYixVQUFlLEdBQXdDO1lBQ3JELElBQUssQ0FBQyxHQUFHLEVBQUc7Z0JBQ1YsT0FBTzthQUNSO1lBRUQsSUFBSSxDQUFDLEdBQUcsR0FBRyxHQUFHLENBQUM7UUFDakIsQ0FBQzs7O09BQUE7SUFJRDs7T0FFRzs7Ozs7O0lBQ0ksZ0NBQUk7Ozs7O0lBQVgsVUFBWSxHQUFXO1FBQ3JCLElBQUssQ0FBQyxHQUFHLEVBQUc7WUFDVixPQUFPO1NBQ1I7UUFDRCxJQUFLLENBQUMsSUFBSSxDQUFDLEdBQUcsRUFBRztZQUNmLE9BQU87U0FDUjtRQUVELElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxHQUFHLEdBQUcsQ0FBQztRQUNwQixJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssRUFBRSxDQUFDO0lBQ25CLENBQUM7SUFFRDs7T0FFRzs7Ozs7O0lBQ0ksOENBQWtCOzs7OztJQUF6QixVQUEwQixVQUFrQjtRQUMxQyxJQUFLLENBQUMsVUFBVSxFQUFHO1lBQ2pCLE9BQU87U0FDUjs7WUFFSyxHQUFHLEdBQUcsTUFBTSxDQUFDLFFBQVEsQ0FBQyxNQUFNO2NBQ3RCLE1BQU0sQ0FBQyxRQUFRLENBQUMsUUFBUTtjQUN4QixvQkFBb0I7Y0FDcEIsVUFBVTtRQUN0QixJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQ2pCLENBQUM7O2dCQTNDRixVQUFVLFNBQUM7b0JBQ1YsVUFBVSxFQUFFLE1BQU07aUJBQ25COzs7Ozs0QkFURDtDQW1EQyxBQTVDRCxJQTRDQztTQXpDWSxpQkFBaUI7Ozs7OztJQUM1QixnQ0FBaUQiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG4vKipcbiAqIE9wZW5zIG5ldyBicm93c2VyIHRhYnMuXG4gKiBXb3JrYXJvdW5kIGZvciBDaHJvbWUuXG4gKiBAc2VlIGh0dHBzOi8vYmxvZy5jaHJvbWl1bS5vcmcvMjAwOS8xMi9saW5rcy10aGF0LW9wZW4taW4tbmV3LXByb2Nlc3Nlcy5odG1sXG4gKiBAc2VlIGh0dHBzOi8vc3RhY2tvdmVyZmxvdy5jb20vYS80MDkwOTE5OFxuICovXG5ASW5qZWN0YWJsZSh7XG4gIHByb3ZpZGVkSW46ICdyb290J1xufSlcbmV4cG9ydCBjbGFzcyBCcm93c2VyVGFiU2VydmljZSB7XG4gIHByaXZhdGUgdGFnOiB7IGhyZWY6IHN0cmluZywgY2xpY2s6ICgpID0+IHZvaWQgfTtcbiAgc2V0IGFuY2hvclRhZyggdGFnOiB7IGhyZWY6IHN0cmluZywgY2xpY2s6ICgpID0+IHZvaWQgfSApIHtcbiAgICBpZiAoICF0YWcgKSB7XG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgdGhpcy50YWcgPSB0YWc7XG4gIH1cblxuICBjb25zdHJ1Y3RvcigpIHsgfVxuXG4gIC8qKlxuICAgKiBPcGVucyBhIG5ldyBicm93c2VyIHRhYlxuICAgKi9cbiAgcHVibGljIG9wZW4odXJsOiBzdHJpbmcpOiB2b2lkIHtcbiAgICBpZiAoICF1cmwgKSB7XG4gICAgICByZXR1cm47XG4gICAgfVxuICAgIGlmICggIXRoaXMudGFnICkge1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIHRoaXMudGFnLmhyZWYgPSB1cmw7XG4gICAgdGhpcy50YWcuY2xpY2soKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBPcGVucyBXZWxsYm9yZSBEZXRhaWwgaW4gYSBuZXcgYnJvd3NlciB0YWJcbiAgICovXG4gIHB1YmxpYyBvcGVuV2VsbGJvcmVEZXRhaWwod2VsbGJvcmVJZDogc3RyaW5nKTogdm9pZCB7XG4gICAgaWYgKCAhd2VsbGJvcmVJZCApIHtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICBjb25zdCB1cmwgPSB3aW5kb3cubG9jYXRpb24ub3JpZ2luXG4gICAgICAgICAgICAgICsgd2luZG93LmxvY2F0aW9uLnBhdGhuYW1lXG4gICAgICAgICAgICAgICsgJyMvd2VsbGJvcmUtZGV0YWlsLydcbiAgICAgICAgICAgICAgKyB3ZWxsYm9yZUlkO1xuICAgIHRoaXMub3Blbih1cmwpO1xuICB9XG59XG4iXX0=