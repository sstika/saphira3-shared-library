/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { timer } from 'rxjs';
import * as i0 from "@angular/core";
import * as i1 from "@angular/platform-browser";
var TitleFlasherService = /** @class */ (function () {
    function TitleFlasherService(titleService) {
        this.titleService = titleService;
    }
    // Method to start flashing document title
    // Interval: interval of flash
    // String to flash in browser title
    // Method to start flashing document title
    // Interval: interval of flash
    // String to flash in browser title
    /**
     * @param {?} interval
     * @param {?} titleToFlash
     * @return {?}
     */
    TitleFlasherService.prototype.startFlashing = 
    // Method to start flashing document title
    // Interval: interval of flash
    // String to flash in browser title
    /**
     * @param {?} interval
     * @param {?} titleToFlash
     * @return {?}
     */
    function (interval, titleToFlash) {
        var _this = this;
        if (!this.isAlreadyFlashingTitle) {
            this.prevTitle = this.titleService.getTitle();
            this.documentTitleFlash = timer(0, interval);
            this.documentTitleFlashSubscription = this.documentTitleFlash.subscribe((/**
             * @param {?} data
             * @return {?}
             */
            function (data) {
                if (data % 2 === 0) {
                    _this.setTitle(titleToFlash);
                }
                else {
                    _this.setTitle(_this.prevTitle);
                }
            }));
            this.isAlreadyFlashingTitle = true;
        }
    };
    /**
     * @return {?}
     */
    TitleFlasherService.prototype.stopFlashing = /**
     * @return {?}
     */
    function () {
        if (this.isAlreadyFlashingTitle) {
            this.documentTitleFlashSubscription.unsubscribe();
            this.setTitle(this.prevTitle);
            this.isAlreadyFlashingTitle = false;
        }
    };
    /**
     * @param {?} title
     * @return {?}
     */
    TitleFlasherService.prototype.setTitle = /**
     * @param {?} title
     * @return {?}
     */
    function (title) {
        this.titleService.setTitle(title);
    };
    /**
     * @return {?}
     */
    TitleFlasherService.prototype.getTitle = /**
     * @return {?}
     */
    function () {
        return this.titleService.getTitle();
    };
    TitleFlasherService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    TitleFlasherService.ctorParameters = function () { return [
        { type: Title }
    ]; };
    /** @nocollapse */ TitleFlasherService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function TitleFlasherService_Factory() { return new TitleFlasherService(i0.ɵɵinject(i1.Title)); }, token: TitleFlasherService, providedIn: "root" });
    return TitleFlasherService;
}());
export { TitleFlasherService };
if (false) {
    /**
     * @type {?}
     * @private
     */
    TitleFlasherService.prototype.documentTitleFlash;
    /**
     * @type {?}
     * @private
     */
    TitleFlasherService.prototype.documentTitleFlashSubscription;
    /**
     * @type {?}
     * @private
     */
    TitleFlasherService.prototype.prevTitle;
    /** @type {?} */
    TitleFlasherService.prototype.isAlreadyFlashingTitle;
    /**
     * @type {?}
     * @private
     */
    TitleFlasherService.prototype.titleService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGl0bGUtZmxhc2hlci1zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGhwdGVjaC9zYXBoaXJhLXNoYXJlZC1saWIvIiwic291cmNlcyI6WyJsaWIvc2VydmljZXMvdGl0bGUtZmxhc2hlci1zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFBRSxLQUFLLEVBQUUsTUFBTSwyQkFBMkIsQ0FBQztBQUNsRCxPQUFPLEVBQUUsS0FBSyxFQUE0QixNQUFNLE1BQU0sQ0FBQzs7O0FBR3ZEO0lBU0UsNkJBQW9CLFlBQW1CO1FBQW5CLGlCQUFZLEdBQVosWUFBWSxDQUFPO0lBRXZDLENBQUM7SUFDRCwwQ0FBMEM7SUFDMUMsOEJBQThCO0lBQzlCLG1DQUFtQzs7Ozs7Ozs7O0lBQ25DLDJDQUFhOzs7Ozs7Ozs7SUFBYixVQUFjLFFBQWdCLEVBQUUsWUFBb0I7UUFBcEQsaUJBY0M7UUFiQyxJQUFJLENBQUMsSUFBSSxDQUFDLHNCQUFzQixFQUFFO1lBQ2hDLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUM5QyxJQUFJLENBQUMsa0JBQWtCLEdBQUcsS0FBSyxDQUFDLENBQUMsRUFBRSxRQUFRLENBQUMsQ0FBQztZQUM3QyxJQUFJLENBQUMsOEJBQThCLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFNBQVM7Ozs7WUFBQyxVQUFDLElBQVk7Z0JBQ25GLElBQUksSUFBSSxHQUFHLENBQUMsS0FBSyxDQUFDLEVBQUU7b0JBQ2xCLEtBQUksQ0FBQyxRQUFRLENBQUMsWUFBWSxDQUFDLENBQUM7aUJBQzdCO3FCQUFNO29CQUNMLEtBQUksQ0FBQyxRQUFRLENBQUMsS0FBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO2lCQUMvQjtZQUNILENBQUMsRUFBQyxDQUFDO1lBQ0gsSUFBSSxDQUFDLHNCQUFzQixHQUFHLElBQUksQ0FBQztTQUNwQztJQUVILENBQUM7Ozs7SUFDRCwwQ0FBWTs7O0lBQVo7UUFDRSxJQUFJLElBQUksQ0FBQyxzQkFBc0IsRUFBRTtZQUMvQixJQUFJLENBQUMsOEJBQThCLENBQUMsV0FBVyxFQUFFLENBQUM7WUFDbEQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDOUIsSUFBSSxDQUFDLHNCQUFzQixHQUFHLEtBQUssQ0FBQztTQUNyQztJQUVILENBQUM7Ozs7O0lBQ0Qsc0NBQVE7Ozs7SUFBUixVQUFTLEtBQWE7UUFDcEIsSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUM7SUFFcEMsQ0FBQzs7OztJQUNELHNDQUFROzs7SUFBUjtRQUNFLE9BQU8sSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLEVBQUUsQ0FBQztJQUN0QyxDQUFDOztnQkE1Q0YsVUFBVSxTQUFDO29CQUNWLFVBQVUsRUFBRSxNQUFNO2lCQUNuQjs7OztnQkFOUSxLQUFLOzs7OEJBRGQ7Q0FrREMsQUE3Q0QsSUE2Q0M7U0F6Q1ksbUJBQW1COzs7Ozs7SUFDOUIsaURBQStDOzs7OztJQUMvQyw2REFBcUQ7Ozs7O0lBQ3JELHdDQUEwQjs7SUFDMUIscURBQXVDOzs7OztJQUMzQiwyQ0FBMkIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBUaXRsZSB9IGZyb20gJ0Bhbmd1bGFyL3BsYXRmb3JtLWJyb3dzZXInO1xuaW1wb3J0IHsgdGltZXIsIE9ic2VydmFibGUsIFN1YnNjcmlwdGlvbiB9IGZyb20gJ3J4anMnO1xuXG5cbkBJbmplY3RhYmxlKHtcbiAgcHJvdmlkZWRJbjogJ3Jvb3QnXG59KVxuLy8gU2VydmljZSB0byBmbGFzaCB0aXRsZW9mIGN1cnJlY3QgYnJvd3NlciB0YWIuXG5leHBvcnQgY2xhc3MgVGl0bGVGbGFzaGVyU2VydmljZSB7XG4gIHByaXZhdGUgZG9jdW1lbnRUaXRsZUZsYXNoOiBPYnNlcnZhYmxlPG51bWJlcj47XG4gIHByaXZhdGUgZG9jdW1lbnRUaXRsZUZsYXNoU3Vic2NyaXB0aW9uOiBTdWJzY3JpcHRpb247XG4gIHByaXZhdGUgcHJldlRpdGxlOiBzdHJpbmc7XG4gIHB1YmxpYyBpc0FscmVhZHlGbGFzaGluZ1RpdGxlOiBib29sZWFuO1xuICBjb25zdHJ1Y3Rvcihwcml2YXRlIHRpdGxlU2VydmljZTogVGl0bGUpIHtcblxuICB9XG4gIC8vIE1ldGhvZCB0byBzdGFydCBmbGFzaGluZyBkb2N1bWVudCB0aXRsZVxuICAvLyBJbnRlcnZhbDogaW50ZXJ2YWwgb2YgZmxhc2hcbiAgLy8gU3RyaW5nIHRvIGZsYXNoIGluIGJyb3dzZXIgdGl0bGVcbiAgc3RhcnRGbGFzaGluZyhpbnRlcnZhbDogbnVtYmVyLCB0aXRsZVRvRmxhc2g6IHN0cmluZykge1xuICAgIGlmICghdGhpcy5pc0FscmVhZHlGbGFzaGluZ1RpdGxlKSB7XG4gICAgICB0aGlzLnByZXZUaXRsZSA9IHRoaXMudGl0bGVTZXJ2aWNlLmdldFRpdGxlKCk7XG4gICAgICB0aGlzLmRvY3VtZW50VGl0bGVGbGFzaCA9IHRpbWVyKDAsIGludGVydmFsKTtcbiAgICAgIHRoaXMuZG9jdW1lbnRUaXRsZUZsYXNoU3Vic2NyaXB0aW9uID0gdGhpcy5kb2N1bWVudFRpdGxlRmxhc2guc3Vic2NyaWJlKChkYXRhOiBudW1iZXIpID0+IHtcbiAgICAgICAgaWYgKGRhdGEgJSAyID09PSAwKSB7XG4gICAgICAgICAgdGhpcy5zZXRUaXRsZSh0aXRsZVRvRmxhc2gpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIHRoaXMuc2V0VGl0bGUodGhpcy5wcmV2VGl0bGUpO1xuICAgICAgICB9XG4gICAgICB9KTtcbiAgICAgIHRoaXMuaXNBbHJlYWR5Rmxhc2hpbmdUaXRsZSA9IHRydWU7XG4gICAgfVxuXG4gIH1cbiAgc3RvcEZsYXNoaW5nKCkge1xuICAgIGlmICh0aGlzLmlzQWxyZWFkeUZsYXNoaW5nVGl0bGUpIHtcbiAgICAgIHRoaXMuZG9jdW1lbnRUaXRsZUZsYXNoU3Vic2NyaXB0aW9uLnVuc3Vic2NyaWJlKCk7XG4gICAgICB0aGlzLnNldFRpdGxlKHRoaXMucHJldlRpdGxlKTtcbiAgICAgIHRoaXMuaXNBbHJlYWR5Rmxhc2hpbmdUaXRsZSA9IGZhbHNlO1xuICAgIH1cblxuICB9XG4gIHNldFRpdGxlKHRpdGxlOiBzdHJpbmcpIHtcbiAgICB0aGlzLnRpdGxlU2VydmljZS5zZXRUaXRsZSh0aXRsZSk7XG5cbiAgfVxuICBnZXRUaXRsZSgpIHtcbiAgICByZXR1cm4gdGhpcy50aXRsZVNlcnZpY2UuZ2V0VGl0bGUoKTtcbiAgfVxufVxuIl19