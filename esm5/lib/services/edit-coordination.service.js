/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import * as i0 from "@angular/core";
var EditCoordinationService = /** @class */ (function () {
    function EditCoordinationService() {
        this.editStatus = {
            infoCompany: false,
            infoField: false,
            infoPad: false,
            infoWell: false,
            wellboreInfo: false,
            wellboreMagnetic: false,
            trajectoryInfo: false,
            trajectoryTieIn: false,
            trajectorySurveys: false,
            surveySetInfo: false,
            surveySetRigContact: false,
            surveySetGrid: false,
        };
        this.editFieldStatus = new BehaviorSubject(false);
        this.getEditFieldStatus = this.editFieldStatus.asObservable();
    }
    /**
     * @param {?} field
     * @param {?} status
     * @return {?}
     */
    EditCoordinationService.prototype.toggleField = /**
     * @param {?} field
     * @param {?} status
     * @return {?}
     */
    function (field, status) {
        this.editStatus[field] = status;
        this.setEditFieldStatus(this.editStatus);
    };
    /**
     * @return {?}
     */
    EditCoordinationService.prototype.areEditFieldsClosed = /**
     * @return {?}
     */
    function () {
        var _this = this;
        /** @type {?} */
        var currentlyEdited = 0;
        Object.keys(this.editStatus).forEach((/**
         * @param {?} es
         * @return {?}
         */
        function (es) {
            if (_this.editStatus[es] === true) {
                currentlyEdited++;
            }
        }));
        return currentlyEdited === 0;
    };
    /**
     * @private
     * @param {?} set
     * @return {?}
     */
    EditCoordinationService.prototype.setEditFieldStatus = /**
     * @private
     * @param {?} set
     * @return {?}
     */
    function (set) {
        this.editFieldStatus.next(set);
    };
    EditCoordinationService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    EditCoordinationService.ctorParameters = function () { return []; };
    /** @nocollapse */ EditCoordinationService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function EditCoordinationService_Factory() { return new EditCoordinationService(); }, token: EditCoordinationService, providedIn: "root" });
    return EditCoordinationService;
}());
export { EditCoordinationService };
if (false) {
    /** @type {?} */
    EditCoordinationService.prototype.editStatus;
    /**
     * @type {?}
     * @private
     */
    EditCoordinationService.prototype.editFieldStatus;
    /** @type {?} */
    EditCoordinationService.prototype.getEditFieldStatus;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZWRpdC1jb29yZGluYXRpb24uc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BocHRlY2gvc2FwaGlyYS1zaGFyZWQtbGliLyIsInNvdXJjZXMiOlsibGliL3NlcnZpY2VzL2VkaXQtY29vcmRpbmF0aW9uLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLE1BQU0sQ0FBQzs7QUFFdkM7SUF1QkU7UUFsQkEsZUFBVSxHQUFHO1lBQ1gsV0FBVyxFQUFFLEtBQUs7WUFDbEIsU0FBUyxFQUFFLEtBQUs7WUFDaEIsT0FBTyxFQUFFLEtBQUs7WUFDZCxRQUFRLEVBQUUsS0FBSztZQUNmLFlBQVksRUFBRSxLQUFLO1lBQ25CLGdCQUFnQixFQUFFLEtBQUs7WUFDdkIsY0FBYyxFQUFFLEtBQUs7WUFDckIsZUFBZSxFQUFFLEtBQUs7WUFDdEIsaUJBQWlCLEVBQUUsS0FBSztZQUN4QixhQUFhLEVBQUUsS0FBSztZQUNwQixtQkFBbUIsRUFBRSxLQUFLO1lBQzFCLGFBQWEsRUFBRSxLQUFLO1NBQ3JCLENBQUM7UUFFTSxvQkFBZSxHQUFHLElBQUksZUFBZSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ3JELHVCQUFrQixHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsWUFBWSxFQUFFLENBQUM7SUFFekMsQ0FBQzs7Ozs7O0lBRWpCLDZDQUFXOzs7OztJQUFYLFVBQVksS0FBYSxFQUFFLE1BQWU7UUFDeEMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsR0FBRyxNQUFNLENBQUM7UUFDaEMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztJQUMzQyxDQUFDOzs7O0lBRUQscURBQW1COzs7SUFBbkI7UUFBQSxpQkFRQzs7WUFQSyxlQUFlLEdBQUcsQ0FBQztRQUN2QixNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxPQUFPOzs7O1FBQUMsVUFBQSxFQUFFO1lBQ3JDLElBQUksS0FBSSxDQUFDLFVBQVUsQ0FBQyxFQUFFLENBQUMsS0FBSyxJQUFJLEVBQUU7Z0JBQ2hDLGVBQWUsRUFBRSxDQUFDO2FBQ25CO1FBQ0gsQ0FBQyxFQUFDLENBQUM7UUFDSCxPQUFPLGVBQWUsS0FBSyxDQUFDLENBQUM7SUFDL0IsQ0FBQzs7Ozs7O0lBRU8sb0RBQWtCOzs7OztJQUExQixVQUEyQixHQUFHO1FBQzVCLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQ2pDLENBQUM7O2dCQTFDRixVQUFVLFNBQUM7b0JBQ1YsVUFBVSxFQUFFLE1BQU07aUJBQ25COzs7OztrQ0FMRDtDQThDQyxBQTNDRCxJQTJDQztTQXhDWSx1QkFBdUI7OztJQUVsQyw2Q0FhRTs7Ozs7SUFFRixrREFBcUQ7O0lBQ3JELHFEQUF5RCIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEJlaGF2aW9yU3ViamVjdCB9IGZyb20gJ3J4anMnO1xuXG5ASW5qZWN0YWJsZSh7XG4gIHByb3ZpZGVkSW46ICdyb290J1xufSlcbmV4cG9ydCBjbGFzcyBFZGl0Q29vcmRpbmF0aW9uU2VydmljZSB7XG5cbiAgZWRpdFN0YXR1cyA9IHtcbiAgICBpbmZvQ29tcGFueTogZmFsc2UsXG4gICAgaW5mb0ZpZWxkOiBmYWxzZSxcbiAgICBpbmZvUGFkOiBmYWxzZSxcbiAgICBpbmZvV2VsbDogZmFsc2UsXG4gICAgd2VsbGJvcmVJbmZvOiBmYWxzZSxcbiAgICB3ZWxsYm9yZU1hZ25ldGljOiBmYWxzZSxcbiAgICB0cmFqZWN0b3J5SW5mbzogZmFsc2UsXG4gICAgdHJhamVjdG9yeVRpZUluOiBmYWxzZSxcbiAgICB0cmFqZWN0b3J5U3VydmV5czogZmFsc2UsXG4gICAgc3VydmV5U2V0SW5mbzogZmFsc2UsXG4gICAgc3VydmV5U2V0UmlnQ29udGFjdDogZmFsc2UsXG4gICAgc3VydmV5U2V0R3JpZDogZmFsc2UsXG4gIH07XG5cbiAgcHJpdmF0ZSBlZGl0RmllbGRTdGF0dXMgPSBuZXcgQmVoYXZpb3JTdWJqZWN0KGZhbHNlKTtcbiAgZ2V0RWRpdEZpZWxkU3RhdHVzID0gdGhpcy5lZGl0RmllbGRTdGF0dXMuYXNPYnNlcnZhYmxlKCk7XG5cbiAgY29uc3RydWN0b3IoKSB7IH1cblxuICB0b2dnbGVGaWVsZChmaWVsZDogc3RyaW5nLCBzdGF0dXM6IGJvb2xlYW4pOiB2b2lkIHtcbiAgICB0aGlzLmVkaXRTdGF0dXNbZmllbGRdID0gc3RhdHVzO1xuICAgIHRoaXMuc2V0RWRpdEZpZWxkU3RhdHVzKHRoaXMuZWRpdFN0YXR1cyk7XG4gIH1cblxuICBhcmVFZGl0RmllbGRzQ2xvc2VkKCkge1xuICAgIGxldCBjdXJyZW50bHlFZGl0ZWQgPSAwO1xuICAgIE9iamVjdC5rZXlzKHRoaXMuZWRpdFN0YXR1cykuZm9yRWFjaChlcyA9PiB7XG4gICAgICBpZiAodGhpcy5lZGl0U3RhdHVzW2VzXSA9PT0gdHJ1ZSkge1xuICAgICAgICBjdXJyZW50bHlFZGl0ZWQrKztcbiAgICAgIH1cbiAgICB9KTtcbiAgICByZXR1cm4gY3VycmVudGx5RWRpdGVkID09PSAwO1xuICB9XG5cbiAgcHJpdmF0ZSBzZXRFZGl0RmllbGRTdGF0dXMoc2V0KSB7XG4gICAgdGhpcy5lZGl0RmllbGRTdGF0dXMubmV4dChzZXQpO1xuICB9XG59XG4iXX0=