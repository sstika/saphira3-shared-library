/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { isNullOrUndefined } from 'util';
import * as _ from 'lodash';
import * as moment_ from 'moment';
/*
 * NOTES
 *   Using withCredentials is a huge security no-no!
 *
 *   It leaks the user's username and password with every call, and while
 *   using HTTPS help mitigate the problem, it is generally regarded as a
 *   bad practice.
 *
 *   We would need to fix the back end to patch this vulernability.
 */
var DataService = /** @class */ (function () {
    function DataService(http) {
        this.http = http;
        this.urlRoot = environment.apiRoot + '/saphira/api/';
    }
    /**
     * This function is for extracting the usable value from GET,POST,PUT,DELETE Response.
     * @param  res - {any} the response object (possibly) from a call;
     * @returns Observable<any> -  Returns an Observable<any> .
     */
    /**
     * This function is for extracting the usable value from GET,POST,PUT,DELETE Response.
     * @protected
     * @param {?} res - {any} the response object (possibly) from a call;
     * @return {?} Observable<any> -  Returns an Observable<any> .
     */
    DataService.prototype.extractData = /**
     * This function is for extracting the usable value from GET,POST,PUT,DELETE Response.
     * @protected
     * @param {?} res - {any} the response object (possibly) from a call;
     * @return {?} Observable<any> -  Returns an Observable<any> .
     */
    function (res) {
        if (!res) {
            return null;
        }
        if (res.body === undefined) {
            return res;
        }
        return res.body;
    };
    /**
     * Display a notification that informs the user that an HTTP error occurred.
     * Must be consumed by the child service, does NOT get called by default.
     * @param msg User friendly message to display to the user
     */
    /**
     * Display a notification that informs the user that an HTTP error occurred.
     * Must be consumed by the child service, does NOT get called by default.
     * @protected
     * @param {?} msg User friendly message to display to the user
     * @return {?}
     */
    DataService.prototype.displayErrorMsg = /**
     * Display a notification that informs the user that an HTTP error occurred.
     * Must be consumed by the child service, does NOT get called by default.
     * @protected
     * @param {?} msg User friendly message to display to the user
     * @return {?}
     */
    function (msg) {
        // TODO: this is a placeholder, a snackbar is what should be used
    };
    /**
     * Sends a verbose error message to the console for further debugging
     * @param  error - HttpErrorResponse error object that is supplied via HttpClient
     * @param method - the HTTP method used
     * @returns `HttpError` that was passed in or a placeholder if none was provided
     */
    /**
     * Sends a verbose error message to the console for further debugging
     * @protected
     * @param {?} error - HttpErrorResponse error object that is supplied via HttpClient
     * @param {?} method - the HTTP method used
     * @return {?} `HttpError` that was passed in or a placeholder if none was provided
     */
    DataService.prototype.logError = /**
     * Sends a verbose error message to the console for further debugging
     * @protected
     * @param {?} error - HttpErrorResponse error object that is supplied via HttpClient
     * @param {?} method - the HTTP method used
     * @return {?} `HttpError` that was passed in or a placeholder if none was provided
     */
    function (error, method) {
        if (isNullOrUndefined(error) || _.isEmpty(error)) {
            /** @type {?} */
            var err = new Error('An unknown error occurred and no error was supplied to the handler');
            error = new HttpErrorResponse({ error: err });
        }
        /** @type {?} */
        var message = error.message || 'An error occurred and no message was supplied to the handler';
        /** @type {?} */
        var url = error.url || '???';
        // TODO: if we get a lot of ???'s we can pass in the URL
        /** @type {?} */
        var status = error.status || '???';
        /** @type {?} */
        var statusText = error.statusText || '???';
        /** @type {?} */
        var headers = this.headers2str(error.headers) || '???';
        /** @type {?} */
        var stack = error.error.stack || 'No Call Stack Provided';
        /** @type {?} */
        var moment = moment_;
        // TODO: remote logging would go here!
        // Error Message here!
        // Context: http://localhost:4200/#/
        // Action:  GET /saphira/api/ping
        // Status:  400 statusText
        // Time:    2019-04-23T15:28:00+8
        // Headers: {foo: bar}
        // call stack
        // <this line intentionally left blank>
        console.error(message + '\n' +
            ("Context: " + window.location.href + "\n") +
            ("Action:  " + method + " " + url + "\n") +
            ("Status:  " + status + " " + statusText + "\n") +
            ("Time:    " + moment().toISOString(true) + "\n") +
            ("Headers: " + headers + "\n") +
            stack + '\n\n');
        return throwError(error);
    };
    /**
     * Maps `HttpHeaders` to simple JSON string for debugging
     */
    /**
     * Maps `HttpHeaders` to simple JSON string for debugging
     * @private
     * @param {?} headers
     * @return {?}
     */
    DataService.prototype.headers2str = /**
     * Maps `HttpHeaders` to simple JSON string for debugging
     * @private
     * @param {?} headers
     * @return {?}
     */
    function (headers) {
        var e_1, _a;
        if (isNullOrUndefined(headers)) {
            return null;
        }
        /** @type {?} */
        var json = {};
        try {
            for (var _b = tslib_1.__values(headers.keys()), _c = _b.next(); !_c.done; _c = _b.next()) {
                var key = _c.value;
                /** @type {?} */
                var values = headers.getAll(key);
                if (values.length === 1) {
                    json[key] = values[0];
                    continue;
                }
                json[key] = tslib_1.__spread(values);
            }
        }
        catch (e_1_1) { e_1 = { error: e_1_1 }; }
        finally {
            try {
                if (_c && !_c.done && (_a = _b.return)) _a.call(_b);
            }
            finally { if (e_1) throw e_1.error; }
        }
        return JSON.stringify(json, null, 2);
    };
    /**
     * Master GET function.
     * @param  path - string path of the endpoint;
     * @param  headers - custom headers supplied to the call;
     * @returns Observable<any> -  Returns an Observable<any> .
     */
    /**
     * Master GET function.
     * @protected
     * @param {?} path - string path of the endpoint;
     * @param {?=} headers - custom headers supplied to the call;
     * @return {?} Observable<any> -  Returns an Observable<any> .
     */
    DataService.prototype.get = /**
     * Master GET function.
     * @protected
     * @param {?} path - string path of the endpoint;
     * @param {?=} headers - custom headers supplied to the call;
     * @return {?} Observable<any> -  Returns an Observable<any> .
     */
    function (path, headers) {
        var _this = this;
        /** @type {?} */
        var options = { withCredentials: true };
        if (headers) {
            options.headers = headers;
        }
        return this.http
            .get(this.urlRoot + path, options)
            .pipe(map(this.extractData), catchError((/**
         * @param {?} err
         * @return {?}
         */
        function (err) { return _this.logError(err, 'GET'); })));
    };
    /**
     * Master PUT function.
     * @param  path - string path of the endpoint;
     * @param  obj - object for which you want to put;
     * @param  headers - custom headers supplied to the call;
     * @returns Observable<any> -  Returns an Observable<any> .
     */
    /**
     * Master PUT function.
     * @protected
     * @param {?} path - string path of the endpoint;
     * @param {?} obj - object for which you want to put;
     * @param {?=} headers - custom headers supplied to the call;
     * @return {?} Observable<any> -  Returns an Observable<any> .
     */
    DataService.prototype.put = /**
     * Master PUT function.
     * @protected
     * @param {?} path - string path of the endpoint;
     * @param {?} obj - object for which you want to put;
     * @param {?=} headers - custom headers supplied to the call;
     * @return {?} Observable<any> -  Returns an Observable<any> .
     */
    function (path, obj, headers) {
        var _this = this;
        /** @type {?} */
        var options = { withCredentials: true };
        if (headers) {
            options.headers = headers;
        }
        return this.http
            .put(this.urlRoot + path, obj, options)
            .pipe(map(this.extractData), catchError((/**
         * @param {?} err
         * @return {?}
         */
        function (err) { return _this.logError(err, 'PUT'); })));
    };
    /**
     * Master POST function.
     * @param  path - string path of the endpoint;
     * @param  obj - object for which you want to put;
     * @param  headers - custom headers supplied to the call;
     * @returns Observable<any> -  Returns an Observable<any> .
     */
    /**
     * Master POST function.
     * @protected
     * @param {?} path - string path of the endpoint;
     * @param {?} obj - object for which you want to put;
     * @param {?=} headers - custom headers supplied to the call;
     * @return {?} Observable<any> -  Returns an Observable<any> .
     */
    DataService.prototype.post = /**
     * Master POST function.
     * @protected
     * @param {?} path - string path of the endpoint;
     * @param {?} obj - object for which you want to put;
     * @param {?=} headers - custom headers supplied to the call;
     * @return {?} Observable<any> -  Returns an Observable<any> .
     */
    function (path, obj, headers) {
        var _this = this;
        /** @type {?} */
        var options = { withCredentials: true };
        if (headers) {
            options.headers = headers;
        }
        return this.http
            .post(this.urlRoot + path, obj, options)
            .pipe(map(this.extractData), map((/**
         * @param {?} res
         * @return {?}
         */
        function (res) { return res || isNullOrUndefined(res); })), // TODO: Need to investigate further
        catchError((/**
         * @param {?} err
         * @return {?}
         */
        function (err) { return _this.logError(err, 'POST'); })));
    };
    /**
     * Master DELETE function.
     * @param  path - string path of the endpoint;
     * @param  headers - custom headers supplied to the call;
     * @returns Observable<any> -  Returns an Observable<any> .
     */
    /**
     * Master DELETE function.
     * @protected
     * @param {?} path - string path of the endpoint;
     * @param {?=} headers - custom headers supplied to the call;
     * @return {?} Observable<any> -  Returns an Observable<any> .
     */
    DataService.prototype.delete = /**
     * Master DELETE function.
     * @protected
     * @param {?} path - string path of the endpoint;
     * @param {?=} headers - custom headers supplied to the call;
     * @return {?} Observable<any> -  Returns an Observable<any> .
     */
    function (path, headers) {
        var _this = this;
        /** @type {?} */
        var options = { withCredentials: true };
        if (headers) {
            options.headers = headers;
        }
        return this.http
            .delete(this.urlRoot + path, options)
            .pipe(map(this.extractData), map((/**
         * @param {?} res
         * @return {?}
         */
        function (res) { return res || isNullOrUndefined(res); })), // TODO: Need to investigate further
        catchError((/**
         * @param {?} err
         * @return {?}
         */
        function (err) { return _this.logError(err, 'DELETE'); })));
    };
    DataService.decorators = [
        { type: Injectable }
    ];
    /** @nocollapse */
    DataService.ctorParameters = function () { return [
        { type: HttpClient }
    ]; };
    return DataService;
}());
export { DataService };
if (false) {
    /**
     * @type {?}
     * @protected
     */
    DataService.prototype.urlRoot;
    /**
     * @type {?}
     * @protected
     */
    DataService.prototype.http;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGF0YS5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGhwdGVjaC9zYXBoaXJhLXNoYXJlZC1saWIvIiwic291cmNlcyI6WyJsaWIvc2VydmljZXMvZGF0YS5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUFjLFVBQVUsRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUM5QyxPQUFPLEVBQUUsVUFBVSxFQUFFLEdBQUcsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQ2pELE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFFLFVBQVUsRUFBRSxpQkFBaUIsRUFBZSxNQUFNLHNCQUFzQixDQUFDO0FBQ2xGLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSxnQ0FBZ0MsQ0FBQztBQUM3RCxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFDekMsT0FBTyxLQUFLLENBQUMsTUFBTSxRQUFRLENBQUM7QUFDNUIsT0FBTyxLQUFLLE9BQU8sTUFBTSxRQUFRLENBQUM7Ozs7Ozs7Ozs7O0FBYWxDO0lBSUUscUJBQXVCLElBQWdCO1FBQWhCLFNBQUksR0FBSixJQUFJLENBQVk7UUFGN0IsWUFBTyxHQUFHLFdBQVcsQ0FBQyxPQUFPLEdBQUcsZUFBZSxDQUFDO0lBRWQsQ0FBQztJQUU3Qzs7OztPQUlHOzs7Ozs7O0lBQ08saUNBQVc7Ozs7OztJQUFyQixVQUFzQixHQUFRO1FBQzVCLElBQUssQ0FBQyxHQUFHLEVBQUc7WUFDVixPQUFPLElBQUksQ0FBQztTQUNiO1FBRUQsSUFBSyxHQUFHLENBQUMsSUFBSSxLQUFLLFNBQVMsRUFBRztZQUM1QixPQUFPLEdBQUcsQ0FBQztTQUNaO1FBRUQsT0FBTyxHQUFHLENBQUMsSUFBSSxDQUFDO0lBQ2xCLENBQUM7SUFFRDs7OztPQUlHOzs7Ozs7OztJQUNPLHFDQUFlOzs7Ozs7O0lBQXpCLFVBQTBCLEdBQVc7UUFDbkMsaUVBQWlFO0lBQ25FLENBQUM7SUFFRDs7Ozs7T0FLRzs7Ozs7Ozs7SUFDTyw4QkFBUTs7Ozs7OztJQUFsQixVQUNVLEtBQXdCLEVBQ3hCLE1BQXlDO1FBRWpELElBQUssaUJBQWlCLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsRUFBRzs7Z0JBQzVDLEdBQUcsR0FBRyxJQUFJLEtBQUssQ0FDbkIsb0VBQW9FLENBQ3JFO1lBQ0QsS0FBSyxHQUFHLElBQUksaUJBQWlCLENBQUMsRUFBRSxLQUFLLEVBQUUsR0FBRyxFQUFFLENBQUMsQ0FBQztTQUMvQzs7WUFFSyxPQUFPLEdBQUcsS0FBSyxDQUFDLE9BQU8sSUFBSSw4REFBOEQ7O1lBQ3pGLEdBQUcsR0FBRyxLQUFLLENBQUMsR0FBRyxJQUFJLEtBQUs7OztZQUN4QixNQUFNLEdBQUcsS0FBSyxDQUFDLE1BQU0sSUFBSSxLQUFLOztZQUM5QixVQUFVLEdBQUcsS0FBSyxDQUFDLFVBQVUsSUFBSSxLQUFLOztZQUN0QyxPQUFPLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksS0FBSzs7WUFDbEQsS0FBSyxHQUFHLEtBQUssQ0FBQyxLQUFLLENBQUMsS0FBSyxJQUFJLHdCQUF3Qjs7WUFDckQsTUFBTSxHQUFHLE9BQU87UUFHdEIsc0NBQXNDO1FBRXRDLHNCQUFzQjtRQUN0QixvQ0FBb0M7UUFDcEMsaUNBQWlDO1FBQ2pDLDBCQUEwQjtRQUMxQixpQ0FBaUM7UUFDakMsc0JBQXNCO1FBQ3RCLGFBQWE7UUFDYix1Q0FBdUM7UUFFdkMsT0FBTyxDQUFDLEtBQUssQ0FDWCxPQUFPLEdBQUcsSUFBSTthQUNkLGNBQVksTUFBTSxDQUFDLFFBQVEsQ0FBQyxJQUFJLE9BQUksQ0FBQTthQUNwQyxjQUFZLE1BQU0sU0FBSSxHQUFHLE9BQUksQ0FBQTthQUM3QixjQUFZLE1BQU0sU0FBSSxVQUFVLE9BQUksQ0FBQTthQUNwQyxjQUFZLE1BQU0sRUFBRSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsT0FBSSxDQUFBO2FBQzFDLGNBQVksT0FBTyxPQUFJLENBQUE7WUFDdkIsS0FBSyxHQUFJLE1BQU0sQ0FDaEIsQ0FBQztRQUVGLE9BQU8sVUFBVSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQzNCLENBQUM7SUFFRDs7T0FFRzs7Ozs7OztJQUNLLGlDQUFXOzs7Ozs7SUFBbkIsVUFBcUIsT0FBb0I7O1FBQ3ZDLElBQUssaUJBQWlCLENBQUMsT0FBTyxDQUFDLEVBQUc7WUFDaEMsT0FBTyxJQUFJLENBQUM7U0FDYjs7WUFFSyxJQUFJLEdBQUcsRUFBRTs7WUFDZixLQUFtQixJQUFBLEtBQUEsaUJBQUEsT0FBTyxDQUFDLElBQUksRUFBRSxDQUFBLGdCQUFBLDRCQUFHO2dCQUE5QixJQUFNLEdBQUcsV0FBQTs7b0JBQ1AsTUFBTSxHQUFHLE9BQU8sQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDO2dCQUNsQyxJQUFLLE1BQU0sQ0FBQyxNQUFNLEtBQUssQ0FBQyxFQUFHO29CQUN6QixJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUN0QixTQUFTO2lCQUNWO2dCQUVELElBQUksQ0FBQyxHQUFHLENBQUMsb0JBQVEsTUFBTSxDQUFFLENBQUM7YUFDM0I7Ozs7Ozs7OztRQUdELE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQyxDQUFDO0lBQ3ZDLENBQUM7SUFFRDs7Ozs7T0FLRzs7Ozs7Ozs7SUFDTyx5QkFBRzs7Ozs7OztJQUFiLFVBQ1ksSUFBWSxFQUNaLE9BQW1DO1FBRi9DLGlCQWdCQzs7WUFaTyxPQUFPLEdBQVEsRUFBRSxlQUFlLEVBQUUsSUFBSSxFQUFFO1FBRTlDLElBQUssT0FBTyxFQUFHO1lBQ2IsT0FBTyxDQUFDLE9BQU8sR0FBRyxPQUFPLENBQUM7U0FDM0I7UUFFRCxPQUFPLElBQUksQ0FBQyxJQUFJO2FBQ2IsR0FBRyxDQUFDLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxFQUFFLE9BQU8sQ0FBQzthQUNqQyxJQUFJLENBQ0gsR0FBRyxDQUFFLElBQUksQ0FBQyxXQUFXLENBQUUsRUFDdkIsVUFBVTs7OztRQUFFLFVBQUEsR0FBRyxJQUFJLE9BQUEsS0FBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLEVBQUUsS0FBSyxDQUFDLEVBQXpCLENBQXlCLEVBQUUsQ0FDL0MsQ0FBQztJQUNOLENBQUM7SUFFRDs7Ozs7O09BTUc7Ozs7Ozs7OztJQUNPLHlCQUFHOzs7Ozs7OztJQUFiLFVBQ1ksSUFBWSxFQUNaLEdBQVEsRUFDUixPQUFtQztRQUgvQyxpQkFpQkM7O1lBWk8sT0FBTyxHQUFRLEVBQUUsZUFBZSxFQUFFLElBQUksRUFBRTtRQUU5QyxJQUFLLE9BQU8sRUFBRztZQUNiLE9BQU8sQ0FBQyxPQUFPLEdBQUcsT0FBTyxDQUFDO1NBQzNCO1FBRUQsT0FBTyxJQUFJLENBQUMsSUFBSTthQUNiLEdBQUcsQ0FBQyxJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksRUFBRSxHQUFHLEVBQUUsT0FBTyxDQUFDO2FBQ3RDLElBQUksQ0FDSCxHQUFHLENBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBRSxFQUN2QixVQUFVOzs7O1FBQUUsVUFBQSxHQUFHLElBQUksT0FBQSxLQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsRUFBRSxLQUFLLENBQUMsRUFBekIsQ0FBeUIsRUFBRSxDQUMvQyxDQUFDO0lBQ04sQ0FBQztJQUVEOzs7Ozs7T0FNRzs7Ozs7Ozs7O0lBQ08sMEJBQUk7Ozs7Ozs7O0lBQWQsVUFDWSxJQUFZLEVBQ1osR0FBUSxFQUNSLE9BQW1DO1FBSC9DLGlCQWtCQzs7WUFiTyxPQUFPLEdBQVEsRUFBRSxlQUFlLEVBQUUsSUFBSSxFQUFFO1FBRTlDLElBQUssT0FBTyxFQUFHO1lBQ2IsT0FBTyxDQUFDLE9BQU8sR0FBRyxPQUFPLENBQUM7U0FDM0I7UUFFRCxPQUFPLElBQUksQ0FBQyxJQUFJO2FBQ2IsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxFQUFFLEdBQUcsRUFBRSxPQUFPLENBQUM7YUFDdkMsSUFBSSxDQUNILEdBQUcsQ0FBRSxJQUFJLENBQUMsV0FBVyxDQUFFLEVBQ3ZCLEdBQUc7Ozs7UUFBRSxVQUFBLEdBQUcsSUFBSSxPQUFBLEdBQUcsSUFBSSxpQkFBaUIsQ0FBQyxHQUFHLENBQUMsRUFBN0IsQ0FBNkIsRUFBRSxFQUFFLG9DQUFvQztRQUNqRixVQUFVOzs7O1FBQUUsVUFBQSxHQUFHLElBQUksT0FBQSxLQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsRUFBRSxNQUFNLENBQUMsRUFBMUIsQ0FBMEIsRUFBRSxDQUNoRCxDQUFDO0lBQ04sQ0FBQztJQUVEOzs7OztPQUtHOzs7Ozs7OztJQUNPLDRCQUFNOzs7Ozs7O0lBQWhCLFVBQ1ksSUFBWSxFQUNaLE9BQW1DO1FBRi9DLGlCQWlCQzs7WUFiTyxPQUFPLEdBQVEsRUFBRSxlQUFlLEVBQUUsSUFBSSxFQUFFO1FBRTlDLElBQUssT0FBTyxFQUFHO1lBQ2IsT0FBTyxDQUFDLE9BQU8sR0FBRyxPQUFPLENBQUM7U0FDM0I7UUFFRCxPQUFPLElBQUksQ0FBQyxJQUFJO2FBQ2IsTUFBTSxDQUFDLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxFQUFFLE9BQU8sQ0FBRTthQUNyQyxJQUFJLENBQ0gsR0FBRyxDQUFFLElBQUksQ0FBQyxXQUFXLENBQUUsRUFDdkIsR0FBRzs7OztRQUFFLFVBQUEsR0FBRyxJQUFJLE9BQUEsR0FBRyxJQUFJLGlCQUFpQixDQUFDLEdBQUcsQ0FBQyxFQUE3QixDQUE2QixFQUFFLEVBQUUsb0NBQW9DO1FBQ2pGLFVBQVU7Ozs7UUFBRSxVQUFBLEdBQUcsSUFBSSxPQUFBLEtBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxFQUFFLFFBQVEsQ0FBQyxFQUE1QixDQUE0QixFQUFFLENBQ2xELENBQUM7SUFDTixDQUFDOztnQkE3TUYsVUFBVTs7OztnQkFqQkYsVUFBVTs7SUFnT25CLGtCQUFDO0NBQUEsQUEvTUQsSUErTUM7U0E5TVksV0FBVzs7Ozs7O0lBQ3RCLDhCQUEwRDs7Ozs7SUFFN0MsMkJBQTBCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgT2JzZXJ2YWJsZSwgdGhyb3dFcnJvciB9IGZyb20gJ3J4anMnO1xuaW1wb3J0IHsgY2F0Y2hFcnJvciwgbWFwIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xuaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgSHR0cENsaWVudCwgSHR0cEVycm9yUmVzcG9uc2UsIEh0dHBIZWFkZXJzIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xuaW1wb3J0IHsgZW52aXJvbm1lbnQgfSBmcm9tICcuLi8uLi9lbnZpcm9ubWVudHMvZW52aXJvbm1lbnQnO1xuaW1wb3J0IHsgaXNOdWxsT3JVbmRlZmluZWQgfSBmcm9tICd1dGlsJztcbmltcG9ydCAqIGFzIF8gZnJvbSAnbG9kYXNoJztcbmltcG9ydCAqIGFzIG1vbWVudF8gZnJvbSAnbW9tZW50JztcblxuLypcbiAqIE5PVEVTXG4gKiAgIFVzaW5nIHdpdGhDcmVkZW50aWFscyBpcyBhIGh1Z2Ugc2VjdXJpdHkgbm8tbm8hXG4gKlxuICogICBJdCBsZWFrcyB0aGUgdXNlcidzIHVzZXJuYW1lIGFuZCBwYXNzd29yZCB3aXRoIGV2ZXJ5IGNhbGwsIGFuZCB3aGlsZVxuICogICB1c2luZyBIVFRQUyBoZWxwIG1pdGlnYXRlIHRoZSBwcm9ibGVtLCBpdCBpcyBnZW5lcmFsbHkgcmVnYXJkZWQgYXMgYVxuICogICBiYWQgcHJhY3RpY2UuXG4gKlxuICogICBXZSB3b3VsZCBuZWVkIHRvIGZpeCB0aGUgYmFjayBlbmQgdG8gcGF0Y2ggdGhpcyB2dWxlcm5hYmlsaXR5LlxuICovXG5cbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBEYXRhU2VydmljZSB7XG4gIHByb3RlY3RlZCB1cmxSb290ID0gZW52aXJvbm1lbnQuYXBpUm9vdCArICcvc2FwaGlyYS9hcGkvJztcblxuICBjb25zdHJ1Y3RvciggcHJvdGVjdGVkIGh0dHA6IEh0dHBDbGllbnQgKSB7IH1cblxuICAvKipcbiAgICogVGhpcyBmdW5jdGlvbiBpcyBmb3IgZXh0cmFjdGluZyB0aGUgdXNhYmxlIHZhbHVlIGZyb20gR0VULFBPU1QsUFVULERFTEVURSBSZXNwb25zZS5cbiAgICogQHBhcmFtICByZXMgLSB7YW55fSB0aGUgcmVzcG9uc2Ugb2JqZWN0IChwb3NzaWJseSkgZnJvbSBhIGNhbGw7XG4gICAqIEByZXR1cm5zIE9ic2VydmFibGU8YW55PiAtICBSZXR1cm5zIGFuIE9ic2VydmFibGU8YW55PiAuXG4gICAqL1xuICBwcm90ZWN0ZWQgZXh0cmFjdERhdGEocmVzOiBhbnkpOiBhbnkge1xuICAgIGlmICggIXJlcyApIHtcbiAgICAgIHJldHVybiBudWxsO1xuICAgIH1cblxuICAgIGlmICggcmVzLmJvZHkgPT09IHVuZGVmaW5lZCApIHtcbiAgICAgIHJldHVybiByZXM7XG4gICAgfVxuXG4gICAgcmV0dXJuIHJlcy5ib2R5O1xuICB9XG5cbiAgLyoqXG4gICAqIERpc3BsYXkgYSBub3RpZmljYXRpb24gdGhhdCBpbmZvcm1zIHRoZSB1c2VyIHRoYXQgYW4gSFRUUCBlcnJvciBvY2N1cnJlZC5cbiAgICogTXVzdCBiZSBjb25zdW1lZCBieSB0aGUgY2hpbGQgc2VydmljZSwgZG9lcyBOT1QgZ2V0IGNhbGxlZCBieSBkZWZhdWx0LlxuICAgKiBAcGFyYW0gbXNnIFVzZXIgZnJpZW5kbHkgbWVzc2FnZSB0byBkaXNwbGF5IHRvIHRoZSB1c2VyXG4gICAqL1xuICBwcm90ZWN0ZWQgZGlzcGxheUVycm9yTXNnKG1zZzogc3RyaW5nKTogdm9pZCB7XG4gICAgLy8gVE9ETzogdGhpcyBpcyBhIHBsYWNlaG9sZGVyLCBhIHNuYWNrYmFyIGlzIHdoYXQgc2hvdWxkIGJlIHVzZWRcbiAgfVxuXG4gIC8qKlxuICAgKiBTZW5kcyBhIHZlcmJvc2UgZXJyb3IgbWVzc2FnZSB0byB0aGUgY29uc29sZSBmb3IgZnVydGhlciBkZWJ1Z2dpbmdcbiAgICogQHBhcmFtICBlcnJvciAtIEh0dHBFcnJvclJlc3BvbnNlIGVycm9yIG9iamVjdCB0aGF0IGlzIHN1cHBsaWVkIHZpYSBIdHRwQ2xpZW50XG4gICAqIEBwYXJhbSBtZXRob2QgLSB0aGUgSFRUUCBtZXRob2QgdXNlZFxuICAgKiBAcmV0dXJucyBgSHR0cEVycm9yYCB0aGF0IHdhcyBwYXNzZWQgaW4gb3IgYSBwbGFjZWhvbGRlciBpZiBub25lIHdhcyBwcm92aWRlZFxuICAgKi9cbiAgcHJvdGVjdGVkIGxvZ0Vycm9yKFxuICAgICAgICAgICAgZXJyb3I6IEh0dHBFcnJvclJlc3BvbnNlLFxuICAgICAgICAgICAgbWV0aG9kOiAnR0VUJyB8ICdQT1NUJyB8ICdQVVQnIHwgJ0RFTEVURSdcbiAgICAgICAgICApOiBPYnNlcnZhYmxlPEh0dHBFcnJvclJlc3BvbnNlPiB7XG4gICAgaWYgKCBpc051bGxPclVuZGVmaW5lZChlcnJvcikgfHwgXy5pc0VtcHR5KGVycm9yKSApIHtcbiAgICAgIGNvbnN0IGVyciA9IG5ldyBFcnJvcihcbiAgICAgICAgJ0FuIHVua25vd24gZXJyb3Igb2NjdXJyZWQgYW5kIG5vIGVycm9yIHdhcyBzdXBwbGllZCB0byB0aGUgaGFuZGxlcidcbiAgICAgICk7XG4gICAgICBlcnJvciA9IG5ldyBIdHRwRXJyb3JSZXNwb25zZSh7IGVycm9yOiBlcnIgfSk7XG4gICAgfVxuXG4gICAgY29uc3QgbWVzc2FnZSA9IGVycm9yLm1lc3NhZ2UgfHwgJ0FuIGVycm9yIG9jY3VycmVkIGFuZCBubyBtZXNzYWdlIHdhcyBzdXBwbGllZCB0byB0aGUgaGFuZGxlcic7XG4gICAgY29uc3QgdXJsID0gZXJyb3IudXJsIHx8ICc/Pz8nOyAvLyBUT0RPOiBpZiB3ZSBnZXQgYSBsb3Qgb2YgPz8/J3Mgd2UgY2FuIHBhc3MgaW4gdGhlIFVSTFxuICAgIGNvbnN0IHN0YXR1cyA9IGVycm9yLnN0YXR1cyB8fCAnPz8/JztcbiAgICBjb25zdCBzdGF0dXNUZXh0ID0gZXJyb3Iuc3RhdHVzVGV4dCB8fCAnPz8/JztcbiAgICBjb25zdCBoZWFkZXJzID0gdGhpcy5oZWFkZXJzMnN0cihlcnJvci5oZWFkZXJzKSB8fCAnPz8/JztcbiAgICBjb25zdCBzdGFjayA9IGVycm9yLmVycm9yLnN0YWNrIHx8ICdObyBDYWxsIFN0YWNrIFByb3ZpZGVkJztcbiAgICBjb25zdCBtb21lbnQgPSBtb21lbnRfO1xuXG5cbiAgICAvLyBUT0RPOiByZW1vdGUgbG9nZ2luZyB3b3VsZCBnbyBoZXJlIVxuXG4gICAgLy8gRXJyb3IgTWVzc2FnZSBoZXJlIVxuICAgIC8vIENvbnRleHQ6IGh0dHA6Ly9sb2NhbGhvc3Q6NDIwMC8jL1xuICAgIC8vIEFjdGlvbjogIEdFVCAvc2FwaGlyYS9hcGkvcGluZ1xuICAgIC8vIFN0YXR1czogIDQwMCBzdGF0dXNUZXh0XG4gICAgLy8gVGltZTogICAgMjAxOS0wNC0yM1QxNToyODowMCs4XG4gICAgLy8gSGVhZGVyczoge2ZvbzogYmFyfVxuICAgIC8vIGNhbGwgc3RhY2tcbiAgICAvLyA8dGhpcyBsaW5lIGludGVudGlvbmFsbHkgbGVmdCBibGFuaz5cblxuICAgIGNvbnNvbGUuZXJyb3IoXG4gICAgICBtZXNzYWdlICsgJ1xcbicgK1xuICAgICAgYENvbnRleHQ6ICR7d2luZG93LmxvY2F0aW9uLmhyZWZ9XFxuYCArXG4gICAgICBgQWN0aW9uOiAgJHttZXRob2R9ICR7dXJsfVxcbmAgK1xuICAgICAgYFN0YXR1czogICR7c3RhdHVzfSAke3N0YXR1c1RleHR9XFxuYCArXG4gICAgICBgVGltZTogICAgJHttb21lbnQoKS50b0lTT1N0cmluZyh0cnVlKX1cXG5gICtcbiAgICAgIGBIZWFkZXJzOiAke2hlYWRlcnN9XFxuYCArXG4gICAgICBzdGFjayArICAnXFxuXFxuJ1xuICAgICk7XG5cbiAgICByZXR1cm4gdGhyb3dFcnJvcihlcnJvcik7XG4gIH1cblxuICAvKipcbiAgICogTWFwcyBgSHR0cEhlYWRlcnNgIHRvIHNpbXBsZSBKU09OIHN0cmluZyBmb3IgZGVidWdnaW5nXG4gICAqL1xuICBwcml2YXRlIGhlYWRlcnMyc3RyKCBoZWFkZXJzOiBIdHRwSGVhZGVycyApOiBzdHJpbmcge1xuICAgIGlmICggaXNOdWxsT3JVbmRlZmluZWQoaGVhZGVycykgKSB7XG4gICAgICByZXR1cm4gbnVsbDtcbiAgICB9XG5cbiAgICBjb25zdCBqc29uID0ge307XG4gICAgZm9yICggY29uc3Qga2V5IG9mIGhlYWRlcnMua2V5cygpICkge1xuICAgICAgY29uc3QgdmFsdWVzID0gaGVhZGVycy5nZXRBbGwoa2V5KTtcbiAgICAgIGlmICggdmFsdWVzLmxlbmd0aCA9PT0gMSApIHtcbiAgICAgICAganNvbltrZXldID0gdmFsdWVzWzBdO1xuICAgICAgICBjb250aW51ZTtcbiAgICAgIH1cblxuICAgICAganNvbltrZXldID0gWyAuLi52YWx1ZXMgXTtcbiAgICB9XG5cblxuICAgIHJldHVybiBKU09OLnN0cmluZ2lmeShqc29uLCBudWxsLCAyKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBNYXN0ZXIgR0VUIGZ1bmN0aW9uLlxuICAgKiBAcGFyYW0gIHBhdGggLSBzdHJpbmcgcGF0aCBvZiB0aGUgZW5kcG9pbnQ7XG4gICAqIEBwYXJhbSAgaGVhZGVycyAtIGN1c3RvbSBoZWFkZXJzIHN1cHBsaWVkIHRvIHRoZSBjYWxsO1xuICAgKiBAcmV0dXJucyBPYnNlcnZhYmxlPGFueT4gLSAgUmV0dXJucyBhbiBPYnNlcnZhYmxlPGFueT4gLlxuICAgKi9cbiAgcHJvdGVjdGVkIGdldChcbiAgICAgICAgICAgICAgcGF0aDogc3RyaW5nLFxuICAgICAgICAgICAgICBoZWFkZXJzPzogeyBba2V5OiBzdHJpbmddOiBzdHJpbmcgfVxuICAgICAgICAgICAgKTogT2JzZXJ2YWJsZTxhbnk+IHtcbiAgICBjb25zdCBvcHRpb25zOiBhbnkgPSB7IHdpdGhDcmVkZW50aWFsczogdHJ1ZSB9O1xuXG4gICAgaWYgKCBoZWFkZXJzICkge1xuICAgICAgb3B0aW9ucy5oZWFkZXJzID0gaGVhZGVycztcbiAgICB9XG5cbiAgICByZXR1cm4gdGhpcy5odHRwXG4gICAgICAuZ2V0KHRoaXMudXJsUm9vdCArIHBhdGgsIG9wdGlvbnMpXG4gICAgICAucGlwZShcbiAgICAgICAgbWFwKCB0aGlzLmV4dHJhY3REYXRhICksXG4gICAgICAgIGNhdGNoRXJyb3IoIGVyciA9PiB0aGlzLmxvZ0Vycm9yKGVyciwgJ0dFVCcpIClcbiAgICAgICk7XG4gIH1cblxuICAvKipcbiAgICogTWFzdGVyIFBVVCBmdW5jdGlvbi5cbiAgICogQHBhcmFtICBwYXRoIC0gc3RyaW5nIHBhdGggb2YgdGhlIGVuZHBvaW50O1xuICAgKiBAcGFyYW0gIG9iaiAtIG9iamVjdCBmb3Igd2hpY2ggeW91IHdhbnQgdG8gcHV0O1xuICAgKiBAcGFyYW0gIGhlYWRlcnMgLSBjdXN0b20gaGVhZGVycyBzdXBwbGllZCB0byB0aGUgY2FsbDtcbiAgICogQHJldHVybnMgT2JzZXJ2YWJsZTxhbnk+IC0gIFJldHVybnMgYW4gT2JzZXJ2YWJsZTxhbnk+IC5cbiAgICovXG4gIHByb3RlY3RlZCBwdXQoXG4gICAgICAgICAgICAgIHBhdGg6IHN0cmluZyxcbiAgICAgICAgICAgICAgb2JqOiBhbnksXG4gICAgICAgICAgICAgIGhlYWRlcnM/OiB7IFtrZXk6IHN0cmluZ106IHN0cmluZyB9XG4gICAgICAgICAgICApOiBPYnNlcnZhYmxlPGFueT4ge1xuICAgIGNvbnN0IG9wdGlvbnM6IGFueSA9IHsgd2l0aENyZWRlbnRpYWxzOiB0cnVlIH07XG5cbiAgICBpZiAoIGhlYWRlcnMgKSB7XG4gICAgICBvcHRpb25zLmhlYWRlcnMgPSBoZWFkZXJzO1xuICAgIH1cblxuICAgIHJldHVybiB0aGlzLmh0dHBcbiAgICAgIC5wdXQodGhpcy51cmxSb290ICsgcGF0aCwgb2JqLCBvcHRpb25zKVxuICAgICAgLnBpcGUoXG4gICAgICAgIG1hcCggdGhpcy5leHRyYWN0RGF0YSApLFxuICAgICAgICBjYXRjaEVycm9yKCBlcnIgPT4gdGhpcy5sb2dFcnJvcihlcnIsICdQVVQnKSApXG4gICAgICApO1xuICB9XG5cbiAgLyoqXG4gICAqIE1hc3RlciBQT1NUIGZ1bmN0aW9uLlxuICAgKiBAcGFyYW0gIHBhdGggLSBzdHJpbmcgcGF0aCBvZiB0aGUgZW5kcG9pbnQ7XG4gICAqIEBwYXJhbSAgb2JqIC0gb2JqZWN0IGZvciB3aGljaCB5b3Ugd2FudCB0byBwdXQ7XG4gICAqIEBwYXJhbSAgaGVhZGVycyAtIGN1c3RvbSBoZWFkZXJzIHN1cHBsaWVkIHRvIHRoZSBjYWxsO1xuICAgKiBAcmV0dXJucyBPYnNlcnZhYmxlPGFueT4gLSAgUmV0dXJucyBhbiBPYnNlcnZhYmxlPGFueT4gLlxuICAgKi9cbiAgcHJvdGVjdGVkIHBvc3QoXG4gICAgICAgICAgICAgIHBhdGg6IHN0cmluZyxcbiAgICAgICAgICAgICAgb2JqOiBhbnksXG4gICAgICAgICAgICAgIGhlYWRlcnM/OiB7IFtrZXk6IHN0cmluZ106IHN0cmluZyB9XG4gICAgICAgICAgICApOiBPYnNlcnZhYmxlPGFueT4ge1xuICAgIGNvbnN0IG9wdGlvbnM6IGFueSA9IHsgd2l0aENyZWRlbnRpYWxzOiB0cnVlIH07XG5cbiAgICBpZiAoIGhlYWRlcnMgKSB7XG4gICAgICBvcHRpb25zLmhlYWRlcnMgPSBoZWFkZXJzO1xuICAgIH1cblxuICAgIHJldHVybiB0aGlzLmh0dHBcbiAgICAgIC5wb3N0KHRoaXMudXJsUm9vdCArIHBhdGgsIG9iaiwgb3B0aW9ucylcbiAgICAgIC5waXBlKFxuICAgICAgICBtYXAoIHRoaXMuZXh0cmFjdERhdGEgKSxcbiAgICAgICAgbWFwKCByZXMgPT4gcmVzIHx8IGlzTnVsbE9yVW5kZWZpbmVkKHJlcykgKSwgLy8gVE9ETzogTmVlZCB0byBpbnZlc3RpZ2F0ZSBmdXJ0aGVyXG4gICAgICAgIGNhdGNoRXJyb3IoIGVyciA9PiB0aGlzLmxvZ0Vycm9yKGVyciwgJ1BPU1QnKSApXG4gICAgICApO1xuICB9XG5cbiAgLyoqXG4gICAqIE1hc3RlciBERUxFVEUgZnVuY3Rpb24uXG4gICAqIEBwYXJhbSAgcGF0aCAtIHN0cmluZyBwYXRoIG9mIHRoZSBlbmRwb2ludDtcbiAgICogQHBhcmFtICBoZWFkZXJzIC0gY3VzdG9tIGhlYWRlcnMgc3VwcGxpZWQgdG8gdGhlIGNhbGw7XG4gICAqIEByZXR1cm5zIE9ic2VydmFibGU8YW55PiAtICBSZXR1cm5zIGFuIE9ic2VydmFibGU8YW55PiAuXG4gICAqL1xuICBwcm90ZWN0ZWQgZGVsZXRlKFxuICAgICAgICAgICAgICBwYXRoOiBzdHJpbmcsXG4gICAgICAgICAgICAgIGhlYWRlcnM/OiB7IFtrZXk6IHN0cmluZ106IHN0cmluZyB9XG4gICAgICAgICAgICApOiBPYnNlcnZhYmxlPGFueT4ge1xuICAgIGNvbnN0IG9wdGlvbnM6IGFueSA9IHsgd2l0aENyZWRlbnRpYWxzOiB0cnVlIH07XG5cbiAgICBpZiAoIGhlYWRlcnMgKSB7XG4gICAgICBvcHRpb25zLmhlYWRlcnMgPSBoZWFkZXJzO1xuICAgIH1cblxuICAgIHJldHVybiB0aGlzLmh0dHBcbiAgICAgIC5kZWxldGUodGhpcy51cmxSb290ICsgcGF0aCwgb3B0aW9ucyApXG4gICAgICAucGlwZShcbiAgICAgICAgbWFwKCB0aGlzLmV4dHJhY3REYXRhICksXG4gICAgICAgIG1hcCggcmVzID0+IHJlcyB8fCBpc051bGxPclVuZGVmaW5lZChyZXMpICksIC8vIFRPRE86IE5lZWQgdG8gaW52ZXN0aWdhdGUgZnVydGhlclxuICAgICAgICBjYXRjaEVycm9yKCBlcnIgPT4gdGhpcy5sb2dFcnJvcihlcnIsICdERUxFVEUnKSApXG4gICAgICApO1xuICB9XG5cbn1cbiJdfQ==