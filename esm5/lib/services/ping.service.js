/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { DataService } from './data.service';
import { HttpClient } from '@angular/common/http';
import { catchError, mergeMap, retry, tap } from 'rxjs/operators';
import { interval, BehaviorSubject, throwError, concat, EMPTY } from 'rxjs';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common/http";
var PingService = /** @class */ (function (_super) {
    tslib_1.__extends(PingService, _super);
    function PingService(http) {
        var _this = _super.call(this, http) || this;
        _this.pingSuffix = 'ping';
        _this.pingStatus = new BehaviorSubject(true);
        /**
         * Round trip time in ms
         */
        _this.tripTime = 0;
        return _this;
    }
    /**
     * @return {?}
     */
    PingService.prototype.start = /**
     * @return {?}
     */
    function () {
        var _this = this;
        // only one ping
        if (this.ping) {
            return;
        }
        /** @type {?} */
        var startTime;
        /** @type {?} */
        var firstPing = _super.prototype.get.call(this, this.pingSuffix).pipe(catchError((/**
         * @return {?}
         */
        function () {
            _this.pingStatus.next(false);
            return EMPTY;
        })));
        /** @type {?} */
        var recurringPing = interval(10000).pipe(tap((/**
         * @return {?}
         */
        function () { return startTime = performance.now(); })), mergeMap((/**
         * @return {?}
         */
        function () { return _super.prototype.get.call(_this, _this.pingSuffix); })), catchError((/**
         * @param {?} error
         * @return {?}
         */
        function (error) {
            _this.pingStatus.next(false);
            return throwError(error);
        })), retry() // DON'T STOP ME NOW!
        );
        this.ping = concat(firstPing, recurringPing)
            .subscribe((/**
         * @param {?} apiVersion
         * @return {?}
         */
        function (apiVersion) {
            _this.tripTime = performance.now() - startTime;
            _this.pingStatus.next(true);
            _this.apiVersion = apiVersion;
        }));
    };
    /**
     * @return {?}
     */
    PingService.prototype.status = /**
     * @return {?}
     */
    function () {
        return this.pingStatus.asObservable();
    };
    PingService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    PingService.ctorParameters = function () { return [
        { type: HttpClient }
    ]; };
    /** @nocollapse */ PingService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function PingService_Factory() { return new PingService(i0.ɵɵinject(i1.HttpClient)); }, token: PingService, providedIn: "root" });
    return PingService;
}(DataService));
export { PingService };
if (false) {
    /**
     * @type {?}
     * @private
     */
    PingService.prototype.ping;
    /**
     * @type {?}
     * @private
     */
    PingService.prototype.pingSuffix;
    /**
     * @type {?}
     * @private
     */
    PingService.prototype.pingStatus;
    /**
     * Round trip time in ms
     * @type {?}
     */
    PingService.prototype.tripTime;
    /**
     * Backend API version
     * @type {?}
     */
    PingService.prototype.apiVersion;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGluZy5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGhwdGVjaC9zYXBoaXJhLXNoYXJlZC1saWIvIiwic291cmNlcyI6WyJsaWIvc2VydmljZXMvcGluZy5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMzQyxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFDN0MsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBQ2xELE9BQU8sRUFBRSxVQUFVLEVBQUUsUUFBUSxFQUFFLEtBQUssRUFBRSxHQUFHLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUNsRSxPQUFPLEVBQUUsUUFBUSxFQUFjLGVBQWUsRUFBRSxVQUFVLEVBQWdCLE1BQU0sRUFBRSxLQUFLLEVBQUUsTUFBTSxNQUFNLENBQUM7OztBQUV0RztJQUdpQyx1Q0FBVztJQVcxQyxxQkFDRSxJQUFnQjtRQURsQixZQUdFLGtCQUFNLElBQUksQ0FBQyxTQUNaO1FBYk8sZ0JBQVUsR0FBRyxNQUFNLENBQUM7UUFDcEIsZ0JBQVUsR0FBRyxJQUFJLGVBQWUsQ0FBQyxJQUFJLENBQUMsQ0FBQzs7OztRQUd4QyxjQUFRLEdBQUcsQ0FBQyxDQUFDOztJQVNwQixDQUFDOzs7O0lBRUQsMkJBQUs7OztJQUFMO1FBQUEsaUJBOEJDO1FBN0JDLGdCQUFnQjtRQUNoQixJQUFLLElBQUksQ0FBQyxJQUFJLEVBQUc7WUFDZixPQUFPO1NBQ1I7O1lBQ0csU0FBUzs7WUFFUCxTQUFTLEdBQUcsaUJBQU0sR0FBRyxZQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxJQUFJLENBQy9DLFVBQVU7OztRQUFFO1lBQ1YsS0FBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDNUIsT0FBTyxLQUFLLENBQUM7UUFDZixDQUFDLEVBQUMsQ0FDSDs7WUFFSyxhQUFhLEdBQUcsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDLElBQUksQ0FDeEMsR0FBRzs7O1FBQUUsY0FBTSxPQUFBLFNBQVMsR0FBRyxXQUFXLENBQUMsR0FBRyxFQUFFLEVBQTdCLENBQTZCLEVBQUUsRUFDMUMsUUFBUTs7O1FBQUUsY0FBTSxPQUFBLGlCQUFNLEdBQUcsYUFBQyxLQUFJLENBQUMsVUFBVSxDQUFDLEVBQTFCLENBQTBCLEVBQUUsRUFDNUMsVUFBVTs7OztRQUFFLFVBQUEsS0FBSztZQUNmLEtBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQzVCLE9BQU8sVUFBVSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQzNCLENBQUMsRUFBQyxFQUNGLEtBQUssRUFBRSxDQUFDLHFCQUFxQjtTQUM5QjtRQUVELElBQUksQ0FBQyxJQUFJLEdBQUcsTUFBTSxDQUFDLFNBQVMsRUFBRSxhQUFhLENBQUM7YUFDekMsU0FBUzs7OztRQUFFLFVBQUEsVUFBVTtZQUNwQixLQUFJLENBQUMsUUFBUSxHQUFHLFdBQVcsQ0FBQyxHQUFHLEVBQUUsR0FBRyxTQUFTLENBQUM7WUFDOUMsS0FBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDM0IsS0FBSSxDQUFDLFVBQVUsR0FBRyxVQUFVLENBQUM7UUFDL0IsQ0FBQyxFQUFDLENBQUM7SUFDUCxDQUFDOzs7O0lBRUQsNEJBQU07OztJQUFOO1FBQ0UsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLFlBQVksRUFBRSxDQUFDO0lBQ3hDLENBQUM7O2dCQXRERixVQUFVLFNBQUM7b0JBQ1YsVUFBVSxFQUFFLE1BQU07aUJBQ25COzs7O2dCQU5RLFVBQVU7OztzQkFGbkI7Q0E2REMsQUF2REQsQ0FHaUMsV0FBVyxHQW9EM0M7U0FwRFksV0FBVzs7Ozs7O0lBQ3RCLDJCQUEyQjs7Ozs7SUFDM0IsaUNBQTRCOzs7OztJQUM1QixpQ0FBK0M7Ozs7O0lBRy9DLCtCQUFvQjs7Ozs7SUFHcEIsaUNBQTBCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgRGF0YVNlcnZpY2UgfSBmcm9tICcuL2RhdGEuc2VydmljZSc7XG5pbXBvcnQgeyBIdHRwQ2xpZW50IH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xuaW1wb3J0IHsgY2F0Y2hFcnJvciwgbWVyZ2VNYXAsIHJldHJ5LCB0YXAgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XG5pbXBvcnQgeyBpbnRlcnZhbCwgT2JzZXJ2YWJsZSwgQmVoYXZpb3JTdWJqZWN0LCB0aHJvd0Vycm9yLCBTdWJzY3JpcHRpb24sIGNvbmNhdCwgRU1QVFkgfSBmcm9tICdyeGpzJztcblxuQEluamVjdGFibGUoe1xuICBwcm92aWRlZEluOiAncm9vdCdcbn0pXG5leHBvcnQgY2xhc3MgUGluZ1NlcnZpY2UgZXh0ZW5kcyBEYXRhU2VydmljZSB7XG4gIHByaXZhdGUgcGluZzogU3Vic2NyaXB0aW9uO1xuICBwcml2YXRlIHBpbmdTdWZmaXggPSAncGluZyc7XG4gIHByaXZhdGUgcGluZ1N0YXR1cyA9IG5ldyBCZWhhdmlvclN1YmplY3QodHJ1ZSk7XG5cbiAgLyoqIFJvdW5kIHRyaXAgdGltZSBpbiBtcyAqL1xuICBwdWJsaWMgdHJpcFRpbWUgPSAwO1xuXG4gIC8qKiBCYWNrZW5kIEFQSSB2ZXJzaW9uICovXG4gIHB1YmxpYyBhcGlWZXJzaW9uOiBzdHJpbmc7XG5cbiAgY29uc3RydWN0b3IoXG4gICAgaHR0cDogSHR0cENsaWVudFxuICApIHtcbiAgICBzdXBlcihodHRwKTtcbiAgfVxuXG4gIHN0YXJ0KCk6IHZvaWQge1xuICAgIC8vIG9ubHkgb25lIHBpbmdcbiAgICBpZiAoIHRoaXMucGluZyApIHtcbiAgICAgIHJldHVybjtcbiAgICB9XG4gICAgbGV0IHN0YXJ0VGltZTtcblxuICAgIGNvbnN0IGZpcnN0UGluZyA9IHN1cGVyLmdldCh0aGlzLnBpbmdTdWZmaXgpLnBpcGUoXG4gICAgICBjYXRjaEVycm9yKCAoKSA9PiB7XG4gICAgICAgIHRoaXMucGluZ1N0YXR1cy5uZXh0KGZhbHNlKTtcbiAgICAgICAgcmV0dXJuIEVNUFRZO1xuICAgICAgfSlcbiAgICApO1xuXG4gICAgY29uc3QgcmVjdXJyaW5nUGluZyA9IGludGVydmFsKDEwMDAwKS5waXBlKFxuICAgICAgdGFwKCAoKSA9PiBzdGFydFRpbWUgPSBwZXJmb3JtYW5jZS5ub3coKSApLFxuICAgICAgbWVyZ2VNYXAoICgpID0+IHN1cGVyLmdldCh0aGlzLnBpbmdTdWZmaXgpICksXG4gICAgICBjYXRjaEVycm9yKCBlcnJvciA9PiB7XG4gICAgICAgIHRoaXMucGluZ1N0YXR1cy5uZXh0KGZhbHNlKTtcbiAgICAgICAgcmV0dXJuIHRocm93RXJyb3IoZXJyb3IpO1xuICAgICAgfSksXG4gICAgICByZXRyeSgpIC8vIERPTidUIFNUT1AgTUUgTk9XIVxuICAgICk7XG5cbiAgICB0aGlzLnBpbmcgPSBjb25jYXQoZmlyc3RQaW5nLCByZWN1cnJpbmdQaW5nKVxuICAgICAgLnN1YnNjcmliZSggYXBpVmVyc2lvbiA9PiB7XG4gICAgICAgIHRoaXMudHJpcFRpbWUgPSBwZXJmb3JtYW5jZS5ub3coKSAtIHN0YXJ0VGltZTtcbiAgICAgICAgdGhpcy5waW5nU3RhdHVzLm5leHQodHJ1ZSk7XG4gICAgICAgIHRoaXMuYXBpVmVyc2lvbiA9IGFwaVZlcnNpb247XG4gICAgICB9KTtcbiAgfVxuXG4gIHN0YXR1cygpOiBPYnNlcnZhYmxlPGJvb2xlYW4+IHtcbiAgICByZXR1cm4gdGhpcy5waW5nU3RhdHVzLmFzT2JzZXJ2YWJsZSgpO1xuICB9XG59XG4iXX0=