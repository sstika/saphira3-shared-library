/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { of, BehaviorSubject } from 'rxjs';
import { catchError, tap, skipWhile } from 'rxjs/operators';
import { DataService } from '../data.service';
import { isNullOrUndefined } from 'util';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common/http";
var CompanyService = /** @class */ (function (_super) {
    tslib_1.__extends(CompanyService, _super);
    function CompanyService(http) {
        var _this = _super.call(this, http) || this;
        _this.allCompanies = 'repositories/company/getAll';
        _this.getAllUnderCompany = 'repositories/company/getAllUnderCompany';
        _this.addComp = 'repositories/company/add';
        _this.getOneComp = 'repositories/company/getOne';
        _this.deleteComp = 'repositories/company/delete';
        _this.companySnapshot = 'utils/getCompanySnapshot';
        _this.restoreFromCompanySnapshot = 'utils/restoreFromCompanySnapshot';
        _this.downloadAtt = 'attachments/search/findById';
        _this.getOneMeta = 'repositories/company/getOneMeta';
        _this.update = 'repositories/company/update';
        _this.lastModifiedDate = 'repositories/company/lastModifiedDate';
        _this.currentCompany = new BehaviorSubject(null);
        return _this;
    }
    /**
     * @return {?}
     */
    CompanyService.prototype.getAllCompanies = /**
     * @return {?}
     */
    function () {
        return _super.prototype.get.call(this, this.allCompanies).pipe(catchError((/**
         * @param {?} err
         * @return {?}
         */
        function (err) { return of(false); })));
    };
    /**
     * @param {?} compId
     * @return {?}
     */
    CompanyService.prototype.getAllForCompany = /**
     * @param {?} compId
     * @return {?}
     */
    function (compId) {
        return _super.prototype.get.call(this, this.getAllUnderCompany + "?id=" + compId).pipe(catchError((/**
         * @param {?} err
         * @return {?}
         */
        function (err) { return of(false); })));
    };
    /**
     * @param {?} compId
     * @return {?}
     */
    CompanyService.prototype.getOne = /**
     * @param {?} compId
     * @return {?}
     */
    function (compId) {
        var _this = this;
        return _super.prototype.get.call(this, this.getOneComp + "?id=" + compId).pipe(tap((/**
         * @param {?} company
         * @return {?}
         */
        function (company) { return _this.currentCompany.next(company); })), catchError((/**
         * @param {?} err
         * @return {?}
         */
        function (err) { return of(false); })));
    };
    /**
     * @return {?}
     */
    CompanyService.prototype.getCurrent = /**
     * @return {?}
     */
    function () {
        return this.currentCompany
            .asObservable()
            .pipe(skipWhile((/**
         * @param {?} company
         * @return {?}
         */
        function (company) { return isNullOrUndefined(company); })));
    };
    /**
     * @param {?} compId
     * @return {?}
     */
    CompanyService.prototype.delete = /**
     * @param {?} compId
     * @return {?}
     */
    function (compId) {
        return _super.prototype.delete.call(this, this.deleteComp + "?id=" + compId).pipe(catchError((/**
         * @param {?} err
         * @return {?}
         */
        function (err) { return of(false); })));
    };
    /**
     * @param {?} newCompObj
     * @param {?=} headerObj
     * @return {?}
     */
    CompanyService.prototype.add = /**
     * @param {?} newCompObj
     * @param {?=} headerObj
     * @return {?}
     */
    function (newCompObj, headerObj) {
        if (headerObj === void 0) { headerObj = {}; }
        return this
            .post(this.addComp, newCompObj, headerObj).pipe(catchError((/**
         * @param {?} err
         * @return {?}
         */
        function (err) { return of(false); })));
    };
    /**
     * @param {?} compId
     * @return {?}
     */
    CompanyService.prototype.getCompanySnapshot = /**
     * @param {?} compId
     * @return {?}
     */
    function (compId) {
        return _super.prototype.get.call(this, this.companySnapshot + "?companyId=" + compId)
            .pipe(catchError((/**
         * @param {?} err
         * @return {?}
         */
        function (err) { return of(false); })));
    };
    /**
     * @param {?} bodyObj
     * @param {?=} headerObj
     * @return {?}
     */
    CompanyService.prototype.postCompanyUpdate = /**
     * @param {?} bodyObj
     * @param {?=} headerObj
     * @return {?}
     */
    function (bodyObj, headerObj) {
        if (headerObj === void 0) { headerObj = {}; }
        return _super.prototype.post.call(this, this.update, bodyObj, headerObj).pipe(catchError((/**
         * @param {?} err
         * @return {?}
         */
        function (err) { return of(false); })));
    };
    /**
     * @return {?}
     */
    CompanyService.prototype.returnFileTreeCompanies = /**
     * @return {?}
     */
    function () {
        var _this = this;
        return _super.prototype.get.call(this, this.allCompanies).pipe(tap((/**
         * @param {?} comps
         * @return {?}
         */
        function (comps) {
            return _this.companiesFiltered = _this.cleanCompanies(comps);
        })), catchError((/**
         * @param {?} err
         * @return {?}
         */
        function (err) { return of(false); })));
    };
    /**
     * @param {?} companies
     * @return {?}
     */
    CompanyService.prototype.cleanCompanies = /**
     * @param {?} companies
     * @return {?}
     */
    function (companies) {
        if (!companies) {
            return;
        }
        /** @type {?} */
        var uniqueCompanyNames = [];
        return companies.reduce((/**
         * @param {?} newArr
         * @param {?} comp
         * @return {?}
         */
        function (newArr, comp) {
            /** @type {?} */
            var incrementer;
            if (!uniqueCompanyNames.includes(comp.name)) {
                uniqueCompanyNames.push(comp.name);
                comp['hierarchy'] = [comp['commonName'] + " : " + comp['name']];
            }
            else {
                comp['hierarchy'] = [comp['commonName'] + " : " + comp['name'] + (incrementer += 1)];
            }
            newArr.push(comp);
            return newArr;
        }), []);
    };
    /**
     * @return {?}
     */
    CompanyService.prototype.returnFilteredComps = /**
     * @return {?}
     */
    function () {
        return this.companiesFiltered;
    };
    CompanyService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    CompanyService.ctorParameters = function () { return [
        { type: HttpClient }
    ]; };
    /** @nocollapse */ CompanyService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function CompanyService_Factory() { return new CompanyService(i0.ɵɵinject(i1.HttpClient)); }, token: CompanyService, providedIn: "root" });
    return CompanyService;
}(DataService));
export { CompanyService };
if (false) {
    /**
     * @type {?}
     * @protected
     */
    CompanyService.prototype.allCompanies;
    /**
     * @type {?}
     * @protected
     */
    CompanyService.prototype.getAllUnderCompany;
    /**
     * @type {?}
     * @protected
     */
    CompanyService.prototype.addComp;
    /**
     * @type {?}
     * @protected
     */
    CompanyService.prototype.getOneComp;
    /**
     * @type {?}
     * @protected
     */
    CompanyService.prototype.deleteComp;
    /**
     * @type {?}
     * @protected
     */
    CompanyService.prototype.companySnapshot;
    /**
     * @type {?}
     * @protected
     */
    CompanyService.prototype.restoreFromCompanySnapshot;
    /**
     * @type {?}
     * @protected
     */
    CompanyService.prototype.downloadAtt;
    /**
     * @type {?}
     * @protected
     */
    CompanyService.prototype.getOneMeta;
    /**
     * @type {?}
     * @protected
     */
    CompanyService.prototype.update;
    /**
     * @type {?}
     * @protected
     */
    CompanyService.prototype.lastModifiedDate;
    /**
     * @type {?}
     * @private
     */
    CompanyService.prototype.currentCompany;
    /** @type {?} */
    CompanyService.prototype.companiesFiltered;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tcGFueS5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGhwdGVjaC9zYXBoaXJhLXNoYXJlZC1saWIvIiwic291cmNlcyI6WyJsaWIvc2VydmljZXMvYWRtaW4tc2VydmljZXMvY29tcGFueS5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMzQyxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFDbEQsT0FBTyxFQUFjLEVBQUUsRUFBRSxlQUFlLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFDdkQsT0FBTyxFQUFFLFVBQVUsRUFBTyxHQUFHLEVBQUUsU0FBUyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFFakUsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBRTlDLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLE1BQU0sQ0FBQzs7O0FBSXpDO0lBR29DLDBDQUFXO0lBZ0I3Qyx3QkFBWSxJQUFnQjtRQUE1QixZQUNFLGtCQUFNLElBQUksQ0FBQyxTQUNaO1FBakJTLGtCQUFZLEdBQUcsNkJBQTZCLENBQUM7UUFDN0Msd0JBQWtCLEdBQUcseUNBQXlDLENBQUM7UUFDL0QsYUFBTyxHQUFHLDBCQUEwQixDQUFDO1FBQ3JDLGdCQUFVLEdBQUcsNkJBQTZCLENBQUM7UUFDM0MsZ0JBQVUsR0FBRyw2QkFBNkIsQ0FBQztRQUMzQyxxQkFBZSxHQUFHLDBCQUEwQixDQUFDO1FBQzdDLGdDQUEwQixHQUFHLGtDQUFrQyxDQUFDO1FBQ2hFLGlCQUFXLEdBQUcsNkJBQTZCLENBQUM7UUFDNUMsZ0JBQVUsR0FBRyxpQ0FBaUMsQ0FBQztRQUMvQyxZQUFNLEdBQUcsNkJBQTZCLENBQUM7UUFDdkMsc0JBQWdCLEdBQUcsdUNBQXVDLENBQUM7UUFFN0Qsb0JBQWMsR0FBeUIsSUFBSSxlQUFlLENBQU0sSUFBSSxDQUFDLENBQUM7O0lBSzlFLENBQUM7Ozs7SUFHTSx3Q0FBZTs7O0lBQXRCO1FBQ0UsT0FBTyxpQkFDSixHQUFHLFlBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLElBQUksQ0FDMUIsVUFBVTs7OztRQUFDLFVBQUEsR0FBRyxJQUFJLE9BQUEsRUFBRSxDQUFDLEtBQUssQ0FBQyxFQUFULENBQVMsRUFBQyxDQUM3QixDQUFDO0lBQ04sQ0FBQzs7Ozs7SUFFTSx5Q0FBZ0I7Ozs7SUFBdkIsVUFBd0IsTUFBTTtRQUM1QixPQUFPLGlCQUNKLEdBQUcsWUFBSSxJQUFJLENBQUMsa0JBQWtCLFlBQU8sTUFBUSxDQUFDLENBQUMsSUFBSSxDQUNsRCxVQUFVOzs7O1FBQUMsVUFBQSxHQUFHLElBQUksT0FBQSxFQUFFLENBQUMsS0FBSyxDQUFDLEVBQVQsQ0FBUyxFQUFDLENBQzdCLENBQUM7SUFDTixDQUFDOzs7OztJQUVNLCtCQUFNOzs7O0lBQWIsVUFBYyxNQUFNO1FBQXBCLGlCQU1DO1FBTEMsT0FBTyxpQkFDSixHQUFHLFlBQUksSUFBSSxDQUFDLFVBQVUsWUFBTyxNQUFRLENBQUMsQ0FBQyxJQUFJLENBQzFDLEdBQUc7Ozs7UUFBQyxVQUFBLE9BQU8sSUFBSSxPQUFBLEtBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxFQUFqQyxDQUFpQyxFQUFDLEVBQ2pELFVBQVU7Ozs7UUFBQyxVQUFBLEdBQUcsSUFBSSxPQUFBLEVBQUUsQ0FBQyxLQUFLLENBQUMsRUFBVCxDQUFTLEVBQUMsQ0FDN0IsQ0FBQztJQUNOLENBQUM7Ozs7SUFFTSxtQ0FBVTs7O0lBQWpCO1FBQ0UsT0FBTyxJQUFJLENBQUMsY0FBYzthQUNkLFlBQVksRUFBRTthQUNkLElBQUksQ0FDSCxTQUFTOzs7O1FBQUUsVUFBQSxPQUFPLElBQUksT0FBQSxpQkFBaUIsQ0FBQyxPQUFPLENBQUMsRUFBMUIsQ0FBMEIsRUFBRSxDQUNwRCxDQUFDO0lBQ2QsQ0FBQzs7Ozs7SUFFTSwrQkFBTTs7OztJQUFiLFVBQWMsTUFBTTtRQUNsQixPQUFPLGlCQUNKLE1BQU0sWUFBSSxJQUFJLENBQUMsVUFBVSxZQUFPLE1BQVEsQ0FBQyxDQUFDLElBQUksQ0FDN0MsVUFBVTs7OztRQUFDLFVBQUEsR0FBRyxJQUFJLE9BQUEsRUFBRSxDQUFDLEtBQUssQ0FBQyxFQUFULENBQVMsRUFBQyxDQUM3QixDQUFDO0lBQ04sQ0FBQzs7Ozs7O0lBRU0sNEJBQUc7Ozs7O0lBQVYsVUFBVyxVQUFVLEVBQUUsU0FBYztRQUFkLDBCQUFBLEVBQUEsY0FBYztRQUNuQyxPQUFPLElBQUk7YUFDUixJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxVQUFVLEVBQUUsU0FBUyxDQUFDLENBQUMsSUFBSSxDQUM3QyxVQUFVOzs7O1FBQUMsVUFBQSxHQUFHLElBQUksT0FBQSxFQUFFLENBQUMsS0FBSyxDQUFDLEVBQVQsQ0FBUyxFQUFDLENBQzNCLENBQUM7SUFDUixDQUFDOzs7OztJQUVNLDJDQUFrQjs7OztJQUF6QixVQUEwQixNQUFNO1FBQzlCLE9BQU8saUJBQ0osR0FBRyxZQUFJLElBQUksQ0FBQyxlQUFlLG1CQUFjLE1BQVEsQ0FBQzthQUNsRCxJQUFJLENBQ0gsVUFBVTs7OztRQUFDLFVBQUEsR0FBRyxJQUFJLE9BQUEsRUFBRSxDQUFDLEtBQUssQ0FBQyxFQUFULENBQVMsRUFBQyxDQUM3QixDQUFDO0lBQ04sQ0FBQzs7Ozs7O0lBRU0sMENBQWlCOzs7OztJQUF4QixVQUF5QixPQUFPLEVBQUUsU0FBYztRQUFkLDBCQUFBLEVBQUEsY0FBYztRQUM5QyxPQUFPLGlCQUFNLElBQUksWUFBQyxJQUFJLENBQUMsTUFBTSxFQUFFLE9BQU8sRUFBRSxTQUFTLENBQUMsQ0FBQyxJQUFJLENBQ3JELFVBQVU7Ozs7UUFBQyxVQUFBLEdBQUcsSUFBSSxPQUFBLEVBQUUsQ0FBQyxLQUFLLENBQUMsRUFBVCxDQUFTLEVBQUMsQ0FDN0IsQ0FBQztJQUNKLENBQUM7Ozs7SUFHTSxnREFBdUI7OztJQUE5QjtRQUFBLGlCQVFDO1FBUEMsT0FBTyxpQkFDSixHQUFHLFlBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLElBQUksQ0FDMUIsR0FBRzs7OztRQUFDLFVBQUEsS0FBSztZQUNQLE9BQU8sS0FBSSxDQUFDLGlCQUFpQixHQUFHLEtBQUksQ0FBQyxjQUFjLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDN0QsQ0FBQyxFQUFDLEVBQ0YsVUFBVTs7OztRQUFDLFVBQUEsR0FBRyxJQUFJLE9BQUEsRUFBRSxDQUFDLEtBQUssQ0FBQyxFQUFULENBQVMsRUFBQyxDQUM3QixDQUFDO0lBQ04sQ0FBQzs7Ozs7SUFHTSx1Q0FBYzs7OztJQUFyQixVQUFzQixTQUFTO1FBQzdCLElBQUksQ0FBQyxTQUFTLEVBQUU7WUFDZCxPQUFPO1NBQ1I7O1lBQ0ssa0JBQWtCLEdBQUcsRUFBRTtRQUM3QixPQUFPLFNBQVMsQ0FBQyxNQUFNOzs7OztRQUFDLFVBQUMsTUFBTSxFQUFFLElBQUk7O2dCQUMvQixXQUFtQjtZQUN2QixJQUFJLENBQUMsa0JBQWtCLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRTtnQkFDM0Msa0JBQWtCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDbkMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUksSUFBSSxDQUFDLFlBQVksQ0FBQyxXQUFNLElBQUksQ0FBQyxNQUFNLENBQUcsQ0FBQyxDQUFDO2FBQ2pFO2lCQUFNO2dCQUNMLElBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFJLElBQUksQ0FBQyxZQUFZLENBQUMsV0FBTSxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUcsV0FBVyxJQUFJLENBQUMsQ0FBRSxDQUFDLENBQUM7YUFDcEY7WUFDRCxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ2xCLE9BQU8sTUFBTSxDQUFDO1FBQ2hCLENBQUMsR0FBRSxFQUFFLENBQUMsQ0FBQztJQUNULENBQUM7Ozs7SUFFTSw0Q0FBbUI7OztJQUExQjtRQUNFLE9BQU8sSUFBSSxDQUFDLGlCQUFpQixDQUFDO0lBQ2hDLENBQUM7O2dCQWxIRixVQUFVLFNBQUM7b0JBQ1YsVUFBVSxFQUFFLE1BQU07aUJBQ25COzs7O2dCQVpRLFVBQVU7Ozt5QkFEbkI7Q0ErSEMsQUFwSEQsQ0FHb0MsV0FBVyxHQWlIOUM7U0FqSFksY0FBYzs7Ozs7O0lBQ3pCLHNDQUF1RDs7Ozs7SUFDdkQsNENBQXlFOzs7OztJQUN6RSxpQ0FBK0M7Ozs7O0lBQy9DLG9DQUFxRDs7Ozs7SUFDckQsb0NBQXFEOzs7OztJQUNyRCx5Q0FBdUQ7Ozs7O0lBQ3ZELG9EQUEwRTs7Ozs7SUFDMUUscUNBQXNEOzs7OztJQUN0RCxvQ0FBeUQ7Ozs7O0lBQ3pELGdDQUFpRDs7Ozs7SUFDakQsMENBQXFFOzs7OztJQUVyRSx3Q0FBOEU7O0lBQzlFLDJDQUF5QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEh0dHBDbGllbnQgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XG5pbXBvcnQgeyBPYnNlcnZhYmxlLCBvZiwgQmVoYXZpb3JTdWJqZWN0IH0gZnJvbSAncnhqcyc7XG5pbXBvcnQgeyBjYXRjaEVycm9yLCBtYXAsIHRhcCwgc2tpcFdoaWxlIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xuXG5pbXBvcnQgeyBEYXRhU2VydmljZSB9IGZyb20gJy4uL2RhdGEuc2VydmljZSc7XG5pbXBvcnQgKiBhcyBfIGZyb20gJ2xvZGFzaCc7XG5pbXBvcnQgeyBpc051bGxPclVuZGVmaW5lZCB9IGZyb20gJ3V0aWwnO1xuXG5cblxuQEluamVjdGFibGUoe1xuICBwcm92aWRlZEluOiAncm9vdCdcbn0pXG5leHBvcnQgY2xhc3MgQ29tcGFueVNlcnZpY2UgZXh0ZW5kcyBEYXRhU2VydmljZSB7XG4gIHByb3RlY3RlZCBhbGxDb21wYW5pZXMgPSAncmVwb3NpdG9yaWVzL2NvbXBhbnkvZ2V0QWxsJztcbiAgcHJvdGVjdGVkIGdldEFsbFVuZGVyQ29tcGFueSA9ICdyZXBvc2l0b3JpZXMvY29tcGFueS9nZXRBbGxVbmRlckNvbXBhbnknO1xuICBwcm90ZWN0ZWQgYWRkQ29tcCA9ICdyZXBvc2l0b3JpZXMvY29tcGFueS9hZGQnO1xuICBwcm90ZWN0ZWQgZ2V0T25lQ29tcCA9ICdyZXBvc2l0b3JpZXMvY29tcGFueS9nZXRPbmUnO1xuICBwcm90ZWN0ZWQgZGVsZXRlQ29tcCA9ICdyZXBvc2l0b3JpZXMvY29tcGFueS9kZWxldGUnO1xuICBwcm90ZWN0ZWQgY29tcGFueVNuYXBzaG90ID0gJ3V0aWxzL2dldENvbXBhbnlTbmFwc2hvdCc7XG4gIHByb3RlY3RlZCByZXN0b3JlRnJvbUNvbXBhbnlTbmFwc2hvdCA9ICd1dGlscy9yZXN0b3JlRnJvbUNvbXBhbnlTbmFwc2hvdCc7XG4gIHByb3RlY3RlZCBkb3dubG9hZEF0dCA9ICdhdHRhY2htZW50cy9zZWFyY2gvZmluZEJ5SWQnO1xuICBwcm90ZWN0ZWQgZ2V0T25lTWV0YSA9ICdyZXBvc2l0b3JpZXMvY29tcGFueS9nZXRPbmVNZXRhJztcbiAgcHJvdGVjdGVkIHVwZGF0ZSA9ICdyZXBvc2l0b3JpZXMvY29tcGFueS91cGRhdGUnO1xuICBwcm90ZWN0ZWQgbGFzdE1vZGlmaWVkRGF0ZSA9ICdyZXBvc2l0b3JpZXMvY29tcGFueS9sYXN0TW9kaWZpZWREYXRlJztcblxuICBwcml2YXRlIGN1cnJlbnRDb21wYW55OiBCZWhhdmlvclN1YmplY3Q8YW55PiA9IG5ldyBCZWhhdmlvclN1YmplY3Q8YW55PihudWxsKTtcbiAgcHVibGljIGNvbXBhbmllc0ZpbHRlcmVkO1xuXG4gIGNvbnN0cnVjdG9yKGh0dHA6IEh0dHBDbGllbnQpIHtcbiAgICBzdXBlcihodHRwKTtcbiAgfVxuXG5cbiAgcHVibGljIGdldEFsbENvbXBhbmllcygpOiBPYnNlcnZhYmxlPGFueT4ge1xuICAgIHJldHVybiBzdXBlclxuICAgICAgLmdldCh0aGlzLmFsbENvbXBhbmllcykucGlwZShcbiAgICAgICAgY2F0Y2hFcnJvcihlcnIgPT4gb2YoZmFsc2UpKVxuICAgICAgKTtcbiAgfVxuXG4gIHB1YmxpYyBnZXRBbGxGb3JDb21wYW55KGNvbXBJZCk6IE9ic2VydmFibGU8YW55PiB7XG4gICAgcmV0dXJuIHN1cGVyXG4gICAgICAuZ2V0KGAke3RoaXMuZ2V0QWxsVW5kZXJDb21wYW55fT9pZD0ke2NvbXBJZH1gKS5waXBlKFxuICAgICAgICBjYXRjaEVycm9yKGVyciA9PiBvZihmYWxzZSkpXG4gICAgICApO1xuICB9XG5cbiAgcHVibGljIGdldE9uZShjb21wSWQpOiBPYnNlcnZhYmxlPGFueT4ge1xuICAgIHJldHVybiBzdXBlclxuICAgICAgLmdldChgJHt0aGlzLmdldE9uZUNvbXB9P2lkPSR7Y29tcElkfWApLnBpcGUoXG4gICAgICAgIHRhcChjb21wYW55ID0+IHRoaXMuY3VycmVudENvbXBhbnkubmV4dChjb21wYW55KSksXG4gICAgICAgIGNhdGNoRXJyb3IoZXJyID0+IG9mKGZhbHNlKSlcbiAgICAgICk7XG4gIH1cblxuICBwdWJsaWMgZ2V0Q3VycmVudCgpOiBPYnNlcnZhYmxlPGFueT4ge1xuICAgIHJldHVybiB0aGlzLmN1cnJlbnRDb21wYW55XG4gICAgICAgICAgICAgICAuYXNPYnNlcnZhYmxlKClcbiAgICAgICAgICAgICAgIC5waXBlKFxuICAgICAgICAgICAgICAgICBza2lwV2hpbGUoIGNvbXBhbnkgPT4gaXNOdWxsT3JVbmRlZmluZWQoY29tcGFueSkgKVxuICAgICAgICAgICAgICApO1xuICB9XG5cbiAgcHVibGljIGRlbGV0ZShjb21wSWQpOiBPYnNlcnZhYmxlPGFueT4ge1xuICAgIHJldHVybiBzdXBlclxuICAgICAgLmRlbGV0ZShgJHt0aGlzLmRlbGV0ZUNvbXB9P2lkPSR7Y29tcElkfWApLnBpcGUoXG4gICAgICAgIGNhdGNoRXJyb3IoZXJyID0+IG9mKGZhbHNlKSlcbiAgICAgICk7XG4gIH1cblxuICBwdWJsaWMgYWRkKG5ld0NvbXBPYmosIGhlYWRlck9iaiA9IHt9KTogT2JzZXJ2YWJsZTxzdHJpbmc+IHtcbiAgICByZXR1cm4gdGhpc1xuICAgICAgLnBvc3QodGhpcy5hZGRDb21wLCBuZXdDb21wT2JqLCBoZWFkZXJPYmopLnBpcGUoXG4gICAgICAgIGNhdGNoRXJyb3IoZXJyID0+IG9mKGZhbHNlKSlcbiAgICAgICAgKTtcbiAgfVxuXG4gIHB1YmxpYyBnZXRDb21wYW55U25hcHNob3QoY29tcElkKTogT2JzZXJ2YWJsZTxhbnk+IHtcbiAgICByZXR1cm4gc3VwZXJcbiAgICAgIC5nZXQoYCR7dGhpcy5jb21wYW55U25hcHNob3R9P2NvbXBhbnlJZD0ke2NvbXBJZH1gKVxuICAgICAgLnBpcGUoXG4gICAgICAgIGNhdGNoRXJyb3IoZXJyID0+IG9mKGZhbHNlKSlcbiAgICAgICk7XG4gIH1cblxuICBwdWJsaWMgcG9zdENvbXBhbnlVcGRhdGUoYm9keU9iaiwgaGVhZGVyT2JqID0ge30pIHtcbiAgICByZXR1cm4gc3VwZXIucG9zdCh0aGlzLnVwZGF0ZSwgYm9keU9iaiwgaGVhZGVyT2JqKS5waXBlKFxuICAgICAgY2F0Y2hFcnJvcihlcnIgPT4gb2YoZmFsc2UpKVxuICAgICk7XG4gIH1cblxuXG4gIHB1YmxpYyByZXR1cm5GaWxlVHJlZUNvbXBhbmllcygpIHtcbiAgICByZXR1cm4gc3VwZXJcbiAgICAgIC5nZXQodGhpcy5hbGxDb21wYW5pZXMpLnBpcGUoXG4gICAgICAgIHRhcChjb21wcyA9PiB7XG4gICAgICAgICAgcmV0dXJuIHRoaXMuY29tcGFuaWVzRmlsdGVyZWQgPSB0aGlzLmNsZWFuQ29tcGFuaWVzKGNvbXBzKTtcbiAgICAgICAgfSksXG4gICAgICAgIGNhdGNoRXJyb3IoZXJyID0+IG9mKGZhbHNlKSlcbiAgICAgICk7XG4gIH1cblxuXG4gIHB1YmxpYyBjbGVhbkNvbXBhbmllcyhjb21wYW5pZXMpIHtcbiAgICBpZiAoIWNvbXBhbmllcykge1xuICAgICAgcmV0dXJuO1xuICAgIH1cbiAgICBjb25zdCB1bmlxdWVDb21wYW55TmFtZXMgPSBbXTtcbiAgICByZXR1cm4gY29tcGFuaWVzLnJlZHVjZSgobmV3QXJyLCBjb21wKSA9PiB7XG4gICAgICBsZXQgaW5jcmVtZW50ZXI6IG51bWJlcjtcbiAgICAgIGlmICghdW5pcXVlQ29tcGFueU5hbWVzLmluY2x1ZGVzKGNvbXAubmFtZSkpIHtcbiAgICAgICAgdW5pcXVlQ29tcGFueU5hbWVzLnB1c2goY29tcC5uYW1lKTtcbiAgICAgICAgY29tcFsnaGllcmFyY2h5J10gPSBbYCR7Y29tcFsnY29tbW9uTmFtZSddfSA6ICR7Y29tcFsnbmFtZSddfWBdO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgY29tcFsnaGllcmFyY2h5J10gPSBbYCR7Y29tcFsnY29tbW9uTmFtZSddfSA6ICR7Y29tcFsnbmFtZSddfSR7aW5jcmVtZW50ZXIgKz0gMX1gXTtcbiAgICAgIH1cbiAgICAgIG5ld0Fyci5wdXNoKGNvbXApO1xuICAgICAgcmV0dXJuIG5ld0FycjtcbiAgICB9LCBbXSk7XG4gIH1cblxuICBwdWJsaWMgcmV0dXJuRmlsdGVyZWRDb21wcygpIHtcbiAgICByZXR1cm4gdGhpcy5jb21wYW5pZXNGaWx0ZXJlZDtcbiAgfVxuXG59XG4iXX0=