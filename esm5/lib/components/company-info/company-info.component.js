/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Component, Input } from '@angular/core';
import { distanceArray, distanceObject, translateDistance } from '../../../helpers/translate-distance';
import { Router } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { CompanyService } from '../../services/admin-services/company.service';
import { EditCoordinationService } from '../../services/edit-coordination.service';
import { ErrorModalComponent } from '../error-modal/error-modal.component';
import { SuccessModalComponent } from '../success-modal/success-modal.component';
import { ConfirmationModalComponent } from '../confirmation-modal/confirmation-modal.component';
var CompanyInfoComponent = /** @class */ (function () {
    function CompanyInfoComponent(companyService, router, _fb, dialog, editCoordinationService) {
        this.companyService = companyService;
        this.router = router;
        this._fb = _fb;
        this.dialog = dialog;
        this.editCoordinationService = editCoordinationService;
        this.loading = true;
        this.distanceTypes = [];
        this.distanceObject = {};
        this.formValidatorFloat = '[^A-z ]*';
        this.formValidatorInteger = '[^A-z \.]*';
        // triggers
        this.companyIsEditable = false;
    }
    /**
     * @return {?}
     */
    CompanyInfoComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        this.distanceTypes = distanceArray;
        this.distanceObject = distanceObject;
        this.makeCompanyForm();
    };
    /**
     * @return {?}
     */
    CompanyInfoComponent.prototype.ngOnChanges = /**
     * @return {?}
     */
    function () {
        this.makeCompanyForm();
    };
    /**
     * @param {?} unit
     * @return {?}
     */
    CompanyInfoComponent.prototype.translateDistance = /**
     * @param {?} unit
     * @return {?}
     */
    function (unit) {
        return translateDistance(unit, true);
    };
    /**
     * @param {?} sectionName
     * @return {?}
     */
    CompanyInfoComponent.prototype.toggleEdit = /**
     * @param {?} sectionName
     * @return {?}
     */
    function (sectionName) {
        this.enableCompanyForm();
        this.companyIsEditable = true;
        this.sendEditStatus('infoCompany', true);
    };
    /**
     * @param {?} sectionName
     * @return {?}
     */
    CompanyInfoComponent.prototype.cancelEdit = /**
     * @param {?} sectionName
     * @return {?}
     */
    function (sectionName) {
        this.makeCompanyForm();
        this.companyIsEditable = false;
        this.sendEditStatus('infoCompany', false);
    };
    /**
     * @param {?} formStatus
     * @return {?}
     */
    CompanyInfoComponent.prototype.checkTooltip = /**
     * @param {?} formStatus
     * @return {?}
     */
    function (formStatus) {
        if (formStatus) {
            return 'Save';
        }
        else {
            return 'Cannot save - certain fields are invalid.';
        }
    };
    /**
     * @param {?} sectionName
     * @return {?}
     */
    CompanyInfoComponent.prototype.saveEdit = /**
     * @param {?} sectionName
     * @return {?}
     */
    function (sectionName) {
        this.onSubmitCompany();
        this.companyIsEditable = false;
        this.sendEditStatus('infoCompany', false);
    };
    /**
     * @return {?}
     */
    CompanyInfoComponent.prototype.makeCompanyForm = /**
     * @return {?}
     */
    function () {
        this.companyForm = this._fb.group({
            name: [{ value: this.company['name'], disabled: true }, [Validators.required]],
            sigmaLevel: [{ value: this.company['sigmaLevel'], disabled: true },
                [Validators.required, Validators.min(0.0001), Validators.max(3), Validators.pattern(this.formValidatorFloat)]]
        });
        this.loading = false;
    };
    /**
     * @return {?}
     */
    CompanyInfoComponent.prototype.enableCompanyForm = /**
     * @return {?}
     */
    function () {
        this.companyForm.controls.name.enable();
        this.companyForm.controls.sigmaLevel.enable();
    };
    /**
     * @return {?}
     */
    CompanyInfoComponent.prototype.onSubmitCompany = /**
     * @return {?}
     */
    function () {
        var _this = this;
        /** @type {?} */
        var newObj = {};
        newObj['message'] = "You are about to update Company information. Do you wish to continue?";
        newObj['action'] = 'confirm';
        /** @type {?} */
        var dialogRef = this.dialog.open(ConfirmationModalComponent, {
            width: '600px',
            data: newObj,
            disableClose: true,
        });
        dialogRef.componentInstance.submitEmitter.subscribe((/**
         * @param {?} data
         * @return {?}
         */
        function (data) {
            if (!data.response) {
                _this.makeCompanyForm();
                return;
            }
            else {
                _this.sendInformation();
            }
        }));
    };
    /**
     * @return {?}
     */
    CompanyInfoComponent.prototype.sendInformation = /**
     * @return {?}
     */
    function () {
        var _this = this;
        this.loading = true;
        this.companyForm.controls.name.disable();
        this.companyForm.controls.sigmaLevel.disable();
        /** @type {?} */
        var companyClone = tslib_1.__assign({}, this.company);
        companyClone['name'] = this.companyForm.value['name'];
        companyClone['sigmaLevel'] = this.companyForm.value['sigmaLevel'];
        this.companyService.postCompanyUpdate(companyClone).subscribe((/**
         * @param {?} res
         * @return {?}
         */
        function (res) {
            if (res === true) {
                _this.companyService.getOne(_this.company['id']).subscribe((/**
                 * @return {?}
                 */
                function () {
                    /** @type {?} */
                    var newObj = {};
                    newObj['successMessage'] = 'Company information updated.';
                    newObj['icon'] = 'check_circle';
                    newObj['action'] = 'success';
                    _this.dialog.open(SuccessModalComponent, {
                        width: '600px',
                        data: newObj
                    });
                }));
            }
            else {
                /** @type {?} */
                var newObj = {};
                newObj['errMessage'] = 'Unable to update company.';
                newObj['action'] = 'error';
                _this.dialog.open(ErrorModalComponent, {
                    width: '600px',
                    data: newObj
                });
                _this.makeCompanyForm();
            }
        }), (/**
         * @param {?} error
         * @return {?}
         */
        function (error) {
            /** @type {?} */
            var newObj = {};
            newObj['errMessage'] = 'Unable to update company.';
            newObj['action'] = 'error';
            _this.dialog.open(ErrorModalComponent, {
                width: '600px',
                data: newObj
            });
        }));
    };
    /**
     * @return {?}
     */
    CompanyInfoComponent.prototype.checkEditButtonRenderStatus = /**
     * @return {?}
     */
    function () {
        return !this.companyIsEditable && this.companyForm && !this.loading && this.editCoordinationService.areEditFieldsClosed();
    };
    /**
     * @param {?} field
     * @param {?} status
     * @return {?}
     */
    CompanyInfoComponent.prototype.sendEditStatus = /**
     * @param {?} field
     * @param {?} status
     * @return {?}
     */
    function (field, status) {
        this.editCoordinationService.toggleField(field, status);
    };
    CompanyInfoComponent.decorators = [
        { type: Component, args: [{
                    selector: 'app-company-info',
                    template: "<mat-card class=\"form-card\">\n  <mat-card-header class=\"form-card-header\">\n    <mat-card-title class=\"title-type\">Company</mat-card-title>\n    <span class=\"edit-container\">\n      <mat-icon *ngIf=\"checkEditButtonRenderStatus()\" name=\"companyEditIcon\" matTooltip=\"Edit\" (click)=\"toggleEdit('company')\" class=\"edit-button-icon\">edit</mat-icon>\n      <mat-icon *ngIf=\"companyIsEditable\" matTooltip=\"cancel\" name=\"companyCancelIcon\" (click)=\"cancelEdit('company')\" class=\"edit-button-icon\">cancel</mat-icon>\n      <mat-icon *ngIf=\"companyIsEditable\" name=\"companySaveIcon\" [matTooltip]=\"this.checkTooltip(this.companyForm.valid)\"\n        (click)=\"this.companyForm.valid ? saveEdit('company') : ''\"\n        [ngClass]=\"this.companyForm.valid ? 'edit-button-icon' : 'invalid-save-icon'\">check_circle</mat-icon>\n    </span>\n  </mat-card-header>\n  <mat-spinner *ngIf=\"loading\" [diameter]=\"35\"></mat-spinner>\n  <mat-card-content class=\"form-card-content\" *ngIf=\"this.companyForm && !this.loading\">\n    <form class=\"horizontal-form-container\" (ngSubmit)=\"onSubmitCompany()\" name=\"companyForm\" [formGroup]=\"companyForm\">\n      <mat-form-field>\n        <input matInput class=\"general-form-field\" id=\"companyInfoName\" placeholder=\"Name\" formControlName=\"name\">\n        <mat-error *ngIf=\"this.companyForm.get('name').hasError('required')\">Company name cannot be blank.</mat-error>\n      </mat-form-field>\n      <mat-form-field>\n        <input matInput type=\"number\" class=\"general-form-field\" id=\"companyInfoSigmaLevel\" placeholder=\"Sigma Level\" formControlName=\"sigmaLevel\">\n        <mat-error *ngIf=\"this.companyForm.get('sigmaLevel').hasError('required')\">Sigma Level must be a number.</mat-error>\n        <mat-error *ngIf=\"this.companyForm.get('sigmaLevel').hasError('pattern')\">Sigma Level must be a number.</mat-error>\n        <mat-error *ngIf=\"this.companyForm.get('sigmaLevel').hasError('max')\">Sigma Level must be 3 or smaller.</mat-error>\n        <mat-error *ngIf=\"this.companyForm.get('sigmaLevel').hasError('min')\">Sigma Level must be greater than 0.</mat-error>\n      </mat-form-field>\n    </form>\n    <mat-card-actions align=\"end\">\n      <button id=\"alert-modal-ok-btn\"\n              color=\"primary\" mat-flat-button *ngIf=\"parentApp === 'Admin'\">\n        <mat-icon matTooltip=\"Select\" class=\"add-icon\" name=\"addField\">add_circle</mat-icon>\n        Add Field\n      </button>\n    </mat-card-actions>\n  </mat-card-content>\n</mat-card>\n",
                    styles: ["@media print{.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-appearance-legacy.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-1.28122em) scale(.75);transform:translateY(-1.28122em) scale(.75)}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-form-field-autofill-control:-webkit-autofill+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.28121em) scale(.75);transform:translateY(-1.28121em) scale(.75)}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.2812em) scale(.75);transform:translateY(-1.2812em) scale(.75)}}.mat-badge-content{font-weight:600;font-size:12px;font-family:NunitoSans,sans-serif}.mat-badge-small .mat-badge-content{font-size:9px}.mat-badge-large .mat-badge-content{font-size:24px}.mat-h1,.mat-headline,.mat-typography h1{font:400 24px/32px NunitoSans,sans-serif;margin:0 0 16px}.mat-h2,.mat-title,.mat-typography h2{font:500 20px/32px NunitoSans,sans-serif;margin:0 0 16px}.mat-h3,.mat-subheading-2,.mat-typography h3{font:400 16px/28px NunitoSans,sans-serif;margin:0 0 16px}.mat-h4,.mat-subheading-1,.mat-typography h4{font:400 15px/24px NunitoSans,sans-serif;margin:0 0 16px}.mat-h5,.mat-h6,.mat-typography h5,.mat-typography h6{margin:0 0 12px}.mat-body-2,.mat-body-strong{font:500 14px/24px NunitoSans,sans-serif}.mat-body,.mat-body-1,.mat-typography{font:400 14px/20px NunitoSans,sans-serif}.mat-body p,.mat-body-1 p,.mat-typography p{margin:0 0 12px}.mat-caption,.mat-small{font:400 12px/20px NunitoSans,sans-serif}.mat-display-4,.mat-typography .mat-display-4{font:300 112px/112px NunitoSans,sans-serif;letter-spacing:-.05em;margin:0 0 56px}.mat-display-3,.mat-typography .mat-display-3{font:400 56px/56px NunitoSans,sans-serif;letter-spacing:-.02em;margin:0 0 64px}.mat-display-2,.mat-typography .mat-display-2{font:400 45px/48px NunitoSans,sans-serif;letter-spacing:-.005em;margin:0 0 64px}.mat-display-1,.mat-typography .mat-display-1{font:400 34px/40px NunitoSans,sans-serif;margin:0 0 64px}.mat-bottom-sheet-container{font:400 14px/20px NunitoSans,sans-serif}.mat-button,.mat-fab,.mat-flat-button,.mat-icon-button,.mat-mini-fab,.mat-raised-button,.mat-stroked-button{font-family:NunitoSans,sans-serif;font-size:14px;font-weight:500}.mat-button-toggle,.mat-card{font-family:NunitoSans,sans-serif}.mat-card-title{font-size:24px;font-weight:500}.mat-card-header .mat-card-title{font-size:20px}.mat-card-content,.mat-card-subtitle{font-size:14px}.mat-checkbox{font-family:NunitoSans,sans-serif}.mat-checkbox-layout .mat-checkbox-label{line-height:24px}.mat-chip{font-size:14px;font-weight:500}.mat-chip .mat-chip-remove.mat-icon,.mat-chip .mat-chip-trailing-icon.mat-icon{font-size:18px}.mat-table{font-family:NunitoSans,sans-serif}.mat-header-cell{font-size:12px;font-weight:500}.mat-cell,.mat-footer-cell{font-size:14px}.mat-calendar{font-family:NunitoSans,sans-serif}.mat-calendar-body{font-size:13px}.mat-calendar-body-label,.mat-calendar-period-button{font-size:14px;font-weight:500}.mat-calendar-table-header th{font-size:11px;font-weight:400}.mat-dialog-title{font:500 20px/32px NunitoSans,sans-serif}.mat-expansion-panel-header{font-family:NunitoSans,sans-serif;font-size:15px;font-weight:400}.mat-expansion-panel-content{font:400 14px/20px NunitoSans,sans-serif}.mat-form-field{font-size:inherit;font-weight:400;line-height:1.125;font-family:NunitoSans,sans-serif}.mat-form-field-wrapper{padding-bottom:1.34375em}.mat-form-field-prefix .mat-icon,.mat-form-field-suffix .mat-icon{font-size:150%;line-height:1.125}.mat-form-field-prefix .mat-icon-button,.mat-form-field-suffix .mat-icon-button{height:1.5em;width:1.5em}.mat-form-field-prefix .mat-icon-button .mat-icon,.mat-form-field-suffix .mat-icon-button .mat-icon{height:1.125em;line-height:1.125}.mat-form-field-infix{padding:.5em 0;border-top:.84375em solid transparent}.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-1.34373em) scale(.75);transform:translateY(-1.34373em) scale(.75);width:133.3333533333%}.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.34372em) scale(.75);transform:translateY(-1.34372em) scale(.75);width:133.3333633333%}.mat-form-field-label-wrapper{top:-.84375em;padding-top:.84375em}.mat-form-field-label{top:1.34375em}.mat-form-field-underline{bottom:1.34375em}.mat-form-field-subscript-wrapper{font-size:75%;margin-top:.6666666667em;top:calc(100% - 1.7916666667em)}.mat-form-field-appearance-legacy .mat-form-field-wrapper{padding-bottom:1.25em}.mat-form-field-appearance-legacy .mat-form-field-infix{padding:.4375em 0}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-appearance-legacy.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00106px);transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00106px);-ms-transform:translateY(-1.28119em) scale(.75);width:133.3333933333%}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-form-field-autofill-control:-webkit-autofill+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00107px);transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00107px);-ms-transform:translateY(-1.28118em) scale(.75);width:133.3334033333%}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00108px);transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00108px);-ms-transform:translateY(-1.28117em) scale(.75);width:133.3334133333%}.mat-form-field-appearance-legacy .mat-form-field-label{top:1.28125em}.mat-form-field-appearance-legacy .mat-form-field-underline{bottom:1.25em}.mat-form-field-appearance-legacy .mat-form-field-subscript-wrapper{margin-top:.5416666667em;top:calc(100% - 1.6666666667em)}@media print{.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-appearance-legacy.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-1.28116em) scale(.75);transform:translateY(-1.28116em) scale(.75)}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-form-field-autofill-control:-webkit-autofill+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.28115em) scale(.75);transform:translateY(-1.28115em) scale(.75)}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.28114em) scale(.75);transform:translateY(-1.28114em) scale(.75)}}.mat-form-field-appearance-fill .mat-form-field-infix{padding:.25em 0 .75em}.mat-form-field-appearance-fill .mat-form-field-label{top:1.09375em;margin-top:-.5em}.mat-form-field-appearance-fill.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-appearance-fill.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-.59373em) scale(.75);transform:translateY(-.59373em) scale(.75);width:133.3333533333%}.mat-form-field-appearance-fill.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-.59372em) scale(.75);transform:translateY(-.59372em) scale(.75);width:133.3333633333%}.mat-form-field-appearance-outline .mat-form-field-infix{padding:1em 0}.mat-form-field-appearance-outline .mat-form-field-label{top:1.84375em;margin-top:-.25em}.mat-form-field-appearance-outline.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-appearance-outline.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-1.59373em) scale(.75);transform:translateY(-1.59373em) scale(.75);width:133.3333533333%}.mat-form-field-appearance-outline.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.59372em) scale(.75);transform:translateY(-1.59372em) scale(.75);width:133.3333633333%}.mat-grid-tile-footer,.mat-grid-tile-header{font-size:14px}.mat-grid-tile-footer .mat-line,.mat-grid-tile-header .mat-line{white-space:nowrap;overflow:hidden;text-overflow:ellipsis;display:block;box-sizing:border-box}.mat-grid-tile-footer .mat-line:nth-child(n+2),.mat-grid-tile-header .mat-line:nth-child(n+2){font-size:12px}input.mat-input-element{margin-top:-.0625em}.mat-menu-item{font-family:NunitoSans,sans-serif;font-size:14px;font-weight:400}.mat-paginator,.mat-paginator-page-size .mat-select-trigger{font-family:NunitoSans,sans-serif;font-size:12px}.mat-radio-button,.mat-select{font-family:NunitoSans,sans-serif}.mat-select-trigger{height:1.125em}.mat-slide-toggle-content{font-family:NunitoSans,sans-serif}.mat-slider-thumb-label-text{font-family:NunitoSans,sans-serif;font-size:12px;font-weight:500}.mat-stepper-horizontal,.mat-stepper-vertical{font-family:NunitoSans,sans-serif}.mat-step-label{font-size:14px;font-weight:400}.mat-step-sub-label-error{font-weight:400}.mat-step-label-error{font-size:14px}.mat-step-label-selected{font-size:14px;font-weight:500}.mat-tab-group{font-family:NunitoSans,sans-serif}.mat-tab-label,.mat-tab-link{font-family:NunitoSans,sans-serif;font-size:14px;font-weight:500}.mat-toolbar,.mat-toolbar h1,.mat-toolbar h2,.mat-toolbar h3,.mat-toolbar h4,.mat-toolbar h5,.mat-toolbar h6{font:500 20px/32px NunitoSans,sans-serif;margin:0}.mat-tooltip{font-family:NunitoSans,sans-serif;font-size:10px;padding-top:6px;padding-bottom:6px}.mat-tooltip-handset{font-size:14px;padding-top:8px;padding-bottom:8px}.mat-list-item,.mat-list-option{font-family:NunitoSans,sans-serif}.mat-list-base .mat-list-item{font-size:16px}.mat-list-base .mat-list-item .mat-line{white-space:nowrap;overflow:hidden;text-overflow:ellipsis;display:block;box-sizing:border-box}.mat-list-base .mat-list-item .mat-line:nth-child(n+2){font-size:14px}.mat-list-base .mat-list-option{font-size:16px}.mat-list-base .mat-list-option .mat-line{white-space:nowrap;overflow:hidden;text-overflow:ellipsis;display:block;box-sizing:border-box}.mat-list-base .mat-list-option .mat-line:nth-child(n+2){font-size:14px}.mat-list-base .mat-subheader{font-family:NunitoSans,sans-serif;font-size:14px;font-weight:500}.mat-list-base[dense] .mat-list-item{font-size:12px}.mat-list-base[dense] .mat-list-item .mat-line{white-space:nowrap;overflow:hidden;text-overflow:ellipsis;display:block;box-sizing:border-box}.mat-list-base[dense] .mat-list-item .mat-line:nth-child(n+2),.mat-list-base[dense] .mat-list-option{font-size:12px}.mat-list-base[dense] .mat-list-option .mat-line{white-space:nowrap;overflow:hidden;text-overflow:ellipsis;display:block;box-sizing:border-box}.mat-list-base[dense] .mat-list-option .mat-line:nth-child(n+2){font-size:12px}.mat-list-base[dense] .mat-subheader{font-family:NunitoSans,sans-serif;font-size:12px;font-weight:500}.mat-option{font-family:NunitoSans,sans-serif;font-size:16px}.mat-optgroup-label{font:500 14px/24px NunitoSans,sans-serif}.mat-simple-snackbar{font-family:NunitoSans,sans-serif;font-size:14px}.mat-simple-snackbar-action{line-height:1;font-family:inherit;font-size:inherit;font-weight:500}.mat-tree{font-family:NunitoSans,sans-serif}.mat-nested-tree-node,.mat-tree-node{font-weight:400;font-size:14px}.mat-ripple{overflow:hidden;position:relative}.mat-ripple.mat-ripple-unbounded{overflow:visible}.mat-ripple-element{position:absolute;border-radius:50%;pointer-events:none;-webkit-transition:opacity,-webkit-transform cubic-bezier(0,0,.2,1);transition:opacity,transform cubic-bezier(0,0,.2,1),-webkit-transform cubic-bezier(0,0,.2,1);-webkit-transform:scale(0);transform:scale(0)}@media (-ms-high-contrast:active){.mat-ripple-element{display:none}}.cdk-visually-hidden{border:0;clip:rect(0 0 0 0);height:1px;margin:-1px;overflow:hidden;padding:0;position:absolute;width:1px;outline:0;-webkit-appearance:none;-moz-appearance:none}.cdk-global-overlay-wrapper,.cdk-overlay-container{pointer-events:none;top:0;left:0;height:100%;width:100%}.cdk-overlay-container{position:fixed;z-index:1000}.cdk-overlay-container:empty{display:none}.cdk-global-overlay-wrapper{display:-webkit-box;display:flex;position:absolute;z-index:1000}.cdk-overlay-pane{position:absolute;pointer-events:auto;box-sizing:border-box;z-index:1000;display:-webkit-box;display:flex;max-width:100%;max-height:100%}.cdk-overlay-backdrop{position:absolute;top:0;bottom:0;left:0;right:0;z-index:1000;pointer-events:auto;-webkit-tap-highlight-color:transparent;-webkit-transition:opacity .4s cubic-bezier(.25,.8,.25,1);transition:opacity .4s cubic-bezier(.25,.8,.25,1);opacity:0}.cdk-overlay-backdrop.cdk-overlay-backdrop-showing{opacity:1}@media screen and (-ms-high-contrast:active){.cdk-overlay-backdrop.cdk-overlay-backdrop-showing{opacity:.6}}.cdk-overlay-dark-backdrop{background:rgba(0,0,0,.32)}.cdk-overlay-transparent-backdrop,.cdk-overlay-transparent-backdrop.cdk-overlay-backdrop-showing{opacity:0}.cdk-overlay-connected-position-bounding-box{position:absolute;z-index:1000;display:-webkit-box;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;flex-direction:column;min-width:1px;min-height:1px}.cdk-global-scrollblock{position:fixed;width:100%;overflow-y:scroll}@-webkit-keyframes cdk-text-field-autofill-start{/*!*/}@keyframes cdk-text-field-autofill-start{/*!*/}@-webkit-keyframes cdk-text-field-autofill-end{/*!*/}@keyframes cdk-text-field-autofill-end{/*!*/}.cdk-text-field-autofill-monitored:-webkit-autofill{-webkit-animation-name:cdk-text-field-autofill-start;animation-name:cdk-text-field-autofill-start}.cdk-text-field-autofill-monitored:not(:-webkit-autofill){-webkit-animation-name:cdk-text-field-autofill-end;animation-name:cdk-text-field-autofill-end}textarea.cdk-textarea-autosize{resize:none}textarea.cdk-textarea-autosize-measuring{height:auto!important;overflow:hidden!important;padding:2px 0!important;box-sizing:content-box!important}mat-spinner{margin:0 auto}mat-card-actions{padding:0}.edit-container{width:60px;display:-webkit-box;display:flex;-webkit-box-orient:horizontal;-webkit-box-direction:reverse;flex-direction:row-reverse}.invalid-save-icon{color:#f99}.show-map-icon{cursor:pointer}.add-icon{color:#fff}.form-card-header{display:-webkit-box;display:flex;-webkit-box-pack:justify;justify-content:space-between}.horizontal-flex-spread{flex-wrap:wrap}.vertical-form-container{display:-webkit-box;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;flex-direction:column}.form-field-spacer{margin:0 10px 0 0}.small-form-field{width:148px}.general-form-field{margin:0 10px 0 0}.wide-form-field{min-width:250px}.wider-form-field{min-width:350px}.multiple-input-form-field{display:-webkit-box;display:flex;min-width:350px}.full-width-form-field{width:100%}.pad-table{margin:0 auto}"]
                }] }
    ];
    /** @nocollapse */
    CompanyInfoComponent.ctorParameters = function () { return [
        { type: CompanyService },
        { type: Router },
        { type: FormBuilder },
        { type: MatDialog },
        { type: EditCoordinationService }
    ]; };
    CompanyInfoComponent.propDecorators = {
        company: [{ type: Input }],
        parentApp: [{ type: Input }]
    };
    return CompanyInfoComponent;
}());
export { CompanyInfoComponent };
if (false) {
    /** @type {?} */
    CompanyInfoComponent.prototype.company;
    /** @type {?} */
    CompanyInfoComponent.prototype.parentApp;
    /** @type {?} */
    CompanyInfoComponent.prototype.loading;
    /** @type {?} */
    CompanyInfoComponent.prototype.distanceTypes;
    /** @type {?} */
    CompanyInfoComponent.prototype.distanceObject;
    /** @type {?} */
    CompanyInfoComponent.prototype.formValidatorFloat;
    /** @type {?} */
    CompanyInfoComponent.prototype.formValidatorInteger;
    /** @type {?} */
    CompanyInfoComponent.prototype.companyForm;
    /** @type {?} */
    CompanyInfoComponent.prototype.companyIsEditable;
    /**
     * @type {?}
     * @private
     */
    CompanyInfoComponent.prototype.companyService;
    /**
     * @type {?}
     * @private
     */
    CompanyInfoComponent.prototype.router;
    /**
     * @type {?}
     * @private
     */
    CompanyInfoComponent.prototype._fb;
    /**
     * @type {?}
     * @private
     */
    CompanyInfoComponent.prototype.dialog;
    /** @type {?} */
    CompanyInfoComponent.prototype.editCoordinationService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tcGFueS1pbmZvLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BocHRlY2gvc2FwaGlyYS1zaGFyZWQtbGliLyIsInNvdXJjZXMiOlsibGliL2NvbXBvbmVudHMvY29tcGFueS1pbmZvL2NvbXBhbnktaW5mby5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLEtBQUssRUFBYSxNQUFNLGVBQWUsQ0FBQztBQUNwRSxPQUFPLEVBQUUsYUFBYSxFQUFFLGNBQWMsRUFBRSxpQkFBaUIsRUFBRSxNQUFNLHFDQUFxQyxDQUFDO0FBQ3ZHLE9BQU8sRUFBRSxNQUFNLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUN6QyxPQUFPLEVBQUUsV0FBVyxFQUFhLFVBQVUsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQ3BFLE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQztBQUM5QyxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sK0NBQStDLENBQUM7QUFDL0UsT0FBTyxFQUFFLHVCQUF1QixFQUFFLE1BQU0sMENBQTBDLENBQUM7QUFDbkYsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sc0NBQXNDLENBQUM7QUFDM0UsT0FBTyxFQUFFLHFCQUFxQixFQUFFLE1BQU0sMENBQTBDLENBQUM7QUFDakYsT0FBTyxFQUFFLDBCQUEwQixFQUFFLE1BQU0sb0RBQW9ELENBQUM7QUFFaEc7SUFzQkUsOEJBQ1UsY0FBOEIsRUFDOUIsTUFBYyxFQUNkLEdBQWdCLEVBQ2hCLE1BQWlCLEVBQ2xCLHVCQUFnRDtRQUovQyxtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDOUIsV0FBTSxHQUFOLE1BQU0sQ0FBUTtRQUNkLFFBQUcsR0FBSCxHQUFHLENBQWE7UUFDaEIsV0FBTSxHQUFOLE1BQU0sQ0FBVztRQUNsQiw0QkFBdUIsR0FBdkIsdUJBQXVCLENBQXlCO1FBbEJ6RCxZQUFPLEdBQUcsSUFBSSxDQUFDO1FBRWYsa0JBQWEsR0FBRyxFQUFFLENBQUM7UUFDbkIsbUJBQWMsR0FBRyxFQUFFLENBQUM7UUFFcEIsdUJBQWtCLEdBQUcsVUFBVSxDQUFDO1FBQ2hDLHlCQUFvQixHQUFHLFlBQVksQ0FBQzs7UUFLcEMsc0JBQWlCLEdBQUcsS0FBSyxDQUFDO0lBUzFCLENBQUM7Ozs7SUFFRCx1Q0FBUTs7O0lBQVI7UUFDRSxJQUFJLENBQUMsYUFBYSxHQUFHLGFBQWEsQ0FBQztRQUNuQyxJQUFJLENBQUMsY0FBYyxHQUFHLGNBQWMsQ0FBQztRQUNyQyxJQUFJLENBQUMsZUFBZSxFQUFFLENBQUM7SUFDekIsQ0FBQzs7OztJQUVELDBDQUFXOzs7SUFBWDtRQUNFLElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQztJQUN6QixDQUFDOzs7OztJQUVELGdEQUFpQjs7OztJQUFqQixVQUFrQixJQUFJO1FBQ3BCLE9BQU8saUJBQWlCLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQ3ZDLENBQUM7Ozs7O0lBRU0seUNBQVU7Ozs7SUFBakIsVUFBa0IsV0FBVztRQUN2QixJQUFJLENBQUMsaUJBQWlCLEVBQUUsQ0FBQztRQUN6QixJQUFJLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDO1FBQzlCLElBQUksQ0FBQyxjQUFjLENBQUMsYUFBYSxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQy9DLENBQUM7Ozs7O0lBRU0seUNBQVU7Ozs7SUFBakIsVUFBa0IsV0FBVztRQUN2QixJQUFJLENBQUMsZUFBZSxFQUFFLENBQUM7UUFDdkIsSUFBSSxDQUFDLGlCQUFpQixHQUFHLEtBQUssQ0FBQztRQUMvQixJQUFJLENBQUMsY0FBYyxDQUFDLGFBQWEsRUFBRSxLQUFLLENBQUMsQ0FBQztJQUNoRCxDQUFDOzs7OztJQUVELDJDQUFZOzs7O0lBQVosVUFBYSxVQUFVO1FBQ3JCLElBQUksVUFBVSxFQUFFO1lBQ2QsT0FBTyxNQUFNLENBQUM7U0FDZjthQUFNO1lBQ0wsT0FBTywyQ0FBMkMsQ0FBQztTQUNwRDtJQUNILENBQUM7Ozs7O0lBRU0sdUNBQVE7Ozs7SUFBZixVQUFnQixXQUFXO1FBQ3pCLElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQztRQUN2QixJQUFJLENBQUMsaUJBQWlCLEdBQUcsS0FBSyxDQUFDO1FBQy9CLElBQUksQ0FBQyxjQUFjLENBQUMsYUFBYSxFQUFFLEtBQUssQ0FBQyxDQUFDO0lBQzVDLENBQUM7Ozs7SUFFRCw4Q0FBZTs7O0lBQWY7UUFDRSxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDO1lBQ2hDLElBQUksRUFBRSxDQUFDLEVBQUUsS0FBSyxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLEVBQUUsUUFBUSxFQUFFLElBQUksRUFBRSxFQUFFLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQzlFLFVBQVUsRUFBRSxDQUFDLEVBQUUsS0FBSyxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsWUFBWSxDQUFDLEVBQUUsUUFBUSxFQUFFLElBQUksRUFBRTtnQkFDbEUsQ0FBQyxVQUFVLENBQUMsUUFBUSxFQUFFLFVBQVUsQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLEVBQUUsVUFBVSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsRUFBRSxVQUFVLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDLENBQUM7U0FDL0csQ0FBQyxDQUFDO1FBQ0gsSUFBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7SUFDdkIsQ0FBQzs7OztJQUVELGdEQUFpQjs7O0lBQWpCO1FBQ0UsSUFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQ3hDLElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxNQUFNLEVBQUUsQ0FBQztJQUNoRCxDQUFDOzs7O0lBRUQsOENBQWU7OztJQUFmO1FBQUEsaUJBa0JDOztZQWpCTyxNQUFNLEdBQUcsRUFBRTtRQUNqQixNQUFNLENBQUMsU0FBUyxDQUFDLEdBQUcsdUVBQXVFLENBQUM7UUFDNUYsTUFBTSxDQUFDLFFBQVEsQ0FBQyxHQUFHLFNBQVMsQ0FBQzs7WUFDdkIsU0FBUyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLDBCQUEwQixFQUFFO1lBQzdELEtBQUssRUFBRSxPQUFPO1lBQ2QsSUFBSSxFQUFFLE1BQU07WUFDWixZQUFZLEVBQUUsSUFBSTtTQUNuQixDQUFDO1FBRUYsU0FBUyxDQUFDLGlCQUFpQixDQUFDLGFBQWEsQ0FBQyxTQUFTOzs7O1FBQUMsVUFBQyxJQUFJO1lBQ3ZELElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFO2dCQUNsQixLQUFJLENBQUMsZUFBZSxFQUFFLENBQUM7Z0JBQ3ZCLE9BQU87YUFDUjtpQkFBTTtnQkFDTCxLQUFJLENBQUMsZUFBZSxFQUFFLENBQUM7YUFDeEI7UUFDSCxDQUFDLEVBQUMsQ0FBQztJQUNMLENBQUM7Ozs7SUFFRCw4Q0FBZTs7O0lBQWY7UUFBQSxpQkFzQ0M7UUFyQ0MsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7UUFDcEIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDO1FBQ3pDLElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxPQUFPLEVBQUUsQ0FBQzs7WUFDekMsWUFBWSx3QkFBUSxJQUFJLENBQUMsT0FBTyxDQUFFO1FBQ3hDLFlBQVksQ0FBQyxNQUFNLENBQUMsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUN0RCxZQUFZLENBQUMsWUFBWSxDQUFDLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLENBQUM7UUFDbEUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxpQkFBaUIsQ0FBQyxZQUFZLENBQUMsQ0FBQyxTQUFTOzs7O1FBQUMsVUFBQSxHQUFHO1lBQy9ELElBQUksR0FBRyxLQUFLLElBQUksRUFBRTtnQkFDaEIsS0FBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsS0FBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLFNBQVM7OztnQkFBQzs7d0JBQ2pELE1BQU0sR0FBRyxFQUFFO29CQUNqQixNQUFNLENBQUMsZ0JBQWdCLENBQUMsR0FBRyw4QkFBOEIsQ0FBQztvQkFDMUQsTUFBTSxDQUFDLE1BQU0sQ0FBQyxHQUFHLGNBQWMsQ0FBQztvQkFDaEMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxHQUFHLFNBQVMsQ0FBQztvQkFDN0IsS0FBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMscUJBQXFCLEVBQUU7d0JBQ3RDLEtBQUssRUFBRSxPQUFPO3dCQUNkLElBQUksRUFBRSxNQUFNO3FCQUNiLENBQUMsQ0FBQztnQkFDTCxDQUFDLEVBQUMsQ0FBQzthQUNKO2lCQUFNOztvQkFDQyxNQUFNLEdBQUcsRUFBRTtnQkFDakIsTUFBTSxDQUFDLFlBQVksQ0FBQyxHQUFHLDJCQUEyQixDQUFDO2dCQUNuRCxNQUFNLENBQUMsUUFBUSxDQUFDLEdBQUcsT0FBTyxDQUFDO2dCQUMzQixLQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxtQkFBbUIsRUFBRTtvQkFDcEMsS0FBSyxFQUFFLE9BQU87b0JBQ2QsSUFBSSxFQUFFLE1BQU07aUJBQ2IsQ0FBQyxDQUFDO2dCQUNILEtBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQzthQUN4QjtRQUNILENBQUM7Ozs7UUFBRSxVQUFBLEtBQUs7O2dCQUNBLE1BQU0sR0FBRyxFQUFFO1lBQ2pCLE1BQU0sQ0FBQyxZQUFZLENBQUMsR0FBRywyQkFBMkIsQ0FBQztZQUNuRCxNQUFNLENBQUMsUUFBUSxDQUFDLEdBQUcsT0FBTyxDQUFDO1lBQzNCLEtBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLG1CQUFtQixFQUFFO2dCQUNwQyxLQUFLLEVBQUUsT0FBTztnQkFDZCxJQUFJLEVBQUUsTUFBTTthQUNiLENBQUMsQ0FBQztRQUNMLENBQUMsRUFBQyxDQUFDO0lBQ0wsQ0FBQzs7OztJQUVELDBEQUEyQjs7O0lBQTNCO1FBQ0UsT0FBTyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsSUFBSSxJQUFJLENBQUMsV0FBVyxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sSUFBSSxJQUFJLENBQUMsdUJBQXVCLENBQUMsbUJBQW1CLEVBQUUsQ0FBQztJQUM1SCxDQUFDOzs7Ozs7SUFFRCw2Q0FBYzs7Ozs7SUFBZCxVQUFlLEtBQWEsRUFBRSxNQUFlO1FBQzNDLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxXQUFXLENBQUMsS0FBSyxFQUFFLE1BQU0sQ0FBQyxDQUFDO0lBQzFELENBQUM7O2dCQXZKRixTQUFTLFNBQUM7b0JBQ1QsUUFBUSxFQUFFLGtCQUFrQjtvQkFDNUIsNGdGQUE0Qzs7aUJBRTdDOzs7O2dCQVZRLGNBQWM7Z0JBSGQsTUFBTTtnQkFDTixXQUFXO2dCQUNYLFNBQVM7Z0JBRVQsdUJBQXVCOzs7MEJBVzdCLEtBQUs7NEJBQ0wsS0FBSzs7SUFpSlIsMkJBQUM7Q0FBQSxBQXhKRCxJQXdKQztTQW5KWSxvQkFBb0I7OztJQUMvQix1Q0FBcUI7O0lBQ3JCLHlDQUEyQjs7SUFFM0IsdUNBQWU7O0lBRWYsNkNBQW1COztJQUNuQiw4Q0FBb0I7O0lBRXBCLGtEQUFnQzs7SUFDaEMsb0RBQW9DOztJQUVwQywyQ0FBdUI7O0lBR3ZCLGlEQUEwQjs7Ozs7SUFHeEIsOENBQXNDOzs7OztJQUN0QyxzQ0FBc0I7Ozs7O0lBQ3RCLG1DQUF3Qjs7Ozs7SUFDeEIsc0NBQXlCOztJQUN6Qix1REFBdUQiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgSW5wdXQsIE9uQ2hhbmdlcyB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgZGlzdGFuY2VBcnJheSwgZGlzdGFuY2VPYmplY3QsIHRyYW5zbGF0ZURpc3RhbmNlIH0gZnJvbSAnLi4vLi4vLi4vaGVscGVycy90cmFuc2xhdGUtZGlzdGFuY2UnO1xuaW1wb3J0IHsgUm91dGVyIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcbmltcG9ydCB7IEZvcm1CdWlsZGVyLCBGb3JtR3JvdXAsIFZhbGlkYXRvcnMgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XG5pbXBvcnQgeyBNYXREaWFsb2cgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbCc7XG5pbXBvcnQgeyBDb21wYW55U2VydmljZSB9IGZyb20gJy4uLy4uL3NlcnZpY2VzL2FkbWluLXNlcnZpY2VzL2NvbXBhbnkuc2VydmljZSc7XG5pbXBvcnQgeyBFZGl0Q29vcmRpbmF0aW9uU2VydmljZSB9IGZyb20gJy4uLy4uL3NlcnZpY2VzL2VkaXQtY29vcmRpbmF0aW9uLnNlcnZpY2UnO1xuaW1wb3J0IHsgRXJyb3JNb2RhbENvbXBvbmVudCB9IGZyb20gJy4uL2Vycm9yLW1vZGFsL2Vycm9yLW1vZGFsLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBTdWNjZXNzTW9kYWxDb21wb25lbnQgfSBmcm9tICcuLi9zdWNjZXNzLW1vZGFsL3N1Y2Nlc3MtbW9kYWwuY29tcG9uZW50JztcbmltcG9ydCB7IENvbmZpcm1hdGlvbk1vZGFsQ29tcG9uZW50IH0gZnJvbSAnLi4vY29uZmlybWF0aW9uLW1vZGFsL2NvbmZpcm1hdGlvbi1tb2RhbC5jb21wb25lbnQnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdhcHAtY29tcGFueS1pbmZvJyxcbiAgdGVtcGxhdGVVcmw6ICcuL2NvbXBhbnktaW5mby5jb21wb25lbnQuaHRtbCcsXG4gIHN0eWxlVXJsczogWycuL2NvbXBhbnktaW5mby5jb21wb25lbnQuc2NzcyddXG59KVxuZXhwb3J0IGNsYXNzIENvbXBhbnlJbmZvQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBPbkNoYW5nZXMge1xuICBASW5wdXQoKSBjb21wYW55OiB7fTtcbiAgQElucHV0KCkgcGFyZW50QXBwOiBzdHJpbmc7XG5cbiAgbG9hZGluZyA9IHRydWU7XG5cbiAgZGlzdGFuY2VUeXBlcyA9IFtdO1xuICBkaXN0YW5jZU9iamVjdCA9IHt9O1xuXG4gIGZvcm1WYWxpZGF0b3JGbG9hdCA9ICdbXkEteiBdKic7XG4gIGZvcm1WYWxpZGF0b3JJbnRlZ2VyID0gJ1teQS16IFxcLl0qJztcblxuICBjb21wYW55Rm9ybTogRm9ybUdyb3VwO1xuXG4gIC8vIHRyaWdnZXJzXG4gIGNvbXBhbnlJc0VkaXRhYmxlID0gZmFsc2U7XG5cbiAgY29uc3RydWN0b3IoXG4gICAgcHJpdmF0ZSBjb21wYW55U2VydmljZTogQ29tcGFueVNlcnZpY2UsXG4gICAgcHJpdmF0ZSByb3V0ZXI6IFJvdXRlcixcbiAgICBwcml2YXRlIF9mYjogRm9ybUJ1aWxkZXIsXG4gICAgcHJpdmF0ZSBkaWFsb2c6IE1hdERpYWxvZyxcbiAgICBwdWJsaWMgZWRpdENvb3JkaW5hdGlvblNlcnZpY2U6IEVkaXRDb29yZGluYXRpb25TZXJ2aWNlXG4gICkge1xuICB9XG5cbiAgbmdPbkluaXQoKSB7XG4gICAgdGhpcy5kaXN0YW5jZVR5cGVzID0gZGlzdGFuY2VBcnJheTtcbiAgICB0aGlzLmRpc3RhbmNlT2JqZWN0ID0gZGlzdGFuY2VPYmplY3Q7XG4gICAgdGhpcy5tYWtlQ29tcGFueUZvcm0oKTtcbiAgfVxuXG4gIG5nT25DaGFuZ2VzKCkge1xuICAgIHRoaXMubWFrZUNvbXBhbnlGb3JtKCk7XG4gIH1cblxuICB0cmFuc2xhdGVEaXN0YW5jZSh1bml0KSB7XG4gICAgcmV0dXJuIHRyYW5zbGF0ZURpc3RhbmNlKHVuaXQsIHRydWUpO1xuICB9XG5cbiAgcHVibGljIHRvZ2dsZUVkaXQoc2VjdGlvbk5hbWUpIHtcbiAgICAgICAgdGhpcy5lbmFibGVDb21wYW55Rm9ybSgpO1xuICAgICAgICB0aGlzLmNvbXBhbnlJc0VkaXRhYmxlID0gdHJ1ZTtcbiAgICAgICAgdGhpcy5zZW5kRWRpdFN0YXR1cygnaW5mb0NvbXBhbnknLCB0cnVlKTtcbiAgfVxuXG4gIHB1YmxpYyBjYW5jZWxFZGl0KHNlY3Rpb25OYW1lKSB7XG4gICAgICAgIHRoaXMubWFrZUNvbXBhbnlGb3JtKCk7XG4gICAgICAgIHRoaXMuY29tcGFueUlzRWRpdGFibGUgPSBmYWxzZTtcbiAgICAgICAgdGhpcy5zZW5kRWRpdFN0YXR1cygnaW5mb0NvbXBhbnknLCBmYWxzZSk7XG4gIH1cblxuICBjaGVja1Rvb2x0aXAoZm9ybVN0YXR1cykge1xuICAgIGlmIChmb3JtU3RhdHVzKSB7XG4gICAgICByZXR1cm4gJ1NhdmUnO1xuICAgIH0gZWxzZSB7XG4gICAgICByZXR1cm4gJ0Nhbm5vdCBzYXZlIC0gY2VydGFpbiBmaWVsZHMgYXJlIGludmFsaWQuJztcbiAgICB9XG4gIH1cblxuICBwdWJsaWMgc2F2ZUVkaXQoc2VjdGlvbk5hbWUpIHtcbiAgICB0aGlzLm9uU3VibWl0Q29tcGFueSgpO1xuICAgIHRoaXMuY29tcGFueUlzRWRpdGFibGUgPSBmYWxzZTtcbiAgICB0aGlzLnNlbmRFZGl0U3RhdHVzKCdpbmZvQ29tcGFueScsIGZhbHNlKTtcbiAgfVxuXG4gIG1ha2VDb21wYW55Rm9ybSgpIHtcbiAgICB0aGlzLmNvbXBhbnlGb3JtID0gdGhpcy5fZmIuZ3JvdXAoe1xuICAgICAgbmFtZTogW3sgdmFsdWU6IHRoaXMuY29tcGFueVsnbmFtZSddLCBkaXNhYmxlZDogdHJ1ZSB9LCBbVmFsaWRhdG9ycy5yZXF1aXJlZF1dLFxuICAgICAgc2lnbWFMZXZlbDogW3sgdmFsdWU6IHRoaXMuY29tcGFueVsnc2lnbWFMZXZlbCddLCBkaXNhYmxlZDogdHJ1ZSB9LFxuICAgICAgW1ZhbGlkYXRvcnMucmVxdWlyZWQsIFZhbGlkYXRvcnMubWluKDAuMDAwMSksIFZhbGlkYXRvcnMubWF4KDMpLCBWYWxpZGF0b3JzLnBhdHRlcm4odGhpcy5mb3JtVmFsaWRhdG9yRmxvYXQpXV1cbiAgICB9KTtcbiAgICB0aGlzLmxvYWRpbmcgPSBmYWxzZTtcbiAgfVxuXG4gIGVuYWJsZUNvbXBhbnlGb3JtKCkge1xuICAgIHRoaXMuY29tcGFueUZvcm0uY29udHJvbHMubmFtZS5lbmFibGUoKTtcbiAgICB0aGlzLmNvbXBhbnlGb3JtLmNvbnRyb2xzLnNpZ21hTGV2ZWwuZW5hYmxlKCk7XG4gIH1cblxuICBvblN1Ym1pdENvbXBhbnkoKSB7XG4gICAgY29uc3QgbmV3T2JqID0ge307XG4gICAgbmV3T2JqWydtZXNzYWdlJ10gPSBgWW91IGFyZSBhYm91dCB0byB1cGRhdGUgQ29tcGFueSBpbmZvcm1hdGlvbi4gRG8geW91IHdpc2ggdG8gY29udGludWU/YDtcbiAgICBuZXdPYmpbJ2FjdGlvbiddID0gJ2NvbmZpcm0nO1xuICAgIGNvbnN0IGRpYWxvZ1JlZiA9IHRoaXMuZGlhbG9nLm9wZW4oQ29uZmlybWF0aW9uTW9kYWxDb21wb25lbnQsIHtcbiAgICAgIHdpZHRoOiAnNjAwcHgnLFxuICAgICAgZGF0YTogbmV3T2JqLFxuICAgICAgZGlzYWJsZUNsb3NlOiB0cnVlLFxuICAgIH0pO1xuXG4gICAgZGlhbG9nUmVmLmNvbXBvbmVudEluc3RhbmNlLnN1Ym1pdEVtaXR0ZXIuc3Vic2NyaWJlKChkYXRhKSA9PiB7XG4gICAgICBpZiAoIWRhdGEucmVzcG9uc2UpIHtcbiAgICAgICAgdGhpcy5tYWtlQ29tcGFueUZvcm0oKTtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgdGhpcy5zZW5kSW5mb3JtYXRpb24oKTtcbiAgICAgIH1cbiAgICB9KTtcbiAgfVxuXG4gIHNlbmRJbmZvcm1hdGlvbigpIHtcbiAgICB0aGlzLmxvYWRpbmcgPSB0cnVlO1xuICAgIHRoaXMuY29tcGFueUZvcm0uY29udHJvbHMubmFtZS5kaXNhYmxlKCk7XG4gICAgdGhpcy5jb21wYW55Rm9ybS5jb250cm9scy5zaWdtYUxldmVsLmRpc2FibGUoKTtcbiAgICBjb25zdCBjb21wYW55Q2xvbmUgPSB7IC4uLnRoaXMuY29tcGFueSB9O1xuICAgIGNvbXBhbnlDbG9uZVsnbmFtZSddID0gdGhpcy5jb21wYW55Rm9ybS52YWx1ZVsnbmFtZSddO1xuICAgIGNvbXBhbnlDbG9uZVsnc2lnbWFMZXZlbCddID0gdGhpcy5jb21wYW55Rm9ybS52YWx1ZVsnc2lnbWFMZXZlbCddO1xuICAgIHRoaXMuY29tcGFueVNlcnZpY2UucG9zdENvbXBhbnlVcGRhdGUoY29tcGFueUNsb25lKS5zdWJzY3JpYmUocmVzID0+IHtcbiAgICAgIGlmIChyZXMgPT09IHRydWUpIHtcbiAgICAgICAgdGhpcy5jb21wYW55U2VydmljZS5nZXRPbmUodGhpcy5jb21wYW55WydpZCddKS5zdWJzY3JpYmUoKCkgPT4ge1xuICAgICAgICAgIGNvbnN0IG5ld09iaiA9IHt9O1xuICAgICAgICAgIG5ld09ialsnc3VjY2Vzc01lc3NhZ2UnXSA9ICdDb21wYW55IGluZm9ybWF0aW9uIHVwZGF0ZWQuJztcbiAgICAgICAgICBuZXdPYmpbJ2ljb24nXSA9ICdjaGVja19jaXJjbGUnO1xuICAgICAgICAgIG5ld09ialsnYWN0aW9uJ10gPSAnc3VjY2Vzcyc7XG4gICAgICAgICAgdGhpcy5kaWFsb2cub3BlbihTdWNjZXNzTW9kYWxDb21wb25lbnQsIHtcbiAgICAgICAgICAgIHdpZHRoOiAnNjAwcHgnLFxuICAgICAgICAgICAgZGF0YTogbmV3T2JqXG4gICAgICAgICAgfSk7XG4gICAgICAgIH0pO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgY29uc3QgbmV3T2JqID0ge307XG4gICAgICAgIG5ld09ialsnZXJyTWVzc2FnZSddID0gJ1VuYWJsZSB0byB1cGRhdGUgY29tcGFueS4nO1xuICAgICAgICBuZXdPYmpbJ2FjdGlvbiddID0gJ2Vycm9yJztcbiAgICAgICAgdGhpcy5kaWFsb2cub3BlbihFcnJvck1vZGFsQ29tcG9uZW50LCB7XG4gICAgICAgICAgd2lkdGg6ICc2MDBweCcsXG4gICAgICAgICAgZGF0YTogbmV3T2JqXG4gICAgICAgIH0pO1xuICAgICAgICB0aGlzLm1ha2VDb21wYW55Rm9ybSgpO1xuICAgICAgfVxuICAgIH0sIGVycm9yID0+IHtcbiAgICAgIGNvbnN0IG5ld09iaiA9IHt9O1xuICAgICAgbmV3T2JqWydlcnJNZXNzYWdlJ10gPSAnVW5hYmxlIHRvIHVwZGF0ZSBjb21wYW55Lic7XG4gICAgICBuZXdPYmpbJ2FjdGlvbiddID0gJ2Vycm9yJztcbiAgICAgIHRoaXMuZGlhbG9nLm9wZW4oRXJyb3JNb2RhbENvbXBvbmVudCwge1xuICAgICAgICB3aWR0aDogJzYwMHB4JyxcbiAgICAgICAgZGF0YTogbmV3T2JqXG4gICAgICB9KTtcbiAgICB9KTtcbiAgfVxuXG4gIGNoZWNrRWRpdEJ1dHRvblJlbmRlclN0YXR1cygpIHtcbiAgICByZXR1cm4gIXRoaXMuY29tcGFueUlzRWRpdGFibGUgJiYgdGhpcy5jb21wYW55Rm9ybSAmJiAhdGhpcy5sb2FkaW5nICYmIHRoaXMuZWRpdENvb3JkaW5hdGlvblNlcnZpY2UuYXJlRWRpdEZpZWxkc0Nsb3NlZCgpO1xuICB9XG5cbiAgc2VuZEVkaXRTdGF0dXMoZmllbGQ6IHN0cmluZywgc3RhdHVzOiBib29sZWFuKSB7XG4gICAgdGhpcy5lZGl0Q29vcmRpbmF0aW9uU2VydmljZS50b2dnbGVGaWVsZChmaWVsZCwgc3RhdHVzKTtcbiAgfVxufVxuIl19