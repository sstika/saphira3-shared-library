/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Input, } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { PingService } from '../../services/ping.service';
import { takeWhile } from 'rxjs/operators';
import { BrowserTabService } from '../../services/browser-tab.service';
import { environment } from '../../../environments/environment';
var HeaderComponent = /** @class */ (function () {
    function HeaderComponent(auth, ping, router, browserTabService) {
        this.auth = auth;
        this.ping = ping;
        this.router = router;
        this.browserTabService = browserTabService;
        this.isAdmin = false;
        this.username = '';
        this.isConnected = 'status connected';
        this.connectedStatus = 'Connected';
        this.shouldPing = true;
        this.oldSaphiraAdmin = {
            dev: "http://localhost:8245/#/admin/permissions",
            prod: window.location.origin + "/#/admin/permissions"
        };
    }
    /**
     * @return {?}
     */
    HeaderComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        var _this = this;
        this.updateUsername();
        this.ping
            .status()
            .pipe(takeWhile((/**
         * @return {?}
         */
        function () { return _this.shouldPing; }))).subscribe((/**
         * @param {?} connected
         * @return {?}
         */
        function (connected) {
            if (connected) {
                _this.isConnected = 'status connected';
                _this.connectedStatus = 'Connected';
                if (_this.ping.tripTime > 1) {
                    _this.connectedStatus +=
                        " at " + _this.ping.tripTime.toFixed(0) + "ms to server";
                }
            }
            else {
                _this.isConnected = 'status not-connected';
                _this.connectedStatus = 'Not connected';
            }
        }));
    };
    /**
     * @return {?}
     */
    HeaderComponent.prototype.updateUsername = /**
     * @return {?}
     */
    function () {
        var _this = this;
        this.auth.getUser().subscribe((/**
         * @param {?} res
         * @return {?}
         */
        function (res) {
            if (res) {
                Object.values(res['principal']['attributes']).forEach((/**
                 * @param {?} o
                 * @return {?}
                 */
                function (o) {
                    if (o['name'] === 'displayName') {
                        _this.username = o['values'][0];
                    }
                }));
                if (!_this.username.length) {
                    _this.username = res['principal']['name'];
                }
            }
            else {
                _this.username = '';
            }
        }));
    };
    /**
     * @return {?}
     */
    HeaderComponent.prototype.sendEmail = /**
     * @return {?}
     */
    function () {
        location.href = 'mailto:surveys@magvar.com';
    };
    /**
     * @return {?}
     */
    HeaderComponent.prototype.logout = /**
     * @return {?}
     */
    function () {
        this.username = '';
        this.auth.logout().subscribe();
    };
    /**
     * @return {?}
     */
    HeaderComponent.prototype.goToOldSaphiraAdmin = /**
     * @return {?}
     */
    function () {
        if (environment.production) {
            this.browserTabService.open(this.oldSaphiraAdmin.prod);
        }
        else {
            this.browserTabService.open(this.oldSaphiraAdmin.dev);
        }
    };
    /**
     * @return {?}
     */
    HeaderComponent.prototype.ngOnDestroy = /**
     * @return {?}
     */
    function () {
        this.shouldPing = false;
    };
    HeaderComponent.decorators = [
        { type: Component, args: [{
                    selector: 'app-header',
                    template: "<section class=\"main-header\">\n  <div class=\"header-left\">\n    <div class=\"text-logo-container\">\n      <img class=\"login-logo\" src=\"assets/saphira-font-logo-white.png\" [routerLink]=\"['/wellbore-selection']\" routerLinkActive=\"router-link-active\">\n    </div>\n\n      <button class=\"menu-button\"\n              matTooltip=\"Direct: 303-264-6380, Cell: 720-384-7649\"\n              matTooltipClass=\"larger-tooltip\"\n              mat-icon-button>\n        <mat-icon class=\"contact-icons\">phone</mat-icon>\n      </button>\n      <button class=\"menu-button\"\n              matTooltip=\"surveys@magvar.com\"\n              matTooltipClass=\"larger-tooltip\"\n              mat-icon-button\n              (click)=\"sendEmail()\">\n        <mat-icon class=\"contact-icons\">email</mat-icon>\n      </button>\n    </div>\n\n    <div class=\"magvar-logo-container\">\n      <img class=\"login-logo\" src=\"assets/magvar_logo_white.png\" [routerLink]=\"['/wellbore-selection']\" routerLinkActive=\"router-link-active\">\n      <p class=\"copyright-text\">Copyright \u00A9 Magnetic Variations Services LLC 2019</p>\n    </div>\n\n    <span class=\"right-menu\">\n      <div class=\"versions\">\n        <span class=\"lighter\">VERSIONS </span>\n        <span>UI </span>\n        <strong>{{version}} </strong>\n        <span>BE </span>\n        <strong>{{ping.apiVersion}}</strong>\n      </div>\n      <div class=\"connected-container\">\n        <i class=\"material-icons account-icon\">\n          account_circle\n        </i>\n        <div [matTooltip]=\"connectedStatus\" [class]=\"isConnected\"></div>\n      </div>\n      <h4 class=\"user-name\">{{this.username}}</h4>\n\n      <button class=\"menu-button\" mat-icon-button [matMenuTriggerFor]=\"primaryMenu\" id=\"user-menu\">\n        <mat-icon class=\"material-icons menu-arrow-down\">\n          keyboard_arrow_down\n        </mat-icon>\n      </button>\n\n      <mat-menu #primaryMenu=\"matMenu\">\n        <a [routerLink]=\"['/wellbore-selection']\" routerLinkActive=\"router-link-active\">\n          <button mat-menu-item class=\"button-link\" class=\"user-menu-item\" id=\"wellbore-selection-btn\">\n            Wellbore Selection\n          </button>\n        </a>\n        <a [routerLink]=\"['/wellbore-status']\" routerLinkActive=\"router-link-active\">\n          <button mat-menu-item class=\"button-link\" class=\"user-menu-item\">\n            Wellbore Status\n          </button>\n        </a>\n        <br>\n        <button mat-menu-item class=\"user-menu-item\" id=\"admin-view-btn\" (click)=\"goToOldSaphiraAdmin()\">Admin View</button>\n        <a routerLinkActive=\"router-link-active\">\n          <button mat-menu-item (click)=\"logout()\" class=\"user-menu-item\" id=\"logout-btn\">Logout</button>\n        </a>\n      </mat-menu>\n    </span>\n</section>\n",
                    styles: ["@media print{.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-appearance-legacy.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-1.28122em) scale(.75);transform:translateY(-1.28122em) scale(.75)}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-form-field-autofill-control:-webkit-autofill+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.28121em) scale(.75);transform:translateY(-1.28121em) scale(.75)}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.2812em) scale(.75);transform:translateY(-1.2812em) scale(.75)}}.mat-badge-content{font-weight:600;font-size:12px;font-family:NunitoSans,sans-serif}.mat-badge-small .mat-badge-content{font-size:9px}.mat-badge-large .mat-badge-content{font-size:24px}.mat-h1,.mat-headline,.mat-typography h1{font:400 24px/32px NunitoSans,sans-serif;margin:0 0 16px}.mat-h2,.mat-title,.mat-typography h2{font:500 20px/32px NunitoSans,sans-serif;margin:0 0 16px}.mat-h3,.mat-subheading-2,.mat-typography h3{font:400 16px/28px NunitoSans,sans-serif;margin:0 0 16px}.mat-h4,.mat-subheading-1,.mat-typography h4{font:400 15px/24px NunitoSans,sans-serif;margin:0 0 16px}.mat-h5,.mat-h6,.mat-typography h5,.mat-typography h6{margin:0 0 12px}.mat-body-2,.mat-body-strong{font:500 14px/24px NunitoSans,sans-serif}.mat-body,.mat-body-1,.mat-typography{font:400 14px/20px NunitoSans,sans-serif}.mat-body p,.mat-body-1 p,.mat-typography p{margin:0 0 12px}.mat-caption,.mat-small{font:400 12px/20px NunitoSans,sans-serif}.mat-display-4,.mat-typography .mat-display-4{font:300 112px/112px NunitoSans,sans-serif;letter-spacing:-.05em;margin:0 0 56px}.mat-display-3,.mat-typography .mat-display-3{font:400 56px/56px NunitoSans,sans-serif;letter-spacing:-.02em;margin:0 0 64px}.mat-display-2,.mat-typography .mat-display-2{font:400 45px/48px NunitoSans,sans-serif;letter-spacing:-.005em;margin:0 0 64px}.mat-display-1,.mat-typography .mat-display-1{font:400 34px/40px NunitoSans,sans-serif;margin:0 0 64px}.mat-bottom-sheet-container{font:400 14px/20px NunitoSans,sans-serif}.mat-button,.mat-fab,.mat-flat-button,.mat-icon-button,.mat-mini-fab,.mat-raised-button,.mat-stroked-button{font-family:NunitoSans,sans-serif;font-size:14px;font-weight:500}.mat-button-toggle,.mat-card{font-family:NunitoSans,sans-serif}.mat-card-title{font-size:24px;font-weight:500}.mat-card-header .mat-card-title{font-size:20px}.mat-card-content,.mat-card-subtitle{font-size:14px}.mat-checkbox{font-family:NunitoSans,sans-serif}.mat-checkbox-layout .mat-checkbox-label{line-height:24px}.mat-chip{font-size:14px;font-weight:500}.mat-chip .mat-chip-remove.mat-icon,.mat-chip .mat-chip-trailing-icon.mat-icon{font-size:18px}.mat-table{font-family:NunitoSans,sans-serif}.mat-header-cell{font-size:12px;font-weight:500}.mat-cell,.mat-footer-cell{font-size:14px}.mat-calendar{font-family:NunitoSans,sans-serif}.mat-calendar-body{font-size:13px}.mat-calendar-body-label,.mat-calendar-period-button{font-size:14px;font-weight:500}.mat-calendar-table-header th{font-size:11px;font-weight:400}.mat-dialog-title{font:500 20px/32px NunitoSans,sans-serif}.mat-expansion-panel-header{font-family:NunitoSans,sans-serif;font-size:15px;font-weight:400}.mat-expansion-panel-content{font:400 14px/20px NunitoSans,sans-serif}.mat-form-field{font-size:inherit;font-weight:400;line-height:1.125;font-family:NunitoSans,sans-serif}.mat-form-field-wrapper{padding-bottom:1.34375em}.mat-form-field-prefix .mat-icon,.mat-form-field-suffix .mat-icon{font-size:150%;line-height:1.125}.mat-form-field-prefix .mat-icon-button,.mat-form-field-suffix .mat-icon-button{height:1.5em;width:1.5em}.mat-form-field-prefix .mat-icon-button .mat-icon,.mat-form-field-suffix .mat-icon-button .mat-icon{height:1.125em;line-height:1.125}.mat-form-field-infix{padding:.5em 0;border-top:.84375em solid transparent}.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-1.34373em) scale(.75);transform:translateY(-1.34373em) scale(.75);width:133.3333533333%}.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.34372em) scale(.75);transform:translateY(-1.34372em) scale(.75);width:133.3333633333%}.mat-form-field-label-wrapper{top:-.84375em;padding-top:.84375em}.mat-form-field-label{top:1.34375em}.mat-form-field-underline{bottom:1.34375em}.mat-form-field-subscript-wrapper{font-size:75%;margin-top:.6666666667em;top:calc(100% - 1.7916666667em)}.mat-form-field-appearance-legacy .mat-form-field-wrapper{padding-bottom:1.25em}.mat-form-field-appearance-legacy .mat-form-field-infix{padding:.4375em 0}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-appearance-legacy.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00106px);transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00106px);-ms-transform:translateY(-1.28119em) scale(.75);width:133.3333933333%}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-form-field-autofill-control:-webkit-autofill+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00107px);transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00107px);-ms-transform:translateY(-1.28118em) scale(.75);width:133.3334033333%}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00108px);transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00108px);-ms-transform:translateY(-1.28117em) scale(.75);width:133.3334133333%}.mat-form-field-appearance-legacy .mat-form-field-label{top:1.28125em}.mat-form-field-appearance-legacy .mat-form-field-underline{bottom:1.25em}.mat-form-field-appearance-legacy .mat-form-field-subscript-wrapper{margin-top:.5416666667em;top:calc(100% - 1.6666666667em)}@media print{.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-appearance-legacy.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-1.28116em) scale(.75);transform:translateY(-1.28116em) scale(.75)}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-form-field-autofill-control:-webkit-autofill+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.28115em) scale(.75);transform:translateY(-1.28115em) scale(.75)}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.28114em) scale(.75);transform:translateY(-1.28114em) scale(.75)}}.mat-form-field-appearance-fill .mat-form-field-infix{padding:.25em 0 .75em}.mat-form-field-appearance-fill .mat-form-field-label{top:1.09375em;margin-top:-.5em}.mat-form-field-appearance-fill.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-appearance-fill.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-.59373em) scale(.75);transform:translateY(-.59373em) scale(.75);width:133.3333533333%}.mat-form-field-appearance-fill.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-.59372em) scale(.75);transform:translateY(-.59372em) scale(.75);width:133.3333633333%}.mat-form-field-appearance-outline .mat-form-field-infix{padding:1em 0}.mat-form-field-appearance-outline .mat-form-field-label{top:1.84375em;margin-top:-.25em}.mat-form-field-appearance-outline.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-appearance-outline.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-1.59373em) scale(.75);transform:translateY(-1.59373em) scale(.75);width:133.3333533333%}.mat-form-field-appearance-outline.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.59372em) scale(.75);transform:translateY(-1.59372em) scale(.75);width:133.3333633333%}.mat-grid-tile-footer,.mat-grid-tile-header{font-size:14px}.mat-grid-tile-footer .mat-line,.mat-grid-tile-header .mat-line{white-space:nowrap;overflow:hidden;text-overflow:ellipsis;display:block;box-sizing:border-box}.mat-grid-tile-footer .mat-line:nth-child(n+2),.mat-grid-tile-header .mat-line:nth-child(n+2){font-size:12px}input.mat-input-element{margin-top:-.0625em}.mat-menu-item{font-family:NunitoSans,sans-serif;font-size:14px;font-weight:400}.mat-paginator,.mat-paginator-page-size .mat-select-trigger{font-family:NunitoSans,sans-serif;font-size:12px}.mat-radio-button,.mat-select{font-family:NunitoSans,sans-serif}.mat-select-trigger{height:1.125em}.mat-slide-toggle-content{font-family:NunitoSans,sans-serif}.mat-slider-thumb-label-text{font-family:NunitoSans,sans-serif;font-size:12px;font-weight:500}.mat-stepper-horizontal,.mat-stepper-vertical{font-family:NunitoSans,sans-serif}.mat-step-label{font-size:14px;font-weight:400}.mat-step-sub-label-error{font-weight:400}.mat-step-label-error{font-size:14px}.mat-step-label-selected{font-size:14px;font-weight:500}.mat-tab-group{font-family:NunitoSans,sans-serif}.mat-tab-label,.mat-tab-link{font-family:NunitoSans,sans-serif;font-size:14px;font-weight:500}.mat-toolbar,.mat-toolbar h1,.mat-toolbar h2,.mat-toolbar h3,.mat-toolbar h4,.mat-toolbar h5,.mat-toolbar h6{font:500 20px/32px NunitoSans,sans-serif;margin:0}.mat-tooltip{font-family:NunitoSans,sans-serif;font-size:10px;padding-top:6px;padding-bottom:6px}.mat-tooltip-handset{font-size:14px;padding-top:8px;padding-bottom:8px}.mat-list-item,.mat-list-option{font-family:NunitoSans,sans-serif}.mat-list-base .mat-list-item{font-size:16px}.mat-list-base .mat-list-item .mat-line{white-space:nowrap;overflow:hidden;text-overflow:ellipsis;display:block;box-sizing:border-box}.mat-list-base .mat-list-item .mat-line:nth-child(n+2){font-size:14px}.mat-list-base .mat-list-option{font-size:16px}.mat-list-base .mat-list-option .mat-line{white-space:nowrap;overflow:hidden;text-overflow:ellipsis;display:block;box-sizing:border-box}.mat-list-base .mat-list-option .mat-line:nth-child(n+2){font-size:14px}.mat-list-base .mat-subheader{font-family:NunitoSans,sans-serif;font-size:14px;font-weight:500}.mat-list-base[dense] .mat-list-item{font-size:12px}.mat-list-base[dense] .mat-list-item .mat-line{white-space:nowrap;overflow:hidden;text-overflow:ellipsis;display:block;box-sizing:border-box}.mat-list-base[dense] .mat-list-item .mat-line:nth-child(n+2),.mat-list-base[dense] .mat-list-option{font-size:12px}.mat-list-base[dense] .mat-list-option .mat-line{white-space:nowrap;overflow:hidden;text-overflow:ellipsis;display:block;box-sizing:border-box}.mat-list-base[dense] .mat-list-option .mat-line:nth-child(n+2){font-size:12px}.mat-list-base[dense] .mat-subheader{font-family:NunitoSans,sans-serif;font-size:12px;font-weight:500}.mat-option{font-family:NunitoSans,sans-serif;font-size:16px}.mat-optgroup-label{font:500 14px/24px NunitoSans,sans-serif}.mat-simple-snackbar{font-family:NunitoSans,sans-serif;font-size:14px}.mat-simple-snackbar-action{line-height:1;font-family:inherit;font-size:inherit;font-weight:500}.mat-tree{font-family:NunitoSans,sans-serif}.mat-nested-tree-node,.mat-tree-node{font-weight:400;font-size:14px}.mat-ripple{overflow:hidden;position:relative}.mat-ripple.mat-ripple-unbounded{overflow:visible}.mat-ripple-element{position:absolute;border-radius:50%;pointer-events:none;-webkit-transition:opacity,-webkit-transform cubic-bezier(0,0,.2,1);transition:opacity,transform cubic-bezier(0,0,.2,1),-webkit-transform cubic-bezier(0,0,.2,1);-webkit-transform:scale(0);transform:scale(0)}@media (-ms-high-contrast:active){.mat-ripple-element{display:none}}.cdk-visually-hidden{border:0;clip:rect(0 0 0 0);height:1px;margin:-1px;overflow:hidden;padding:0;position:absolute;width:1px;outline:0;-webkit-appearance:none;-moz-appearance:none}.cdk-global-overlay-wrapper,.cdk-overlay-container{pointer-events:none;top:0;left:0;height:100%;width:100%}.cdk-overlay-container{position:fixed;z-index:1000}.cdk-overlay-container:empty{display:none}.cdk-global-overlay-wrapper{display:-webkit-box;display:flex;position:absolute;z-index:1000}.cdk-overlay-pane{position:absolute;pointer-events:auto;box-sizing:border-box;z-index:1000;display:-webkit-box;display:flex;max-width:100%;max-height:100%}.cdk-overlay-backdrop{position:absolute;top:0;bottom:0;left:0;right:0;z-index:1000;pointer-events:auto;-webkit-tap-highlight-color:transparent;-webkit-transition:opacity .4s cubic-bezier(.25,.8,.25,1);transition:opacity .4s cubic-bezier(.25,.8,.25,1);opacity:0}.cdk-overlay-backdrop.cdk-overlay-backdrop-showing{opacity:1}@media screen and (-ms-high-contrast:active){.cdk-overlay-backdrop.cdk-overlay-backdrop-showing{opacity:.6}}.cdk-overlay-dark-backdrop{background:rgba(0,0,0,.32)}.cdk-overlay-transparent-backdrop,.cdk-overlay-transparent-backdrop.cdk-overlay-backdrop-showing{opacity:0}.cdk-overlay-connected-position-bounding-box{position:absolute;z-index:1000;display:-webkit-box;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;flex-direction:column;min-width:1px;min-height:1px}.cdk-global-scrollblock{position:fixed;width:100%;overflow-y:scroll}@-webkit-keyframes cdk-text-field-autofill-start{/*!*/}@keyframes cdk-text-field-autofill-start{/*!*/}@-webkit-keyframes cdk-text-field-autofill-end{/*!*/}@keyframes cdk-text-field-autofill-end{/*!*/}.cdk-text-field-autofill-monitored:-webkit-autofill{-webkit-animation-name:cdk-text-field-autofill-start;animation-name:cdk-text-field-autofill-start}.cdk-text-field-autofill-monitored:not(:-webkit-autofill){-webkit-animation-name:cdk-text-field-autofill-end;animation-name:cdk-text-field-autofill-end}textarea.cdk-textarea-autosize{resize:none}textarea.cdk-textarea-autosize-measuring{height:auto!important;overflow:hidden!important;padding:2px 0!important;box-sizing:content-box!important}.main-header{padding:0 20px;height:56px;display:-webkit-box;display:flex;-webkit-box-pack:justify;justify-content:space-between;border-bottom:1px solid #333;background-color:#1e4e79;-webkit-box-align:center;align-items:center}.main-header .header-left{display:-webkit-box;display:flex;-webkit-box-orient:horizontal;-webkit-box-direction:normal;flex-direction:row;width:244px;-webkit-box-pack:justify;justify-content:space-between}.main-header .header-left .text-logo-container{display:-webkit-box;display:flex;-webkit-box-align:center;align-items:center;margin-right:4px}.main-header .header-left .text-logo-container img{height:24px}.main-header .magvar-logo-container{left:50%;-webkit-transform:translate(-50%);transform:translate(-50%);position:absolute;display:-webkit-box;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;flex-direction:column;-webkit-box-align:center;align-items:center}.copyright-text{width:275px;height:14px;font-family:NunitoSans;font-size:10px;font-weight:400;font-style:normal;font-stretch:normal;line-height:normal;letter-spacing:.2px;color:#fff;text-align:center}.button-link{text-decoration:none}.right-menu{-webkit-box-align:center;align-items:center;display:-webkit-box;display:flex;margin-right:-12px}.right-menu .connected-container{position:relative;margin-right:12px}.right-menu .connected-container i{color:#fff;font-size:29px}.menu-arrow-down{color:#fff}.contact-icons{color:#b4e1f1}.versions{color:#fff;font-family:NunitoSans;font-size:16px;font-weight:200;letter-spacing:.2px;margin-right:8px;padding-top:4px}.lighter{color:#fff8}.user-name{color:#fff;font-family:NunitoSans;font-size:20px;font-weight:200;letter-spacing:.2px}.status{position:absolute;top:0;right:-2px;width:10px;height:10px;border-radius:6px;border:.5px solid #1e4e79}.connected{background:radial-gradient(#c4f100,#00ff40,#00ff40);box-shadow:-3px 4px 5px rgba(196,241,0,.5)}.not-connected{background:radial-gradient(#fd7302,#f90000,#9e0602);box-shadow:-3px 4px 5px rgba(251,104,104,.5)}.main-text{font-family:NunitoSans,sans-serif;font-size:36px;font-weight:200;color:#333;cursor:pointer;outline:0}.main-subtext{color:#777}.menu-button{-webkit-transition-duration:.2s;transition-duration:.2s;color:#fff}.menu-button:hover{-webkit-transition-duration:.15s;transition-duration:.15s}a{text-decoration:none}.larger-tooltip{font-size:14px}"]
                }] }
    ];
    /** @nocollapse */
    HeaderComponent.ctorParameters = function () { return [
        { type: AuthService },
        { type: PingService },
        { type: Router },
        { type: BrowserTabService }
    ]; };
    HeaderComponent.propDecorators = {
        version: [{ type: Input }]
    };
    return HeaderComponent;
}());
export { HeaderComponent };
if (false) {
    /** @type {?} */
    HeaderComponent.prototype.version;
    /** @type {?} */
    HeaderComponent.prototype.isAdmin;
    /** @type {?} */
    HeaderComponent.prototype.username;
    /** @type {?} */
    HeaderComponent.prototype.isConnected;
    /** @type {?} */
    HeaderComponent.prototype.connectedStatus;
    /** @type {?} */
    HeaderComponent.prototype.shouldPing;
    /** @type {?} */
    HeaderComponent.prototype.oldSaphiraAdmin;
    /**
     * @type {?}
     * @private
     */
    HeaderComponent.prototype.auth;
    /** @type {?} */
    HeaderComponent.prototype.ping;
    /**
     * @type {?}
     * @private
     */
    HeaderComponent.prototype.router;
    /**
     * @type {?}
     * @private
     */
    HeaderComponent.prototype.browserTabService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaGVhZGVyLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BocHRlY2gvc2FwaGlyYS1zaGFyZWQtbGliLyIsInNvdXJjZXMiOlsibGliL2NvbXBvbmVudHMvaGVhZGVyL2hlYWRlci5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsS0FBSyxHQUF5QyxNQUFNLGVBQWUsQ0FBQztBQUN4RixPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sNkJBQTZCLENBQUM7QUFDMUQsT0FBTyxFQUFFLE1BQU0sRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQ3pDLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSw2QkFBNkIsQ0FBQztBQUMxRCxPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFDM0MsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sb0NBQW9DLENBQUM7QUFDdkUsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLG1DQUFtQyxDQUFDO0FBRWhFO0lBa0JFLHlCQUNVLElBQWlCLEVBQ2xCLElBQWlCLEVBQ2hCLE1BQWMsRUFDZCxpQkFBb0M7UUFIcEMsU0FBSSxHQUFKLElBQUksQ0FBYTtRQUNsQixTQUFJLEdBQUosSUFBSSxDQUFhO1FBQ2hCLFdBQU0sR0FBTixNQUFNLENBQVE7UUFDZCxzQkFBaUIsR0FBakIsaUJBQWlCLENBQW1CO1FBZHZDLFlBQU8sR0FBRyxLQUFLLENBQUM7UUFDaEIsYUFBUSxHQUFHLEVBQUUsQ0FBQztRQUNkLGdCQUFXLEdBQUcsa0JBQWtCLENBQUM7UUFDakMsb0JBQWUsR0FBRyxXQUFXLENBQUM7UUFDOUIsZUFBVSxHQUFHLElBQUksQ0FBQztRQUNsQixvQkFBZSxHQUFHO1lBQ3ZCLEdBQUcsRUFBRSwyQ0FBMkM7WUFDaEQsSUFBSSxFQUFLLE1BQU0sQ0FBQyxRQUFRLENBQUMsTUFBTSx5QkFBc0I7U0FDdEQsQ0FBQztJQU9FLENBQUM7Ozs7SUFFTCxrQ0FBUTs7O0lBQVI7UUFBQSxpQkFvQkM7UUFuQkMsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO1FBRXRCLElBQUksQ0FBQyxJQUFJO2FBQ0osTUFBTSxFQUFFO2FBQ1IsSUFBSSxDQUNILFNBQVM7OztRQUFFLGNBQU0sT0FBQSxLQUFJLENBQUMsVUFBVSxFQUFmLENBQWUsRUFBRSxDQUNuQyxDQUFDLFNBQVM7Ozs7UUFBRSxVQUFBLFNBQVM7WUFDcEIsSUFBSyxTQUFTLEVBQUc7Z0JBQ2YsS0FBSSxDQUFDLFdBQVcsR0FBRyxrQkFBa0IsQ0FBQztnQkFDdEMsS0FBSSxDQUFDLGVBQWUsR0FBRyxXQUFXLENBQUM7Z0JBQ25DLElBQUssS0FBSSxDQUFDLElBQUksQ0FBQyxRQUFRLEdBQUcsQ0FBQyxFQUFHO29CQUM1QixLQUFJLENBQUMsZUFBZTt3QkFDbEIsU0FBTyxLQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLGlCQUFjLENBQUM7aUJBQ3REO2FBQ0Y7aUJBQU07Z0JBQ0wsS0FBSSxDQUFDLFdBQVcsR0FBRyxzQkFBc0IsQ0FBQztnQkFDMUMsS0FBSSxDQUFDLGVBQWUsR0FBRyxlQUFlLENBQUM7YUFDeEM7UUFDSCxDQUFDLEVBQUMsQ0FBQztJQUNULENBQUM7Ozs7SUFHTSx3Q0FBYzs7O0lBQXJCO1FBQUEsaUJBZUM7UUFkQyxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDLFNBQVM7Ozs7UUFBQyxVQUFBLEdBQUc7WUFDL0IsSUFBSSxHQUFHLEVBQUU7Z0JBQ1AsTUFBTSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUFDLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxPQUFPOzs7O2dCQUFDLFVBQUEsQ0FBQztvQkFDckQsSUFBSSxDQUFDLENBQUMsTUFBTSxDQUFDLEtBQUssYUFBYSxFQUFFO3dCQUMvQixLQUFJLENBQUMsUUFBUSxHQUFHLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztxQkFDaEM7Z0JBQ0gsQ0FBQyxFQUFDLENBQUM7Z0JBQ0gsSUFBSSxDQUFDLEtBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxFQUFFO29CQUN6QixLQUFJLENBQUMsUUFBUSxHQUFHLEdBQUcsQ0FBQyxXQUFXLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQztpQkFDMUM7YUFDRjtpQkFBTTtnQkFDTCxLQUFJLENBQUMsUUFBUSxHQUFHLEVBQUUsQ0FBQzthQUNwQjtRQUNILENBQUMsRUFBQyxDQUFDO0lBQ0wsQ0FBQzs7OztJQUVNLG1DQUFTOzs7SUFBaEI7UUFDRSxRQUFRLENBQUMsSUFBSSxHQUFHLDJCQUEyQixDQUFDO0lBQzlDLENBQUM7Ozs7SUFFTSxnQ0FBTTs7O0lBQWI7UUFDRSxJQUFJLENBQUMsUUFBUSxHQUFHLEVBQUUsQ0FBQztRQUNuQixJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLFNBQVMsRUFBRSxDQUFDO0lBQ2pDLENBQUM7Ozs7SUFFTSw2Q0FBbUI7OztJQUExQjtRQUNFLElBQUksV0FBVyxDQUFDLFVBQVUsRUFBRTtZQUMxQixJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDeEQ7YUFBTTtZQUNMLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxHQUFHLENBQUMsQ0FBQztTQUN2RDtJQUNILENBQUM7Ozs7SUFFRCxxQ0FBVzs7O0lBQVg7UUFDRSxJQUFJLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQztJQUMxQixDQUFDOztnQkFwRkYsU0FBUyxTQUFDO29CQUNULFFBQVEsRUFBRSxZQUFZO29CQUN0QixpekZBQXNDOztpQkFFdkM7Ozs7Z0JBWFEsV0FBVztnQkFFWCxXQUFXO2dCQURYLE1BQU07Z0JBR04saUJBQWlCOzs7MEJBU3ZCLEtBQUs7O0lBK0VSLHNCQUFDO0NBQUEsQUFyRkQsSUFxRkM7U0FoRlksZUFBZTs7O0lBQzFCLGtDQUFpQjs7SUFFakIsa0NBQXVCOztJQUN2QixtQ0FBcUI7O0lBQ3JCLHNDQUF3Qzs7SUFDeEMsMENBQXFDOztJQUNyQyxxQ0FBeUI7O0lBQ3pCLDBDQUdFOzs7OztJQUdBLCtCQUF5Qjs7SUFDekIsK0JBQXdCOzs7OztJQUN4QixpQ0FBc0I7Ozs7O0lBQ3RCLDRDQUE0QyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgSW5wdXQsIE9uRGVzdHJveSwgT25Jbml0LCBWaWV3RW5jYXBzdWxhdGlvbiwgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEF1dGhTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vc2VydmljZXMvYXV0aC5zZXJ2aWNlJztcbmltcG9ydCB7IFJvdXRlciB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XG5pbXBvcnQgeyBQaW5nU2VydmljZSB9IGZyb20gJy4uLy4uL3NlcnZpY2VzL3Bpbmcuc2VydmljZSc7XG5pbXBvcnQgeyB0YWtlV2hpbGUgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XG5pbXBvcnQgeyBCcm93c2VyVGFiU2VydmljZSB9IGZyb20gJy4uLy4uL3NlcnZpY2VzL2Jyb3dzZXItdGFiLnNlcnZpY2UnO1xuaW1wb3J0IHsgZW52aXJvbm1lbnQgfSBmcm9tICcuLi8uLi8uLi9lbnZpcm9ubWVudHMvZW52aXJvbm1lbnQnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdhcHAtaGVhZGVyJyxcbiAgdGVtcGxhdGVVcmw6ICcuL2hlYWRlci5jb21wb25lbnQuaHRtbCcsXG4gIHN0eWxlVXJsczogWycuL2hlYWRlci5jb21wb25lbnQuc2NzcyddLFxufSlcbmV4cG9ydCBjbGFzcyBIZWFkZXJDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIE9uRGVzdHJveSB7XG4gIEBJbnB1dCgpIHZlcnNpb247XG5cbiAgcHVibGljIGlzQWRtaW4gPSBmYWxzZTtcbiAgcHVibGljIHVzZXJuYW1lID0gJyc7XG4gIHB1YmxpYyBpc0Nvbm5lY3RlZCA9ICdzdGF0dXMgY29ubmVjdGVkJztcbiAgcHVibGljIGNvbm5lY3RlZFN0YXR1cyA9ICdDb25uZWN0ZWQnO1xuICBwdWJsaWMgc2hvdWxkUGluZyA9IHRydWU7XG4gIHB1YmxpYyBvbGRTYXBoaXJhQWRtaW4gPSB7XG4gICAgZGV2OiBgaHR0cDovL2xvY2FsaG9zdDo4MjQ1LyMvYWRtaW4vcGVybWlzc2lvbnNgLFxuICAgIHByb2Q6IGAke3dpbmRvdy5sb2NhdGlvbi5vcmlnaW59LyMvYWRtaW4vcGVybWlzc2lvbnNgXG4gIH07XG5cbiAgY29uc3RydWN0b3IoXG4gICAgcHJpdmF0ZSBhdXRoOiBBdXRoU2VydmljZSxcbiAgICBwdWJsaWMgcGluZzogUGluZ1NlcnZpY2UsXG4gICAgcHJpdmF0ZSByb3V0ZXI6IFJvdXRlcixcbiAgICBwcml2YXRlIGJyb3dzZXJUYWJTZXJ2aWNlOiBCcm93c2VyVGFiU2VydmljZVxuICApIHsgfVxuXG4gIG5nT25Jbml0KCkge1xuICAgIHRoaXMudXBkYXRlVXNlcm5hbWUoKTtcblxuICAgIHRoaXMucGluZ1xuICAgICAgICAuc3RhdHVzKClcbiAgICAgICAgLnBpcGUoXG4gICAgICAgICAgdGFrZVdoaWxlKCAoKSA9PiB0aGlzLnNob3VsZFBpbmcgKSxcbiAgICAgICAgKS5zdWJzY3JpYmUoIGNvbm5lY3RlZCA9PiB7XG4gICAgICAgICAgaWYgKCBjb25uZWN0ZWQgKSB7XG4gICAgICAgICAgICB0aGlzLmlzQ29ubmVjdGVkID0gJ3N0YXR1cyBjb25uZWN0ZWQnO1xuICAgICAgICAgICAgdGhpcy5jb25uZWN0ZWRTdGF0dXMgPSAnQ29ubmVjdGVkJztcbiAgICAgICAgICAgIGlmICggdGhpcy5waW5nLnRyaXBUaW1lID4gMSApIHtcbiAgICAgICAgICAgICAgdGhpcy5jb25uZWN0ZWRTdGF0dXMgKz1cbiAgICAgICAgICAgICAgICBgIGF0ICR7dGhpcy5waW5nLnRyaXBUaW1lLnRvRml4ZWQoMCl9bXMgdG8gc2VydmVyYDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgdGhpcy5pc0Nvbm5lY3RlZCA9ICdzdGF0dXMgbm90LWNvbm5lY3RlZCc7XG4gICAgICAgICAgICB0aGlzLmNvbm5lY3RlZFN0YXR1cyA9ICdOb3QgY29ubmVjdGVkJztcbiAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICB9XG5cblxuICBwdWJsaWMgdXBkYXRlVXNlcm5hbWUoKSB7XG4gICAgdGhpcy5hdXRoLmdldFVzZXIoKS5zdWJzY3JpYmUocmVzID0+IHtcbiAgICAgIGlmIChyZXMpIHtcbiAgICAgICAgT2JqZWN0LnZhbHVlcyhyZXNbJ3ByaW5jaXBhbCddWydhdHRyaWJ1dGVzJ10pLmZvckVhY2gobyA9PiB7XG4gICAgICAgICAgaWYgKG9bJ25hbWUnXSA9PT0gJ2Rpc3BsYXlOYW1lJykge1xuICAgICAgICAgICAgdGhpcy51c2VybmFtZSA9IG9bJ3ZhbHVlcyddWzBdO1xuICAgICAgICAgIH1cbiAgICAgICAgfSk7XG4gICAgICAgIGlmICghdGhpcy51c2VybmFtZS5sZW5ndGgpIHtcbiAgICAgICAgICB0aGlzLnVzZXJuYW1lID0gcmVzWydwcmluY2lwYWwnXVsnbmFtZSddO1xuICAgICAgICB9XG4gICAgICB9IGVsc2Uge1xuICAgICAgICB0aGlzLnVzZXJuYW1lID0gJyc7XG4gICAgICB9XG4gICAgfSk7XG4gIH1cblxuICBwdWJsaWMgc2VuZEVtYWlsKCkge1xuICAgIGxvY2F0aW9uLmhyZWYgPSAnbWFpbHRvOnN1cnZleXNAbWFndmFyLmNvbSc7XG4gIH1cblxuICBwdWJsaWMgbG9nb3V0KCkge1xuICAgIHRoaXMudXNlcm5hbWUgPSAnJztcbiAgICB0aGlzLmF1dGgubG9nb3V0KCkuc3Vic2NyaWJlKCk7XG4gIH1cblxuICBwdWJsaWMgZ29Ub09sZFNhcGhpcmFBZG1pbigpIHtcbiAgICBpZiAoZW52aXJvbm1lbnQucHJvZHVjdGlvbikge1xuICAgICAgdGhpcy5icm93c2VyVGFiU2VydmljZS5vcGVuKHRoaXMub2xkU2FwaGlyYUFkbWluLnByb2QpO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLmJyb3dzZXJUYWJTZXJ2aWNlLm9wZW4odGhpcy5vbGRTYXBoaXJhQWRtaW4uZGV2KTtcbiAgICB9XG4gIH1cblxuICBuZ09uRGVzdHJveSgpIHtcbiAgICB0aGlzLnNob3VsZFBpbmcgPSBmYWxzZTtcbiAgfVxufVxuIl19