/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
// Todo: check if this component is required in shared library
import { Component, Input, Output, EventEmitter } from '@angular/core';
var DefinitiveTableErrorsComponent = /** @class */ (function () {
    function DefinitiveTableErrorsComponent() {
        this.conflictLines = new EventEmitter();
        this._surveys = [];
        this.errors = [];
        this.trackingIdErrors = {};
        this.trackingIdKeys = [];
    }
    Object.defineProperty(DefinitiveTableErrorsComponent.prototype, "definitiveSurveys", {
        get: /**
         * @return {?}
         */
        function () {
            return this._surveys;
        },
        set: /**
         * @param {?} surveys
         * @return {?}
         */
        function (surveys) {
            if (!surveys || !surveys.length) {
                this.trackingIdKeys = [];
                this.trackingIdErrors = {};
                this.errors = [];
                return;
            }
            this._surveys = surveys;
            this.evaluateErrors();
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @private
     * @return {?}
     */
    DefinitiveTableErrorsComponent.prototype.evaluateErrors = /**
     * @private
     * @return {?}
     */
    function () {
        var _this = this;
        if (this.definitiveSurveys === null || !this.definitiveSurveys[0]) {
            return;
        }
        this.errors = [];
        // Todo: uncomment once the pipes are implemented
        // this.mdUnits = shorthandUnitsObject[this.definitiveSurveys[0].md.unit];
        /** @type {?} */
        var standardSurveys = this.definitiveSurveys.filter((/**
         * @param {?} survey
         * @return {?}
         */
        function (survey) { return (survey.type === 'STANDARD' || survey.type === 'POOR' || survey.type === 'DEFINITIVE_INTERPOLATED'); }));
        var _loop_1 = function (i) {
            if (standardSurveys[i].md.value === standardSurveys[i - 1].md.value) {
                /** @type {?} */
                var duplicatedSurvey = this_1.errors.find((/**
                 * @param {?} error
                 * @return {?}
                 */
                function (error) { return standardSurveys[i].md.value === error.md.value; }));
                if (duplicatedSurvey) {
                    duplicatedSurvey.counter++;
                }
                else {
                    this_1.errors.push({ md: standardSurveys[i].md, counter: 2 });
                }
            }
        };
        var this_1 = this;
        for (var i = 1; i < standardSurveys.length; i++) {
            _loop_1(i);
        }
        this.trackingIdErrors = {};
        this.trackingIdKeys = [];
        this.trackingIdErrors = standardSurveys.reduce((/**
         * @param {?} acc
         * @param {?} survey
         * @return {?}
         */
        function (acc, survey) {
            if (acc[survey['trackingId']]) {
                acc[survey['trackingId']]++;
            }
            else if (survey['trackingId']) {
                acc[survey['trackingId']] = 1;
            }
            return acc;
        }), {});
        this.trackingIdKeys = Object.keys(this.trackingIdErrors).filter((/**
         * @param {?} key
         * @return {?}
         */
        function (key) {
            if (_this.trackingIdErrors[key] > 1) {
                return key;
            }
        }));
        /** @type {?} */
        var spaceUsed = 0;
        if (this.errors.length) {
            spaceUsed++;
        }
        if (this.trackingIdKeys.length && this.showTrackingIds) {
            spaceUsed++;
        }
        this.conflictLines.emit(spaceUsed);
    };
    DefinitiveTableErrorsComponent.decorators = [
        { type: Component, args: [{
                    selector: 'app-definitive-table-errors',
                    template: "<div *ngIf=\"errors.length\" class=\"definitive-error-header md-errors\">\n  <span class=\"definitive-header-spacing definitive-error-msg\">Multiple D Type surveys at the following MD(s):</span>\n  <span class=\"definitive-error-msg definitive-header-spacing\" *ngFor=\"let error of errors\"> ({{ error.counter }}X) {{ error.md.value }} {{ mdUnits }}</span>\n</div>\n<div *ngIf=\"trackingIdKeys.length && showTrackingIds\" class=\"definitive-error-header\">\n  <span class=\"definitive-header-spacing definitive-error-msg\">Multiple D Type surveys with the following tracking ID's:</span>\n  <span class=\"definitive-error-msg definitive-header-spacing\" *ngFor=\"let trackingId of trackingIdKeys\">({{ trackingIdErrors[trackingId] }}X) {{ trackingId }}</span>\n</div>\n",
                    styles: ["@media print{.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-appearance-legacy.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-1.28122em) scale(.75);transform:translateY(-1.28122em) scale(.75)}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-form-field-autofill-control:-webkit-autofill+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.28121em) scale(.75);transform:translateY(-1.28121em) scale(.75)}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.2812em) scale(.75);transform:translateY(-1.2812em) scale(.75)}}.mat-badge-content{font-weight:600;font-size:12px;font-family:NunitoSans,sans-serif}.mat-badge-small .mat-badge-content{font-size:9px}.mat-badge-large .mat-badge-content{font-size:24px}.mat-h1,.mat-headline,.mat-typography h1{font:400 24px/32px NunitoSans,sans-serif;margin:0 0 16px}.mat-h2,.mat-title,.mat-typography h2{font:500 20px/32px NunitoSans,sans-serif;margin:0 0 16px}.mat-h3,.mat-subheading-2,.mat-typography h3{font:400 16px/28px NunitoSans,sans-serif;margin:0 0 16px}.mat-h4,.mat-subheading-1,.mat-typography h4{font:400 15px/24px NunitoSans,sans-serif;margin:0 0 16px}.mat-h5,.mat-h6,.mat-typography h5,.mat-typography h6{margin:0 0 12px}.mat-body-2,.mat-body-strong{font:500 14px/24px NunitoSans,sans-serif}.mat-body,.mat-body-1,.mat-typography{font:400 14px/20px NunitoSans,sans-serif}.mat-body p,.mat-body-1 p,.mat-typography p{margin:0 0 12px}.mat-caption,.mat-small{font:400 12px/20px NunitoSans,sans-serif}.mat-display-4,.mat-typography .mat-display-4{font:300 112px/112px NunitoSans,sans-serif;letter-spacing:-.05em;margin:0 0 56px}.mat-display-3,.mat-typography .mat-display-3{font:400 56px/56px NunitoSans,sans-serif;letter-spacing:-.02em;margin:0 0 64px}.mat-display-2,.mat-typography .mat-display-2{font:400 45px/48px NunitoSans,sans-serif;letter-spacing:-.005em;margin:0 0 64px}.mat-display-1,.mat-typography .mat-display-1{font:400 34px/40px NunitoSans,sans-serif;margin:0 0 64px}.mat-bottom-sheet-container{font:400 14px/20px NunitoSans,sans-serif}.mat-button,.mat-fab,.mat-flat-button,.mat-icon-button,.mat-mini-fab,.mat-raised-button,.mat-stroked-button{font-family:NunitoSans,sans-serif;font-size:14px;font-weight:500}.mat-button-toggle,.mat-card{font-family:NunitoSans,sans-serif}.mat-card-title{font-size:24px;font-weight:500}.mat-card-header .mat-card-title{font-size:20px}.mat-card-content,.mat-card-subtitle{font-size:14px}.mat-checkbox{font-family:NunitoSans,sans-serif}.mat-checkbox-layout .mat-checkbox-label{line-height:24px}.mat-chip{font-size:14px;font-weight:500}.mat-chip .mat-chip-remove.mat-icon,.mat-chip .mat-chip-trailing-icon.mat-icon{font-size:18px}.mat-table{font-family:NunitoSans,sans-serif}.mat-header-cell{font-size:12px;font-weight:500}.mat-cell,.mat-footer-cell{font-size:14px}.mat-calendar{font-family:NunitoSans,sans-serif}.mat-calendar-body{font-size:13px}.mat-calendar-body-label,.mat-calendar-period-button{font-size:14px;font-weight:500}.mat-calendar-table-header th{font-size:11px;font-weight:400}.mat-dialog-title{font:500 20px/32px NunitoSans,sans-serif}.mat-expansion-panel-header{font-family:NunitoSans,sans-serif;font-size:15px;font-weight:400}.mat-expansion-panel-content{font:400 14px/20px NunitoSans,sans-serif}.mat-form-field{font-size:inherit;font-weight:400;line-height:1.125;font-family:NunitoSans,sans-serif}.mat-form-field-wrapper{padding-bottom:1.34375em}.mat-form-field-prefix .mat-icon,.mat-form-field-suffix .mat-icon{font-size:150%;line-height:1.125}.mat-form-field-prefix .mat-icon-button,.mat-form-field-suffix .mat-icon-button{height:1.5em;width:1.5em}.mat-form-field-prefix .mat-icon-button .mat-icon,.mat-form-field-suffix .mat-icon-button .mat-icon{height:1.125em;line-height:1.125}.mat-form-field-infix{padding:.5em 0;border-top:.84375em solid transparent}.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-1.34373em) scale(.75);transform:translateY(-1.34373em) scale(.75);width:133.3333533333%}.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.34372em) scale(.75);transform:translateY(-1.34372em) scale(.75);width:133.3333633333%}.mat-form-field-label-wrapper{top:-.84375em;padding-top:.84375em}.mat-form-field-label{top:1.34375em}.mat-form-field-underline{bottom:1.34375em}.mat-form-field-subscript-wrapper{font-size:75%;margin-top:.6666666667em;top:calc(100% - 1.7916666667em)}.mat-form-field-appearance-legacy .mat-form-field-wrapper{padding-bottom:1.25em}.mat-form-field-appearance-legacy .mat-form-field-infix{padding:.4375em 0}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-appearance-legacy.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00106px);transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00106px);-ms-transform:translateY(-1.28119em) scale(.75);width:133.3333933333%}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-form-field-autofill-control:-webkit-autofill+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00107px);transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00107px);-ms-transform:translateY(-1.28118em) scale(.75);width:133.3334033333%}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00108px);transform:translateY(-1.28125em) scale(.75) perspective(100px) translateZ(.00108px);-ms-transform:translateY(-1.28117em) scale(.75);width:133.3334133333%}.mat-form-field-appearance-legacy .mat-form-field-label{top:1.28125em}.mat-form-field-appearance-legacy .mat-form-field-underline{bottom:1.25em}.mat-form-field-appearance-legacy .mat-form-field-subscript-wrapper{margin-top:.5416666667em;top:calc(100% - 1.6666666667em)}@media print{.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-appearance-legacy.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-1.28116em) scale(.75);transform:translateY(-1.28116em) scale(.75)}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-form-field-autofill-control:-webkit-autofill+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.28115em) scale(.75);transform:translateY(-1.28115em) scale(.75)}.mat-form-field-appearance-legacy.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.28114em) scale(.75);transform:translateY(-1.28114em) scale(.75)}}.mat-form-field-appearance-fill .mat-form-field-infix{padding:.25em 0 .75em}.mat-form-field-appearance-fill .mat-form-field-label{top:1.09375em;margin-top:-.5em}.mat-form-field-appearance-fill.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-appearance-fill.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-.59373em) scale(.75);transform:translateY(-.59373em) scale(.75);width:133.3333533333%}.mat-form-field-appearance-fill.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-.59372em) scale(.75);transform:translateY(-.59372em) scale(.75);width:133.3333633333%}.mat-form-field-appearance-outline .mat-form-field-infix{padding:1em 0}.mat-form-field-appearance-outline .mat-form-field-label{top:1.84375em;margin-top:-.25em}.mat-form-field-appearance-outline.mat-form-field-can-float .mat-input-server:focus+.mat-form-field-label-wrapper .mat-form-field-label,.mat-form-field-appearance-outline.mat-form-field-can-float.mat-form-field-should-float .mat-form-field-label{-webkit-transform:translateY(-1.59373em) scale(.75);transform:translateY(-1.59373em) scale(.75);width:133.3333533333%}.mat-form-field-appearance-outline.mat-form-field-can-float .mat-input-server[label]:not(:label-shown)+.mat-form-field-label-wrapper .mat-form-field-label{-webkit-transform:translateY(-1.59372em) scale(.75);transform:translateY(-1.59372em) scale(.75);width:133.3333633333%}.mat-grid-tile-footer,.mat-grid-tile-header{font-size:14px}.mat-grid-tile-footer .mat-line,.mat-grid-tile-header .mat-line{white-space:nowrap;overflow:hidden;text-overflow:ellipsis;display:block;box-sizing:border-box}.mat-grid-tile-footer .mat-line:nth-child(n+2),.mat-grid-tile-header .mat-line:nth-child(n+2){font-size:12px}input.mat-input-element{margin-top:-.0625em}.mat-menu-item{font-family:NunitoSans,sans-serif;font-size:14px;font-weight:400}.mat-paginator,.mat-paginator-page-size .mat-select-trigger{font-family:NunitoSans,sans-serif;font-size:12px}.mat-radio-button,.mat-select{font-family:NunitoSans,sans-serif}.mat-select-trigger{height:1.125em}.mat-slide-toggle-content{font-family:NunitoSans,sans-serif}.mat-slider-thumb-label-text{font-family:NunitoSans,sans-serif;font-size:12px;font-weight:500}.mat-stepper-horizontal,.mat-stepper-vertical{font-family:NunitoSans,sans-serif}.mat-step-label{font-size:14px;font-weight:400}.mat-step-sub-label-error{font-weight:400}.mat-step-label-error{font-size:14px}.mat-step-label-selected{font-size:14px;font-weight:500}.mat-tab-group{font-family:NunitoSans,sans-serif}.mat-tab-label,.mat-tab-link{font-family:NunitoSans,sans-serif;font-size:14px;font-weight:500}.mat-toolbar,.mat-toolbar h1,.mat-toolbar h2,.mat-toolbar h3,.mat-toolbar h4,.mat-toolbar h5,.mat-toolbar h6{font:500 20px/32px NunitoSans,sans-serif;margin:0}.mat-tooltip{font-family:NunitoSans,sans-serif;font-size:10px;padding-top:6px;padding-bottom:6px}.mat-tooltip-handset{font-size:14px;padding-top:8px;padding-bottom:8px}.mat-list-item,.mat-list-option{font-family:NunitoSans,sans-serif}.mat-list-base .mat-list-item{font-size:16px}.mat-list-base .mat-list-item .mat-line{white-space:nowrap;overflow:hidden;text-overflow:ellipsis;display:block;box-sizing:border-box}.mat-list-base .mat-list-item .mat-line:nth-child(n+2){font-size:14px}.mat-list-base .mat-list-option{font-size:16px}.mat-list-base .mat-list-option .mat-line{white-space:nowrap;overflow:hidden;text-overflow:ellipsis;display:block;box-sizing:border-box}.mat-list-base .mat-list-option .mat-line:nth-child(n+2){font-size:14px}.mat-list-base .mat-subheader{font-family:NunitoSans,sans-serif;font-size:14px;font-weight:500}.mat-list-base[dense] .mat-list-item{font-size:12px}.mat-list-base[dense] .mat-list-item .mat-line{white-space:nowrap;overflow:hidden;text-overflow:ellipsis;display:block;box-sizing:border-box}.mat-list-base[dense] .mat-list-item .mat-line:nth-child(n+2),.mat-list-base[dense] .mat-list-option{font-size:12px}.mat-list-base[dense] .mat-list-option .mat-line{white-space:nowrap;overflow:hidden;text-overflow:ellipsis;display:block;box-sizing:border-box}.mat-list-base[dense] .mat-list-option .mat-line:nth-child(n+2){font-size:12px}.mat-list-base[dense] .mat-subheader{font-family:NunitoSans,sans-serif;font-size:12px;font-weight:500}.mat-option{font-family:NunitoSans,sans-serif;font-size:16px}.mat-optgroup-label{font:500 14px/24px NunitoSans,sans-serif}.mat-simple-snackbar{font-family:NunitoSans,sans-serif;font-size:14px}.mat-simple-snackbar-action{line-height:1;font-family:inherit;font-size:inherit;font-weight:500}.mat-tree{font-family:NunitoSans,sans-serif}.mat-nested-tree-node,.mat-tree-node{font-weight:400;font-size:14px}.mat-ripple{overflow:hidden;position:relative}.mat-ripple.mat-ripple-unbounded{overflow:visible}.mat-ripple-element{position:absolute;border-radius:50%;pointer-events:none;-webkit-transition:opacity,-webkit-transform cubic-bezier(0,0,.2,1);transition:opacity,transform cubic-bezier(0,0,.2,1),-webkit-transform cubic-bezier(0,0,.2,1);-webkit-transform:scale(0);transform:scale(0)}@media (-ms-high-contrast:active){.mat-ripple-element{display:none}}.cdk-visually-hidden{border:0;clip:rect(0 0 0 0);height:1px;margin:-1px;overflow:hidden;padding:0;position:absolute;width:1px;outline:0;-webkit-appearance:none;-moz-appearance:none}.cdk-global-overlay-wrapper,.cdk-overlay-container{pointer-events:none;top:0;left:0;height:100%;width:100%}.cdk-overlay-container{position:fixed;z-index:1000}.cdk-overlay-container:empty{display:none}.cdk-global-overlay-wrapper{display:-webkit-box;display:flex;position:absolute;z-index:1000}.cdk-overlay-pane{position:absolute;pointer-events:auto;box-sizing:border-box;z-index:1000;display:-webkit-box;display:flex;max-width:100%;max-height:100%}.cdk-overlay-backdrop{position:absolute;top:0;bottom:0;left:0;right:0;z-index:1000;pointer-events:auto;-webkit-tap-highlight-color:transparent;-webkit-transition:opacity .4s cubic-bezier(.25,.8,.25,1);transition:opacity .4s cubic-bezier(.25,.8,.25,1);opacity:0}.cdk-overlay-backdrop.cdk-overlay-backdrop-showing{opacity:1}@media screen and (-ms-high-contrast:active){.cdk-overlay-backdrop.cdk-overlay-backdrop-showing{opacity:.6}}.cdk-overlay-dark-backdrop{background:rgba(0,0,0,.32)}.cdk-overlay-transparent-backdrop,.cdk-overlay-transparent-backdrop.cdk-overlay-backdrop-showing{opacity:0}.cdk-overlay-connected-position-bounding-box{position:absolute;z-index:1000;display:-webkit-box;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;flex-direction:column;min-width:1px;min-height:1px}.cdk-global-scrollblock{position:fixed;width:100%;overflow-y:scroll}@-webkit-keyframes cdk-text-field-autofill-start{/*!*/}@keyframes cdk-text-field-autofill-start{/*!*/}@-webkit-keyframes cdk-text-field-autofill-end{/*!*/}@keyframes cdk-text-field-autofill-end{/*!*/}.cdk-text-field-autofill-monitored:-webkit-autofill{-webkit-animation-name:cdk-text-field-autofill-start;animation-name:cdk-text-field-autofill-start}.cdk-text-field-autofill-monitored:not(:-webkit-autofill){-webkit-animation-name:cdk-text-field-autofill-end;animation-name:cdk-text-field-autofill-end}textarea.cdk-textarea-autosize{resize:none}textarea.cdk-textarea-autosize-measuring{height:auto!important;overflow:hidden!important;padding:2px 0!important;box-sizing:content-box!important}.definitive-header-spacing{margin-left:5px}.definitive-error-msg{color:#f5ff8b}.definitive-error-header{margin-left:5px}"]
                }] }
    ];
    /** @nocollapse */
    DefinitiveTableErrorsComponent.ctorParameters = function () { return []; };
    DefinitiveTableErrorsComponent.propDecorators = {
        showTrackingIds: [{ type: Input }],
        definitiveSurveys: [{ type: Input, args: ['definitiveSurveys',] }],
        conflictLines: [{ type: Output }]
    };
    return DefinitiveTableErrorsComponent;
}());
export { DefinitiveTableErrorsComponent };
if (false) {
    /** @type {?} */
    DefinitiveTableErrorsComponent.prototype.showTrackingIds;
    /** @type {?} */
    DefinitiveTableErrorsComponent.prototype.conflictLines;
    /**
     * @type {?}
     * @private
     */
    DefinitiveTableErrorsComponent.prototype._surveys;
    /** @type {?} */
    DefinitiveTableErrorsComponent.prototype.errors;
    /** @type {?} */
    DefinitiveTableErrorsComponent.prototype.mdUnits;
    /** @type {?} */
    DefinitiveTableErrorsComponent.prototype.trackingIdErrors;
    /** @type {?} */
    DefinitiveTableErrorsComponent.prototype.trackingIdKeys;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGVmaW5pdGl2ZS10YWJsZS1lcnJvcnMuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGhwdGVjaC9zYXBoaXJhLXNoYXJlZC1saWIvIiwic291cmNlcyI6WyJsaWIvY29tcG9uZW50cy9kZWZpbml0aXZlLXRhYmxlLWVycm9ycy9kZWZpbml0aXZlLXRhYmxlLWVycm9ycy5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFFQSxPQUFPLEVBQUUsU0FBUyxFQUFxQixLQUFLLEVBQUUsTUFBTSxFQUFFLFlBQVksRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUsxRjtJQWdDRTtRQVRVLGtCQUFhLEdBQUcsSUFBSSxZQUFZLEVBQVUsQ0FBQztRQUU3QyxhQUFRLEdBQUcsRUFBRSxDQUFDO1FBQ2YsV0FBTSxHQUFVLEVBQUUsQ0FBQztRQUduQixxQkFBZ0IsR0FBRyxFQUFFLENBQUM7UUFDdEIsbUJBQWMsR0FBRyxFQUFFLENBQUM7SUFFWCxDQUFDO0lBekJqQixzQkFDSSw2REFBaUI7Ozs7UUFXckI7WUFDRSxPQUFPLElBQUksQ0FBQyxRQUFRLENBQUM7UUFDdkIsQ0FBQzs7Ozs7UUFkRCxVQUNzQixPQUFjO1lBQ2xDLElBQUksQ0FBQyxPQUFPLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxFQUFFO2dCQUMvQixJQUFJLENBQUMsY0FBYyxHQUFHLEVBQUUsQ0FBQztnQkFDekIsSUFBSSxDQUFDLGdCQUFnQixHQUFHLEVBQUUsQ0FBQztnQkFDM0IsSUFBSSxDQUFDLE1BQU0sR0FBRyxFQUFFLENBQUM7Z0JBQ2pCLE9BQU87YUFDUjtZQUNELElBQUksQ0FBQyxRQUFRLEdBQUcsT0FBTyxDQUFDO1lBQ3hCLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztRQUN4QixDQUFDOzs7T0FBQTs7Ozs7SUFpQk8sdURBQWM7Ozs7SUFBdEI7UUFBQSxpQkEwQ0M7UUF6Q0MsSUFBSSxJQUFJLENBQUMsaUJBQWlCLEtBQUssSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQyxFQUFFO1lBQ2pFLE9BQU87U0FDUjtRQUVELElBQUksQ0FBQyxNQUFNLEdBQUcsRUFBRSxDQUFDOzs7O1lBR1gsZUFBZSxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxNQUFNOzs7O1FBQUMsVUFBQSxNQUFNLElBQUksT0FBQSxDQUFDLE1BQU0sQ0FBQyxJQUFJLEtBQUssVUFBVSxJQUFJLE1BQU0sQ0FBQyxJQUFJLEtBQUssTUFBTSxJQUFJLE1BQU0sQ0FBQyxJQUFJLEtBQUsseUJBQXlCLENBQUMsRUFBbkcsQ0FBbUcsRUFBQztnQ0FDM0osQ0FBQztZQUNSLElBQUksZUFBZSxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxLQUFLLEtBQUssZUFBZSxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsS0FBSyxFQUFFOztvQkFDN0QsZ0JBQWdCLEdBQUcsT0FBSyxNQUFNLENBQUMsSUFBSTs7OztnQkFBQyxVQUFBLEtBQUssSUFBSSxPQUFBLGVBQWUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsS0FBSyxLQUFLLEtBQUssQ0FBQyxFQUFFLENBQUMsS0FBSyxFQUE5QyxDQUE4QyxFQUFDO2dCQUVsRyxJQUFJLGdCQUFnQixFQUFFO29CQUNwQixnQkFBZ0IsQ0FBQyxPQUFPLEVBQUUsQ0FBQztpQkFDNUI7cUJBQU07b0JBQ0wsT0FBSyxNQUFNLENBQUMsSUFBSSxDQUFDLEVBQUUsRUFBRSxFQUFFLGVBQWUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLEVBQUUsT0FBTyxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUM7aUJBQzdEO2FBQ0Y7OztRQVRILEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxlQUFlLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRTtvQkFBdEMsQ0FBQztTQVVUO1FBRUQsSUFBSSxDQUFDLGdCQUFnQixHQUFHLEVBQUUsQ0FBQztRQUMzQixJQUFJLENBQUMsY0FBYyxHQUFHLEVBQUUsQ0FBQztRQUN6QixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsZUFBZSxDQUFDLE1BQU07Ozs7O1FBQUMsVUFBQyxHQUFHLEVBQUUsTUFBTTtZQUN6RCxJQUFJLEdBQUcsQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLENBQUMsRUFBRTtnQkFDN0IsR0FBRyxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsQ0FBQyxFQUFFLENBQUM7YUFDN0I7aUJBQU0sSUFBSSxNQUFNLENBQUMsWUFBWSxDQUFDLEVBQUU7Z0JBQy9CLEdBQUcsQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUM7YUFDL0I7WUFDRCxPQUFPLEdBQUcsQ0FBQztRQUNiLENBQUMsR0FBRSxFQUFFLENBQUMsQ0FBQztRQUVQLElBQUksQ0FBQyxjQUFjLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxNQUFNOzs7O1FBQUMsVUFBQSxHQUFHO1lBQ2pFLElBQUksS0FBSSxDQUFDLGdCQUFnQixDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsRUFBRTtnQkFDbEMsT0FBTyxHQUFHLENBQUM7YUFDWjtRQUNILENBQUMsRUFBQyxDQUFDOztZQUVDLFNBQVMsR0FBRyxDQUFDO1FBQ2pCLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEVBQUU7WUFBRSxTQUFTLEVBQUUsQ0FBQztTQUFFO1FBQ3hDLElBQUksSUFBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDLGVBQWUsRUFBRTtZQUFFLFNBQVMsRUFBRSxDQUFDO1NBQUU7UUFDeEUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7SUFDckMsQ0FBQzs7Z0JBNUVGLFNBQVMsU0FBQztvQkFDVCxRQUFRLEVBQUUsNkJBQTZCO29CQUN2Qyw4d0JBQXVEOztpQkFFeEQ7Ozs7O2tDQUVFLEtBQUs7b0NBQ0wsS0FBSyxTQUFDLG1CQUFtQjtnQ0FnQnpCLE1BQU07O0lBc0RULHFDQUFDO0NBQUEsQUE3RUQsSUE2RUM7U0F4RVksOEJBQThCOzs7SUFDekMseURBQWlDOztJQWlCakMsdURBQXFEOzs7OztJQUVyRCxrREFBc0I7O0lBQ3RCLGdEQUEwQjs7SUFDMUIsaURBQXVCOztJQUV2QiwwREFBNkI7O0lBQzdCLHdEQUEyQiIsInNvdXJjZXNDb250ZW50IjpbIi8vIFRvZG86IGNoZWNrIGlmIHRoaXMgY29tcG9uZW50IGlzIHJlcXVpcmVkIGluIHNoYXJlZCBsaWJyYXJ5XG5cbmltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBPbkRlc3Ryb3ksIElucHV0LCBPdXRwdXQsIEV2ZW50RW1pdHRlciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuLy8gVG9kbzogSW1wbGVtZW50IHBpcGVzIG9uY2UgcGlwZXMgYXJlIGNvbnZlcnRlZCBpbnRvIHNoYXJlZCBsaWIgYW5kIHVuY29tbWVudCB0aGUgY29kZVxuLy8gaW1wb3J0IHsgc2hvcnRoYW5kVW5pdHNPYmplY3QgfSBmcm9tICcuLi8uLi9oZWxwZXJzL3RyYW5zbGF0ZS1kaXN0YW5jZSc7XG5pbXBvcnQgeyBTdWJzY3JpcHRpb24gfSBmcm9tICdyeGpzJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnYXBwLWRlZmluaXRpdmUtdGFibGUtZXJyb3JzJyxcbiAgdGVtcGxhdGVVcmw6ICcuL2RlZmluaXRpdmUtdGFibGUtZXJyb3JzLmNvbXBvbmVudC5odG1sJyxcbiAgc3R5bGVVcmxzOiBbJy4vZGVmaW5pdGl2ZS10YWJsZS1lcnJvcnMuY29tcG9uZW50LnNjc3MnXVxufSlcbmV4cG9ydCBjbGFzcyBEZWZpbml0aXZlVGFibGVFcnJvcnNDb21wb25lbnQge1xuICBASW5wdXQoKXNob3dUcmFja2luZ0lkczogYm9vbGVhbjtcbiAgQElucHV0KCdkZWZpbml0aXZlU3VydmV5cycpXG4gIHNldCBkZWZpbml0aXZlU3VydmV5cyhzdXJ2ZXlzOiBhbnlbXSkge1xuICAgIGlmICghc3VydmV5cyB8fCAhc3VydmV5cy5sZW5ndGgpIHtcbiAgICAgIHRoaXMudHJhY2tpbmdJZEtleXMgPSBbXTtcbiAgICAgIHRoaXMudHJhY2tpbmdJZEVycm9ycyA9IHt9O1xuICAgICAgdGhpcy5lcnJvcnMgPSBbXTtcbiAgICAgIHJldHVybjtcbiAgICB9XG4gICAgdGhpcy5fc3VydmV5cyA9IHN1cnZleXM7XG4gICAgdGhpcy5ldmFsdWF0ZUVycm9ycygpO1xuICB9XG5cbiAgZ2V0IGRlZmluaXRpdmVTdXJ2ZXlzKCk6IGFueVtdIHtcbiAgICByZXR1cm4gdGhpcy5fc3VydmV5cztcbiAgfVxuXG4gIEBPdXRwdXQoKSBjb25mbGljdExpbmVzID0gbmV3IEV2ZW50RW1pdHRlcjxudW1iZXI+KCk7XG5cbiAgcHJpdmF0ZSBfc3VydmV5cyA9IFtdO1xuICBwdWJsaWMgZXJyb3JzOiBhbnlbXSA9IFtdO1xuICBwdWJsaWMgbWRVbml0czogc3RyaW5nO1xuXG4gIHB1YmxpYyB0cmFja2luZ0lkRXJyb3JzID0ge307XG4gIHB1YmxpYyB0cmFja2luZ0lkS2V5cyA9IFtdO1xuXG4gIGNvbnN0cnVjdG9yKCkgeyB9XG5cbiAgcHJpdmF0ZSBldmFsdWF0ZUVycm9ycygpOiB2b2lkIHtcbiAgICBpZiAodGhpcy5kZWZpbml0aXZlU3VydmV5cyA9PT0gbnVsbCB8fCAhdGhpcy5kZWZpbml0aXZlU3VydmV5c1swXSkge1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIHRoaXMuZXJyb3JzID0gW107XG4gICAgLy8gVG9kbzogdW5jb21tZW50IG9uY2UgdGhlIHBpcGVzIGFyZSBpbXBsZW1lbnRlZFxuICAgIC8vIHRoaXMubWRVbml0cyA9IHNob3J0aGFuZFVuaXRzT2JqZWN0W3RoaXMuZGVmaW5pdGl2ZVN1cnZleXNbMF0ubWQudW5pdF07XG4gICAgY29uc3Qgc3RhbmRhcmRTdXJ2ZXlzID0gdGhpcy5kZWZpbml0aXZlU3VydmV5cy5maWx0ZXIoc3VydmV5ID0+IChzdXJ2ZXkudHlwZSA9PT0gJ1NUQU5EQVJEJyB8fCBzdXJ2ZXkudHlwZSA9PT0gJ1BPT1InIHx8IHN1cnZleS50eXBlID09PSAnREVGSU5JVElWRV9JTlRFUlBPTEFURUQnKSk7XG4gICAgZm9yIChsZXQgaSA9IDE7IGkgPCBzdGFuZGFyZFN1cnZleXMubGVuZ3RoOyBpKyspIHtcbiAgICAgIGlmIChzdGFuZGFyZFN1cnZleXNbaV0ubWQudmFsdWUgPT09IHN0YW5kYXJkU3VydmV5c1tpIC0gMV0ubWQudmFsdWUpIHtcbiAgICAgICAgY29uc3QgZHVwbGljYXRlZFN1cnZleSA9IHRoaXMuZXJyb3JzLmZpbmQoZXJyb3IgPT4gc3RhbmRhcmRTdXJ2ZXlzW2ldLm1kLnZhbHVlID09PSBlcnJvci5tZC52YWx1ZSk7XG5cbiAgICAgICAgaWYgKGR1cGxpY2F0ZWRTdXJ2ZXkpIHtcbiAgICAgICAgICBkdXBsaWNhdGVkU3VydmV5LmNvdW50ZXIrKztcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICB0aGlzLmVycm9ycy5wdXNoKHsgbWQ6IHN0YW5kYXJkU3VydmV5c1tpXS5tZCwgY291bnRlcjogMiB9KTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cblxuICAgIHRoaXMudHJhY2tpbmdJZEVycm9ycyA9IHt9O1xuICAgIHRoaXMudHJhY2tpbmdJZEtleXMgPSBbXTtcbiAgICB0aGlzLnRyYWNraW5nSWRFcnJvcnMgPSBzdGFuZGFyZFN1cnZleXMucmVkdWNlKChhY2MsIHN1cnZleSkgPT4ge1xuICAgICAgaWYgKGFjY1tzdXJ2ZXlbJ3RyYWNraW5nSWQnXV0pIHtcbiAgICAgICAgYWNjW3N1cnZleVsndHJhY2tpbmdJZCddXSsrO1xuICAgICAgfSBlbHNlIGlmIChzdXJ2ZXlbJ3RyYWNraW5nSWQnXSkge1xuICAgICAgICBhY2Nbc3VydmV5Wyd0cmFja2luZ0lkJ11dID0gMTtcbiAgICAgIH1cbiAgICAgIHJldHVybiBhY2M7XG4gICAgfSwge30pO1xuXG4gICAgdGhpcy50cmFja2luZ0lkS2V5cyA9IE9iamVjdC5rZXlzKHRoaXMudHJhY2tpbmdJZEVycm9ycykuZmlsdGVyKGtleSA9PiB7XG4gICAgICBpZiAodGhpcy50cmFja2luZ0lkRXJyb3JzW2tleV0gPiAxKSB7XG4gICAgICAgIHJldHVybiBrZXk7XG4gICAgICB9XG4gICAgfSk7XG5cbiAgICBsZXQgc3BhY2VVc2VkID0gMDtcbiAgICBpZiAodGhpcy5lcnJvcnMubGVuZ3RoKSB7IHNwYWNlVXNlZCsrOyB9XG4gICAgaWYgKHRoaXMudHJhY2tpbmdJZEtleXMubGVuZ3RoICYmIHRoaXMuc2hvd1RyYWNraW5nSWRzKSB7IHNwYWNlVXNlZCsrOyB9XG4gICAgdGhpcy5jb25mbGljdExpbmVzLmVtaXQoc3BhY2VVc2VkKTtcbiAgfVxufVxuIl19