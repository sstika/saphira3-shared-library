/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Pipe } from '@angular/core';
var MagneticUnitPipe = /** @class */ (function () {
    function MagneticUnitPipe() {
    }
    /**
     * @param {?} unit
     * @param {?=} full
     * @return {?}
     */
    MagneticUnitPipe.prototype.transform = /**
     * @param {?} unit
     * @param {?=} full
     * @return {?}
     */
    function (unit, full) {
        /** @type {?} */
        var string = '';
        switch (unit) {
            case 'NANOTESLA':
                string = (full === true ? 'Nanotesla' : 'nT');
                break;
            case 'MICROTESLA':
                string = (full === true ? 'Microtesla' : decodeURI('%C2%B5') + 'T');
                break;
            case 'MILLITESLA':
                string = (full === true ? 'Millitesla' : 'mT');
                break;
            case 'TESLA':
                string = (full === true ? 'Tesla' : 'T');
                break;
            case 'GAUSS':
                string = (full === true ? 'Gauss' : 'Gs');
                break;
            case 'GAMMA':
                string = (full === true ? 'Gamma' : decodeURI('%CE%B3'));
                break;
            default:
                string = unit;
                break;
        }
        return string;
    };
    MagneticUnitPipe.decorators = [
        { type: Pipe, args: [{
                    name: 'magneticUnit'
                },] }
    ];
    return MagneticUnitPipe;
}());
export { MagneticUnitPipe };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFnbmV0aWMtdW5pdC5waXBlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGhwdGVjaC9zYXBoaXJhLXNoYXJlZC1saWIvIiwic291cmNlcyI6WyJsaWIvcGlwZXMvbWFnbmV0aWMtdW5pdC5waXBlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsSUFBSSxFQUFpQixNQUFNLGVBQWUsQ0FBQztBQUVwRDtJQUFBO0lBaUNBLENBQUM7Ozs7OztJQTVCQyxvQ0FBUzs7Ozs7SUFBVCxVQUFVLElBQVMsRUFBRSxJQUFVOztZQUN6QixNQUFNLEdBQUcsRUFBRTtRQUNmLFFBQVEsSUFBSSxFQUFFO1lBQ1osS0FBSyxXQUFXO2dCQUNkLE1BQU0sR0FBRyxDQUFDLElBQUksS0FBSyxJQUFJLENBQUMsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQzlDLE1BQU07WUFDUixLQUFLLFlBQVk7Z0JBQ2YsTUFBTSxHQUFHLENBQUMsSUFBSSxLQUFLLElBQUksQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLEdBQUcsR0FBRyxDQUFDLENBQUM7Z0JBQ3BFLE1BQU07WUFDUixLQUFLLFlBQVk7Z0JBQ2YsTUFBTSxHQUFHLENBQUMsSUFBSSxLQUFLLElBQUksQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDL0MsTUFBTTtZQUNSLEtBQUssT0FBTztnQkFDVixNQUFNLEdBQUcsQ0FBQyxJQUFJLEtBQUssSUFBSSxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDO2dCQUN6QyxNQUFNO1lBQ1IsS0FBSyxPQUFPO2dCQUNWLE1BQU0sR0FBRyxDQUFDLElBQUksS0FBSyxJQUFJLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQzFDLE1BQU07WUFDUixLQUFLLE9BQU87Z0JBQ1YsTUFBTSxHQUFHLENBQUMsSUFBSSxLQUFLLElBQUksQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztnQkFDekQsTUFBTTtZQUNSO2dCQUNFLE1BQU0sR0FBRyxJQUFJLENBQUM7Z0JBQ2QsTUFBTTtTQUNUO1FBQ0QsT0FBTyxNQUFNLENBQUM7SUFDaEIsQ0FBQzs7Z0JBL0JGLElBQUksU0FBQztvQkFDSixJQUFJLEVBQUUsY0FBYztpQkFDckI7O0lBK0JELHVCQUFDO0NBQUEsQUFqQ0QsSUFpQ0M7U0E5QlksZ0JBQWdCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgUGlwZSwgUGlwZVRyYW5zZm9ybSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5AUGlwZSh7XG4gIG5hbWU6ICdtYWduZXRpY1VuaXQnXG59KVxuZXhwb3J0IGNsYXNzIE1hZ25ldGljVW5pdFBpcGUgaW1wbGVtZW50cyBQaXBlVHJhbnNmb3JtIHtcblxuICB0cmFuc2Zvcm0odW5pdDogYW55LCBmdWxsPzogYW55KTogYW55IHtcbiAgICBsZXQgc3RyaW5nID0gJyc7XG4gICAgc3dpdGNoICh1bml0KSB7XG4gICAgICBjYXNlICdOQU5PVEVTTEEnOlxuICAgICAgICBzdHJpbmcgPSAoZnVsbCA9PT0gdHJ1ZSA/ICdOYW5vdGVzbGEnIDogJ25UJyk7XG4gICAgICAgIGJyZWFrO1xuICAgICAgY2FzZSAnTUlDUk9URVNMQSc6XG4gICAgICAgIHN0cmluZyA9IChmdWxsID09PSB0cnVlID8gJ01pY3JvdGVzbGEnIDogZGVjb2RlVVJJKCclQzIlQjUnKSArICdUJyk7XG4gICAgICAgIGJyZWFrO1xuICAgICAgY2FzZSAnTUlMTElURVNMQSc6XG4gICAgICAgIHN0cmluZyA9IChmdWxsID09PSB0cnVlID8gJ01pbGxpdGVzbGEnIDogJ21UJyk7XG4gICAgICAgIGJyZWFrO1xuICAgICAgY2FzZSAnVEVTTEEnOlxuICAgICAgICBzdHJpbmcgPSAoZnVsbCA9PT0gdHJ1ZSA/ICdUZXNsYScgOiAnVCcpO1xuICAgICAgICBicmVhaztcbiAgICAgIGNhc2UgJ0dBVVNTJzpcbiAgICAgICAgc3RyaW5nID0gKGZ1bGwgPT09IHRydWUgPyAnR2F1c3MnIDogJ0dzJyk7XG4gICAgICAgIGJyZWFrO1xuICAgICAgY2FzZSAnR0FNTUEnOlxuICAgICAgICBzdHJpbmcgPSAoZnVsbCA9PT0gdHJ1ZSA/ICdHYW1tYScgOiBkZWNvZGVVUkkoJyVDRSVCMycpKTtcbiAgICAgICAgYnJlYWs7XG4gICAgICBkZWZhdWx0OlxuICAgICAgICBzdHJpbmcgPSB1bml0O1xuICAgICAgICBicmVhaztcbiAgICB9XG4gICAgcmV0dXJuIHN0cmluZztcbiAgfVxuXG59XG4iXX0=