/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Pipe } from '@angular/core';
import * as moment_ from 'moment';
var TimeAgoPipe = /** @class */ (function () {
    function TimeAgoPipe() {
    }
    /**
     * @param {?} time
     * @return {?}
     */
    TimeAgoPipe.prototype.transform = /**
     * @param {?} time
     * @return {?}
     */
    function (time) {
        /** @type {?} */
        var moment = moment_;
        /** @type {?} */
        var now = moment();
        if (time && moment.isMoment(time)) {
            if (time > now) {
                return 'Now';
            }
            else {
                return time.fromNow();
            }
        }
        else {
            return 'Never';
        }
    };
    TimeAgoPipe.decorators = [
        { type: Pipe, args: [{
                    name: 'timeAgo'
                },] }
    ];
    return TimeAgoPipe;
}());
export { TimeAgoPipe };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGltZS1hZ28ucGlwZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BocHRlY2gvc2FwaGlyYS1zaGFyZWQtbGliLyIsInNvdXJjZXMiOlsibGliL3BpcGVzL3RpbWUtYWdvLnBpcGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxJQUFJLEVBQWlCLE1BQU0sZUFBZSxDQUFDO0FBQ3BELE9BQU8sS0FBSyxPQUFPLE1BQU0sUUFBUSxDQUFDO0FBRWxDO0lBQUE7SUFxQkEsQ0FBQzs7Ozs7SUFkQywrQkFBUzs7OztJQUFULFVBQVUsSUFBSTs7WUFDTixNQUFNLEdBQUcsT0FBTzs7WUFDaEIsR0FBRyxHQUFHLE1BQU0sRUFBRTtRQUNwQixJQUFJLElBQUksSUFBSSxNQUFNLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxFQUFFO1lBQ2pDLElBQUksSUFBSSxHQUFHLEdBQUcsRUFBRTtnQkFDZCxPQUFPLEtBQUssQ0FBQzthQUNkO2lCQUFNO2dCQUNMLE9BQU8sSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDO2FBQ3ZCO1NBQ0Y7YUFBTTtZQUNMLE9BQU8sT0FBTyxDQUFDO1NBQ2hCO0lBQ0gsQ0FBQzs7Z0JBbkJGLElBQUksU0FBQztvQkFDSixJQUFJLEVBQUUsU0FBUztpQkFDaEI7O0lBbUJELGtCQUFDO0NBQUEsQUFyQkQsSUFxQkM7U0FoQlksV0FBVyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFBpcGUsIFBpcGVUcmFuc2Zvcm0gfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCAqIGFzIG1vbWVudF8gZnJvbSAnbW9tZW50JztcblxuQFBpcGUoe1xuICBuYW1lOiAndGltZUFnbydcbn0pXG4vLyBBIGZpbHRlciB0byBjb252ZXJ0IGEgbW9tZW50IHRvIGEgaHVtYW4gcmVhZGFibGUgc3RyaW5nXG4vLyBvZiB0aGUgZm9ybWF0IFwiWCBzZWNvbmRzIGFnb1wiLCBcIlkgbWludXRlcyBhZ29cIiwgXCI0IG1vbnRocyBhZ29cIlxuZXhwb3J0IGNsYXNzIFRpbWVBZ29QaXBlIGltcGxlbWVudHMgUGlwZVRyYW5zZm9ybSB7XG5cbiAgdHJhbnNmb3JtKHRpbWUpOiBhbnkge1xuICAgIGNvbnN0IG1vbWVudCA9IG1vbWVudF87XG4gICAgY29uc3Qgbm93ID0gbW9tZW50KCk7XG4gICAgaWYgKHRpbWUgJiYgbW9tZW50LmlzTW9tZW50KHRpbWUpKSB7XG4gICAgICBpZiAodGltZSA+IG5vdykge1xuICAgICAgICByZXR1cm4gJ05vdyc7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICByZXR1cm4gdGltZS5mcm9tTm93KCk7XG4gICAgICB9XG4gICAgfSBlbHNlIHtcbiAgICAgIHJldHVybiAnTmV2ZXInO1xuICAgIH1cbiAgfVxuXG59XG4iXX0=