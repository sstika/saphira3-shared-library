/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Pipe } from '@angular/core';
var AngleDecimalPipe = /** @class */ (function () {
    function AngleDecimalPipe() {
        // number of decimal places to display for various angle units
        this.decimalMap = {
            DEGREE: 2,
            RADIAN: 2,
            GRADIAN: 2,
            Degrees: 2,
            '°': 2,
            Radians: 2,
            rad: 2,
            Gradians: 2,
            grad: 2
        };
    }
    /**
     * @param {?} value
     * @param {?=} unit
     * @return {?}
     */
    AngleDecimalPipe.prototype.transform = /**
     * @param {?} value
     * @param {?=} unit
     * @return {?}
     */
    function (value, unit) {
        if (!this.isValidFloat(value)) {
            return 'N/A';
        }
        else {
            value = parseFloat(value);
            if (typeof (this.decimalMap[unit]) !== 'undefined') {
                return this.formatNumber(value.toFixed(this.decimalMap[unit]));
            }
            else {
                return 'Unsupported Unit: ' + unit;
            }
        }
    };
    /**
     * isValidFloat:
     *
     * A stricter float filter than parseFloat().
     *
     * @param value Input value to be parsed as float
     * @returns float
     */
    /**
     * isValidFloat:
     *
     * A stricter float filter than parseFloat().
     *
     * @param {?} value Input value to be parsed as float
     * @return {?} float
     */
    AngleDecimalPipe.prototype.isValidFloat = /**
     * isValidFloat:
     *
     * A stricter float filter than parseFloat().
     *
     * @param {?} value Input value to be parsed as float
     * @return {?} float
     */
    function (value) {
        return !isNaN(parseFloat(value));
    };
    /**
      * formatNumber:
      *
      * Adds commas and removes the negative sign for zero values.
      *
      * @param number Input value to be formatted
      */
    /**
     * formatNumber:
     *
     * Adds commas and removes the negative sign for zero values.
     *
     * @param {?} number Input value to be formatted
     * @return {?}
     */
    AngleDecimalPipe.prototype.formatNumber = /**
     * formatNumber:
     *
     * Adds commas and removes the negative sign for zero values.
     *
     * @param {?} number Input value to be formatted
     * @return {?}
     */
    function (number) {
        return this.removeSignOnZero(this.addCommas(number));
    };
    /**
       * removeSignOnZero:
       *
       * If the value is zero, remove any negative signs. (converts -0.00 to 0.00)
       *
       * @param number Input value to be sanitized
       */
    /**
     * removeSignOnZero:
     *
     * If the value is zero, remove any negative signs. (converts -0.00 to 0.00)
     *
     * @param {?} number Input value to be sanitized
     * @return {?}
     */
    AngleDecimalPipe.prototype.removeSignOnZero = /**
     * removeSignOnZero:
     *
     * If the value is zero, remove any negative signs. (converts -0.00 to 0.00)
     *
     * @param {?} number Input value to be sanitized
     * @return {?}
     */
    function (number) {
        /** @type {?} */
        var split = number.toString().split('-');
        if (split.length === 2) {
            if (split[1] === 0) {
                return split[1];
            }
        }
        return number;
    };
    /**
       * addCommas:
       *
       * Helper function that adds in commas to a number for readability.
       *
       * @param number Input value to add commas
       * @returns string
       */
    /**
     * addCommas:
     *
     * Helper function that adds in commas to a number for readability.
     *
     * @param {?} number Input value to add commas
     * @return {?} string
     */
    AngleDecimalPipe.prototype.addCommas = /**
     * addCommas:
     *
     * Helper function that adds in commas to a number for readability.
     *
     * @param {?} number Input value to add commas
     * @return {?} string
     */
    function (number) {
        // check if we have a decimal
        /** @type {?} */
        var split = number.toString().split('.');
        // if we have a decimal
        if (split.length === 2) {
            // only add commas to the first half and return the second half
            return split[0].replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,') + '.' + split[1];
        }
        else {
            // add commas throughout
            return split[0].replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
        }
    };
    AngleDecimalPipe.decorators = [
        { type: Pipe, args: [{
                    name: 'angleDecimal'
                },] }
    ];
    return AngleDecimalPipe;
}());
export { AngleDecimalPipe };
if (false) {
    /** @type {?} */
    AngleDecimalPipe.prototype.decimalMap;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYW5nbGUtZGVjaW1hbC5waXBlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGhwdGVjaC9zYXBoaXJhLXNoYXJlZC1saWIvIiwic291cmNlcyI6WyJsaWIvcGlwZXMvYW5nbGUtZGVjaW1hbC5waXBlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsSUFBSSxFQUFpQixNQUFNLGVBQWUsQ0FBQztBQUVwRDtJQUFBOztRQU9FLGVBQVUsR0FBRztZQUNYLE1BQU0sRUFBRSxDQUFDO1lBQ1QsTUFBTSxFQUFFLENBQUM7WUFDVCxPQUFPLEVBQUUsQ0FBQztZQUNWLE9BQU8sRUFBRSxDQUFDO1lBQ1YsR0FBRyxFQUFFLENBQUM7WUFDTixPQUFPLEVBQUUsQ0FBQztZQUNWLEdBQUcsRUFBRSxDQUFDO1lBQ04sUUFBUSxFQUFFLENBQUM7WUFDWCxJQUFJLEVBQUUsQ0FBQztTQUNSLENBQUM7SUEyRUosQ0FBQzs7Ozs7O0lBekVDLG9DQUFTOzs7OztJQUFULFVBQVUsS0FBVSxFQUFFLElBQVU7UUFDOUIsSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLEVBQUU7WUFDN0IsT0FBTyxLQUFLLENBQUM7U0FDZDthQUFNO1lBQ0wsS0FBSyxHQUFHLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUMzQixJQUFJLE9BQU8sQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDLEtBQUssV0FBVyxFQUFFO2dCQUNqRCxPQUFPLElBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQzthQUNoRTtpQkFBTTtnQkFDTCxPQUFPLG9CQUFvQixHQUFHLElBQUksQ0FBQzthQUNwQztTQUNGO0lBQ0gsQ0FBQztJQUVEOzs7Ozs7O09BT0c7Ozs7Ozs7OztJQUNJLHVDQUFZOzs7Ozs7OztJQUFuQixVQUFvQixLQUFLO1FBQ3ZCLE9BQU8sQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7SUFDbkMsQ0FBQztJQUVEOzs7Ozs7UUFNSTs7Ozs7Ozs7O0lBQ0csdUNBQVk7Ozs7Ozs7O0lBQW5CLFVBQW9CLE1BQU07UUFDeEIsT0FBTyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO0lBQ3ZELENBQUM7SUFFRDs7Ozs7O1NBTUs7Ozs7Ozs7OztJQUNFLDJDQUFnQjs7Ozs7Ozs7SUFBdkIsVUFBd0IsTUFBTTs7WUFDdEIsS0FBSyxHQUFHLE1BQU0sQ0FBQyxRQUFRLEVBQUUsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDO1FBQzFDLElBQUksS0FBSyxDQUFDLE1BQU0sS0FBSyxDQUFDLEVBQUU7WUFDdEIsSUFBSSxLQUFLLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxFQUFFO2dCQUNsQixPQUFPLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQzthQUNqQjtTQUNGO1FBQ0QsT0FBTyxNQUFNLENBQUM7SUFDaEIsQ0FBQztJQUVEOzs7Ozs7O1NBT0s7Ozs7Ozs7OztJQUNFLG9DQUFTOzs7Ozs7OztJQUFoQixVQUFpQixNQUFNOzs7WUFFZixLQUFLLEdBQUcsTUFBTSxDQUFDLFFBQVEsRUFBRSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUM7UUFDMUMsdUJBQXVCO1FBQ3ZCLElBQUksS0FBSyxDQUFDLE1BQU0sS0FBSyxDQUFDLEVBQUU7WUFDdEIsK0RBQStEO1lBQy9ELE9BQU8sS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyx5QkFBeUIsRUFBRSxLQUFLLENBQUMsR0FBRyxHQUFHLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO1NBQzVFO2FBQU07WUFDTCx3QkFBd0I7WUFDeEIsT0FBTyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLHlCQUF5QixFQUFFLEtBQUssQ0FBQyxDQUFDO1NBQzNEO0lBQ0gsQ0FBQzs7Z0JBM0ZGLElBQUksU0FBQztvQkFDSixJQUFJLEVBQUUsY0FBYztpQkFDckI7O0lBMEZELHVCQUFDO0NBQUEsQUE1RkQsSUE0RkM7U0F2RlksZ0JBQWdCOzs7SUFFM0Isc0NBVUUiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBQaXBlLCBQaXBlVHJhbnNmb3JtIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBQaXBlKHtcbiAgbmFtZTogJ2FuZ2xlRGVjaW1hbCdcbn0pXG4vLyBSZXR1cm5zIHRoZSBhbmdsZSBhcyBhIHN0cmluZyB3aXRoIGFuIGFtb3VudCBvZiBkZWNpbWFsIHBsYWNlcyBiYXNlZCBvbiBpdHMgdW5pdC5cbi8vIEFsc28gZm9ybWF0cyB0aGUgc3RyaW5nIHdpdGggY29tbWFzIGZvciByZWFkYWJpbGl0eS5cbmV4cG9ydCBjbGFzcyBBbmdsZURlY2ltYWxQaXBlIGltcGxlbWVudHMgUGlwZVRyYW5zZm9ybSB7XG4gIC8vIG51bWJlciBvZiBkZWNpbWFsIHBsYWNlcyB0byBkaXNwbGF5IGZvciB2YXJpb3VzIGFuZ2xlIHVuaXRzXG4gIGRlY2ltYWxNYXAgPSB7XG4gICAgREVHUkVFOiAyLFxuICAgIFJBRElBTjogMixcbiAgICBHUkFESUFOOiAyLFxuICAgIERlZ3JlZXM6IDIsXG4gICAgJ8KwJzogMixcbiAgICBSYWRpYW5zOiAyLFxuICAgIHJhZDogMixcbiAgICBHcmFkaWFuczogMixcbiAgICBncmFkOiAyXG4gIH07XG5cbiAgdHJhbnNmb3JtKHZhbHVlOiBhbnksIHVuaXQ/OiBhbnkpOiBhbnkge1xuICAgIGlmICghdGhpcy5pc1ZhbGlkRmxvYXQodmFsdWUpKSB7XG4gICAgICByZXR1cm4gJ04vQSc7XG4gICAgfSBlbHNlIHtcbiAgICAgIHZhbHVlID0gcGFyc2VGbG9hdCh2YWx1ZSk7XG4gICAgIGlmICh0eXBlb2YgKHRoaXMuZGVjaW1hbE1hcFt1bml0XSkgIT09ICd1bmRlZmluZWQnKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmZvcm1hdE51bWJlcih2YWx1ZS50b0ZpeGVkKHRoaXMuZGVjaW1hbE1hcFt1bml0XSkpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgcmV0dXJuICdVbnN1cHBvcnRlZCBVbml0OiAnICsgdW5pdDtcbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICAvKipcbiAgICogaXNWYWxpZEZsb2F0OlxuICAgKlxuICAgKiBBIHN0cmljdGVyIGZsb2F0IGZpbHRlciB0aGFuIHBhcnNlRmxvYXQoKS5cbiAgICpcbiAgICogQHBhcmFtIHZhbHVlIElucHV0IHZhbHVlIHRvIGJlIHBhcnNlZCBhcyBmbG9hdFxuICAgKiBAcmV0dXJucyBmbG9hdFxuICAgKi9cbiAgcHVibGljIGlzVmFsaWRGbG9hdCh2YWx1ZSkge1xuICAgIHJldHVybiAhaXNOYU4ocGFyc2VGbG9hdCh2YWx1ZSkpO1xuICB9XG5cbiAgLyoqXG4gICAgKiBmb3JtYXROdW1iZXI6XG4gICAgKlxuICAgICogQWRkcyBjb21tYXMgYW5kIHJlbW92ZXMgdGhlIG5lZ2F0aXZlIHNpZ24gZm9yIHplcm8gdmFsdWVzLlxuICAgICpcbiAgICAqIEBwYXJhbSBudW1iZXIgSW5wdXQgdmFsdWUgdG8gYmUgZm9ybWF0dGVkXG4gICAgKi9cbiAgcHVibGljIGZvcm1hdE51bWJlcihudW1iZXIpIHtcbiAgICByZXR1cm4gdGhpcy5yZW1vdmVTaWduT25aZXJvKHRoaXMuYWRkQ29tbWFzKG51bWJlcikpO1xuICB9XG5cbiAgLyoqXG4gICAgICogcmVtb3ZlU2lnbk9uWmVybzpcbiAgICAgKlxuICAgICAqIElmIHRoZSB2YWx1ZSBpcyB6ZXJvLCByZW1vdmUgYW55IG5lZ2F0aXZlIHNpZ25zLiAoY29udmVydHMgLTAuMDAgdG8gMC4wMClcbiAgICAgKlxuICAgICAqIEBwYXJhbSBudW1iZXIgSW5wdXQgdmFsdWUgdG8gYmUgc2FuaXRpemVkXG4gICAgICovXG4gIHB1YmxpYyByZW1vdmVTaWduT25aZXJvKG51bWJlcikge1xuICAgIGNvbnN0IHNwbGl0ID0gbnVtYmVyLnRvU3RyaW5nKCkuc3BsaXQoJy0nKTtcbiAgICBpZiAoc3BsaXQubGVuZ3RoID09PSAyKSB7XG4gICAgICBpZiAoc3BsaXRbMV0gPT09IDApIHtcbiAgICAgICAgcmV0dXJuIHNwbGl0WzFdO1xuICAgICAgfVxuICAgIH1cbiAgICByZXR1cm4gbnVtYmVyO1xuICB9XG5cbiAgLyoqXG4gICAgICogYWRkQ29tbWFzOlxuICAgICAqXG4gICAgICogSGVscGVyIGZ1bmN0aW9uIHRoYXQgYWRkcyBpbiBjb21tYXMgdG8gYSBudW1iZXIgZm9yIHJlYWRhYmlsaXR5LlxuICAgICAqXG4gICAgICogQHBhcmFtIG51bWJlciBJbnB1dCB2YWx1ZSB0byBhZGQgY29tbWFzXG4gICAgICogQHJldHVybnMgc3RyaW5nXG4gICAgICovXG4gIHB1YmxpYyBhZGRDb21tYXMobnVtYmVyKSB7XG4gICAgLy8gY2hlY2sgaWYgd2UgaGF2ZSBhIGRlY2ltYWxcbiAgICBjb25zdCBzcGxpdCA9IG51bWJlci50b1N0cmluZygpLnNwbGl0KCcuJyk7XG4gICAgLy8gaWYgd2UgaGF2ZSBhIGRlY2ltYWxcbiAgICBpZiAoc3BsaXQubGVuZ3RoID09PSAyKSB7XG4gICAgICAvLyBvbmx5IGFkZCBjb21tYXMgdG8gdGhlIGZpcnN0IGhhbGYgYW5kIHJldHVybiB0aGUgc2Vjb25kIGhhbGZcbiAgICAgIHJldHVybiBzcGxpdFswXS5yZXBsYWNlKC8oXFxkKSg/PShcXGR7M30pKyg/IVxcZCkpL2csICckMSwnKSArICcuJyArIHNwbGl0WzFdO1xuICAgIH0gZWxzZSB7XG4gICAgICAvLyBhZGQgY29tbWFzIHRocm91Z2hvdXRcbiAgICAgIHJldHVybiBzcGxpdFswXS5yZXBsYWNlKC8oXFxkKSg/PShcXGR7M30pKyg/IVxcZCkpL2csICckMSwnKTtcbiAgICB9XG4gIH1cbn1cbiJdfQ==