/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Pipe } from '@angular/core';
import { DatePipe } from '@angular/common';
var InstantPipe = /** @class */ (function (_super) {
    tslib_1.__extends(InstantPipe, _super);
    function InstantPipe() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    /**
     * @param {?} dateObj
     * @return {?}
     */
    InstantPipe.prototype.transform = /**
     * @param {?} dateObj
     * @return {?}
     */
    function (dateObj) {
        return (dateObj !== null && dateObj !== undefined) ? (_super.prototype.transform.call(this, dateObj.seconds * 1000, 'medium', 'UTC')) : null;
    };
    InstantPipe.decorators = [
        { type: Pipe, args: [{
                    name: 'instant'
                },] }
    ];
    return InstantPipe;
}(DatePipe));
export { InstantPipe };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5zdGFudC5waXBlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGhwdGVjaC9zYXBoaXJhLXNoYXJlZC1saWIvIiwic291cmNlcyI6WyJsaWIvcGlwZXMvaW5zdGFudC5waXBlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUFFLElBQUksRUFBaUIsTUFBTSxlQUFlLENBQUM7QUFDcEQsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBRTNDO0lBSWlDLHVDQUFRO0lBSnpDOztJQVVBLENBQUM7Ozs7O0lBSkMsK0JBQVM7Ozs7SUFBVCxVQUFVLE9BQVk7UUFDcEIsT0FBTyxDQUFDLE9BQU8sS0FBSyxJQUFJLElBQUksT0FBTyxLQUFLLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLGlCQUFNLFNBQVMsWUFBQyxPQUFPLENBQUMsT0FBTyxHQUFHLElBQUksRUFBRSxRQUFRLEVBQUUsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO0lBQ3pILENBQUM7O2dCQVJGLElBQUksU0FBQztvQkFDSixJQUFJLEVBQUUsU0FBUztpQkFDaEI7O0lBUUQsa0JBQUM7Q0FBQSxBQVZELENBSWlDLFFBQVEsR0FNeEM7U0FOWSxXQUFXIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgUGlwZSwgUGlwZVRyYW5zZm9ybSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgRGF0ZVBpcGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xuXG5AUGlwZSh7XG4gIG5hbWU6ICdpbnN0YW50J1xufSlcbi8vIENvbnZlcnRzIHRoZSBzZWNvbmRzIGludG8gaHVtYW4gcmVhZGFibGUgZGF0ZVxuZXhwb3J0IGNsYXNzIEluc3RhbnRQaXBlIGV4dGVuZHMgRGF0ZVBpcGUgaW1wbGVtZW50cyBQaXBlVHJhbnNmb3JtIHtcblxuICB0cmFuc2Zvcm0oZGF0ZU9iajogYW55KTogYW55IHtcbiAgICByZXR1cm4gKGRhdGVPYmogIT09IG51bGwgJiYgZGF0ZU9iaiAhPT0gdW5kZWZpbmVkKSA/IChzdXBlci50cmFuc2Zvcm0oZGF0ZU9iai5zZWNvbmRzICogMTAwMCwgJ21lZGl1bScsICdVVEMnKSkgOiBudWxsO1xuICB9XG5cbn1cbiJdfQ==