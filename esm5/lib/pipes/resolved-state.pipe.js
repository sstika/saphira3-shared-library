/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Pipe } from '@angular/core';
var ResolvedStatePipe = /** @class */ (function () {
    function ResolvedStatePipe() {
    }
    // A simple filter that converts resolved state enums to friendlier string values.
    // A simple filter that converts resolved state enums to friendlier string values.
    /**
     * @param {?} resolution
     * @param {?=} value
     * @return {?}
     */
    ResolvedStatePipe.prototype.transform = 
    // A simple filter that converts resolved state enums to friendlier string values.
    /**
     * @param {?} resolution
     * @param {?=} value
     * @return {?}
     */
    function (resolution, value) {
        if (resolution === 'RESOLVED') {
            return 'Resolved';
        }
        else if (resolution === 'UNRESOLVED') {
            return 'Unresolved';
        }
        else {
            return 'Manually Set : ' + value;
        }
    };
    ResolvedStatePipe.decorators = [
        { type: Pipe, args: [{
                    name: 'resolvedState'
                },] }
    ];
    return ResolvedStatePipe;
}());
export { ResolvedStatePipe };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmVzb2x2ZWQtc3RhdGUucGlwZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BocHRlY2gvc2FwaGlyYS1zaGFyZWQtbGliLyIsInNvdXJjZXMiOlsibGliL3BpcGVzL3Jlc29sdmVkLXN0YXRlLnBpcGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxJQUFJLEVBQWlCLE1BQU0sZUFBZSxDQUFDO0FBRXBEO0lBQUE7SUFjQSxDQUFDO0lBVkMsa0ZBQWtGOzs7Ozs7O0lBQ2xGLHFDQUFTOzs7Ozs7O0lBQVQsVUFBVSxVQUFlLEVBQUUsS0FBVztRQUNwQyxJQUFJLFVBQVUsS0FBSyxVQUFVLEVBQUU7WUFDN0IsT0FBTyxVQUFVLENBQUM7U0FDbkI7YUFBTSxJQUFJLFVBQVUsS0FBSyxZQUFZLEVBQUU7WUFDdEMsT0FBTyxZQUFZLENBQUM7U0FDckI7YUFBTTtZQUNMLE9BQU8saUJBQWlCLEdBQUcsS0FBSyxDQUFDO1NBQ2xDO0lBQ0gsQ0FBQzs7Z0JBYkYsSUFBSSxTQUFDO29CQUNKLElBQUksRUFBRSxlQUFlO2lCQUN0Qjs7SUFZRCx3QkFBQztDQUFBLEFBZEQsSUFjQztTQVhZLGlCQUFpQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFBpcGUsIFBpcGVUcmFuc2Zvcm0gfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQFBpcGUoe1xuICBuYW1lOiAncmVzb2x2ZWRTdGF0ZSdcbn0pXG5leHBvcnQgY2xhc3MgUmVzb2x2ZWRTdGF0ZVBpcGUgaW1wbGVtZW50cyBQaXBlVHJhbnNmb3JtIHtcbiAgLy8gQSBzaW1wbGUgZmlsdGVyIHRoYXQgY29udmVydHMgcmVzb2x2ZWQgc3RhdGUgZW51bXMgdG8gZnJpZW5kbGllciBzdHJpbmcgdmFsdWVzLlxuICB0cmFuc2Zvcm0ocmVzb2x1dGlvbjogYW55LCB2YWx1ZT86IGFueSk6IGFueSB7XG4gICAgaWYgKHJlc29sdXRpb24gPT09ICdSRVNPTFZFRCcpIHtcbiAgICAgIHJldHVybiAnUmVzb2x2ZWQnO1xuICAgIH0gZWxzZSBpZiAocmVzb2x1dGlvbiA9PT0gJ1VOUkVTT0xWRUQnKSB7XG4gICAgICByZXR1cm4gJ1VucmVzb2x2ZWQnO1xuICAgIH0gZWxzZSB7XG4gICAgICByZXR1cm4gJ01hbnVhbGx5IFNldCA6ICcgKyB2YWx1ZTtcbiAgICB9XG4gIH1cbn1cbiJdfQ==