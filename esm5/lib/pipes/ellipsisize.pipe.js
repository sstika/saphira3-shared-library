/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Pipe } from '@angular/core';
var EllipsisizePipe = /** @class */ (function () {
    function EllipsisizePipe() {
    }
    /**
     * @param {?} value
     * @param {?} desiredLengthIncludingElip
     * @return {?}
     */
    EllipsisizePipe.prototype.transform = /**
     * @param {?} value
     * @param {?} desiredLengthIncludingElip
     * @return {?}
     */
    function (value, desiredLengthIncludingElip) {
        if (value && value.length > desiredLengthIncludingElip) {
            // TODO: need to make this clickable to show the whole string or something
            return value.substring(0, desiredLengthIncludingElip - 3) + '...';
        }
        return value;
    };
    EllipsisizePipe.decorators = [
        { type: Pipe, args: [{ name: 'ellipsisize' },] }
    ];
    return EllipsisizePipe;
}());
export { EllipsisizePipe };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZWxsaXBzaXNpemUucGlwZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BocHRlY2gvc2FwaGlyYS1zaGFyZWQtbGliLyIsInNvdXJjZXMiOlsibGliL3BpcGVzL2VsbGlwc2lzaXplLnBpcGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxJQUFJLEVBQWlCLE1BQU0sZUFBZSxDQUFDO0FBRXBEO0lBQUE7SUFTQSxDQUFDOzs7Ozs7SUFQQyxtQ0FBUzs7Ozs7SUFBVCxVQUFVLEtBQVUsRUFBRSwwQkFBa0M7UUFDdEQsSUFBSSxLQUFLLElBQUksS0FBSyxDQUFDLE1BQU0sR0FBRywwQkFBMEIsRUFBRTtZQUN0RCwwRUFBMEU7WUFDMUUsT0FBTyxLQUFLLENBQUMsU0FBUyxDQUFDLENBQUMsRUFBRSwwQkFBMEIsR0FBRyxDQUFDLENBQUMsR0FBRyxLQUFLLENBQUM7U0FDbkU7UUFDRCxPQUFPLEtBQUssQ0FBQztJQUNmLENBQUM7O2dCQVJGLElBQUksU0FBQyxFQUFDLElBQUksRUFBRSxhQUFhLEVBQUM7O0lBUzNCLHNCQUFDO0NBQUEsQUFURCxJQVNDO1NBUlksZUFBZSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFBpcGUsIFBpcGVUcmFuc2Zvcm0gfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQFBpcGUoe25hbWU6ICdlbGxpcHNpc2l6ZSd9KVxuZXhwb3J0IGNsYXNzIEVsbGlwc2lzaXplUGlwZSBpbXBsZW1lbnRzIFBpcGVUcmFuc2Zvcm0ge1xuICB0cmFuc2Zvcm0odmFsdWU6IGFueSwgZGVzaXJlZExlbmd0aEluY2x1ZGluZ0VsaXA6IG51bWJlcikge1xuICAgIGlmICh2YWx1ZSAmJiB2YWx1ZS5sZW5ndGggPiBkZXNpcmVkTGVuZ3RoSW5jbHVkaW5nRWxpcCkge1xuICAgICAgLy8gVE9ETzogbmVlZCB0byBtYWtlIHRoaXMgY2xpY2thYmxlIHRvIHNob3cgdGhlIHdob2xlIHN0cmluZyBvciBzb21ldGhpbmdcbiAgICAgIHJldHVybiB2YWx1ZS5zdWJzdHJpbmcoMCwgZGVzaXJlZExlbmd0aEluY2x1ZGluZ0VsaXAgLSAzKSArICcuLi4nO1xuICAgIH1cbiAgICByZXR1cm4gdmFsdWU7XG4gIH1cbn1cbiJdfQ==