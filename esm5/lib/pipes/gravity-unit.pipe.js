/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Pipe } from '@angular/core';
var GravityUnitPipe = /** @class */ (function () {
    function GravityUnitPipe() {
    }
    /**
     * @param {?} unit
     * @param {?=} full
     * @return {?}
     */
    GravityUnitPipe.prototype.transform = /**
     * @param {?} unit
     * @param {?=} full
     * @return {?}
     */
    function (unit, full) {
        /** @type {?} */
        var string = '';
        switch (unit) {
            case 'M_PER_SEC_SQ':
                string = (full === true ? 'Meters per Second Sq.' : 'm/s' + decodeURI('%C2%B2'));
                break;
            case 'FT_PER_SEC_SQ':
                string = (full === true ? 'Feet per Second Sq.' : 'f/s' + decodeURI('%C2%B2'));
                break;
            case 'G':
                string = 'G';
                break;
            case 'MILLI_G':
                string = 'mG';
                break;
            case 'G_98':
                string = 'G (9.8)';
                break;
            case 'GAL':
                string = 'Gal';
                break;
            case 'MILLI_GAL':
                string = 'mGal';
                break;
            default:
                string = unit;
                break;
        }
        return string;
    };
    GravityUnitPipe.decorators = [
        { type: Pipe, args: [{
                    name: 'gravityUnit'
                },] }
    ];
    return GravityUnitPipe;
}());
export { GravityUnitPipe };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ3Jhdml0eS11bml0LnBpcGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AaHB0ZWNoL3NhcGhpcmEtc2hhcmVkLWxpYi8iLCJzb3VyY2VzIjpbImxpYi9waXBlcy9ncmF2aXR5LXVuaXQucGlwZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLElBQUksRUFBaUIsTUFBTSxlQUFlLENBQUM7QUFFcEQ7SUFBQTtJQW9DQSxDQUFDOzs7Ozs7SUEvQkMsbUNBQVM7Ozs7O0lBQVQsVUFBVSxJQUFZLEVBQUUsSUFBVTs7WUFDNUIsTUFBTSxHQUFHLEVBQUU7UUFDZixRQUFRLElBQUksRUFBRTtZQUNWLEtBQUssY0FBYztnQkFDZixNQUFNLEdBQUcsQ0FBQyxJQUFJLEtBQUssSUFBSSxDQUFDLENBQUMsQ0FBQyx1QkFBdUIsQ0FBQyxDQUFDLENBQUMsS0FBSyxHQUFHLFNBQVMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO2dCQUNqRixNQUFNO1lBQ1YsS0FBSyxlQUFlO2dCQUNoQixNQUFNLEdBQUcsQ0FBQyxJQUFJLEtBQUssSUFBSSxDQUFDLENBQUMsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDLENBQUMsS0FBSyxHQUFHLFNBQVMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO2dCQUMvRSxNQUFNO1lBQ1YsS0FBSyxHQUFHO2dCQUNKLE1BQU0sR0FBRyxHQUFHLENBQUM7Z0JBQ2IsTUFBTTtZQUNWLEtBQUssU0FBUztnQkFDVixNQUFNLEdBQUcsSUFBSSxDQUFDO2dCQUNkLE1BQU07WUFDVixLQUFLLE1BQU07Z0JBQ1AsTUFBTSxHQUFHLFNBQVMsQ0FBQztnQkFDbkIsTUFBTTtZQUNWLEtBQUssS0FBSztnQkFDTixNQUFNLEdBQUcsS0FBSyxDQUFDO2dCQUNmLE1BQU07WUFDVixLQUFLLFdBQVc7Z0JBQ1osTUFBTSxHQUFHLE1BQU0sQ0FBQztnQkFDaEIsTUFBTTtZQUNWO2dCQUNJLE1BQU0sR0FBRyxJQUFJLENBQUM7Z0JBQ2QsTUFBTTtTQUNiO1FBQ0QsT0FBTyxNQUFNLENBQUM7SUFDaEIsQ0FBQzs7Z0JBbENGLElBQUksU0FBQztvQkFDSixJQUFJLEVBQUUsYUFBYTtpQkFDcEI7O0lBa0NELHNCQUFDO0NBQUEsQUFwQ0QsSUFvQ0M7U0FqQ1ksZUFBZSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFBpcGUsIFBpcGVUcmFuc2Zvcm0gfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQFBpcGUoe1xuICBuYW1lOiAnZ3Jhdml0eVVuaXQnXG59KVxuZXhwb3J0IGNsYXNzIEdyYXZpdHlVbml0UGlwZSBpbXBsZW1lbnRzIFBpcGVUcmFuc2Zvcm0ge1xuXG4gIHRyYW5zZm9ybSh1bml0OiBzdHJpbmcsIGZ1bGw/OiBhbnkpOiBzdHJpbmcge1xuICAgIGxldCBzdHJpbmcgPSAnJztcbiAgICBzd2l0Y2ggKHVuaXQpIHtcbiAgICAgICAgY2FzZSAnTV9QRVJfU0VDX1NRJzpcbiAgICAgICAgICAgIHN0cmluZyA9IChmdWxsID09PSB0cnVlID8gJ01ldGVycyBwZXIgU2Vjb25kIFNxLicgOiAnbS9zJyArIGRlY29kZVVSSSgnJUMyJUIyJykpO1xuICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgIGNhc2UgJ0ZUX1BFUl9TRUNfU1EnOlxuICAgICAgICAgICAgc3RyaW5nID0gKGZ1bGwgPT09IHRydWUgPyAnRmVldCBwZXIgU2Vjb25kIFNxLicgOiAnZi9zJyArIGRlY29kZVVSSSgnJUMyJUIyJykpO1xuICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgIGNhc2UgJ0cnOlxuICAgICAgICAgICAgc3RyaW5nID0gJ0cnO1xuICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgIGNhc2UgJ01JTExJX0cnOlxuICAgICAgICAgICAgc3RyaW5nID0gJ21HJztcbiAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICBjYXNlICdHXzk4JzpcbiAgICAgICAgICAgIHN0cmluZyA9ICdHICg5LjgpJztcbiAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICBjYXNlICdHQUwnOlxuICAgICAgICAgICAgc3RyaW5nID0gJ0dhbCc7XG4gICAgICAgICAgICBicmVhaztcbiAgICAgICAgY2FzZSAnTUlMTElfR0FMJzpcbiAgICAgICAgICAgIHN0cmluZyA9ICdtR2FsJztcbiAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICBkZWZhdWx0OlxuICAgICAgICAgICAgc3RyaW5nID0gdW5pdDtcbiAgICAgICAgICAgIGJyZWFrO1xuICAgIH1cbiAgICByZXR1cm4gc3RyaW5nO1xuICB9XG5cbn1cbiJdfQ==