/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Pipe } from '@angular/core';
var GravityDecimalPipe = /** @class */ (function () {
    function GravityDecimalPipe() {
        // number of decimal places to display for various gravity units
        this.decimalMap = {
            M_PER_SEC_SQ: 3,
            FT_PER_SEC_SQ: 3,
            G: 4,
            MILLI_G: 2,
            G_98: 1,
            GAL: 4,
            MILLI_GAL: 1,
            'Meters per Second Sq.': 3,
            'm/s²': 3,
            'f/s²': 3,
            mG: 2,
            'G (9.8)': 1,
            Gal: 4,
            mGal: 1,
        };
    }
    /**
     * @param {?} value
     * @param {?=} unit
     * @return {?}
     */
    GravityDecimalPipe.prototype.transform = /**
     * @param {?} value
     * @param {?=} unit
     * @return {?}
     */
    function (value, unit) {
        if (!this.isValidFloat(value)) {
            return 'N/A';
        }
        else {
            value = parseFloat(value);
            if (typeof (this.decimalMap[unit]) !== 'undefined') {
                return this.formatNumber(value.toFixed(this.decimalMap[unit]));
            }
            else {
                return 'Unsupported Unit: ' + unit;
            }
        }
    };
    /**
     * isValidFloat:
     *
     * A stricter float filter than parseFloat().
     *
     * @param value Input value to be parsed as float
     * @returns float
     */
    /**
     * isValidFloat:
     *
     * A stricter float filter than parseFloat().
     *
     * @param {?} value Input value to be parsed as float
     * @return {?} float
     */
    GravityDecimalPipe.prototype.isValidFloat = /**
     * isValidFloat:
     *
     * A stricter float filter than parseFloat().
     *
     * @param {?} value Input value to be parsed as float
     * @return {?} float
     */
    function (value) {
        return !isNaN(parseFloat(value));
    };
    /**
      * formatNumber:
      *
      * Adds commas and removes the negative sign for zero values.
      *
      * @param number Input value to be formatted
      */
    /**
     * formatNumber:
     *
     * Adds commas and removes the negative sign for zero values.
     *
     * @param {?} number Input value to be formatted
     * @return {?}
     */
    GravityDecimalPipe.prototype.formatNumber = /**
     * formatNumber:
     *
     * Adds commas and removes the negative sign for zero values.
     *
     * @param {?} number Input value to be formatted
     * @return {?}
     */
    function (number) {
        return this.removeSignOnZero(this.addCommas(number));
    };
    /**
       * removeSignOnZero:
       *
       * If the value is zero, remove any negative signs. (converts -0.00 to 0.00)
       *
       * @param number Input value to be sanitized
       */
    /**
     * removeSignOnZero:
     *
     * If the value is zero, remove any negative signs. (converts -0.00 to 0.00)
     *
     * @param {?} number Input value to be sanitized
     * @return {?}
     */
    GravityDecimalPipe.prototype.removeSignOnZero = /**
     * removeSignOnZero:
     *
     * If the value is zero, remove any negative signs. (converts -0.00 to 0.00)
     *
     * @param {?} number Input value to be sanitized
     * @return {?}
     */
    function (number) {
        /** @type {?} */
        var split = number.toString().split('-');
        if (split.length === 2) {
            if (split[1] === 0) {
                return split[1];
            }
        }
        return number;
    };
    /**
       * addCommas:
       *
       * Helper function that adds in commas to a number for readability.
       *
       * @param number Input value to add commas
       * @returns string
       */
    /**
     * addCommas:
     *
     * Helper function that adds in commas to a number for readability.
     *
     * @param {?} number Input value to add commas
     * @return {?} string
     */
    GravityDecimalPipe.prototype.addCommas = /**
     * addCommas:
     *
     * Helper function that adds in commas to a number for readability.
     *
     * @param {?} number Input value to add commas
     * @return {?} string
     */
    function (number) {
        // check if we have a decimal
        /** @type {?} */
        var split = number.toString().split('.');
        // if we have a decimal
        if (split.length === 2) {
            // only add commas to the first half and return the second half
            return split[0].replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,') + '.' + split[1];
        }
        else {
            // add commas throughout
            return split[0].replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
        }
    };
    GravityDecimalPipe.decorators = [
        { type: Pipe, args: [{
                    name: 'gravityDecimal'
                },] }
    ];
    return GravityDecimalPipe;
}());
export { GravityDecimalPipe };
if (false) {
    /** @type {?} */
    GravityDecimalPipe.prototype.decimalMap;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ3Jhdml0eS1kZWNpbWFsLnBpcGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AaHB0ZWNoL3NhcGhpcmEtc2hhcmVkLWxpYi8iLCJzb3VyY2VzIjpbImxpYi9waXBlcy9ncmF2aXR5LWRlY2ltYWwucGlwZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLElBQUksRUFBaUIsTUFBTSxlQUFlLENBQUM7QUFFcEQ7SUFBQTs7UUFPRSxlQUFVLEdBQUc7WUFDWCxZQUFZLEVBQUUsQ0FBQztZQUNmLGFBQWEsRUFBRSxDQUFDO1lBQ2hCLENBQUMsRUFBRSxDQUFDO1lBQ0osT0FBTyxFQUFFLENBQUM7WUFDVixJQUFJLEVBQUUsQ0FBQztZQUNQLEdBQUcsRUFBRSxDQUFDO1lBQ04sU0FBUyxFQUFFLENBQUM7WUFDWix1QkFBdUIsRUFBRSxDQUFDO1lBQzFCLE1BQU0sRUFBRSxDQUFDO1lBQ1QsTUFBTSxFQUFFLENBQUM7WUFDVCxFQUFFLEVBQUUsQ0FBQztZQUNMLFNBQVMsRUFBRSxDQUFDO1lBQ1osR0FBRyxFQUFFLENBQUM7WUFDTixJQUFJLEVBQUUsQ0FBQztTQUNSLENBQUM7SUE0RUosQ0FBQzs7Ozs7O0lBMUVDLHNDQUFTOzs7OztJQUFULFVBQVUsS0FBVSxFQUFFLElBQVU7UUFDOUIsSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLEVBQUU7WUFDN0IsT0FBTyxLQUFLLENBQUM7U0FDZDthQUFNO1lBQ0wsS0FBSyxHQUFHLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUMxQixJQUFJLE9BQU8sQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDLEtBQUssV0FBVyxFQUFFO2dCQUNsRCxPQUFPLElBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQzthQUNoRTtpQkFBTTtnQkFDTCxPQUFPLG9CQUFvQixHQUFHLElBQUksQ0FBQzthQUNwQztTQUNGO0lBQ0gsQ0FBQztJQUVEOzs7Ozs7O09BT0c7Ozs7Ozs7OztJQUNJLHlDQUFZOzs7Ozs7OztJQUFuQixVQUFvQixLQUFLO1FBQ3ZCLE9BQU8sQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7SUFDbkMsQ0FBQztJQUVEOzs7Ozs7UUFNSTs7Ozs7Ozs7O0lBQ0cseUNBQVk7Ozs7Ozs7O0lBQW5CLFVBQW9CLE1BQU07UUFDeEIsT0FBTyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO0lBQ3ZELENBQUM7SUFFRDs7Ozs7O1NBTUs7Ozs7Ozs7OztJQUNFLDZDQUFnQjs7Ozs7Ozs7SUFBdkIsVUFBd0IsTUFBTTs7WUFDdEIsS0FBSyxHQUFHLE1BQU0sQ0FBQyxRQUFRLEVBQUUsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDO1FBQzFDLElBQUksS0FBSyxDQUFDLE1BQU0sS0FBSyxDQUFDLEVBQUU7WUFDdEIsSUFBSSxLQUFLLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxFQUFFO2dCQUNsQixPQUFPLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQzthQUNqQjtTQUNGO1FBQ0QsT0FBTyxNQUFNLENBQUM7SUFDaEIsQ0FBQztJQUVEOzs7Ozs7O1NBT0s7Ozs7Ozs7OztJQUNFLHNDQUFTOzs7Ozs7OztJQUFoQixVQUFpQixNQUFNOzs7WUFFZixLQUFLLEdBQUcsTUFBTSxDQUFDLFFBQVEsRUFBRSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUM7UUFDMUMsdUJBQXVCO1FBQ3ZCLElBQUksS0FBSyxDQUFDLE1BQU0sS0FBSyxDQUFDLEVBQUU7WUFDdEIsK0RBQStEO1lBQy9ELE9BQU8sS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyx5QkFBeUIsRUFBRSxLQUFLLENBQUMsR0FBRyxHQUFHLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO1NBQzVFO2FBQU07WUFDTCx3QkFBd0I7WUFDeEIsT0FBTyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLHlCQUF5QixFQUFFLEtBQUssQ0FBQyxDQUFDO1NBQzNEO0lBQ0gsQ0FBQzs7Z0JBaEdGLElBQUksU0FBQztvQkFDSixJQUFJLEVBQUUsZ0JBQWdCO2lCQUN2Qjs7SUFnR0QseUJBQUM7Q0FBQSxBQWxHRCxJQWtHQztTQTdGWSxrQkFBa0I7OztJQUU3Qix3Q0FlRSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFBpcGUsIFBpcGVUcmFuc2Zvcm0gfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQFBpcGUoe1xuICBuYW1lOiAnZ3Jhdml0eURlY2ltYWwnXG59KVxuLy8gUmV0dXJucyB0aGUgZ3Jhdml0eSB2YWx1ZSBhcyBhIHN0cmluZyB3aXRoIGFuIGFtb3VudCBvZiBkZWNpbWFsIHBsYWNlcyBiYXNlZCBvbiBpdHMgdW5pdC5cbi8vIEFsc28gZm9ybWF0cyB0aGUgc3RyaW5nIHdpdGggY29tbWFzIGZvciByZWFkYWJpbGl0eS5cbmV4cG9ydCBjbGFzcyBHcmF2aXR5RGVjaW1hbFBpcGUgaW1wbGVtZW50cyBQaXBlVHJhbnNmb3JtIHtcbiAgLy8gbnVtYmVyIG9mIGRlY2ltYWwgcGxhY2VzIHRvIGRpc3BsYXkgZm9yIHZhcmlvdXMgZ3Jhdml0eSB1bml0c1xuICBkZWNpbWFsTWFwID0ge1xuICAgIE1fUEVSX1NFQ19TUTogMyxcbiAgICBGVF9QRVJfU0VDX1NROiAzLFxuICAgIEc6IDQsXG4gICAgTUlMTElfRzogMixcbiAgICBHXzk4OiAxLFxuICAgIEdBTDogNCxcbiAgICBNSUxMSV9HQUw6IDEsXG4gICAgJ01ldGVycyBwZXIgU2Vjb25kIFNxLic6IDMsXG4gICAgJ20vc8KyJzogMyxcbiAgICAnZi9zwrInOiAzLFxuICAgIG1HOiAyLFxuICAgICdHICg5LjgpJzogMSxcbiAgICBHYWw6IDQsXG4gICAgbUdhbDogMSxcbiAgfTtcblxuICB0cmFuc2Zvcm0odmFsdWU6IGFueSwgdW5pdD86IGFueSk6IGFueSB7XG4gICAgaWYgKCF0aGlzLmlzVmFsaWRGbG9hdCh2YWx1ZSkpIHtcbiAgICAgIHJldHVybiAnTi9BJztcbiAgICB9IGVsc2Uge1xuICAgICAgdmFsdWUgPSBwYXJzZUZsb2F0KHZhbHVlKTtcbiAgICAgIGlmICh0eXBlb2YgKHRoaXMuZGVjaW1hbE1hcFt1bml0XSkgIT09ICd1bmRlZmluZWQnKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmZvcm1hdE51bWJlcih2YWx1ZS50b0ZpeGVkKHRoaXMuZGVjaW1hbE1hcFt1bml0XSkpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgcmV0dXJuICdVbnN1cHBvcnRlZCBVbml0OiAnICsgdW5pdDtcbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICAvKipcbiAgICogaXNWYWxpZEZsb2F0OlxuICAgKlxuICAgKiBBIHN0cmljdGVyIGZsb2F0IGZpbHRlciB0aGFuIHBhcnNlRmxvYXQoKS5cbiAgICpcbiAgICogQHBhcmFtIHZhbHVlIElucHV0IHZhbHVlIHRvIGJlIHBhcnNlZCBhcyBmbG9hdFxuICAgKiBAcmV0dXJucyBmbG9hdFxuICAgKi9cbiAgcHVibGljIGlzVmFsaWRGbG9hdCh2YWx1ZSkge1xuICAgIHJldHVybiAhaXNOYU4ocGFyc2VGbG9hdCh2YWx1ZSkpO1xuICB9XG5cbiAgLyoqXG4gICAgKiBmb3JtYXROdW1iZXI6XG4gICAgKlxuICAgICogQWRkcyBjb21tYXMgYW5kIHJlbW92ZXMgdGhlIG5lZ2F0aXZlIHNpZ24gZm9yIHplcm8gdmFsdWVzLlxuICAgICpcbiAgICAqIEBwYXJhbSBudW1iZXIgSW5wdXQgdmFsdWUgdG8gYmUgZm9ybWF0dGVkXG4gICAgKi9cbiAgcHVibGljIGZvcm1hdE51bWJlcihudW1iZXIpIHtcbiAgICByZXR1cm4gdGhpcy5yZW1vdmVTaWduT25aZXJvKHRoaXMuYWRkQ29tbWFzKG51bWJlcikpO1xuICB9XG5cbiAgLyoqXG4gICAgICogcmVtb3ZlU2lnbk9uWmVybzpcbiAgICAgKlxuICAgICAqIElmIHRoZSB2YWx1ZSBpcyB6ZXJvLCByZW1vdmUgYW55IG5lZ2F0aXZlIHNpZ25zLiAoY29udmVydHMgLTAuMDAgdG8gMC4wMClcbiAgICAgKlxuICAgICAqIEBwYXJhbSBudW1iZXIgSW5wdXQgdmFsdWUgdG8gYmUgc2FuaXRpemVkXG4gICAgICovXG4gIHB1YmxpYyByZW1vdmVTaWduT25aZXJvKG51bWJlcikge1xuICAgIGNvbnN0IHNwbGl0ID0gbnVtYmVyLnRvU3RyaW5nKCkuc3BsaXQoJy0nKTtcbiAgICBpZiAoc3BsaXQubGVuZ3RoID09PSAyKSB7XG4gICAgICBpZiAoc3BsaXRbMV0gPT09IDApIHtcbiAgICAgICAgcmV0dXJuIHNwbGl0WzFdO1xuICAgICAgfVxuICAgIH1cbiAgICByZXR1cm4gbnVtYmVyO1xuICB9XG5cbiAgLyoqXG4gICAgICogYWRkQ29tbWFzOlxuICAgICAqXG4gICAgICogSGVscGVyIGZ1bmN0aW9uIHRoYXQgYWRkcyBpbiBjb21tYXMgdG8gYSBudW1iZXIgZm9yIHJlYWRhYmlsaXR5LlxuICAgICAqXG4gICAgICogQHBhcmFtIG51bWJlciBJbnB1dCB2YWx1ZSB0byBhZGQgY29tbWFzXG4gICAgICogQHJldHVybnMgc3RyaW5nXG4gICAgICovXG4gIHB1YmxpYyBhZGRDb21tYXMobnVtYmVyKSB7XG4gICAgLy8gY2hlY2sgaWYgd2UgaGF2ZSBhIGRlY2ltYWxcbiAgICBjb25zdCBzcGxpdCA9IG51bWJlci50b1N0cmluZygpLnNwbGl0KCcuJyk7XG4gICAgLy8gaWYgd2UgaGF2ZSBhIGRlY2ltYWxcbiAgICBpZiAoc3BsaXQubGVuZ3RoID09PSAyKSB7XG4gICAgICAvLyBvbmx5IGFkZCBjb21tYXMgdG8gdGhlIGZpcnN0IGhhbGYgYW5kIHJldHVybiB0aGUgc2Vjb25kIGhhbGZcbiAgICAgIHJldHVybiBzcGxpdFswXS5yZXBsYWNlKC8oXFxkKSg/PShcXGR7M30pKyg/IVxcZCkpL2csICckMSwnKSArICcuJyArIHNwbGl0WzFdO1xuICAgIH0gZWxzZSB7XG4gICAgICAvLyBhZGQgY29tbWFzIHRocm91Z2hvdXRcbiAgICAgIHJldHVybiBzcGxpdFswXS5yZXBsYWNlKC8oXFxkKSg/PShcXGR7M30pKyg/IVxcZCkpL2csICckMSwnKTtcbiAgICB9XG4gIH1cblxufVxuIl19