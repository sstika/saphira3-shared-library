/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Pipe } from '@angular/core';
var DistanceUnitPipe = /** @class */ (function () {
    function DistanceUnitPipe() {
    }
    /**
     * @param {?} unit
     * @param {?=} full
     * @return {?}
     */
    DistanceUnitPipe.prototype.transform = /**
     * @param {?} unit
     * @param {?=} full
     * @return {?}
     */
    function (unit, full) {
        /** @type {?} */
        var string = '';
        switch (unit) {
            case 'METER':
                string = 'Meters';
                break;
            case 'KILOMETER':
                string = 'Kilometers';
                break;
            case 'FOOT':
                string = 'Feet';
                break;
            case 'FOOT_US':
                string = 'ft US';
                break;
            case 'YARD':
                string = 'Yards';
                break;
            case 'MILE':
                string = 'Miles';
                break;
            default:
                string = unit;
                break;
        }
        return string;
    };
    DistanceUnitPipe.decorators = [
        { type: Pipe, args: [{
                    name: 'distanceUnit'
                },] }
    ];
    return DistanceUnitPipe;
}());
export { DistanceUnitPipe };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGlzdGFuY2UtdW5pdC5waXBlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGhwdGVjaC9zYXBoaXJhLXNoYXJlZC1saWIvIiwic291cmNlcyI6WyJsaWIvcGlwZXMvZGlzdGFuY2UtdW5pdC5waXBlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsSUFBSSxFQUFpQixNQUFNLGVBQWUsQ0FBQztBQUVwRDtJQUFBO0lBaUNBLENBQUM7Ozs7OztJQTVCQyxvQ0FBUzs7Ozs7SUFBVCxVQUFVLElBQVMsRUFBRSxJQUFVOztZQUN6QixNQUFNLEdBQUcsRUFBRTtRQUNmLFFBQVEsSUFBSSxFQUFFO1lBQ1osS0FBSyxPQUFPO2dCQUNWLE1BQU0sR0FBRyxRQUFRLENBQUM7Z0JBQ2xCLE1BQU07WUFDUixLQUFLLFdBQVc7Z0JBQ2QsTUFBTSxHQUFHLFlBQVksQ0FBQztnQkFDdEIsTUFBTTtZQUNSLEtBQUssTUFBTTtnQkFDVCxNQUFNLEdBQUcsTUFBTSxDQUFDO2dCQUNoQixNQUFNO1lBQ1IsS0FBSyxTQUFTO2dCQUNaLE1BQU0sR0FBRyxPQUFPLENBQUM7Z0JBQ2pCLE1BQU07WUFDUixLQUFLLE1BQU07Z0JBQ1QsTUFBTSxHQUFHLE9BQU8sQ0FBQztnQkFDakIsTUFBTTtZQUNSLEtBQUssTUFBTTtnQkFDVCxNQUFNLEdBQUcsT0FBTyxDQUFDO2dCQUNqQixNQUFNO1lBQ1I7Z0JBQ0UsTUFBTSxHQUFHLElBQUksQ0FBQztnQkFDZCxNQUFNO1NBQ1Q7UUFDRCxPQUFPLE1BQU0sQ0FBQztJQUNoQixDQUFDOztnQkEvQkYsSUFBSSxTQUFDO29CQUNKLElBQUksRUFBRSxjQUFjO2lCQUNyQjs7SUErQkQsdUJBQUM7Q0FBQSxBQWpDRCxJQWlDQztTQTlCWSxnQkFBZ0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBQaXBlLCBQaXBlVHJhbnNmb3JtIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBQaXBlKHtcbiAgbmFtZTogJ2Rpc3RhbmNlVW5pdCdcbn0pXG5leHBvcnQgY2xhc3MgRGlzdGFuY2VVbml0UGlwZSBpbXBsZW1lbnRzIFBpcGVUcmFuc2Zvcm0ge1xuXG4gIHRyYW5zZm9ybSh1bml0OiBhbnksIGZ1bGw/OiBhbnkpOiBhbnkge1xuICAgIGxldCBzdHJpbmcgPSAnJztcbiAgICBzd2l0Y2ggKHVuaXQpIHtcbiAgICAgIGNhc2UgJ01FVEVSJzpcbiAgICAgICAgc3RyaW5nID0gJ01ldGVycyc7XG4gICAgICAgIGJyZWFrO1xuICAgICAgY2FzZSAnS0lMT01FVEVSJzpcbiAgICAgICAgc3RyaW5nID0gJ0tpbG9tZXRlcnMnO1xuICAgICAgICBicmVhaztcbiAgICAgIGNhc2UgJ0ZPT1QnOlxuICAgICAgICBzdHJpbmcgPSAnRmVldCc7XG4gICAgICAgIGJyZWFrO1xuICAgICAgY2FzZSAnRk9PVF9VUyc6XG4gICAgICAgIHN0cmluZyA9ICdmdCBVUyc7XG4gICAgICAgIGJyZWFrO1xuICAgICAgY2FzZSAnWUFSRCc6XG4gICAgICAgIHN0cmluZyA9ICdZYXJkcyc7XG4gICAgICAgIGJyZWFrO1xuICAgICAgY2FzZSAnTUlMRSc6XG4gICAgICAgIHN0cmluZyA9ICdNaWxlcyc7XG4gICAgICAgIGJyZWFrO1xuICAgICAgZGVmYXVsdDpcbiAgICAgICAgc3RyaW5nID0gdW5pdDtcbiAgICAgICAgYnJlYWs7XG4gICAgfVxuICAgIHJldHVybiBzdHJpbmc7XG4gIH1cblxufVxuIl19