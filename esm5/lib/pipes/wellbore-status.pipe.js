/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Pipe } from '@angular/core';
var WellboreStatusPipe = /** @class */ (function () {
    function WellboreStatusPipe() {
    }
    /**
     * @param {?} value
     * @param {?=} args
     * @return {?}
     */
    WellboreStatusPipe.prototype.transform = /**
     * @param {?} value
     * @param {?=} args
     * @return {?}
     */
    function (value, args) {
        switch (value) {
            case 'ACTIVE_BATCH_PROCESS':
                return 'Active - Batch Process';
            case 'ACTIVE_REALTIME':
                return 'Active - Realtime';
            case 'PRE_OPERATIONS':
                return 'Pre-Operations';
            case 'COMPLETED':
                return 'Completed';
            default:
                return 'Unknown status';
        }
    };
    WellboreStatusPipe.decorators = [
        { type: Pipe, args: [{
                    name: 'wellboreStatus'
                },] }
    ];
    return WellboreStatusPipe;
}());
export { WellboreStatusPipe };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoid2VsbGJvcmUtc3RhdHVzLnBpcGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AaHB0ZWNoL3NhcGhpcmEtc2hhcmVkLWxpYi8iLCJzb3VyY2VzIjpbImxpYi9waXBlcy93ZWxsYm9yZS1zdGF0dXMucGlwZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLElBQUksRUFBaUIsTUFBTSxlQUFlLENBQUM7QUFFcEQ7SUFBQTtJQW9CQSxDQUFDOzs7Ozs7SUFmQyxzQ0FBUzs7Ozs7SUFBVCxVQUFVLEtBQVUsRUFBRSxJQUFVO1FBQzlCLFFBQVEsS0FBSyxFQUFFO1lBQ1gsS0FBSyxzQkFBc0I7Z0JBQ3ZCLE9BQU8sd0JBQXdCLENBQUM7WUFDcEMsS0FBSyxpQkFBaUI7Z0JBQ2xCLE9BQU8sbUJBQW1CLENBQUM7WUFDL0IsS0FBSyxnQkFBZ0I7Z0JBQ2pCLE9BQU8sZ0JBQWdCLENBQUM7WUFDNUIsS0FBSyxXQUFXO2dCQUNaLE9BQU8sV0FBVyxDQUFDO1lBQ3ZCO2dCQUNJLE9BQU8sZ0JBQWdCLENBQUM7U0FDL0I7SUFDSCxDQUFDOztnQkFsQkYsSUFBSSxTQUFDO29CQUNKLElBQUksRUFBRSxnQkFBZ0I7aUJBQ3ZCOztJQWtCRCx5QkFBQztDQUFBLEFBcEJELElBb0JDO1NBakJZLGtCQUFrQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFBpcGUsIFBpcGVUcmFuc2Zvcm0gfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQFBpcGUoe1xuICBuYW1lOiAnd2VsbGJvcmVTdGF0dXMnXG59KVxuZXhwb3J0IGNsYXNzIFdlbGxib3JlU3RhdHVzUGlwZSBpbXBsZW1lbnRzIFBpcGVUcmFuc2Zvcm0ge1xuXG4gIHRyYW5zZm9ybSh2YWx1ZTogYW55LCBhcmdzPzogYW55KTogYW55IHtcbiAgICBzd2l0Y2ggKHZhbHVlKSB7XG4gICAgICAgIGNhc2UgJ0FDVElWRV9CQVRDSF9QUk9DRVNTJzpcbiAgICAgICAgICAgIHJldHVybiAnQWN0aXZlIC0gQmF0Y2ggUHJvY2Vzcyc7XG4gICAgICAgIGNhc2UgJ0FDVElWRV9SRUFMVElNRSc6XG4gICAgICAgICAgICByZXR1cm4gJ0FjdGl2ZSAtIFJlYWx0aW1lJztcbiAgICAgICAgY2FzZSAnUFJFX09QRVJBVElPTlMnOlxuICAgICAgICAgICAgcmV0dXJuICdQcmUtT3BlcmF0aW9ucyc7XG4gICAgICAgIGNhc2UgJ0NPTVBMRVRFRCc6XG4gICAgICAgICAgICByZXR1cm4gJ0NvbXBsZXRlZCc7XG4gICAgICAgIGRlZmF1bHQ6XG4gICAgICAgICAgICByZXR1cm4gJ1Vua25vd24gc3RhdHVzJztcbiAgICB9XG4gIH1cblxufVxuIl19