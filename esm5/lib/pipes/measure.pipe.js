/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Pipe } from '@angular/core';
import { DistanceDecimalPipe } from './distance-decimal.pipe';
import { DistanceUnitPipe } from './distance-unit.pipe';
import { AngleDecimalPipe } from './angle-decimal.pipe';
import { AngleUnitPipe } from './angle-unit.pipe';
import { GravityDecimalPipe } from './gravity-decimal.pipe';
import { GravityUnitPipe } from './gravity-unit.pipe';
import { MagneticDecimalPipe } from './magnetic-decimal.pipe';
import { MagneticUnitPipe } from './magnetic-unit.pipe';
var MeasurePipe = /** @class */ (function () {
    function MeasurePipe() {
    }
    /**
     * @param {?} measure
     * @param {?=} displayUnit
     * @return {?}
     */
    MeasurePipe.prototype.transform = /**
     * @param {?} measure
     * @param {?=} displayUnit
     * @return {?}
     */
    function (measure, displayUnit) {
        /** @type {?} */
        var value;
        /** @type {?} */
        var unit;
        // default: display units
        if (displayUnit === undefined) {
            displayUnit = true;
        }
        // Check if measure is undefined or unit is undefined
        if (measure === undefined || measure.unit === undefined) {
            return 'Unsupported Input for Measure Filter';
        }
        switch (measure.unit) {
            // Old Format
            case 'METER':
            case 'KILOMETER':
            case 'FOOT':
            case 'FOOT_US':
            case 'YARD':
            case 'MILE':
            // New Format
            case 'Meters':
            case 'Kilometers':
            case 'Feet':
            case 'ft US':
            case 'U.S. Survey Feet':
            case 'Yards':
            case 'Miles':
                value = new DistanceDecimalPipe().transform(measure.value, measure.unit);
                unit = ' ' + new DistanceUnitPipe().transform(measure.unit);
                break;
            // Old Format
            case 'DEGREE':
            case 'RADIAN':
            case 'GRADIAN':
            // New Format
            case 'Degrees':
            case '°':
            case 'Radians':
            case 'rad':
            case 'Gradians':
            case 'grad':
                value = new AngleDecimalPipe().transform(measure.value, measure.unit);
                unit = ' ' + new AngleUnitPipe().transform(measure.unit);
                break;
            // Old Format
            case 'M_PER_SEC_SQ':
            case 'FT_PER_SEC_SQ':
            case 'G':
            case 'MILLI_G':
            case 'G_98':
            case 'GAL':
            case 'MILLI_GAL':
            // New Format
            case 'Meters per Second Sq.':
            case 'Feet per Second Sq.':
            case 'm/s²':
            case 'f/s²':
            case 'mG':
            case 'G (9.8)':
            case 'Gal':
            case 'mGal':
                value = new GravityDecimalPipe().transform(measure.value, measure.unit);
                unit = ' ' + new GravityUnitPipe().transform(measure.unit);
                break;
            // Old Format
            case 'NANOTESLA':
            case 'MICROTESLA':
            case 'MILLITESLA':
            case 'TESLA':
            case 'GAUSS':
            case 'GAMMA':
            // New Format
            case 'Nanotesla':
            case 'nT':
            case 'Microtesla':
            case 'µT':
            case 'Millitesla':
            case 'mT':
            case 'Tesla':
            case 'T':
            case 'Gauss':
            case 'Gs':
            case 'Gamma':
            case 'γ':
                value = new MagneticDecimalPipe().transform(measure.value, measure.unit);
                unit = ' ' + new MagneticUnitPipe().transform(measure.unit);
                break;
            default:
                return 'Unsupported Unit: ' + measure.unit;
        }
        // if we are displaying units
        if (displayUnit) {
            return value + unit;
        }
        else { // if we aren't displaying units, just give the value
            return value;
        }
    };
    MeasurePipe.decorators = [
        { type: Pipe, args: [{
                    name: 'measure'
                },] }
    ];
    return MeasurePipe;
}());
export { MeasurePipe };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWVhc3VyZS5waXBlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGhwdGVjaC9zYXBoaXJhLXNoYXJlZC1saWIvIiwic291cmNlcyI6WyJsaWIvcGlwZXMvbWVhc3VyZS5waXBlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsSUFBSSxFQUFpQixNQUFNLGVBQWUsQ0FBQztBQUNwRCxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSx5QkFBeUIsQ0FBQztBQUM5RCxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUN4RCxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUN4RCxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sbUJBQW1CLENBQUM7QUFDbEQsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0sd0JBQXdCLENBQUM7QUFDNUQsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLHFCQUFxQixDQUFDO0FBQ3RELE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLHlCQUF5QixDQUFDO0FBQzlELE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBRXhEO0lBQUE7SUF5R0EsQ0FBQzs7Ozs7O0lBbkdDLCtCQUFTOzs7OztJQUFULFVBQVUsT0FBWSxFQUFFLFdBQWlCOztZQUNuQyxLQUFLOztZQUNMLElBQUk7UUFFUix5QkFBeUI7UUFDekIsSUFBSSxXQUFXLEtBQUssU0FBUyxFQUFFO1lBQzdCLFdBQVcsR0FBRyxJQUFJLENBQUM7U0FDcEI7UUFDRCxxREFBcUQ7UUFDckQsSUFBSSxPQUFPLEtBQUssU0FBUyxJQUFJLE9BQU8sQ0FBQyxJQUFJLEtBQUssU0FBUyxFQUFFO1lBQ3ZELE9BQU8sc0NBQXNDLENBQUM7U0FDL0M7UUFDRCxRQUFRLE9BQU8sQ0FBQyxJQUFJLEVBQUU7WUFDcEIsYUFBYTtZQUNiLEtBQUssT0FBTyxDQUFDO1lBQ2IsS0FBSyxXQUFXLENBQUM7WUFDakIsS0FBSyxNQUFNLENBQUM7WUFDWixLQUFLLFNBQVMsQ0FBQztZQUNmLEtBQUssTUFBTSxDQUFDO1lBQ1osS0FBSyxNQUFNLENBQUM7WUFDWixhQUFhO1lBQ2IsS0FBSyxRQUFRLENBQUM7WUFDZCxLQUFLLFlBQVksQ0FBQztZQUNsQixLQUFLLE1BQU0sQ0FBQztZQUNaLEtBQUssT0FBTyxDQUFDO1lBQ2IsS0FBSyxrQkFBa0IsQ0FBQztZQUN4QixLQUFLLE9BQU8sQ0FBQztZQUNiLEtBQUssT0FBTztnQkFDVixLQUFLLEdBQUcsSUFBSSxtQkFBbUIsRUFBRSxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsS0FBSyxFQUFFLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDekUsSUFBSSxHQUFHLEdBQUcsR0FBRyxJQUFJLGdCQUFnQixFQUFFLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDNUQsTUFBTTtZQUNSLGFBQWE7WUFDYixLQUFLLFFBQVEsQ0FBQztZQUNkLEtBQUssUUFBUSxDQUFDO1lBQ2QsS0FBSyxTQUFTLENBQUM7WUFDZixhQUFhO1lBQ2IsS0FBSyxTQUFTLENBQUM7WUFDZixLQUFLLEdBQUcsQ0FBQztZQUNULEtBQUssU0FBUyxDQUFDO1lBQ2YsS0FBSyxLQUFLLENBQUM7WUFDWCxLQUFLLFVBQVUsQ0FBQztZQUNoQixLQUFLLE1BQU07Z0JBQ1QsS0FBSyxHQUFHLElBQUksZ0JBQWdCLEVBQUUsQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLEtBQUssRUFBRSxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ3RFLElBQUksR0FBRyxHQUFHLEdBQUcsSUFBSSxhQUFhLEVBQUUsQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUN6RCxNQUFNO1lBQ1IsYUFBYTtZQUNiLEtBQUssY0FBYyxDQUFDO1lBQ3BCLEtBQUssZUFBZSxDQUFDO1lBQ3JCLEtBQUssR0FBRyxDQUFDO1lBQ1QsS0FBSyxTQUFTLENBQUM7WUFDZixLQUFLLE1BQU0sQ0FBQztZQUNaLEtBQUssS0FBSyxDQUFDO1lBQ1gsS0FBSyxXQUFXLENBQUM7WUFDakIsYUFBYTtZQUNiLEtBQUssdUJBQXVCLENBQUM7WUFDN0IsS0FBSyxxQkFBcUIsQ0FBQztZQUMzQixLQUFLLE1BQU0sQ0FBQztZQUNaLEtBQUssTUFBTSxDQUFDO1lBQ1osS0FBSyxJQUFJLENBQUM7WUFDVixLQUFLLFNBQVMsQ0FBQztZQUNmLEtBQUssS0FBSyxDQUFDO1lBQ1gsS0FBSyxNQUFNO2dCQUNULEtBQUssR0FBRyxJQUFJLGtCQUFrQixFQUFFLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxLQUFLLEVBQUUsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUN4RSxJQUFJLEdBQUcsR0FBRyxHQUFHLElBQUksZUFBZSxFQUFFLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDM0QsTUFBTTtZQUNSLGFBQWE7WUFDYixLQUFLLFdBQVcsQ0FBQztZQUNqQixLQUFLLFlBQVksQ0FBQztZQUNsQixLQUFLLFlBQVksQ0FBQztZQUNsQixLQUFLLE9BQU8sQ0FBQztZQUNiLEtBQUssT0FBTyxDQUFDO1lBQ2IsS0FBSyxPQUFPLENBQUM7WUFDYixhQUFhO1lBQ2IsS0FBSyxXQUFXLENBQUM7WUFDakIsS0FBSyxJQUFJLENBQUM7WUFDVixLQUFLLFlBQVksQ0FBQztZQUNsQixLQUFLLElBQUksQ0FBQztZQUNWLEtBQUssWUFBWSxDQUFDO1lBQ2xCLEtBQUssSUFBSSxDQUFDO1lBQ1YsS0FBSyxPQUFPLENBQUM7WUFDYixLQUFLLEdBQUcsQ0FBQztZQUNULEtBQUssT0FBTyxDQUFDO1lBQ2IsS0FBSyxJQUFJLENBQUM7WUFDVixLQUFLLE9BQU8sQ0FBQztZQUNiLEtBQUssR0FBRztnQkFDTixLQUFLLEdBQUcsSUFBSSxtQkFBbUIsRUFBRSxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsS0FBSyxFQUFFLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDekUsSUFBSSxHQUFHLEdBQUcsR0FBRyxJQUFJLGdCQUFnQixFQUFFLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDNUQsTUFBTTtZQUNSO2dCQUNFLE9BQU8sb0JBQW9CLEdBQUcsT0FBTyxDQUFDLElBQUksQ0FBQztTQUM5QztRQUNELDZCQUE2QjtRQUM3QixJQUFJLFdBQVcsRUFBRTtZQUNmLE9BQU8sS0FBSyxHQUFHLElBQUksQ0FBQztTQUNyQjthQUFNLEVBQUUscURBQXFEO1lBQzVELE9BQU8sS0FBSyxDQUFDO1NBQ2Q7SUFDSCxDQUFDOztnQkF2R0YsSUFBSSxTQUFDO29CQUNKLElBQUksRUFBRSxTQUFTO2lCQUNoQjs7SUF1R0Qsa0JBQUM7Q0FBQSxBQXpHRCxJQXlHQztTQXJHWSxXQUFXIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgUGlwZSwgUGlwZVRyYW5zZm9ybSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgRGlzdGFuY2VEZWNpbWFsUGlwZSB9IGZyb20gJy4vZGlzdGFuY2UtZGVjaW1hbC5waXBlJztcbmltcG9ydCB7IERpc3RhbmNlVW5pdFBpcGUgfSBmcm9tICcuL2Rpc3RhbmNlLXVuaXQucGlwZSc7XG5pbXBvcnQgeyBBbmdsZURlY2ltYWxQaXBlIH0gZnJvbSAnLi9hbmdsZS1kZWNpbWFsLnBpcGUnO1xuaW1wb3J0IHsgQW5nbGVVbml0UGlwZSB9IGZyb20gJy4vYW5nbGUtdW5pdC5waXBlJztcbmltcG9ydCB7IEdyYXZpdHlEZWNpbWFsUGlwZSB9IGZyb20gJy4vZ3Jhdml0eS1kZWNpbWFsLnBpcGUnO1xuaW1wb3J0IHsgR3Jhdml0eVVuaXRQaXBlIH0gZnJvbSAnLi9ncmF2aXR5LXVuaXQucGlwZSc7XG5pbXBvcnQgeyBNYWduZXRpY0RlY2ltYWxQaXBlIH0gZnJvbSAnLi9tYWduZXRpYy1kZWNpbWFsLnBpcGUnO1xuaW1wb3J0IHsgTWFnbmV0aWNVbml0UGlwZSB9IGZyb20gJy4vbWFnbmV0aWMtdW5pdC5waXBlJztcblxuQFBpcGUoe1xuICBuYW1lOiAnbWVhc3VyZSdcbn0pXG4vLyBSZXR1cm5zIGEgc3RyaW5nIHJlcHJlc2VudGF0aW9uIG9mIHRoZSBtZWFzdXJlIHdpdGggcHJvcGVyIGRlY2ltYWwgcGxhY2VzIGFuZCBmb3JtYXR0aW5nLlxuZXhwb3J0IGNsYXNzIE1lYXN1cmVQaXBlIGltcGxlbWVudHMgUGlwZVRyYW5zZm9ybSB7XG5cbiAgdHJhbnNmb3JtKG1lYXN1cmU6IGFueSwgZGlzcGxheVVuaXQ/OiBhbnkpOiBhbnkge1xuICAgIGxldCB2YWx1ZTtcbiAgICBsZXQgdW5pdDtcblxuICAgIC8vIGRlZmF1bHQ6IGRpc3BsYXkgdW5pdHNcbiAgICBpZiAoZGlzcGxheVVuaXQgPT09IHVuZGVmaW5lZCkge1xuICAgICAgZGlzcGxheVVuaXQgPSB0cnVlO1xuICAgIH1cbiAgICAvLyBDaGVjayBpZiBtZWFzdXJlIGlzIHVuZGVmaW5lZCBvciB1bml0IGlzIHVuZGVmaW5lZFxuICAgIGlmIChtZWFzdXJlID09PSB1bmRlZmluZWQgfHwgbWVhc3VyZS51bml0ID09PSB1bmRlZmluZWQpIHtcbiAgICAgIHJldHVybiAnVW5zdXBwb3J0ZWQgSW5wdXQgZm9yIE1lYXN1cmUgRmlsdGVyJztcbiAgICB9XG4gICAgc3dpdGNoIChtZWFzdXJlLnVuaXQpIHtcbiAgICAgIC8vIE9sZCBGb3JtYXRcbiAgICAgIGNhc2UgJ01FVEVSJzpcbiAgICAgIGNhc2UgJ0tJTE9NRVRFUic6XG4gICAgICBjYXNlICdGT09UJzpcbiAgICAgIGNhc2UgJ0ZPT1RfVVMnOlxuICAgICAgY2FzZSAnWUFSRCc6XG4gICAgICBjYXNlICdNSUxFJzpcbiAgICAgIC8vIE5ldyBGb3JtYXRcbiAgICAgIGNhc2UgJ01ldGVycyc6XG4gICAgICBjYXNlICdLaWxvbWV0ZXJzJzpcbiAgICAgIGNhc2UgJ0ZlZXQnOlxuICAgICAgY2FzZSAnZnQgVVMnOlxuICAgICAgY2FzZSAnVS5TLiBTdXJ2ZXkgRmVldCc6XG4gICAgICBjYXNlICdZYXJkcyc6XG4gICAgICBjYXNlICdNaWxlcyc6XG4gICAgICAgIHZhbHVlID0gbmV3IERpc3RhbmNlRGVjaW1hbFBpcGUoKS50cmFuc2Zvcm0obWVhc3VyZS52YWx1ZSwgbWVhc3VyZS51bml0KTtcbiAgICAgICAgdW5pdCA9ICcgJyArIG5ldyBEaXN0YW5jZVVuaXRQaXBlKCkudHJhbnNmb3JtKG1lYXN1cmUudW5pdCk7XG4gICAgICAgIGJyZWFrO1xuICAgICAgLy8gT2xkIEZvcm1hdFxuICAgICAgY2FzZSAnREVHUkVFJzpcbiAgICAgIGNhc2UgJ1JBRElBTic6XG4gICAgICBjYXNlICdHUkFESUFOJzpcbiAgICAgIC8vIE5ldyBGb3JtYXRcbiAgICAgIGNhc2UgJ0RlZ3JlZXMnOlxuICAgICAgY2FzZSAnwrAnOlxuICAgICAgY2FzZSAnUmFkaWFucyc6XG4gICAgICBjYXNlICdyYWQnOlxuICAgICAgY2FzZSAnR3JhZGlhbnMnOlxuICAgICAgY2FzZSAnZ3JhZCc6XG4gICAgICAgIHZhbHVlID0gbmV3IEFuZ2xlRGVjaW1hbFBpcGUoKS50cmFuc2Zvcm0obWVhc3VyZS52YWx1ZSwgbWVhc3VyZS51bml0KTtcbiAgICAgICAgdW5pdCA9ICcgJyArIG5ldyBBbmdsZVVuaXRQaXBlKCkudHJhbnNmb3JtKG1lYXN1cmUudW5pdCk7XG4gICAgICAgIGJyZWFrO1xuICAgICAgLy8gT2xkIEZvcm1hdFxuICAgICAgY2FzZSAnTV9QRVJfU0VDX1NRJzpcbiAgICAgIGNhc2UgJ0ZUX1BFUl9TRUNfU1EnOlxuICAgICAgY2FzZSAnRyc6XG4gICAgICBjYXNlICdNSUxMSV9HJzpcbiAgICAgIGNhc2UgJ0dfOTgnOlxuICAgICAgY2FzZSAnR0FMJzpcbiAgICAgIGNhc2UgJ01JTExJX0dBTCc6XG4gICAgICAvLyBOZXcgRm9ybWF0XG4gICAgICBjYXNlICdNZXRlcnMgcGVyIFNlY29uZCBTcS4nOlxuICAgICAgY2FzZSAnRmVldCBwZXIgU2Vjb25kIFNxLic6XG4gICAgICBjYXNlICdtL3PCsic6XG4gICAgICBjYXNlICdmL3PCsic6XG4gICAgICBjYXNlICdtRyc6XG4gICAgICBjYXNlICdHICg5LjgpJzpcbiAgICAgIGNhc2UgJ0dhbCc6XG4gICAgICBjYXNlICdtR2FsJzpcbiAgICAgICAgdmFsdWUgPSBuZXcgR3Jhdml0eURlY2ltYWxQaXBlKCkudHJhbnNmb3JtKG1lYXN1cmUudmFsdWUsIG1lYXN1cmUudW5pdCk7XG4gICAgICAgIHVuaXQgPSAnICcgKyBuZXcgR3Jhdml0eVVuaXRQaXBlKCkudHJhbnNmb3JtKG1lYXN1cmUudW5pdCk7XG4gICAgICAgIGJyZWFrO1xuICAgICAgLy8gT2xkIEZvcm1hdFxuICAgICAgY2FzZSAnTkFOT1RFU0xBJzpcbiAgICAgIGNhc2UgJ01JQ1JPVEVTTEEnOlxuICAgICAgY2FzZSAnTUlMTElURVNMQSc6XG4gICAgICBjYXNlICdURVNMQSc6XG4gICAgICBjYXNlICdHQVVTUyc6XG4gICAgICBjYXNlICdHQU1NQSc6XG4gICAgICAvLyBOZXcgRm9ybWF0XG4gICAgICBjYXNlICdOYW5vdGVzbGEnOlxuICAgICAgY2FzZSAnblQnOlxuICAgICAgY2FzZSAnTWljcm90ZXNsYSc6XG4gICAgICBjYXNlICfCtVQnOlxuICAgICAgY2FzZSAnTWlsbGl0ZXNsYSc6XG4gICAgICBjYXNlICdtVCc6XG4gICAgICBjYXNlICdUZXNsYSc6XG4gICAgICBjYXNlICdUJzpcbiAgICAgIGNhc2UgJ0dhdXNzJzpcbiAgICAgIGNhc2UgJ0dzJzpcbiAgICAgIGNhc2UgJ0dhbW1hJzpcbiAgICAgIGNhc2UgJ86zJzpcbiAgICAgICAgdmFsdWUgPSBuZXcgTWFnbmV0aWNEZWNpbWFsUGlwZSgpLnRyYW5zZm9ybShtZWFzdXJlLnZhbHVlLCBtZWFzdXJlLnVuaXQpO1xuICAgICAgICB1bml0ID0gJyAnICsgbmV3IE1hZ25ldGljVW5pdFBpcGUoKS50cmFuc2Zvcm0obWVhc3VyZS51bml0KTtcbiAgICAgICAgYnJlYWs7XG4gICAgICBkZWZhdWx0OlxuICAgICAgICByZXR1cm4gJ1Vuc3VwcG9ydGVkIFVuaXQ6ICcgKyBtZWFzdXJlLnVuaXQ7XG4gICAgfVxuICAgIC8vIGlmIHdlIGFyZSBkaXNwbGF5aW5nIHVuaXRzXG4gICAgaWYgKGRpc3BsYXlVbml0KSB7XG4gICAgICByZXR1cm4gdmFsdWUgKyB1bml0O1xuICAgIH0gZWxzZSB7IC8vIGlmIHdlIGFyZW4ndCBkaXNwbGF5aW5nIHVuaXRzLCBqdXN0IGdpdmUgdGhlIHZhbHVlXG4gICAgICByZXR1cm4gdmFsdWU7XG4gICAgfVxuICB9XG5cbn1cbiJdfQ==