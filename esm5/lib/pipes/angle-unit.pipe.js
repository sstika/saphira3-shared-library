/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Pipe } from '@angular/core';
var AngleUnitPipe = /** @class */ (function () {
    function AngleUnitPipe() {
    }
    /**
     * @param {?} unit
     * @param {?=} full
     * @return {?}
     */
    AngleUnitPipe.prototype.transform = /**
     * @param {?} unit
     * @param {?=} full
     * @return {?}
     */
    function (unit, full) {
        /** @type {?} */
        var string = '';
        switch (unit) {
            case 'DEGREE':
                string = (full === true ? 'Degrees' : decodeURI('%C2%B0'));
                break;
            case 'RADIAN':
                string = (full === true ? 'Radians' : 'rad');
                break;
            case 'GRADIAN':
                string = (full === true ? 'Gradians' : 'grad');
                break;
            default:
                string = unit;
                break;
        }
        return string;
    };
    AngleUnitPipe.decorators = [
        { type: Pipe, args: [{
                    name: 'angleUnit'
                },] }
    ];
    return AngleUnitPipe;
}());
export { AngleUnitPipe };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYW5nbGUtdW5pdC5waXBlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGhwdGVjaC9zYXBoaXJhLXNoYXJlZC1saWIvIiwic291cmNlcyI6WyJsaWIvcGlwZXMvYW5nbGUtdW5pdC5waXBlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsSUFBSSxFQUFpQixNQUFNLGVBQWUsQ0FBQztBQUVwRDtJQUFBO0lBdUJBLENBQUM7Ozs7OztJQWxCQyxpQ0FBUzs7Ozs7SUFBVCxVQUFVLElBQVMsRUFBRSxJQUFVOztZQUN6QixNQUFNLEdBQUcsRUFBRTtRQUNmLFFBQVEsSUFBSSxFQUFFO1lBQ1osS0FBSyxRQUFRO2dCQUNYLE1BQU0sR0FBRyxDQUFDLElBQUksS0FBSyxJQUFJLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7Z0JBQzNELE1BQU07WUFDUixLQUFLLFFBQVE7Z0JBQ1gsTUFBTSxHQUFHLENBQUMsSUFBSSxLQUFLLElBQUksQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDN0MsTUFBTTtZQUNSLEtBQUssU0FBUztnQkFDWixNQUFNLEdBQUcsQ0FBQyxJQUFJLEtBQUssSUFBSSxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDO2dCQUMvQyxNQUFNO1lBQ1I7Z0JBQ0UsTUFBTSxHQUFHLElBQUksQ0FBQztnQkFDZCxNQUFNO1NBQ1Q7UUFDRCxPQUFPLE1BQU0sQ0FBQztJQUNoQixDQUFDOztnQkF0QkYsSUFBSSxTQUFDO29CQUNKLElBQUksRUFBRSxXQUFXO2lCQUNsQjs7SUFxQkQsb0JBQUM7Q0FBQSxBQXZCRCxJQXVCQztTQXBCWSxhQUFhIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgUGlwZSwgUGlwZVRyYW5zZm9ybSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5AUGlwZSh7XG4gIG5hbWU6ICdhbmdsZVVuaXQnXG59KVxuZXhwb3J0IGNsYXNzIEFuZ2xlVW5pdFBpcGUgaW1wbGVtZW50cyBQaXBlVHJhbnNmb3JtIHtcblxuICB0cmFuc2Zvcm0odW5pdDogYW55LCBmdWxsPzogYW55KTogYW55IHtcbiAgICBsZXQgc3RyaW5nID0gJyc7XG4gICAgc3dpdGNoICh1bml0KSB7XG4gICAgICBjYXNlICdERUdSRUUnOlxuICAgICAgICBzdHJpbmcgPSAoZnVsbCA9PT0gdHJ1ZSA/ICdEZWdyZWVzJyA6IGRlY29kZVVSSSgnJUMyJUIwJykpO1xuICAgICAgICBicmVhaztcbiAgICAgIGNhc2UgJ1JBRElBTic6XG4gICAgICAgIHN0cmluZyA9IChmdWxsID09PSB0cnVlID8gJ1JhZGlhbnMnIDogJ3JhZCcpO1xuICAgICAgICBicmVhaztcbiAgICAgIGNhc2UgJ0dSQURJQU4nOlxuICAgICAgICBzdHJpbmcgPSAoZnVsbCA9PT0gdHJ1ZSA/ICdHcmFkaWFucycgOiAnZ3JhZCcpO1xuICAgICAgICBicmVhaztcbiAgICAgIGRlZmF1bHQ6XG4gICAgICAgIHN0cmluZyA9IHVuaXQ7XG4gICAgICAgIGJyZWFrO1xuICAgIH1cbiAgICByZXR1cm4gc3RyaW5nO1xuICB9XG59XG4iXX0=