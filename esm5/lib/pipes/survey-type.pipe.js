/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Pipe } from '@angular/core';
var SurveyTypePipe = /** @class */ (function () {
    function SurveyTypePipe() {
        this.surveyTypes = {
            'STANDARD': {
                definitive: true,
                interpolated: false,
                acronym: 'D-S'
            },
            'POOR': {
                definitive: true,
                interpolated: false,
                acronym: 'D-P'
            },
            'DEFINITIVE_INTERPOLATED': {
                definitive: true,
                interpolated: true,
                acronym: 'D-I'
            },
            'BAD': {
                definitive: false,
                interpolated: false,
                acronym: 'X-B'
            },
            'ACC_CHECK': {
                definitive: false,
                interpolated: false,
                acronym: 'X-A'
            },
            'CHECKSHOT': {
                definitive: false,
                interpolated: false,
                acronym: 'X-C'
            },
            'INTERPOLATED': {
                definitive: false,
                interpolated: false,
                acronym: 'X-I'
            }
        };
    }
    /**
     * @param {?} value
     * @return {?}
     */
    SurveyTypePipe.prototype.transform = /**
     * @param {?} value
     * @return {?}
     */
    function (value) {
        if (this.surveyTypes[value]) {
            return this.surveyTypes[value].acronym;
        }
        else {
            return 'Unsupported Type';
        }
    };
    SurveyTypePipe.decorators = [
        { type: Pipe, args: [{
                    name: 'surveyType'
                },] }
    ];
    return SurveyTypePipe;
}());
export { SurveyTypePipe };
if (false) {
    /** @type {?} */
    SurveyTypePipe.prototype.surveyTypes;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3VydmV5LXR5cGUucGlwZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BocHRlY2gvc2FwaGlyYS1zaGFyZWQtbGliLyIsInNvdXJjZXMiOlsibGliL3BpcGVzL3N1cnZleS10eXBlLnBpcGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxJQUFJLEVBQWlCLE1BQU0sZUFBZSxDQUFDO0FBRXBEO0lBQUE7UUFLRSxnQkFBVyxHQUFHO1lBQ1osVUFBVSxFQUFFO2dCQUNWLFVBQVUsRUFBRSxJQUFJO2dCQUNoQixZQUFZLEVBQUUsS0FBSztnQkFDbkIsT0FBTyxFQUFFLEtBQUs7YUFDZjtZQUNELE1BQU0sRUFBRTtnQkFDTixVQUFVLEVBQUUsSUFBSTtnQkFDaEIsWUFBWSxFQUFFLEtBQUs7Z0JBQ25CLE9BQU8sRUFBRSxLQUFLO2FBQ2Y7WUFDRCx5QkFBeUIsRUFBRTtnQkFDekIsVUFBVSxFQUFFLElBQUk7Z0JBQ2hCLFlBQVksRUFBRSxJQUFJO2dCQUNsQixPQUFPLEVBQUUsS0FBSzthQUNmO1lBQ0QsS0FBSyxFQUFFO2dCQUNMLFVBQVUsRUFBRSxLQUFLO2dCQUNqQixZQUFZLEVBQUUsS0FBSztnQkFDbkIsT0FBTyxFQUFFLEtBQUs7YUFDZjtZQUNELFdBQVcsRUFBRTtnQkFDWCxVQUFVLEVBQUUsS0FBSztnQkFDakIsWUFBWSxFQUFFLEtBQUs7Z0JBQ25CLE9BQU8sRUFBRSxLQUFLO2FBQ2Y7WUFDRCxXQUFXLEVBQUU7Z0JBQ1gsVUFBVSxFQUFFLEtBQUs7Z0JBQ2pCLFlBQVksRUFBRSxLQUFLO2dCQUNuQixPQUFPLEVBQUUsS0FBSzthQUNmO1lBQ0QsY0FBYyxFQUFFO2dCQUNkLFVBQVUsRUFBRSxLQUFLO2dCQUNqQixZQUFZLEVBQUUsS0FBSztnQkFDbkIsT0FBTyxFQUFFLEtBQUs7YUFDZjtTQUNGLENBQUM7SUFTSixDQUFDOzs7OztJQVBDLGtDQUFTOzs7O0lBQVQsVUFBVSxLQUFhO1FBQ3JCLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsRUFBRTtZQUMzQixPQUFPLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUMsT0FBTyxDQUFDO1NBQ3hDO2FBQU07WUFDTCxPQUFPLGtCQUFrQixDQUFDO1NBQzNCO0lBQ0gsQ0FBQzs7Z0JBakRGLElBQUksU0FBQztvQkFDSixJQUFJLEVBQUUsWUFBWTtpQkFDbkI7O0lBZ0RELHFCQUFDO0NBQUEsQUFsREQsSUFrREM7U0E5Q1ksY0FBYzs7O0lBQ3pCLHFDQW9DRSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFBpcGUsIFBpcGVUcmFuc2Zvcm0gfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQFBpcGUoe1xuICBuYW1lOiAnc3VydmV5VHlwZSdcbn0pXG4vLyBDb252ZXJ0cyBiZXR3ZWVuIHN1cnZleSB0eXBlcyBhbmQgYWJicmV2aWF0aW9ucy5cbmV4cG9ydCBjbGFzcyBTdXJ2ZXlUeXBlUGlwZSBpbXBsZW1lbnRzIFBpcGVUcmFuc2Zvcm0ge1xuICBzdXJ2ZXlUeXBlcyA9IHtcbiAgICAnU1RBTkRBUkQnOiB7XG4gICAgICBkZWZpbml0aXZlOiB0cnVlLFxuICAgICAgaW50ZXJwb2xhdGVkOiBmYWxzZSxcbiAgICAgIGFjcm9ueW06ICdELVMnXG4gICAgfSxcbiAgICAnUE9PUic6IHtcbiAgICAgIGRlZmluaXRpdmU6IHRydWUsXG4gICAgICBpbnRlcnBvbGF0ZWQ6IGZhbHNlLFxuICAgICAgYWNyb255bTogJ0QtUCdcbiAgICB9LFxuICAgICdERUZJTklUSVZFX0lOVEVSUE9MQVRFRCc6IHtcbiAgICAgIGRlZmluaXRpdmU6IHRydWUsXG4gICAgICBpbnRlcnBvbGF0ZWQ6IHRydWUsXG4gICAgICBhY3JvbnltOiAnRC1JJ1xuICAgIH0sXG4gICAgJ0JBRCc6IHtcbiAgICAgIGRlZmluaXRpdmU6IGZhbHNlLFxuICAgICAgaW50ZXJwb2xhdGVkOiBmYWxzZSxcbiAgICAgIGFjcm9ueW06ICdYLUInXG4gICAgfSxcbiAgICAnQUNDX0NIRUNLJzoge1xuICAgICAgZGVmaW5pdGl2ZTogZmFsc2UsXG4gICAgICBpbnRlcnBvbGF0ZWQ6IGZhbHNlLFxuICAgICAgYWNyb255bTogJ1gtQSdcbiAgICB9LFxuICAgICdDSEVDS1NIT1QnOiB7XG4gICAgICBkZWZpbml0aXZlOiBmYWxzZSxcbiAgICAgIGludGVycG9sYXRlZDogZmFsc2UsXG4gICAgICBhY3JvbnltOiAnWC1DJ1xuICAgIH0sXG4gICAgJ0lOVEVSUE9MQVRFRCc6IHtcbiAgICAgIGRlZmluaXRpdmU6IGZhbHNlLFxuICAgICAgaW50ZXJwb2xhdGVkOiBmYWxzZSxcbiAgICAgIGFjcm9ueW06ICdYLUknXG4gICAgfVxuICB9O1xuXG4gIHRyYW5zZm9ybSh2YWx1ZTogc3RyaW5nKTogYW55IHtcbiAgICBpZiAodGhpcy5zdXJ2ZXlUeXBlc1t2YWx1ZV0pIHtcbiAgICAgIHJldHVybiB0aGlzLnN1cnZleVR5cGVzW3ZhbHVlXS5hY3JvbnltO1xuICAgIH0gZWxzZSB7XG4gICAgICByZXR1cm4gJ1Vuc3VwcG9ydGVkIFR5cGUnO1xuICAgIH1cbiAgfVxufVxuIl19