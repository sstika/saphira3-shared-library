/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Pipe } from '@angular/core';
var DistanceDecimalPipe = /** @class */ (function () {
    function DistanceDecimalPipe() {
        // number of decimal places to display for various distance units.
        this.decimalMap = {
            // Old Format
            METER: 2,
            KILOMETER: 1,
            FOOT: 2,
            FOOT_US: 2,
            YARD: 2,
            MILE: 1,
            // New Format
            Meters: 2,
            Kilometers: 1,
            Feet: 2,
            'ft US': 2,
            'U.S. Survey Feet': 2,
            Yards: 2,
            Miles: 1,
        };
    }
    /**
     * @param {?} value
     * @param {?=} unit
     * @return {?}
     */
    DistanceDecimalPipe.prototype.transform = /**
     * @param {?} value
     * @param {?=} unit
     * @return {?}
     */
    function (value, unit) {
        if (!this.isValidFloat(value)) {
            return 'N/A';
        }
        else {
            value = parseFloat(value);
            if (typeof (this.decimalMap[unit]) !== 'undefined') {
                return this.formatNumber(value.toFixed(this.decimalMap[unit]));
            }
            else {
                return 'Unsupported Unit: ' + unit;
            }
        }
    };
    /**
     * isValidFloat:
     *
     * A stricter float filter than parseFloat().
     *
     * @param value Input value to be parsed as float
     * @returns float
     */
    /**
     * isValidFloat:
     *
     * A stricter float filter than parseFloat().
     *
     * @param {?} value Input value to be parsed as float
     * @return {?} float
     */
    DistanceDecimalPipe.prototype.isValidFloat = /**
     * isValidFloat:
     *
     * A stricter float filter than parseFloat().
     *
     * @param {?} value Input value to be parsed as float
     * @return {?} float
     */
    function (value) {
        return !isNaN(parseFloat(value));
    };
    /**
      * formatNumber:
      *
      * Adds commas and removes the negative sign for zero values.
      *
      * @param number Input value to be formatted
      */
    /**
     * formatNumber:
     *
     * Adds commas and removes the negative sign for zero values.
     *
     * @param {?} number Input value to be formatted
     * @return {?}
     */
    DistanceDecimalPipe.prototype.formatNumber = /**
     * formatNumber:
     *
     * Adds commas and removes the negative sign for zero values.
     *
     * @param {?} number Input value to be formatted
     * @return {?}
     */
    function (number) {
        return this.removeSignOnZero(this.addCommas(number));
    };
    /**
       * removeSignOnZero:
       *
       * If the value is zero, remove any negative signs. (converts -0.00 to 0.00)
       *
       * @param number Input value to be sanitized
       */
    /**
     * removeSignOnZero:
     *
     * If the value is zero, remove any negative signs. (converts -0.00 to 0.00)
     *
     * @param {?} number Input value to be sanitized
     * @return {?}
     */
    DistanceDecimalPipe.prototype.removeSignOnZero = /**
     * removeSignOnZero:
     *
     * If the value is zero, remove any negative signs. (converts -0.00 to 0.00)
     *
     * @param {?} number Input value to be sanitized
     * @return {?}
     */
    function (number) {
        /** @type {?} */
        var split = number.toString().split('-');
        if (split.length === 2) {
            if (split[1] === 0) {
                return split[1];
            }
        }
        return number;
    };
    /**
       * addCommas:
       *
       * Helper function that adds in commas to a number for readability.
       *
       * @param number Input value to add commas
       * @returns string
       */
    /**
     * addCommas:
     *
     * Helper function that adds in commas to a number for readability.
     *
     * @param {?} number Input value to add commas
     * @return {?} string
     */
    DistanceDecimalPipe.prototype.addCommas = /**
     * addCommas:
     *
     * Helper function that adds in commas to a number for readability.
     *
     * @param {?} number Input value to add commas
     * @return {?} string
     */
    function (number) {
        // check if we have a decimal
        /** @type {?} */
        var split = number.toString().split('.');
        // if we have a decimal
        if (split.length === 2) {
            // only add commas to the first half and return the second half
            return split[0].replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,') + '.' + split[1];
        }
        else {
            // add commas throughout
            return split[0].replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
        }
    };
    DistanceDecimalPipe.decorators = [
        { type: Pipe, args: [{
                    name: 'distanceDecimal'
                },] }
    ];
    return DistanceDecimalPipe;
}());
export { DistanceDecimalPipe };
if (false) {
    /** @type {?} */
    DistanceDecimalPipe.prototype.decimalMap;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGlzdGFuY2UtZGVjaW1hbC5waXBlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGhwdGVjaC9zYXBoaXJhLXNoYXJlZC1saWIvIiwic291cmNlcyI6WyJsaWIvcGlwZXMvZGlzdGFuY2UtZGVjaW1hbC5waXBlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsSUFBSSxFQUFpQixNQUFNLGVBQWUsQ0FBQztBQUVwRDtJQUFBOztRQU9FLGVBQVUsR0FBRzs7WUFFWCxLQUFLLEVBQUUsQ0FBQztZQUNSLFNBQVMsRUFBRSxDQUFDO1lBQ1osSUFBSSxFQUFFLENBQUM7WUFDUCxPQUFPLEVBQUUsQ0FBQztZQUNWLElBQUksRUFBRSxDQUFDO1lBQ1AsSUFBSSxFQUFFLENBQUM7O1lBRVAsTUFBTSxFQUFFLENBQUM7WUFDVCxVQUFVLEVBQUUsQ0FBQztZQUNiLElBQUksRUFBRSxDQUFDO1lBQ1AsT0FBTyxFQUFFLENBQUM7WUFDVixrQkFBa0IsRUFBRSxDQUFDO1lBQ3JCLEtBQUssRUFBRSxDQUFDO1lBQ1IsS0FBSyxFQUFFLENBQUM7U0FDVCxDQUFDO0lBMkVKLENBQUM7Ozs7OztJQXpFQyx1Q0FBUzs7Ozs7SUFBVCxVQUFVLEtBQVUsRUFBRSxJQUFVO1FBQzlCLElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssQ0FBQyxFQUFFO1lBQzdCLE9BQU8sS0FBSyxDQUFDO1NBQ2Q7YUFBTTtZQUNMLEtBQUssR0FBRyxVQUFVLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDMUIsSUFBSSxPQUFNLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQyxLQUFLLFdBQVcsRUFBRTtnQkFDakQsT0FBTyxJQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7YUFDaEU7aUJBQU07Z0JBQ0wsT0FBTyxvQkFBb0IsR0FBRyxJQUFJLENBQUM7YUFDcEM7U0FDRjtJQUNILENBQUM7SUFFRDs7Ozs7OztPQU9HOzs7Ozs7Ozs7SUFDSSwwQ0FBWTs7Ozs7Ozs7SUFBbkIsVUFBb0IsS0FBSztRQUN2QixPQUFPLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO0lBQ25DLENBQUM7SUFFRDs7Ozs7O1FBTUk7Ozs7Ozs7OztJQUNHLDBDQUFZOzs7Ozs7OztJQUFuQixVQUFvQixNQUFNO1FBQ3hCLE9BQU8sSUFBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztJQUN2RCxDQUFDO0lBRUQ7Ozs7OztTQU1LOzs7Ozs7Ozs7SUFDRSw4Q0FBZ0I7Ozs7Ozs7O0lBQXZCLFVBQXdCLE1BQU07O1lBQ3RCLEtBQUssR0FBRyxNQUFNLENBQUMsUUFBUSxFQUFFLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQztRQUMxQyxJQUFJLEtBQUssQ0FBQyxNQUFNLEtBQUssQ0FBQyxFQUFFO1lBQ3RCLElBQUksS0FBSyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsRUFBRTtnQkFDbEIsT0FBTyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7YUFDakI7U0FDRjtRQUNELE9BQU8sTUFBTSxDQUFDO0lBQ2hCLENBQUM7SUFFRDs7Ozs7OztTQU9LOzs7Ozs7Ozs7SUFDRSx1Q0FBUzs7Ozs7Ozs7SUFBaEIsVUFBaUIsTUFBTTs7O1lBRWYsS0FBSyxHQUFHLE1BQU0sQ0FBQyxRQUFRLEVBQUUsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDO1FBQzFDLHVCQUF1QjtRQUN2QixJQUFJLEtBQUssQ0FBQyxNQUFNLEtBQUssQ0FBQyxFQUFFO1lBQ3RCLCtEQUErRDtZQUMvRCxPQUFPLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMseUJBQXlCLEVBQUUsS0FBSyxDQUFDLEdBQUcsR0FBRyxHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztTQUM1RTthQUFNO1lBQ0wsd0JBQXdCO1lBQ3hCLE9BQU8sS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyx5QkFBeUIsRUFBRSxLQUFLLENBQUMsQ0FBQztTQUMzRDtJQUNILENBQUM7O2dCQWpHRixJQUFJLFNBQUM7b0JBQ0osSUFBSSxFQUFFLGlCQUFpQjtpQkFDeEI7O0lBZ0dELDBCQUFDO0NBQUEsQUFsR0QsSUFrR0M7U0E3RlksbUJBQW1COzs7SUFFOUIseUNBZ0JFIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgUGlwZSwgUGlwZVRyYW5zZm9ybSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5AUGlwZSh7XG4gIG5hbWU6ICdkaXN0YW5jZURlY2ltYWwnXG59KVxuLy8gUmV0dXJucyB0aGUgZGlzdGFuY2UgYXMgYSBzdHJpbmcgd2l0aCBhbiBhbW91bnQgb2YgZGVjaW1hbCBwbGFjZXMgYmFzZWQgb24gaXRzIHVuaXQuXG4vLyBBbHNvIGZvcm1hdHMgdGhlIHN0cmluZyB3aXRoIGNvbW1hcyBmb3IgcmVhZGFiaWxpdHkuXG5leHBvcnQgY2xhc3MgRGlzdGFuY2VEZWNpbWFsUGlwZSBpbXBsZW1lbnRzIFBpcGVUcmFuc2Zvcm0ge1xuICAvLyBudW1iZXIgb2YgZGVjaW1hbCBwbGFjZXMgdG8gZGlzcGxheSBmb3IgdmFyaW91cyBkaXN0YW5jZSB1bml0cy5cbiAgZGVjaW1hbE1hcCA9IHtcbiAgICAvLyBPbGQgRm9ybWF0XG4gICAgTUVURVI6IDIsXG4gICAgS0lMT01FVEVSOiAxLFxuICAgIEZPT1Q6IDIsXG4gICAgRk9PVF9VUzogMixcbiAgICBZQVJEOiAyLFxuICAgIE1JTEU6IDEsXG4gICAgLy8gTmV3IEZvcm1hdFxuICAgIE1ldGVyczogMixcbiAgICBLaWxvbWV0ZXJzOiAxLFxuICAgIEZlZXQ6IDIsXG4gICAgJ2Z0IFVTJzogMixcbiAgICAnVS5TLiBTdXJ2ZXkgRmVldCc6IDIsXG4gICAgWWFyZHM6IDIsXG4gICAgTWlsZXM6IDEsXG4gIH07XG5cbiAgdHJhbnNmb3JtKHZhbHVlOiBhbnksIHVuaXQ/OiBhbnkpOiBhbnkge1xuICAgIGlmICghdGhpcy5pc1ZhbGlkRmxvYXQodmFsdWUpKSB7XG4gICAgICByZXR1cm4gJ04vQSc7XG4gICAgfSBlbHNlIHtcbiAgICAgIHZhbHVlID0gcGFyc2VGbG9hdCh2YWx1ZSk7XG4gICAgICBpZiAodHlwZW9mKHRoaXMuZGVjaW1hbE1hcFt1bml0XSkgIT09ICd1bmRlZmluZWQnKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmZvcm1hdE51bWJlcih2YWx1ZS50b0ZpeGVkKHRoaXMuZGVjaW1hbE1hcFt1bml0XSkpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgcmV0dXJuICdVbnN1cHBvcnRlZCBVbml0OiAnICsgdW5pdDtcbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICAvKipcbiAgICogaXNWYWxpZEZsb2F0OlxuICAgKlxuICAgKiBBIHN0cmljdGVyIGZsb2F0IGZpbHRlciB0aGFuIHBhcnNlRmxvYXQoKS5cbiAgICpcbiAgICogQHBhcmFtIHZhbHVlIElucHV0IHZhbHVlIHRvIGJlIHBhcnNlZCBhcyBmbG9hdFxuICAgKiBAcmV0dXJucyBmbG9hdFxuICAgKi9cbiAgcHVibGljIGlzVmFsaWRGbG9hdCh2YWx1ZSkge1xuICAgIHJldHVybiAhaXNOYU4ocGFyc2VGbG9hdCh2YWx1ZSkpO1xuICB9XG5cbiAgLyoqXG4gICAgKiBmb3JtYXROdW1iZXI6XG4gICAgKlxuICAgICogQWRkcyBjb21tYXMgYW5kIHJlbW92ZXMgdGhlIG5lZ2F0aXZlIHNpZ24gZm9yIHplcm8gdmFsdWVzLlxuICAgICpcbiAgICAqIEBwYXJhbSBudW1iZXIgSW5wdXQgdmFsdWUgdG8gYmUgZm9ybWF0dGVkXG4gICAgKi9cbiAgcHVibGljIGZvcm1hdE51bWJlcihudW1iZXIpIHtcbiAgICByZXR1cm4gdGhpcy5yZW1vdmVTaWduT25aZXJvKHRoaXMuYWRkQ29tbWFzKG51bWJlcikpO1xuICB9XG5cbiAgLyoqXG4gICAgICogcmVtb3ZlU2lnbk9uWmVybzpcbiAgICAgKlxuICAgICAqIElmIHRoZSB2YWx1ZSBpcyB6ZXJvLCByZW1vdmUgYW55IG5lZ2F0aXZlIHNpZ25zLiAoY29udmVydHMgLTAuMDAgdG8gMC4wMClcbiAgICAgKlxuICAgICAqIEBwYXJhbSBudW1iZXIgSW5wdXQgdmFsdWUgdG8gYmUgc2FuaXRpemVkXG4gICAgICovXG4gIHB1YmxpYyByZW1vdmVTaWduT25aZXJvKG51bWJlcikge1xuICAgIGNvbnN0IHNwbGl0ID0gbnVtYmVyLnRvU3RyaW5nKCkuc3BsaXQoJy0nKTtcbiAgICBpZiAoc3BsaXQubGVuZ3RoID09PSAyKSB7XG4gICAgICBpZiAoc3BsaXRbMV0gPT09IDApIHtcbiAgICAgICAgcmV0dXJuIHNwbGl0WzFdO1xuICAgICAgfVxuICAgIH1cbiAgICByZXR1cm4gbnVtYmVyO1xuICB9XG5cbiAgLyoqXG4gICAgICogYWRkQ29tbWFzOlxuICAgICAqXG4gICAgICogSGVscGVyIGZ1bmN0aW9uIHRoYXQgYWRkcyBpbiBjb21tYXMgdG8gYSBudW1iZXIgZm9yIHJlYWRhYmlsaXR5LlxuICAgICAqXG4gICAgICogQHBhcmFtIG51bWJlciBJbnB1dCB2YWx1ZSB0byBhZGQgY29tbWFzXG4gICAgICogQHJldHVybnMgc3RyaW5nXG4gICAgICovXG4gIHB1YmxpYyBhZGRDb21tYXMobnVtYmVyKSB7XG4gICAgLy8gY2hlY2sgaWYgd2UgaGF2ZSBhIGRlY2ltYWxcbiAgICBjb25zdCBzcGxpdCA9IG51bWJlci50b1N0cmluZygpLnNwbGl0KCcuJyk7XG4gICAgLy8gaWYgd2UgaGF2ZSBhIGRlY2ltYWxcbiAgICBpZiAoc3BsaXQubGVuZ3RoID09PSAyKSB7XG4gICAgICAvLyBvbmx5IGFkZCBjb21tYXMgdG8gdGhlIGZpcnN0IGhhbGYgYW5kIHJldHVybiB0aGUgc2Vjb25kIGhhbGZcbiAgICAgIHJldHVybiBzcGxpdFswXS5yZXBsYWNlKC8oXFxkKSg/PShcXGR7M30pKyg/IVxcZCkpL2csICckMSwnKSArICcuJyArIHNwbGl0WzFdO1xuICAgIH0gZWxzZSB7XG4gICAgICAvLyBhZGQgY29tbWFzIHRocm91Z2hvdXRcbiAgICAgIHJldHVybiBzcGxpdFswXS5yZXBsYWNlKC8oXFxkKSg/PShcXGR7M30pKyg/IVxcZCkpL2csICckMSwnKTtcbiAgICB9XG4gIH1cbn1cbiJdfQ==