/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Pipe } from '@angular/core';
var BooleanOnOffPipe = /** @class */ (function () {
    function BooleanOnOffPipe() {
    }
    /**
     * @param {?} value
     * @return {?}
     */
    BooleanOnOffPipe.prototype.transform = /**
     * @param {?} value
     * @return {?}
     */
    function (value) {
        if (value) {
            return 'On';
        }
        else {
            return 'Off';
        }
    };
    BooleanOnOffPipe.decorators = [
        { type: Pipe, args: [{
                    name: 'booleanOnOff'
                },] }
    ];
    return BooleanOnOffPipe;
}());
export { BooleanOnOffPipe };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYm9vbGVhbi1vbi1vZmYucGlwZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BocHRlY2gvc2FwaGlyYS1zaGFyZWQtbGliLyIsInNvdXJjZXMiOlsibGliL3BpcGVzL2Jvb2xlYW4tb24tb2ZmLnBpcGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxJQUFJLEVBQWlCLE1BQU0sZUFBZSxDQUFDO0FBRXBEO0lBQUE7SUFhQSxDQUFDOzs7OztJQVBDLG9DQUFTOzs7O0lBQVQsVUFBVSxLQUFVO1FBQ2xCLElBQUksS0FBSyxFQUFFO1lBQ1QsT0FBTyxJQUFJLENBQUM7U0FDYjthQUFNO1lBQ0wsT0FBTyxLQUFLLENBQUM7U0FDZDtJQUNILENBQUM7O2dCQVpGLElBQUksU0FBQztvQkFDSixJQUFJLEVBQUUsY0FBYztpQkFDckI7O0lBV0QsdUJBQUM7Q0FBQSxBQWJELElBYUM7U0FUWSxnQkFBZ0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBQaXBlLCBQaXBlVHJhbnNmb3JtIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBQaXBlKHtcbiAgbmFtZTogJ2Jvb2xlYW5Pbk9mZidcbn0pXG4vLyBBIHNpbXBsZSBmaWx0ZXIgdGhhdCBjb252ZXJ0cyAndHJ1ZScgdG8gJ09uJyBhbmQgJ2ZhbHNlJyB0byAnT2ZmJy5cbmV4cG9ydCBjbGFzcyBCb29sZWFuT25PZmZQaXBlIGltcGxlbWVudHMgUGlwZVRyYW5zZm9ybSB7XG5cbiAgdHJhbnNmb3JtKHZhbHVlOiBhbnkpOiBhbnkge1xuICAgIGlmICh2YWx1ZSkge1xuICAgICAgcmV0dXJuICdPbic7XG4gICAgfSBlbHNlIHtcbiAgICAgIHJldHVybiAnT2ZmJztcbiAgICB9XG4gIH1cbn1cbiJdfQ==