/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Pipe } from '@angular/core';
var OrderByPipe = /** @class */ (function () {
    function OrderByPipe() {
    }
    /**
     * @param {?} array
     * @param {?} field
     * @return {?}
     */
    OrderByPipe.prototype.transform = /**
     * @param {?} array
     * @param {?} field
     * @return {?}
     */
    function (array, field) {
        array.sort((/**
         * @param {?} a
         * @param {?} b
         * @return {?}
         */
        function (a, b) {
            if (a[field] < b[field]) {
                return -1;
            }
            else if (a[field] > b[field]) {
                return 1;
            }
            else {
                return 0;
            }
        }));
        return array;
    };
    /**
     * @param {?} array
     * @param {?} field
     * @param {?} topElement
     * @return {?}
     */
    OrderByPipe.prototype.transformSurveySet = /**
     * @param {?} array
     * @param {?} field
     * @param {?} topElement
     * @return {?}
     */
    function (array, field, topElement) {
        array.sort((/**
         * @param {?} a
         * @param {?} b
         * @return {?}
         */
        function (a, b) {
            if (a === topElement || a[field] < b[field]) {
                return -1;
            }
            else if (a[field] > b[field]) {
                return 1;
            }
            else {
                return 0;
            }
        }));
        return array;
    };
    OrderByPipe.decorators = [
        { type: Pipe, args: [{ name: 'orderBy', pure: false },] }
    ];
    return OrderByPipe;
}());
export { OrderByPipe };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib3JkZXItYnkucGlwZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BocHRlY2gvc2FwaGlyYS1zaGFyZWQtbGliLyIsInNvdXJjZXMiOlsibGliL3BpcGVzL29yZGVyLWJ5LnBpcGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxJQUFJLEVBQWlCLE1BQU0sZUFBZSxDQUFDO0FBRXBEO0lBQUE7SUE0QkEsQ0FBQzs7Ozs7O0lBekJDLCtCQUFTOzs7OztJQUFULFVBQVUsS0FBWSxFQUFFLEtBQWE7UUFDbkMsS0FBSyxDQUFDLElBQUk7Ozs7O1FBQUMsVUFBQyxDQUFNLEVBQUUsQ0FBTTtZQUN4QixJQUFJLENBQUMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsS0FBSyxDQUFDLEVBQUU7Z0JBQ3ZCLE9BQU8sQ0FBQyxDQUFDLENBQUM7YUFDWDtpQkFBTSxJQUFJLENBQUMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsS0FBSyxDQUFDLEVBQUU7Z0JBQzlCLE9BQU8sQ0FBQyxDQUFDO2FBQ1Y7aUJBQU07Z0JBQ0wsT0FBTyxDQUFDLENBQUM7YUFDVjtRQUNILENBQUMsRUFBQyxDQUFDO1FBQ0gsT0FBTyxLQUFLLENBQUM7SUFDZixDQUFDOzs7Ozs7O0lBRUQsd0NBQWtCOzs7Ozs7SUFBbEIsVUFBbUIsS0FBWSxFQUFFLEtBQWEsRUFBRSxVQUFrQjtRQUNoRSxLQUFLLENBQUMsSUFBSTs7Ozs7UUFBQyxVQUFDLENBQU0sRUFBRSxDQUFNO1lBQ3hCLElBQUksQ0FBQyxLQUFLLFVBQVUsSUFBSSxDQUFDLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEtBQUssQ0FBQyxFQUFFO2dCQUMzQyxPQUFPLENBQUMsQ0FBQyxDQUFDO2FBQ1g7aUJBQU0sSUFBSSxDQUFDLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEtBQUssQ0FBQyxFQUFFO2dCQUM5QixPQUFPLENBQUMsQ0FBQzthQUNWO2lCQUFNO2dCQUNMLE9BQU8sQ0FBQyxDQUFDO2FBQ1Y7UUFDSCxDQUFDLEVBQUMsQ0FBQztRQUNILE9BQU8sS0FBSyxDQUFDO0lBQ2YsQ0FBQzs7Z0JBM0JGLElBQUksU0FBQyxFQUFDLElBQUksRUFBRSxTQUFTLEVBQUUsSUFBSSxFQUFFLEtBQUssRUFBQzs7SUE0QnBDLGtCQUFDO0NBQUEsQUE1QkQsSUE0QkM7U0EzQlksV0FBVyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFBpcGUsIFBpcGVUcmFuc2Zvcm0gfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQFBpcGUoe25hbWU6ICdvcmRlckJ5JywgcHVyZTogZmFsc2V9KVxuZXhwb3J0IGNsYXNzIE9yZGVyQnlQaXBlIGltcGxlbWVudHMgUGlwZVRyYW5zZm9ybSB7XG5cbiAgdHJhbnNmb3JtKGFycmF5OiBhbnlbXSwgZmllbGQ6IHN0cmluZyk6IGFueVtdIHtcbiAgICBhcnJheS5zb3J0KChhOiBhbnksIGI6IGFueSkgPT4ge1xuICAgICAgaWYgKGFbZmllbGRdIDwgYltmaWVsZF0pIHtcbiAgICAgICAgcmV0dXJuIC0xO1xuICAgICAgfSBlbHNlIGlmIChhW2ZpZWxkXSA+IGJbZmllbGRdKSB7XG4gICAgICAgIHJldHVybiAxO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgcmV0dXJuIDA7XG4gICAgICB9XG4gICAgfSk7XG4gICAgcmV0dXJuIGFycmF5O1xuICB9XG5cbiAgdHJhbnNmb3JtU3VydmV5U2V0KGFycmF5OiBhbnlbXSwgZmllbGQ6IHN0cmluZywgdG9wRWxlbWVudDogc3RyaW5nKTogYW55W10ge1xuICAgIGFycmF5LnNvcnQoKGE6IGFueSwgYjogYW55KSA9PiB7XG4gICAgICBpZiAoYSA9PT0gdG9wRWxlbWVudCB8fCBhW2ZpZWxkXSA8IGJbZmllbGRdKSB7XG4gICAgICAgIHJldHVybiAtMTtcbiAgICAgIH0gZWxzZSBpZiAoYVtmaWVsZF0gPiBiW2ZpZWxkXSkge1xuICAgICAgICByZXR1cm4gMTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHJldHVybiAwO1xuICAgICAgfVxuICAgIH0pO1xuICAgIHJldHVybiBhcnJheTtcbiAgfVxufVxuIl19