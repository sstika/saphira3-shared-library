/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Pipe } from '@angular/core';
var FormatObjectPipe = /** @class */ (function () {
    function FormatObjectPipe() {
    }
    /**
     * @param {?} unit
     * @param {?=} full
     * @return {?}
     */
    FormatObjectPipe.prototype.transform = /**
     * @param {?} unit
     * @param {?=} full
     * @return {?}
     */
    function (unit, full) {
        /** @type {?} */
        var string = '';
        switch (unit) {
            case 'DEGREE':
                string = '°';
                break;
            case 'GRADIAN':
                string = 'gon';
                break;
            case 'RADIAN':
                string = 'rad';
                break;
            case 'NANOTESLA':
                string = 'nT';
                break;
            case 'MICROTESLA':
                string = 'mT';
                break;
            case 'TESLA':
                string = 'T';
                break;
            case 'GAUSS':
                string = 'Gauss';
                break;
            case 'KILOMETER':
                string = 'Kilometers';
                break;
            case 'GAMMA':
                string = 'Gamma';
                break;
            case 'M_PER_SEC_SQ':
                string = 'M Per Sec Sq';
                break;
            case 'FT_PER_SEC_SQ':
                string = 'Ft Per Sec Sq';
                break;
            case 'G':
                string = 'G';
                break;
            case 'MILLI_G':
                string = 'Milli G';
                break;
            case 'G_98':
                string = 'G 98';
                break;
            case 'GAL':
                string = 'Gal';
                break;
            case 'MILLI_GAL':
                string = 'Milli Gal';
                break;
            case 'FOOT':
                string = 'Feet';
                break;
            case 'FOOT_US':
                string = 'ft US';
                break;
            case 'METER':
                string = 'Meters';
                break;
            case 'KILOMETER':
                string = 'Kilometers';
                break;
            case 'MILE':
                string = 'Miles';
                break;
            case 'YARD':
                string = 'Yards';
                break;
            case 'STANDARD':
                string = 'D-S';
                break;
            case 'POOR':
                string = 'D-P';
                break;
            case 'DEFINITIVE_INTERPOLATED':
                string = 'D-I';
                break;
            case 'BAD':
                string = 'X-B';
                break;
            case 'ACC_CHECK':
                string = 'X-A';
                break;
            case 'CHECKSHOT':
                string = 'X-C';
                break;
            case 'INTERPOLATED':
                string = 'X-I';
                break;
            default:
                string = unit;
                break;
        }
        return string;
    };
    FormatObjectPipe.decorators = [
        { type: Pipe, args: [{
                    name: 'formatObject'
                },] }
    ];
    return FormatObjectPipe;
}());
export { FormatObjectPipe };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybWF0LW9iamVjdC5waXBlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGhwdGVjaC9zYXBoaXJhLXNoYXJlZC1saWIvIiwic291cmNlcyI6WyJsaWIvcGlwZXMvZm9ybWF0LW9iamVjdC5waXBlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsSUFBSSxFQUFpQixNQUFNLGVBQWUsQ0FBQztBQUVwRDtJQUFBO0lBb0lBLENBQUM7Ozs7OztJQS9IQyxvQ0FBUzs7Ozs7SUFBVCxVQUFVLElBQVMsRUFBRSxJQUFVOztZQUN6QixNQUFNLEdBQUcsRUFBRTtRQUNmLFFBQVEsSUFBSSxFQUFFO1lBQ1osS0FBSyxRQUFRO2dCQUNYLE1BQU0sR0FBRyxHQUFHLENBQUM7Z0JBQ2IsTUFBTTtZQUNSLEtBQUssU0FBUztnQkFDWixNQUFNLEdBQUcsS0FBSyxDQUFDO2dCQUNmLE1BQU07WUFDUixLQUFLLFFBQVE7Z0JBQ1gsTUFBTSxHQUFHLEtBQUssQ0FBQztnQkFDZixNQUFNO1lBQ1IsS0FBSyxXQUFXO2dCQUNkLE1BQU0sR0FBRyxJQUFJLENBQUM7Z0JBQ2QsTUFBTTtZQUNSLEtBQUssWUFBWTtnQkFDZixNQUFNLEdBQUcsSUFBSSxDQUFDO2dCQUNkLE1BQU07WUFDUixLQUFLLE9BQU87Z0JBQ1YsTUFBTSxHQUFHLEdBQUcsQ0FBQztnQkFDYixNQUFNO1lBQ1IsS0FBSyxPQUFPO2dCQUNWLE1BQU0sR0FBRyxPQUFPLENBQUM7Z0JBQ2pCLE1BQU07WUFDUixLQUFLLFdBQVc7Z0JBQ2QsTUFBTSxHQUFHLFlBQVksQ0FBQztnQkFDdEIsTUFBTTtZQUNSLEtBQUssT0FBTztnQkFDVixNQUFNLEdBQUcsT0FBTyxDQUFDO2dCQUNqQixNQUFNO1lBQ1IsS0FBSyxjQUFjO2dCQUNqQixNQUFNLEdBQUcsY0FBYyxDQUFDO2dCQUN4QixNQUFNO1lBQ1IsS0FBSyxlQUFlO2dCQUNsQixNQUFNLEdBQUcsZUFBZSxDQUFDO2dCQUN6QixNQUFNO1lBQ1IsS0FBSyxHQUFHO2dCQUNOLE1BQU0sR0FBRyxHQUFHLENBQUM7Z0JBQ2IsTUFBTTtZQUNSLEtBQUssU0FBUztnQkFDWixNQUFNLEdBQUcsU0FBUyxDQUFDO2dCQUNuQixNQUFNO1lBQ1IsS0FBSyxNQUFNO2dCQUNULE1BQU0sR0FBRyxNQUFNLENBQUM7Z0JBQ2hCLE1BQU07WUFDUixLQUFLLEtBQUs7Z0JBQ1IsTUFBTSxHQUFHLEtBQUssQ0FBQztnQkFDZixNQUFNO1lBQ1IsS0FBSyxXQUFXO2dCQUNkLE1BQU0sR0FBRyxXQUFXLENBQUM7Z0JBQ3JCLE1BQU07WUFDUixLQUFLLE1BQU07Z0JBQ1QsTUFBTSxHQUFHLE1BQU0sQ0FBQztnQkFDaEIsTUFBTTtZQUNSLEtBQUssU0FBUztnQkFDWixNQUFNLEdBQUcsT0FBTyxDQUFDO2dCQUNqQixNQUFNO1lBQ1IsS0FBSyxPQUFPO2dCQUNWLE1BQU0sR0FBRyxRQUFRLENBQUM7Z0JBQ2xCLE1BQU07WUFDUixLQUFLLFdBQVc7Z0JBQ2QsTUFBTSxHQUFHLFlBQVksQ0FBQztnQkFDdEIsTUFBTTtZQUNSLEtBQUssTUFBTTtnQkFDVCxNQUFNLEdBQUcsT0FBTyxDQUFDO2dCQUNqQixNQUFNO1lBQ1IsS0FBSyxNQUFNO2dCQUNULE1BQU0sR0FBRyxPQUFPLENBQUM7Z0JBQ2pCLE1BQU07WUFDUixLQUFLLFVBQVU7Z0JBQ2IsTUFBTSxHQUFHLEtBQUssQ0FBQztnQkFDZixNQUFNO1lBQ1IsS0FBSyxNQUFNO2dCQUNULE1BQU0sR0FBRyxLQUFLLENBQUM7Z0JBQ2YsTUFBTTtZQUNSLEtBQUsseUJBQXlCO2dCQUM1QixNQUFNLEdBQUcsS0FBSyxDQUFDO2dCQUNmLE1BQU07WUFDUixLQUFLLEtBQUs7Z0JBQ1IsTUFBTSxHQUFHLEtBQUssQ0FBQztnQkFDZixNQUFNO1lBQ1IsS0FBSyxXQUFXO2dCQUNkLE1BQU0sR0FBRyxLQUFLLENBQUM7Z0JBQ2YsTUFBTTtZQUNSLEtBQUssV0FBVztnQkFDZCxNQUFNLEdBQUcsS0FBSyxDQUFDO2dCQUNmLE1BQU07WUFDUixLQUFLLGNBQWM7Z0JBQ2pCLE1BQU0sR0FBRyxLQUFLLENBQUM7Z0JBQ2YsTUFBTTtZQUNSO2dCQUNFLE1BQU0sR0FBRyxJQUFJLENBQUM7Z0JBQ2QsTUFBTTtTQUNUO1FBQ0QsT0FBTyxNQUFNLENBQUM7SUFDaEIsQ0FBQzs7Z0JBcEdGLElBQUksU0FBQztvQkFDSixJQUFJLEVBQUUsY0FBYztpQkFDckI7O0lBa0lELHVCQUFDO0NBQUEsQUFwSUQsSUFvSUM7U0FqSVksZ0JBQWdCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgUGlwZSwgUGlwZVRyYW5zZm9ybSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5AUGlwZSh7XG4gIG5hbWU6ICdmb3JtYXRPYmplY3QnXG59KVxuZXhwb3J0IGNsYXNzIEZvcm1hdE9iamVjdFBpcGUgaW1wbGVtZW50cyBQaXBlVHJhbnNmb3JtIHtcblxuICB0cmFuc2Zvcm0odW5pdDogYW55LCBmdWxsPzogYW55KTogYW55IHtcbiAgICBsZXQgc3RyaW5nID0gJyc7XG4gICAgc3dpdGNoICh1bml0KSB7XG4gICAgICBjYXNlICdERUdSRUUnOlxuICAgICAgICBzdHJpbmcgPSAnwrAnO1xuICAgICAgICBicmVhaztcbiAgICAgIGNhc2UgJ0dSQURJQU4nOlxuICAgICAgICBzdHJpbmcgPSAnZ29uJztcbiAgICAgICAgYnJlYWs7XG4gICAgICBjYXNlICdSQURJQU4nOlxuICAgICAgICBzdHJpbmcgPSAncmFkJztcbiAgICAgICAgYnJlYWs7XG4gICAgICBjYXNlICdOQU5PVEVTTEEnOlxuICAgICAgICBzdHJpbmcgPSAnblQnO1xuICAgICAgICBicmVhaztcbiAgICAgIGNhc2UgJ01JQ1JPVEVTTEEnOlxuICAgICAgICBzdHJpbmcgPSAnbVQnO1xuICAgICAgICBicmVhaztcbiAgICAgIGNhc2UgJ1RFU0xBJzpcbiAgICAgICAgc3RyaW5nID0gJ1QnO1xuICAgICAgICBicmVhaztcbiAgICAgIGNhc2UgJ0dBVVNTJzpcbiAgICAgICAgc3RyaW5nID0gJ0dhdXNzJztcbiAgICAgICAgYnJlYWs7XG4gICAgICBjYXNlICdLSUxPTUVURVInOlxuICAgICAgICBzdHJpbmcgPSAnS2lsb21ldGVycyc7XG4gICAgICAgIGJyZWFrO1xuICAgICAgY2FzZSAnR0FNTUEnOlxuICAgICAgICBzdHJpbmcgPSAnR2FtbWEnO1xuICAgICAgICBicmVhaztcbiAgICAgIGNhc2UgJ01fUEVSX1NFQ19TUSc6XG4gICAgICAgIHN0cmluZyA9ICdNIFBlciBTZWMgU3EnO1xuICAgICAgICBicmVhaztcbiAgICAgIGNhc2UgJ0ZUX1BFUl9TRUNfU1EnOlxuICAgICAgICBzdHJpbmcgPSAnRnQgUGVyIFNlYyBTcSc7XG4gICAgICAgIGJyZWFrO1xuICAgICAgY2FzZSAnRyc6XG4gICAgICAgIHN0cmluZyA9ICdHJztcbiAgICAgICAgYnJlYWs7XG4gICAgICBjYXNlICdNSUxMSV9HJzpcbiAgICAgICAgc3RyaW5nID0gJ01pbGxpIEcnO1xuICAgICAgICBicmVhaztcbiAgICAgIGNhc2UgJ0dfOTgnOlxuICAgICAgICBzdHJpbmcgPSAnRyA5OCc7XG4gICAgICAgIGJyZWFrO1xuICAgICAgY2FzZSAnR0FMJzpcbiAgICAgICAgc3RyaW5nID0gJ0dhbCc7XG4gICAgICAgIGJyZWFrO1xuICAgICAgY2FzZSAnTUlMTElfR0FMJzpcbiAgICAgICAgc3RyaW5nID0gJ01pbGxpIEdhbCc7XG4gICAgICAgIGJyZWFrO1xuICAgICAgY2FzZSAnRk9PVCc6XG4gICAgICAgIHN0cmluZyA9ICdGZWV0JztcbiAgICAgICAgYnJlYWs7XG4gICAgICBjYXNlICdGT09UX1VTJzpcbiAgICAgICAgc3RyaW5nID0gJ2Z0IFVTJztcbiAgICAgICAgYnJlYWs7XG4gICAgICBjYXNlICdNRVRFUic6XG4gICAgICAgIHN0cmluZyA9ICdNZXRlcnMnO1xuICAgICAgICBicmVhaztcbiAgICAgIGNhc2UgJ0tJTE9NRVRFUic6XG4gICAgICAgIHN0cmluZyA9ICdLaWxvbWV0ZXJzJztcbiAgICAgICAgYnJlYWs7XG4gICAgICBjYXNlICdNSUxFJzpcbiAgICAgICAgc3RyaW5nID0gJ01pbGVzJztcbiAgICAgICAgYnJlYWs7XG4gICAgICBjYXNlICdZQVJEJzpcbiAgICAgICAgc3RyaW5nID0gJ1lhcmRzJztcbiAgICAgICAgYnJlYWs7XG4gICAgICBjYXNlICdTVEFOREFSRCc6XG4gICAgICAgIHN0cmluZyA9ICdELVMnO1xuICAgICAgICBicmVhaztcbiAgICAgIGNhc2UgJ1BPT1InOlxuICAgICAgICBzdHJpbmcgPSAnRC1QJztcbiAgICAgICAgYnJlYWs7XG4gICAgICBjYXNlICdERUZJTklUSVZFX0lOVEVSUE9MQVRFRCc6XG4gICAgICAgIHN0cmluZyA9ICdELUknO1xuICAgICAgICBicmVhaztcbiAgICAgIGNhc2UgJ0JBRCc6XG4gICAgICAgIHN0cmluZyA9ICdYLUInO1xuICAgICAgICBicmVhaztcbiAgICAgIGNhc2UgJ0FDQ19DSEVDSyc6XG4gICAgICAgIHN0cmluZyA9ICdYLUEnO1xuICAgICAgICBicmVhaztcbiAgICAgIGNhc2UgJ0NIRUNLU0hPVCc6XG4gICAgICAgIHN0cmluZyA9ICdYLUMnO1xuICAgICAgICBicmVhaztcbiAgICAgIGNhc2UgJ0lOVEVSUE9MQVRFRCc6XG4gICAgICAgIHN0cmluZyA9ICdYLUknO1xuICAgICAgICBicmVhaztcbiAgICAgIGRlZmF1bHQ6XG4gICAgICAgIHN0cmluZyA9IHVuaXQ7XG4gICAgICAgIGJyZWFrO1xuICAgIH1cbiAgICByZXR1cm4gc3RyaW5nO1xuICB9XG4gIC8vIGV4cG9ydCBjb25zdCBmb3JtYXRPYmplY3QgPSB7XG4gIC8vICAgJ0RFR1JFRSc6ICfCsCcsXG4gIC8vICAgJ0dSQURJQU4nOiAnZ29uJyxcbiAgLy8gICAnUkFESUFOJzogJ3JhZCcsXG4gIC8vICAgJ05BTk9URVNMQSc6ICduVCcsXG4gIC8vICAgJ01JQ1JPVEVTTEEnOiAndVQnLFxuICAvLyAgICdNSUxMSVRFU0xBJzogJ21UJyxcbiAgLy8gICAnVEVTTEEnOiAnVCcsXG4gIC8vICAgJ0dBVVNTJzogJ0dhdXNzJyxcbiAgLy8gICAnR0FNTUEnOiAnR2FtbWEnLFxuICAvLyAgICdNX1BFUl9TRUNfU1EnOiAnTSBQZXIgU2VjIFNxJyxcbiAgLy8gICAnRlRfUEVSX1NFQ19TUSc6ICdGdCBQZXIgU2VjIFNxJyxcbiAgLy8gICAnRyc6ICdHJyxcbiAgLy8gICAnTUlMTElfRyc6ICdNaWxsaSBHJyxcbiAgLy8gICAnR185OCc6ICdHIDk4JyxcbiAgLy8gICAnR0FMJzogJ0dhbCcsXG4gIC8vICAgJ01JTExJX0dBTCc6ICdNaWxsaSBHYWwnLFxuICAvLyAgICdGT09UJzogJ0ZlZXQnLFxuICAvLyAgICdGT09UX1VTJzogJ2Z0IFVTJyxcbiAgLy8gICAnS0lMT01FVEVSJzogJ0tpbG9tZXRlcnMnLFxuICAvLyAgICdNRVRFUic6ICdNZXRlcnMnLFxuICAvLyAgICdNSUxFJzogJ01pbGVzJyxcbiAgLy8gICAnWUFSRCc6ICdZYXJkcycsXG4gIC8vICAgJ1NUQU5EQVJEJzogJ0QtUycsXG4gIC8vICAgJ1BPT1InOiAnRC1QJyxcbiAgLy8gICAnREVGSU5JVElWRV9JTlRFUlBPTEFURUQnOiAnRC1JJyxcbiAgLy8gICAnQkFEJzogJ1gtQicsXG4gIC8vICAgJ0FDQ19DSEVDSyc6ICdYLUEnLFxuICAvLyAgICdDSEVDS1NIT1QnOiAnWC1DJyxcbiAgLy8gICAnSU5URVJQT0xBVEVEJzogJ1gtSSdcbiAgLy8gfTtcbn1cbiJdfQ==