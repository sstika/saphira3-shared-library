/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Pipe } from '@angular/core';
var OrderSurveysByMdPipe = /** @class */ (function () {
    function OrderSurveysByMdPipe() {
        this.filtered = [];
    }
    /**
     * @param {?} items
     * @param {?=} reverse
     * @return {?}
     */
    OrderSurveysByMdPipe.prototype.transform = /**
     * @param {?} items
     * @param {?=} reverse
     * @return {?}
     */
    function (items, reverse) {
        var _this = this;
        items.forEach((/**
         * @param {?} item
         * @return {?}
         */
        function (item) {
            _this.filtered.push(item);
        }));
        this.filtered.sort((/**
         * @param {?} a
         * @param {?} b
         * @return {?}
         */
        function (a, b) {
            return (a.md.value > b.md.value ? 1 : -1);
        }));
        if (reverse) {
            this.filtered.reverse();
        }
        return this.filtered;
    };
    OrderSurveysByMdPipe.decorators = [
        { type: Pipe, args: [{
                    name: 'orderSurveysByMd'
                },] }
    ];
    return OrderSurveysByMdPipe;
}());
export { OrderSurveysByMdPipe };
if (false) {
    /** @type {?} */
    OrderSurveysByMdPipe.prototype.filtered;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib3JkZXItc3VydmV5cy1ieS1tZC5waXBlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGhwdGVjaC9zYXBoaXJhLXNoYXJlZC1saWIvIiwic291cmNlcyI6WyJsaWIvcGlwZXMvb3JkZXItc3VydmV5cy1ieS1tZC5waXBlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsSUFBSSxFQUFpQixNQUFNLGVBQWUsQ0FBQztBQUVwRDtJQUFBO1FBS0UsYUFBUSxHQUFHLEVBQUUsQ0FBQztJQWFoQixDQUFDOzs7Ozs7SUFaQyx3Q0FBUzs7Ozs7SUFBVCxVQUFVLEtBQVUsRUFBRSxPQUFhO1FBQW5DLGlCQVdDO1FBVkMsS0FBSyxDQUFDLE9BQU87Ozs7UUFBQyxVQUFBLElBQUk7WUFDaEIsS0FBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDM0IsQ0FBQyxFQUFDLENBQUM7UUFDSCxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUk7Ozs7O1FBQUMsVUFBQyxDQUFDLEVBQUUsQ0FBQztZQUN0QixPQUFPLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxLQUFLLEdBQUcsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUM1QyxDQUFDLEVBQUMsQ0FBQztRQUNILElBQUksT0FBTyxFQUFFO1lBQ1gsSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLEVBQUUsQ0FBQztTQUN6QjtRQUNELE9BQU8sSUFBSSxDQUFDLFFBQVEsQ0FBQztJQUN2QixDQUFDOztnQkFqQkYsSUFBSSxTQUFDO29CQUNKLElBQUksRUFBRSxrQkFBa0I7aUJBQ3pCOztJQWdCRCwyQkFBQztDQUFBLEFBbEJELElBa0JDO1NBZFksb0JBQW9COzs7SUFDL0Isd0NBQWMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBQaXBlLCBQaXBlVHJhbnNmb3JtIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBQaXBlKHtcbiAgbmFtZTogJ29yZGVyU3VydmV5c0J5TWQnXG59KVxuLy8gT3JkZXJzIHRoZSBzdXJ2ZXlzIGluIGFzY2VuZGluZy9kZXNjZW5kaW5nIG9yZGVyIGJhc2VkIG9uIHRoZSBNZCB2YWx1ZVxuZXhwb3J0IGNsYXNzIE9yZGVyU3VydmV5c0J5TWRQaXBlIGltcGxlbWVudHMgUGlwZVRyYW5zZm9ybSB7XG4gIGZpbHRlcmVkID0gW107XG4gIHRyYW5zZm9ybShpdGVtczogYW55LCByZXZlcnNlPzogYW55KTogYW55IHtcbiAgICBpdGVtcy5mb3JFYWNoKGl0ZW0gPT4ge1xuICAgICAgdGhpcy5maWx0ZXJlZC5wdXNoKGl0ZW0pO1xuICAgIH0pO1xuICAgIHRoaXMuZmlsdGVyZWQuc29ydCgoYSwgYikgPT4ge1xuICAgICAgcmV0dXJuIChhLm1kLnZhbHVlID4gYi5tZC52YWx1ZSA/IDEgOiAtMSk7XG4gICAgfSk7XG4gICAgaWYgKHJldmVyc2UpIHtcbiAgICAgIHRoaXMuZmlsdGVyZWQucmV2ZXJzZSgpO1xuICAgIH1cbiAgICByZXR1cm4gdGhpcy5maWx0ZXJlZDtcbiAgfVxufVxuIl19