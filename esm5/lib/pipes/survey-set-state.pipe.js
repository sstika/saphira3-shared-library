/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Pipe } from '@angular/core';
var SurveySetStatePipe = /** @class */ (function () {
    function SurveySetStatePipe() {
    }
    /**
     * @param {?} value
     * @return {?}
     */
    SurveySetStatePipe.prototype.transform = /**
     * @param {?} value
     * @return {?}
     */
    function (value) {
        switch (value) {
            case 'WAITING_SURVEY_SUBMIT':
                return 'Waiting for a new survey';
            case 'WAITING_CORRECTION':
                return 'Waiting for a correction';
            case 'WAITING_VALIDATION':
                return 'Waiting for validation';
            default:
                return 'Unknown';
        }
    };
    SurveySetStatePipe.decorators = [
        { type: Pipe, args: [{
                    name: 'surveySetState'
                },] }
    ];
    return SurveySetStatePipe;
}());
export { SurveySetStatePipe };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3VydmV5LXNldC1zdGF0ZS5waXBlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGhwdGVjaC9zYXBoaXJhLXNoYXJlZC1saWIvIiwic291cmNlcyI6WyJsaWIvcGlwZXMvc3VydmV5LXNldC1zdGF0ZS5waXBlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsSUFBSSxFQUFpQixNQUFNLGVBQWUsQ0FBQztBQUVwRDtJQUFBO0lBaUJBLENBQUM7Ozs7O0lBWkMsc0NBQVM7Ozs7SUFBVCxVQUFVLEtBQWE7UUFDckIsUUFBUSxLQUFLLEVBQUU7WUFDYixLQUFLLHVCQUF1QjtnQkFDMUIsT0FBTywwQkFBMEIsQ0FBQztZQUNwQyxLQUFLLG9CQUFvQjtnQkFDdkIsT0FBTywwQkFBMEIsQ0FBQztZQUNwQyxLQUFLLG9CQUFvQjtnQkFDdkIsT0FBTyx3QkFBd0IsQ0FBQztZQUNsQztnQkFDRSxPQUFPLFNBQVMsQ0FBQztTQUNwQjtJQUNILENBQUM7O2dCQWhCRixJQUFJLFNBQUM7b0JBQ0osSUFBSSxFQUFFLGdCQUFnQjtpQkFDdkI7O0lBZUQseUJBQUM7Q0FBQSxBQWpCRCxJQWlCQztTQWRZLGtCQUFrQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFBpcGUsIFBpcGVUcmFuc2Zvcm0gfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQFBpcGUoe1xuICBuYW1lOiAnc3VydmV5U2V0U3RhdGUnXG59KVxuZXhwb3J0IGNsYXNzIFN1cnZleVNldFN0YXRlUGlwZSBpbXBsZW1lbnRzIFBpcGVUcmFuc2Zvcm0ge1xuXG4gIHRyYW5zZm9ybSh2YWx1ZTogc3RyaW5nKTogYW55IHtcbiAgICBzd2l0Y2ggKHZhbHVlKSB7XG4gICAgICBjYXNlICdXQUlUSU5HX1NVUlZFWV9TVUJNSVQnOlxuICAgICAgICByZXR1cm4gJ1dhaXRpbmcgZm9yIGEgbmV3IHN1cnZleSc7XG4gICAgICBjYXNlICdXQUlUSU5HX0NPUlJFQ1RJT04nOlxuICAgICAgICByZXR1cm4gJ1dhaXRpbmcgZm9yIGEgY29ycmVjdGlvbic7XG4gICAgICBjYXNlICdXQUlUSU5HX1ZBTElEQVRJT04nOlxuICAgICAgICByZXR1cm4gJ1dhaXRpbmcgZm9yIHZhbGlkYXRpb24nO1xuICAgICAgZGVmYXVsdDpcbiAgICAgICAgcmV0dXJuICdVbmtub3duJztcbiAgICB9XG4gIH1cbn1cbiJdfQ==