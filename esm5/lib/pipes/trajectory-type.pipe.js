/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Pipe } from '@angular/core';
var TrajectoryTypePipe = /** @class */ (function () {
    function TrajectoryTypePipe() {
    }
    /**
     * @param {?} value
     * @param {?=} args
     * @return {?}
     */
    TrajectoryTypePipe.prototype.transform = /**
     * @param {?} value
     * @param {?=} args
     * @return {?}
     */
    function (value, args) {
        switch (value) {
            case 'ACTUAL':
                return 'Actual';
            case 'DEFINITIVE':
                return 'Definitive';
            case 'PLAN':
                return 'Prototype';
            case 'PROTOTYPE':
                return 'Completed';
            case 'REPORTED':
                return 'Reported';
        }
    };
    TrajectoryTypePipe.decorators = [
        { type: Pipe, args: [{
                    name: 'trajectoryType'
                },] }
    ];
    return TrajectoryTypePipe;
}());
export { TrajectoryTypePipe };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidHJhamVjdG9yeS10eXBlLnBpcGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AaHB0ZWNoL3NhcGhpcmEtc2hhcmVkLWxpYi8iLCJzb3VyY2VzIjpbImxpYi9waXBlcy90cmFqZWN0b3J5LXR5cGUucGlwZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLElBQUksRUFBaUIsTUFBTSxlQUFlLENBQUM7QUFFcEQ7SUFBQTtJQW1CQSxDQUFDOzs7Ozs7SUFkQyxzQ0FBUzs7Ozs7SUFBVCxVQUFVLEtBQVUsRUFBRSxJQUFVO1FBQzlCLFFBQVEsS0FBSyxFQUFFO1lBQ2IsS0FBSyxRQUFRO2dCQUNYLE9BQU8sUUFBUSxDQUFDO1lBQ2xCLEtBQUssWUFBWTtnQkFDZixPQUFPLFlBQVksQ0FBQztZQUN0QixLQUFLLE1BQU07Z0JBQ1QsT0FBTyxXQUFXLENBQUM7WUFDckIsS0FBSyxXQUFXO2dCQUNkLE9BQU8sV0FBVyxDQUFDO1lBQ3JCLEtBQUssVUFBVTtnQkFDYixPQUFPLFVBQVUsQ0FBQztTQUNyQjtJQUNILENBQUM7O2dCQWxCRixJQUFJLFNBQUM7b0JBQ0osSUFBSSxFQUFFLGdCQUFnQjtpQkFDdkI7O0lBaUJELHlCQUFDO0NBQUEsQUFuQkQsSUFtQkM7U0FoQlksa0JBQWtCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgUGlwZSwgUGlwZVRyYW5zZm9ybSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5AUGlwZSh7XG4gIG5hbWU6ICd0cmFqZWN0b3J5VHlwZSdcbn0pXG5leHBvcnQgY2xhc3MgVHJhamVjdG9yeVR5cGVQaXBlIGltcGxlbWVudHMgUGlwZVRyYW5zZm9ybSB7XG5cbiAgdHJhbnNmb3JtKHZhbHVlOiBhbnksIGFyZ3M/OiBhbnkpOiBhbnkge1xuICAgIHN3aXRjaCAodmFsdWUpIHtcbiAgICAgIGNhc2UgJ0FDVFVBTCc6XG4gICAgICAgIHJldHVybiAnQWN0dWFsJztcbiAgICAgIGNhc2UgJ0RFRklOSVRJVkUnOlxuICAgICAgICByZXR1cm4gJ0RlZmluaXRpdmUnO1xuICAgICAgY2FzZSAnUExBTic6XG4gICAgICAgIHJldHVybiAnUHJvdG90eXBlJztcbiAgICAgIGNhc2UgJ1BST1RPVFlQRSc6XG4gICAgICAgIHJldHVybiAnQ29tcGxldGVkJztcbiAgICAgIGNhc2UgJ1JFUE9SVEVEJzpcbiAgICAgICAgcmV0dXJuICdSZXBvcnRlZCc7XG4gICAgfVxuICB9XG59XG4iXX0=