/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Pipe } from '@angular/core';
var MagneticDecimalPipe = /** @class */ (function () {
    function MagneticDecimalPipe() {
        // number of decimal places to display for various angle units
        this.decimalMap = {
            // Old Format
            NANOTESLA: 0,
            MICROTESLA: 4,
            MILLITESLA: 0,
            TESLA: 0,
            GAUSS: 5,
            GAMMA: 0,
            // New Format
            Nanotesla: 0,
            nT: 0,
            Microtesla: 4,
            'µT': 4,
            Millitesla: 0,
            mT: 0,
            Tesla: 0,
            T: 0,
            Gauss: 5,
            Gs: 5,
            Gamma: 0,
            'γ': 0
        };
    }
    /**
     * @param {?} value
     * @param {?=} unit
     * @return {?}
     */
    MagneticDecimalPipe.prototype.transform = /**
     * @param {?} value
     * @param {?=} unit
     * @return {?}
     */
    function (value, unit) {
        if (!this.isValidFloat(value)) {
            return 'N/A';
        }
        else {
            value = parseFloat(value);
            if (typeof (this.decimalMap[unit]) !== 'undefined') {
                return this.formatNumber(value.toFixed(this.decimalMap[unit]));
            }
            else {
                return 'Unsupported Unit: ' + unit;
            }
        }
    };
    /**
     * isValidFloat:
     *
     * A stricter float filter than parseFloat().
     *
     * @param value Input value to be parsed as float
     * @returns float
     */
    /**
     * isValidFloat:
     *
     * A stricter float filter than parseFloat().
     *
     * @param {?} value Input value to be parsed as float
     * @return {?} float
     */
    MagneticDecimalPipe.prototype.isValidFloat = /**
     * isValidFloat:
     *
     * A stricter float filter than parseFloat().
     *
     * @param {?} value Input value to be parsed as float
     * @return {?} float
     */
    function (value) {
        return !isNaN(parseFloat(value));
    };
    /**
      * formatNumber:
      *
      * Adds commas and removes the negative sign for zero values.
      *
      * @param number Input value to be formatted
      */
    /**
     * formatNumber:
     *
     * Adds commas and removes the negative sign for zero values.
     *
     * @param {?} number Input value to be formatted
     * @return {?}
     */
    MagneticDecimalPipe.prototype.formatNumber = /**
     * formatNumber:
     *
     * Adds commas and removes the negative sign for zero values.
     *
     * @param {?} number Input value to be formatted
     * @return {?}
     */
    function (number) {
        return this.removeSignOnZero(this.addCommas(number));
    };
    /**
       * removeSignOnZero:
       *
       * If the value is zero, remove any negative signs. (converts -0.00 to 0.00)
       *
       * @param number Input value to be sanitized
       */
    /**
     * removeSignOnZero:
     *
     * If the value is zero, remove any negative signs. (converts -0.00 to 0.00)
     *
     * @param {?} number Input value to be sanitized
     * @return {?}
     */
    MagneticDecimalPipe.prototype.removeSignOnZero = /**
     * removeSignOnZero:
     *
     * If the value is zero, remove any negative signs. (converts -0.00 to 0.00)
     *
     * @param {?} number Input value to be sanitized
     * @return {?}
     */
    function (number) {
        /** @type {?} */
        var split = number.toString().split('-');
        if (split.length === 2) {
            if (split[1] === 0) {
                return split[1];
            }
        }
        return number;
    };
    /**
       * addCommas:
       *
       * Helper function that adds in commas to a number for readability.
       *
       * @param number Input value to add commas
       * @returns string
       */
    /**
     * addCommas:
     *
     * Helper function that adds in commas to a number for readability.
     *
     * @param {?} number Input value to add commas
     * @return {?} string
     */
    MagneticDecimalPipe.prototype.addCommas = /**
     * addCommas:
     *
     * Helper function that adds in commas to a number for readability.
     *
     * @param {?} number Input value to add commas
     * @return {?} string
     */
    function (number) {
        // check if we have a decimal
        /** @type {?} */
        var split = number.toString().split('.');
        // if we have a decimal
        if (split.length === 2) {
            // only add commas to the first half and return the second half
            return split[0].replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,') + '.' + split[1];
        }
        else {
            // add commas throughout
            return split[0].replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
        }
    };
    MagneticDecimalPipe.decorators = [
        { type: Pipe, args: [{
                    name: 'magneticDecimal'
                },] }
    ];
    return MagneticDecimalPipe;
}());
export { MagneticDecimalPipe };
if (false) {
    /** @type {?} */
    MagneticDecimalPipe.prototype.decimalMap;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFnbmV0aWMtZGVjaW1hbC5waXBlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGhwdGVjaC9zYXBoaXJhLXNoYXJlZC1saWIvIiwic291cmNlcyI6WyJsaWIvcGlwZXMvbWFnbmV0aWMtZGVjaW1hbC5waXBlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsSUFBSSxFQUFpQixNQUFNLGVBQWUsQ0FBQztBQUVwRDtJQUFBOztRQU9FLGVBQVUsR0FBRzs7WUFFWCxTQUFTLEVBQUUsQ0FBQztZQUNaLFVBQVUsRUFBRSxDQUFDO1lBQ2IsVUFBVSxFQUFFLENBQUM7WUFDYixLQUFLLEVBQUUsQ0FBQztZQUNSLEtBQUssRUFBRSxDQUFDO1lBQ1IsS0FBSyxFQUFFLENBQUM7O1lBRVIsU0FBUyxFQUFFLENBQUM7WUFDWixFQUFFLEVBQUUsQ0FBQztZQUNMLFVBQVUsRUFBRSxDQUFDO1lBQ2IsSUFBSSxFQUFFLENBQUM7WUFDUCxVQUFVLEVBQUUsQ0FBQztZQUNiLEVBQUUsRUFBRSxDQUFDO1lBQ0wsS0FBSyxFQUFFLENBQUM7WUFDUixDQUFDLEVBQUUsQ0FBQztZQUNKLEtBQUssRUFBRSxDQUFDO1lBQ1IsRUFBRSxFQUFFLENBQUM7WUFDTCxLQUFLLEVBQUUsQ0FBQztZQUNSLEdBQUcsRUFBRSxDQUFDO1NBQ1AsQ0FBQztJQTJFSixDQUFDOzs7Ozs7SUF6RUMsdUNBQVM7Ozs7O0lBQVQsVUFBVSxLQUFVLEVBQUUsSUFBVTtRQUM5QixJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxLQUFLLENBQUMsRUFBRTtZQUM3QixPQUFPLEtBQUssQ0FBQztTQUNkO2FBQU07WUFDTCxLQUFLLEdBQUcsVUFBVSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQzFCLElBQUksT0FBTyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLENBQUMsS0FBSyxXQUFXLEVBQUU7Z0JBQ2xELE9BQU8sSUFBSSxDQUFDLFlBQVksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO2FBQ2hFO2lCQUFNO2dCQUNMLE9BQU8sb0JBQW9CLEdBQUcsSUFBSSxDQUFDO2FBQ3BDO1NBQ0Y7SUFDSCxDQUFDO0lBRUQ7Ozs7Ozs7T0FPRzs7Ozs7Ozs7O0lBQ0ksMENBQVk7Ozs7Ozs7O0lBQW5CLFVBQW9CLEtBQUs7UUFDdkIsT0FBTyxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztJQUNuQyxDQUFDO0lBRUQ7Ozs7OztRQU1JOzs7Ozs7Ozs7SUFDRywwQ0FBWTs7Ozs7Ozs7SUFBbkIsVUFBb0IsTUFBTTtRQUN4QixPQUFPLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7SUFDdkQsQ0FBQztJQUVEOzs7Ozs7U0FNSzs7Ozs7Ozs7O0lBQ0UsOENBQWdCOzs7Ozs7OztJQUF2QixVQUF3QixNQUFNOztZQUN0QixLQUFLLEdBQUcsTUFBTSxDQUFDLFFBQVEsRUFBRSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUM7UUFDMUMsSUFBSSxLQUFLLENBQUMsTUFBTSxLQUFLLENBQUMsRUFBRTtZQUN0QixJQUFJLEtBQUssQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLEVBQUU7Z0JBQ2xCLE9BQU8sS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO2FBQ2pCO1NBQ0Y7UUFDRCxPQUFPLE1BQU0sQ0FBQztJQUNoQixDQUFDO0lBRUQ7Ozs7Ozs7U0FPSzs7Ozs7Ozs7O0lBQ0UsdUNBQVM7Ozs7Ozs7O0lBQWhCLFVBQWlCLE1BQU07OztZQUVmLEtBQUssR0FBRyxNQUFNLENBQUMsUUFBUSxFQUFFLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQztRQUMxQyx1QkFBdUI7UUFDdkIsSUFBSSxLQUFLLENBQUMsTUFBTSxLQUFLLENBQUMsRUFBRTtZQUN0QiwrREFBK0Q7WUFDL0QsT0FBTyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLHlCQUF5QixFQUFFLEtBQUssQ0FBQyxHQUFHLEdBQUcsR0FBRyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7U0FDNUU7YUFBTTtZQUNMLHdCQUF3QjtZQUN4QixPQUFPLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMseUJBQXlCLEVBQUUsS0FBSyxDQUFDLENBQUM7U0FDM0Q7SUFDSCxDQUFDOztnQkF0R0YsSUFBSSxTQUFDO29CQUNKLElBQUksRUFBRSxpQkFBaUI7aUJBQ3hCOztJQXFHRCwwQkFBQztDQUFBLEFBdkdELElBdUdDO1NBbEdZLG1CQUFtQjs7O0lBRTlCLHlDQXFCRSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFBpcGUsIFBpcGVUcmFuc2Zvcm0gfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQFBpcGUoe1xuICBuYW1lOiAnbWFnbmV0aWNEZWNpbWFsJ1xufSlcbi8vIFJldHVybnMgdGhlIGFuZ2xlIGFzIGEgc3RyaW5nIHdpdGggYW4gYW1vdW50IG9mIGRlY2ltYWwgcGxhY2VzIGJhc2VkIG9uIGl0cyB1bml0LlxuLy8gQWxzbyBmb3JtYXRzIHRoZSBzdHJpbmcgd2l0aCBjb21tYXMgZm9yIHJlYWRhYmlsaXR5LlxuZXhwb3J0IGNsYXNzIE1hZ25ldGljRGVjaW1hbFBpcGUgaW1wbGVtZW50cyBQaXBlVHJhbnNmb3JtIHtcbiAgLy8gbnVtYmVyIG9mIGRlY2ltYWwgcGxhY2VzIHRvIGRpc3BsYXkgZm9yIHZhcmlvdXMgYW5nbGUgdW5pdHNcbiAgZGVjaW1hbE1hcCA9IHtcbiAgICAvLyBPbGQgRm9ybWF0XG4gICAgTkFOT1RFU0xBOiAwLFxuICAgIE1JQ1JPVEVTTEE6IDQsXG4gICAgTUlMTElURVNMQTogMCxcbiAgICBURVNMQTogMCxcbiAgICBHQVVTUzogNSxcbiAgICBHQU1NQTogMCxcbiAgICAvLyBOZXcgRm9ybWF0XG4gICAgTmFub3Rlc2xhOiAwLFxuICAgIG5UOiAwLFxuICAgIE1pY3JvdGVzbGE6IDQsXG4gICAgJ8K1VCc6IDQsXG4gICAgTWlsbGl0ZXNsYTogMCxcbiAgICBtVDogMCxcbiAgICBUZXNsYTogMCxcbiAgICBUOiAwLFxuICAgIEdhdXNzOiA1LFxuICAgIEdzOiA1LFxuICAgIEdhbW1hOiAwLFxuICAgICfOsyc6IDBcbiAgfTtcblxuICB0cmFuc2Zvcm0odmFsdWU6IGFueSwgdW5pdD86IGFueSk6IGFueSB7XG4gICAgaWYgKCF0aGlzLmlzVmFsaWRGbG9hdCh2YWx1ZSkpIHtcbiAgICAgIHJldHVybiAnTi9BJztcbiAgICB9IGVsc2Uge1xuICAgICAgdmFsdWUgPSBwYXJzZUZsb2F0KHZhbHVlKTtcbiAgICAgIGlmICh0eXBlb2YgKHRoaXMuZGVjaW1hbE1hcFt1bml0XSkgIT09ICd1bmRlZmluZWQnKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmZvcm1hdE51bWJlcih2YWx1ZS50b0ZpeGVkKHRoaXMuZGVjaW1hbE1hcFt1bml0XSkpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgcmV0dXJuICdVbnN1cHBvcnRlZCBVbml0OiAnICsgdW5pdDtcbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICAvKipcbiAgICogaXNWYWxpZEZsb2F0OlxuICAgKlxuICAgKiBBIHN0cmljdGVyIGZsb2F0IGZpbHRlciB0aGFuIHBhcnNlRmxvYXQoKS5cbiAgICpcbiAgICogQHBhcmFtIHZhbHVlIElucHV0IHZhbHVlIHRvIGJlIHBhcnNlZCBhcyBmbG9hdFxuICAgKiBAcmV0dXJucyBmbG9hdFxuICAgKi9cbiAgcHVibGljIGlzVmFsaWRGbG9hdCh2YWx1ZSkge1xuICAgIHJldHVybiAhaXNOYU4ocGFyc2VGbG9hdCh2YWx1ZSkpO1xuICB9XG5cbiAgLyoqXG4gICAgKiBmb3JtYXROdW1iZXI6XG4gICAgKlxuICAgICogQWRkcyBjb21tYXMgYW5kIHJlbW92ZXMgdGhlIG5lZ2F0aXZlIHNpZ24gZm9yIHplcm8gdmFsdWVzLlxuICAgICpcbiAgICAqIEBwYXJhbSBudW1iZXIgSW5wdXQgdmFsdWUgdG8gYmUgZm9ybWF0dGVkXG4gICAgKi9cbiAgcHVibGljIGZvcm1hdE51bWJlcihudW1iZXIpIHtcbiAgICByZXR1cm4gdGhpcy5yZW1vdmVTaWduT25aZXJvKHRoaXMuYWRkQ29tbWFzKG51bWJlcikpO1xuICB9XG5cbiAgLyoqXG4gICAgICogcmVtb3ZlU2lnbk9uWmVybzpcbiAgICAgKlxuICAgICAqIElmIHRoZSB2YWx1ZSBpcyB6ZXJvLCByZW1vdmUgYW55IG5lZ2F0aXZlIHNpZ25zLiAoY29udmVydHMgLTAuMDAgdG8gMC4wMClcbiAgICAgKlxuICAgICAqIEBwYXJhbSBudW1iZXIgSW5wdXQgdmFsdWUgdG8gYmUgc2FuaXRpemVkXG4gICAgICovXG4gIHB1YmxpYyByZW1vdmVTaWduT25aZXJvKG51bWJlcikge1xuICAgIGNvbnN0IHNwbGl0ID0gbnVtYmVyLnRvU3RyaW5nKCkuc3BsaXQoJy0nKTtcbiAgICBpZiAoc3BsaXQubGVuZ3RoID09PSAyKSB7XG4gICAgICBpZiAoc3BsaXRbMV0gPT09IDApIHtcbiAgICAgICAgcmV0dXJuIHNwbGl0WzFdO1xuICAgICAgfVxuICAgIH1cbiAgICByZXR1cm4gbnVtYmVyO1xuICB9XG5cbiAgLyoqXG4gICAgICogYWRkQ29tbWFzOlxuICAgICAqXG4gICAgICogSGVscGVyIGZ1bmN0aW9uIHRoYXQgYWRkcyBpbiBjb21tYXMgdG8gYSBudW1iZXIgZm9yIHJlYWRhYmlsaXR5LlxuICAgICAqXG4gICAgICogQHBhcmFtIG51bWJlciBJbnB1dCB2YWx1ZSB0byBhZGQgY29tbWFzXG4gICAgICogQHJldHVybnMgc3RyaW5nXG4gICAgICovXG4gIHB1YmxpYyBhZGRDb21tYXMobnVtYmVyKSB7XG4gICAgLy8gY2hlY2sgaWYgd2UgaGF2ZSBhIGRlY2ltYWxcbiAgICBjb25zdCBzcGxpdCA9IG51bWJlci50b1N0cmluZygpLnNwbGl0KCcuJyk7XG4gICAgLy8gaWYgd2UgaGF2ZSBhIGRlY2ltYWxcbiAgICBpZiAoc3BsaXQubGVuZ3RoID09PSAyKSB7XG4gICAgICAvLyBvbmx5IGFkZCBjb21tYXMgdG8gdGhlIGZpcnN0IGhhbGYgYW5kIHJldHVybiB0aGUgc2Vjb25kIGhhbGZcbiAgICAgIHJldHVybiBzcGxpdFswXS5yZXBsYWNlKC8oXFxkKSg/PShcXGR7M30pKyg/IVxcZCkpL2csICckMSwnKSArICcuJyArIHNwbGl0WzFdO1xuICAgIH0gZWxzZSB7XG4gICAgICAvLyBhZGQgY29tbWFzIHRocm91Z2hvdXRcbiAgICAgIHJldHVybiBzcGxpdFswXS5yZXBsYWNlKC8oXFxkKSg/PShcXGR7M30pKyg/IVxcZCkpL2csICckMSwnKTtcbiAgICB9XG4gIH1cbn1cbiJdfQ==